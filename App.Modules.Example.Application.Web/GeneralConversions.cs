namespace App.Core.AppHost
{
    using App.Core.Domain.Entities;


    public static class GeneralConversions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gender"></param>
        /// <returns></returns>
        /// <internal>
        /// Interim solution used to map Gender enums to strings for 
        /// passing up to Knockout ViewModel, to back dropdowns.
        /// </internal>
        public static string Map(this Gender gender)
        {
            return gender.ToString().Substring(0, 1);
        }
    }
}