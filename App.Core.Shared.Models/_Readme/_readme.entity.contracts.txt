﻿# Contracts on Entities #


## Summary ##
Contracts on summaries do not add abstraction (there often is not much point to abstracting
Anemic Entities).

But they add a lot of value in a couple of other indirect ways:

* write less/reuse: By defining extension methods against contract, extension methods
  can be used against more than one type of entity.
* add more guarantees that code is coherent: contracts are just that...contracts. They ensure
  that what you are writing are living up to expected behavior/contracts.
  Ie, you end up with unexpected bugs due to mismatched type/name.
* add flexibility. If you want to rename a property, or change it's type, just update the 
  interface def, and Resharper will update all references to it.
* Save time with documentaiton: if you document the contract, GhostDoc will reuse the 
  contract's documentation on all the classes you applied the the interface.