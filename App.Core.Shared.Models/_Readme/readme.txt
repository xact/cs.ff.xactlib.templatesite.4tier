﻿
Whereas one instantiates services via the indirection of contracts
using either a ServiceLocator or (prefered) dependency injection
into a constructor, entities are instantiated directly.

Interfaces are applied to entities not for instantiation, but for
matching (eg ExtensionMethods, automatic PersistStrategies, etc.)



## Adjustments due to Entity Framework ##

To not have compilation loops -- while complying with DDD layout ---
one has to move the Entities out of the Infrastructure.Data assembly
into the Domain.Entities assembly.

The process to do that was the following:

* In Infrastructure.Data, right-click the the DataModel.tt and edit it by 
  hand, to comment out DependentUpon. 

    <None Include="DataModel.tt">
      <Generator>TextTemplatingFileGenerator</Generator>
      <!--<DependentUpon>DataModel.edmx</DependentUpon>-->
      <LastGenOutput>DataModel.cs</LastGenOutput>
    </None>

This makes the file moveable.
Copy/Paste it to the new Assembly.
Edit the DataModel.tt to adjust the path, so that it finds the original *.edmx:

    const string relativeDir = @"..\App.Core.Core.Infrastructure.Data.Model\";
    const string inputFile = relativeDir+@"DataModel.edmx";

The model will build the entities in the new assembly.

Finally, cleanup, by right-clicking the original DataModel.tt and turn its generation off,
by clearing out the 'TextTemplatingFileGenerator'


