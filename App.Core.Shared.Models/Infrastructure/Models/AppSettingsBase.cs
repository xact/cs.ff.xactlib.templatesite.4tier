﻿namespace App.Core.Infrastructure
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Settings;

    /// <summary>
    /// Abstract base class for an application specific <see cref="Settings"/>
    /// object.
    /// <para>
    /// Contains a base set of settings common to all enterprise applications.
    /// Can be extended.
    /// </para>
    /// </summary>
    [DataContract(Name = "AppSettingsBase")]
    public class AppSettingsBase : XAct.Settings.ApplicationSettings
    {
                /// <summary>
        /// Initializes a new instance of the <see cref="AppSettings" /> class.
        /// </summary>
        public AppSettingsBase()
            : base()
        {
        }


        /// <summary>
        /// Gets the IQ logic search sep char.
        /// </summary>
        /// <value>
        /// The IQ logic search sep char.
        /// </value>
        /// <interal>
        /// During dev: on 10.240.11.145 it was "\t"
        /// </interal>
        public string Ping
        {
            get
            {
                string result;
                base.TryGetSettingValue("Ping", out result, "DefaultValue");
                return result;


            }

        }



        /// <summary>
        /// Gets a value indicating whether settings came from the Db.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [ping db]; otherwise, <c>false</c>.
        /// </value>
        public string PingDb
        {
            get
            {
                string result;
                base.TryGetSettingValue("PingDb", out result, "Default Value (not from Db or AppSettings).");
                return result;
            }
        }


        /// <summary>
        /// Gets the date default format.
        /// <para>
        /// Every single app has a need for a Default date format...as well as a short/long format....
        /// and then even with the best intentions, there are exceptions.
        /// </para>
        /// </summary>
        /// <value>
        /// The date default format.
        /// </value>
        public string DateDefaultFormat
        {
            get
            {
                string result;
                base.TryGetSettingValue("Formatting/DateDefaultFormat", out result, "d");
                return result;
            }
        }

        /// <summary>
        /// Gets the date short format.
        /// <para>
        /// Every single app has a need for a Default date format...as well as a short/long format....
        /// and then even with the best intentions, there are exceptions.
        /// </para>
        /// </summary>
        /// <value>
        /// The date short format.
        /// </value>
        public string DateShortFormat
        {
            get
            {
                string result;
                base.TryGetSettingValue("Formatting/DateShortFormat", out result, "d MMM yyyy");
                return result;

            }
        }

        /// <summary>
        /// Gets the date long format.
        /// <para>
        /// Every single app has a need for a Default date format...as well as a short/long format....
        /// and then even with the best intentions, there are exceptions.
        /// </para>
        /// </summary>
        /// <value>
        /// The date long format.
        /// </value>
        public string DateLongFormat
        {
            get
            {
                string result;
                base.TryGetSettingValue("Formatting/DateLongFormat", out result, "D");
                return result;


            }
        }

        /// <summary>
        /// Gets the time default format.
        /// <para>
        /// Every single app has a need for a Default time format...as well as a short/long format....
        /// and then even with the best intentions, there are exceptions.
        /// </para>
        /// </summary>
        /// <value>
        /// The time default format.
        /// </value>
        public string TimeDefaultFormat
        {
            get
            {
                string result;
                base.TryGetSettingValue("Formatting/TimeDefaultFormat", out result, "t");
                return result;

            }
        }

        /// <summary>
        /// Gets the time short format.
        /// <para>
        /// Every single app has a need for a Default time format...as well as a short/long format....
        /// and then even with the best intentions, there are exceptions.
        /// </para>
        /// </summary>
        /// <value>
        /// The time short format.
        /// </value>
        public string TimeShortFormat
        {
            get
            {
                string result;
                base.TryGetSettingValue("Formatting/TimeShortFormat", out result, "t");
                return result;


            }
        }

        /// <summary>
        /// Gets the time long format.
        /// <para>
        /// Every single app has a need for a Default time format...as well as a short/long format....
        /// and then even with the best intentions, there are exceptions.
        /// </para>
        /// </summary>
        /// <value>
        /// The time long format.
        /// </value>
        public string TimeLongFormat
        {
            get
            {
                string result;
                base.TryGetSettingValue("Formatting/TimeLongFormat", out result, "T");
                return result;

            }
        }

        /// <summary>
        /// Gets the date time default format.
        /// <para>
        /// Every single app has a need for a Default date/time format...as well as a short/long format....
        /// and then even with the best intentions, there are exceptions.
        /// </para>
        /// </summary>
        /// <value>
        /// The date time default format.
        /// </value>
        public string DateTimeDefaultFormat
        {
            get
            {
                string result;
                base.TryGetSettingValue("Formatting/DateTimeDefaultFormat", out result, "g");
                return result;

            }
        }

        /// <summary>
        /// Gets the date time short format.
        /// <para>
        /// Every single app has a need for a Default date/time format...as well as a short/long format....
        /// and then even with the best intentions, there are exceptions.
        /// </para>
        /// </summary>
        /// <value>
        /// The date time short format.
        /// </value>
        public string DateTimeShortFormat
        {
            get
            {
                string result;
                base.TryGetSettingValue("Formatting/DateTimeShortFormat", out result, "g");
                return result;
            }
        }

        /// <summary>
        /// Gets the date time long format.
        /// <para>
        /// Every single app has a need for a Default date/time format...as well as a short/long format....
        /// and then even with the best intentions, there are exceptions.
        /// </para>
        /// </summary>
        /// <value>
        /// The date time long format.
        /// </value>
        public string DateTimeLongFormat
        {
            get
            {
                string result;
                base.TryGetSettingValue("Formatting/DateTimeLongFormat", out result, "F");
                return result;

            }
        }


        public int DefaultPageSize
        {
            get
            {
                int size;
                base.TryGetSettingValue("Data/DefaultPageSize", out size, 20);
                return size;
            }
        }



        /// <summary>
        /// Gets the support address used on the Contact page.
        /// </summary>
        /// <value>
        /// The support address.
        /// </value>
        public FrontEndAddress SupportContactInfo
        {
            get
            {
                if (_supportAddress == null)
                {

                    string name;
                    base.TryGetSettingValue("Support/ContacInfo/Name", out name, "UNDEFINED");
                    string email;
                    base.TryGetSettingValue("Support/ContacInfo/Email", out email, "UNDEFINED");
                    string phone;
                    base.TryGetSettingValue("Support/ContacInfo/Phone", out phone, "UNDEFINED");
                    string phoneAfterHours;
                    base.TryGetSettingValue("Support/ContacInfo/PhoneAfterHours", out phoneAfterHours, "UNDEFINED");
                    string address;
                    base.TryGetSettingValue("Support/ContacInfo/Address", out address, "UNDEFINED");




                    _supportAddress = new FrontEndAddress
                    {
                        Name = name,
                        Email = email,
                        Phone = phone,
                        PhoneAfterHours = phoneAfterHours,
                        Address = address
                    };
                }

                return _supportAddress;
            }
        }

        private FrontEndAddress _supportAddress;



        public TimeSpan ShortCacheTimeInSeconds
        {
            get
            {

                int seconds;
                base.TryGetSettingValue<int>("Caching/ShortCacheTimeInSeconds", out seconds, 60);

                TimeSpan result = new TimeSpan(0, 0, 0, seconds);

                return (result > ReferenceCacheTimeSpan) ? ReferenceCacheTimeSpan : result;
            }

        }


        public TimeSpan ReferenceCacheTimeSpan
        {
            get
            {

                int minutes;
                base.TryGetSettingValue<int>("Caching/ReferenceCacheTimeSpanInMinutes", out minutes, 20);
                return new TimeSpan(0, 0, minutes);
            }

        }



        public bool Services_Messaging_SMTP_Enabled
        {
            get
            {
                bool result;
                base.TryGetSettingValue<bool>("Services/Messaging/SMTP/Enabled", out result, false);
                return result;
            }
        }

        public string Services_Messaging_SMTP_Host
        {
            get
            {
                string result;
                base.TryGetSettingValue<string>("Services/Messaging/SMTP/Host", out result, "NOT_SET");
                return result;
            }
        }
        public int Services_Messaging_SMTP_Port
        {
            get
            {
                int result;
                base.TryGetSettingValue<int>("Services/Messaging/SMTP/Port", out result, 25);
                return result;
            }
        }
        public bool Services_Messaging_SMTP_SSL
        {
            get
            {
                bool result;
                base.TryGetSettingValue<bool>("Services/Messaging/SMTP/SSL", out result, false);
                return result;
            }
        }
        public string Services_Messaging_SMTP_UserName
        {
            get
            {
                string result;
                base.TryGetSettingValue<string>("Services/Messaging/SMTP/UserName", out result, null);
                return result;
            }
        }

        public string Services_Messaging_SMTP_Password
        {
            get
            {
                string result;
                base.TryGetSettingValue<string>("Services/Messaging/SMTP/Password", out result, null);
                return result;
            }
        }


        /// <summary>
        /// Gets the smtp from address.
        /// </summary>
        /// <value>
        /// The smtp from address.
        /// </value>
        public string Services_Messaging_SMTP_FromAddress
        {
            get
            {
                string result;
                base.TryGetSettingValue("Services/Messaging/SMTP/FromAddress", out result, null);
                return result;
            }
        }


    }
}
