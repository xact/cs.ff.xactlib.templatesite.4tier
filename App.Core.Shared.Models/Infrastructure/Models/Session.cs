﻿using App.Core.Infrastructure.Models;

namespace App.Core.Domain.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An entity to persist a User's Session
    /// in the central datastore, making it available
    /// across load balanced servers.
    /// </summary>
    [DataContract]
    public class Session : SessionBase
    {
        //Re use the standard Session base,
        //We're not adding anything specific to this app.
    }
}