namespace App.Core.Domain.Models
{
    using System.Runtime.Serialization;
    using App.Core.Domain.Entities;

    [DataContract]
    public class Reference_InterfaceType : ReferenceDataBase<InterfaceType>, IHasReferenceDataBase
    {

    }
}
