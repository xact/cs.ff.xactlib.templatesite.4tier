﻿using System;

namespace App.Core.Infrastructure.Models
{
    using System.Runtime.Serialization;
    using XAct;


    /// <summary>
    /// Gets the relationship between a User and an SSO.
    /// </summary>
    [DataContract]
    public class UserToSSO : XAct.IHasDistributedGuidIdAndTimestamp
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets the FK to the User that owns.
        /// </summary>
        /// <value>
        /// The user fk.
        /// </value>
        [DataMember]
        public virtual Guid UserFK { get; set; }

        [DataMember]
        public virtual User User { get; set; }


        /// <summary>
        /// Gets or sets the SSO on the SSO side of things.
        /// </summary>
        [DataMember]
        public Guid SSOFK { get; set; }
        [DataMember]
        public SSO SSO { get; set; }

        
        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="UserToSSO"/> class.
        /// </summary>
        public UserToSSO()
        {
            this.GenerateDistributedId();
        }
    }
}
