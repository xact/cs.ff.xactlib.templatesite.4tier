namespace App.Core.Infrastructure.Models
{
    using System;
    using System.Runtime.Serialization;
    using App.Core.Domain.Entities;

    /// <summary>
    /// An entity to persist a User's Session
    /// in the central datastore, making it available
    /// across load balanced servers.
    /// </summary>
    [DataContract]
    public abstract class SessionBase : IHasPrimaryKey<Guid>
    {

        /*
        IMPORTANT:
        WCF is an *opt-in* serialization mechanism. 
        All properties you want serialized across the wire
        have to be marked DataMember, and the class with DataContract
        before they make it across the tier correctly.
        Domain Enum Type have to be marked with EnumMember.
        If you forget to add that, you'll be chasing annoying bugs.
        */

        /// <summary>
        /// Gets or sets the datastore identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the base64 serialized identity.
        /// <para>
        /// The session is encoded using Base64 serialization of 
        /// BinarySerialization as Identity's are not DataContractSerializable
        /// (ie cannot be passed between Tiers using WCF).
        /// </para>
        /// <para>
        /// The serialized identity allows for an <see cref="System.Security.Principal.IIdentity"/>,
        /// with it's <c>Claims</c> -- containing among other things, Authorisation Role information --
        /// being shared across servers.
        /// </para>
        /// <para>
        /// Advantages of saving the identity as a whole serialized object are:
        /// </para>
        /// <para>
        /// * reuse of datastore schema across applications (one column is all that is needed).
        /// </para>
        /// <para>
        /// * reduction of a means of tampering with authentication tokens in mid-flight of a Session.
        /// </para>
        /// </summary>
        /// <value>
        /// The serialized identity.
        /// </value>
        [DataMember]
        public virtual string SerializedIdentity { get; set; }        
    }
}
