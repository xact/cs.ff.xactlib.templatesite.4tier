﻿namespace App.Core.Infrastructure.Models
{
    using System;
    using System.Runtime.Serialization;
    using XAct;

    [DataContract]
    public class SSO : XAct.IHasDistributedGuidIdAndTimestamp, IHasName
    {
        /// <summary>
        /// Gets or sets the datastore identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the unique name for the identifier.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public string Name { get; set; }
    }
}