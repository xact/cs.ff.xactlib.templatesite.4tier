
namespace App.Core.Domain.Models
{
    using System;
    using System.Runtime.Serialization;
    using App.Core.Domain.Entities;
    using XAct;
    using XAct.Messages;

    /// <summary>
    /// <para>
    /// Base class for applications Reference data.
    /// </para>
    /// <para>
    /// Important:
    /// </para>
    /// <para>
    /// There's an <see cref="XAct.Messages.ReferenceDataBase{TId}"/>
    /// that would have been been so much easier to use here -- 
    /// but it's common that a client has specific needs beyond that
    /// are different.
    /// </para>
    /// <para>
    ///  The similarity in naming (<see cref="XAct.Messages.ReferenceDataBase{TId}"/>
    /// and <see cref="App.Core.Domain.Models.ReferenceDataBase{TId}"/>
    /// is a bit unfortunate -- one just has to remain vigilant).
    /// </para>
    /// <para>
    /// TODO: Get rid of most of the following, and inherit directly from XAct.Messages.ReferenceData
    /// but then go back to maps and Ignore() what the Ministry does not want to deal with.
    /// </para>
    /// </summary>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    [DataContract]
    public abstract class ReferenceDataBase<TId> : ReferenceDataCodedBase<TId>, IHasTimestamp

        where TId:struct
    {
        /*
        IMPORTANT:
        WCF is an *opt-in* serialization mechanism. 
        All properties you want serialized across the wire
        have to be marked DataMember, and the class with DataContract
        before they make it across the tier correctly.
        Domain Enum Type have to be marked with EnumMember.
        If you forget to add that, you'll be chasing annoying bugs.
        */

        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        //[DataMember]
        //public virtual string ModifiedBy { get; set; }

        ///// <summary>
        ///// Note that in this application, DateTimes are non-Utc.
        ///// </summary>
        //[DataMember]
        //public virtual DateTime ModifiedDateTime { get; set; }


    }
}