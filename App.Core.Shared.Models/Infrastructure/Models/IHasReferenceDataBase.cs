namespace App.Core.Domain.Models
{
    /// <summary>
    /// Contract applied to all reference data within the application
    /// </summary>
    public interface IHasReferenceDataBase
    {

    }
}