﻿namespace App.Core.Domain.Security
{
    using System;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Security.Principal;
    using XAct;

    /// <summary>
    /// Abstract base class for an application specific
    /// <c>AppIdentity</c>.
    /// <para>
    /// </para>
    /// </summary>
    [Serializable]
    public abstract class AppIdentityBase : GenericIdentity,  IHasInnerItem<IIdentity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppIdentityBase" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        protected AppIdentityBase(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppIdentityBase"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="claims">The claims.</param>
        protected AppIdentityBase(string name, IEnumerable<Claim> claims)
            : this(name)
        {
            AddClaims(claims);
        }

        /// <summary>
        /// Gets or sets the session token for the 
        /// Session to which this <c>AppIdentity</c> is associated.
        /// <para>
        /// Note that the session token -- or an XOR'ed version of it -- 
        /// is round-tripped to the UserAgents.
        /// </para>
        /// </summary>
        /// <value>
        /// The session token.
        /// </value>
        public Guid? SessionToken { get; set; }

        /// <summary>
        /// Serializes the identity to base64 format.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public string SerializeToBase64()
        {
            SerializationMethod serializationMethod = SerializationMethod.Base64Binary;

            return this.GetType()
                .Serialize(this, ref serializationMethod);
        }

        /// <summary>
        /// Deserializes the App session from base64 string
        /// </summary>
        /// <param name="identity">The identity.</param>
        /// <returns>
        /// App Session Entity
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public static AppIdentityBase DeserializeFromBase64<T>(string identity)
            where T:AppIdentityBase
        {
            return (T)
                   typeof(T)
                       .DeSerialize(identity, SerializationMethod.Base64Binary);
        }

        

        ///// <summary>
        ///// Validates the serialized identity.
        ///// </summary>
        ///// <param name="identity">The identity.</param>
        ///// <returns></returns>
        //public static bool ValidateSerializedIdentity(string identity)
        //{
        //    return DeserializeFromBase64(identity) != null;
        //}


        public void SetInnerObject(object o)
        {
            _innerItem = o as IIdentity;
        }

        public TItem GetInnerItem<TItem>()
        {
            return (TItem)_innerItem;
        }
        IIdentity _innerItem;
    }
}