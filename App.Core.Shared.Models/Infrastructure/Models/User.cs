﻿using System;

namespace App.Core.Infrastructure.Models
{
    using System.Collections.ObjectModel;
    using XAct;
    using System.Runtime.Serialization;

    [DataContract]
    public class User : IHasDistributedGuidIdAndTimestamp
    {
        /// <summary>
        /// Uniq
        /// </summary>
        [DataMember]
        public Guid Id { get; set; }

        
        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        [DataMember]
        public byte[] Timestamp { get; set; }



        //[DataMember]
        //public DateTime LastLoginDateTimeUtc { get; set; }

        /// <summary>
        /// Gets or sets the Display name to render in UX scenarios.
        /// </summary>
        /// <value>
        /// The display name.
        /// </value>
        [DataMember]
        public string DisplayName { get; set; }


        /// <summary>
        /// Gets the collection of User-to-SSO identifiers.
        /// <para>
        /// Depending on the number of SSOs used, this should be on 1*.
        /// </para>
        /// </summary>
        /// <value>
        /// The user sso identifiers.
        /// </value>
        public virtual Collection<UserToSSO> UserSSOIdentifiers { get { return _userssoIdentifier ?? (_userssoIdentifier = new Collection<UserToSSO>()); } }
        [DataMember]
        private Collection<UserToSSO> _userssoIdentifier;


        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        public User()
        {
            this.GenerateDistributedId();
        }

    }
}
