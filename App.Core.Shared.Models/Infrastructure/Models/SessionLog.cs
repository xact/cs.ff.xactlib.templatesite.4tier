
using App.Core.Domain.Models;

namespace App.Core.Infrastructure.Models
{
    using System;
    using System.Runtime.Serialization;
    using App.Core.Domain.Entities;

    [DataContract]
    public class SessionLog : IHasPrimaryKey<Guid>
    {

        /*
        IMPORTANT:
        WCF is an *opt-in* serialization mechanism. 
        All properties you want serialized across the wire
        have to be marked DataMember, and the class with DataContract
        before they make it across the tier correctly.
        Domain Enum Type have to be marked with EnumMember.
        If you forget to add that, you'll be chasing annoying bugs.
        */

        [DataMember]
        public virtual Guid Id { get; set; }

        [DataMember]
        public virtual DateTime LastAccessedDateTime { get; set; }

        [DataMember]
        public virtual string SSOType { get; set; }

        [DataMember]
        public virtual string SSOIdentifier { get; set; }

        [DataMember]
        public virtual string User { get; set; }

        [DataMember]
        public virtual InterfaceType InterfaceTypeFK { get; set; }


        [IgnoreDataMember]//REF
        public virtual Reference_InterfaceType InterfaceType { get; set; }

        [DataMember]
        public virtual DateTime LoggedInDateTime { get; set; }

        [DataMember]
        public virtual DateTime? LoggedOutDateTime { get; set; }

        [DataMember]
        public virtual string RemoteIp { get; set; }

		[DataMember]
		public virtual string OrganisationCode { get; set; }
    }
}
