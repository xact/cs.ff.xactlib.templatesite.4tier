﻿
namespace App.Core.Constants.Caching
{
    public static class CacheKeys
    {
        /// <summary>
        /// <para>
        /// Value is '_'
        /// </para>
        /// 
        /// </summary>
        public static string CacheKeyPrefix = "_:";

        //Cache Key for storage of whole AppSettings object.
        /// <summary>
        /// <para>
        /// Value is '_AppSetting:'
        /// </para>
        /// </summary>
        public static string AppSettingsKey = CacheKeyPrefix + "AppSettings";

        //Prefix to put before each Parameter as it is persisted in Cache.
        /// <summary>
        /// <para>
        /// Value is '_Parameter:'
        /// </para>
        /// </summary>
        public static string ParameterKeyPrefix = CacheKeyPrefix + "Parameter:";

        /// <summary>
        /// <para>
        /// Value is '_Resource:'
        /// </para>
        /// </summary>
        public static string ResourceKeyPrefix = CacheKeyPrefix + "Resource:";

        /// <summary>
        /// <para>
        /// Value is '_SessionToken:'
        /// </para>
        /// </summary>
        public static string SessionTokenPrefix = CacheKeyPrefix + "SessionToken:";

        /// <summary>
        /// Prefix for Front Tier caching of Immutable Reference Data.
        /// <para>
        /// In the back tier it is used to cache
        /// <see cref="ReferenceData"/> sets per 
        /// type of <see cref="Domain.Messages.ReferenceData"/>
        /// </para>
        /// <summary>
        /// <para>
        /// Value is '_RefData:'
        /// </para>
        /// </summary>
        /// </summary>
        public static string ReferenceDataPrefix = CacheKeyPrefix = "RefData:";
    }
}
