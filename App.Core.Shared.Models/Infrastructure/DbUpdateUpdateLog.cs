﻿using System;

namespace XAct.Data.Updates
{
    public class UpdateLog : IHasDistributedGuidIdAndTimestamp, IHasName, IHasDateTimeCreatedOnUtc, IHasDescription
    {
        public Guid Id { get; set; }

        public byte[] Timestamp { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime? CreatedOnUtc { get; set; }

        public UpdateLog()
        {
            this.GenerateDistributedId();
        }
    }
}
