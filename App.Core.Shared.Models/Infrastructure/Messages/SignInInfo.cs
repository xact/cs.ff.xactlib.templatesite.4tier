﻿namespace App.Core.Domain.Messages
{
    using System.Runtime.Serialization;
    using App.Core.Domain.Entities;

    [DataContract]
    public class SignInInfo
    {
        [DataMember]
        public virtual string UserName { get; set; }

        /// <summary>
        /// Gets or sets the sso service type.
        /// <para>
        /// Eg: OATH
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string SSOProtocol { get; set; }

        /// <summary>
        /// Gets or sets the sso identifier.
        /// <para>
        /// Eg: Facebook, Bitbucket, GitHub, ESAA
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string SSOIdentifier { get; set; }


        /// <summary>
        /// Gets or sets the interface type.
        /// <para>
        /// Eg: CSV, XML, WebService, WCF, REST, WEB)
        /// </para>
        /// </summary>
        /// <value>
        /// The interface type fk.
        /// </value>
        [DataMember]
        public virtual InterfaceType InterfaceTypeFK { get; set; }

        /// <summary>
        /// Gets or sets the user agent's remote ip.
        /// <para>
        /// eg: "127.1.2.3"
        /// </para>
        /// </summary>
        /// <value>
        /// The remote ip.
        /// </value>
        [DataMember]
        public virtual string RemoteIp { get; set; }

        
        
        [DataMember]
        public virtual string OrganisationId { get; set; }
    }
}
