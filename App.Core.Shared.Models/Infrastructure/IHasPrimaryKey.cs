﻿namespace App.Core.Domain.Entities
{
    using XAct;

    public interface IHasPrimaryKey<TPk> : IHasId<TPk>
    {
    }
}
