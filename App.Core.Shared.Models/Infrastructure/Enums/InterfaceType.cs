﻿// For enums, use the same namespace as the entities they describe 

// ReSharper disable CheckNamespace

using App.Core.Infrastructure.Models;

namespace App.Core.Domain.Entities
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;
    using App.Core.Domain.Models;

    /// <summary>
    /// The method of access used by this <see cref="SessionBase"/>.
    /// </summary>
    [DataContract]
    public enum InterfaceType
    {
        /// <summary>
        ///     No value specified.
        ///     <para>
        ///         NOTE: This is an error condition (a speficified type is expected).
        ///     </para>
        ///     <para>
        ///         Value = 0
        ///     </para>
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        /// The current Session was initiated via a
        /// Website interface.
        /// <para>
        /// Value =1
        /// </para>
        /// </summary>
        [EnumMember] Web = 1,


        /// <summary>
        /// The current Session was initiated via a
        /// CSV interface.
        /// <para>
        /// Value = 2
        /// </para>
        /// </summary>
        [EnumMember]
        BATCH = 2,

        /// <summary>
        /// The current Session was initiated via a
        /// POX interface.
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        [EnumMember] XML = 3,
         
        /// <summary>
        /// The current Session was initiated via a
        /// WebService/Soap interface.
        /// </summary>
        [EnumMember]
        WebServer = 4,

        /// <summary>
        /// The current Session was initiated via a
        /// WCF/Soap interface.
        /// </summary>
        [EnumMember] SOAP=5,

        /// <summary>
        /// The current session was initiated via 
        /// the Rest interface.
        /// </summary>
        [EnumMember] REST=6,
    }



}