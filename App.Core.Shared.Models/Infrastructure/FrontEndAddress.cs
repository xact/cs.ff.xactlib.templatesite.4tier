﻿
namespace App.Core.Infrastructure
{
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    /// <internal></internal>
    [DataContract]
    public class FrontEndAddress
    {
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Email { get; set; }
        
        [DataMember]
        public string Phone { get; set; }
        
        [DataMember]
        public string PhoneAfterHours { get; set; }

        [DataMember]
        public string Address { get; set; }
    }
}
