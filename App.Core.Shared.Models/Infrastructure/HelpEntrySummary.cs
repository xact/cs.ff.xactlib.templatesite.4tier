﻿// ReSharper disable CheckNamespace
namespace App.Core.Domain.Messages
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    using XAct;

    /// <summary/>
    [DataContract]
    public class HelpEntrySummary
        : IHasChildrenCollectionReadOnly<HelpEntrySummary>
        // : XAct.Assistance.HelpEntrySummary

    {

        [DataMember]
        private ICollection<HelpEntrySummary> _children;

        /// <summary>
        /// Gets or sets the key.
        /// 
        /// <para>
        /// Member defined in<see cref="T:XAct.IHasKey"/>
        /// </para>
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The key.
        /// 
        /// </value>
        [DataMember]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The value.
        /// 
        /// </value>
        [DataMember]
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets a collection of nested child entries, if any exist.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The child entries.
        /// 
        /// </value>
        public ICollection<HelpEntrySummary> Children
        {
            get
            {
                return this._children ?? (this._children = (ICollection<HelpEntrySummary>)new Collection<HelpEntrySummary>());
            }
        }


        public HelpEntrySummary()
        {
            OnDeserializing(default(StreamingContext));
        }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext streamingContext)
        {
            if (_children == null)
            {
                _children = new Collection<HelpEntrySummary>();
            }
        }
    }
}
