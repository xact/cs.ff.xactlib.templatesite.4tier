﻿namespace App.Core
{
    public interface IHasAppCoreService : XAct.IHasService, XAct.IHasMediumBindingPriority
    {
    }
    public interface IHasAppCoreServiceConfiguration : XAct.IHasServiceConfiguration, XAct.IHasMediumBindingPriority
    {
        
    }
    public interface IHasAppCoreServiceState : XAct.IHasServiceConfiguration, XAct.IHasMediumBindingPriority
    {

    }


    public interface IHasAppService : XAct.IHasService, XAct.IHasMediumBindingPriority
    {
    }

    public interface IHasAppServiceConfiguration : XAct.IHasServiceConfiguration, XAct.IHasMediumBindingPriority
    {

    }
    public interface IHasAppServiceState : XAct.IHasServiceConfiguration, XAct.IHasMediumBindingPriority
    {

    }
}
