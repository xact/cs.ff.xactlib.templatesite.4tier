﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Domain.Entities
{
    public class MigrationTest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
