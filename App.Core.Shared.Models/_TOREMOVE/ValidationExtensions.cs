﻿//using System;
//using System.Linq;
//using System.Text.RegularExpressions;

//namespace App.Core.Domain.Extensions
//{
//    public static class ValidationExtensions
//    {
//        // "Names must start with an apostrophe or an alphabetic character. "
//        private static readonly Regex Regex_DoesNameStartWithValidLettersRegex = new Regex(@"^[a-zA-Z~\'\ ]", RegexOptions.Compiled);


//        // Used for rule #272
//        // Allowed characters: alphabetic, space, hyphen, apostrophe - or a single tilde ('~')
//        // Unicode - The p{L} part should allow unicode letters.
//        // 28/08/14 - We are not accepting diacritics for now, so removing  p{L} in the regex expressions.
//        // It is expected for some diacritics to be added back in, sometime in the future. ValidtionTest has also been added to not accept diacritics.
//        private static readonly Regex Regex_DoesNameContainsValidCharacters = new Regex(@"^[a-zA-Z \s\'\-]*$", RegexOptions.Compiled);
//        private static readonly Regex Regex_DoesNameContainsValidCharactersWithTilde = new Regex(@"^[a-zA-Z ~\s\'\-]*$", RegexOptions.Compiled);

//        private static readonly Regex Regex_IsValidEmailAddress = new Regex(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$", RegexOptions.Compiled);
//        private static readonly Regex Regex_IsValidOrgId = new Regex(@"^[a-zA-Z0-9]+$", RegexOptions.Compiled);
//        private static readonly Regex Regex_IsValidDate = new Regex(@"\d{4}-\d{2}-\d{2}", RegexOptions.Compiled);

//        // Disallow characters - currently used to stop concurrent 'special' characters. Concurrent mixtures are allowed - e.g.: Goodall-'Taia
//        private static readonly string[] DisallowedCharacterSets = new string[] { "--", "  ", "''" }; //, "' " 

//        private static readonly string[] DisallowedRegularExpressions = new string[]
//        {
//            "^['-]+$", // When (allowed special characters) appear only by themselves
//            "\\s+['-]+\\s+", // When (allowed special characters) appear without letters (in the middle)
//            "^['-]+\\s+", // When (allowed special characters) appear without letters (at the start)
//            "\\s+['-]+$", // When (allowed special characters) appear without letters (at the end)
//            "\r\n",
//            "\r",
//            "\n",
//            "\t"
//        };





//    }
//}