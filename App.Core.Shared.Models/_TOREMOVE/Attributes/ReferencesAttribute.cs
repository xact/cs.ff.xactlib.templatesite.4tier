﻿//namespace XAct
//{
//    using System;

//    /// <summary>
//    /// Attribute that allows annotating code
//    /// as to the Functional, NonFunction, Technical
//    /// referenced.
//    /// </summary>
//    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
//    public class SpecReferenceAttribute : Attribute
//    {
//        public string Reference { get; set; }
//        public string Note { get; set; }

//        public SpecReferenceAttribute(string references,string note=null)
//        {
//            Reference = references;
//            Note = note;
//        }
//    }
//}
