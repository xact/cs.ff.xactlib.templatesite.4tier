﻿//Put Extensions in the lowest (ie, the App) Namespace
//So that they are available without having to add
//a using namespace statement. More intutitive.

using System;
using App.Core.Domain.Models;
using App.Core.Infrastructure.Models;

namespace App.Core.Domain.Extensions
{
    /// <summary>
    /// Provides a set of static methods for extending objects of type Student.
    /// </summary>
    public static class SessionExtensions
    {
        /// <summary>
        /// Creates an identifier to identify a session in the cache.
        /// <para>
        /// Format is '_SessionToken:{guid}'
        /// </para>
        /// </summary>
        /// <param name="session">The session.</param>
        /// <returns></returns>
        public static string CreateCacheIdentifier(this SessionBase session)
        {
            if (session == null) throw new ArgumentNullException("session");
            if (session.Id == null) throw new ArgumentNullException();

            return session.Id.CreateCacheIdentitfier();
        }

        /// <summary>
        /// From the given SessionToken Guid,
        /// creates the string cache identitfier/key.
        /// <para>
        /// Format is '_SessionToken:{guid}'
        /// </para>
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <returns></returns>
        public static string CreateCacheIdentitfier(this Guid sessionToken)
        {
            return Constants.Caching.CacheKeys.SessionTokenPrefix + sessionToken;
        }
    }
}
