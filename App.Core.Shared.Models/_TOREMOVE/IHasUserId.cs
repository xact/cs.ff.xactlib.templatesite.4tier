﻿//using System.Linq;
//using System;

//namespace App.Core.Domain.Entities
//{
//    /// <summary>
//    /// <para>
//    /// Used by 
//    /// <c>POXLoginRequest</c>,
//    /// <c>POXMergeRequest</c>,
//    /// etc.
//    /// </para>
//    /// <para>
//    /// The use of Legacy in the contract name
//    /// is to clearly indicate that a system
//    /// that passes around 50% of the information
//    /// required to authenticate a user is bad
//    /// practice, and only still within this app for 
//    /// legacy reasons that can't be dropped. Yet.
//    /// </para>
//    /// </summary>
//    public interface IHasLegacyUserName 
//    {
//        /// <summary>
//        /// Gets the (Legacy) UserName (not the newer Ids
//        /// that don't disclose the user's login name).
//        /// <para>
//        /// The use of Legacy in the contract name
//        /// is to clearly indicate that a system
//        /// that passes around 50% of the information
//        /// required to authenticate a user is bad
//        /// practice, and only within this app for 
//        /// legacy reasons that can't be dropped. Yet.
//        /// </para>
//        /// </summary>
//        string UserName { get; set; }
//    }
//}