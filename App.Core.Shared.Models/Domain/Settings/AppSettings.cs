﻿using XAct.Services;

namespace App.Core.Infrastructure
{
	using System;
	using System.Runtime.Serialization;
	using XAct;

    [DataContract(Name = "AppSettings")]
    public class AppSettings : AppSettingsBase
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AppSettings" /> class.
		/// </summary>
		public AppSettings()
			: base()
		{
		}



		/// <summary>
		/// Gets the session time out in minutes.
		/// <value>
		/// The default value is 20 (minutes).
		/// </value>
		/// </summary>
		/// <value>
		/// The session time out.
		/// </value>
		public TimeSpan SessionTimeOut
		{
			get
			{
				int minTime = 1;
				int maxTime = 60;
				int defaultTime = 20;
				
                //Would like to cache for performance reasons, but if I do, 
                //it won't pick updates from parameters page.
                //if (_sessionTimeOut.HasValue) return _sessionTimeOut.Value;
				int result;
				
                //base.TryGetSettingValue("Security/Session/TimeOutInMinutes", out result, defaultTime);

                

                //AMAZING. Can't imagine how it was possible that a developer didn't see that this had to come from
                //a different path...

                base.TryGetSettingValue("Parameters/Parameter/session_timeout", out result, defaultTime);

                result = Math.Min(Math.Max(result, minTime), maxTime);
				_sessionTimeOut = TimeSpan.FromMinutes(result);

                return _sessionTimeOut.Value;
			}
			set { _sessionTimeOut = value; }
		}

		private TimeSpan? _sessionTimeOut = null;

		public TimeSpan SessionMaxLength
		{
			get
			{
				int minTime = 60;
				int maxTime = 480;
				int defaultTime = 480; //from 8 * 60;
				if (_sessionMaxLength.HasValue) return _sessionMaxLength.Value;
				int result;
				base.TryGetSettingValue("Security/Session/MaxLengthInMinutes", out result, defaultTime);
				result = Math.Min(Math.Max(result, minTime), maxTime);

				_sessionMaxLength = TimeSpan.FromMinutes(result);
				return _sessionMaxLength.Value;
			}
			set { _sessionMaxLength = value; }
		}

		private TimeSpan? _sessionMaxLength = null;








		public int Parameter_Session_Timeout
		{
			get
			{
				return this.SessionTimeOut.Minutes;
			}
		}





	}
}
