namespace App.Core.Domain.Security
{

    public interface IAppIdentityOrganisationSummary
    {
        
        /// <summary>
        /// Helper to extract Typed values from the internal
        /// Settings dictionary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        T GetValue<T>(string key, T defaultValue = default(T));


        /// <summary>
        /// Sets the typed value 
        /// into the Settings dictionary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        void SetValue<T>(string key, T value);
    }
}