﻿namespace App.Core.Domain.Security
{
    using System.Linq;

    public class UserDetails
	{
		// -- Basic claims:

		public string UserId { get; set; }

		public string Email { get; set; }
		

		// -- Additional claims:

		public string FirstName { get; set; }

		public string LastName { get; set; }
		
		public string OrgId { get; set; }

		public string OrgName { get; set; }


		public string FullDisplayName
		{
			get
			{
				// Construct the space-separated name 
				// (Note: some accounts are missing 1 of the 2. (Particularly test accounts))
// ReSharper disable RedundantExplicitArrayCreation
				string result = string.Join(" ", new string[]
// ReSharper restore RedundantExplicitArrayCreation
					{
						FirstName,
						LastName
					}.Where(n => !string.IsNullOrEmpty(n)));

				if (!string.IsNullOrEmpty(result))
				{
					return result;
				}

				// If the name isn't populated, fallback to the user ID
				if (!string.IsNullOrEmpty(UserId))
				{
					return UserId;
				}

				// Fallback
				return "(Unknown)";
			}
		}
	}
}
