﻿namespace App.Core.Domain.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using XAct;


    [Serializable]
    public sealed class AppIdentity : GenericIdentity, IAppIdentity, IHasInnerItem<IIdentity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppIdentity" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public AppIdentity(string name)
            : base(name)
        {
        }        

        public AppIdentity(string name, IEnumerable<Claim> claims)
            : this(name)
        {
            AddClaims(claims);
        }

        /// <summary>
        /// Gets or sets the session token for the Session to which this AppIdentity is associated.
        /// </summary>
        /// <value>
        /// The session token.
        /// </value>
        public Guid? SessionToken

        {
            get
            {
                if (_sessionToken == null)
                {
                    var claim = Claims.FirstOrDefault(x => x.Type == "SessionToken");

                    var value = (claim != null) ? claim.Value : null;

                    if (value != null)
                    {
                        _sessionToken = Guid.Parse(value);

                    }
                }
                return _sessionToken;
            }
            set
            {
                this.AddClaim(new Claim("SessionToken", value.ToString()));
                _sessionToken = value;
            }
        }
        

        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        public string FullName
        {
            get
            {
                var claim = Claims.FirstOrDefault(c => c.Type == Constants.Security.ClaimTypes.FullName);

                return (claim != null) ? claim.Value : null;

            }
        }

		/// <summary>
		/// Gets the UserId (the ESAA-supplied login name)
		/// </summary>
	    public string UserId
	    {
		    get
		    {
		        var claim = Claims.FirstOrDefault(c => c.Type == Constants.Security.ClaimTypes.UserId);

		        return (claim != null) ? claim.Value : null;
		    }
	    }

		/// <summary>
		/// The ID of the organisation the user is linked to
		/// </summary>
	    public string ProviderCode
	    {
            get
            {
                var claim = Claims.FirstOrDefault(c => c.Type == Constants.Security.ClaimTypes.OrgId);
                return (claim != null) ? claim.Value : null;

                
            }
	    }


        public int ProviderFK { get { return this.OrganisationSummary.GetValue<int>("OrgFK"); } set { this.OrganisationSummary.SetValue("OrgFK",value); } }
        public string ProviderName { get { return this.OrganisationSummary.GetValue<string>("Name"); } set { this.OrganisationSummary.SetValue("Name",value); } }



        public AppIdentityOrganisationSummary OrganisationSummary
        {
            get { return _organisationSettings ?? (_organisationSettings = new AppIdentityOrganisationSummary()); }
            set { _organisationSettings = value; }
        }
        private AppIdentityOrganisationSummary _organisationSettings;


        /// <summary>
        /// Gets the privileges.
        /// </summary>
        /// <value>
        /// The privileges.
        /// </value>
        public IEnumerable<string> Privileges
        {
            get
            {
                return FindAll(c => c.Type == Constants.Security.ClaimTypes.Privilege).Select(c => c.Value);
            }
        }

	    public bool HasPriviledge(string privilege)
	    {
		    return Privileges.Contains(privilege);
	    }

		/// <summary>
        /// Serializes the identity to base64 format.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public string SerializeToBase64()
        {
            SerializationMethod serializationMethod = SerializationMethod.Base64Binary;
            return typeof(AppIdentity)
                .Serialize(this, ref serializationMethod);
        }

        /// <summary>
        /// Deserializes the App session from base64 string
        /// </summary>
        /// <param name="identity">The identity.</param>
        /// <returns>
        /// App Session Entity
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public static AppIdentity DeserializeFromBase64(string identity)
        {            
            return (AppIdentity) 
                typeof (AppIdentity)
                .DeSerialize(identity, SerializationMethod.Base64Binary);
        }

        /// <summary>
        /// Attaches the identity to the calling thread.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public IPrincipal CreatePrincipal()
        {
            GenericPrincipal principal = new GenericPrincipal(this, Privileges.ToArray());
            return principal;

            //NEVER NEVER NEVER!
            //REALLY...NEVER !Thread.CurrentPrincipal = principal;
        }
          

        /// <summary>
        /// Validates the serialized identity.
        /// </summary>
        /// <param name="identity">The identity.</param>
        /// <returns></returns>
        public static bool ValidateSerializedIdentity(string identity)
        {
            return DeserializeFromBase64(identity) != null;
        }

        /// <summary>
        /// Tries the deserialize the identity from base64.
        /// </summary>
        /// <param name="identityBase64">The identity base64.</param>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        public static bool TryDeserializeFromBase64(string identityBase64, out IAppIdentity result)
        {
            result = DeserializeFromBase64(identityBase64);
            return result != null;
        }

        public void SetInnerObject(object o)
        {
            _innerItem = o as IIdentity;
        }

        public TItem GetInnerItem<TItem>()
        {
            return (TItem)_innerItem;
        }
        IIdentity _innerItem;


        private Guid? _sessionToken;
    }
}