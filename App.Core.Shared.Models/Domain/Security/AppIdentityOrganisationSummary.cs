﻿namespace App.Core.Domain.Security
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using XAct;

    [Serializable]
    public class AppIdentityOrganisationSummary : IAppIdentityOrganisationSummary
    {
        /// <summary>
        /// Gets or sets the settings.
        /// </summary>
        /// <value>
        /// The settings.
        /// </value>
        [DataMember]
        public Dictionary<string, object> Settings { get { return _settings ?? (_settings = new Dictionary<string, object>()); } }
        private Dictionary<string, object> _settings;


        /// <summary>
        /// Helper to extract Typed values from the internal
        /// Settings dictionary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public T GetValue<T>(string key, T defaultValue = default(T))
        {
            object tmp;
            if (Settings.TryGetValue(key, out tmp))
            {
                return  tmp.ConvertTo<T>();
            }
            return defaultValue;
        }

        /// <summary>
        /// Sets the typed value
        /// into the Settings dictionary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void SetValue<T>(string key, T value)
        {
            Settings[key] = value;
        }

    }
}