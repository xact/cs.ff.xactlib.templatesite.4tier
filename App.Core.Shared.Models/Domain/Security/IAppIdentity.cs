namespace App.Core.Domain.Security
{
	using System;
	using System.Collections.Generic;
    using System.Security.Principal;


    public interface IAppIdentity : IIdentity
	{
        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        string FullName { get; }

        /// <summary>
        /// Gets the privileges.
        /// </summary>
        /// <value>
        /// The privileges.
        /// </value>
        IEnumerable<string> Privileges { get; }

	    /// <summary>
	    /// Gets the UserId (the ESAA-supplied login name)
	    /// </summary>
	    string UserId { get; }

	    /// <summary>
	    /// The ID of the organisation the user is linked to
	    /// </summary>
	    string ProviderCode { get; }

	    int ProviderFK { get; set; }

	    string ProviderName { get; set; }

        AppIdentityOrganisationSummary OrganisationSummary { get; set; }

	    /// <summary>
        /// Serializes the identity to base64 format.
        /// </summary>
        /// <returns>Serialized Identity in bas64 format.</returns>
	    string SerializeToBase64();

        /// <summary>
        /// Attaches the identity to the calling thread.
        /// </summary>
        IPrincipal CreatePrincipal();



	    /// <summary>
	    /// Gets or sets the session token for the Session to which this AppIdentity is associated.
	    /// </summary>
	    /// <value>
	    /// The session token.
	    /// </value>
	    Guid? SessionToken { get; set; }
	}
}