﻿namespace App.Core.Domain.Messages
{
    using System.Runtime.Serialization;
    using App.Core.Domain.Models;
    using App.Core.Domain.Security;

    [DataContract]
    public class SessionResponse
    {
        [DataMember]
        public Session Session { get; set; }

        [DataMember]
        public AppIdentityOrganisationSummary OrganisationSummary { get; set; }

    }
}
