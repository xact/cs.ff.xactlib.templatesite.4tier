﻿using System;

namespace App.Core.Domain.Messages
{
    public class InvalidMessageException<T> : Exception
        where T : class
    {
        public T MessageData { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidMessageException{T}" /> class.
        /// </summary>
        /// <param name="messageData">The message data.</param>
        public InvalidMessageException(T messageData)
        {
            MessageData = messageData;
        }
    }
}
