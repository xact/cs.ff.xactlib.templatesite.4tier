namespace App.Core.Domain.Messages
{
    using System.ComponentModel.Design;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// Used by <see cref="IResourceService" />
    /// to indicate whether resources need to be transformed
    /// prior to rendering.
    /// </remarks>
	[DataContract(Name="ResourceTransformation")]
	public enum ResourceTransformation
	{
        /// <summary>
        /// Undefined.
        /// <para>
        /// An Error State.
        /// </para>
        /// <para>
        /// Value=0
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        /// No transformation applied -- just return raw Resource string.
        /// <para>
        /// Value=1
        /// </para>
        /// </summary>
        [EnumMember]
        None = 1,

        /// <summary>
        /// Return the Resource, transformed and returned as Markdown.
        /// <para>
        /// Value=0
        /// </para>
        /// </summary>
        [EnumMember]
        Markdown = 2,
	}
}