﻿namespace App.Core.Domain.Messages
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// <para>
    /// A cacheable message abstract Message projection of 
    /// an app's Reference data.
    /// </para>
    /// <para>
    /// Advantages are:
    /// * by making it an abstract Message prior to caching, 
    /// it removes the ambiguity of trying to do something weird such
    /// as get from cache an Entity, update it, reattach it to a db context
    /// and persist it.
    /// </para>
    /// An in-app specialization. 
    /// </summary>
    [DataContract]
    public class ReferenceDataMessage :  XAct.Messages.ReferenceDataBase<Guid>
    {



    }
}
