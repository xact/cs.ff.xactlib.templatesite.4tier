﻿namespace App.Core.Domain.Messages
{
    using System.Runtime.Serialization;

    [DataContract]
    public class ReferenceDataViewModel : XAct.Messages.ReferenceDataIOModel
    {

        /// <summary>
        /// HACK: Underlying object is now called Text. Whereas original, 
        /// on which UI was built, the displayed string was called Value.
        /// </summary>
        /// <internal>
        /// Note: must be marked virtual or fails within <c>MessageCodeSanityCheck</c>
        /// when seeking to resolve for <c>MessageCodes.General_InvalidXMLCommand_769</c>
        /// </internal>
        [DataMember]
        public virtual string Value
        {
            get
            {
                return base.Text;
            }
        }

        
    }
}
