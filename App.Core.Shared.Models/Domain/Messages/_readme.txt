﻿# About #

In addition to Domain Entities, an app commonly has other objects.
The more well known ones are the Presentation layer Input/Output Models (in MVC, known as View Model and Response/PostBack Model).

But you also have cross tier Request/Response Objects that are not enough for Rendering purposes (and it's suspect to have I/O 
objects cross so many tiers of technology).

They are not Infrastructure objects (they know about Domain properties they are carrying)
Nor Application objects as they will be used by the RepositoryServices.
Therefore, by exclusion, they are Domain objects, that are non persisted. ie, just Domain Messages.
 