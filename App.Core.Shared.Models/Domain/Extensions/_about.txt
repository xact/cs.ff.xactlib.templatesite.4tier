﻿Extension methods are poorly understood by intermediate messages.

They are nothing more than an elegant syntactic way of adding 
(generally layer-specific) Behaviour to Message objects 
passed between layers and tiers.

Use them abundently, leaving the actual messages be nothing 
more than Properties -- as that is the only thing that can be 
mapped across Tiers (it makes no sense either to serialize 
an objects Methods, so WCF/JSON don't).