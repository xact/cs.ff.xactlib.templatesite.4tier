﻿namespace App.Core.Domain.Models
{
    using System.Runtime.Serialization;
    using App.Core.Domain.Entities;
    using XAct;

    [DataContract(Name = "User")]
    public class User 
    {
        //[DataMember]
        //public int Id { get; set; }

        /// <summary>
        /// Is PK for now.
        ///   <para>
        /// IMPORTANT: It is fundamentally poor
        /// design practice to have the app
        /// know 50% of information that the user
        /// uses to login.
        ///   </para>
        ///   <para>
        /// This property should be fazed out
        /// when ESAA2 comes on line, and returns
        /// only a Single-SP-Specific UserId.
        ///   </para>
        /// </summary>
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string UserFriendlyName { get; set; }
    }
}
