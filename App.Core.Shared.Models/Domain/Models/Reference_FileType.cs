
namespace App.Core.Domain.Models
{
    using System.Runtime.Serialization;
    using App.Core.Domain.Entities;

    [DataContract]
// ReSharper disable InconsistentNaming
    //Naming pattern is client (MINISTRY) Directive.
    public class Reference_FileType : ReferenceDataBase<FileType>
// ReSharper restore InconsistentNaming
    {

    }
}
