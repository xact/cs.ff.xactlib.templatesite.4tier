﻿IMPORTANT:
WCF is an *opt-in* serialization mechanism. 
All properties you want serialized across the wire
have to be marked DataMember, and the class with DataContract
before they make it across the tier correctly.
Domain Enum Type have to be marked with EnumMember.
If you forget to add that, you'll be chasing annoying bugs.