﻿using System.Collections.Generic;
using System.Linq;
using App.Core.Domain.Models;
using XAct;

namespace XAct
{
    public static class HasIdListExtensions
    {
        public static IEnumerable<TId> GetIds<TEntity, TId>(this IEnumerable<TEntity> entities)  
            where TEntity : IHasId<TId>
        {
            return entities.Select(s => s.Id);
        }

	    public static bool AllValuesUnique<T>(this IEnumerable<T> ids)
	    {
	        var tmp = ids.ToArray();
            return tmp.Distinct().Count() == tmp.Count();
        }
    }
}
