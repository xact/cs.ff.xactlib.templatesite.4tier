﻿using System.Runtime.Serialization;

namespace XAct
{
    [DataContract]
    public enum CaseSensitivity
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Sensitive,
        [EnumMember]
        Insensitive
    }
}