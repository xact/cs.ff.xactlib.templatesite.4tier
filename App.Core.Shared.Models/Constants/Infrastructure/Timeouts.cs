﻿using System;

namespace App.Core.Constants.Communication
{
    public class Timeouts
    {
        public static TimeSpan ServiceChannelFactoryClosing = TimeSpan.FromSeconds(3);
        public static TimeSpan WpfServiceClosing = TimeSpan.FromSeconds(3);
    }

}
