﻿

namespace App.Core.Constants.Infrastructure.Diagnostics
{
    public static class PerformanceCounters
    {
        /// <summary>
        /// The prefix to put before each PerformanceCounter Category Name.
        /// </summary>
        public const string CategoryNamePrefix = "App";

        /// <summary>
        /// 
        /// </summary>
        public const string SubOperations = 
            "RepositoryService.Queries,RepositoryService.Queries.Immediate,RepositoryService.Queries.Deferred,RepositoryService.Queries.Commit.Entities.Changed,RepositoryService.Queries.Commit.Entities.Added,RepositoryService.Queries.Commit.Entities.Updated,RepositoryService.Queries.Commit.Entities.Deleted";

    }
}
