﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Constants.Infrastructure.Tracing
{
    public static class EventLog
    {
        /// <summary>
        /// Name of the AppSetting that defines the EventLog Name.
        /// <para>
        /// See: <c>EnsureAccessToEventLogInitializationStep</c>
        /// </para>
        /// </summary>
        public static string AppSettingEventLogName = "EventLogSourceNames";
    }
}
