﻿
namespace App.Core.Constants.UI
{


        public static class Views
        {

            /*
             * View Elements are of the following:
             * * Display (no Action)
             * * Input, 
             * * Nav, 
             * * Action
             * SubCategorised as: 
             * * Text (not Label, as Text is more inclusive: Headers don't have Label's)
             * * Tip (not Tooltip as not always a Tool, not Hover, which is depracated behaviour in Windows8)
             * * Image (not Icon, as Image can Icon, PNG, or SVG)
             * Examples are:
             * * A View with an Form containing one Field and one button at bottom, will be:
             * * ViewBackgroundDisplayImage; 
             * * ViewTitleDisplayImage = some jpg to set the form tone.
             * * ViewTitleDisplayText = "...";
             * * ViewTitleDisplayImage = some jpg to set the form tone.
             * * FirstNameLabel goes to label
             * * N/A: FirsNameLabelTip would be a red herring, as its the same tip as the Control the Label is labelling.
             * * FirstNameTip goes to Ghost, Hover, or CSS popup to right.
             * * FirstNameHelp goes to margin to right or other off-form area.
             * * FirstNameIcon would be the Icon in the left margin of input control
             * * SubmitActionText would be submit button's label
             * * SubmitActionTip ...etc.
             * * SubmitActionImage ....some icon on the button.
             * 
             */

            public static class Header
            {
                // ReSharper disable MemberHidesStaticFromOuterClass
                public static string ResourceKey = "HeaderView";
                // ReSharper restore MemberHidesStaticFromOuterClass

                public static string ViewTitleDisplayText = "[Unused]";
                public static string ViewTagLineDisplayText = "[Unused]";

                //TODO: Unclear naming strategy. Improve:
                public static string Logo = "Logo";
                public static string LogoTitle = "LogoTitle";
                public static string LogoText = "LogoText";

            }




            public static class Navigation
            {
                public static string ResourceKey = "NavigationView";


                //It's the Text of a Label of a Secondary LinkAction
                //Can live with dropping the word action, but rest stands.
                public static string HomeNavText = "Home";



            }

            public static class Home
            {
                // ReSharper disable MemberHidesStaticFromOuterClass
                public static string ResourceKey = "HomeView";
                // ReSharper restore MemberHidesStaticFromOuterClass

                public static string ViewTitleDisplayText = "ViewTitle";

                public static string ViewSubtitleDisplayText = "ViewSubtitle";

                public static string ViewTagLineDisplayText = "ViewTagLine";

                //TODO: Unclear naming strategy. Improve:
                //Notice that it's called Action -- not button. Different AppHosts render Actions differently (not always buttons).

                public static string RequestACopyNavText = "RequestACopyNavText";

                public static string AuthorisedInfoMatchingProgrammeNavText =
                    "AuthorisedInfoMatchingProgrammeNavText";

            }

            public static class Welcome
            {
                // ReSharper disable MemberHidesStaticFromOuterClass
                public static string ResourceKey = "WelcomeView";
                // ReSharper restore MemberHidesStaticFromOuterClass

                public static string ViewTitleDisplayText = "ViewTitle";
                public static string ViewTagLineDisplayText = "ViewTagLine";

                public static string MessageOfTheDayTitleDisplayText = "LabelTextMessageOfTheDayTitle";
                public static string MessageOfThedayBodyDisplayText = "LabelTextMessageOfTheDayBody";

                public static string LabelTextNavActionWarning1 = "LabelTextNavActionWarning1";
                public static string LabelTextNavActionWarning2 = "LabelTextNavActionWarning2";
                public static string ChangePasswordNavText = "LabelTextNavActionChangePassword";
                public static string ChangeInstitutionNavText = "LabelTextNavActionChangeInstitution";

                public static string ChangeChallengePhraseNavDisplayText = "LabelTextNavActionChangeChallengePhase";

                public static string LabelTextNavActionChangeMyDetails = "LabelTextNavActionChangeMyDetails";




            }


        }

}