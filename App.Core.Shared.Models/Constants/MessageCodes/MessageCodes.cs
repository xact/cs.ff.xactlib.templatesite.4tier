﻿using XAct.Messages;

namespace App.Core.Constants
{
    public partial class MessageCodes
    {

        [MessageCodeMetadata("SUCCESS", "SUCCESS", 0)]
        public static MessageCode SUCCESS = new MessageCode(0, (XAct.Severity)0);


        [MessageCodeMetadata("UnknownError", "UnknownError", 0)]
        public static MessageCode UnknownError = new MessageCode(1, (XAct.Severity)1);


        [MessageCodeMetadata("AuthorisationDenied", "AuthorisationDenied", 0)]
        public static MessageCode AuthorisationDenied = new MessageCode(101, (XAct.Severity)101);


        [MessageCodeMetadata("InvalidUserNameOrPasswod", "InvalidUserNameOrPasswod", 0)]
        public static MessageCode InvalidUserNameOrPasswod = new MessageCode(102, (XAct.Severity)102);


        [MessageCodeMetadata("InvalidOrganisationForUser", "InvalidOrganisationForUser", 0)]
        public static MessageCode InvalidOrganisationForUser = new MessageCode(103, (XAct.Severity)103);


        [MessageCodeMetadata("InterfaceNotEnabledForUser", "InterfaceNotEnabledForUser", 0)]
        public static MessageCode InterfaceNotEnabledForUser = new MessageCode(104, (XAct.Severity)104);


        [MessageCodeMetadata("SessionTimout", "SessionTimout", 0)]
        public static MessageCode SessionTimout = new MessageCode(105, (XAct.Severity)105);


        [MessageCodeMetadata("SessionTooLong", "SessionTooLong", 0)]
        public static MessageCode SessionTooLong = new MessageCode(106, (XAct.Severity)106);


        [MessageCodeMetadata("SessionLoggedOut", "SessionLoggedOut", 0)]
        public static MessageCode SessionLoggedOut = new MessageCode(107, (XAct.Severity)107);




    }//~class
}//~ns
