﻿

namespace App.Core.Constants.ReferenceData
{


    /// <summary>
    /// Constants used to name the ReferenceData
    /// <para>
    /// Used by <c>CultureSpecificImmutableReferenceDataService</c>
    /// to retrieve the right Reference data.
    /// </para>
    /// </summary>
    public static partial class ImmutableReferenceData
    {
        public const string ExampleCategory = "ExampleCategory";
        public const string ExampleType = "ExampleType";

        public const string Gender = "Gender";
        public const string InterfaceType = "InterfaceType";



    }
}

