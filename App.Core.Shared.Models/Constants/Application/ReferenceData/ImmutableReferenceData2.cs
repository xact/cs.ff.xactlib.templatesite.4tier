﻿namespace App.Core.Constants.ReferenceData
{
    /// <summary>
    /// Constants used to find things in Resource Table.
    /// </summary>
    public static partial class ImmutableReferenceData2
    {
        public const string MessageCode = "MessageCode";
        public const string APIResponseCode = "APIResponseCode";
    }
}