// ReSharper disable CheckNamespace
namespace App.Core.Constants.Security
// ReSharper restore CheckNamespace
{
        public static class ClaimTypes
        {
            public const string FullName = "FullName";
            public const string UserId = "UserId";
            public const string OrgId = "OrgId";
            public const string OrgName = "OrgName";
            public const string Privilege = "Privilege";
        }
}