﻿using App.Core.Domain.Entities;
using XAct.Messages;

namespace App.Core.Constants.Application.API
{
	public interface ILegacyMessageMappingService
	{
		/// <summary>
		/// Converts the specified source message into the target for the given interface type. If no mapping configuration for the given
		/// source message is available, the source message will be returned.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="interfaceType">Type of the interface.</param>		
		Message MapToLegacy(Message source, InterfaceType interfaceType);

		/// <summary>
		/// Converts ALL messages within the given response object into the target messages for the given interface type. If no mapping configuration for the given
		/// source message is available, the source message will be returned.
		/// </summary>
		/// <typeparam name="TData">The type of the data.</typeparam>
		/// <param name="response">The response.</param>
		/// <param name="interfaceType">Type of the interface.</param>
		/// <returns></returns>
		IResponse<TData> MapToLegacy<TData>(IResponse<TData> response, InterfaceType interfaceType);
	}
}