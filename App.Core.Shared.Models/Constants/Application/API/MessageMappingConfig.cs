using XAct.Messages;

namespace App.Core.Constants.Application.API
{
	/// <summary>
	/// Configuration options for message mapping
	/// </summary>
	public class MessageMappingConfig
	{
		public MessageMappingConfig(MessageCode targetMessageCode)
		{
			TargetMessageCode = targetMessageCode;
		}

		public MessageCode TargetMessageCode { get; set; }

		// As our functionality requirements of the message mapping increase,
		// you can add properties to control it correctly here:

		/// <summary>
		/// Replaces the arguments property
		/// (E.g.: If the target message requires 1 argument, and the source has none)
		/// </summary>
		public string[] ReplacementArguments { get; set; }

		/// <summary>
		/// If set to true, this searches if the source message is contained in the inner message list. 
		/// If found, it will return this the target message as the new root message.
		/// (E.g.: Merge returns #820 with #338 inside, we can return #338 as the new root message)
		/// </summary>
		public bool SearchInnerMessagesAndReplace { get; set; }

	}
}