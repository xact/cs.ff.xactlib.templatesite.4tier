﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using App.Core.Domain.Entities;
using App.Core.Domain.Models;
using XAct.Messages;
using XAct.Services;

namespace App.Core.Constants.Application.API
{
    using XAct;

    [DefaultBindingImplementation(typeof(ILegacyMessageMappingService),BindingLifetimeType.Undefined, Priority.Low)]
	public class LegacyMessageMappingService : ILegacyMessageMappingService
	{
		private readonly IDictionary<string, MessageMappingConfig> _mappings = new SortedDictionary<string, MessageMappingConfig>();

		/// <summary>
		/// Initializes a new instance of the <see cref="LegacyMessageMappingService"/> class.
		/// </summary>
		public LegacyMessageMappingService()
		{
			InitializeMappings();
		}

		/// <summary>
		/// Initializes the mappings.
		/// 
		/// EXAMPLE 1:
		/// 
		/// To change mapping of a message code 'SourceMessage' to
		///
		/// Message_Soap for SOAP
		/// Message_Xml for XML
		/// Message_Batch for BATCH
		/// 		
		/// configure the mapping as follows:
		/// 
		///	AddMapping
		///		(
		///			source: MessageCodes.SourceMessage,					
		///			soap: MessageCodes.Message_Soap,
		///			xml: MessageCodes.Message_Xml,
		///			batch: MessageCodes.Message_Batch
		///		);
		/// 
		/// EXAMPLE 2:
		/// 
		/// if the mapping of a particular message to all legacy interfaces is the same use the 'allLegacy' argument as follows:
		/// 
		/// AddMapping
		///		(
		///			source: MessageCodes.SourceMessage,
		///			allLegacy : MessageCodes.TargetMessageForAllInterfaces
		///		);				
		/// 		
		/// </summary>
		private void InitializeMappings()
		{

			/* Copy, paste and update to create a new mapping,			 
			 * IMPORANT : if there is a mapping for a particular source message already, just extend it.
			 * 
			 
			AddMapping
				(
					source: MessageCodes.Validation_Date_001,
					soap: null,
					xml: null,
					batch: null
				);
			 
			*/

        }

		/// <summary>
		/// Converts the specified source message into the target for the given interface type. If no mapping configuration for the given
		/// source message is available, the source message will be returned.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="interfaceType">Type of the interface.</param>
		/// <returns></returns>
		public Message MapToLegacy(Message source, InterfaceType interfaceType)
		{
			MessageMappingConfig target;

			// Also recurse inner messages (which get passed back as the new root message)
			if (source.InnerMessages != null
				&& source.InnerMessages.Any())
			{
				foreach (var innerMessage in source.InnerMessages)
				{
					if (_mappings.TryGetValue(CreateKey(interfaceType, innerMessage.MessageCode), out target)
						&& target.SearchInnerMessagesAndReplace)
					{
						return MapToLegacy(innerMessage, interfaceType);
					}
				}
			}

			// Map known codes
			if (_mappings.TryGetValue(CreateKey(interfaceType, source.MessageCode), out target))
			{
				// Set arguments
				var args = (target.ReplacementArguments != null
							&& target.ReplacementArguments.Any())
					? target.ReplacementArguments //add arguments of the target message if they are overriden in the mappings
					: source.Arguments; //add arguments of the source if they are not overriden in the mappings
				var returnMessage = new Message(target.TargetMessageCode, args);

				// Copy inner messages
				if (source.InnerMessages.Any())
				{
					returnMessage.InnerMessages.AddRange(source.InnerMessages);
				}

                return returnMessage;
			}

			

			// Else there is no mapping, let the source pass through
			return source;
		}

		public IResponse<TData> MapToLegacy<TData>(IResponse<TData> response, InterfaceType interfaceType)
		{
			for (int i = 0; i < response.Messages.Count; i++)
			{
				response.Messages[i] = MapToLegacy(response.Messages[i], interfaceType);
			}
			return response;
		}

		private void AddMapping(MessageCode source, MessageMappingConfig soap = null, MessageMappingConfig xml = null, MessageMappingConfig batch = null, MessageMappingConfig allLegacy = null)
		{
		    if (allLegacy != null)
		    {
                _mappings[CreateKey(InterfaceType.SOAP, source)] = allLegacy;
                _mappings[CreateKey(InterfaceType.XML, source)] = allLegacy;
                _mappings[CreateKey(InterfaceType.BATCH, source)] = allLegacy;
            }

			if (soap != null)
			{
                _mappings[CreateKey(InterfaceType.SOAP, source)] = soap;
            }

			if (xml != null)
			{
                _mappings[CreateKey(InterfaceType.XML, source)] = xml;
            }

			if (batch != null)
			{
                _mappings[CreateKey(InterfaceType.BATCH, source)] = batch;
			}
        }

	   private static string CreateKey(InterfaceType interfaceType, MessageCode messageCode)
	   {
		   var result = interfaceType.ToString() + "_" + messageCode.Id;
		   return result;
	   }
	

	}
}