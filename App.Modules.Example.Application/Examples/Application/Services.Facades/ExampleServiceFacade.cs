﻿
// ReSharper disable CheckNamespace

namespace App.Modules.Example.Examples.Application.Services.Facades
// ReSharper restore CheckNamespace
{
    using System;
    using App.Core.Application.Services.Facades.Implementations;
    using App.Modules.Example.Domain.Entities;
    using App.Modules.Example.Domain.Messages;
    using App.Modules.Example.Domain.Models;
    using App.Modules.Example.Example.ApplicationFacade.Services;
    using App.Modules.Example.Examples.Application.Services.Implementations;
    using XAct;
    using XAct.Messages;
    using XAct.Services;
    using App.Core.Infrastructure;
    using App.Core.Infrastructure.Services;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// A Service Facade has pros and cos:
    /// <![CDATA[
    /// * Pros:
    ///     * It's where to put WCF specific error trapping, keeping it out of the Application Service layer
    ///     * provides a clear operation exit point, a good place to invoke dbContext.Commit
    ///         * while handling multiple re-entries to Application Service layer methods, without needing
    ///           a stack counter.
    ///     * Operation logging
    ///   
    /// ]]>
    /// </internal>
    //[DefaultBindingImplementation(typeof(ApplicationFacade.Services.IExampleService))] //NOTE : just the 'ritcher' version of the service for resolving - then cast to the facade one in the WCF service factory (to be implemented)
    [DefaultBindingImplementation(typeof(IExampleServiceFacade),BindingLifetimeType.Undefined, Priority.Low)]
	[ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class ExampleServiceFacade : WcfSessionAuthenticatedServiceFacadeBase, IExampleServiceFacade
    {

        private readonly IExampleService _exampleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExampleService" /> class.
        /// </summary>
        public ExampleServiceFacade(IExampleService exampleService, IUnitOfWorkService unitOfWorkService)
            : base(unitOfWorkService)//, IApplicationSettingsService applicationSettingsService)
        {
            _exampleService = exampleService;
        }


        /// <summary>
        /// Adds the specified <see cref="Example"/> entity
        /// <para>
        /// If validation fails, or the record is not found, etc., the <see cref="Response"/>
        /// will instead contain error messages.
        /// </para>
        /// </summary>
        public virtual Response Add(NullableExample example)
        {
            return InvokeServiceFunction(()=>_exampleService.AddOnCommit(example));
        }

        /// <summary>
        /// Deletes the specified entity,
        /// and returns a <see cref="Response"/>
        /// </summary>
        public virtual Response Delete(Guid id)
        {
            return InvokeServiceFunction(() => _exampleService.DeleteOnCommit(id));
        }

        /// <summary>
        /// Deletes the specified entity,
        /// and returns a <see cref="Response"/>
        /// </summary>
        public virtual Response Delete(string code)
        {
            return InvokeServiceFunction(() => _exampleService.DeleteOnCommit(code));
        }

        /// <summary>
        /// Deletes the specified entity,
        /// and returns a <see cref="Response"/>
        /// </summary>
        public virtual Response Delete(NullableExample example)
        {
            return InvokeServiceFunction(() => _exampleService.DeleteOnCommit(example.Id));
        }

        /// <summary>
        ///     Gets a <see cref="Example" />
        ///     containing a single <see cref="Example" />.
        /// </summary>
        public virtual Response<Example> GetSingle(Guid id)
        {
            Response<Example> result = InvokeServiceFunction(() => _exampleService.GetSingle(id));

            return result;
        }

        /// <summary>
        ///     Gets a <see cref="Example" />
        ///     containing a single <see cref="Example" />.
        /// </summary>
        public virtual Response<Example> GetSingle(string code)
        {
            Response<Example> result = InvokeServiceFunction(() => _exampleService.GetSingle(code));

            return result;
        }


        //public PagedResponse<ExampleSummary> PagedSearch(PagedRequest<ExampleSearchTerms> request)
        //{
        //    try
        //    {
        //        PagedResponse<ExampleSummary> response = new PagedResponse<ExampleSummary>();

        //        if (!request.Data.Validate(response))
        //        {
        //            return response;
        //        }

        //        try
        //        {
        //            if (request.PageSize == 0)
        //            {
        //                request.PageSize = _applicationSettingsService.Settings.DefaultPageSize;
        //            }

        //            ExampleSummary[] records = this._exampleService.Search(request.Data.Term, request);

        //            response.Data = records;
        //            response.Success = true;
        //        }
        //        catch
        //        {
        //            //TODO: Add Response Message
        //            response.Success = false;
        //        }

        //        return response;
        //    }
        //    catch (System.Exception e)
        //    {
        //        if (System.ServiceModel.OperationContext.Current == null)
        //        {
        //            throw;
        //        }
        //        throw SoapExceptionFactory.CreateFaultException(true, e, new FaultReason(e.Message));
        //    }
        //}

        public virtual Response<ExampleSummary[]> Search(ExampleSearchTerms request)
        {

            Response<ExampleSummary[]> result = InvokeServiceFunction(() => _exampleService.Search(request));

            return result;
        }

        public virtual Response Update(NullableExample example)
        {
            Response result = InvokeServiceFunction(() => _exampleService.UpdateOnCommit(example));

            return result;
        }


    }
}