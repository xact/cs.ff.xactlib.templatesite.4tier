﻿namespace App.Modules.Example.Examples.Application.Services
{
    using System;
    using App.Core;
    using App.Modules.Example.Domain.Entities;
    using App.Modules.Example.Domain.Messages;
    using App.Modules.Example.Domain.Models;
    using XAct.Messages;

    /// <summary>
    /// Contract for an Application level service to coordinate
    /// calls between Domain Services and Interface services (caching, etc.)
    /// </summary>
    /// <internal>
    /// <para>
    /// Note that the difference between this service, at the Application
    /// level, and the AppFacade service.
    /// </para>
    /// <para>
    /// Methods at this level can be re-entrant/recursive, whereas the Application
    /// level service's methods are not (they commit at the end of them).
    /// </para>
    /// <para>
    /// Another key difference is that Authorisation is not performed at this level
    /// (one doesn't want to to pay the tax of Authorisation on every iteration of a recursive
    /// call). That's again done at the Application Facade service level.
    /// </para>
    /// </internal>
    public interface IExampleService : IHasAppService
	{
        // WARNING: DO NOT MARK METHODS ON THIS INTERFACE
        //          WITH SERVICECONTRACT AND DATACONTRACT.

        // NOTE:    Add to this contract any Methods that you don't 
        //          want exposed as Operations to the Top Tier.


        /// <summary>
        /// Gets a <see cref="Response"/> 
        /// containing a single <see cref="Example" /> with
        /// an Id property matching the given unique reference id.
        /// </summary>
        Response<Example> GetSingle(Guid id);


        /// <summary>
        /// Gets a <see cref="Response"/> 
        /// containing a single <see cref="Example" /> with
        /// a Code property matching the given unique reference code.
        /// </summary>
        Response<Example> GetSingle(string code);


        //PagedResponse<ExampleSummary> PagedSearch(PagedRequest<ExampleSearchTerms> request);

        //ExampleSummary[] Search(ExampleSearchTerms searchTerms);

        /// <summary>
        /// Upon Commit being invoked by an Application Facade layer Service operation,
        /// Adds the given record.
        /// </summary>
        Response AddOnCommit(NullableExample example);

        /// <summary>
        /// Upon Commit being invoked by an Application Facade layer Service operation,
        /// Updates the given record.
        /// </summary>
        /// <param name="example">The example.</param>
        /// <param name="acceptExampleAsComplete">if set to <c>true</c> accept the given <paramref name="example"/> as complete, attach it to the context, and persist it (rather than retrieve the existing record, and merge the new parameters over it, and then persist the existing record again).</param>
        /// <returns></returns>
        Response UpdateOnCommit(Example example, bool acceptExampleAsComplete = false);


        Response<ExampleSummary[]> Search(ExampleSearchTerms request);


        /// <summary>
        /// Upon Commit being invoked by an Application Facade layer Service operation,
        /// deletes any record that exists with the given unique datastore Id.
        /// </summary>
        Response DeleteOnCommit(Guid id);

        /// <summary>
        /// Upon Commit being invoked by an Application Facade layer Service operation,
        /// deletes any record that exists with the given unique reference Code.
        /// </summary>
        Response DeleteOnCommit(string code);


	}
}
