﻿namespace App.Modules.Example.Examples.Application.Services.Implementations
{
    using System;
    using App.Core.Infrastructure.Services;
    using App.Modules.Example.Domain.Entities;
    using App.Modules.Example.Domain.Messages;
    using App.Modules.Example.Domain.Models;
    using App.Modules.Example.Domain.Repositories;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// An implementation of the Application layer <see cref="IExampleService"/> contract.
    /// </summary>
    public class ExampleService : IExampleService
    {

        private readonly IExampleRepositoryService _exampleRepositoryService;
        private readonly IObjectMappingService _objectMappingService;


        /// <summary>
        /// Initializes a new instance of the <see cref="ExampleService"/> class.
        /// </summary>
        /// <param name="exampleRepositoryService">The example repository service.</param>
        /// <param name="objectMappingService">The object mapping service.</param>
        public ExampleService(IExampleRepositoryService exampleRepositoryService,
                              IObjectMappingService objectMappingService)
        {
            this._exampleRepositoryService = exampleRepositoryService;
            _objectMappingService = objectMappingService;
        }


        /// <summary>
        /// Upon Commit being invoked by an Application Facade layer Service operation,
        /// Adds the given record.
        /// </summary>
        public Response AddOnCommit(NullableExample example)
        {
            this._exampleRepositoryService.Add(example);

            return new Response();
        }


        /// <summary>
        /// Upon Commit being invoked by an Application Facade layer Service operation,
        /// deletes any record that exists with the given unique datastore Id.
        /// </summary>
        public Response DeleteOnCommit(Guid id)
        {
            this._exampleRepositoryService.Delete(id);

            return new Response();
        }

        /// <summary>
        /// Upon Commit being invoked by an Application Facade layer Service operation,
        /// deletes any record that exists with the given unique reference Code.
        /// </summary>
        public Response DeleteOnCommit(string code)
        {
            this._exampleRepositoryService.Delete(code);

            return new Response();
        }

        /// <summary>
        /// Gets a <see cref="Response"/> 
        /// containing a single <see cref="Example" /> with
        /// an Id matching the given unique reference id.
        /// </summary>
        public Response<Example> GetSingle(Guid id)
        {
            // Tip:
            // It's common to invoke ExampleRepositoryService directly,
            // without requiring to call a Domain Service.
            // Domain Services (other than Domain Repository Services) are only 
            // required to sync across two domain Entities,
            // while encapsulating some logic that may affect one or both the entities
            // before being persisted.

            Example result = this._exampleRepositoryService.GetSingle(id);

            return new Response<Example>(result);

        }


        /// <summary>
        /// Gets a <see cref="Response"/> 
        /// containing a single <see cref="Example" /> with
        /// a Code property matching the given unique reference code.
        /// </summary>
        public Response<Example> GetSingle(string code)
        {
            Example result = this._exampleRepositoryService.GetSingle(code);

            return new Response<Example>(result);
        }



        //public PagedResponse<ExampleSummary> PagedSearch(PagedRequest<ExampleSearchTerms> request)
        //{
        //    try
        //    {
        //        PagedResponse<ExampleSummary> response = new PagedResponse<ExampleSummary>();

        //        if (!request.Data.Validate(response))
        //        {
        //            return response;
        //        }

        //        try
        //        {
        //            if (request.PageSize == 0)
        //            {
        //                request.PageSize = _applicationSettingsService.Settings.DefaultPageSize;
        //            }

        //            ExampleSummary[] records = this._exampleRepositoryService.Search(request.Data.Term, request);

        //            response.Data = records;
        //            response.Success = true;
        //        }
        //        catch
        //        {
        //            //TODO: Add Response Message
        //            response.Success = false;
        //        }

        //        return response;
        //    }
        //    catch (System.Exception e)
        //    {
        //        if (System.ServiceModel.OperationContext.Current == null)
        //        {
        //            throw;
        //        }
        //        throw SoapExceptionFactory.CreateFaultException(true, e);
        //    }
        //}

        public Response<ExampleSummary[]> Search(ExampleSearchTerms request)
        {
            var results = this._exampleRepositoryService.Search(request.Term);

            return new Response<ExampleSummary[]>(results);
        }


        /// <summary>
        /// Upon Commit being invoked by an Application Facade layer Service operation,
        /// Updates the given record.
        /// </summary>
        /// <param name="example">The example.</param>
        /// <param name="acceptExampleAsComplete">if set to <c>true</c> [accept example as complete].</param>
        /// <returns></returns>
        public Response UpdateOnCommit(Example example, bool acceptExampleAsComplete = false)
        {
            // We explained earlier (think it was up in the Presentation layer)
            // why using a Nullable version of the Entity -- rather than the Entity itself -- is very important).
            // When dealing with a Nullable argument, it's a bit different than
            // simply accepting it as a whole record, Attaching the entity 
            // to the Context, and then trying to persist it.
            // 
            // Instead, the Application Layer has to start by pulling up the existing record
            // in order to crush its properties with the new property values.
            // Note that as all the properties of the incoming argument all nullable, 
            // it could be only a few properties
            // that are modified.



            Example retrievedRecord;
            if (acceptExampleAsComplete)
            {
                retrievedRecord = this._exampleRepositoryService.GetSingle(example.Id);

                if (retrievedRecord == null)
                {
                    Response response = new Response();

                    //TODO:Attach MessageCode to response:
                    return response;
                }

                //Map the incoming values over the current record:
                _objectMappingService.Map(example, retrievedRecord);
            }
            else
            {
                retrievedRecord = example;


            }


            //TODO: Validate the new object:  

            this._exampleRepositoryService.Update(retrievedRecord);

            return new Response();
        }
    }
}