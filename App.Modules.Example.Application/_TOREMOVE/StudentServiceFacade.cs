﻿//// ReSharper disable CheckNamespace

//namespace App.Core.Application.Services.Facades.Implementations
//// ReSharper restore CheckNamespace
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using App.Core.Application.Services.Facades;
//    using App.Core.Domain.Messages;
//    using App.Core.Domain.Messages.Implementations;
//    using App.Core.Domain.Messages.Implementations.Legacy;
//    using XAct.Messages;
//    using XAct.Services;
//    using App.Core.Domain.Models;
//    using App.Core.Infrastructure;
//    using App.Core.Application.Services;
//    using App.Core.Domain.Entities;
//    using App.Core.Infrastructure.Services;
//    using XAct;

//    [DefaultBindingImplementation(typeof (IStudentServiceFacade))]
//    [ServiceErrorBehaviour(typeof (HttpErrorHandler))]
//    public class StudentServiceFacade : WcfSessionAuthenticatedServiceFacadeBase, IStudentServiceFacade
//    {
//        private readonly IStudentSearchService _studentSearchService;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="StudentServiceFacade" /> class.
//        /// </summary>
//        /// <param name="studentSearchService">The student search service.</param>
//        /// <param name="unitOfWorkService">The unit of work service.</param>		
//        public StudentServiceFacade(
//            IStudentSearchService studentSearchService,
//            IUnitOfWorkService unitOfWorkService
//            )
//            : base(unitOfWorkService)
//        {
//            _studentSearchService = studentSearchService;
//        }


//        // -- Student - Searches

//        public virtual PagedResponse<SearchStudentsResponseItem> PagedCriteriaSearch(PagedRequest<SearchStudentsCriteria> studentQueryCriteria)
//        {
//            return InvokeServiceFunction(() => _studentSearchService.StudentNameCriteriaSearch(studentQueryCriteria));
//        }

//    }
//}