﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Core.AppHost.Models
{
    public class HomeViewModel
	{
		// TODO: Change this generation to build-server level? It would allow multiple front-end servers

		// Store single, app-wide cache busting value
		private static string _appCacheBustingValue;

		public string CacheBustingValue { get
		{
			if (string.IsNullOrEmpty(_appCacheBustingValue))
			{
				// Generate a short, random number to use for the lifetime of the app.
				// This ensures that each deployment will get a new cache busting URL param, and the user's browser will update their cache
				// (Note - must not cycle more than once per deployment - as it must honour HTTP caching headers)
				_appCacheBustingValue = (DateTime.Now.Ticks % 314159).ToString();
			}

			return _appCacheBustingValue;
		}}
	}
}