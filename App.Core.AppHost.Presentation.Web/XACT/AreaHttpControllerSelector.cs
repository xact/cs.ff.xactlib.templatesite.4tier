﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using XAct.Diagnostics;

namespace XActDebug.Web.Http.Dispatcher
{
    /// <summary>
    /// An specialization of the 
    /// <see cref="DefaultHttpControllerSelector"/>
    /// so that in route to webAPI controllers within
    /// <c>Areas.</c>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Basically, the issue is that Areas allow the organisation of code
    /// so that not all controllers are in the root (large apps generally
    /// have too many to manage easily).
    /// </para>
    /// <para>
    /// Registration is done as follows:
    /// <para>
    /// <code>
    /// <![CDATA[
    /// protected void Application_Start()
    /// {
    ///   ...
    ///   GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector), new AreaHttpControllerSelector(GlobalConfiguration.Configuration));
    /// }
    /// ]]>
    /// </code>
    /// then add a route:
    /// <code>
    /// <![CDATA[
    /// routes.MapHttpRoute(
    ///   name: "DefaultApi",
    ///   routeTemplate: "api/{area}/{controller}/{id}",
    ///   defaults: new { id = RouteParameter.Optional }
    /// );
    /// ]]>
    /// </code>
    /// </para>
    /// </para>
    /// </remarks>
    public class AreaHttpControllerSelector : DefaultHttpControllerSelector
    {

        private new const string ControllerSuffix = "Controller";
        private const string AreaRouteVariableName = "area";

        private readonly HttpConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="AreaHttpControllerSelector" /> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public AreaHttpControllerSelector(HttpConfiguration configuration)
            : base(configuration)
        {
            //keep a ref to config that we passed to underlying 
            //defaultHttpControllerSelector
            _configuration = configuration;
        }



        #region Private Methods

        private Dictionary<string, Type> _apiControllerTypes;

        private Dictionary<string, Type> ApiControllerTypes
        {
            get
            {
                var result =
                    _apiControllerTypes ?? (_apiControllerTypes = GetControllerTypes());
                return result;
            }
        }


        /// <summary>
        /// Selects a <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> for the given <see cref="T:System.Net.Http.HttpRequestMessage" />.
        /// </summary>
        /// <param name="request">The HTTP request message.</param>
        /// <returns>
        /// The <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> instance for the given <see cref="T:System.Net.Http.HttpRequestMessage" />.
        /// </returns>
        public override HttpControllerDescriptor SelectController(HttpRequestMessage request)
        {
            var result = GetApiController(request) ?? base.SelectController(request);
            return result;
        }

        /// <summary>
        /// Method GetControllerTypes takes all the API controllers types from all of your assemblies, and store it inside the dictionary, where the key is FullName of the type and value is the type itself.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.DebuggerHidden]
        private Dictionary<string, Type> GetControllerTypes()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            Dictionary<string, Type> types = new Dictionary<string, Type>();

            foreach (Assembly assembly in assemblies)
            {
                try
                {
                    foreach (Type type in assembly.GetTypes().Where(
                        t =>
                        !t.IsAbstract && t.Name.EndsWith(ControllerSuffix) &&
                        typeof (IHttpController).IsAssignableFrom(t)))
                    {
                        types.Add(type.FullName, type);
                    }
                }
                catch (Exception e)
                {
                    //Log completion:
                    if (XAct.DependencyResolver.BindingResults.IsInitialized)
                    {
                        ITracingService tracingService =
                            XAct.DependencyResolver.Current.GetInstance<ITracingService>(false);

                        if (tracingService != null)
                        {
                            XAct.DependencyResolver.Current.GetInstance<ITracingService>()
                                .TraceException(
                                    TraceLevel.Verbose,
                                    e,
                                    "AreaHttpControllerSelector.GetControllerTypes exception.");
                        }
                    }
                }
            }

        //var types = assemblies.SelectMany(
        //        a =>
        //        a.GetTypes().Where(
        //            t =>
        //            !t.IsAbstract && t.Name.EndsWith(ControllerSuffix) && typeof(IHttpController).IsAssignableFrom(t)))
        //        .ToDictionary(t => t.FullName, t => t);

            return types;
        }




        private static string GetAreaName(HttpRequestMessage request)
        {
            var data = request.GetRouteData();

            var result =
                !data.Values.ContainsKey(AreaRouteVariableName) ? null : data.Values[AreaRouteVariableName].ToString().ToLower();

            return result;
        }

        private Type GetControllerTypeByArea(string areaName, string controllerName)
        {
            var areaNameToFind = string.Format(".{0}.", areaName.ToLower());
            var controllerNameToFind = string.Format(".{0}{1}", controllerName, ControllerSuffix);

            Type type = ApiControllerTypes.Where(t => t.Key.ToLower().Contains(areaNameToFind) && t.Key.EndsWith(controllerNameToFind, StringComparison.OrdinalIgnoreCase))
                    .Select(t => t.Value).FirstOrDefault();
            return type;
        }

        private HttpControllerDescriptor GetApiController(HttpRequestMessage request)
        {
            var controllerName = base.GetControllerName(request);

            var areaName = GetAreaName(request);
            if (string.IsNullOrEmpty(areaName))
            {
                return null;
            }

            var type = GetControllerTypeByArea(areaName, controllerName);
            if (type == null)
            {
                return null;
            }

            return new HttpControllerDescriptor(_configuration, controllerName, type);
        }

        #endregion
    }
}

