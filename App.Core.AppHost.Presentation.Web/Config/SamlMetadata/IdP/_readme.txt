﻿This directory is for persisting the Metadata received from the SAML IdP.
It's pointed to via the Web Config File.


It was downloaded from:

  * DEV:  https://development.security.education.org.nz/opensso  (this is Beta)
  * ST:  https://lsi.security.education.org.nz/opensso 
  * SIT: https://lsi.security.education.org.nz/opensso