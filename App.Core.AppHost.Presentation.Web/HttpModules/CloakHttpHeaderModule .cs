﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Core.AppHost.HttpModules
{
    /// <summary>
    /// Custom HTTP Module for Cloaking IIS7 Server Settings to allow anonymity
    /// </summary>
    public class CloakHttpHeaderModule : IHttpModule
    {
        private List<string> headersToCloak;

        /// <summary>
        /// Initializes a new instance of the <see cref="CloakHttpHeaderModule"/> class.
        /// </summary>
        public CloakHttpHeaderModule()
        {
            this.headersToCloak = new List<string>
                                      {
                                              "Server",
                                              "X-Powered-By",
                                            //"X-AspNet-Version", // Could be done here but currently being removed in the web.config
                                            //"X-AspNetMvc-Version" // Could be done here but currently being removed in the global.asax
                                      };
        }

        public void Init(HttpApplication context)
        {
            context.PreSendRequestHeaders += OnPreSendRequestHeaders;
        }

        public void Dispose()
        { }

        void OnPreSendRequestHeaders(object sender, EventArgs e)
        {
            if (HttpContext.Current != null)
            {
                this.headersToCloak.ForEach(h => HttpContext.Current.Response.Headers.Remove(h));                
            }
        } 
    }
}