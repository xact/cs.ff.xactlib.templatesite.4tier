﻿using Microsoft.Owin;

//MUST: as per http://www.asp.net/aspnet/overview/owin-and-katana/owin-startup-class-detection
//Requires the following attribute/
//AND: because it's not called 'AssemblyName.Startup' (the convention) as it has
//* a namespace that is not the same asssembly name 
//* AND it has a classname that is SignalRStartup rather than Startup,
//you have to also add an AppSettings as follows:
//<add key="owin:appStartup" value="App.Core.AppHost.Initialization.SignalRStartup" />
[assembly: OwinStartup(typeof(App.Core.AppHost.Initialization.OWinStartup))]
namespace App.Core.AppHost.Initialization
{
    using Microsoft.AspNet.SignalR;
    using Owin;

    public class OWinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            HubConfiguration hubConfiguration = new HubConfiguration();
            hubConfiguration.EnableDetailedErrors = true;
            hubConfiguration.EnableJavaScriptProxies = true;

            app.MapSignalR(hubConfiguration);

        }
    }
}