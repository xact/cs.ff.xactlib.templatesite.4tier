﻿using App.Core.Presentation.Initialization;
using XAct;

namespace App.Core.AppHost.Initialization.Steps
{

    public class InitializeLowerLayersInitializationStep : IHasInitializationStep
    {

        public void Execute()
        {
            PresentationInitializer.Execute();
        }
    }
}
