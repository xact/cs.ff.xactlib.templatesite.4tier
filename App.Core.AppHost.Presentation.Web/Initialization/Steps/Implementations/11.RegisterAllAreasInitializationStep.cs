﻿using System.Web.Mvc;
using XAct.Services;

namespace App.Core.AppHost.Initialization.Steps
{
    using XAct;

    //[DefaultBindingImplementation(typeof(IRegisterAllAreasInitializationStep))]
    public class RegisterAllAreasInitializationStep  : IHasInitializationStep//: IRegisterAllAreasInitializationStep
    {
        public void Execute()
        {
            //This will registere both MVC and WebAPI routes in each Area
            try
            {
                AreaRegistration.RegisterAllAreas();
            }
// ReSharper disable RedundantCatchClause
            catch (System.Exception e)
            {
                throw;
            }
// ReSharper restore RedundantCatchClause
        }
    }
}