﻿using XAct.Services;

namespace App.Core.AppHost.Initialization.Steps
{
    using System.Web.Http.ExceptionHandling;
    using System.Web.Mvc;
    using System.Web.Mvc.Async;
    using XAct;
    using XAct.Services.IoC;
    using global::XAct;


    //[DefaultBindingImplementation(typeof(IRegisterReplacementMVCFactoriesInitializationStep))]
    public class RegisterReplacementMVCFactoriesInitializationStep : IHasInitializationStep//: IRegisterReplacementMVCFactoriesInitializationStep
    {
        public void Execute()
        {

            //At this point in the initialisation lifecycle, all the underlying components 
            //(ie, Infrastructure, and the Layers that rely on it -- Presentation, Application, etc.)  
            //are ready as we already called the IoC Bootstapper.

            //All that's left to do is handle the bootstrapping of the current AppHost and Presentation Technology Component:


            //But MVC uses a whole set of Initializes that don't use ServiceLocator -- which is a problem.
            //They need to be replaced, so that new Controllers, etc. can be DependencyInjected when the Controllers
            //are created.


            //Which is why we call:
            XAct.Services.IoC.MvcBootstrapper.Initialize();

            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(new BindingDescriptor(BindingType.Custom, typeof(ITempDataProvider), typeof(SessionStateTempDataProvider)));
            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(new BindingDescriptor(BindingType.Custom, typeof(ITempDataProviderFactory), typeof(ServiceLocatorTempDataProviderFactory)));


            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(new BindingDescriptor(BindingType.Custom, typeof(IAsyncActionInvoker), typeof(AsyncControllerActionInvoker)));
            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(new BindingDescriptor(BindingType.Custom, typeof(IAsyncActionInvokerFactory), typeof(ServiceLocatorAsyncActionInvokerFactory)));


            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(new BindingDescriptor(BindingType.Custom, typeof(IExceptionHandler), typeof(System.Web.Http.ExceptionHandling.ExceptionHandler)));

            /*
            //Which does the following:
            
            //First things first: replace the default DependencyResolver with a ServiceLocator based one:
            DependencyResolver.SetResolver(new ServiceLocatorDependencyResolver());
            
            //Once that's done, we ened to register within the above new DependencyResolver
            //a Controller Factory (used to create new Controllers)
            //that can instantiate Controllers, while Dependency Injecting them.
            XAct.DependencyResolver.Current.RegisterServiceBindingInIoCByReflection(
                new BindingDescriptor<IControllerFactory, ServiceLocatorControllerFactory>());
            
            //Same goes for how Views Are constructed:
            XAct.DependencyResolver.Current.RegisterServiceBindingInIoCByReflection(
                new BindingDescriptor<IViewPageActivator, ServiceLocatorViewPageActivator>());
            
            if (modelMetadataProviderType != null)
            {
                //As MVC uses the default Resource resolver, and it is not registered, this will cause a minor exception:
                //ModelMetadataProviders.Current = new XAct.UI.ViewModeModelMetadataProvider();
                //When using an DependencyInjectionContainer, the preferred solution is registering it in the underlying DependencyInjectionContainer:
                    XAct.DependencyResolver.Current.RegisterServiceBindingInIoCByReflection(
                    new BindingDescriptor(typeof(ModelMetadataProvider), modelMetadataProviderType)
                    );
            }
            */



        }
    }
}


namespace XAct.Services.IoC
{
    using System.Linq.Expressions;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http.ExceptionHandling;

 
    using System.Web.Mvc;
    using System.Web.Mvc.Async;




    public class ServiceLocatorAsyncActionInvokerFactory : IAsyncActionInvokerFactory
    {
        public IAsyncActionInvoker CreateInstance()
        {
            IAsyncActionInvoker result = XAct.DependencyResolver.Current.GetInstance<IAsyncActionInvoker>(false);
            return result ?? new AsyncControllerActionInvoker();
        }
    }


    public class ServiceLocatorTempDataProviderFactory : ITempDataProviderFactory
    {
        public ITempDataProvider CreateInstance()
        {
            ITempDataProvider result = XAct.DependencyResolver.Current.GetInstance<ITempDataProvider>(false);
            return result ?? new SessionStateTempDataProvider();
        }
    }
}