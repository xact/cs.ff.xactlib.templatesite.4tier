﻿
using System.Web.Http;
using XAct.Services;
using XAct.Services.IoC;

namespace App.Core.AppHost.Initialization.Steps
{
    using XAct;

    //[DefaultBindingImplementation(typeof (IRegisterReplacementWebAPIFactoriesInitializationStep))]
    public class RegisterReplacementWebAPIFactoriesInitializationStep : IHasInitializationStep//: IRegisterReplacementWebAPIFactoriesInitializationStep
    {

		public void Execute()
        {
            //That takes care of MVC. 
            //But WebAPI, no matter how integrated it appears to be with MVC, is not MVC,
            //so it too needs it's own SericeLocator based DependencyResolver to be registered:
           System.Web.Http.Dependencies.IDependencyResolver originalDependencyResolver = 
               GlobalConfiguration.Configuration.DependencyResolver;

			var tracingService = XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.ITracingService>();

            GlobalConfiguration.Configuration.DependencyResolver =
                new XActDebug.Services.IoC.WebAPIServiceLocatorDependencyResolver(tracingService);



    
        }
    }
}
