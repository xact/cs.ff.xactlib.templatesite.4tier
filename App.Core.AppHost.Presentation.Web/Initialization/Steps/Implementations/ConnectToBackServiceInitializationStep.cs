﻿namespace App.Core.AppHost.Initialization.Steps
{
    using App.Core.Application.Services.Facades;
    using App.Core.Infrastructure.Front.Services;
    using XAct;

    public class ConnectToBackServiceInitializationStep : IHasInitializationStep
    {
        public void Execute()
        {
            var s = XAct.DependencyResolver.Current.GetInstance<IWcfServiceAgentService>();

            string r = s.Query<IDiagnosticsServiceFacade, string>(service => service.Ping());

            var s2 = XAct.DependencyResolver.Current.GetInstance<ICachedWcfServiceAgentService>();

            string r2 = s2.Query<IDiagnosticsServiceFacade, string>(service => service.Ping(), "startup:ping");



        }
    }
}