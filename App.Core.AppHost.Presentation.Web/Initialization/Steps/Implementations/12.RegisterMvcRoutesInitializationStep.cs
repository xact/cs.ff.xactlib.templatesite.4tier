﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using App.Core.AppHost.UI;
using App.Core.Core.AppHost.UI;
using XAct.Services;

// ReSharper disable CheckNamespace
namespace App.Core.AppHost.Initialization.Steps
// ReSharper restore CheckNamespace
{
    using XAct;

    //[DefaultBindingImplementation(typeof(IRegisterMvcRoutesInitializationStep))]
    public class RegisterMvcRoutesInitializationStep : IHasInitializationStep//: IRegisterMvcRoutesInitializationStep
    {
        public void Execute()
        {

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AuthConfig.RegisterAuth();
        }
    }
}