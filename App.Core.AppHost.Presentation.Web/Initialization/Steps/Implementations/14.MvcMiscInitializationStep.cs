﻿using System.Web.Mvc;
using XAct;
using XAct.Services;

namespace App.Core.AppHost.Initialization.Steps
{
    //[DefaultBindingImplementation(typeof(IMvcMiscInitializationStep))]
    public class MvcMiscInitializationStep : IHasInitializationStep//: IMvcMiscInitializationStep
    {

        public void Execute()
        {

            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                new BindingDescriptor(BindingType.Custom, 
                    typeof(System.Web.Mvc.ITempDataProvider), typeof(SessionStateTempDataProvider)));


            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                new BindingDescriptor(
                    BindingType.Custom, 
                    typeof(System.Web.Mvc.Async.IAsyncActionInvoker),
                    typeof(System.Web.Mvc.Async.AsyncControllerActionInvoker)));

            var check1 = XAct.DependencyResolver.Current.GetInstance<System.Web.Mvc.ITempDataProvider>(false);
            var check2 = XAct.DependencyResolver.Current.GetInstance<System.Web.Mvc.Async.AsyncControllerActionInvoker>(false);


            //These are the classes that I've seen being requested through the 
            //System.Web.Http.Hosting.IHostBufferPolicySelector;
            //System.Web.Http.Metadata.ModelMetadataProvider ;
            //System.Web.Http.Tracing.ITraceManager ;
            //System.Web.Http.Tracing.ITraceWriter ;
            //System.Web.Http.Dispatcher.IHttpControllerSelector ;
            //System.Web.Http.Dispatcher.IAssembliesResolver ;
            //System.Web.Http.Dispatcher.IHttpControllerTypeResolver ;
            //System.Web.Http.Controllers.IHttpActionSelector ;
            //System.Web.Http.Controllers.IActionValueBinder ;
            //System.Net.Http.Formatting.IContentNegotiator ;
            //System.Web.Http.Dispatcher.IHttpControllerActivator ;



        }
    }
}