﻿using System.Web.Http;
using App.Core.AppHost.UI;
using XAct.Services;

namespace App.Core.AppHost.Initialization.Steps
{
    using XAct;

    //[DefaultBindingImplementation(typeof(IRegisterWebAPIRoutesInitializationStep))]
    public class RegisterWebAPIRoutesInitializationStep : IHasInitializationStep//: IRegisterWebAPIRoutesInitializationStep
    {
        public void Execute()
        {
            //4.0
			//WebApiConfig.Register(GlobalConfiguration.Configuration);
            //5.0
            GlobalConfiguration.Configure(WebApiConfig.Register);

            //routes.MapHttpRoute(
            //  name: "DefaultApi",
            //  routeTemplate: "api/{area}/{controller}/{id}",
            //  defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}