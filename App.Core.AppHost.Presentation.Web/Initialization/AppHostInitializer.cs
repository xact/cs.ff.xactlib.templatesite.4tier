﻿namespace App.Core.AppHost.Initialization
{
    using System.Reflection;
    using App.Core.AppHost.Initialization.Steps;
    using XAct.Commands;

    /// <summary>
    /// Part of the initialization sequence chain.
    /// <para>
    /// The initialization sequence is as follows:
    /// <code>
    /// * Global.asax 
    ///     * ... invokes AppHostInitializer (this class),
    ///         * ... which first invokes PresentationInitializer
    ///             * ... which first invokes FrontInfrastructureInitializer
    ///               where IoC (ie Unity) is initialized with Infrastructure Services...
    ///         * ....now, coming back up...Presentation services are registered...
    ///     * ... now that both Infras and presentation are initialized, AppHostInitializer
    ///       finishes up by registering AppHost level Services.
    /// * Initialization done...
    /// * Integration cycle then starts...
    ///   It too goes down through the layers, and back up, in much the same way... 
    ///   but this time actually using remote services (eg, to load caches, etc).      
    /// </code>
    /// </para>
    /// </summary>
    public static class AppHostInitializer
    {


        //At this point, after FrontAppBootstrapper has been invoked
        //IoC is available to get hold of all services.
        //Except both MVC and WebAPI have custom factories that by 
        //default don't rely on ServiceLocator and so need to be replaced:
        //And WebAPI's Resolver needs to be replaced with something that can do areas.


        public static void Execute()
        {

                //Initialize the lower layers first:
                new InitializeLowerLayersInitializationStep().Execute(); // Must occur before MVC routes

                //Now that lower layers (ie Front Infrastructure) is initialized
               //IoC works, etc.
                //Continue....
                new RegisterAllAreasInitializationStep().Execute(); // Must occur before MVC routes
                new RegisterWebAPIRoutesInitializationStep().Execute(); // Must occur before MVC routes
                new RegisterMvcRoutesInitializationStep().Execute();
                new RegisterReplacementMVCFactoriesInitializationStep().Execute();
                new MvcMiscInitializationStep().Execute();
                new RegisterReplacementWebAPIFactoriesInitializationStep().Execute();
                new RegisterWebAPIAreaRouterInitializationStep().Execute();
                new ConnectToBackServiceInitializationStep().Execute();

        //    new HasExecutableActionRunner
        //        (
        //        MethodBase.GetCurrentMethod().DeclaringType.FullName,
        //        true, true, null,
        //        /*
        //        //Just for comparison, this is the default sequence:
        //        AreaRegistration.RegisterAllAreas();
        //        WebApiConfig.Register(GlobalConfiguration.Configuration);
        //        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        //        RouteConfig.RegisterRoutes(RouteTable.Routes);
        //        */

        //        //----------------------------------------------------------
        //        //As these InitializationSteps are done before Services are
        //        //available via ServiceLocator, we invoke them *NOT* by 
        //        //interface, but by Class (ExecutableActionRunner knows to 
        //        //skip the use ServiceLocator if invoked by Class):

        //        typeof(InitializeLowerLayersInitializationStep), // Must occur before MVC routes

        //        //Now that we all Services have been registered, we can reference
        //        //Steps by Interface (and therefore start using Dependency Injection
        //        //as needed).
        //        //----------------------------------------------------------


        //        typeof(IRegisterAllAreasInitializationStep), // Must occur before MVC routes
        //        typeof(IRegisterWebAPIRoutesInitializationStep), // Must occur before MVC routes
        //        typeof(IRegisterMvcRoutesInitializationStep),
        //        typeof(IRegisterReplacementMVCFactoriesInitializationStep),
        //        typeof(IMvcMiscInitializationStep),
        //        typeof(IRegisterReplacementWebAPIFactoriesInitializationStep),
        //        typeof(IRegisterWebApiAreaRouterInitializationStep)
        //        )
        //        .Execute();
        
        
        
        }
    }
}