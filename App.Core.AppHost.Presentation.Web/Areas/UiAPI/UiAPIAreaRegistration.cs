﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using App.Core.AppHost.Areas.UiAPI.Controllers;

namespace App.Core.AppHost.Areas.UiAPI
{
	public class UiAPIAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				// Must match up with the namespace
				// (Used for area-selection)
				return "UiAPI";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{

            var route = context.Routes.MapHttpRoute(
                "Session",
                "UIAPI/Session",
                new { area = AreaName },
                new { httpMethod = new HttpMethodConstraint(HttpMethod.Delete.ToString()), Controller = "SessionController" }
                );
            AddAreaWebApiRoute(route);

            //// A specific controller that adds State to WebAPI controllers
            ////(by default they don't --- but SessionController needs it to logout).
            route.RouteHandler = new MyHttpControllerRouteHandler();

            
            //NOT the way to do it:
            //* for one, it has Actions (that's an MVC, not an WebAPI concept)
            //* it also has no tie-in to namespacing:
            //* for three, it's not MapRoute, it's MapHttpRoute:d:\CODE\NSI2\Dev\WA\App.Core.Front.AppHost.Web\Areas\UiAPI\UiAPIAreaRegistration.cs
            //  For MVC we use route = context.MapRoute
            //  Whereas in WebAPI we use route = context.MapHttpRoute
            //context.MapRoute(
            //    "UiAPI_default",
            //    "UiAPI/{controller}/{action}/{id}",
            //    new { action = "Index", id = UrlParameter.Optional }
            //);


            //AddAreaWebApiRoute(context.Routes.MapHttpRoute(
            //                       "API_UI_default_student",
            //                       "UIAPI/{controller}/{student}",
            //                       new { area = AreaName }
            //                       ));

            //AddAreaWebApiRoute(context.Routes.MapHttpRoute(
            //                       "API_UI_default_data",
            //                       "UIAPI/{controller}/{data}",
            //                       new { area = AreaName }
            //                       ));


            //AND (Important as it's so easy to miss) no Action is in path

			//AddAreaWebApiRoute(context.Routes.MapHttpRoute(
			//	"API_UI_Students",
			//	"UIAPI/Students/{criteria}",
			//	//Use RouteParameter (WebAPI), not UrlParameter(Mvc)
			//					   new { area = AreaName }
			//					   ));
			
            // routes.MapHttpRoute("DefaultApiWithId", "Api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });
            // routes.MapHttpRoute("DefaultApiWithAction", "Api/{controller}/{action}");

            //routes.MapHttpRoute("DefaultApiWithId", "Api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });
            //routes.MapHttpRoute("DefaultApiWithAction", "Api/{controller}/{action}");
            //routes.MapHttpRoute("DefaultApiGet", "Api/{controller}", new { action = "Get" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            //routes.MapHttpRoute("DefaultApiPost", "Api/{controller}", new { action = "Post" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

			// Default route maps:
			AddAreaWebApiRoute(context.Routes.MapHttpRoute(
                "API_UI_default",
                "UIAPI/{controller}",
                //Use RouteParameter (WebAPI), not UrlParameter(Mvc)
                                   new { area = AreaName}
                                   ));

            AddAreaWebApiRoute(context.Routes.MapHttpRoute(
                "API_UI_default_opt",
                "UIAPI/{controller}/{id}",
	                               //Use RouteParameter (WebAPI), not UrlParameter(Mvc)
                                   new { area = AreaName, id = RouteParameter.Optional },
                                   new { id = @"\d+" } //Regex constraint to numbers:
                                   ));

			// Custom route maps. 

			AddAreaWebApiRoute(context.Routes.MapHttpRoute(
				"API_UI_default_id_version",
				"UIAPI/{controller}/{id}/{version}",
				//Use RouteParameter (WebAPI), not UrlParameter(Mvc)
				new {area = AreaName},
				new {id = @"\d+", version = @"\d+"} //Regex constraint to numbers:
				                   ));


			AddAreaWebApiRoute(context.Routes.MapHttpRoute(
				"API_UI_MergeRequest_Discard",
				"UIAPI/MergeRequest/{id}/{action}",
				new {area = AreaName},
				new {Controller = "MergeRequestController"}
				                   ));


            ////Place Action based routing after Id, as long as regex rule is applied to id, to catch integers.
            //AddAreaWebApiRoute(context.Routes.MapHttpRoute(
            //    "API_UI_default_action",
            //    "UIAPI/{controller}/{action}",
            //    //Use RouteParameter (WebAPI), not UrlParameter(Mvc)
            //           new { area = AreaName }
            //           ));

            AddAreaWebApiRoute(context.Routes.MapHttpRoute(
                "API_UI_default_name",
                "UIAPI/{controller}/{name}",
                //Use RouteParameter (WebAPI), not UrlParameter(Mvc)
                                   new { area = AreaName },
                                   new { name = @"\w+" } //Regex constraint to numbers:
                                   ));


            AddAreaWebApiRoute(context.Routes.MapHttpRoute(
                "API_UI_default_names",
                "UIAPI/{controller}/{names}",
                //Use RouteParameter (WebAPI), not UrlParameter(Mvc)
                                   new { area = AreaName },
                                   new { names = @"([\w]+)([,;\|][\w]+)*" } //Regex constraint to numbers:
                                   ));



            //AddAreaWebApiRoute(context.Routes.MapHttpRoute(
            //    "API_UI_default_key",
            //    "UIAPI/{controller}/{key}",
            //    //Use RouteParameter (WebAPI), not UrlParameter(Mvc)
            //                       new { area = AreaName },
            //                       new { name = @"[a-zA-Z]+" } //Regex constraint to numbers:
            //                       ));




            AddAreaWebApiRoute(context.Routes.MapHttpRoute(
                "API_UI_default_ApiGet",
                "UIAPI/{controller}",
                new { action = "Get" },
                new { httpMethod = new HttpMethodConstraint(HttpMethod.Get.ToString()) }
	                               ));

            AddAreaWebApiRoute(context.Routes.MapHttpRoute(
                "API_UI_default_ApiPost",
                "UIAPI/{controller}",
                new { action = "Post" },
                new { httpMethod = new HttpMethodConstraint(HttpMethod.Post.ToString()) }
                ));






	    }

	    private void AddAreaWebApiRoute(Route route)
	    {
            if (route.DataTokens == null)
            {
                route.DataTokens = new RouteValueDictionary();
            }

            //We assoicate this route to this namespace:
            string nsToSearchIn = typeof(App.Core.AppHost.Areas.UiAPI.Controllers.ResourceSetController).Namespace;
	        route.DataTokens["Namespaces"] = new[] {nsToSearchIn};

        }
	}
}
