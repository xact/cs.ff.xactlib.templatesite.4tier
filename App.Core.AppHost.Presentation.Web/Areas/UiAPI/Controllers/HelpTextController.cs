﻿using System.Collections.Generic;
using System.Web.Http;
using App.Core.Domain.Messages;
using App.Core.Presentation.Services;

namespace App.Core.AppHost.Areas.UiAPI.Controllers
{
    using App.Core.Infrastructure.Services;

    //[BreezeController]
    public class HelpTextController : ApiControllerBase
    {
        private readonly IHelpService _helpService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpTextController" /> class.
        /// </summary>
        /// <param name="helpService">The help service.</param>
        public HelpTextController(IHelpService helpService)
        {
            _helpService = helpService;
        }

        //
        // GET: /UiAPI/HelpText/

        [System.Web.Http.HttpGet]
        public Dictionary<string, string> GetByViewName(string name)
        {
            var results = new Dictionary<string, string>();
            var cultureInfo = System.Threading.Thread.CurrentThread.CurrentUICulture;

            HelpEntrySummary helpEntrySummary = _helpService.GetHelpEntryByKey(name, cultureInfo);

            FlattenHelpEntry(helpEntrySummary, results);
            
            return results;

        }

        private void FlattenHelpEntry(HelpEntrySummary helpEntrySummary, Dictionary<string, string> target)
        {
            if (helpEntrySummary == null)
            {
                return;
            }
            if (!target.ContainsKey(helpEntrySummary.Key))
            {
                target.Add(helpEntrySummary.Key, helpEntrySummary.Value);
            }
            else
            {
                target[helpEntrySummary.Key] = helpEntrySummary.Value;
            }

            if (helpEntrySummary.Children != null && helpEntrySummary.Children.Count > 0)
            {
                foreach (var childEntry in helpEntrySummary.Children)
                {
                    if (!target.ContainsKey(childEntry.Key))
                    {
                        target.Add(childEntry.Key, childEntry.Value);
                    }
                    else
                    {
                        target[childEntry.Key] = childEntry.Value;
                    }
                }
            }
        }

    }
}
