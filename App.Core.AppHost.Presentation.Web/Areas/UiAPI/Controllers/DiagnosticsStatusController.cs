﻿namespace App.Core.AppHost.Areas.UiAPI.Controllers
{
    using System;
    using System.Web.Http;
    using App.Core.Infrastructure.Front.Services;
    using App.Core.Infrastructure.Services;
    using XAct.Diagnostics.Services.Implementations;

    /// <summary>
    /// Controller to return diagnostic information as to the health of the application.
    /// </summary>
    public class DiagnosticsStatusController : ApiControllerBase
    {
        private readonly IDiagnosticsStatusService _statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DiagnosticsStatusController"/> class.
        /// </summary>
        /// <param name="statusService">The status service.</param>
        public DiagnosticsStatusController(IDiagnosticsStatusService statusService)
        {
            _statusService = statusService;
        }


        //[System.Web.Http.HttpGet]
        //public virtual StatusResponse[] Get(string[] names, DateTime? startDateTimeUtc = null, DateTime? endDateTimeUtc = null)
        //{
        //    var results = _statusService.Get(names, startDateTimeUtc, endDateTimeUtc);
        //    return results;
        //}

        [System.Web.Http.HttpGet]
        //[Route("/uiapi/diagnosticsStatus/{name}/{arguments:object?}")]
        public virtual StatusResponse Get(string name, object arguments = null, DateTime? startDateTimeUtc = null,
                                          DateTime? endDateTimeUtc = null)
        {
            var results = _statusService.Get(name, arguments, startDateTimeUtc, endDateTimeUtc);
            return results;
        }

    }

}