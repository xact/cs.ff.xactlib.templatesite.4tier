namespace App.Core.AppHost.Areas.UiAPI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Web.Http;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Front.Services;
    using App.Core.Infrastructure.Services;

    /// <summary>
    /// Controller for returning Resources Per View 
    /// <para>
    /// See ResourceController if you want to work with single Resources.
    /// </para>
    /// </summary>
    public class ResourceSetController : ApiControllerBase /*ODataController*/
    {
        //TIP: Keep NS on to disambiguate betwen Presentation and Infa service of same name.
        private readonly ICultureSpecificResourceService _resourceService;
        private readonly IClientEnvironmentService _clientEnvironmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceSetController" /> class.
        /// </summary>
        /// <param name="resourceService">The resource service.</param>
        /// <param name="clientEnvironmentService">The client environment service.</param>
        public ResourceSetController(ICultureSpecificResourceService resourceService, IClientEnvironmentService clientEnvironmentService)
        {
            _resourceService = resourceService;
            _clientEnvironmentService = clientEnvironmentService;
        }


        //public string Get()
        //{
        //    return "Works";
        //}

        //public string Get(string id)
        //{
        //    return "Works";
        //}

        // GET: /API/ViewResources/welcome

         //~/breeze/todos/Todos
         //~/breeze/todos/Todos?$filter=IsArchived eq false&$orderby=CreatedAt
        //public Dictionary<string, string> GetByViewName(string id)
        //{
        //    CultureInfo cultureInfo = _clientEnvironmentService.ClientUICulture;

        //    try
        //    {

        //        var result = _resourceService.GetResourceSet(id, cultureInfo);

        //        //return _contextProvider.Context.Todos;
        //        return result;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}
        [System.Web.Http.HttpGet]
        public Dictionary<string, Dictionary<string, string>> GetByViewName(string id)
        {
			CultureInfo cultureInfo = _clientEnvironmentService.ClientUICulture;

            var results = new Dictionary<string, Dictionary<string, string>>();
            var filters = id.Split(new[]{',','|',';'},StringSplitOptions.RemoveEmptyEntries);
			if (filters.Length > 0)
			{
				results = _resourceService.GetResourceSets(cultureInfo, ResourceTransformation.None, filters);
			}

			return results;
		}

		[System.Web.Http.HttpGet]
        public Dictionary<string, Dictionary<string, string>> GetByViewName()
        {
			var results = new Dictionary<string, Dictionary<string, string>>();

			CultureInfo cultureInfo = _clientEnvironmentService.ClientUICulture;
			
				var allFilters = new [] {
							"ShellView"
							,"MessageCode"
							,"FooterView"
							,"HeaderSessionInfo" // General page stuff
							,"HomeView"
							,"WelcomeView" // Pages - Basic
							,"StudentSearchView"
							,"SearchResultsGridView"
							,"StudentDetailsView"
							,"StudentHistoryView"
							,"MergeEntryView"// Pages - Merge Entry
						    ,"MergeListView" // Pages - Merge List
							,"MergeDenialReasonModalView"//ModalWindow - MergeDenialReason
							,"MergeDecisionView"//Pages - Merge Decision 
						    ,"MergeHistoryView"//Pages - Merge History
							,"MergeStatusView"//Pages - Merge status
                            ,"SearchRecordsToMergeView"
                            ,"CHCHView"
                            ,"MergeDeniedPairWidgetView"
                            ,"AddStudentView"
							,"ModifyStudentView"
							,"ProviderListView"
							,"ProviderDetailsView"
                            ,"ParameterDetailsView"
							,"ParametersView"
							,"ParameterMessagesView"
							,"BatchRequestsView"
							,"TransactionActivityReportView"
							,"BatchResultsView"

				};

				results = _resourceService.GetResourceSets(cultureInfo, ResourceTransformation.None, allFilters);

			

			//else if (filters.Length = 0)
			//{
			//	CultureInfo cultureInfo = _clientEnvironmentService.ClientUICulture;
			//	results = _resourceService.GetResourceSets(cultureInfo, ResourceTransformation.None, filters);
			//}

			//if (filters.Length > 0)
			//{
			//	CultureInfo cultureInfo = _clientEnvironmentService.ClientUICulture;
			//	results = _resourceService.GetResourceSets(cultureInfo, ResourceTransformation.None, resourceservice.resources);
			//}



            return results;

        }
    }
}

