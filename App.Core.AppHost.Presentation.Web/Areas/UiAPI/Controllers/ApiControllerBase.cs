﻿using System.Web.Mvc;
using App.Core.AppHost.Attributes;

namespace App.Core.AppHost.Areas.UiAPI.Controllers
{
    using System;
    using System.Net;
    using System.ServiceModel;
    using System.Web;
    using System.Web.Http;

    [ValidateJsonAntiForgeryToken]
    // The ValidateJsonAntiForgeryToken attribute checks that the request has an anti forgery token in the request headers
    // that matches the token in the antiforgery cookie in order to protect against CSRF attacks
    // Strictly speaking the check only needs to happen for POST or PUT or DELETE and not GET, as the same-origin-policy makes it impossible for an attacker to read the response 
    // As the UI on the whole uses AJAX to do GET requests it doesn't hurt to do the check on GET request as well
    // If an action does not need CSRF protection, e.g. the Session Controller, then the actions or Controller can be marked with AllowAnonymous

    [NoOutputCache]
    public abstract class ApiControllerBase : ApiController
    {

        protected TR InvokeServiceFunction<TR>(Func<TR> a)
        {
            try
            {
                TR result = a.Invoke();
                return result;
            }
            catch
            {
                throw;

            }
        }

        //this method is the same as above
        //idea: if we have problems with the AOP - since we have the facade anyways we can find out 
        //what method name has been passed here through expression tree (change the argument to expression)
        //and then do a one-line of code authorization here by calling the authorizationbymethod service 
        //then we can drop the AOP arguments and proxy generation
        //protected TR InvokeServiceFunction<TR>(Func<TR> a)
        //{
        //    var result = default(TR);
        //    InvokeServiceAction(() => result = a.Invoke());
        //    return result;            
        //}

        protected void InvokeServiceAction(Action a)
        {
            try
            {
                a.Invoke();
            }
// ReSharper disable RedundantCatchClause
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                throw;
            }
// ReSharper restore RedundantCatchClause
        }


    }
}