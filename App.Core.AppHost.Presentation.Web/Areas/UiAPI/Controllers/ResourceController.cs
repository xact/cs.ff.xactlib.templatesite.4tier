namespace App.Core.AppHost.Areas.UiAPI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Web.Http;
    using App.Core.Domain.Messages;

    using App.Core.Infrastructure.Front.Services;
    using App.Core.Infrastructure.Services;

    /// <summary>
    /// A Rest/WebAPI Controller that handles *single* resources.
    /// <para>
    /// Note: it's more efficient to use the less chatty ResourceSetController
    /// for fetching the resources required by a View.
    /// </para>
    /// </summary>
    /// <internal>
    /// As we replaced the default Controller factory, 
    /// the controller's constructor can be DI enabled.
    /// </internal>
    //[BreezeController]
    public class ResourceController : ApiControllerBase // ODataController
    {
        private readonly ICultureSpecificResourceService _resourceService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceController" /> class.
        /// </summary>
        /// <param name="resourceService">The resource service.</param>
        public ResourceController(ICultureSpecificResourceService resourceService)
        {
            _resourceService = resourceService;
        }



        [System.Web.Http.HttpGet]
        public Dictionary<string, Dictionary<string, string>> GetResourceSets(string views)
        {
            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentUICulture;
            string[] filters = views.Split(new char[] {',','|',';'}, StringSplitOptions.RemoveEmptyEntries);

            var results = _resourceService.GetResourceSets(cultureInfo, ResourceTransformation.None, filters);

            return results;
        }


        
    }
}
