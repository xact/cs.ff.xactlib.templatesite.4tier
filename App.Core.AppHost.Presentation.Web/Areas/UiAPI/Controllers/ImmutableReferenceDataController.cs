﻿using System.Collections.Generic;
using System.Web.Http.ModelBinding;
using App.Core.AppHost.Models.Binders;
using App.Core.Domain.Messages;
using App.Core.Infrastructure.Front.Services;

namespace App.Core.AppHost.Areas.UiAPI.Controllers
{
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services;

    public class ImmutableReferenceDataController : ApiControllerBase
    {
        private readonly IClientEnvironmentService _clientEnvironmentService;
        private ICachedCultureSpecificReferenceDataViewModelService _cultureSpecificCachedReferenceDataService;


        public ImmutableReferenceDataController(
            Infrastructure.Services.IClientEnvironmentService clientEnvironmentService,
            ICachedCultureSpecificReferenceDataViewModelService cultureSpecificCachedReferenceDataService)
        {
            _clientEnvironmentService = clientEnvironmentService;
            _cultureSpecificCachedReferenceDataService = cultureSpecificCachedReferenceDataService;
        }

        

		public Dictionary<string, ReferenceDataViewModel[]> GetImmutableReferenceData([ModelBinder(typeof(CommaDelimitedArrayModelBinder))] string[] names)
        {
	        var result = new Dictionary<string, ReferenceDataViewModel[]>();


            foreach (var name in names)
            {
                ReferenceDataViewModel[] referenceDataViewModels = _cultureSpecificCachedReferenceDataService.GetReferenceDataViewModel(name, _clientEnvironmentService.ClientUICulture);                
                result.Add(name, referenceDataViewModels);
            }

            return result;
        }

    }
}
