﻿namespace App.Core.AppHost.Areas.PublicAPI.v1.Controllers.Services
{
	using System.Net;

	using XAct.Messages;

	public interface IRestMessageHttpCodeMappingService
	{
		HttpStatusCode GetHttpCodeForRestMessage(MessageCode messageCode);
	}
}