﻿using System.Collections.Generic;

namespace App.Core.AppHost.Areas.PublicAPI.v1.Controllers.Services
{
	using System.Net;

	using App.Core.Constants;

	using XAct.Messages;
	using XAct.Services;

	public class RestMessageHttpCodeMappingService : IRestMessageHttpCodeMappingService
	{
		private readonly IDictionary<MessageCode, HttpStatusCode> _codeMapping;

		public RestMessageHttpCodeMappingService()
		{
			_codeMapping = InitMapping();
		}

		private static IDictionary<MessageCode, HttpStatusCode> InitMapping()
		{
			return new Dictionary<MessageCode, HttpStatusCode>
				       {
					       {MessageCodes.InvalidOrganisationForUser, HttpStatusCode.Forbidden}, //403
					       {MessageCodes.InterfaceNotEnabledForUser, HttpStatusCode.Forbidden}, //403
						   {MessageCodes.AuthorisationDenied, HttpStatusCode.Unauthorized}, //401
						   {MessageCodes.InvalidUserNameOrPasswod, HttpStatusCode.Unauthorized} //401
				       };
		}

		public HttpStatusCode GetHttpCodeForRestMessage(MessageCode messageCode)
		{
			return _codeMapping.Keys.Contains(messageCode) ?
				_codeMapping[messageCode] :
				HttpStatusCode.BadRequest; //400
		}
	}
}