﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace App.Core.AppHost.Areas.PublicAPI.v1.Controllers
{
    using App.Core.Constants.ReferenceData;
    using App.Core.Infrastructure.Front.Services;
    using App.Core.Infrastructure.Services;

    public class HeartBeatController : ApiController
    {
        private readonly ICachedImmutableReferenceDataMessageService _cachedReferenceDataService;

        public HeartBeatController(ICachedImmutableReferenceDataMessageService cachedReferenceDataService)
        {
            _cachedReferenceDataService = cachedReferenceDataService;
        }

        //Access using: https://localhost:44300/uiapi/HeartBeat/Check?accessDb=true
        [HttpGet]
        public HttpResponseMessage Check(bool accessDb = false)
        {
            if (!accessDb)
            {
                //If we don't need more than this, it's good enough to just give back a 200.
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }


            //Get a small set of data, 
            var results = _cachedReferenceDataService.GetReferenceData(ImmutableReferenceData.Gender, true);
            return (results.Length > 0)
                       ? Request.CreateResponse(HttpStatusCode.OK, true)
                       : Request.CreateResponse(HttpStatusCode.InternalServerError, true);
        }
    }
}
