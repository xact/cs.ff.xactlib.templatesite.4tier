﻿using System.Runtime.Serialization;
using System.Web.Http;
using App.Core.AppHost.Areas.API.V1.Controllers;
using App.Core.Infrastructure.Services;

namespace App.Core.AppHost.Areas.PublicAPI.v1.Controllers
{
    /// <summary>
    /// Controller for returning Resources Per View 
    /// <para>
    /// See ResourceController if you want to work with single Resources.
    /// </para>
    /// </summary>
    public class TestController : ApiControllerBase
    {
	    private readonly IPrincipalService _principalService;

	    public TestController(IPrincipalService principalService)
        {
	        _principalService = principalService;
        }


	    [HttpGet]
        public string Get()
        {
            return "Works";
        }

        [HttpGet]
        public string Get(int id)
        {
	        var currentUser = _principalService.CurrentAppIdentity;

	        var userName = currentUser.UserId ?? "Unknown";

            return "Get[apiv1]: Works. id: " + id + ", user: " + userName;
        }

        [HttpPost]
		public TestSerialisation Post([FromUri] int id, [FromBody] TestSerialisation body)
        {
            return body;
        }

		[DataContract]
		public class TestSerialisation
		{
			[DataMember(Name = "test_one")]
			public string One { get; set; }

			public string Two { get; set; }
		}
    }
}

