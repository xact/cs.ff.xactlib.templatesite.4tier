﻿ Logs contain lots of data useful to hackers.

Until there is a strategy for keeping no log output locally, and log to a 
remote location over WCF or other, log files have to be put on the HD.

You can choose something standard like c:\Logs\* but that can be problematic 
(requires the AD account to have access to files outside the application directory tree, 
which can get exploited).

Keep the output in the app directory tree, but tighten security (by adding a local web.config) 
to allow no access to any files within it as.
