﻿
namespace App.Core.AppHost.SignalR
{
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;



    //Created every time a UserAgent contacts the Server

    //If no HubName specified, would be camelCase of ClassName.
    //The HubName is how the javascript finds the hub.
    [HubName("messageHub")]
    public class MessageHub : Hub
     {

         //No specific 
         public MessageHub() : base(){}

         public void SendMessage(string sender, string message)
        {
            //Process messageType here...or just roundtrip it back out:

             //WHat happens:
            //Call all Clients, 
            //invoking the javascript method called 'sendMessage', 
            //passing the args sender, message (which can be json, whatever, depending one expected :
            Clients.All.sendMessage(sender, message);
        }

         public void SendSignal(string messageType, string data)
         {
             //Process messageType here...or just roundtrip it back out:

             //WHat happens:
             //Call all Clients, 
             //invoking the javascript method called 'sendSignal', 
             //passing the args sender, message (which can be json, whatever, depending one expected :
             Clients.All.sendSignal(messageType, data);
         }
     }
}