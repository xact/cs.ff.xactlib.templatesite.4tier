﻿(function ($) {
	//For IE, console is not defined unless the developer tools is opened, so only run
	//it when it is defined.
	if (typeof (console) !== "undefined" && console.log !== undefined) {
		console.log('Shim Started');
	}

	$.extend($, {
		placeholder: {
			browser_supported: function () {
				return this._supported !== undefined ?
                  this._supported :
                  (this._supported = !!('placeholder' in $('<input type="text">')[0]));
			},
			shim: function (opts) {
				var config = {
					color: '#b3b3b3',
					cls: 'placeholderText',
					selector: 'input[placeholder], textarea[placeholder]'
				};
				$.extend(config, opts);
				return !this.browser_supported() && $(config.selector)._placeholder_shim(config);
			}
		}
	});

	$.extend($.fn, {
		_placeholder_shim: function (config) {

			function calcPositionCss(target) {
				//var op = $(target).offsetParent().offset();
				//var ot = $(target).offset();

				return {
					//top: ot.top - op.top + 4,
					//left: ot.left - op.left + 2,
					width: $(target).width()
				};
			}

			function adjustToResizing(label) {
				var $target = label.data('target');
				if (typeof $target !== "undefined") {
					label.css(calcPositionCss($target));
					$(window).one("resize", function () { adjustToResizing(label); });
				}
			}

			return this.each(function () {
				var $this = $(this);

				if ($this.is(':visible')) {

					if ($this.data('placeholder')) {
						var $ol = $this.data('placeholder');
						$ol.css(calcPositionCss($this));
						return true;
					}

					$this.wrap("<div class='shim'></div>");

					var ol = $('<span />')
                           .text($this.attr('placeholder'))
                           .addClass(config.cls)
                           .attr('aria-labelledby', this.id)
                           .data('target', $this)
                           .click(function () {
                           	$(this).data('target').focus();
                           })
                           .insertBefore(this);
					$this
                           .data('placeholder', ol)
                           .focus(function () {
                           	ol.hide();
                           }).blur(function () {
                           	ol[$this.val().length ? 'hide' : 'show']();
                           }).triggerHandler('blur');
					$(window).one("resize", function () { adjustToResizing(ol); });
				}
			});
		}
	});
})(jQuery);

jQuery(document).add(window).bind('ready load', function () {
	if (jQuery.placeholder) {
		jQuery.placeholder.shim();
	}
});
