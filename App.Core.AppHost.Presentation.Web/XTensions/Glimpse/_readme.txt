﻿Glimpse provides a means to plugin IRuntimePolicy classes 
that are found by reflection.

An example is `GlimpseCustomizableAppSecurityPolicy`
which allows local machine, as well as requests from specific
machinenames, or the user is in a specific role.

