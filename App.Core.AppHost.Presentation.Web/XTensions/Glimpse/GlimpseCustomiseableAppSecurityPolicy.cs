﻿
using System;
using System.Linq;
using XAct;
using Glimpse.AspNet.Extensions;
using Glimpse.Core.Extensibility;


namespace App.Core.AppHost.XTensions.Glimpse
{
    public class GlimpseCustomizableAppSecurityPolicy : IRuntimePolicy
    {
        private static string[] prefixes;
        private static string[] roles;

        public RuntimePolicy Execute(IRuntimePolicyContext policyContext)
        {
            var httpContext = policyContext.GetHttpContext();

            if (httpContext == null)
            {
                return RuntimePolicy.Off;
            }

            if (prefixes == null)
            {
                var tmp = System.Configuration.ConfigurationManager.AppSettings["GlimpseAllowMachines"];
                if (tmp.IsNullOrEmpty())
                {
                    tmp = "__notset__";
                }
                prefixes = tmp.Split(';', ',', '|');
            }
            if (roles == null)
            {
                var tmp = System.Configuration.ConfigurationManager.AppSettings["GlimpseAllowRoles"];
                if (tmp.IsNullOrEmpty())
                {
                    tmp = "__notset__";
                }
                roles = tmp.Split(';', ',', '|');
            }

            //Sometimes we can get the remote machine name:
            string remoteHostName = httpContext.Request.UserHostName;

            if (remoteHostName == null)
            {
                remoteHostName = string.Empty;
            }
            if (
                httpContext.Request.IsLocal
                ||
                System.Environment.MachineName.StartsWith("STDMZ", StringComparison.InvariantCultureIgnoreCase)
                ||
                prefixes.Any(s => remoteHostName.StartsWith(s, StringComparison.InvariantCultureIgnoreCase))
                ||
                roles.Any(s => httpContext.User.IsInRole(s))
                )
            {
                return RuntimePolicy.On;
            }

            return RuntimePolicy.Off;
        }

        public RuntimeEvent ExecuteOn
        {
            get { return RuntimeEvent.EndRequest; }
        }
    }
}
