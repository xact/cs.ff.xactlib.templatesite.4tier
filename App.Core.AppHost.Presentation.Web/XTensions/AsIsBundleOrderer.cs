﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace App.Core.AppHost.XTensions
{
	public class AsIsBundleOrderer : IBundleOrderer
	{
		public IEnumerable<FileInfo> OrderFiles(BundleContext context, IEnumerable<FileInfo> files)
		{
			return files;
		}


		public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
		{
			return files;
		}
	}
}