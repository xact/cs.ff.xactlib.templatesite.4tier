define(function () {

    var enums = {};

    enums.gender = {
        undefined: 0,
        male: 1,
        female: 2,
        unknown: 3
    };

    enums.studentStatus = {
        undefined: 0,
        active: 1,
        inactive: 2,
        partial: 3
    };

    enums.studentStatusReason = {
        undefined: 0,
        inUse: 1,
        createdInError: 2,
        deceased: 3,
        doNotUse: 4,
        slave: 5
    };

    enums.nameBirthDateVerificationMethod = {
        undefined: 0,
        birthCertificate: 1,
        birthRegister: 2,
        other: 3,
        passport: 4,
        unverified: 5
    };

    enums.residentialStatus = {
        undefined: 0,
        australianCitizen: 1,
        citizen: 2,
        overseas: 3,
        permanentResident: 4,
        unknown: 5
    };

    enums.residentialStatusVerificationMethod = {
        undefined: 0,
        birthCertificate: 1,
        birthRegister: 2,
        other: 3,
        passport: 4,
        unverified: 5
    }; 

    enums.mergeRequestStatus = {
        requested: 1,
        approvedAutomatically: 2,
        approvedManually: 3,
        awaitingManualIntervention: 4,
        denied: 5,
        discarded: 6,
        rejectedAutomatically: 7
    };
    
    enums.mergeDenialReason = {
        undefinded:0,
        differentStudents: 1,
        noVerificationFromProvider: 2
    };

    enums.parentPage = {
    	undefinded: 0,
    	addStudent: 1,
    	editStudent: 2,
    };

    enums.batchStatus = {
		undefined: 0,
		uploaded: 1,
		processing: 2,
		processed: 3,
		error: 4
	};

    return enums;

});
