﻿define(['services/resourceService',
		'data/settings'],
	function (resourceService, settings) {

		createNamespace('com.nsi.frontend.data.routes');
		var routes = com.nsi.frontend.data.routes;

		// Property differentiations:
		// .name - Used for the navigation
		// .title - Used for the window title (otherwise it defaults to .name)
		// .breadcrumb - Used for the breadcrumb (otherwise it defaults to .name)


		// Home, Search, Merge, Admin, Reports, Batch
		routes.navRoutes = [
			// -- Basic pages
			{
				route: ['', 'home'], // Don't just use empty - '', or our registration process will exclude it
				moduleId: 'viewmodels/Home',
				name: 'Home',
				nav: false,
				settings: { area: 'one', isHome: true }
			},
			{
				route: 'welcome',
				moduleId: 'viewmodels/welcome',
				name: 'Home',
				nav: true,
				settings: { area: 'one', isHome: true, authorise: [] }
			},
			// ----------------
			// -- Student pages
			{
				route: 'students/search',
				moduleId: 'viewmodels/students/search',
				rootPath: 'students',
				name: 'Search',
				title: 'Student Search',
				nav: true,
				afterNavAction: "studentSearch:reset",
				settings: { area: 'Student', authorise: [settings.securityClaims.NSI2_SEARCH_VIEW] },
				childRoutes: [
						{
							route: 'students/view/:nsn',
							moduleId: 'viewmodels/students/viewStudent',
							name: 'Students Details',
							breadcrumb: 'Details',
							nav: false,
							settings: {
									area: 'Student',
									authorise: [settings.securityClaims.NSI2_SEARCH_VIEW]
								},
							childRoutes: [
								{
									route: 'students/history/:nsn',
									moduleId: 'viewmodels/students/studentHistory',
									name: 'Student History',
											breadcrumb: 'History',
									nav: false,
									settings: {
										area: 'Student',
										authorise: [settings.securityClaims.NSI2_SEARCH_VIEW]
									}
								},
								{
									route: 'students/studentProviderRelationships/:nsn',
									moduleId: 'viewmodels/students/studentProviderRelationshipsModalWindow',
									name: 'Student Provider Relationship',
									nav: false,
									settings: {
										area: 'Student',
										authorise: [settings.securityClaims.NSI2_SEARCH_VIEW]
									}
								},
								{
									route: 'students/challengeChange/:nsn',
									moduleId: 'viewmodels/students/challengeChange',
									name: 'Challenge',
									nav: false,
									settings: {
										area: 'Student',
										authorise: [settings.securityClaims.NSI2_CHALLENGE_STUDENT]
									}
								},
								{
									route: 'students/edit/:nsn',
									moduleId: 'viewmodels/students/editStudent',
									name: 'Modify Student',
									breadcrumb: 'Modify',
									nav: false,
									settings: {
										area: 'Student',
										authorise: [settings.securityClaims.NSI2_MODIFY_STUDENT]
									}
								}
							]
					},
					
					{
						route: 'students/add',
						moduleId: 'viewmodels/students/addStudent',
						name: 'Add Student',
						breadcrumb: 'Add',
			    		nav: false,
			    		settings: {
			    			area: 'Student',
							authorise: [settings.securityClaims.NSI2_ADD_STUDENT]
			    		}
					}
					
				]
			},
		
			// -------------
			// -- Merge pages
			{
				isDisplayShell: true,
				rootPath: 'merge',
				name: 'Merge',
				nav: true,
				settings: { area: 'Merge', authorise: [] },
				childRoutes: [
					{
						route: 'merge/list',
						moduleId: 'viewmodels/merge/mergeList',
						name: 'List',
						title: 'Merge List',
						breadcrumb: 'Merge List',
						nav: true,
						afterNavAction: "mergeList:reset",
						settings: {
							area: 'Merge',
							authorise: [settings.securityClaims.NSI2_MERGE_MANUAL_DECISION]
						},
						childRoutes: [
							{
								route: 'merge/decision/:id',
								moduleId: 'viewmodels/merge/mergeDecision',
								name: 'Merge Decision',
								nav: false,
								settings: {
									area: 'Merge',
									authorise: [settings.securityClaims.NSI2_MERGE_MANUAL_DECISION]
								},
								childRoutes: [
									{
										route: 'merge/history/:id',
										moduleId: 'viewmodels/merge/mergeHistory',
										name: 'Compare Student History',
										nav: false,
										settings: {
											area: 'Merge',
											authorise: [settings.securityClaims.NSI2_MERGE_MANUAL_DECISION]
						}
									}]
							}
						]
					},
					{
						route: 'merge/entry',
						moduleId: 'viewmodels/merge/mergeEntry',
						name: 'Entry',
						title: 'Merge Entry',
						breadcrumb: 'Merge Entry',
						nav: true,
						afterNavAction: "mergeRequest:reset",
						settings: {
							area: 'Merge',
							authorise: [settings.securityClaims.NSI2_MERGE_REQUEST]
						}
					},
					{
						route: 'merge/status',
						moduleId: 'viewmodels/merge/mergeStatus',
						name: 'Status',
						title: 'Merge Request Status',
						breadcrumb: 'Merge Status',
						nav: true,
						afterNavAction: "mergeStatus:reset",
						settings: {
							area: 'Merge',
							authorise: [settings.securityClaims.NSI2_MERGE_REQUEST, settings.securityClaims.NSI2_MERGESTATUS_ALL_PROVIDERS]
						}
					}]
			},
			
			// --------------
			// -- Admin pages
			{
				isDisplayShell: true,
				rootPath: 'admin',
				name: 'Admin',
				nav: true,
				settings: {
					area: 'one',
					authorise: [settings.securityClaims.NSI2_PROVIDER]
				}, // TODO: Should I allow the ability to have 'or' auth check? Or check the children instead?
				childRoutes: [
					{
					route: 'admin/providers',
					moduleId: 'viewmodels/admin/providerList',
						name: 'Providers',
						breadcrumb: 'Provider List',
						title: 'Provider List',
						nav: true,
						afterNavAction: "providerList:reset",
						settings: {
							area: 'Providers',
							authorise: [settings.securityClaims.NSI2_PROVIDER]
						}, childRoutes: [
							{
								route: 'admin/provider/details/:id',
								moduleId: 'viewmodels/admin/providerDetails',
								name: 'Provider Details',
								nav: false,
								settings: { area: 'Providers', authorise: [settings.securityClaims.NSI2_PROVIDER] }
							}
						]
					},
					{
					    route: 'admin/parameters',
					    moduleId: 'viewmodels/admin/parameterList',
					    name: 'Parameters',
					    breadcrumb: 'Parameter List',
					    title: 'Parameter List',
					    nav: true,
					    afterNavAction: "parameterList:reset",
					    settings: {
					        area: 'Parameters',
					        authorise: [settings.securityClaims.NSI2_PARAMETER]
					    }, childRoutes: [
							{
							    route: 'admin/parameter/details',
							    moduleId: 'viewmodels/admin/parameterDetails',
							    name: 'Parameter Details',
							    nav: false,
							    settings: { area: 'Parameters', authorise: [settings.securityClaims.NSI2_PARAMETER] }
							}
					    ]
				}]
			},
			// ----------------
			// -- Reports pages
			{
				isDisplayShell: true,
				name: 'Reports',
				rootPath: 'reports',
				nav: true,
				settings: { area: 'one', authorise: [] },
				childRoutes: [
					{
						notYetImplemented: false, 
						route: 'reports/transactionActivityReport',
						moduleId: 'viewmodels/reports/transactionActivityReport',
						name: 'Transaction Activity',
						title: 'Transaction Activity Report',
						nav: true,
						afterNavAction: "transactionActivityReport:reset",
						settings: {
							area: 'Reports',
							authorise: [settings.securityClaims.NSI2_REPORTS]
						}
					},
					{
						notYetImplemented: false, // TODO: Remove before go-live
						route: 'admin/AdminReports',
						moduleId: 'viewmodels/reports/adminReports',
						name: 'Admin',
						title: 'Admin Reports',
						nav: true,
						afterNavAction: "",
						settings: {
							area: 'Reports',
							authorise: [settings.securityClaims.NSI2_ADMIN_REPORTS]
						}
					}]
			},
			
			// --------------
			// -- Batch pages
			{
				isDisplayShell: true,
				name: 'Batch',
				rootPath: 'batch',
				nav: true,
				settings: {
					area: 'one',
					authorise: []
				},
				childRoutes: [
					{
						route: 'batch/results*details',
						moduleId: 'viewmodels/batch/batchResults',
						name: 'Results',
						hash: '#batch/results',
						title: 'Batch Results',
						breadcrumb: 'Batch Results',
						nav: true,
						settings: {
							area: 'Batch',
							authorise: [settings.securityClaims.NSI2_BATCH]
						}
					},
					{
						route: 'batch/Requests',
						moduleId: 'viewmodels/batch/batchRequests',
						name: 'Requests',
						title: 'Batch Requests',
						breadcrumb: 'Batch Requests',
						nav: true,
						afterNavAction: "batchRequests:reset",
						settings: {
							area: 'Batch',
							authorise: [settings.securityClaims.NSI2_BATCH]
						}
					}
				]
			}];
	

		return routes;
	});