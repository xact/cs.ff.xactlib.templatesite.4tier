﻿/** -- NSI - specific validation
*/

// Note: This is loaded with RequireJS to give it access to the resource service.
// It doesn't return any accessable methods, but must be included by the pages using it.

// Note: RE RESOURCES: Currently, the load sequence won't allow the resource text for validation messages to be loaded.
// So - The implemententing view will still set the message themselves. The messages below are just for fall-back.

define(['knockout',
		'data/settings',
		'services/resourceService'],
	function (ko, settings, resourceService) {

		ko.validation.rules['validNsn'] = {
			// For MessageCode #003
			validator: function (val) {
				return val.trim().length < 11; // not longer than 10 digits
			},
			message: "Invalid NSN"
		};

		ko.validation.rules['validProviderCode'] = {
			// For MessageCode #060
			validator: function (val) {
				// Allow MoE provider code (-1)

				var moeCodeRegex = '^\\s*-1\\s*$'; // Allow MoE code, with possible spaces

				if (val.match(moeCodeRegex)) {
					return true;
				}

				var providerCodeRegex = '^\\s*[a-zA-Z0-9]{0,10}\\s*$'; // Alphaumeric, greater than 1, max 10 digits/letters

				return ko.validation.utils.isEmptyVal(val) || val.match(providerCodeRegex) !== null;
			}
		};

		ko.validation.rules['validProviderName'] = {
			// For MessageCode #060
			validator: function (val) {



			}
		};

		// Unused - Message #931 is now deprecated. It may get reinstantiated, so keeping this here for now. - GB
		//ko.validation.rules["startWithValidLetters"] = {
		//	// For Messagecode #931
		//	validator: function (val) {
		//		var unicodeWord = XRegExp('^[a-zA-Z~\'\\p{L}]');
		//		return unicodeWord.test(val);
		//	},
		//	message: "Names must start with an apostrophe, tilde or an alphabetic character"
		//};


		var isValidSingleWord = function(val, params) {

			// Don't validate empty strings
			if (!val) {
				return true;
			}

			val = val.trim();

			var allowSingleTildeRegex = XRegExp('^\\s*~\\s*$');

			if (params.allowSingleTilde && allowSingleTildeRegex.test(val)) {
				return true;
			}
			
			if (
				// Check for concurrent 'special characters'. Note: mixed special characters are allowed
				val.indexOf("--") > 0
				|| val.indexOf("  ") > 0
				|| val.indexOf("''") > -1
				//allow string to start with an apostrophe, but cannot have more than one or contain only an apostrophe 
				|| val == "'"
				|| val == "-") {
				return false;
			}

			//Removed the unicode part \\p{L} from the accepted regex expression for now. The Diacritics will be added back in the future.
			// Allowed characters: (unicode) alphabetic characters, space, hyphen
			var allowedCharacters = XRegExp('^\\s*[a-zA-Z \s\'\-]*\\s*$');
			var allowedCharactersWithTilde = XRegExp('^\\s*[a-zA-Z ~\s\'\-]*\\s*$');

			var hasAllowedCharacters = params.allowTilde ? allowedCharactersWithTilde.test(val) : allowedCharacters.test(val);
			if (!hasAllowedCharacters) {
				return false;
			}

			// Must start with an apostrophe, alphabetic character or a space
			var allowedStartCharacters = XRegExp('^[a-zA-Z~\']');
			if (!allowedStartCharacters.test(val)) {
				return false;
			}
			
			// All rules passed, is value
			return true;
		}

		/* Params:
		    - allowTilde		- Allows a tilde to occur in the string. Used for search strings (student/merge list)
			- allowSingleTilde	- Allows the field to have a single '~'. Used for givenName1
		*/
	    //This validation is moved into the 'lostfocus' event rather 'keyin' event
		ko.validation.rules["containsValidCharacters"] = {
			validator: function (val, params) {
				// For MessageCode #272

				if (params.allowSingleTilde) {
					return isValidSingleWord(val, params);
				}

				// Split the word by spaces. Evaluate each component
				var res = val.split(" ");
				for (var i = 0; i < res.length; i++) {
					//if (i == 0 && params.) {

					//}

					if (!isValidSingleWord(res[i], params)) {
						return false;
					}
				}

				// Success case - All above rules passed
				return true;

			},
			message: "Field contains invalid characters"
		};

		ko.validation.rules["containsValidCharactersForOneSingleName"] = {
			validator: function (val) {

				var res = val.split(" ");
				for (var i = 0; i < res.length; i++) {
					//if (i == 0 && params.) {

					//}

					var params = {
						allowSingleTilde: true
					};

					if (!isValidSingleWord(res[i], params)) {
						return false;
					}
				}

				// Success case - All above rules passed
				return true;
			},
			message: "Field contains invalid characters"
		};

		ko.validation.rules["notJustAllowedSymbols"] = {
			validator: function (val) {
				if (!val) {
					return true;
				}

				// Returns true for strings that *only* contain hyphen and apostrophe characters (and spaces)
				var onlyContainsAllowSymbols = new RegExp(/^[-' ]+$/);

				return !onlyContainsAllowSymbols.test(val);
			},
			message: "Hyphen and apostrophe cannot be used without another character."
		};

		/** -- General validation
		*/
		/*https://github.com/moment/moment/issues/391
        As per moment.js, the isvalid() function just checks whether the date exists or not.
        So, formatting issue will be done thru regex.        
	    ToDO: simplify this regex and proper variable names to validate with 1 regex for all formats..
        */
		ko.validation.rules['validNzDate'] = {
			validator: function (val) {
				var acceptedFormats1 = /^\d{1,2}\/\d{1,2}\/(?:\d{4}|\d{2})$/;
				var acceptedFormats2 = /^\d{1,2} \d{1,2} (?:\d{4}|\d{2})$/;
				var acceptedFormats3 = /^\d{1,2} [a-zA-Z]{3} (?:\d{4}|\d{2})$/;
				var result = ko.validation.utils.isEmptyVal(val) || val.match(acceptedFormats1) != null || val.match(acceptedFormats2) !== null || val.match(acceptedFormats3) != null;
				return result;

			},
			message: 'Please enter the date in the following format dd/mm/yyyy'
		};

		ko.validation.rules['nzDate'] = {
			validator: function (val) {
				var parsedDate = moment(val, settings.momentAcceptedDateInputs, true);
				return parsedDate.isValid();
			},
			message: 'Given date is not a valid date'
		};

		ko.validation.rules['maxDate'] = {
			validator: function (val, maxVal) {
				var maxDate = ko.utils.unwrapObservable(maxVal);

				maxDate = moment(maxDate);
				var valDate = moment(val);

				if (!valDate.isValid()
						|| !maxDate.isValid()) {
					return false;
				}

				return valDate.isBefore(maxDate);
			},
			message: 'The date field must be before {0}'
		};

		//ko.validation.rules['minDate'] = {
		//	validator: function (val, maxVal) {
		//		var maxDate = ko.utils.unwrapObservable(maxVal);

		//		maxDate = moment(maxDate);
		//		var valDate = moment(val);

		//		if (!valDate.isValid()
		//				|| !maxDate.isValid()) {
		//			return false;
		//		}

		//		return valDate.isBefore(maxDate);
		//	},
		//	message: 'The date field must be before {0}'
		//};

		ko.validation.rules['nzDateTime'] = {
			validator: function (val) {
				// Ignore empty values - a separate validation
				if (!val) {
					return true;
				}
				var parsedDate = moment(val, settings.momentAcceptedDateTimeInputs, true);
				return parsedDate.isValid();
			},
			message: 'Given date time is not a valid date'
		};

		ko.validation.rules['validNzDateTime'] = {
			// Ignore empty values - a separate validation
			validator: function (val) {
				// Ignore empty values - a separate validation
				if (!val) {
					return true;
				}
				var acceptedFormats1 = /^\d{4}\-\d{1,2}\-\d{1,2} \d{1,2}\:\d{1,2}$/;
				var result = ko.validation.utils.isEmptyVal(val) || val.match(acceptedFormats1) != null;
				return result;
			},
			message: 'Date time field must be in the following format: yyyy-mm-dd hh:mm'
		};

		// For MessageCode #087
		ko.validation.rules['validURL'] = {
			// Ignore empty values - a separate validation
			validator: function (val) {
				if (!val) {
					return true;
				}
				//var validUrlRegex = '^(http:\/\/|https:\/\/|www\.)?[a-zA-Z0-9-\.\/]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$';
				var validUrlRegex = '^(http:\/\/|https:\/\/)+([a-zA-Z0-9\.\=\%\?\\\#\_\&\|\/\-\:\;\{\}\$\*\(\)\!\@\^\+\<\>\-])*$';
				return ko.validation.utils.isEmptyVal(val) || val.match(validUrlRegex) !== null;
			}
		};

		ko.validation.rules['maxDateIsToday'] = {
			validator: function (val, maxVal) {
				// Ignore empty values - a separate validation
				if (!val) {
					return true;
				}

				//var maxDate = moment(new Date());
				var maxDate = moment();

				//var inputDate = moment(val);
				var inputDate = moment(val, settings.momentAcceptedDateInputs, true);

				// Ignore invalid dates - they're a separate valdiation
				if (!inputDate.isValid()) {
					return true;
				}

				return maxDate.isAfter(inputDate, 'Date');
			},
			message: 'The date field must be before {0}'
		};

		ko.validation.rules['dateCannotBeToday'] = {
			validator: function (val) {
				// Ignore empty values - a separate validation
				if (!val) {
					return true;
				}
				var now = moment();
				var inputDate = moment(val, settings.momentAcceptedDateInputs, true);

				// Ignore invalid dates - they're a separate valdiation
				if (!inputDate.isValid()) {
					return true;
				}
				return now.diff(inputDate, 'days') > 0;
			},
			message: 'Birth date cannot be today\'s date'
		};

		ko.validation.rules['minDateIsToday'] = {
			validator: function (val, minVal) {
				// Ignore empty values - a separate validation
				if (!val) {
					return true;
				}

				var today = moment().hours(0).minutes(0).seconds(0).milliseconds(0).subtract(1, 'ms');
				//.day(moment().day()-1).hours(23).minutes(59).seconds(59).milliseconds(999);

				var momentAcceptedDateInputs = ['D/M/YY', 'DD/M/YY', 'D/MM/YY', 'DD/MM/YY', 'D/M/YYYY', 'DD/M/YYYY', 'D/MM/YYYY', 'DD/MM/YYYY', 'D MM YY', 'DD MM YY', 'D M YY', 'DD M YY', 'D MM YYYY', 'DD MM YYYY', 'D M YYYY', 'DD M YYYY', 'D MMM YY', 'DD MMM YY', 'D MMM YYYY', 'DD MMM YYYY'];
				var inputDate = moment(val, momentAcceptedDateInputs, true);

				// Ignore invalid dates - they're a separate valdiation
				if (!inputDate.isValid()) {
					return true;
				}

				return today.isBefore(inputDate, 'Date');
			},
			message: 'NSI live date can never be in the past'
		};

		ko.validation.rules['dateRangeGreaterThan'] = {
			validator: function (val, params) {
				var fromDateVal = ko.validation.utils.getValue(params),
					fromDate = ko.isObservable(fromDateVal) ? fromDateVal() : fromDateVal,
					parsedToDate = moment(val, settings.momentAcceptedDateInputs, true),
					parsedFromDate = moment(fromDate, settings.momentAcceptedDateInputs, true),
					diff = parsedToDate.diff(parsedFromDate, 'month', true);

				return diff < 6;
			},
			message: 'Date range greater than 6 months. Please check entered date range.'
		};

		ko.validation.rules['dateRangeGreaterThanPast'] = {
			validator: function (val, params) {
				var toDateVal = ko.validation.utils.getValue(params),
					toDate = ko.isObservable(toDateVal) ? toDateVal() : toDateVal,
					parsedFromDate = moment(val, settings.momentAcceptedDateInputs, true),
					parsedToDate = moment(toDate, settings.momentAcceptedDateInputs, true),
					diff = parsedToDate.diff(parsedFromDate, 'month', true);
				return diff < 6.05; //moment returns differences in minor decimals while subtracting dates
			},
			message: 'Date range greater than 6 months. Please check entered date range.'
		};

		ko.validation.rules['dateCannotEarlierThan'] = {
			validator: function (val, params) {
				var fromDateVal = ko.validation.utils.getValue(params),
					fromDate = ko.isObservable(fromDateVal) ? fromDateVal() : fromDateVal,
					parsedToDate = moment(val, settings.momentAcceptedDateInputs, true),
					parsedFromDate = moment(fromDate, settings.momentAcceptedDateInputs, true),
					diff = parsedToDate.diff(parsedFromDate, 'month', true);

				return diff >= 0;
			},
			message: '{0} cannot be earlier than {1}.'
		};

		ko.validation.rules['dateisBefore'] = {
			validator: function (val, params) {
				var toDateVal = ko.validation.utils.getValue(params),
		            toDate = ko.isObservable(toDateVal) ? toDateVal() : toDateVal,
		            parsedFromDate = moment(val, settings.momentAcceptedDateInputs, true),
		            parsedToDate = moment(toDate, settings.momentAcceptedDateInputs, true),
		            diff = parsedFromDate.isBefore(parsedToDate, 'Days');

				return !diff;
			},
			message: '{0} cannot be earlier than {1}.'
		};

		ko.validation.rules['dateCannotBeLaterThan'] = {
			validator: function (val, params) {
				var fromDateVal = ko.validation.utils.getValue(params),
					fromDate = ko.isObservable(fromDateVal) ? fromDateVal() : fromDateVal,
					parsedToDate = moment(val, settings.momentAcceptedDateInputs, true),
					parsedFromDate = moment(fromDate, settings.momentAcceptedDateInputs, true),
					diff = parsedToDate.diff(parsedFromDate, 'month', true);

				return diff <= 0;
			},
			message: '{0} cannot be later than {1}.'
		};

		ko.validation.rules['isNumeric'] = {
			validator: function (val) {
				var isNumericRegex = '^[0-9]{1,10}$';

				var testCase = typeof val === 'string' ? val : val.toString(); // Ensure the input is a string

				return ko.validation.utils.isEmptyVal(testCase) || testCase.match(isNumericRegex) != null;
			},
			message: "Must be numeric"
		};

		ko.validation.rules['isDecimalNumeric'] = {
		    validator: function (val) {
		        var isDecimalNumericRegex = '^[0-9]+(?:\.[0-9]{1,5})?$';

		        var testCase = typeof val === 'string' ? val : val.toString(); // Ensure the input is a string

		        var s = ko.validation.utils.isEmptyVal(testCase) || testCase.match(isDecimalNumericRegex) != null;

		        return s;
		    },
		    message: "Must be decimal numeric"
		};

		ko.validation.rules['pattern2'] = {
			validator: function (val, regex) {
				return utils.isEmptyVal(val) || val.match(regex) != null;
			},
			message: 'Please check this value.'
		};

		ko.extenders['validation2'] = function (observable, rules) { // allow single rule or array
			ko.utils.arrayForEach(utils.isArray(rules) ? rules : [rules], function (rule) {
				// the 'rule' being passed in here has no name to identify a core Rule,
				// so we add it as an anonymous rule
				// If the developer is wanting to use a core Rule, but use a different message see the 'addExtender' logic for examples
				ko.validation.addAnonymousRule(observable, rule);
			});
			return observable;
		};

		ko.validation.rules['validEmailAddress'] = {
			// For MessageCode #004
			validator: function (val) {

				var validEmailRegex = '^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$'; // Common Regex for Email Validation

				return ko.validation.utils.isEmptyVal(val) || val.match(validEmailRegex) !== null;
			}
		};

		ko.validation.rules['noLeadingOrTrailingSpaces'] = {
			// For MessageCode #946
			validator: function (val) {

				var spacesRegex = '/^\s+|\s+$/g'; // 

				return ko.validation.utils.isEmptyVal(val) || val.match(spacesRegex) !== null;
			}
		};

		ko.validation.rules['containsMoreThanSpaces'] = {
			//For required fields containing only spaces
			validator: function (val) {
				return val.trim() != "";
			},
			message: "Required"
		};
		// This class only exists to load validation rules in the RequireJS pattern, but must return something

		return ko.validation.rules;

	});
