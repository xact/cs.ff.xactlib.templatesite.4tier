define(function (system) {

    createNamespace('com.nsi.frontend.data.settings');
    var settings = com.nsi.frontend.data.settings;
    //Define early:
    settings.urls = {};


    //Producer is Developer is Vendor
    settings.producerInfo = {
        displayName: '[TODO:PRODUCERDISPLAYNAME]',
        displayTagLine: '[TODO:PRODUCERDISPLAYTAGLINE]',
        termsAndConditionsUrl: '[TODO]'
        //Note: copyrights are a product (not company) attribute.
    };

    //Client is whom Vendor writes code for.
    //If developing inhouse, can be one and the same. 
    settings.clientInfo = {
        displayName: 'Ministry of Education',
        displayTagLine: 'Te Tahuhu o te Matauranga'
        //Note: copyrights are a product (not company) attribute.
    };

    settings.productInfo = {
        uniqueName: 'NSI',
        version: '0.1', //referenced by resourceService
        build: '0.1.0.1', //referenced by resourceService

        displayName: 'National Student Index',//note: ref'ed from resources.js.
        displayTagLine: 'Everybody gets a number...', //note: ref'ed from resources.js.
        copyrightCompany: 'Copyright Ministry of Education', //note: ref'ed from resources.js.
        copyrightYear: '2013' //note: ref'ed from resources.js.
    }; //The subdirectory under images, where the logo, etc. is kept.
    settings.identity = "app";

    //Dateformat settings for datepicker 
    settings.defaultDateFormat = "d M yy";
    settings.momentAcceptedDateInputs = ['D/M/YY', 'DD/M/YY', 'D/MM/YY', 'DD/MM/YY', 'D/M/YYYY', 'DD/M/YYYY', 'D/MM/YYYY', 'DD/MM/YYYY', 'D MM YY', 'DD MM YY', 'D M YY', 'DD M YY', 'D MM YYYY', 'DD MM YYYY', 'D M YYYY', 'DD M YYYY', 'D MMM YY', 'DD MMM YY', 'D MMM YYYY', 'DD MMM YYYY'];
    settings.momentAcceptedDateTimeInputs = ['YYYY-MM-DD HH:mm'];
    settings.dobRangeMin = 5;
    settings.dobRangeMax = 90;

    //Default Gridview page length
    settings.defaultGridviewLength = 20;
    settings.defaultShortGridviewLength = 4;
    settings.defaultHistoryGridviewLength = 14;

    settings.iqOfficeMatchHighlightThreshold = 80;
//    settings.IQDefiniteMatchThreshold = 92;
//    settings.iqPossibleMatchThreshold = 60; 

    settings.debugLevel = 4; //1-Errors, 4=Verbose
    //Used to offset environmentService.getDateUtc()
    settings.dateOffset = null;


    //tracing
    settings.stripTracingForRelease = false;

    // General configuration
    settings.oneMinute = 60 * 1000;
    settings.standardFadeDuration = 4000;
    settings.longAjaxTimeout = settings.oneMinute;
    settings.pageSize = 20;
    settings.shortMaskDelay = 20;
    //used by basePlatform.js to show Sencha mask for x seconds.
    settings.longMaskDelay = 100;
    settings.statusInterrogationPeriod = settings.oneMinute;
    settings.activityTimeOut = 4 * settings.oneMinute;


    settings.hideAnimation = null;
    settings.showAnimation = 'slideIn'; //fade, fadeOut,flip,pop,popOut,slide,slideOut

    //Now that defaults are written, we can crush any var with one 
    //defined above in environmentVariables:
    //Depending on environment, set urls:


    settings.allEnvironmentVars = {
        demo: {
        	identity: "moe",
        	host: 'localhost',
            urls: {
                serverBaseUrl: '/',
                clientStylesUrl: 'content/',
                clientScriptsUrl: 'styles/',
                authenticationServiceUrl: '/data/authentication.json'
            },
            enforceSSL: false,
        	debugLevel: 4
        },
        dev: {
            host: 'localhost',
            urls: {
	             serverBaseUrl: 'http://localhost:10000/website/'
            },
            enforceSSL: false,
            debugLevel: 4
        },
        // Fake FQDN is DNSd in wireless network for SIT
        sit: { enforceSSL: false, debugLevel: 3 },
        qa: { enforceSSL: true, debugLevel: 3 },
        prod: { enforceSSL: true, debugLevel: 3 }
    };






    //set the context
    //use that to set the environmentVariables array:
    //settings.activeConfiguration = fa(Ext.os.is.Desktop)?'dev':'release'; 
    settings.activeConfiguration = 'demo';
    settings.environmentVariables = settings.allEnvironmentVars[settings.activeConfiguration];

    //Build up any additional urls in template, based on baseUrl:
    //Do this before 


    //transfer any var defined in environmentVariables to the base object
    //crushing any defined.
    //TODO: Have to make this recursive one day...
    for (var x in settings.environmentVariables[settings.activeConfiguration]) {
        settings[x] = settings.environmentVariables[x];
    }


    //Set more variables, but only if not yet set by environmentVariables.
    // 'app/'
    settings.urls.clientBaseUrl = (settings.urls.clientBaseUrl !== undefined) ? settings.urls.clientBaseUrl : '';
    // 'app/resources/'
    settings.urls.clientResourceUrl = (settings.urls.clientResourceUrl !== undefined) ? settings.urls.clientResourceUrl : settings.urls.clientBaseUrl + 'content/';
    // 'app/resources/scripts/'
    settings.urls.clientScriptUrl = (settings.urls.clientScriptUrl !== undefined) ? settings.urls.clientScriptUrl : settings.urls.clientScriptUrl + 'scripts/';
    // 'app/resources/scripts/'
    settings.urls.clientStylesUrl = (settings.urls.clientStylesUrl !== undefined) ? settings.urls.clientStylesUrl : settings.urls.clientStylesUrl + 'styles/';
    // 'app/resources/images/'
    settings.urls.clientImageResourceUrl = (settings.urls.clientImageResourceUrl !== undefined) ? settings.urls.clientImageResourceUrl : settings.urls.clientResourceUrl + 'images/';

    settings.urls.clientIdentityImageResourceUrl = (settings.urls.clientIdentityImageResourceUrl !== undefined) ? settings.urls.clientIdentityImageResourceUrl : settings.urls.clientImageResourceUrl + settings.identity;

    // 'app/data/'
    settings.urls.clientDataUrl = (settings.urls.clientDataUrl !== undefined) ? settings.urls.clientDataUrl : settings.urls.clientResourceUrl + 'data/';
    // 'app/data/templates/'
    settings.urls.clientDataTemplateUrl = (settings.urls.clientDataTemplateUrl !== undefined) ? settings.urls.clientDataTemplateUrl : settings.urls.clientResourceUrl + 'templates/';


    settings.serverBaseUrl = (settings.serverBaseUrl !== undefined) ? settings.serverBaseUrl : 'http://NOTSET-IN-SETTINGS/';
    settings.serverServiceBaseUrl = (settings.serverServiceBaseUrl !== undefined) ? settings.serverBaseUrl : 'api/';
    settings.serverAuthenticationServiceUrl = (settings.serverAuthenticationServiceUrl !== undefined) ? settings.serverAuthenticationServiceUrl : settings.urls.clientBaseServiceUrl + '/authentication/NOTSET-IN-SETTINGS/';
    settings.serverSettingsServiceUrl = (settings.serverSettingsServiceUrl !== undefined) ? settings.serverSettingsServiceUrl : settings.serverBaseUrl + 'settings/NOTSET-IN-SETTINGS/';
    settings.serverFeedbackServiceUrl = (settings.serverFeedbackServiceUrl !== undefined) ? settings.serverFeedbackServiceUrl : settings.serverBaseUrl + 'feedback/NOTSET-IN-SETTINGS/';

    settings.footerLinks = [
	{ title: 'Home', target: '/#' },
	{ title: 'About this site', target: '#More/ContactUs' },
	{ title: 'Accessibility', target: '#More/Accessibility' },
	{ title: 'SiteMap', target: '#More/SiteMap' },
	{ title: 'Legal & Privacy', target: '#More/LegalAnPrivacy' },
	{ title: 'Feedback', target: '#More/Feedback' },
	{ title: 'Contact Us', target: '#More/Accessibility' }
    ];

    settings.publicWelcomeLinks = [
		{
		    title: 'Information and resources for NSI Users',
		    target: 'http://www.minedu.govt.nz/NZEducation/EducationPolicies/TertiaryEducation/ForTertiaryEducationInstitutions/NationalStudentIndex/InformationForUsers.aspx'
		},
		{ title: 'A student\'s & parent\'s guide to the NSI', target: 'http://www.minedu.govt.nz/educationSectors/Schools/SchoolOperations/NationalStudentNumber/InformationForParentsAndStudents/GuideForStudentsAndParents.aspx' },
		{ title: 'Request personal information held about you on the NSI', target: 'http://nsi.education.govt.nz/survey/moe2.asp' },
		{ title: 'NSI Authorised Information Matching Programme', target: 'http://www.minedu.govt.nz/NZEducation/EducationPolicies/TertiaryEducation/ForTertiaryEducationInstitutions/NationalStudentIndex/AuthorisedInfoMatchingProg.aspx' },
		{ title: 'Provider Guide for NSN Records', target: 'http://www.minedu.govt.nz/NZEducation/EducationPolicies/TertiaryEducation/ForTertiaryEducationInstitutions/NationalStudentIndex/CurrentDevelopments.aspx' }
    ];

	settings.providers = {
		moeId: 1
	};

	// For details on the permissions, see the document 'NSI Replacement privileges'

    settings.securityClaims = {
        NSI2_SEARCH_VIEW: "NSI2_SEARCH_VIEW",
        NSI2_VIEW_SPR: "NSI2_VIEW_SPR",
        NSI2_ROLLBACK_STUDENT: "NSI2_ROLLBACK_STUDENT",
        NSI2_CHALLENGE_STUDENT: "NSI2_CHALLENGE_STUDENT",
        NSI2_ADD_STUDENT: "NSI2_ADD_STUDENT",
        NSI2_ADD_DUPLICATE: "NSI2_ADD_DUPLICATE",

        NSI2_MODIFY_STUDENT: "NSI2_MODIFY_STUDENT",
        NSI2_MODIFY_ALL_BIRTH_DATES: "NSI2_MODIFY_ALL_BIRTH_DATES",
        NSI2_MODIFY_STUDENT_STATUS: "NSI2_MODIFY_STUDENT_STATUS",
        NSI2_MODIFY_DATE_OF_DEATH: "NSI2_MODIFY_DATE_OF_DEATH",
        NSI2_MODIFY_VERIFIED: "NSI2_MODIFY_VERIFIED",
    	//NSI2_MODIFY_FORCE_MOE: "NSI2_MODIFY_FORCE_MOE",
        NSI2_MODIFY_ALT_NAME_CREATED: "NSI2_MODIFY_ALT_NAME_CREATED",
        NSI2_MODIFY_INACTIVE_STUDENT: "NSI2_MODIFY_INACTIVE_STUDENT",
        NSI2_MODIFY_CHALLENGE_STATUS: "NSI2_MODIFY_CHALLENGE_STATUS",
        NSI2_MODIFY_BDM_VERIFIED: "NSI2_MODIFY_BDM_VERIFIED",
        NSI2_DELETE_ALT_NAME: "NSI2_DELETE_ALT_NAME",

        NSI2_MERGE_REQUEST: "NSI2_MERGE_REQUEST",
        NSI2_MERGE_DENY: "NSI2_MERGE_DENY",
        NSI2_MERGE_MANUAL_DECISION: "NSI2_MERGE_MANUAL_DECISION",
        NSI2_MERGE_REMOVE_DENIED: "NSI2_MERGE_REMOVE_DENIED",
        NSI2_MERGE_UNMERGE: "NSI2_MERGE_UNMERGE",

        NSI2_PROVIDER: "NSI2_PROVIDER",
        NSI2_PARAMETER: "NSI2_PARAMETER",
        NSI2_EDIT_PARAMETER: "NSI2_EDIT_PARAMETER",
        NSI2_REPORTS: "NSI2_REPORTS",
        NSI2_ADMIN_REPORTS: "NSI2_ADMIN_REPORTS",
        NSI2_BATCH: "NSI2_BATCH",
        NSI2_UPDATE_STUDENT_PROVIDER_RELATIONSHIP: "NSI2_UPDATE_STUDENT_PROVIDER_RELATIONSHIP",
        NSI2_CHANGE_NOTIFICATIONS: "NSI2_CHANGE_NOTIFICATIONS",
        NSI2_MERGESTATUS_ALL_PROVIDERS: "NSI2_MERGESTATUS_ALL_PROVIDERS"
    };

    return settings;

});