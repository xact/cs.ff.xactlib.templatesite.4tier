﻿define([],
	function () {

		createNamespace('com.nsi.frontend.data.referenceData');

		var referenceData = com.nsi.frontend.data.referenceData;

		referenceData.genders =
			[
				{ Text: "", Code: "" },
				{ Text: "Male", Code: "M" },
				{ Text: "Female", Code: "F" },
				{ Text: "Unknown", Code: "U" }
			];

		referenceData.residentialStatuses =
			[
				{ Text: "", Code: "" },
				{ Text: "Australian citizen", Code: "A" },
				{ Text: "Citizen", Code: "C" },
				{ Text: "Overseas", Code: "O" },
				{ Text: "Permanent resident", Code: "P" },
				{ Text: "Unknown", Code: "U" }
			];

		return referenceData;
	}
);