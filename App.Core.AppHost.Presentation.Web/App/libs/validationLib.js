﻿define(['data/settings'],
	function(settings) {

		var lib = {};
		
		// Tests if the birthdate is within sensible boundries
		lib.isSaneBirthdate = function (val) {
			
			var parsedDate = moment(val, settings.momentAcceptedDateInputs, true);

			// If it's not a valid date, don't fail it for the reason of not being within this 'sane date' boundary.
			// -> Let the consumer figure that out invalid dates with a separate check of their own
			if (!parsedDate.isValid()) {
				return true;
			}

			var now = moment();
			var age = now.diff(val, 'years', true);
			return age > settings.dobRangeMin && age < settings.dobRangeMax;
		};

		/* Returns true if the NSI Response object contains a given message code
		*/
		lib.hasMessageCode = function(response, messageCode) {

			var hasCode = ko.utils.arrayFirst(response.Messages, function (item) {
				return item.MessageCode.Id === messageCode;
			});

			return hasCode;
		};

		/* Returns an array of all the message text in a given NSI Response object
		*/
		lib.getAllMessagesAsText = function(response) {

			// Fish out all the 'message text'
			
			var result = ko.utils.arrayMap(response.Messages, function (item) {
				return item.PresentationAttributes.Text;
			});

			return result;
		};

		return lib;

	});