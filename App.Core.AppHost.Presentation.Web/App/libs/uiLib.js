﻿define(['services/resourceService'],
	function (resourceService) {

		var uiLib = {};

		// Apply common name validation to a given field
		uiLib.setCommonNameValidation = function (field, fieldName, params) { //, conditions) { // TODO: Required? For Mena.

			if (!field) {
				return;
			}

			var containsValidCharactersParams = {};
			if (params && params.allowSingleTilde) {
				containsValidCharactersParams.allowSingleTilde = params.allowSingleTilde;
			}

			field.extend({
				maxLength: 100,
				containsValidCharacters: {
					message: resourceService.get("MessageCode_Validation_InvalidFormat_272").format(fieldName),
					onlyIf: function () { return field(); }, // && conditions; } // TODO: Required? For Mena.
					params: containsValidCharactersParams
				}
			    //Message 278 should not be validated for name fields;  call from alphanumeric validation
				//,
				//notJustAllowedSymbols: {
				//	message: resourceService.get("MessageCode_Validation_InvalidHyphenApostrophe_278"),
				//	onlyIf: function () { return field(); } // && conditions; } // TODO: Required? For Mena.
				//}
			});
		};

		// Apply common date validation to a given field
		uiLib.setCommonBirthDateValidation = function (field, fieldName) { //, conditions) { // TODO: Required? For Mena.
			if (!field) {
				return;
			}

			field.extend({
			    validNzDate: {
			        //message: 'Birth date is not in a valid format',
			        message: resourceService.get("MessageCode_Validation_Date_001").format(fieldName),
			        onlyIf: function () { return field(); } // Only validate when populated
			    },
				nzDate: {
					//message: 'Birth date is not a valid date',
					message: resourceService.get("MessageCode_Validation_InvalidDate_305").format(fieldName),
					onlyIf: function () { return field(); } // Only validate when populated
				},
				maxDateIsToday: {
					//message: "Birth date cannot be later than current date",
					message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format(fieldName),
					onlyIf: function () { return field(); } // Only validate when populated
				}
			});
		};

		uiLib.notValidCssForArray = function (bountItems) {
			var count = 0;
			ko.utils.arrayForEach(bountItems, function (bountItem) {
				if (!bountItem.isValid()) {
					count++;
				}
			});
// ReSharper disable ConditionIsAlwaysConst
			return count == 0 ? 'normalColourLabel' : 'errorLabel';
// ReSharper restore ConditionIsAlwaysConst
		};

		uiLib.setTildeRuleValidation = function (student) {
			if (student.GivenName2) {
				student.GivenName2.extend({
					validation: [
						{
							// If GN1 has a tilde, GN2 can't contain content
							validator: function () {
								var isInvalid = student.GivenName1() === "~" && student.GivenName2();

								return !isInvalid;
							},
							message: resourceService.get("MessageCode_Validation_InvalidFormat_272").format("Given name 2")
						}
					]
				});
			}

			if (student.GivenName3) {
				student.GivenName3.extend({
					validation: [
						{
							// If GN1 has a tilde, GN3 can't contain content
							validator: function () {
								var isInvalid = student.GivenName1() === "~" && student.GivenName3();

								return !isInvalid;
							},
							message: resourceService.get("MessageCode_Validation_InvalidFormat_272").format("Given name 3")
						}
					]
				});
			}
		}




		return uiLib;
	});