﻿define([
		'durandal/app',
		'singletons/searchCriteria',
		'singletons/mergeRequest',
		'entities/providerEntity'
		],

	function (app, searchCriteria, mergeRequest,provider) {

		var self = this;
		self.beenActivated = ko.observable(false);



		self.triggers = [];

		self.init = function () {
			//For IE, console is not defined unless the developer tools is opened, so only run
			//it when it is defined.
			if (typeof (console) !== "undefined" && console.log !== undefined) {
				console.log("NavLib Running!");
			}

			self.triggers.push(
			app.on("studentSearch:reset").then(function () {
				searchCriteria.clear();
				uiHelper.blurAllInputs(); //For IE to reaload shim/placeholder text
				uiHelper.focusFirstInputElement(); //When a User clicks on the nav link of the page they are already on, its here since composition complete doesnt run again.
			}));

			self.triggers.push(
			app.on("mergeRequest:reset").then(function () {
				mergeRequest.clear();
				$("#numNSN").val('');
				uiHelper.blurAllInputs();
				uiHelper.focusFirstInputElement();
			}));


			self.triggers.push(
			app.on("providerList:reset").then(function () {
				//provider.clear();
				uiHelper.blurAllInputs(); 
				uiHelper.focusFirstInputElement();
			}));

			self.triggers.push(
			app.on("parameterList:reset").then(function () {
				//For IE to lose focus and so the current tab will have right colour
				$('.current a').blur();
			
			}));

			self.triggers.push(
			app.on("transactionActivityReport:reset").then(function () {
				//For IE to lose focus and so the current tab will have right colour
				//$('.current a').blur();

			}));

			self.triggers.push(
			app.on("batchRequests:reset").then(function () {
				//For IE to lose focus and so the current tab will have right colour
				//$('.current a').blur();

			}));

		};

		self.trigger = function (action) {
			app.trigger(action);
		};

		self.triggerAll = function () {
			$.each(self.triggers, function (index, subscription) {
				app.trigger(subscription.events);
			});
		};

		return self;
	});