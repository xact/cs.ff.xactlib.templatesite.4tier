﻿define(['durandal/app',
		'services/logger',
		'services/resourceService',
		'data/enums',
		'data/settings',
		'libs/validationLib',
		'singletons/parameters',
		'singletons/session'
],
	function (app, logger, resourceService, enums, settings, validationLib, parameters, session) {


		var self = {};

		self.resources = resourceService;

		self.attachExtraStudentValidation = function (student, checks) {

			student.NameBirthDateVerificationDateTime.extend({
				validNzDate: {
					message: resourceService.get("MessageCode_Validation_Date_001").format("Name & birth date verification date"),
					onlyIf: function () {
						return student.NameBirthDateVerificationDateTime()
								&& checks.canModifyNameDobVerification();
					}
				},
				nzDate: {
					message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Name & birth date verification date"),
					onlyIf: function () {
						return student.NameBirthDateVerificationDateTime()
									&& checks.canModifyNameDobVerification();
					}
				},
				maxDateIsToday: {
					//message: "Birth date cannot be later than current date",
					message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Name & birth date verification date"),
					onlyIf: function () {
						return student.NameBirthDateVerificationDateTime()
									&& checks.canModifyNameDobVerification();
					}
				},
				dateCannotEarlierThan: {
					params: student.CreatedOn(),
					message: self.resources.get("MessageCode_Validation_verificationDateEarlierThanCreation_935"),
					onlyIf: function () {
						return student.NameBirthDateVerificationDateTime()
							&& checks.canModifyNameDobVerification(); // TODO: I think this is the incorrect check?
					}
				}
			});

			student.ResidentialStatusVerificationDateTime.extend({
				validNzDate: {
					message: resourceService.get("MessageCode_Validation_Date_001").format("Residential status verification date"),
					onlyIf: function () {
						return student.ResidentialStatusVerificationDateTime()
								&& checks.canModifyResidentialStatusVerification();
					}
				},
				nzDate: {
					//message: 'Birth date is not a valid date',
					message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Residential status verification date"),
					onlyIf: function () {
						return student.ResidentialStatusVerificationDateTime()
								&& checks.canModifyResidentialStatusVerification();
					}
				},
				maxDateIsToday: {
					//message: "Birth date cannot be later than current date",
					message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Residential status verification date"),
					onlyIf: function () {
						return student.ResidentialStatusVerificationDateTime()
								&& checks.canModifyResidentialStatusVerification();
					}
				},
				dateCannotEarlierThan: {
					params: student.CreatedOn(),
					message: self.resources.get("MessageCode_Validation_verificationDateEarlierThanCreation_935"),
					onlyIf: function () {
						return student.ResidentialStatusVerificationDateTime()
							&& checks.canModifyResidentialStatusVerification(); // TODO: I think this is the incorrect check?
					}
				}
			});

			student.DateOfDeath.extend({
				validNzDate: {
					//message: 'Birth date is not a valid date',
					message: resourceService.get("MessageCode_Validation_Date_001").format("Date of death"),
					onlyIf: function () { return checks.isDeathDateVisible() && student.DateOfDeath(); } // Only validate when populated
				},
				nzDate: {
					//message: 'Birth date is not a valid date',
					message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Date of death"),
					onlyIf: function () { return checks.isDeathDateVisible() && student.DateOfDeath(); } // Only validate when populated
				},
				maxDateIsToday: {
					//message: "Birth date cannot be later than current date",
					message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Date of death"),
					onlyIf: function () { return checks.isDeathDateVisible() && student.DateOfDeath(); } // Only validate when populated
				},
				dateCannotEarlierThan: {
					params: student.BirthDate(),
					message: self.resources.get("MessageCode_Validation_DeathDateLaterThanBirthDate_306"),
					onlyIf: function () { return checks.isDeathDateVisible() && student.DateOfDeath(); }
				}
			});


		};

		self.hasChangedNameAndDob = function (originalStudent, currentStudent) {
			if (originalStudent.BirthDate() === currentStudent.BirthDate()) {
				return false;
			}

			return self.areNamesSame(originalStudent, currentStudent, { notCaseSensitive: true });


		};

		self.hasChangedDobAndFamilyNameAndGivenName = function (originalStudent, currentStudent) {
			// Tests: Modification by a User of Birth date and Primary Family name together with one or more of 
			//        the Primary Given name fields. (With the exception of capitalisation changes)
			// For messages #934 && 918

			if (originalStudent.BirthDate() === currentStudent.BirthDate()) {
				return false;
			}

			// Skip if family name is unchanged
			if (self.areNamesSame(originalStudent, currentStudent, { notCaseSensitive: true, fields: ["FamilyName"] })) {
				return false;
			}

			return !self.areNamesSame(originalStudent, currentStudent, { notCaseSensitive: true, fields: ["GivenName1", "GivenName2", "GivenName3"] });
		};

		self.directNameComparison = function (name1, name2) {

			// Function to join names to a single string
			var makeName = function (name) {
				var allNames = [
					name.GivenName1,
					name.GivenName2,
					name.GivenName3,
					name.FamilyName
				];

				// Remove blank items
				allNames = ko.utils.arrayFilter(allNames, function (item) {
					return ko.utils.unwrapObservable(item);
				});

				return allNames.join(" ");
			};

			var joinedName1 = makeName(name1);
			var joinedName2 = makeName(name2);

			// TODO: Also compare NameDobVerification?

			return joinedName1.toLowerCase() === joinedName2.toLowerCase();
		};

		// Available options:
		// -- includeNameBirthDateVerification	- Adds the NameBirthDateVerificationMethodFK field as needing to be equal
		// -- notCaseSensitive					- 'makes it case insensitive
		// -- fields							- Fields to check for 'sameness'
		self.areNamesSame = function (name1, name2, options) {
			// Ensure options property
			if (!options) {
				options = {};
			}

			var properties = ["FamilyName", "GivenName1", "GivenName2", "GivenName3"];

			if (options.fields) {
				// Allow override of all fields being tested
				properties = options.fields;
			} else if (options.includeNameBirthDateVerification) {
				// Optional - NameDobVerification

				properties.push("NameBirthDateVerificationMethodFK");
			}

			name1 = ko.utils.unwrapObservable(name1);
			name2 = ko.utils.unwrapObservable(name2);



			// Iterate over each property, and compare
			for (var i = 0; i < properties.length; i++) {
				var propertyName = properties[i];

				// Sanity check - ensure entities are the same
				if (!name1.hasOwnProperty(propertyName)
					|| !name2.hasOwnProperty(propertyName)) {
					logger.log("areNamesSame() - Object is missing the property: '" + propertyName + "'");
				}

				var property1 = ko.utils.unwrapObservable(name1[propertyName]);
				var property2 = ko.utils.unwrapObservable(name2[propertyName]);


				// Blank values are the same
				if (!property1 && !property2) {
					continue;
				}

				// Stop an any non-similar name
				if (!options.notCaseSensitive) {
					if (property1 !== property2) {
						return false;
					}
				} else {
					// Case insenstive comparison
					// (The "" blank string fallback is to stop null values.
					if ((property1 ? property1.toLowerCase() : "") !== (property2 ? property2.toLowerCase() : "")) {
						return false;
					}
				}
			}

			// The names are the same
			return true;
		};

		self.hasDuplicateNames = function (currentStudent, newAltNames) {
			var student = ko.toJS(currentStudent);
			newAltNames = ko.toJS(newAltNames);

			var allNames = [student];
			allNames = allNames.concat(newAltNames);

			if (currentStudent.AltNames && currentStudent.AltNames()) {
				var existingAltNames = ko.toJS(currentStudent.AltNames);

				if (existingAltNames.length === 0
				&& newAltNames.length === 0) {
					// There can be no duplicates, if there are no alt names.
					return false;
				}

				allNames = allNames.concat(existingAltNames);
			}

			if (newAltNames.length === 0) {
				// There can be no duplicates, if there are no alt names.
				return false;
			}


			// Compare all names against each other. 
			// Nested loop is to ensure each comparison only happens once (the Handshaking Lemma)

			for (var i = 0; i < allNames.length; i++) {
				for (var j = i + 1; j < allNames.length; j++) {
					if (self.areNamesSame(allNames[i], allNames[j], { includeNameBirthDateVerification: true }) &&
						(allNames[i].FamilyName || allNames[i].FamilyName != "")) {
						return true;
					}

					if (self.directNameComparison(allNames[i], allNames[j]) &&
						(allNames[i].FamilyName || allNames[i].FamilyName != "")) {
						return true;
					}

				}

			}

			return false;

		};

		function isWeekDay(date) {
			//saturday = 6, sunday = 0
			var day = date.day();
			return day != 0
			       && day != 6;
		}

		function addBusinessDays(specificDate, workingDaysToAdd) {
			//Excludes Weekends
			var completeWeeks = Math.floor(workingDaysToAdd / 5);
			var daysToAdd = (completeWeeks * 7);
			var date = moment(specificDate).add(daysToAdd, 'days');

			workingDaysToAdd = workingDaysToAdd % 5;

			var direction = workingDaysToAdd >= 0 ? 1 : -1;

			for (var i = 0; i < Math.abs(workingDaysToAdd); i++) // Math.Abs used to allow for negative numbers
			{
				date = date.add(direction,'days');
				while (!isWeekDay(date))
				{
					date = date.add(direction, 'days');
				}
			}
			return date;
		};

		self.canModifyBirthDate = function (originalStudent, currentStudent, auth) {

			var nameDobVerification = originalStudent.NameBirthDateVerificationMethodFK();
			var nameBirthDateVerifiedByProviderFk = originalStudent.NameBirthDateVerifiedByProviderFK();
			var nameDobConfirmedByMoe = originalStudent.NameDobConfirmedByMOE();

			// In the event the *current* editing allows editing to become enabled
			if (hasChangedNameDobVerificationToNotBirthRegisterVerified(originalStudent, currentStudent, auth)) {
				return true;
			}

			//If birth register verified then no one can edit FS741
			if (nameDobVerification === enums.nameBirthDateVerificationMethod.birthRegister) {
				return false;
			}

			//For External users FS737
			if ((nameBirthDateVerifiedByProviderFk === settings.providers.moeId
				|| nameDobConfirmedByMoe === 'Yes')
				&& !auth.canModifyVerified) {
				return false;
			}

			//If they are not the user who created the record and do not have privilege
			if (!(session.user().OrgCode === currentStudent.CreatedByProviderCode()) && !auth.canModifyBirthDate
				&& !(originalStudent.BirthDate() === 'Unknown')) {
				return false;
			}

			//Only want date component to compare against.
			var today = moment({ hour: 0, minute: 0, seconds: 0, milliseconds: 0 });
			var createdOn = moment(currentStudent.CreatedOn());

			var days = parameters.get('Birth_date_edit_period');
			days = parseInt(days());
			var gracePeriod = addBusinessDays(createdOn, days);

			if (!createdOn || !days) {
				logger.logError("Couldn't find required param for canModifyBirthDate calculation");
				return false;
			}

			//If not within grace period and doesnt have privilege
			if ((session.user().OrgCode === currentStudent.CreatedByProviderCode()) && !(today <= gracePeriod)
				&& !auth.canModifyBirthDate) {
				return false;
			}

			if (auth.canModifyBirthDate
				&& !auth.canModifyVerified
				&& !originalStudent.nameBirthDateVerificationMethod() === enums.nameBirthDateVerificationMethod.unverified) {
				return false;
			}

			return true;
		};

		self.hasUnverifiedAltNames = function (newAltNames) {

			newAltNames = ko.utils.unwrapObservable(newAltNames);

			if (newAltNames.length === 0) {
				return false;
			}

			var firstUnverified = ko.utils.arrayFirst(newAltNames, function (item) {
				return item.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.unverified;
			});

			return firstUnverified !== null;
		};

		// ----
		// -- 'Can edit' calculations

		var hasChangedNameDobVerificationToNotBirthRegisterVerified = function (originalStudent, currentStudent, auth) {
			return auth.canModifyVerified
				&& originalStudent.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.birthRegister
				&& currentStudent.NameBirthDateVerificationMethodFK() !== enums.nameBirthDateVerificationMethod.birthRegister;
		};

		self.canModifyName = function (originalStudent, currentStudent, auth) {

			var nameBirthDateVerificationMethodFK = originalStudent.NameBirthDateVerificationMethodFK();

			if (!nameBirthDateVerificationMethodFK) { // Test student data has been loaded
				return false;
			}

			// In the event the *current* editing allows editing to become enabled
			if (hasChangedNameDobVerificationToNotBirthRegisterVerified(originalStudent, currentStudent, auth)) {
				return true;
			}

			// Normal checking

			// NameDob unverified records can be modified
			if (nameBirthDateVerificationMethodFK === enums.nameBirthDateVerificationMethod.unverified) {
				return true;
			}

			//External users cannot edit if confirmed by MOE FS737
			if (originalStudent.NameBirthDateConfirmedByMOEDateTime() && !auth.canModifyVerified) {
				return false;
			}

			// NSI2_MODIFY_VERIFIED controls modifying verified records
			if (!auth.canModifyVerified) {
				return false;
			}

			// Can modify non-birth register verified records
			return nameBirthDateVerificationMethodFK !== enums.nameBirthDateVerificationMethod.birthRegister;
		};

		self.canModifyNameDobVerification = function (originalStudent, currentStudent, auth) {

			var nameDobVerification = originalStudent.NameBirthDateVerificationMethodFK();

			if (!nameDobVerification) { // Test student data has been loaded
				return false;
			}

			if (originalStudent.NameBirthDateConfirmedByMOEDateTime() && !auth.canModifyVerified) {
				return false;
			}


			// Normal checking


			// Normal checking

			if (nameDobVerification === enums.nameBirthDateVerificationMethod.unverified) {
				return currentStudent.BirthDate !== 'Unknown'; // TODO: How to accurately calculate if birthdate is unknown?
			}

			if (!auth.canModifyVerified) {
				return false;
			}

			if (nameDobVerification !== enums.nameBirthDateVerificationMethod.birthRegister) {
				return true;
			}

			return auth.canModifyBirthRegisterVerified;
		};

		var hasChangedResidentialVerificationToNotBirthRegisterVerified = function (originalStudent, currentStudent, auth) {
			return auth.canModifyVerified
				&& originalStudent.ResidentialStatusVerificationMethodFK() === enums.residentialStatusVerificationMethod.birthRegister
				&& currentStudent.ResidentialStatusVerificationMethodFK() !== enums.residentialStatusVerificationMethod.birthRegister;
		};

		self.canModifyResidentialStatus = function (originalStudent, currentStudent, auth) {

			var residentialStatusVerificationMethodFK = originalStudent.ResidentialStatusVerificationMethodFK();
			var residentialStatusVerifiedByProviderFk = originalStudent.ResidentialStatusVerifiedByProviderFK();
			var residentialStatusConfirmedByMoe = originalStudent.ResidentialStatusConfirmedByMOE();

			if (!residentialStatusVerificationMethodFK) { // Test student data has been loaded
				return false;
			}

			if (hasChangedResidentialVerificationToNotBirthRegisterVerified(originalStudent, currentStudent, auth)) {
				return true;
			}

			// Normal checking
			//No one can edit if verified by birth register
			if (residentialStatusVerificationMethodFK === enums.residentialStatusVerificationMethod.birthRegister) {
				return false;
			}

			//For External users, cant edit if confirmed or verified by MOE
			if ((residentialStatusVerifiedByProviderFk === settings.providers.moeId
				|| residentialStatusConfirmedByMoe === 'Yes')
				&& !auth.canModifyVerified) {
				return false;
			}

			return true;
		};

		self.canModifyResidentialStatusVerification = function (originalStudent, auth) {

			var residentialStatusVerificationFk = originalStudent.ResidentialStatusVerificationMethodFK();
			var residentialStatusVerifiedByProviderFk = originalStudent.ResidentialStatusVerifiedByProviderFK();
			var residentialStatusConfirmedByMoe = originalStudent.ResidentialStatusConfirmedByMOE();

			if (!residentialStatusVerificationFk) { // Test student data has been loaded
				return false;
			}

			if (residentialStatusVerificationFk === enums.residentialStatusVerificationMethod.unverified) { // TODO: Add to entity

				return residentialStatusVerificationFk === enums.residentialStatus.unknown;
			}

			if (residentialStatusVerificationFk !== enums.residentialStatusVerificationMethod.birthRegister
				&& residentialStatusVerifiedByProviderFk !== settings.providers.moeId
				&& residentialStatusConfirmedByMoe !== 'Yes') {
				return true;
			}

			return auth.canModifyBirthRegisterVerified;
		};

		self.canModifyVerificationConfirmedByMinistry = function (verification, verifyingProviderId, auth) {

			if (!auth.canModifyVerified) {
				return false;
			}

			if (!verification) { // Test that student data has been loaded
				return false;
			}

			if (verification === enums.nameBirthDateVerificationMethod.unverified
				|| verification === enums.nameBirthDateVerificationMethod.birthRegister
				|| verifyingProviderId === settings.providers.moeId
			) {
				return false;
			}

			return true;
		};

		// Confirmations
		self.attachBirthDate029Confirmation = function (dobObject, resetBirthDate) {
			resetBirthDate = resetBirthDate ? ko.utils.unwrapObservable(resetBirthDate) : "";
			dobObject.focused = ko.observable(false);

			// Attach #029 - Birthdate is outside of min/max range - prompt
			var subscription = dobObject.focused.subscribe(function (hasFocus) {
				if (!hasFocus &&
					dobObject.isValid() &&
					dobObject.isModified() &&
					moment(dobObject()).diff(moment("11/11/1918")) != 0
				) {
					var parsedDate = moment(dobObject(), settings.momentAcceptedDateInputs, true);
					if (parsedDate.isValid()) {
						var now = moment();
						var age = now.diff(parsedDate, 'years', true);
						var notTodaysDate = now.diff(parsedDate, 'days', true) > 1 ? true : false;
						if (notTodaysDate &&
							(age < settings.dobRangeMin || age > settings.dobRangeMax)
						) {
							var confirmMessage = resourceService.get("MessageCode_Confirmation_BirthDateOutOfRange_029");
							app.showMessage('', confirmMessage, ['Yes', 'No']).then(function (result) {
								if (result === 'No') {
									dobObject(resetBirthDate);
								}
							});
						}
					}
				}
			});


			ko.utils.domNodeDisposal.addDisposeCallback(self, function () {
				subscription.dispose();
			});
		};

		return self;
	});