﻿
// Cache busting lives for the lifetime of the deployed app
var cacheBusting = $("body").attr("data-cacheVersion");

require.config({
	urlArgs: cacheBusting ? "_=" + cacheBusting : "", 
	paths: {
		'text': '../Scripts/text',
		'durandal': '../Scripts/durandal',
		'plugins': '../Scripts/durandal/plugins',
		'transitions': '../Scripts/durandal/transitions'
	},
	waitSeconds: '60' // TODO: For dev only, to aid debugging
});

// Define 3rd party libraries, already loaded by bundling
define('jquery', function () { return jQuery; });
define('knockout', ko);

// Setup app
define(['durandal/app',
		'durandal/viewLocator',
		'durandal/system',
		'plugins/router',
		'services/sessionService',
		'services/logger',
		'services/navigationService',
		'services/resourceService',
		'services/referenceService',
		'services/parameterService', // It's startup loads its own params
		'durandal/composition',
		'data/validationRules' // Do not remove - loads custom bindings. Must occur before 'ko.validation.init' step (in the start sequence)
	],
    function (app, viewLocator, system, router,
	    sessionService, logger, navigationService, resourceService, referenceService, parameterService, composition) {

		// This is appended after the nav page title (e.g.: 'Student search | [app.title]')
    	app.title = 'NSI';
	    
        // Enable debug message to show in the console 
        system.debug(true);

    	//specify which plugins to install and their configuration
        app.configurePlugins({
        	router: true,
        	dialog: true,
        	widget: {
        		kinds: [
        			'mergeDeniedPairsWidget',
					'editNewAltNamesWidget',
        			'validationSummaryWidget',
        			'helpIconWidget',
        			'breadcrumbWidget'
        		]
        	}
        });
	    

		app.start()
			.then(sessionService.loadSession) // Ensure this happens before routes & locators are loaded
			//.then([resourceService.init, referenceService.loadAll])
			//.then($.when(resourceService.init(), referenceService.loadAll())) // TODO: This doesn't defer correctly - allows page to load before resources. Fix later.
			.then(resourceService.init)
			.then(referenceService.loadAll)
		    .then(function() {

	        	// Only allow route-guarding once session is loaded...
	        	// Check if each accessed route is auth'd
	        	router.guardRoute = nsiGuardRoutes;

				// Register navigation
		        var navRoutes = navigationService.getAllRoutesAsFlatNav();
	        	router.map(navRoutes).buildNavigationModel();

		        router.mapUnknownRoutes(function (instruction) {
		        	logger.logError("Invalid URL", null, "main", true);

			        router.navigate(sessionService.getSession().user().IsAuthenticated
				        ? "#/welcome"
				        : "#");

		        });

	        	// When finding a viewmodel module, replace the viewmodel string 
        		// with view to find it partner view.
        		viewLocator.useConvention();

        		//Show the app by setting the root view model for our application.
        		app.setRoot('viewmodels/shell', 'entrance');

			    toastr.options.positionClass = 'toast-bottom-right';
			    //toastr.options.positionClass = 'toast-top-full-width'; // 'toast-bottom-right';
				toastr.options.backgroundpositionClass = 'toast-bottom-right';

		        // Init validation Framework
				ko.validation.init({
					//messageTemplate: 'validationMsgTemplate',
				    insertMessages: false,
				    parseInputAttributes: true,
					decorateElement: true,
					errorClass: 'error',
					messagesOnModified: true
				});
				//As per the suggestion from durandal forum, need to re-set the binding handler 
				//for 'hasFocus' KO binding, after the composition complete.
	        	//https://groups.google.com/forum/#!msg/durandaljs/Eu6GHDiQe0o/rpJBLX_hEJEJ
				composition.addBindingHandler('hasFocus');
        });
	    
        function nsiGuardRoutes(viewModel, routeInfo) {
        	if (routeInfo.config.route === "*catchall"
        		|| !routeInfo.config.settings.authorise) {
	        	return true;
	        }

	        var isAuthenticated = sessionService.getSession().user().IsAuthenticated;
        	var isAuthorised = sessionService.isAuthorised(routeInfo.config.settings.authorise);

        	if (isAuthenticated && isAuthorised) { // Success condition
        		return true;
        	} else if (!isAuthenticated) { // Unauthorised

				//// We are no longer doing an immediate redirect to ESAA.
        		// Optionally include the return URL
        		if (routeInfo
        			&& routeInfo.fragment
        			&& routeInfo.fragment !== "#" // Ignore home route - make sure it redirects to the default - /welcome
        		) {
        		    //If not authenticated, then it's just timed out.
        		    return "#/?ReturnUrl=" + routeInfo.fragment;// + "&MessageCode=090";
        		} else {
        		    return "#";// + "?MessageCode=090";
        		}
        		
        	} else if (!isAuthorised) { // Unauthorised - Give message & deny access to the page
        		var unauthorisedMessage = (resourceService.get('MessageCode_Security_AccessDenied_529'));
        			
        		logger.logError(unauthorisedMessage, null, "main", true);

		        return "#welcome";
	        }
        };
    });
