﻿define(['services/resourceService',
		'libs/uiLib',
		'data/enums'],
		function (resourceService, uiLib, enums) {

			var entity = function () {

				var self = this;

				//var currentStudent = ko.observable();
				self.Version = ko.observable();
				self.Id = ko.observable();
				self.MasterNSN = ko.observable();
				self.Name = ko.observable();
				self.FamilyName = ko.observable();
				self.GivenName1 = ko.observable();
				self.GivenName2 = ko.observable();
				self.GivenName3 = ko.observable();
				self.GivenNames = ko.observable();
				self.PreferredNameIndicator = ko.observable();				

				self.BirthDate = ko.observable();
				self.DOBVerification = ko.observable();
				self.DateOfDeath = ko.observable();

				self.ResidentialStatus = ko.observable();
				self.ResidentialStatusVerificationMethod = ko.observable();
				self.Gender = ko.observable();
				self.StudentStatus = ko.observable();
				self.StudentStatusFK = ko.numericObservable();
				self.StudentStatusReason = ko.observable();
				self.StudentStatusReasonFK = ko.numericObservable();
				self.StudentStatusSummary = ko.computed(function () {
					return self.StudentStatus() + " (" + self.StudentStatusReason() + ")";
				});
				
				// flag for recently added students -#MB
				self.hasBeenJustAddedOrUpdated = ko.observable(false);

				self.NameBirthDateVerificationSummary = ko.observable();
				self.NameBirthDateVerificationMethod = ko.observable();
				self.NameBirthDateVerificationMethodFK = ko.numericObservable();
				self.NameBirthDateVerificationDateTime = ko.observable();
				self.NameBirthDateVerifiedByProvider = ko.observable();
				self.NameBirthDateVerifiedByProviderFK = ko.numericObservable();
				self.NameDobConfirmedByMOE = ko.observable();
				self.NameBirthDateConfirmedByMOEDateTime = ko.observable();

				self.ResidentialStatusFK = ko.numericObservable();
				self.ResidentialStatusVerificationSummary = ko.observable();
				self.ResidentialStatusVerificationDateTime = ko.observable();
				self.ResidentialStatusVerifiedByProvider = ko.observable();
				self.ResidentialStatusVerifiedByProviderFK = ko.observable();
				self.ResidentialStatusVerificationMethodFK = ko.numericObservable();
				self.ResidentialStatusConfirmedByMOE = ko.observable();
				self.ResidentialStatusConfirmedByMOEDateTime = ko.observable();

				self.ModifiedByUser = ko.observable();
				self.ModifiedDateTime = ko.observable();
				self.ModifiedByProviderName = ko.observable();

				self.CreatedByUser = ko.observable();
				self.CreatedOn = ko.observable();
				self.CreatedByProviderName = ko.observable();
				self.CreatedByProviderCode = ko.observable();

				self.Slaves = ko.observableArray();
				self.AltNames = ko.observableArray();
				
				self.isMaster = ko.computed(function () {
					return !self.MasterNSN();
				}, this);

				var SlaveNSNBeforeUnMerge;

				//self.ChallengeChange = ko.observableArray();

				self.Challenges = ko.observableArray();
				
				//self.currentStudent = currentStudent;

				//    //Used only clientside, to flag whether merging or not:
				//    self.mergeRequested = ko.observable();

				//    //Gives 'John Smith'
				//    self.fullName = ko.computed(function() {
				//        var result = "{0}{1}".format(
				//            self.givenName(),
				//            ((self.givenName()) && (self.familyName)) ? ' ' : '',
				//            self.familyName()
				//        );
				//        return result;
				//    });
				//    //Gives: 'Smith, John'
				//    self.fullNameFormal = ko.computed(function() {
				//        var result = "{0}{1}{2}".format(
				//            self.familyName(),
				//            ((self.familyName()) && (self.givenName)) ? ', ' : '',
				//            self.givenName()
				//        );
				//        return result;
				//    });

			};

			//var result = new entity();

		    //setupValidation(result);
		    entity.prototype.enableRequiredValidation = function(student) {
		        if (!student) {
		            return;
		        }
		        var testRequiredMessage = "{0} is a required field";
		        if (student.GivenName1) {
		        	student.GivenName1.extend({
		        		required: {
		        			message: testRequiredMessage.format("Given name (1)")
		        		}
		        	});
		        }

		        if (student.FamilyName) {
		        	student.FamilyName.extend({
		        		required: {
		        			message: testRequiredMessage.format("Family name")
		        		}
		        	});
		        }
		    };


			entity.prototype.enableValidation = function (student) {
				if (!student) {
					return;
				}

				uiLib.setCommonNameValidation(student.GivenName1, "Given name (1)", { allowSingleTilde: true });
				uiLib.setCommonNameValidation(student.GivenName2, "Given name (2)");
				uiLib.setCommonNameValidation(student.GivenName3, "Given name (3)");
				uiLib.setCommonNameValidation(student.FamilyName, "Family name");

				uiLib.setTildeRuleValidation(student);

				// TODO: How do I scope this correctly?
				//var testRequiredMessage = "{0} is a required field";
				//if (student.GivenName1) {
				//	student.GivenName1.extend({
				//		required: {
				//			message: testRequiredMessage.format("Given name (1)")
				//		}
				//	});
				//}

				//if (student.FamilyName) {
				//	student.FamilyName.extend({
				//		required: {
				//			message: testRequiredMessage.format("Family name")
				//		}
				//	});
				//}
				
				
				
				uiLib.setCommonBirthDateValidation(student.BirthDate, "Birth date");
				//setDateValidation(student.BirthDate, "Birth date"); // namedob verification date
				//setDateValidation(student.BirthDate, "Birth date"); // res verification date
				//setDateValidation(student.BirthDate, "Birth date"); // Date of death


				student.NameBirthDateVerificationMethodFK.extend({
					validation: [
    					{
    						validator: function () {
    							var result = !(student.NameBirthDateVerificationMethodFK() !== enums.nameBirthDateVerificationMethod.unverified
    											&& !student.BirthDate());

    							return result;
    						},
    						message: resourceService.get("MessageCode_Error_UnverifiedBlank_023")
    					}
					]
				});

				student.ResidentialStatusFK.extend({
					validation: [
    					{
    						validator: function () {
    							var result = !(student.ResidentialStatusVerificationMethodFK() !== enums.residentialStatusVerificationMethod.unverified
    											&& student.ResidentialStatusFK() === enums.residentialStatus.unknown);

    							return result;
    						},
    						message: resourceService.get("MessageCode_Error_UnverifiedBlank_023")
    					}
					]
				});
			};
			
			return entity;

		}
);