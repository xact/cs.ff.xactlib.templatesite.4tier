﻿define([],
		function () {

			var entity = function () {

				var self = this;

				self.Parameter = ko.observable();
				self.Value = ko.observable();
				self.Description = ko.observable();
				self.Datatype = ko.observable();
				self.Parametertype = ko.observable();
				//self.Tag = ko.observable();
				self.IsEditable = ko.observable();
				//self.Valuetype = ko.observable();

			};

			return new entity;

		}
);