﻿define([],
		function () {

			var entity = function () {

				var self = this;

				self.FileType = ko.observable();
				self.RequestFileName = ko.observable();
				self.ResponseFileName = ko.observable();
				self.ReceivedOn = ko.observable();
				self.UserId = ko.observable();
				self.Downloaded = ko.observable();
				self.Status = ko.observable();
				self.Id = ko.observable();

			};

			return new entity;

		}
);