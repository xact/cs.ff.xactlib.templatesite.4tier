﻿define([],
		function () {

			var entity = function () {

				var self = this;

				self.ProviderCode = ko.observable();
				self.ProviderName = ko.observable();
				self.NSILiveDate = ko.observable("");
				self.FileFormatForChangeNotifications = ko.observable();
				self.UseMacrons = ko.observable();
				self.ManualMergeRequired = ko.observable();
				self.OnBehalf = ko.observable();
				self.SPRDuration = ko.observable();
				self.ReceiveChangeNotifications = ko.observable();
				self.InterfaceXML = ko.observable();
				self.InterfaceSOAP = ko.observable();
				self.InterfaceBatch = ko.observable();
				self.InterfaceRest = ko.observable();
				self.Interfaces = ko.observable();
				self.ShortName = ko.observable();
				self.LastChangeNotificationDownloadDateTime = ko.observable();

				function clear() {
					self.ProviderCode("");
					self.ProviderName("");
			}
				entity.prototype.clear = function () {
					// Clear out all fields
					self.providerCode("");
					self.providerName("");

				};
			};

			return entity;

		}
);