﻿define(
		function () {

			var entity = function () {

				var self = this;

				self.mergeEntryNSN = ko.observable();
				self.mergeListNSN = ko.observable();
				self.mergeListProviderCode = ko.observable();
				self.mergeListFamilyName = ko.observable();
				self.mergeListGivenNames = ko.observable();
				self.mergeListResults = ko.observableArray();

				self.mergeStatusRequestedFrom = ko.observable("");
				self.mergeStatusRequestedTo = ko.observable("");
				self.mergeStatusChangedFrom = ko.observable("");
				self.mergeStatusChangedTo = ko.observable("");
				self.mergeStatusRequestStatus = ko.observable();
				self.mergeStatusProviderToGet = ko.observable(null);
				self.mergeStatusNSN = ko.observable("");
				self.mergeStatusAllProviders = ko.observable(false);
				self.mergeStatusAllUsers = ko.observable(true);
				self.mergeStatusUser = ko.observable();
				self.mergeStatusResults = ko.observableArray();
				
			};
			return new entity();
		}
);