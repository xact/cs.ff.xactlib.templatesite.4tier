﻿define(['data/enums'],
	function(enums) {

		

		var entity = function() {

			var self = this;
			
			// --------------
			// -- Properties

			self.mergeRequestId = 0;

			self.altNames = ko.observableArray();

			self.mergeDeniedPairs = ko.observableArray();

			self.mergeRequest = ko.observable();

			self.mergeDetails = ko.observableArray();

			self.manualInterventionMessages = ko.observableArray();

			self.selections = {
				masterStudentId: ko.numericObservable(),
				familyName: ko.numericObservable(),
				givenName1: ko.numericObservable(),
				givenName2: ko.numericObservable(),
				givenName3: ko.numericObservable(),
				birthDate: ko.numericObservable(),
				gender: ko.numericObservable(),
				residentialStatus: ko.numericObservable()
			};


			self.isNameAndDobLocked = ko.observable(true);
			
		};

		// -- Methods
		
		// Convenience properties

		entity.prototype.getSelectedStudentIds = function () {
			var selectedStudentIds = [];

			ko.utils.arrayForEach(this.mergeDetails(), function (item) {
				if (item.isSelectedForMerge()) {
					selectedStudentIds.push(item.StudentId);
				}
			});

			return selectedStudentIds;
		};

		entity.prototype.hasInactiveNonSlaves = function() {
			var inactiveNonSlaveStudents = ko.utils.arrayFilter(this.mergeDetails(), function(item) {
				return item.StudentStatusFK === enums.studentStatus.inactive
					&& item.StudentStatusReasonFK !== enums.studentStatusReason.slave;

			});

			return inactiveNonSlaveStudents ? true : false;
		};

		entity.prototype.hasPreviouslyDeniedPairs = function() {
			var selectedStudentIds = this.getSelectedStudentIds();

			if (selectedStudentIds.length === 0) {
				return false;
			}

			var mergeDeniedPairs = this.mergeDeniedPairs();
			
			for (var i = 0; i < mergeDeniedPairs.length; i++) {
				var deniedPair = mergeDeniedPairs[i];
				
				// Check if a denied pair applies to the merge student group
				if (selectedStudentIds.indexOf(deniedPair.Student1Id) > -1
					&& selectedStudentIds.indexOf(deniedPair.Student2Id) > -1) {
					return true;
				}
			}

			return false;
		};

		return new entity();
	}
);
		