define ([],
		function(user) {

		    var emptyUser = { IsAuthenticated: false, DisplayName: "", OrgName: "", OrgCode: "", Claims: [], AntiforgeryToken: null };
			
			var model = function () {
				// Start with an unauthenticated user
			    this.user = ko.observable(emptyUser);
			    //this.invalidUser = ko.observable(false);
			};

			model.prototype.updateUser = function(data) {
				
				if (!data) {
					this.reset();
					return;
				} // Don't update empty

				this.user(data);
				
				// If we later find we need to have the properties as observables, use the mapping
				//var resultUser = ko.mapping.fromJS(data);
				//this.user(resultUser);
			};

			model.prototype.reset = function() {
				this.user(emptyUser);
			};

			return new model();
		}
);