Singletons are modules who's function returns a new object, rather than a function.
As per require.js, that means they will be a singleton, global object.
Useful for passing state around between views.
