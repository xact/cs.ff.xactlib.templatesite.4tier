﻿define(['singletons/searchResults'],
	function(searchResults) {

		

		var entity = function() {

			var self = this;
			
			// Data result records
			self.records = ko.observableArray();
			//self.records = ko.computed(function() {
			//	searchResults.
		    //});
			self.showMaxAllowedRecordsError = ko.observable(false);
			self.mergeIsActivated = ko.observable(false);
			
			self.mergeDeniedPairs = ko.observableArray();
			self.haveMergeDeniedPairsLoaded = false;

			//self.allSelectedNSNs = ko.computed(function () {
			//self.allSelectedStudentIds = ko.computed(function () {
				
			//	var selectedStudentIds = [];
			//	$.each(self.records(), function (index, item) {
			//		if (item.isSelectedForMerge()) {
			//			selectedStudentIds.push(item.StudentId);
			//		}
			//	});
				
			//	return selectedStudentIds;
			//});
			
			self.allSelectedStudentIDs = ko.computed(function () {

				var selectedStudentIds = [];
				$.each(self.records(), function (index, item) {
					if (item.isSelectedForMerge()) {
						selectedStudentIds.push(item.StudentId);
					}
				});

				return selectedStudentIds;
			});
		};

		// -- Methods
		
		entity.prototype.update = function(data) {
			this.records(data);
		};

		entity.prototype.clear = function() {
			this.records.removeAll();
			this.mergeDeniedPairs([]);
			this.haveMergeDeniedPairsLoaded = false;
			this.showMaxAllowedRecordsError(false);
			this.mergeIsActivated(false);
		};

		entity.prototype.addStudent = function (student) {
			// Add new properties:
			student.isSelectedForMerge = ko.observable(false);

			// Add to main list
			this.records.push(student);
		};

		entity.prototype.addMergeDeniedPairs = function(deniedPairs) {

			var scope = this;

			// Remove items already in the display set
			var nonDuplicates = $.grep(deniedPairs, function (item) {
				// Return false if the item already exists
				return !ko.utils.arrayFirst(scope.mergeDeniedPairs(), function (mergeDeniedItem) {
					// Comparison test - based off ID
					var result = mergeDeniedItem.Id === item.Id;

					return result;
				});
			});

			this.mergeDeniedPairs.push.apply(this.mergeDeniedPairs, nonDuplicates);
			this.mergeDeniedPairs.valueHasMutated();
			this.haveMergeDeniedPairsLoaded = true;
		};

		// Convenience properties

		/** Gets all NSNs in the merge request, as an array
		*/
		entity.prototype.getAllNSNs = function() {
			var nsns = [];
			$.each(this.records(), function(index, item) {
				nsns.push(item.NSN);
			});

			return nsns;
		};

		entity.prototype.containsNsn = function(nsn) {
			return ko.utils.arrayFirst(this.records(), function(item) {
				// Deliberate 'non-triple ===' - Allow comparison of "1" == 1 to return true
				return item.NSN == nsn;
			});
		};

		return new entity();
	}
);
		