﻿define(
		function () {

			var entity = function () {

				var self = this;

				self.records = ko.observableArray();
				
//				self.Id = ko.observable();
//				self.ModifiedDateTime = ko.observable();
//				self.ModifiedByProviderName = ko.observable();
//				self.ModifiedByUser = ko.observable();
//				self.UpdateType = ko.observable();
//				self.FieldUpdated = ko.observable();
//				self.OldValue = ko.observable();
//				self.NewValue = ko.observableArray();
			};
		    
		    // -- Methods

			entity.prototype.update = function (data) {
			    this.records(data);
			};

			entity.prototype.clear = function () {
			    this.records.removeAll();
			};
		    
			return new entity();
		}
);


