﻿define(['data/enums'],
	function(enums) {

		var entity = function() {

			var self = this;
			
			// Data result records
			self.records = ko.observableArray();
			
			// Used to identify when a search returned no records (different to the empty search it starts with
			self.foundNoResults = ko.observable(false);
		};

		entity.prototype.update = function(data) {
			// Add a property to mark if it's 'selected'
			$.each(data, function (index, record) {
				record.isSelectedForMerge = ko.observable(false);
			});
			
			$.each(data, function (index, record) {
				record.isInactive = record.StudentStatusId === enums.studentStatus.inactive;
				record.rawMatchScore = record.MatchScore;
				record.MatchScore = Math.floor(record.MatchScore);
			});

			this.records(data);
			this.foundNoResults(!data || data.length === 0);
		};

		// Resets all records as no longer 'selected for merge'
		entity.prototype.resetIsSelectedForMerge = function() {
			$.each(this.records(), function (index, record) {
				if (record.isSelectedForMerge) {
					record.isSelectedForMerge(false);
				}
			});
		};

		entity.prototype.clear = function() {
			this.records.removeAll();
			this.foundNoResults(false);
		};

		return new entity();
	}
);
		