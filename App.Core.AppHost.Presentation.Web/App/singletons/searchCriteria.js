﻿define(['services/resourceService'
	],
	function (resourceService) {

	    var entity = function () {

	        var self = this;

	        self.nsn = ko.observable("");

	        self.name = ko.observable("");
	        self.dob = ko.observable("");
	        self.dobForAdd = ko.observable("");
	        self.nameForAdd = ko.observable("");
	        self.genderForAdd = ko.observable();
	        self.residentialStatusForAdd = ko.observable();
	        //self.name = ko.observable();
	        //self.dob = ko.observable();
	        
	        self.gender = ko.observable();
	        self.residentialStatus = ko.observable();
	        
	        self.mandatoryFieldsFlagEnabled = ko.observable(false);
	        self.canShowErrors = ko.observable(false);
	        self.resources = resourceService.resources;
	    };
	    
        function clear() {
            self.nsn("");
            self.name("");
            self.dob("");
            self.gender("");
            self.residentialStatus("");
            self.mandatoryFieldsFlagEnabled = ko.observable(false);
            self.canShowErrors = ko.observable(false);
        }

	    entity.prototype.clear = function () {
	        // Clear out all fields
	        this.nsn("");
	        this.name("");
	        this.dob("");
	        this.gender("");
	        this.residentialStatus("");
	        this.canShowErrors = ko.observable(false);
	        this.mandatoryFieldsFlagEnabled = ko.observable(false);
	    };

	    var result = new entity();

	    setupValidation(result);

	    return result;


	    function setupValidation(searchCriteria) {
	        searchCriteria.nsn.extend({
	            isNumeric: {
	                params: '^[0-9]{1,10}$',
	                message: resourceService.get("MessageCode_Validation_Numeric_003").format("NSN") // TODO: update 'NSN' from resources
	            },
	            required: {
	                message: function () {
	                    //self.mandatoryFieldsMessage = "Please enter values for the following mandatory field(s)";
	                    self.mandatoryFieldsMessage = resourceService.get("MessageCode_Validation_MissingMandatoryFields_257").format('');
	                    self.NSNerrorMsg = "<ul><li>NSN</li></ul>";

	                    //todo: We need to add a flag to indicate if a secondary list of "required" errors has already been added. #MB
	                    //if (self.mandatoryFieldsFlagEnabled) {
	                    //    self.msg = self.NSNerrorMsg;
	                    //} else {
	                    //    self.msg = self.mandatoryFieldsMessage + self.NSNerrorMsg;
	                    //    self.mandatoryFieldsFlagEnabled = true;
	                    //}
	                    
	                    self.msg = self.mandatoryFieldsMessage + self.NSNerrorMsg;
	                    return self.msg;
	                }
	                //	    			onlyIf: function () {
	                //	    				return searchCriteria.name() != "";
	                //	    			}
	            },
	            validNsn: {
	                message: resourceService.get("MessageCode_Validation_InvalidNSN_336")
	            }
	        });


	        

	        // 'Name' search - Validation
	        searchCriteria.name.extend({
	            containsValidCharactersForOneSingleName: {
	        		message: resourceService.get("MessageCode_Validation_InvalidFormat_272").format("Name")
	        	},
	            validation: [
		            {
		            	validator: function () {
		            		var patt = new RegExp("~");

		            		if (searchCriteria.name().length > 2 &&
								patt.test(searchCriteria.name().substring(1, searchCriteria.name().length - 2))) {
		            			return false;
		            		}

		            		if (searchCriteria.name().length > 1 &&
								searchCriteria.name().substr(0, 1) == "~" &&
		            			searchCriteria.name().substr(searchCriteria.name().length - 1, 1) == "~") {
		            			return false;
		            		}

		            		
		            		return true;
		            	},
		            	//message: "Birth date, Gender and Residential status may only be used in conjunction with at least one Name"
		            	message: resourceService.get("MessageCode_Validation_InvalidFormat_272").format("Name")
		            },
	        		{
	        		    validator: function () {
	        		        var isValid = !searchCriteria.name() && (searchCriteria.dob() || searchCriteria.gender() || searchCriteria.residentialStatus());

	        		        return !isValid;
	        		    },
	        		    //message: "Birth date, Gender and Residential status may only be used in conjunction with at least one Name"
	        		    message: resourceService.get("MessageCode_Validation_OneNameMinimum_012")
	        		},
				    {
				        validator: function() {

				            var isValid = !searchCriteria.name() && (searchCriteria.dob() || searchCriteria.gender() || searchCriteria.residentialStatus());

				            return !isValid;
				        },
				        //message: "Birth date, Gender and Residential status may only be used in conjunction with at least one Name"
				        message: resourceService.get("MessageCode_Validation_OneNameMinimum_012")
				    },
				    {
				        validator: function(input) {
				            // Give priority to the other rule about 'if you've filled in no name, but have done one of the other 3
				            if (!input && (searchCriteria.dob() || searchCriteria.gender() || searchCriteria.residentialStatus())) {
				                return true;
				            }

				            // Fail for pure empty string
				            if (!input) {
				                return false;
				            }



				        	// Accepted characters: letters, '~', '-', '' (apostrophy)
					        if (input.match(/[a-zA-Z~\'\-]+/g)) {
						        var numberOfWords = input.match(/[a-zA-Z~\'\-]+/g).length;

						        if (numberOfWords >= 2 || // 2 + words is accepted
							        (numberOfWords === 1 && searchCriteria.dob())) { // 1 word + DOB is accepted
							        return true;
						        }
					        }
					        return false;
				        },
				        //message: "Please enter a minimum of two names, or one name and a birth date"
				        message: resourceService.get("MessageCode_Validation_OneNameMinimum_012")
				    }],
	            maxLength: 403
		    	
	          
	        });

	        // Birth date validation
	        searchCriteria.dob.extend({
	            validNzDate: {
	                //message: 'Birth date is not a valid date',
	                message: resourceService.get("MessageCode_Validation_Date_001").format("Birth date"),
	                onlyIf: function() { return searchCriteria.dob(); } // Only validate when populated
	            },
	            nzDate: {
	                //message: 'Birth date is not a valid date',
	                message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Birth date"),
	                onlyIf: function() { return searchCriteria.dob(); } // Only validate when populated
	            },
	            maxDateIsToday: {
	                //message: "Birth date cannot be later than current date",
	            	message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Birth date"),
	                onlyIf: function() { return searchCriteria.dob(); } // Only validate when populated
	            },

	            dateCannotBeToday: {
	                message: resourceService.get("MessageCode_Validation_BirthdateCannotBeToday_945"),
	                onlyIf: function() { return searchCriteria.dob(); }
	            }
	    	});            
	    };
	}
)