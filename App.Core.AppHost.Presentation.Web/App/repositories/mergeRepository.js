﻿
define(['durandal/system',
		'services/commService'],
	function (system, commService) {

	    var service = function () {
	    };


	    service.prototype.getStudentSummary = function (nsn) {

	        return commService.get({
	            operationTitle: "Getting Student summary by NSN",
	            url: "uiapi/StudentForMergeRequest/" + nsn
	        });

	    };

	    // ----- Merge requests -----

	    /** Submit merge request
		*/
	    service.prototype.submitMergeRequest = function (studentIds) {

	        return commService.post(
				{
				    operationTitle: "Submitting merge request",
				    url: "uiapi/MergeRequest",
				    data: { "": studentIds }
				});
	    };

	    service.prototype.searchMergeRequests = function (criteria, sortInfo) {

	        // Move sort info to new schema
	        var newCriteria = {
	            Data: {
	                SortOrder: sortInfo.SortOrder,
	                SortColumn: sortInfo.SortColumn
	            },
	            PageIndex: sortInfo.PageIndex,
	            PageSize: sortInfo.RecordsToTake
	        };

	        // Copy over criteria properties
	        $.extend(newCriteria.Data, criteria);

	        return commService.get({
	            operationTitle: "Getting merge requests",
	            url: "uiapi/MergeRequest",
	            data: newCriteria
	        });
	    };


	    //#region MergeStatus - #MB

	    service.prototype.searchMergeStatuses = function (criteria, sortInfo) {

	        // Move sort info to new schema
	        var newCriteria = {
	            Data: {
	                SortOrder: sortInfo.SortOrder,
	                SortColumn: sortInfo.SortColumn
	            },
	            PageIndex: sortInfo.PageIndex,
	            PageSize: sortInfo.RecordsToTake
	        };

	        // Copy over criteria properties
	        $.extend(newCriteria.Data, criteria);

	        return commService.get({
	            operationTitle: "Getting merge statuses",
	            url: "uiapi/MergeStatus",
	            data: newCriteria
	        });
	    };

	    //#endregion

	    service.prototype.setMergeRequestAssignment = function (mergeRequestId, setAssigned) {
	        var message = {
	            MergeRequestId: mergeRequestId,
	            SetAssignment: setAssigned
	        };

	        return commService.put({
	            operationTitle: "Merge request - Set assignment",
	            url: "uiapi/MergeRequestAssignment",
	            data: message
	        });
	    };

	    service.prototype.discardMergeRequest = function (mergeRequestId) {

	        return commService.put({
	            operationTitle: "Discarding merge request",
	            url: "uiapi/MergeRequest/" + mergeRequestId// + "/Discard",
	        });

	    };

	    service.prototype.denyMergeRequest = function (mergeRequestId, mergeDenialReason) {
	    	return commService.patch({
	            operationTitle: "Denying merge request",
	            url: "uiapi/MergeRequest/?requestID=" + mergeRequestId + "&deniedReason=" + mergeDenialReason,
//	    		data: {
//	    			MergeRequestId: mergeRequestId,
//	            	mergeDeniedReason: mergeDenialReason
//	            }
	        });
	    };

	    service.prototype.submitManualInterventionResult = function (mergeRequestId, selectedStudentIds, fieldSelections) {


	        return commService.post({ // TODO: Use a correct verb...
	            operationTitle: "Submitting manual intervention result",
	            url: "uiapi/MergeRequestManualProcess",// + mergeRequestId,
	            data: {
	                MergeRequestId: mergeRequestId,
	                SelectedNsns: selectedStudentIds,
	                FieldSelections: fieldSelections
	            }
	        });
	    };

	    service.prototype.checkNSNIncludedInMergeRequest = function (nsn) {
			
	    	return commService.get(
				{
					operationTitle: "Checking if NSN exists in manual intervention merge request",
					url: "uiapi/MergeStatus/?nsn=" + nsn
				});
	    };

	    // ----- Merge Denied Pairs ------

	    /** Get merge denied pair for a single NSN, or array of NSNs 
		*/
	    service.prototype.getMergeDeniedPairs = function (nsns) {

	        // Ensure the property is always an array
	        if (!(nsns instanceof Array)) {
	            nsns = [nsns];
	        }

	        return commService.get(
				{
				    operationTitle: "Getting MergeDeniedPairs",
				    url: "uiapi/MergeDeniedPair/?id=" + nsns
				});
	    };

	    /** Create merge denied pair
		*/
	    service.prototype.createMergeDeniedPair = function (studentId1, studentId2, reasonId) {
	        return commService.post(
				{
				    operationTitle: "Creating merge denied pair",
				    url: "uiapi/MergeDeniedPair",
				    data: {
				    	StudentId1: studentId1,
				    	StudentId2: studentId2,
				        MergeDeniedReason: reasonId
				    }
				});
	    };

	    /** Delete a merge denied pair
		*/
	    service.prototype.deleteMergeDeniedPair = function (mergedDeniedPairId) {

	        return commService.doDelete(
				{
				    operationTitle: "Deleting merge denied pair",
				    url: "uiapi/MergeDeniedPair/" + mergedDeniedPairId
				});
	    };

	    // -- Merge Decision page

	    service.prototype.getMergeDecisionPage = function (mergeDecisionId) {

	        return commService.get({
	            operationTitle: "Getting merge decision",
	            url: "uiapi/MergeDecisionPage/" + mergeDecisionId
	        });

	    };

	    // -- Unmerge

	    service.prototype.unmergeStudent = function (slaveStudentId, mergeDenialReason) {

	        return commService.post({
	        	operationTitle: "Unmerging NSN: " + slaveStudentId,
	            url: "uiapi/UnmergeStudent/",
	            data: {
	            	Id: slaveStudentId,
	                DenialReason: mergeDenialReason
	            }
	        });
	    };


		// -- MergeRequestDenied (used in Batch - Results)

		service.prototype.getMergeRequestDenied = function(fromDate) {

			return commService.get({
				operationTitle: "Getting Merge Request Denied",
				url: "uiapi/MergeRequestDenied/",
				data: { FromDate: fromDate }
			});
		};

	    // -- End

	    return new service();
	});

