﻿
define(['durandal/system',
		'services/commService'],
	function (system, commService) {

		var service = function () {
		};

		service.prototype.getChallengeChangeForStudent = function(nsn) {

			return commService.get({
				operationTitle: "Getting challenge changes for student",
				url: "uiapi/ChallengeChangeSet/" + nsn
			});
		};

		/** Submit Challenge Change
		*/
		service.prototype.submitChallengeChange = function (nsn, changeType, reason, emailaddress) {

			return commService.post(
				{
					operationTitle: "Submitting Challenge Change",
					url: "uiapi/ChallengeChange",
					data: {
						NSN: nsn,
						ChallengeTypeFK: changeType,
						ReasonforChallengeChange: reason,
						EmailAddress: emailaddress
					}
				});
		};

		service.prototype.updateChallengeChange = function(challenge) {

			return commService.patch({
				operationTitle: "Updating challenge change",
				url: "uiapi/ChallengeChange/" + challenge.Id(),
				data: { "": challenge.ChallengeStatusFK() }
			});
		};

		return new service();
	});

