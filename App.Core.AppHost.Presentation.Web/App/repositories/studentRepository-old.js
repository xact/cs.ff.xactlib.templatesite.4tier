define(['entities/student'],
	function (studentSummary) {

		var service = function(studentSummary) {
			
			this._lastId =3;

			this._records = [
				{nsn: 1, score:100, familyName: "Brown", givenName:'James', dob:'', nameAndDobVerification:1, gender: 1, residentialStatus:3, created: null , residentialStatusVerification:2, createdBy: 'ABC, Ltd', },
				{nsn: 2, score:80, familyName: "Boop", givenName:'Betty', dob:'', nameAndDobVerification:2, gender: 2, residentialStatus:2, created: null , residentialStatusVerification:1, createdBy: 'Bongo, Ltd', },
				{nsn: 3, score:70, familyName: "Field", givenName:'Sally', dob:'', nameAndDobVerification:3, gender: 3, residentialStatus:1, created: null , residentialStatusVerification:3, createdBy: 'Drums, Assoc.', }
			];
		};



		
        service.prototype.getAll=function () {
            return this._records;
        };

        service.prototype.add = function (entity) {
            entity.nsn = ++this._lastId;
            this._records.push(entity);
        };

        service.prototype.getSingleById = function (id) {
			var results = [];
			
            for (var i = 0; i < this._records.length; i++) {
                if (this._records[i].nsn == id) {
                    return this._records[i];
                }
            }
        };
		
		
		//Note: returns a normal array (not a searchResponse)
		service.prototype.getBySearchRequest = function(searchRequest){
			var results = [];
			if (searchRequest.mode() == "nsn"){
				
				var nsn = searchRequest.nsn();
				
				for (var i = 0; i < this._records.length; i++) {
					if (  this._records[i].nsn == nsn) {
						results.push(this._records[i]);
						break;
					}
				}
			}else{
				var pattern = new RegExp(searchRequest.name(),"i");
				
				for (var i = 0; i < this._records.length; i++) {
					if ( (pattern.test(this._records[i].familyName)) ||(pattern.test(this._records[i].givenName)) ) {
						results.push(this._records[i]);
						continue;
					}
				}
			}
			
			//Convert array of JSON to array of obserable entities:
			for(var i=0;i<results.length;i++){
				var data = results[i];
				//Convert from JS to observable:
				var record = new studentSummary();
				results[i] = ko.mapping.fromJS(data, null, record);
			}
			
			//return array of observable objects:
			return results;
		}

		return new service();
    }
);