﻿
define(['durandal/system',
		'services/commService'],
	function (system, commService) {

		var service = function () {
		};

		service.prototype.getChangeNotifications = function(criteria) {

			return commService.get({
				operationTitle: "Getting change notifications",
				url: "uiapi/ChangeNotifications/",
				data: criteria
			});
		};

		service.prototype.hasAnyChangeNotifications = function(fromDate) {

			return commService.get({
				operationTitle: "Checking for change notifications",
				url: "uiapi/ChangeNotificationsCheck",
				data: {
					FromDate: fromDate
				}
			});
		};

		return new service();
	});

