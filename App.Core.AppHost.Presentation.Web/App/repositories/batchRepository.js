﻿define(['durandal/system',
		'services/commService'],
	function (system, commService) {

		var service = function () {
		};

		service.prototype.getBatchRequests = function (sortInfo,params) {

			// Move sort info to new schema
			var newCriteria = {
				Data: params,
				pageIndex: sortInfo.PageIndex,
				pageSize: sortInfo.RecordsToTake

			};

			
			return commService.get({
				operationTitle: "Searching batch requests",
				url: "uiapi/BatchRequest/",
				data: newCriteria
			});
		};

		service.prototype.setDownloaded = function (batchId) {

			return commService.put({
				operationTitle: "Uploading batch requests",
				//url: "uiapi/BatchRequest/",
				url: "uiapi/BatchRequest/?batchId=" + batchId,
				data: batchId
			});
		};


		return new service();


	});