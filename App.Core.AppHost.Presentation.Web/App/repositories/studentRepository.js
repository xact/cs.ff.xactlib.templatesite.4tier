﻿
define(['durandal/system',
		'services/commService'],
	function(system, commService) {

		var service = function() {
		};

		/// Gets a single student by NSN
		service.prototype.getStudentByNSN = function(nsn) {

			var self = this;

			return commService.get(
				{
					operationTitle: "Getting Student By NSN",
					url: "uiapi/student/" + nsn
				});
		};

		/// Gets a single student by NSN
		service.prototype.checkStudentByNSN = function(nsn) {

			var self = this;

			return commService.get(
				{
					operationTitle: "Getting Student By NSN",
					url: "uiapi/StudentCheck/" + nsn
				});
		};

		//
		service.prototype.getStudentSummary = function(nsn) {

			return commService.get({
				operationTitle: "Getting Student summary by NSN",
				url: "uiapi/StudentSummary/" + nsn
			});

		}

		service.prototype.updateStudent = function(student) {
			return commService.put({
				operationTitle: "Updating student",
				url: "uiapi/Student/",
				data: student
			});
		};
		
		service.prototype.addStudent = function(student) {
			return commService.post({
				operationTitle: "Adding student",
				url: "uiapi/Student/",
				data: student
			});
		}

		/// Performs a fuzzy search of the student dataset
		service.prototype.searchStudents = function(criteria) {

			return commService.get(
				{
					operationTitle: "Running student search",
					url: "uiapi/Students",
					data: criteria
				}).done(function(data) {
					system.log('Search students complete. ' + (data && data.Data ? data.Data.length : 0) + ' records returned');
				});

		};

		/// Gets a single student by NSN
		service.prototype.getStudentAuditByNSN = function(nsn) {

			var self = this;

			return commService.get(
				{
					operationTitle: "Getting Student Audit By NSN",
					url: "uiapi/studentAudit/" + nsn
				});
		};


	    /// Gets Student Update History by NSN
		service.prototype.getStudentUpdateHistory = function (nsn) {

			

			return commService.get(
				{
				    operationTitle: "Getting Student Update History By NSN",
				    url: "uiapi/StudentUpdateHistory/" + nsn
				}).done(function(result) {
				    system.log('Student Update History complete. ' + (result ? result.Data.length : 0) + ' records returned');

				});
		};
	    
	    /// Gets Student Merge History by NSN
		service.prototype.getStudentMergeHistory = function (mergeRequestId) {

		  return commService.get(
				{
				    operationTitle: "Getting Student Merge History By NSN",
				    url: "uiapi/StudentMergeHistory/" + mergeRequestId
				}).done(function(result) {
				    system.log('Student Merge History complete. ' + (result ? result.length : 0) + ' records returned');

				});
		};

		return new service();
	});

