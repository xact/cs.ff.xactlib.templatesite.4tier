﻿define(['durandal/system',
		'services/commService'],
	function (system, commService) {

		var service = function () {
		};

		service.prototype.searchProviderRequests = function (criteria, sortInfo) {

			// Move sort info to new schema
			var newCriteria = {
				Data: criteria,
				pageIndex: sortInfo.PageIndex,
				pageSize: sortInfo.RecordsToTake
			};

			// Copy over criteria properties
			$.extend(newCriteria.Data, criteria);

			return commService.get({
				operationTitle: "Searching by Provider",
				url: "uiapi/Providers/",
				data: newCriteria
			});
		};


		service.prototype.updateProvider = function(updatedProvider) {

			return commService.put({
				operationTitle: "Updating Provider",
				url: "uiapi/Provider/",
				data: updatedProvider
			});
		};

		return new service();


	});