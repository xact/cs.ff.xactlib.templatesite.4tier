﻿
define(['durandal/system',
		'services/commService'],
	function(system, commService) {

		var service = function() {
		};

		var sprApiBase = "uiapi/StudentProviderRelationship/";
		var sprCountApiBase = "uiapi/StudentProviderRelationshipCounts/";

		service.prototype.getSPRs = function (studentId) {

			return commService.get({
				operationTitle: "Getting Student Provider relationships",
				url: sprApiBase + studentId
			});

		};

		service.prototype.getSprCounts = function(studentIds) {
			// Ensure the property is always an array
			if (!(studentIds instanceof Array)) {
				studentIds = [studentIds];
			}

			return commService.get({
				operationTitle: "Getting SPR counts",
				url: sprCountApiBase + "?id=" + studentIds
			});
		};

		service.prototype.addSPR = function(studentId, providerCode) {

			return commService.post({
				operationTitle: "Adding SPR",
				url: sprApiBase,
				data: {
					StudentId: studentId,
					ProviderCode: providerCode
				}
			});
		};

		service.prototype.updateSPR = function(spr) {

			return commService.put({
				operationTitle: "Updating SPR",
				url: sprApiBase,
				data: spr
			});
		};

		return new service();
	});
