﻿define(['durandal/system',
		'services/commService'
],
	function (system, commService) {

		var service = function () {
		};

		service.prototype.getAllParameterSettings = function (id) {
		
			return commService.get(
			{
				operationTitle: "Getting All Parameters Settings:" + id,
				url: "uiapi/Parameter/?id=" + id

			});

		};

		return new service();

	});