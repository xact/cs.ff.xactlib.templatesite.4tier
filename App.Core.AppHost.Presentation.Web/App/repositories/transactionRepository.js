﻿define(['durandal/system',
		'services/commService'],
	function (system, commService) {

		var service = function () {
		};
		
		service.prototype.getTransactionResults = function (params) {

			return commService.get({
				operationTitle: "Getting transaction results:",
				url: "uiapi/TransactionReports/",
				data: params
			});
		};

		return new service();


	});