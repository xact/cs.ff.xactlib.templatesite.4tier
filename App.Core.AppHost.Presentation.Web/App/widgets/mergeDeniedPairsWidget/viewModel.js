﻿define(['services/logger',
		'services/sessionService',
		'services/mergeService',
		'services/resourceService',
		'data/settings'
		],

	function (logger, sessionService, mergeService, resourceService, settings) {

		var ctor = function () { };


		ctor.prototype.activate = function (widgetSettings) {
			var self = this;

		    // -- Properties
			this.resources = resourceService.resources;
			this.widgetSettings = widgetSettings;

			// Merge denied pairs are just shown in the order they're presented
			this.mergeDeniedPairs = widgetSettings.items;

			//// Not currently in use - But they may re-add it to the spec at some point.

			//this.sortedMergeDeniedPairs = ko.computed(function () {
			//	if (!self.mergeDeniedPairs()
			//		|| self.mergeDeniedPairs().length === 0) { // Test array === 0

			//		return null;
			//	}

			//	// Sort by NSN
			//	return self.mergeDeniedPairs().sort(function (l, r) {
			//		if (l.Student1Nsn === r.Student2Nsn) {
			//			return l.Student2Nsn > r.Student2Nsn ? 1 : -1;
			//		}

			//		return l.Student1Nsn > r.Student1Nsn ? 1 : -1;
			//	});
			//});


			// -- Security trimming
			self.showRemoveMergeDenied = sessionService.isAuthorised(settings.securityClaims.NSI2_MERGE_DENY);

			self.isVisible = ko.computed(function() {
				return self.mergeDeniedPairs().length !== 0;
			});

			// -- Actions
			self.onDeleteMergeDeniedPair = function (mergeDeniedPair) {
				mergeService.deleteMergeDeniedPair(mergeDeniedPair.Id).done(function () {
					self.mergeDeniedPairs.remove(mergeDeniedPair);
					var message = resourceService.get("MessageCode_Confirmation_DeniedRemovedNSN_058").format(mergeDeniedPair.Student1Nsn, mergeDeniedPair.Student2Nsn);
					logger.log(message, null, "mergeDeniedPairWidget", true);
				});
			};


			
		};
		
		return ctor;
	});