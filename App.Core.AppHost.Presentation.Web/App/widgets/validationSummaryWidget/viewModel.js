﻿define([],

	function () {

		var ctor = function () { };


		ctor.prototype.activate = function (settings) {
			var self = this;

		    // -- Properties
			self.validationErrors = settings.validationErrors;
			self.headerText = settings.headerText ? settings.headerText : "Error:";

			// Default 'is validation section visible' logic
			self.isVisible = ko.computed(function () {
				return self.validationErrors && self.validationErrors().length > 0;
			});
			
			// Allow 'isVisible' logic to be overridden
			if (settings.isVisible) {
				self.isVisible = settings.isVisible;
			}

		};
		
		return ctor;
	});