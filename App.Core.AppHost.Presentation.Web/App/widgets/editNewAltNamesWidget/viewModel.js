﻿define(['services/logger',
		'services/sessionService',
		'services/mergeService',
		'services/resourceService',
		'services/referenceService',
		'data/settings',
		'data/enums',
		'libs/uiLib',
        'singletons/student',
		'singletons/session'
],

	function (logger, sessionService, mergeService, resourceService, referenceService, settings, enums, uiLib, currentStudent, session) {

		var ctor = function () { };


		ctor.prototype.activate = function (config) {
			var self = this;

			// -- Properties
			self.title = config.title;

			self.newAltNames = config.newAltNames;

			self.parentPage = config.parentPage;

			self.triggerValidationConditions = config.triggerValidationConditions ? config.triggerValidationConditions : true;

			self.nameDobVerification = config.nameDobVerification ? config.nameDobVerification : referenceService.getAvailableNameDobVerifications();

			//The editStudent page requires security trimming, while the addStudent doesnt show created by.
			self.enableCreatedBy = self.parentPage == enums.parentPage.editStudent ? sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_ALT_NAME_CREATED) : false;

			self.initialEmptyAltName = config.initialEmptyAltName;

			self.referenceData = {
				nameDobVerification: self.nameDobVerification,
				linkedProviders: config.studentProviderRelationships
			};

			self.shouldViewTable = ko.computed(function () {
				return self.parentPage == enums.parentPage.editStudent ? self.newAltNames().length > 0 : true;
			});

			var newAltName = function () {

				// If the user has permission to change the 'created by provider', default it to the currently logged in provider
				var currentUserProviderId = "";
				if (self.enableCreatedBy) {
					currentUserProviderId = session.user().OrgId;
				}

				var emptyName = {
					CreatedByProviderFK: ko.numericObservable(currentUserProviderId),
					CreatedDateTime: ko.observable(),
					FamilyName: ko.observable(),
					GivenName1: ko.observable(),
					GivenName2: ko.observable(),
					GivenName3: ko.observable(),
					GivenNames: ko.observable(),
					Id: 0,
					NameBirthDateVerificationMethodFK: ko.numericObservable(enums.nameBirthDateVerificationMethod.unverified),
					NameBirthDateVerificationMethod: ko.observable(),
					Nsn: 0,
					PreferredNameIndicator: ko.observable(),
					PreferredNameIndicatorDisplay: ko.observable(),
					CreatedOn: ko.observable()
				};

				//var testRequiredMessage = "{0} is a required field";
				//emptyName.GivenName1.extend({
				//	required: {
				//		param: true,
				//		message: testRequiredMessage.format("Given name 1")
				//	}
				//});

				//emptyName.FamilyName.extend({
				//	required: {
				//		param: true,
				//		message: testRequiredMessage.format("Family name")
				//	}
				//});

				//emptyName.GivenName1.isModified(false);
				//emptyName.FamilyName.isModified(false);


				////Vaidation Rules specific to EditStudent Page
				if (self.parentPage == enums.parentPage.editStudent) {
					//todo:Add validation rules specific to EditStudent page, confirm that validations are common for add/edit, so remove if condition
					emptyName.CreatedOn.extend({
						validNzDate: {
							message: resourceService.get("MessageCode_Validation_Date_001").format("Created date"),
							onlyIf: function () { return self.checks.canEditCreatedBy && emptyName.CreatedOn(); } // Only validate when populated
						},
						nzDate: {
							message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Created date"),
							onlyIf: function () { return self.checks.canEditCreatedBy && emptyName.CreatedOn(); } // Only validate when populated
						},
						dateCannotEarlierThan: {
							params: currentStudent.CreatedOn,
							message: resourceService.get("MessageCode_Validation_verificationDateEarlierThanCreationAltName_936"),
							onlyIf: function () { return self.checks.canEditCreatedBy && emptyName.CreatedOn(); }
						},
						maxDateIsToday: {
							message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Created date"),
							onlyIf: function () { return self.checks.canEditCreatedBy && emptyName.CreatedOn(); } // Only validate when they have access to this, and it's populated
						}
					});

					emptyName.CreatedDateTime.extend({
						validNzDate: {
							message: resourceService.get("MessageCode_Validation_Date_001").format("Created date"),
							onlyIf: function() { return self.checks.canEditCreatedBy && emptyName.CreatedDateTime(); } // Only validate when populated
						},
						nzDate: {
							message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Created date"),
							onlyIf: function () { return self.checks.canEditCreatedBy && emptyName.CreatedDateTime(); } // Only validate when populated
						},
						dateCannotEarlierThan: {
							params: currentStudent.CreatedOn,
							message: resourceService.get("MessageCode_Validation_verificationDateEarlierThanCreationAltName_936"),
							onlyIf: function () { return self.checks.canEditCreatedBy && emptyName.CreatedDateTime(); }
						},
						maxDateIsToday: {
							message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Created date"),
							onlyIf: function () { return self.checks.canEditCreatedBy && emptyName.CreatedDateTime(); }
						}
					});


				}
					////Vaidation Rules specific to AddStudent Page
				//else if (self.parentPage == enums.parentPage.addStudent) {

					//FS1074
					emptyName.FamilyName.extend({
						required: {
							message: "<li>Family name</li>",
							onlyIf: function () {
								return self.triggerValidationConditions() &&
										(emptyName.GivenName1() ||
										emptyName.GivenName2() ||
										emptyName.GivenName3() ||
										emptyName.NameBirthDateVerificationMethodFK() != enums.nameBirthDateVerificationMethod.unverified ||
										emptyName.PreferredNameIndicator()
									);
							}
						}
					});

					emptyName.GivenName1.extend({
						required: {
							message: "<li>Given name (1)</li>",
							onlyIf: function () {
								return self.triggerValidationConditions() &&
											(emptyName.FamilyName() ||
											emptyName.GivenName2() ||
											emptyName.GivenName3() ||
											emptyName.NameBirthDateVerificationMethodFK() != enums.nameBirthDateVerificationMethod.unverified ||
											emptyName.PreferredNameIndicator()
										);
							}
						}
					});
				//}

				////Vaidation Rules that applies for all pages.
				emptyName.CreatedOn.extend({
					maxDateIsToday: {
						//message: "Birth date cannot be later than current date",
						message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Created by date"),
						onlyIf: function () { return self.checks.canEditCreatedBy && emptyName.CreatedOn(); } // Only validate when they have access to this, and it's populated
					}
				});

				uiLib.setCommonNameValidation(emptyName.GivenName1, "Given name (1)", { allowSingleTilde: true });
				uiLib.setCommonNameValidation(emptyName.GivenName2, "Given name (2)");
				uiLib.setCommonNameValidation(emptyName.GivenName3, "Given name (3)");
				uiLib.setCommonNameValidation(emptyName.FamilyName, "Family name");

				uiLib.setTildeRuleValidation(emptyName);

				return emptyName;
			};


			// -- Security trimming
			self.checks = {
				canEditCreatedBy: self.enableCreatedBy
			};


			// -- Actions
			self.onAddAltName = function () {
				var blankAltName = newAltName();
				self.newAltNames.push(blankAltName);

				//initialise AltName required fields
				self.newAltNames()[self.newAltNames().length - 1].FamilyName("");
				self.newAltNames()[self.newAltNames().length - 1].GivenName1("");
				$.placeholder.shim();
				//$("select").each(function () {
				//	$(this.options).first().attr("selected", "selected");
				//});
			};

			self.onDeleteNewAltName = function (item) {
				self.newAltNames.remove(item);

				//// If the list is now empty, add a new blank item
				//if (self.newAltNames().length === 0) {
				//	self.onAddAltName();
				//}
			};

			if (self.initialEmptyAltName) {
				self.onAddAltName();
			}

			//// On startup - Always have a empty record available
			//self.onAddAltName();

		};

		return ctor;
	});