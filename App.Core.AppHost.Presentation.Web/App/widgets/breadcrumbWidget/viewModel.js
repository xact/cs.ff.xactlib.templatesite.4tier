﻿define(['plugins/router',
	'libs/navLib'],

	function (router, navLib) {

		var ctor = function () { };

		// Options fo settings:
		// .breadcrumbChain - complete breadcrumb chain
		// .skipPlaceholderNode

		ctor.prototype.activate = function (settings) {
			var self = this;

			var currentRoute = router.activeInstruction().config;

			self.breadcrumbChain = [];

			// -- Properties
			if (settings.breadcrumbChain) {
				// Accept an override for the breadcrumb trail:
				self.breadcrumbChain = settings.breadcrumbChain;
			} else {
				self.breadcrumbChain = getBreadcrumbChain(currentRoute);
			}
			
			self.doClickAction = function () {
				//Making sure the navLib init runs once.
				if (!navLib.beenActivated()) {
					navLib.init();
					navLib.beenActivated(true);
				}
				
				navLib.triggerAll();
				return true;
			};
		};



		// Calculate the default breadcrumb chain
		function getBreadcrumbChain(currentRoute) {
			var breadcrumbChain = [];

			// Current page, with no link
			breadcrumbChain.push({ name: getName(currentRoute), link: '' });

			var traverseRoute = currentRoute;
			// Enter all parent items
			while (traverseRoute.parent) {
				traverseRoute = traverseRoute.parent;

				// Skip items that are 'shells' for the main nav
				if (traverseRoute.isDisplayShell) {
					continue;
				}

				// Add the parent at the *start* of the array, for correct display
				var name = getName(traverseRoute);
				var link = getLink(traverseRoute.hash);
				breadcrumbChain.unshift({ name: name, link: link });
			}

			// Welcome ('Home') is always the root.
			if (currentRoute.link !== "#"
				&& currentRoute !== "#/welcome") {

				breadcrumbChain.unshift({ name: "Home", link: '#/welcome' });
			}

			return breadcrumbChain;
		}

		function getName(route) {
			return route.breadcrumb ? route.breadcrumb : route.name;
		}

		function getLink(link) {
			// Check for params in the has (e.g: 'page/:id')
			var indexOfColon = link.indexOf(":");

			if (indexOfColon <= 0) {
				return link;
			}

			// Replace the param component with the current page's param
			// NOTE: Works for only single params at the moment. No known need for fancier support yet.
			var strippedLink = link.substring(0, indexOfColon);
			var params = router.activeInstruction().params;
			var idParam = params.length > 0 ? params[0] : "";

			return strippedLink + idParam;
		};

		return ctor;
	});