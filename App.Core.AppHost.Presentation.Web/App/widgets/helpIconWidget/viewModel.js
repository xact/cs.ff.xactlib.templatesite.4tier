﻿define([],

	function () {

		var ctor = function () { };


		ctor.prototype.activate = function (settings) {
			var self = this;

		    // -- Properties
			self.helpText = settings.helpText;
			self.styledHelpText = settings.styledHelpText;

			self.labelledBy = settings.labelledBy ? settings.labelledBy : "";

			self.hasDatepicker = settings.hasDatepicker;

		    self.tooltipCss = settings.hasDatepicker ? "tooltip hasDatepicker" : "tooltip";
		};

		return ctor;
	});