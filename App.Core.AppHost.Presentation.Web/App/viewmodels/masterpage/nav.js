﻿define(['services/logger',
		'services/sessionService',
		'services/navigationService',
		'libs/navLib',
		'singletons/parameters'],
    function (logger, sessionService, navigationService, navLib, parameters) {

    	var model = function () {
    		var self = this;

    		self.session = sessionService.getSession();
    		self.user = self.session.user;
    		self.currentParameters = parameters;
			

    		self.visibleRoutes = navigationService.getVisibleRoutes();

    		
    		self.checkAvailable = function (data) {

				//Making sure the navLib init runs once.
    			if (!navLib.beenActivated()) {
    				navLib.init();
    				navLib.beenActivated(true);
    			}
    			
    			if (data.afterNavAction) {
    				navLib.trigger(data.afterNavAction);
    			}
    			
    			// TODO: Remove before golive
    			// Give a polite message for actions that aren't yet available
    			if (data.notYetImplemented) {
    				logger.log(data.name + " is not yet available", null, "nav", true);
    				return false;
    			}

    			else if (data.route == 'admin/AdminReports') {
    				//window.location.href = self.currentParameters.get('SSRS_landing_page')();
    				window.open(self.currentParameters.get('SSRS_landing_page')(), '_blank');
    				
    				return false;
			    }
    			
    			// Allow normal actions through
    			return true;
    		};
    	};

    	return model;
    });