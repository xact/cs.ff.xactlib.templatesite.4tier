﻿define(['services/resourceService'],
    function (resourceService) {

        var vm = function () {
            var self = this;
            this.resources2 = resourceService.resources;
        };
        return vm;
    });