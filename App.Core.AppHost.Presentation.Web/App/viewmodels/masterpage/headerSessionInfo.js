﻿define(
	['durandal/app',
		'durandal/system',
		'plugins/router',
		'data/settings',
		'services/sessionService',
	'services/logger',
	'services/resourceService'],
	function (app, system, router, settings, sessionService, logger, resourceService) {

	    var model = function () {
		    var self = this;

	        this.view = {
	            name: 'Session Information',
	            description: null,
	        };

	        this.resources2 = resourceService.resources;
	        this.session = sessionService.getSession();

	        this.user = this.session.user;
	    	//this.showSessionInfo = this.user().IsAuthenticated;

	        this.loggedOnAs = ko.computed(function () {
		        var currentUser = self.user();

		        if (!currentUser
	        		|| !currentUser.IsAuthenticated) {
	        		return "(Unknown)";
	        	}
		        
				// Just in case we lose OrgCode...
		        if (currentUser.DisplayName
	        		&& !currentUser.OrgCode) {
			        return currentUser.DisplayName;
		        }
		        
			    // For non MoE (-1) organisations - Display the Org number in brackets
		    	if (self.user().OrgCode !== "-1") {
		    		return self.user().DisplayName + " (" + self.user().OrgCode + ")";
			    }

			    // For MoE, don't display the org number
		    	if (self.user().OrgCode === "-1") {
		    		return self.user().DisplayName;
			    }

			    // Fallback
			    return "(Unknown)";
		    });
	    };

		

	    model.prototype.compositionComplete = function (view) {
	        //you can get the view after it's bound and connected to it's parent dom node if you want
	    };

	    model.prototype.onLogout = function () {
	        sessionService.logout('088');
	        //logger.log('You have been logged out of the NSI', null, '', true);
	    };

	    //return singleton:
	    return new model();
	});