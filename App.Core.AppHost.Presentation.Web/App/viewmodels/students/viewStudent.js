 define(['plugins/router',
        'durandal/app',
		'services/logger',
		'services/resourceService',
		'services/studentService',
        'services/sessionService',
		'services/mergeService',
		'services/studentProviderRelationshipService',
		'singletons/student',
        'viewModels/merge/mergeDenialReasonModal',
		'viewModels/students/studentProviderRelationshipsModal',
		'data/enums'],
    function (router, app, logger,
	    resourceService, studentService, sessionService, mergeService, sprService,
	    student,
		unmergeModalWindow, studentProviderRelationshipModal, enums) {
	    
        var viewModel = function () {
            var self = this;

			// -------------
        	// -- Properties
	        
            var settings = com.nsi.frontend.data.settings;

            // -------------
            // -- Main entity - Current student
            self.currentStudent = student;

            //Runtime resource management
            self.resources = resourceService;
        	
            self.showUnmergeSuccessMsg = ko.observable(false);
            self.showSavedMsg = ko.observable(false);

            self.showSuccessMessage = ko.computed(function () {
	            return self.showUnmergeSuccessMsg() || self.showSavedMsg();
	            //student.hasBeenJustAddedOrUpdated();
            });

            self.successMessage = ko.computed(function () {
            	if (self.showUnmergeSuccessMsg()) {
            	   return resources.get('MessageCode_Information_RedirectAfterUnmerge_924')().format(self.currentStudent.SlaveNSNBeforeUnMerge, self.currentStudent.NSN());
            	}
	            else if (self.showSavedMsg()) {
		            return resources.get('MessageCode_Confirmation_StudentRecordSaved_025')();
	            }
	            
            });

	        self.sprCount = ko.observable(0);

			// ----------------
            // -- Page lifecycle
            self.activate = function (nsn) {
                updateSprCount();
                if (!(self.currentStudent.SlaveNSNBeforeUnMerge == undefined || self.currentStudent.SlaveNSNBeforeUnMerge == null)) {
                	self.showUnmergeSuccessMsg(true);
					//This field needs to cleared since message 924 should be displayed just once.
	                self.currentStudent.SlaveNSNBeforeUnMerge = null;
                }
				if (student.hasBeenJustAddedOrUpdated()) {
					self.showSavedMsg(true);
					//This field needs to be cleared since the message 025 should be displayed just once.
					student.hasBeenJustAddedOrUpdated(null);
				}
            };

            self.canActivate = function (nsn) {
                var promise = $.Deferred();

	            studentService.setCurrentStudentByNSN(nsn).done(function(student) {

		            if (!student) {
			            logger.logError(resourceService.get("MessageCode_Validation_InvalidNSN_336"), "", "studentView", true);
			            promise.resolve({ redirect: "#/students/search" });
			            return;
		            }
		            promise.resolve(true);
	            });

                // Set the current (app-wide) student to the requested NSN

                return promise.promise();
            };

			
            self.sortedSlaveRecords = ko.computed(function () {
            	return self.currentStudent.Slaves().sort(function (left, right) {
            		return left.Id() == right.Id() ?
                        0 :
                        (left.Id() > right.Id() ? -1 : 1);
            	});
            });

			// ----------
            // -- Page display control properties
            self.shouldShowHistory = sessionService.isAuthorised(settings.securityClaims.NSI2_SEARCH_VIEW);
            //self.shouldShowHistory = function() { return true; };

            self.shouldShowChallenge = sessionService.isAuthorised(settings.securityClaims.NSI2_CHALLENGE_STUDENT);

            self.shouldShowRelationships = sessionService.isAuthorised(settings.securityClaims.NSI2_VIEW_SPR);

            self.shouldShowModify = sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_STUDENT);

            self.shouldEnableModify = ko.computed(function () {
                return self.currentStudent.isMaster() ?
            		(self.currentStudent.StudentStatusFK() === enums.studentStatus.inactive
            			? (sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_INACTIVE_STUDENT) && self.currentStudent.StudentStatusReasonFK() !== enums.studentStatusReason.slave ?
            		    true : self.currentStudent.StudentStatusReasonFK() === enums.studentStatusReason.slave ? true : false)
            			: true) : false;
            }, this);

            self.shouldShowUnmerge = ko.computed(function () {
	            return (sessionService.isAuthorised(settings.securityClaims.NSI2_MERGE_UNMERGE)
						&& self.currentStudent.MasterNSN());
            }, this);

            self.shouldShowNameDobConfirmed = ko.computed(function () {
                return self.currentStudent.NameBirthDateVerificationMethodFK() !== enums.nameBirthDateVerificationMethod.unverified;
            }, this);
            
            self.shouldShowResidentialStatusConfirmed = ko.computed(function () {
                return self.currentStudent.ResidentialStatusVerificationMethodFK() !== enums.residentialStatusVerificationMethod.unverified;
            }, this);
            

            self.isDeathDateVisible = ko.computed(function () {
            	return self.currentStudent.StudentStatusFK() === enums.studentStatus.inactive && self.currentStudent.StudentStatusReasonFK() === enums.studentStatusReason.deceased;
            }, this);

            self.isSlaveRecordsVisible = ko.computed(function () {
            	if (self.currentStudent.Slaves().length === 0) {
            		return false;
            	} else {
            		return true;
            	}
            }, this);
	        
            self.isChallengeChangeVisible = ko.computed(function () {
                if (self.currentStudent.Challenges().length === 0) {
                    return false;
                } else {
                    return true;
                }
            }, this);

			// ----------
			// -- Actions
            self.navToStudentHistory = function () {
                router.navigate('/students/history/' + self.currentStudent.NSN());
            };

            self.navToChallengeChange = function () {
            	router.navigate('/students/challengeChange/' + self.currentStudent.NSN());
            };

            self.navigateToSlaveDetails = function (nsn) {
            	router.navigate('/students/view/' + nsn);

            };

	        self.navToModifyStudent = function(nsn) {
	        	router.navigate('/students/edit/' + self.currentStudent.NSN());
	        };

	        self.onShowStudentProviderRelationships = function() {
		        studentProviderRelationshipModal.show({
		        	studentId: self.currentStudent.Id(),
		        	studentNsn: self.currentStudent.NSN(),
		        	studentName: self.currentStudent.GivenName1() + " " + self.currentStudent.FamilyName()
		        }).done(updateSprCount); // Update SPR count after
	        };

            self.showUnmergeModal = function () {
            	unmergeModalWindow.show().done(function (denialReasonId) {
            		// If nothing was selected, close
            		if (!denialReasonId) {
            			return false;
            		}
                    var masterNsnBeforeUnMerge = self.currentStudent.MasterNSN();
            		mergeService.unmergeStudent(self.currentStudent.Id(), denialReasonId).done(function () {
            		    self.currentStudent.SlaveNSNBeforeUnMerge = self.currentStudent.NSN();
            		    router.navigate('/students/view/' + masterNsnBeforeUnMerge);
			            //self.showUnmergeSuccessMsg(true);
		            }).fail(function () {
            			logger.logError("Unmerge outcome uncertain - Updated student details are unavailable. Please refresh the page.", null, null, true);
            		});
            	});
            	// Remember: Also add the merge denied pair back to this page's list
            };
	        
			// -----------
            // -- Messages
            /*
            Story:
            Message#018 comes with parameter {0}, on which MasterNSN will be applied here.
            But the issue is need to provide hyper-link only to part of the string (which is MasterNSN)
            So the tweak is, need to format with MasterNSN value with the original message#018 to avoid {0} displaying in page,
            then split and send the part message only except MasterNSN
            and then link the MasterNSN in html seperately.
            So easy isnt it? Not feel good, then please feel free to change it with your expertise..
            */
            self.slaveMessage = function () {
                var fullString = self.resources.get('MessageCode_Information_SlaveOfMaster_018').format(self.currentStudent.MasterNSN());
                var indexOfMasterNsn = fullString.indexOf(self.currentStudent.MasterNSN());
                return fullString.substring(0, indexOfMasterNsn);
            };

            self.ChallengesPagingText = ko.computed(function () {
                return self.resources.get('StudentDetailsView_ViewStudent_ChallengeChangeStatusPagingInfo');
            }, this); 

            self.AltNamesPagingText = ko.computed(function () {
                return self.resources.get('StudentDetailsView_ViewStudent_AltNamesPagingInfo');
            }, this);
                

	        var updateSprCount = function() {
	        	return sprService.getSprCounts(student.Id()).done(function (sprCounts) {
	        		var sprCountRecord = sprCounts[0];
	        		
	        		self.sprCount(sprCountRecord.SprCount);
	        	});
	        };

            
			// ------
            // Setting up the altnames datatable
            var altNamesTableConfig = {
                dataSource: self.currentStudent.AltNames,
                rowTemplate: 'studentAltNamesRowTemplate',
                columns: [
                    { bVisible: false, mDataProp: 'CreatedDateTime', sDefaultContent: "" }
					, { bSortable: false, mDataProp: 'GivenNames', sDefaultContent: "" }
					, { bSortable: false, mDataProp: 'Id', sDefaultContent: "" }
                    , { bSortable: false, mDataProp: 'PreferredNameIndicator', sDefaultContent: "" }
					, { bSortable: false, mDataProp: 'FamilyName', sDefaultContent: "" }
                ],
                options: {
                    // initial sorting
                    "aaSorting": [[0, 'asc']], // Default - Sort by CreatedDateOn

                    //Gridview length
                    'iDisplayLength': settings.defaultShortGridviewLength,

                    //Overriding text templates
                    "oLanguage": {
                        "sInfo": self.AltNamesPagingText() + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong>",
                        "sEmptyTable": self.resources.get('MessageCode_Information_NoAlternativeNames_019'),
                        "sInfoEmpty": "" //resourceService.get('StudentDetailsView_ViewStudent_GridAltInfo')
                    }
                }
            };

            self.altNamesTableConfig = uiHelper.makeDatatableConfig(altNamesTableConfig);
            
            // Setting up the challenges datatable
            var challengesTableConfig = {
                dataSource: self.currentStudent.Challenges,
                rowTemplate: 'challengesRowTemplate',
                columns: [
                    { bSortable: false, mDataProp: 'ChallengeStatus', sDefaultContent: "" }
                  , { bSortable: false, mDataProp: 'ChallengeType', sDefaultContent: "" }
                    , { bSortable: false, mDataProp: 'CreatedDateTime', sDefaultContent: "" }
                    , { bSortable: false, mDataProp: 'CreatdByProvider', sDefaultContent: "" }
                ],
                options: {
                    // initial sorting
                    "aaSorting": [[2, 'desc']], // Default - Sort by Date raised Desc

                    //Gridview length
                    'iDisplayLength': settings.defaultShortGridviewLength,

                    //Overriding text templates
                    "oLanguage": {
                        "sInfo": self.ChallengesPagingText() + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong>",
                        "sEmptyTable": self.resources.get('MessageCode_General_NoSearchResultsFound_785'),
                        "sInfoEmpty": "" //resourceService.get('StudentDetailsView_ViewStudent_GridAltInfo')
                    }
                }
            };

            self.challengesTableConfig = uiHelper.makeDatatableConfig(challengesTableConfig);

            self.attached = function () {
                var uiHelper = com.uiHelper;
                // adds jQuery events to be bound to controls that hides/shows sections (added by Jo) -MB
                uiHelper.hider();
            };
        };

        return viewModel;

        //#endregion
    });