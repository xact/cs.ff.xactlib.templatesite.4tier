﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/resourceService',
		'services/referenceService',
		'services/studentService',
		'services/studentProviderRelationshipService',
		'services/challengeService',
		'services/helpTextService',
		'services/sessionService',
		'data/settings',
		'data/enums',
		'libs/uiLib',
		'libs/studentLib',
		'singletons/session',
		'singletons/parameters',
		'singletons/student',
		'entities/studentEntity',
		'services/mergeService'
],
    function (router,
	    app,
	    logger,
	    resourceService,
	    referenceService,
	    studentService,
	    studentProviderRelationshipService,
	    challengeService,
	    helpTextService,
	    sessionService,
	    settings,
	    enums,
	    uiLib,
	    studentLib,
	    session,
	    parameters,
	    originalStudent,
	    studentEntity,
		mergeService) {

    	//Comments regarding Code:
    	//* Separate Method Definitions by at least one space.
    	//* Create methods definitions using object name, not self. Reserve self statement
    	//  for getting around thorny 'this' syntax conditions, not method definitinos.
    	//* prefer using full class definitions, so that init methods can invoke methods
    	//  without having to define them higher on the page (code spa

    	var viewModel = function () {

    		var self = this;

    		// -----------------
    		// -- Page lifecycle

    		self.canActivate = function (nsn) {
    			var promise = $.Deferred();

    			// Fetch the current student, and make sure they exist / other BL checks
    			studentService.setCurrentStudentByNSN(nsn).done(function (student) {

    				if (!student) {
    					logger.logError(resourceService.get("MessageCode_Validation_InvalidNSN_336"), "", "editStudent", true);
    					promise.resolve({ redirect: "#/students/search" });
    					return true;
    				}

    				studentProviderRelationshipService.getSPRs(self.originalStudent.Id()).done(function (data) {

    					// Fill in missing providers, to avoid the dropdown forcing a change.

    					// Add NameDob verification provider (if not already in the list)
    					var hasNameVerificationProvider = ko.utils.arrayFirst(data, function (item) {
    						return item.ProviderId === self.originalStudent.NameBirthDateVerifiedByProviderFK();
    					});

    					if (!hasNameVerificationProvider) {
    						if (self.originalStudent.NameBirthDateVerificationProviderName
					    		&& self.originalStudent.NameBirthDateVerifiedByProviderFK
								&& self.originalStudent.NameBirthDateVerificationProviderName() != null
								&& self.originalStudent.NameBirthDateVerifiedByProviderFK() != null) {
    							data.push({
    								ProviderName: self.originalStudent.NameBirthDateVerificationProviderName(),
    								ProviderId: self.originalStudent.NameBirthDateVerifiedByProviderFK()
    							});
    						}
    					}

    					// Add residential verification provider (if not already in the list)
    					var hasResidentialVerificationProvider = ko.utils.arrayFirst(data, function (item) {
    						return item.ProviderId === self.originalStudent.ResidentialStatusVerifiedByProviderFK();
    					});

    					if (!hasResidentialVerificationProvider) {
    						if (self.originalStudent.ResidentialStatusVerificationProviderName
    							&& self.originalStudent.ResidentialStatusVerifiedByProviderFK
    							&& self.originalStudent.ResidentialStatusVerificationProviderName() != null
    							&& self.originalStudent.ResidentialStatusVerifiedByProviderFK() != null) {
    							data.push({
    								ProviderName: self.originalStudent.ResidentialStatusVerificationProviderName(),
    								ProviderId: self.originalStudent.ResidentialStatusVerifiedByProviderFK()
    							});
    						}
    					}

    					// Add the current provider (if not already in the list)

    					var user = session.user();

    					var hasCurrentUsersProvider = ko.utils.arrayFirst(data, function (item) {
    						return item.ProviderId === user.OrgId;
    					});

    					if (!hasCurrentUsersProvider) {
    						data.push({
    							ProviderName: user.OrgName,
    							ProviderId: user.OrgId
    						});
    					}



    					// Ensure providers are sorted
    					data = data.sort(function (a, b) {
    						return a.ProviderName > b.ProviderName ? 1 : -1;
    					});

    					//Talked to Gareth and Michael and agreed on this solution.
    					//Only needed for this case: Users without modifiy_verify permission and going from unverified to verified
    					//NamebirthdateVerificationProviderFK needs to be NULL before its sent to the backend for external users
    					//otherwise it will fail backend validation.
    					//Which is why a blank option is going to be added to the top after sorting, to the dropdown so it defaults to NULL. 
    					//The NamebirthdateVerificationProviderFK value will be updated in the updatebirthdatetrait updatestudent method
    					var externalUserAndUnverifiedRecord = (self.originalStudent.NameBirthDateVerificationMethodFK() == enums.nameBirthDateVerificationMethod.unverified
    														  || self.originalStudent.ResidentialStatusVerificationMethodFK() == enums.residentialStatusVerificationMethod.unverified)
															  && !self.checks.canModifyVerified;

    					if (externalUserAndUnverifiedRecord) {
    						data.unshift({
    							ProviderName: "",
    							ProviderId: null
    						});
    					}

    					// Push data to be available
    					self.referenceData.studentProviderRelationships(data);

    					promise.resolve(true);
    				});

    			});
    			//	.fail(function () {
    			//	logger.logError(resourceService.get("MessageCode_Validation_InvalidNSN_336"), "", "editStudent", true);
    			//	promise.resolve({ redirect: "#/students/search" });
    			//});

    			// Set the current (app-wide) student to the requested NSN

    			return promise.promise();
    		};

    		self.compositionComplete = function () {

    			// Ensure placeholder text
    			// Must happen once page is visible, to support IE
    			$.placeholder.shim();
    			uiHelper.focusFirstInputElement();
    	

    		};

    		self.activate = function () {

				//reset flag
    			self.originalStudent.hasBeenJustAddedOrUpdated(false);

    			// Clone a working copy of the current student for edit
				if (originalStudent.DateOfDeath() == "None") {
					originalStudent.DateOfDeath("");
				}
    			var origData = ko.toJS(originalStudent);
    			ko.mapping.fromJS(origData, null, self.currentStudent);

    			// Attach change tracking for challenge change
    			ko.utils.arrayForEach(self.currentStudent.Challenges(), function (item) {
    				item.ChallengeStatusFK.extend({ trackChange: true });
    			});

    			self.currentStudent.enableValidation(self.currentStudent); // TODO: Fix scope management here... (and studentEntity)

    			studentLib.attachExtraStudentValidation(self.currentStudent, self.checks);

    			studentLib.attachBirthDate029Confirmation(self.currentStudent.BirthDate, self.originalStudent.BirthDate);

    			self.isNameDobVerificationMoeConfirmed(self.currentStudent.NameBirthDateConfirmedByMOEDateTime() ? true : false);
    			self.isResidentialStatusVerificationConfirmed(self.currentStudent.ResidentialStatusConfirmedByMOEDateTime() ? true : false);

    			// Attach behaviour for changing Student Status
    			self.currentStudent.StudentStatusFK.subscribe(function (value) {

    				// If they're switching *to* inactive, default to 'Deceased'
    				if (value === enums.studentStatus.inactive) {
    					self.currentStudent.StudentStatusReasonFK(enums.studentStatusReason.deceased);
    				}
    			});

    			self.currentStudent.StudentStatusReasonFK.subscribe(function (value) {
    				// If they're switching *away* from inactive, clear the death date.
    				if (value !== enums.studentStatusReason.deceased) {
    					self.currentStudent.DateOfDeath("");
    					}
    			});

    			// Tie confirmed 'checked' property to update the datetime
    			self.isNameDobVerificationMoeConfirmed.subscribe(function (newValue) {
    				if (!newValue) {
    					// Remove the 'Is confirmed'
    					self.currentStudent.NameBirthDateConfirmedByMOEDateTime("");
    				} else {
    					// Set the date/time to the current time, to update
    					var updateTime = moment().format("YYYY-MM-DD");
    					self.currentStudent.NameBirthDateConfirmedByMOEDateTime(updateTime);
    				}
    			});

    			self.isResidentialStatusVerificationConfirmed.subscribe(function (newValue) {
    				if (!newValue) {
    					// Remove the 'Is confirmed'
    					self.currentStudent.ResidentialStatusConfirmedByMOEDateTime("");
    				} else {
    					// Set the date/time to the current time, to update
    					var updateTime = moment().format("YYYY-MM-DD");
    					self.currentStudent.ResidentialStatusConfirmedByMOEDateTime(updateTime);
    				}
    			});

    			// Validation...
    			self.currentStudent.BirthDate.extend({
    				required: {
    					message: "<li>Birth date</li>",
    					onlyIf: function () {
    						return !self.isBirthDateUnknown() && self.saveBtnClicked();
    					}
    				},
    				dateCannotBeToday: {
    					message: resourceService.get("MessageCode_Validation_BirthdateCannotBeToday_945"),
    					onlyIf: function () { return !self.isBirthDateUnknown(); }
    				}
    			    ,
    				//dateCannotEarlierThan: {
    				//    params: self.currentStudent.DateOfDeath,
    				//    onlyIf: function () {
    				//        return self.checks.isDeathDateVisible()
    				//            && !self.isBirthDateUnknown()
    				//            && self.currentStudent.BirthDate.isValid()
    				//            && self.currentStudent.DateOfDeath()
    				//            ;
    				//    },
    				//    message: resourceService.get("MessageCode_Validation_DeathDateLaterThanBirthDate_306")
    				//}
    				dateCannotBeLaterThan: {
    					params: self.currentStudent.DateOfDeath,
    					onlyIf: function () {
    						return self.checks.isDeathDateVisible()
    			                && !self.isBirthDateUnknown()
    			                && self.currentStudent.BirthDate.isValid()
    			                && self.currentStudent.DateOfDeath()
    						;
    					},
    					message: resourceService.get("MessageCode_Validation_DeathDateLaterThanBirthDate_306")
    				}
    			});

    			self.currentStudent.DateOfDeath.extend({
    				dateCannotEarlierThan: {
    					params: self.currentStudent.BirthDate,
    					onlyIf: function () { return self.checks.isDeathDateVisible() && !self.isBirthDateUnknown() && self.currentStudent.BirthDate.isValid() && self.currentStudent.DateOfDeath(); },
    					message: resourceService.get("MessageCode_Validation_DeathDateLaterThanBirthDate_306")
    				}
    			});

    			self.currentStudent.NameBirthDateVerificationMethodFK.extend({
    				validation: [
    					{
    						validator: function () {

    							if (self.currentStudent.BirthDate() && moment(self.currentStudent.BirthDate()).diff(moment("11/11/1918")) != 0) {
    								return true;
    							}
    							return self.currentStudent.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.unverified;
    						},
    						message: resourceService.get("MessageCode_Error_UnverifiedBlank_023")
    					}
    				]
    			});



    			self.currentStudent.ResidentialStatusFK.extend({
    				validation: [
    					{
    						validator: function () {
    							var result = !(self.currentStudent.ResidentialStatusVerificationMethodFK() !== enums.residentialStatusVerificationMethod.unverified
    											&& self.currentStudent.ResidentialStatusFK() === enums.residentialStatus.unknown);

    							return result;
    						},
    						message: resourceService.get("MessageCode_Error_UnverifiedBlank_023")
    					}
    				]
    			});
    			// TODO: Switch model to provide date + display copies of the date?
    			self.isBirthDateUnknown(self.currentStudent.BirthDate() && moment(self.currentStudent.BirthDate()).isValid() ? false : true);

    			if (!moment(self.currentStudent.DateOfDeath()).isValid()) {
    				self.currentStudent.DateOfDeath("");
    			}

    			// Display info messages re: student
    			if (!auth.canModifyVerified && !auth.canModifyBirthRegisterVerified) {

    				if (self.originalStudent.NameBirthDateVerificationMethodFK() !== enums.nameBirthDateVerificationMethod.unverified) {
    					var updateInfoMessage = self.resources.get("MessageCode_Information_ChallengeVerifiedStudent_034");

    					// TODO: Fix up the display of this message (HTML / new lines)
    					self.topInfoMessages(updateInfoMessage);
    				}
    			}

			    mergeService.checkNSNIncludedInMergeRequest(self.currentStudent.NSN()).done(function(result) {
				    if (result) {
				    	var mergeInfoMessage = resourceService.get("MessageCode_Information_studentPartOfTheMergeRequest_938");
				    	self.topInfoMessages(mergeInfoMessage);
				    }
					
			    });


		    };

    		self.canDeactivate = function () {

    			//if (self.currentStudent.ResidentialStatusVerifiedByProviderFK() == session.user().OrgId)
    			//self.currentStudent.ResidentialStatusVerifiedByProviderFK(null);

    			// Allow deactivation if no changes are made
    			if (!hasModifiedStudent()) {
    				return true;
    			}

    			// If changes have been made - prompt to confirm
    			var navigateAwayMessage = resourceService.get("MessageCode_Confirmation_NavigateAway_007");

    			return app.showMessage("", navigateAwayMessage, ['Yes', 'No']).done(function (result) {
    				if (result === "No") { // 'Stay on page' option
    					return false;
    				}

    				return true;
    			});
    		};

    		var hasModifiedStudent = function () {
    			return ko.utils.hasChanges(self.currentStudent, originalStudent) // Student fields have changed
    				|| self.newAltNames().length > 0 // Any alt names have been added
    				|| self.originalStudent.AltNames().length !== self.originalStudent.AltNames().length // Any alt names have been deleted
    				|| ko.utils.arrayFirst(self.currentStudent.Challenges(), function (item) { return item.ChallengeStatusFK.isDirty(); }); // Any challenge statuses have been modified
    		};

    		// -------------
    		// -- Properties
    		self.shouldShowChallenge = sessionService.isAuthorised(settings.securityClaims.NSI2_CHALLENGE_STUDENT);
    		self.currentStudent = new studentEntity(); // Student will be copied over in 'activate' method
    		self.enums = enums;

    		self.saveBtnClicked = ko.observable(false);

    		self.originalStudent = originalStudent;
    		self.isBirthDateUnknown = ko.observable(false);
    		self.isBirthDateUnknown.subscribe(function (newValue) {
    			var updatedBirthDate = newValue ? "" : self.originalStudent.BirthDate();
    			self.currentStudent.BirthDate(updatedBirthDate != "Unknown" ? updatedBirthDate : "");
    		});

    		self.isNameDobVerificationMoeConfirmed = ko.observable(false);
    		self.isResidentialStatusVerificationConfirmed = ko.observable(false);

    		// Proxy used to track when there are *no* preferred names set
    		self.hasNoPreferredName = ko.observable();


		    self.updateNullableFields = ko.computed(function() {
				    if (!self.currentStudent.GivenName2() || self.currentStudent.GivenName2() == "")
				    	self.currentStudent.GivenName2(null);

				    if (!self.currentStudent.GivenName3() || self.currentStudent.GivenName3() == "")
				    	self.currentStudent.GivenName3(null);
				}
		    );

    		/* Set up subscriptions to keep 'no preferred name' set correctly.
				This also sets 'no preferred name' = false, when an alt name is deleted */
    		var trackPreferredName = ko.computed(function () {
    			// Don't act if the student isn't loaded
    			if (!self.currentStudent.Id()) {
    				return true;
    			}

    			// Check the main student preferred name
    			if (self.currentStudent.PreferredNameIndicator()) {
    				self.hasNoPreferredName(false);

    				return true;
    			}

    			// Check the existing alt names
    			var hasPreferredNameExisting = ko.utils.arrayFirst(self.currentStudent.AltNames(), function (altName) {
    				return altName.PreferredNameIndicator();
    			});

    			// Check the new alt names
    			var hasPreferredNameNew = ko.utils.arrayFirst(self.newAltNames(), function (altName) {
    				return altName.PreferredNameIndicator();
    			});

    			if (hasPreferredNameExisting || hasPreferredNameNew) {
    				self.hasNoPreferredName(false);
    				return true;
    			}

    			// If there are no preferred names, set both the proxy, and the actual student
    			self.hasNoPreferredName(true);
    			self.currentStudent.PreferredNameIndicator(false);

    			return true;

    		});

    		self.isRunningSubmitEdit = ko.observable(false);

    		self.isChallengeChangeVisible = ko.computed(function () {
    			if (self.currentStudent.Challenges().length <= 0) {
    				return false;
    			} else {
    				return true;
    			}
    		}, this);

    		self.newAltNames = ko.observableArray();



    		self.newAltNamesRaw = ko.computed(function () {
    			return self.newAltNames();
    		});

    		//self.validationErrors = ko.validation.group([self.currentStudent, self.newAltNamesRaw], { observable: true, deep: true });
    		self.validationErrors = ko.computed(function () {
    			return ko.validation.group([self.currentStudent, self.newAltNames], { observable: true, deep: true })();
    		});


    		self.topInfoMessages = ko.observable();

    		self.studentLib = studentLib;

    		// ----------
    		// -- Actions

    		self.onDeleteExistingAltName = function (altName) {
    			// TODO: Check for 'set as primary name', and fallback if necessary

    			var deleteAltNameConfirmationMessage = resourceService.get("MessageCode_Confirmation_DeleteAltName_044");

    			app.showMessage("", deleteAltNameConfirmationMessage, ["Yes", "No"]).done(function (result) {
    				if (result === "No") {
    					return;
    				}

    				self.currentStudent.AltNames.remove(function (item) {
    					return item.Id() === altName.Id();
    				});
    			});
    		};
    		canEnableDeleteAltName: ko.computed(function (nameBirthDateVerificationMethod) {
    			if (nameBirthDateVerificationMethod)
    				return nameBirthDateVerificationMethod === enums.nameBirthDateVerificationMethod.birthRegister;
    			else {
    				return true;
    			}
    		});

    		self.staticErrors = ko.observableArray();

    		self.allErrors = ko.computed(function () {
    			var requiredFieldsCollection = "";
    			var errors = [];

    			errors = errors.concat(self.staticErrors());
    			//errors = errors.concat(self.validationErrors());
    			// split the validation messages to two variables based on their type (required/others)
    			if (self.validationErrors().length > 0) {
    				$.each(self.validationErrors(), function (index, message) {
    					if (message() && message().indexOf("<li>") !== -1) {
    						requiredFieldsCollection += message();
    					}
    					else {
    						//todo: refactor this
    						//Make sure there are no duplicate error messages.
    						if (errors.indexOf(message()) == -1) {
    							errors.push(message());
    						}
    					}
    				});

    				if (requiredFieldsCollection) {
    					errors.unshift(resourceService.get("MessageCode_Validation_MissingMandatoryFields_257").format("<ul>" + requiredFieldsCollection + "</ul>"));
    				}
    			}

    			return errors.length > 0 ? errors : ko.observableArray();;
    		});

    		// ---------------
    		// -- Page actions

    		self.onSave = function () {

				//Will attach a red ! to all input fields which have errored.
    		

    			self.saveBtnClicked(true);

    			//Situation: When a external user adds a alt name, the altname createdby field has a value even though the column isn't visable 
    			//Setting the createdby value to null here so it doesnt get an error when validating. The value will be updated in the backend after validation. 

    			if (!auth.canModifyAltNameCreated) {
    				var altNameLength = self.newAltNames().length;
    				for (var i = 0; i < altNameLength; i++) {
    					self.newAltNames()[i].CreatedByProviderFK = null;
    				}
    			}

    			// Run additional 'on submit' validations
    			onSaveValidation();

    			if (self.validationErrors().length > 0) {

    				$('.validationMark').remove();
    				$(":input:visible.error").after('<span class="validationMark">!</span>');
    				return false;
    			}

    			if (self.staticErrors().length > 0) {
    				return false;
    			}

    			self.checkForUnverifiedFields();

    			return true;
    		};

    		self.hasShownUnverifiedMSG = ko.observable(false);

    		self.checkForUnverifiedFields = function () {
    			// Getting Unverified info - #MB

    			self.hasUnverified = false;

    			//Go directly to unknownDOB confirmation and attemptSave skipping Confirming unverified info 
    			if (self.isBirthDateUnknown() ||
    				(self.currentStudent.BirthDate() &&
    					moment(self.currentStudent.BirthDate()).diff(moment("11/11/1918")) == 0)) {
    				self.attemptSave();
    			} else {
    				// Confirming unverified info 
    				if (self.currentStudent.NameBirthDateVerificationMethodFK() == enums.nameBirthDateVerificationMethod.unverified ||
						self.currentStudent.ResidentialStatusVerificationMethodFK() == enums.residentialStatusVerificationMethod.unverified) {
    					self.hasUnverified = true;
    				}

    				if (self.newAltNames &&
						self.newAltNames().length > 0 &&
						!self.hasUnverified) {
    					for (var i = 0; i < self.newAltNames().length; i++) {

    						if (self.newAltNames()[i].FamilyName() &&
								self.newAltNames()[i].FamilyName() != "" &&
								self.newAltNames()[i].NameBirthDateVerificationMethodFK() == enums.residentialStatusVerificationMethod.unverified) {
    							self.hasUnverified = true;
    						}
    					}
    				}


    				//Changed due to the issue NSIPRJ-2864
    				//if (self.hasUnverified && !self.hasShownUnverifiedMSG()) {
    				if (self.hasUnverified) {
    					//&& self.originalStudent.FamilyName().toLowerCase() != self.currentStudent.FamilyName().toLowerCase()
    					var confirmMessage = resourceService.get("MessageCode_Confirmation_UpdateUnverifiedStudent_927");
    					app.showMessage('', confirmMessage, ['Yes', 'No']).then(function (result) {
    						if (result === 'Yes') {
    							self.attemptSave();
    						}
    					});
    					self.hasShownUnverifiedMSG(true);
    				} else {
    					self.attemptSave();
    				}
    			}
    		};

    		self.attemptSave = function () {

    			// -- Step 2: Check for need to confirm things
    			var confirmationsPromise = onSaveConfirmations();

    			// Step 3: Update student

    			//Delete empty altnames

    			var list = ko.observableArray();
    			var i;
    			if (self.newAltNames &&
					self.newAltNames().length > 0) {
    				for (i = 0; i < self.newAltNames().length; i++) {
    					if (!self.newAltNames()[i].FamilyName() || self.newAltNames()[i].FamilyName() == "") {
    						list.push(self.newAltNames()[i]);
    					}
    				}
    			}

    			for (i = 0; i < list().length; i++) {
    				self.newAltNames.remove(list()[i]);
    			}
    			// end of Delete empty Altnames

    			// If the update is conditional on user input
    			if (confirmationsPromise && confirmationsPromise.length > 0) {
    				var chain = null;

    				ko.utils.arrayForEach(confirmationsPromise, function (item) {
    					if (chain === null) {
    						chain = item();
    					} else {
    						chain = chain.done(item());
    					}
    				});

    				return chain.done(sendStudentUpdate);
    			}

    			sendStudentUpdate();
    			return true;
    		};

    		self.onCancel = function () {
    			self.originalStudent.hasBeenJustAddedOrUpdated(false);
    			router.navigate("#/students/view/" + self.currentStudent.NSN());
    		};

    		self.navToStudentHistory = function () {
    			// TODO: Is this the correct way to cancel the deactivation check?
    			// TODO: Disabled: We'll currently lose state of the edited student...
    			//self.canDeactivate = null;

    			router.navigate('/students/history/' + self.currentStudent.NSN());
    		};

    		self.navToChallengeChange = function () {
    			router.navigate('/students/challengeChange/' + self.currentStudent.NSN());
    		};

    		// ----
    		// -- Support methods for page actions

    		// ** Submit the current student to save
    		var sendStudentUpdate = function () {
    			self.isRunningSubmitEdit(true);
    			studentService.updateStudent(self.currentStudent, self.newAltNames).done(function (result) {

    				if (result.Messages.length > 0) {
    					ko.utils.arrayForEach(result.Messages, function (item) {
    						logger.logError(item.PresentationAttributes.Text, null, null, true);
    					});

    					// Only disable spinner under error condition
    					self.isRunningSubmitEdit(false);

    					return false;
    				}

    				// Next - save the challenge change records
    				var challengesToSave = challengeService.updateChallengeChangesForStudent(self.currentStudent.Challenges);
    				self.currentStudent.BirthDate(self.currentStudent.BirthDate()
    					//?moment(self.currentStudent.BirthDate()).diff(moment("11/11/1918")) != 0
    						? self.currentStudent.BirthDate().trim()
    						//: ""
    					: "");

    				var studentSaved = function () {
    					self.originalStudent.hasBeenJustAddedOrUpdated(true);
    					// Spinner is left running, so it's not temporarily enable while switching to the next page
    					self.canDeactivate = null; // TODO: Is this the correct way to cancel the deactivation check?
    					router.navigate("#/students/view/" + self.currentStudent.NSN());

    					//var saveSuccessMessage = resourceService.get("MessageCode_Confirmation_StudentRecordSaved_025");
    					//logger.log(saveSuccessMessage, null, null, true);
    				};

    				if (!challengesToSave) {
    					studentSaved();
    					return true;
    				}

    				return challengesToSave.done(studentSaved);

    			});

    		};

    		// ** Check 'on submit' validations for the save action
    		var onSaveValidation = function () {

    			var errors = [];

    			var hasDuplicates = studentLib.hasDuplicateNames(self.currentStudent, self.newAltNames);
    			if (hasDuplicates) {
    				var duplicatesMessage = resourceService.get("MessageCode_Error_ExactMatchCannotBeAdded_027");
    				errors.push(duplicatesMessage);
    			}
    			// Always reset the static errors
    			self.staticErrors(errors);
    			self.currentStudent.FamilyName.extend({
    				required: {
    					message: "<li>Family Name</li>",
    					onlyIf: function () { return !self.currentStudent.FamilyName(); }
    				}
    			});
    			self.currentStudent.GivenName1.extend({
    				required: {
    					message: "<li>Given Name (1)</li>",
    					onlyIf: function () { return !self.currentStudent.GivenName1(); }
    				}
    			});

    		};

    		// ** Gets a promise containing all user confirmations that need to be made for the user to proceed
    		var onSaveConfirmations = function () {

    			var promises = [];

    			// Prompt if they're changing verification method, but not resdential status FS1079
    			if (self.currentStudent.ResidentialStatusVerificationMethodFK() !== enums.residentialStatusVerificationMethod.unverified // Current - Is still verified
    				&& self.originalStudent.ResidentialStatusVerificationMethodFK() !== enums.residentialStatusVerificationMethod.unverified // Original - Is verified
    				&& self.currentStudent.ResidentialStatusVerificationMethodFK() === self.originalStudent.ResidentialStatusVerificationMethodFK() // Verification method has changed
    				&& self.currentStudent.ResidentialStatusFK() !== self.originalStudent.ResidentialStatusFK()) { // Residential status hasn't changed

    				var changeVerificationDetailsConfirmationMessage = resourceService.get("MessageCode_Confirmation_ChangeVerificationDetails_041");

    				var changeVerificationPromise = function () {
    					var promise = $.Deferred();

    					app.showMessage(changeVerificationDetailsConfirmationMessage, "", ["Yes", "No"]).done(function (result) {
    						if (result !== "Yes") {
    							promise.reject();
    						} else {
    							promise.resolve();
    						}
    					});

    					return promise.promise();
    				};

    				promises.push(changeVerificationPromise);
    			}
    			// Check for message #028 - Birth date that is equal to 11/11/1918 or 'Unknown' and “Name & birth date verification" is set to Unverified
    			if (self.currentStudent.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.unverified
				    && (moment(self.currentStudent.BirthDate()).diff(moment("11/11/1918")) === 0
    		       || self.isBirthDateUnknown())) {
    				var changeNameDOBVerificationConfirmationMessage = resourceService.get("MessageCode_Error_ProxyBirthDate_028");

    				var changeDOBVerificationPromise = function () {
    					var promise = $.Deferred();

    					app.showMessage(changeNameDOBVerificationConfirmationMessage, "", ["Yes", "No"]).done(function (result) {
    						if (result !== "Yes") {
    							promise.reject();
    						} else {
    							promise.resolve();
    						}
    					});

    					return promise.promise();
    				};

    				promises.push(changeDOBVerificationPromise);
    			}

    			if (!auth.canModifyVerified || !auth.canModifyBirthRegisterVerified) {
    				// -- Confirmations for non MOE support users
    				if (!self.isBirthDateUnknown() && moment(self.currentStudent.BirthDate()).diff(moment("11/11/1918")) != 0) {
    					// # Confirm update verified
    					if ((self.currentStudent.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.unverified
							|| self.currentStudent.ResidentialStatusVerificationMethodFK() === enums.residentialStatusVerificationMethod.unverified
							|| self.studentLib.hasUnverifiedAltNames(self.newAltNames))

							&& !self.hasShownUnverifiedMSG()
    						//&& (self.originalStudent.FamilyName().toLowerCase() != self.currentStudent.FamilyName().toLowerCase()
    						//|| self.originalStudent.GivenName1().toLowerCase() != self.currentStudent.GivenName1().toLowerCase()
    						//|| self.originalStudent.GivenName2().toLowerCase() != self.currentStudent.GivenName2().toLowerCase()
    						//|| self.originalStudent.GivenName3().toLowerCase() != self.currentStudent.GivenName3().toLowerCase()
    						//|| self.originalStudent.BirthDate().toLowerCase() != self.currentStudent.BirthDate().toLowerCase()
    						//)
    					) {

    						self.hasShownUnverifiedMSG(true);
    						var confirmUpdateVerifiedMessage = self.resources.get("MessageCode_Confirmation_UpdateUnverifiedStudent_927");

    						var confirmUpdateVerifiedPromise = function () {
    							var promise = $.Deferred();

    							app.showMessage(confirmUpdateVerifiedMessage, "", ["Yes", "No"]).done(function (result) {
    								if (result !== "Yes") {
    									promise.reject();
    								} else {
    									promise.resolve();
    								}
    							});

    							return promise.promise();
    						};
    						promises.push(confirmUpdateVerifiedPromise);
    					}
    				}
    			} else {
    				// -- Confirmations for MOE support users

    				if (studentLib.hasChangedDobAndFamilyNameAndGivenName(self.originalStudent, self.currentStudent)) {
    					var confirmUpdateVerifiedMessage918 = self.resources.get("MessageCode_Confirmation_NameBirthDateUpdate_918");

    					var confirmUpdateVerifiedPromise918 = function () {
    						var promise = $.Deferred();

    						app.showMessage(confirmUpdateVerifiedMessage918, "", ["Yes", "No"]).done(function (result) {
    							if (result !== "Yes") {
    								promise.reject();
    							} else {
    								promise.resolve();
    							}
    						});

    						return promise;
    					};

    					promises.push(confirmUpdateVerifiedPromise918);

    				}
    			}

    			return promises;
    		};

    		// --------
    		// -- Page support

    		// Raw values, for calculated use later
    		var referenceDataRaw = {
    			nameDobVerification: referenceService.referenceDictionary.get('NameBirthDateVerificationMethod'),
    			residentialStatusVerification: referenceService.referenceDictionary.get('ResidentialStatusVerificationMethod'),
    			studentStatusReason: referenceService.referenceDictionary.get('StudentStatusReason')
    		};
    		self.referenceData = {
    			gender: referenceService.referenceDictionary.get('Gender'),
    			residentialStatus: referenceService.referenceDictionary.get('ResidentialStatus'),
    			residentialStatusProviders: function () {
    				return linkedProviders;
    			},
    			studentStatus: referenceService.referenceDictionary.get('StudentStatus'),
    			studentProviderRelationships: ko.observableArray(),
    			challengeStatus: referenceService.referenceDictionary.get('ChallengeStatus'),

    			nameDobVerification: ko.computed(function () {
    				// Else remove birth register from the list
    				return ko.utils.arrayFilter(referenceDataRaw.nameDobVerification(), function (item) {
    					// Allow type-coersion (.Key is currently a string...)

    					// If currently set to birth register - allow it to remain in the list
    					if (item.Key == enums.nameBirthDateVerificationMethod.birthRegister) {
    						return self.currentStudent.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.birthRegister;
    					}

    					// Filter out 'unknown', if the student is verified
    					if (self.originalStudent.NameBirthDateVerificationMethodFK() !== enums.nameBirthDateVerificationMethod.unverified
    						&& item.Key == enums.nameBirthDateVerificationMethod.unverified) {
    						return false;
    					}

    					return true;
    				});
    			}),

    			nameDobVerificationForAltNames: ko.computed(function () {
    				// Else remove birth register from the list
    				return ko.utils.arrayFilter(referenceDataRaw.nameDobVerification(), function (item) {
    					// Allow type-coersion (.Key is currently a string...)

    					// If currently set to birth register - allow it to remain in the list
    					if (item.Key == enums.nameBirthDateVerificationMethod.birthRegister) {
    						return self.currentStudent.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.birthRegister;
    					}

    					return true;
    				});
    			}),

    			residentialStatusVerification: ko.computed(function () {

    				return ko.utils.arrayFilter(referenceDataRaw.residentialStatusVerification(), function (item) {
    					// Allow type-coersion (.Key is currently a string...)

    					// If currently set to birth register - allow it to remain in the list
    					if (item.Key == enums.residentialStatusVerificationMethod.birthRegister) {
    						return self.currentStudent.ResidentialStatusVerificationMethodFK() === enums.residentialStatusVerificationMethod.birthRegister;
    					}

    					// Filter out 'unknown', if the student is verified
    					if (self.originalStudent.ResidentialStatusVerificationMethodFK() !== enums.residentialStatusVerificationMethod.unverified
    						&& item.Key == enums.residentialStatusVerificationMethod.unverified) {
    						return false;
    					}

    					return true;
    				});
    			}),

    			studentStatusReason: ko.computed(function () {

    				return ko.utils.arrayFilter(referenceDataRaw.studentStatusReason(), function (item) {
    					// Allow type-coersion (.Key is currently a string...)

    					var studentStatus = self.currentStudent.StudentStatusFK();

    					// Filter the 'slave' option out, if it's not a slave student record
    					if (!self.originalStudent.MasterNSN() // For master records
    						&& item.Key == enums.studentStatusReason.slave) {
    						return false;
    					}
    					// Filter list items. Active only returns 'In use', all other status reasons go with status = 'inactive'.
    					return studentStatus == enums.studentStatus.active
							? item.Key == enums.studentStatusReason.inUse
							: item.Key != enums.studentStatusReason.inUse;
    				});
    			})
    		};

    		self.editNewAltNamesWidgetConfig = {
    			title: "New alternative name(s)",
    			newAltNames: self.newAltNames,
    			parentPage: enums.parentPage.editStudent,
    			initialEmptyAltName: false,
    			triggerValidationConditions: self.saveBtnClicked,
    			nameDobVerification: self.referenceData.nameDobVerificationForAltNames,
    			studentProviderRelationships: self.referenceData.studentProviderRelationships
    		};


    		// Authentication checks
    		// Note: These aren't public on this VM, as checking by the *view* should all be relayed through self.checks (for simplicity/clarity)
    		var auth = {
    			canModifyBirthDate: sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_ALL_BIRTH_DATES),
    			canModifyStudentStatus: sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_STUDENT_STATUS),
    			canDeleteAltName: sessionService.isAuthorised(settings.securityClaims.NSI2_DELETE_ALT_NAME),
    			canModifyAltNameCreated: sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_ALT_NAME_CREATED),
    			canModifyVerified: sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_VERIFIED),
    			//canModifyMoeConfirmed: sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_CONFIRMED_MOE),
    			canModifyChallengeStatus: sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_CHALLENGE_STATUS),
    			canModifyBirthRegisterVerified: sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_BDM_VERIFIED),
    			canModifyInactiveStudent: sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_INACTIVE_STUDENT)

    		};

    		// Page-control checks.
    		self.checks = {

    			canModifyName: ko.computed(function () { return studentLib.canModifyName(self.originalStudent, self.currentStudent, auth); }),
    			canModifyBirthDate: ko.computed(function () { return studentLib.canModifyBirthDate(self.originalStudent, self.currentStudent, auth) || self.isBirthDateUnknown(); }),

    			canModifyNameDobVerification: ko.computed(function () { return studentLib.canModifyNameDobVerification(self.originalStudent, self.currentStudent, auth); }),

    			canModifyResidentialStatus: ko.computed(function () { return studentLib.canModifyResidentialStatus(self.originalStudent, self.currentStudent, auth); }),
    			canModifyResidentialStatusVerification: ko.computed(function () { return studentLib.canModifyResidentialStatusVerification(self.originalStudent, auth); }),

    			canModifyNameDobVerficationConfirmedByMoe: ko.computed(function () {
    				return studentLib.canModifyVerificationConfirmedByMinistry(self.currentStudent.NameBirthDateVerificationMethodFK(),
						self.currentStudent.NameBirthDateVerifiedByProviderFK(), auth);
    			}),

    			canModifyResidentialStatusVerificationConfirmedByMoe: ko.computed(function () {

    				return studentLib.canModifyVerificationConfirmedByMinistry(self.currentStudent.ResidentialStatusVerificationMethodFK(),
						self.currentStudent.ResidentialStatusVerifiedByProviderFK(), auth);
    			}),

    			isDeathDateVisible: ko.computed(function () {
    				return self.currentStudent.StudentStatusReasonFK() === enums.studentStatusReason.deceased;
    			}),

    			canDisplayReadonlyNameDobVerficationConfirmedByMoe: ko.computed(function () {
    				if (auth.canModifyVerified) {
    					return false;
    				}

    				if ((!self.currentStudent.NameBirthDateVerificationMethodFK()) || self.currentStudent.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.unverified) {
    					return false;
    				}
    				return true;
    			}),
    			canDisplayReadonlyResidentialStatusVerficationConfirmedByMoe: ko.computed(function () {
    				if (auth.canModifyVerified) {
    					return false;
    				}

    				if ((!self.currentStudent.ResidentialStatusVerificationMethodFK()) || self.currentStudent.ResidentialStatusVerificationMethodFK() === enums.residentialStatusVerificationMethod.unverified) {
    					return false;
    				}
    				return true;
    			}),

    			canModifyStudentStatus: auth.canModifyStudentStatus,
    			canDeleteAltName: auth.canDeleteAltName,
    			canModifyAltNameCreated: auth.canModifyAltNameCreated,
    			canModifyVerified: auth.canModifyVerified,
    			//canModifyMoeConfirmed: auth.canModifyMoeConfirmed,
    			canModifyChallengeStatus: auth.canModifyChallengeStatus,
    			canModifyBirthRegisterVerified: auth.canModifyBirthRegisterVerified,
    			canModifyInactiveStudent: auth.canModifyInactiveStudent
    		};


    		//The computed fields below are to clear fields which were visable and have a value in them(for users with modify_verifiy privilege).
    		//e.g NamebirthdateVerifciation  Unverified --> Academy NZ, tick verifixcation confrimed by MOE tickbox. Academy NZ ---> Unverified but the MOE checkbox is still ticked
			//even through the checkbox is hidden.

    		var  trackNameBirthDateVerification = ko.computed(function() {

    			//Going from verified --> Unverified. Setting the fields which are hidden to null
    			if (self.checks.canModifyVerified
				    && self.currentStudent.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.unverified) {
    				self.currentStudent.NameBirthDateConfirmedByMOEDateTime("");
    				self.currentStudent.NameBirthDateVerificationDateTime("");
    				self.currentStudent.NameBirthDateVerifiedByProviderFK(null);
    			}

    			//Going from Verified --> MOE. Setting the fields which are hidden to null.
    			if (self.checks.canModifyVerified
				    && self.currentStudent.ResidentialStatusVerifiedByProviderFK() === settings.providers.moeId) {
    				self.currentStudent.NameBirthDateConfirmedByMOEDateTime("");
    			}
    		});

    		
    		var trackResidentalStatusVerification = ko.computed(function() {
    			//Going from verified --> Unverified. Setting the fields which are hidden to null
    			if (self.checks.canModifyVerified 
				    && self.currentStudent.ResidentialStatusVerificationMethodFK() === enums.residentialStatusVerificationMethod.unverified) {	    
    				self.currentStudent.ResidentialStatusConfirmedByMOEDateTime("");
    				self.currentStudent.ResidentialStatusVerificationDateTime("");
				    self.currentStudent.ResidentialStatusVerifiedByProviderFK(null);
    			}

    			//Going from Verified --> MOE. Setting the fields which are hidden to null.
    			if (self.checks.canModifyVerified 
				    && self.currentStudent.ResidentialStatusVerifiedByProviderFK() === settings.providers.moeId) {
    				self.currentStudent.ResidentialStatusConfirmedByMOEDateTime("");
    			}

		    });


    		// Additional checks (separate from original declaration, as they're dependent on the other .checks

    		self.checks.isNameDobProviderSectionVisible = ko.computed(function () {
    			// For staff who can't edit it, hide it if it was originally unknown
    			if (!self.checks.canModifyVerified) {
    				var isOriginalNameDobUnverified =
    					self.originalStudent.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.unverified;

    				return !isOriginalNameDobUnverified;
    			}

    			// For staff who can edit it, toggle it based on their verification selection
    			var isNameDobUnverified =
    				self.currentStudent.NameBirthDateVerificationMethodFK() === enums.nameBirthDateVerificationMethod.unverified;

    			return !isNameDobUnverified;
    		}),

			self.checks.isResidentialStatusVerificationProviderSectionVisible = ko.computed(function () {
				// For staff who can't edit it, hide it if it was originally unknown
				if (!self.checks.canModifyVerified) {
					var isOriginalResidentialStatusVerificationUnverified =
						self.originalStudent.ResidentialStatusVerificationMethodFK() === enums.residentialStatusVerificationMethod.unverified;

					return !isOriginalResidentialStatusVerificationUnverified;
				}

				// For staff who can edit it, toggle it based on their verification selection
				var isResidentialStatusUnverified =
					self.currentStudent.ResidentialStatusVerificationMethodFK() === enums.residentialStatusVerificationMethod.unverified;

				return !isResidentialStatusUnverified;
			}),

    		//Make a local reference to global Resources dictionary cache:
    		//Use this to bind to from the ViewModel using syntax similar to
    		//data-bind="text: resources.get("XYZ")"
    		self.resources = resourceService.resources;

    		//Make a local reference to global Reference dictionary cache:
    		//Use this to bind to from the ViewModel using syntax similar to
    		//data-bind="text: referenceData.get("XYZ")"
    		self.resources = resourceService;

    		//#region resources grab
    		self.valMsg = "!"; //resourceService.get('ComDefaultValidationMessage');

    		self.AltNamesPagingText = ko.computed(function () {
    			return self.resources.get('StudentDetailsView_ViewStudent_AltNamesPagingInfo');
    		}, this);

    		// ------
    		// Setting up the altnames datatable
    		var altNamesTableConfig = {
    			dataSource: self.currentStudent.AltNames,
    			rowTemplate: 'editStudentDisplayAltNamesRowTemplate',
    			columns: [
                    { bVisible: true, bSortable: false, mDataProp: 'CreatedDateTime', sDefaultContent: "" }
					, { bSortable: false, mDataProp: 'FamilyName', sDefaultContent: "" }
					, { bSortable: false, mDataProp: 'GivenNames', sDefaultContent: "" }
                    , { bSortable: false, mDataProp: 'NameBirthDateVerificationMethod', sDefaultContent: "" }
					, { bSortable: false, mDataProp: 'PreferredNameIndicator', sDefaultContent: "" }
    				, { bSortable: false, mDataProp: 'CreatedByProvider', sDefaultContent: "" }
    			],
    			options: {
    				// initial sorting
    				"aaSorting": [[1, 'asc'], [2, 'asc']],

    				//Gridview length
    				'iDisplayLength': settings.defaultShortGridviewLength,

    				//Overriding text templates
    				"oLanguage": {
    					"sInfo": self.AltNamesPagingText() + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong>",
    					"sEmptyTable": self.resources.get('MessageCode_Information_NoAlternativeNames_019'),
    					"sInfoEmpty": ""
    				}
    			}
    		};

    		self.altNamesTableConfig = uiHelper.makeDatatableConfig(altNamesTableConfig);

    		// Setting up the challenges datatable
    		var challengesTableConfig = {
    			dataSource: self.currentStudent.Challenges,
    			rowTemplate: 'challengesEditRowTemplate',
    			columns: [
                    { bSortable: false, mDataProp: 'ChallengeStatus', sDefaultContent: "" }
    			    , { bSortable: false, mDataProp: 'ChallengeType', sDefaultContent: "" }
                    , { bSortable: false, mDataProp: 'CreatedDateTime', sDefaultContent: "" }
                    , { bSortable: false, mDataProp: 'CreatdByProvider', sDefaultContent: "" }
    			],
    			options: {
    				// initial sorting
    				"aaSorting": [[2, 'desc']], // Default - Sort by Date raised Desc

    				//Gridview length
    				'iDisplayLength': settings.defaultShortGridviewLength,

    				//Overriding text templates
    				"oLanguage": {
    					"sInfo": "Displaying challenge(s) " + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong>",
    					"sEmptyTable": "No challenge changes.",
    					"sInfoEmpty": ""
    				}
    			}
    		};

    		self.challengeChangeTableConfig = uiHelper.makeDatatableConfig(challengesTableConfig);

    	};

    	return viewModel;
    });