﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/resourceService',
		'services/referenceService',
		'services/studentService',
		'services/helpTextService',
		'singletons/searchCriteria',
		'data/referenceData',
        'singletons/student'
],
    function (router, app, logger, resourceService, referenceService, studentService, helpTextService, searchCriteria, referenceData, studentEntity) {

    	//Comments regarding Code:
    	//* Separate Method Definitions by at least one space.
    	//* Create methods definitions using object name, not self. Reserve self statement
    	//  for getting around thorny 'this' syntax conditions, not method definitinos.
    	//* prefer using full class definitions, so that init methods can invoke methods
    	//  without having to define them higher on the page (code spa

    	var viewModel = function () {

    		var self = this;

    		self.activate = function () {
    			// Subscribe to 'search reset' event
    			app.on("studentSearch:reset").then(function () {
    				self.searchCriteria.clear();
    				self.canShowErrors(false);
    				self.staticErrors.removeAll();
    				$.placeholder.shim(); // Refresh - Damn IE
    			});

    			helpTextService.getStringsForView('StudentSearchView', '');
    		};

    		self.compositionComplete = function () {

    			// Ensure placeholder text
    			// Must happen once page is visible, to support IE	
    			$.placeholder.shim();

    			//Finds the first input element which isn't a button and not disabled to focus
    			uiHelper.focusFirstInputElement();


		    };

    		function myTrim(x) {
    			return x.replace(/^\s+|\s+$/gm, '');
    		}
    		//Make a local reference to global Resources dictionary cache:
    		//Use this to bind to from the ViewModel using syntax similar to
    		//data-bind="text: resources.get("XYZ")"
    		self.resources = resourceService;

    		//Make a local reference to global Reference dictionary cache:
    		//Use this to bind to from the ViewModel using syntax similar to
    		//data-bind="text: referenceData.get("XYZ")"
    		self.referenceData = referenceService.referenceDictionary;

    		self.resources2 = resourceService.resources;

    		//#region resources grab
    		self.valMsg = "!"; //resourceService.get('ComDefaultValidationMessage');

    		// -- Search criteria properties
    		self.searchCriteria = searchCriteria;

    		//#region viewModel events

    		self.attached = function () {
    			var uiHelper = com.uiHelper;

    			// Add the 'show/hide' feature to the criteria box
    			uiHelper.hider();
    			//Type and press Enter to submit Fix
    			$("#numNSN").keyup(function (event) {
    				if (event.keyCode == 13) {
    					$("#numNSN").blur();  //fix for IE9
    					self.onSearchByNsn();
    				}
    			});

    			$("#txtName").keyup(function (event) {
    				if (event.keyCode == 13) {
    					$("#txtName").blur();  //fix for IE9
    					self.onCriteriaSearch();
    				}
    			});

    			$("#txtDOB").keyup(function (event) {
    				if (event.keyCode == 13) {
    					$("#txtDOB").blur();  //fix for IE9
    					self.onCriteriaSearch();
    				}
    			});
    		};

    		self.deactivate = function () {
    			// Subscribe to 'search reset' event
    			//self.searchCriteria.clear();
    			//Because we cant set this property in studentservice.js (it is common for most UI pages), 
    			//Set this student entity as not been added/modified instead it loads from search page
    			if (studentEntity) {
    				studentEntity.hasBeenJustAddedOrUpdated(false);
    			}
    			self.canShowErrors(false);
    			self.staticErrors.removeAll();
    		};

    		//self.canShowErrors = ko.observable(false);
    		self.canShowErrors = self.searchCriteria.canShowErrors;

		    //self.name = ko.observable(myTrim(self.searchCriteria.name()));
    		// -- Validation and groups

    		// Validation - Groups
    		self.errorsNsnSearch = ko.validation.group([searchCriteria.nsn], { observable: true });

		    self.errorsFuzzySearch = ko.validation.group([
				searchCriteria.name,
				searchCriteria.dob
    		], { deep: true });

    		self.staticErrors = ko.observableArray();
    		self.isRunningNsnSearch = ko.observable(false);
    		self.isRunningDetailsSearch = ko.observable(false);
    		
    		self.action = ko.observable('');
    		
    		self.requiredNSN = ko.computed(function () {
    			if (searchCriteria.nsn()) {
    				if (self.isRunningDetailsSearch()) {
    					searchCriteria.nsn('');
    					//self.staticErrors.removeAll();
    					//self.canShowErrors(false);
    					return false;
    				}
    				if (!searchCriteria.nsn.isValid() && searchCriteria.nsn.error() == resourceService.get("MessageCode_Validation_InvalidNSN_336")) {
    					if (!self.staticErrors().length > 0) {
    						self.staticErrors.push(searchCriteria.nsn.error());
    						self.canShowErrors(true);
    						//searchCriteria.nsn.isModified(true);
    					}
    					return true;
    				} else {
    					self.staticErrors.removeAll();
    					//self.staticErrors.pop(searchCriteria.nsn.error());
    					self.canShowErrors(false);
    					//searchCriteria.nsn.isModified(true);
    					return false;
    				}
    			} else {
    				self.staticErrors.removeAll();
    				//self.staticErrors.pop(searchCriteria.nsn.error());
    				self.canShowErrors(false);
    				searchCriteria.nsn.isModified(false);
    				return false;
    			}
    		});

    		//css class changes to label when it's related input is valid/not (to become red) 
    		self.notValidCss = function (boundField, action) {
    			return boundField.isModified() && !boundField.isValid() && self.canShowErrors() && action == self.action() ? 'errorLabel' : 'normalColourLabel';
    		};

    		self.notValidBorderCss = function (boundField, action) {
    			return boundField.isModified() && !boundField.isValid() && self.canShowErrors() && action == self.action() ? 'errorBorder' : 'normalBorder';
    		};
    		self.notValidField = function (boundField, action) {
    			return boundField.isModified() && !boundField.isValid() && self.canShowErrors() && action == self.action();
    		};

    		self.notValidOnSubmit = function (boundField, action) {
    			return !boundField.isValid() && self.canShowErrors() && action == self.action();
    		};
			

    		// -- Search methods
    		self.onSearchByNsn = function () {
    			if (!self.checkValid(self.errorsNsnSearch)) {
    				self.action('NSN');
    				return;
    			}
    			self.isRunningNsnSearch(true);
    			self.isRunningDetailsSearch(false);

    			studentService.checkAndSetCurrentStudentByNSN(searchCriteria.nsn()).done(function (student) {
    				if (!student) {
    					logger.logError(resourceService.get("MessageCode_Validation_NSIRecordDoesNotExist_337").replace("{0}", searchCriteria.nsn()), "", "search", true);
    					return;
    				}

    				router.navigate('/students/view/' + searchCriteria.nsn());
    			}).always(function () {
    				self.isRunningNsnSearch(false);
    				self.action('');
    			});
    		};

    		self.onCriteriaSearch = function () {
			   
    			if (!self.checkValid(self.errorsFuzzySearch)) {
    				self.action('Criteria');
    				return;
    			}
				
    			var params = {
    				Name: myTrim(searchCriteria.name()),
    				DOB: searchCriteria.dob() ? searchCriteria.dob() : "", //.toUTCString() : "",
    				Gender: searchCriteria.gender(),
    				ResidentialStatus: searchCriteria.residentialStatus()
    			};

    			self.isRunningDetailsSearch(true);
    			self.isRunningNsnSearch(false);

    			studentService.doStudentSearch(params).done(function (data) {

    				$.placeholder.shim(); // Refresh - Damn IE
    			}).always(function () {
    				app.trigger("studentSearch:resetRecordsToMerge"); // Ensure 'records to merge' is cleared out with a new search request
    				app.trigger("studentSearch:resetResultsGrid");
    				self.isRunningDetailsSearch(false);
    				self.action('');
    			});
				
    			searchCriteria.dobForAdd(params.DOB);
    			searchCriteria.nameForAdd(params.Name);
    			searchCriteria.genderForAdd(params.Gender);
    			searchCriteria.residentialStatusForAdd(params.ResidentialStatus);
    		};

    		// Validation - Check if the current valdiation group is 
    		self.checkValid = function (validationGroup) {
    			validationGroup.showAllMessages();

    			if (validationGroup().length !== 0) {
    				self.canShowErrors(true);

    				// Manual clear & populate with *non-observable* validation messages
    				var updatedErrors = new Array();
    				ko.utils.arrayForEach(validationGroup(), function (validationMessage) {
    					updatedErrors.push(ko.utils.unwrapObservable(validationMessage));
    				});
    				self.staticErrors(updatedErrors); // Move errors to display list

    				$.placeholder.shim(); // Refresh - Damn IE
    				return false;
    			} else {
    				self.canShowErrors(false);
    				self.staticErrors.removeAll();

    				$.placeholder.shim(); // Refresh - Damn IE
    				return true;
    			}


    			//self.errors.showAllMessages();
    		};

    	};

    	return viewModel;

    	function applyValidation(vm) {

    	}


    });