define(['plugins/router',
        'durandal/app',
		'services/logger',
        'services/referenceService',
		'services/resourceService',
		'services/challengeService',
        'services/parameterService',
		'singletons/parameters'],
    function (router, app, logger, referenceService, resourceService, challengeService, parameterService, parameters) {
    	var viewModel = function () {
    		var self = this;

    		// -- Lifecycle events

    		self.attached = function () {
    			var uiHelper = com.uiHelper;
    			// adds jQuery events to be bound to controls that hides/shows sections (added by Jo) -MB
    			uiHelper.hider();
    			//Type and press Enter to submit Fix
    			$("#emlEmail").keyup(function (event) {
    				if (event.keyCode == 13) {
    					$("#emlEmail").blur();  //fix for IE9
    					self.saveChallengeChangeRequest();
    				}
    			});

    			$("#txtaChangeDetails").on("change keyup paste", function (e) {
    			    var elem = $(this);
                    //Check the logic after 100ms late, as the pasted data gets late to bind to the element.
    			    setTimeout(function () {
    			        // Store the maxlength and value of the field.
    			        var maxlength = elem.attr('maxlength');
    			        var val = elem.val();

    			        // Trim the field if it has content over the maxlength.
    			        if (val.length > maxlength) {
    			            elem.val(val.slice(0, maxlength));
    			        }
    			    }, 100);
    		    });
    		};

    		self.activate = function (nsn) {
    			self.nsn = nsn;
    		};

    		self.compositionComplete = function () {

    			// Ensure placeholder text
    			// Must happen once page is visible, to support IE

    			$.placeholder.shim();
    			uiHelper.focusFirstInputElement();

    		};

    		// ----
    		// -- Properties

    		self.challengeType = ko.observable("0");
    		self.submitClicked = ko.observable(false);

    		//Runtime resource management
    		self.resources2 = resourceService.resources;
    		self.referenceData = referenceService.referenceDictionary;
    		self.currentParameters = parameters;
    		self.challengeTypeOptions = ko.observable(self.referenceData.get('ChallengeType_wPleaseselect')());

    		//#region Internal Methods

    		self.emailAddressInput = ko.observable();

    		self.reasonInput = ko.observable();

    		self.canShowErrors = ko.observable(false);

    		self.challengeTypeValue = ko.computed(function () {
    			if (self.challengeType() === "0") {
    				self.challengeTypeOptions = self.referenceData.get('ChallengeType_wPleaseselect')();
    				return undefined;
    			} else {
    				self.challengeTypeOptions = self.referenceData.get('ChallengeType')();
    				return self.challengeType();
    			}
    		});

    		self.isSubmitting = ko.observable(false);

    		// -------------
    		// -- Validation

    		/*
            Validation:
            1. Challenge Change Reason should be mandatory
            2. Challenge Type is mandatory
            3. Email Address is mandatory            
            4. If Email is given, then the email format should be valid
            */
    		self.requiredFields = ko.observableArray();
    		self.reasonInput.extend({
    			required: {
    				message: "<li>" + resourceService.get('CHCHView_CHCH_ReasonForChallenge') + "</li>",
    				onlyIf: function () { return !self.reasonInput() && self.canShowErrors(); }
    			},
    			containsMoreThanSpaces: {
    				message: "<li>" + resourceService.get('CHCHView_CHCH_ReasonForChallenge') + "</li>",
    				onlyIf: function () { return self.reasonInput() && self.canShowErrors(); }
    			}
    		});
    		self.challengeTypeValue.extend({
    			required: {
    				message: "<li>" + resourceService.get('CHCHView_CHCH_ChallengeType') + "</li>",
    				onlyIf: function () { return !self.challengeTypeValue() && self.canShowErrors(); }
    			},
    			containsMoreThanSpaces: {
    				message: "<li>" + resourceService.get('CHCHView_CHCH_ChallengeType') + "</li>",
    				onlyIf: function () { return self.challengeTypeValue() && self.canShowErrors(); }
    			}
    		});
    		self.emailAddressInput.extend({
    			required: {
    				message: "<li>" + resourceService.get('CHCHView_CHCH_YourEmailAddress') + "</li>",
    				onlyIf: function () { return !self.emailAddressInput() && self.canShowErrors(); }
    			},
    			containsMoreThanSpaces: {
    				message: "<li>" + resourceService.get('CHCHView_CHCH_YourEmailAddress') + "</li>",
    				onlyIf: function () { return self.emailAddressInput() && self.canShowErrors(); }
    			},
    			validEmailAddress: {
    				message: resourceService.resources.get("MessageCode_Validation_Email_004")(),
    				onlyIf: function () { return self.emailAddressInput() && self.canShowErrors(); }
    			}
    		});

    		self.validationGroup = ko.validation.group(self);
    		//Display Errors
    		self.displayErrors = ko.computed(function () {

    			var requiredFieldsCollection = "";
    			var allValidationErrors = [];

    			// split the validation messages to two variables based on their type (required/others)
    			if (self.validationGroup().length > 0) {
    				$.each(self.validationGroup(), function (index, message) {
    					if (message() && message().indexOf("<li>") !== -1) {
    						requiredFieldsCollection += message();
    					} else {
    						allValidationErrors.push(message());
    					}
    				});

    				if (requiredFieldsCollection) {
    					allValidationErrors.unshift(resourceService.get("MessageCode_Validation_MissingMandatoryFields_257").format("<ul>" + requiredFieldsCollection + "</ul>"));
    				}
    			}

    			return allValidationErrors.length > 0 ? allValidationErrors : ko.observableArray();
    		});

    		// ----------
    		// -- Actions

    		//Click Submit - Save and Email the Challenge Change
    		//Set the CanShowError = true, so that the errors will be displayed and return false if there are any errors
    		self.saveChallengeChangeRequest = function () {
    			self.submitClicked(true);
    			self.canShowErrors(true);
    			if (self.displayErrors().length > 0) {
    				return false;
    			}

    			self.isSubmitting(true); // Enable spinner


    			return challengeService.submitChallengeChange(
                    self.nsn,
                    self.challengeTypeValue(),
                    self.reasonInput(),
                    self.emailAddressInput()
                ).done(function (response) {

                	// Show result of Challenge Change fail/pass on screen
                	// Successful submission - No message is sent through?
                	$.placeholder.shim(); // Refresh - Damn IE

                	if (response.Success && response.Messages.length == 0) {

                		// Load new list of challenges to the current student, then navigate to the student view
                		return challengeService.setChallengeChangeForCurrentStudent(self.nsn).done(function () {
                			router.navigate('/students/view/' + self.nsn);
                			logger.log(resourceService.get("MessageCode_Information_ChallengeChangeSubmitted_081"), null, resourceService.get('CHCHView_CHCH_Title'), true);
                		});
                	} else {
                		self.isSubmitting(false); // Disable spinner. (As it hasn't left the page.)
                		logger.logError(resourceService.get("MessageCode_General_SystemFault_789"), null, resourceService.get('CHCHView_CHCH_Title'), true);
                	}
                }).fail(function () {
                	self.isSubmitting(false); // Disable spinner. (As it hasn't left the page.)

                });
    		};

    		//Click Cancel - Revert back to StudentView
    		self.revertBackToStudentView = function () {
    			self.canShowErrors(false);
    			self.submitClicked(false);
    			router.navigate('/students/view/' + self.nsn);
    		};

    		self.canDeactivate = function () {
    			//If Submit clicked, then navigate (validation will be done at submit event)
    			if (self.submitClicked())
    				return true;

    			// Allow deactivation if no changes are made
    			if (!self.challengeTypeValue()
    		        && !self.reasonInput()
    		        && !self.emailAddressInput()) {
    				return true;
    			}
    			// If changes have been made - prompt to confirm
    			var confirmMessage = resourceService.get("MessageCode_Confirmation_NavigateAway_007");
    			return app.showMessage('', confirmMessage, ['Yes', 'No']).done(function (result) {
    				if (result === 'No') {
    					return false;
    				}
    				self.submitClicked(false);
    				return true;
    			});
    		};
    	};
    	return viewModel;

    	//#endregion
    });