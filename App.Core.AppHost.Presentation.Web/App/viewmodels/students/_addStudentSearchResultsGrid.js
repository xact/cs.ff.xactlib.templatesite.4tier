﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/resourceService',
		'services/sessionService',
		'singletons/addStudentSearchResults',
		'singletons/mergeRequest',
		'data/settings'],
	function (router, app, logger, resourceService, sessionService, addStudentSearchResults, mergeRequest, settings) {
		//function (router, app, logger, resourceService, sessionService, addStudentSearchResults, mergeRequest, settings) {

		var viewModel = function () {
			var self = this;
			//var isRecordCreatedBySorted = false;
			//#region viewModel events
			self.activate = function () {
				
			};
			//#region properties
			self.searchResults = addStudentSearchResults;

			self.isVisible = ko.computed(function () {

				return (self.searchResults
	                && self.searchResults.records
	                && self.searchResults.records()
	                && self.searchResults.records().length > 0);
			});

			self.customSortingFlag = ko.observable(true);

			var customDateDDMMMYYYYToOrd = function (date) {
				"use strict"; //let's avoid tom-foolery in this function
				// Convert to a number YYYYMMDD which we can use to order
				var dateParts = date.split(" ");
				return (dateParts[2] * 10000) + ($.inArray(dateParts[1].toUpperCase(), ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]) * 100) + dateParts[0];
			};
			
			// define the sorts
			jQuery.fn.dataTableExt.oSort['date-dd-mmm-yyyy-asc'] = function (a, b) {
				"use strict"; //let's avoid tom-foolery in this function
				if (a === "Unknown") {
					a = "01 JAN 0001";
				}
				if (b === "Unknown") {
					b = "01 JAN 0001";
				}
				var ordA = customDateDDMMMYYYYToOrd(a),
					ordB = customDateDDMMMYYYYToOrd(b);
				return (ordA < ordB) ? -1 : ((ordA > ordB) ? 1 : 0);
			};

			jQuery.fn.dataTableExt.oSort['date-dd-mmm-yyyy-desc'] = function (a, b) {
				"use strict"; //let's avoid tom-foolery in this function
				if (a === "Unknown") {
					a = "01 JAN 0001";
				}
				if (b === "Unknown") {
					b = "01 JAN 0001";
				}
				var ordA = customDateDDMMMYYYYToOrd(a),
					ordB = customDateDDMMMYYYYToOrd(b);
				return (ordA < ordB) ? 1 : ((ordA > ordB) ? -1 : 0);
			};

			var searchResultsConfig = {
				resources: resourceService.resources,
				dataSource: self.searchResults.records,
				rowTemplate: 'addStudentResultRowTemplate',
				columns: [
	                { bSortable: false, mDataProp: 'MatchScore' } // Note: The plugin currently insists that mDataProp be set, even though it's not relevant
	                , { bSortable: false, mDataProp: 'MatchScore' }
	                , { bSortable: false, mDataProp: 'MatchScore' }
	                , 'MatchScore'
	                , 'NSN'
	                , 'FamilyName'
	                , 'GivenNamesSummary'
	        		, { mDataProp: 'BirthDate', bSortable: true, "sType": "date-dd-mmm-yyyy" } // Sort in reverse order, by default
	                , 'Gender'
	                , 'ResidentialStatus'
	                , { bSortable: false, mDataProp: 'CreatedByProviderName' }
	                , { bVisible: false, mDataProp: 'CreatedByProviderName' } // Hidden fields, for sorting
	                , { bVisible: false, mDataProp: 'CreatedOn' }
	                , { bVisible: false, mDataProp: 'NSN' }
				],
				options: {
					// Initial sorting - By IQ Office score, then by the (hidden) NSN field
					"aaSorting": [[3, 'desc'], [13, 'desc']],

					"fnCreatedRow": function (nRow, aData, iDataIndex) {
						// Highlight records over the 'IQ Office score' threshold
						if (aData.MatchType && aData.MatchType === 1) {
							$(nRow).attr('class', 'highlight');
						}
					},

					//Overriding text templates
					"oLanguage": {
						"sInfo": "<span>" + resourceService.get('SearchResultsGridView_SearchResultsGrid_Paging_Results') + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>"
					}
				}
			};

			// Search results table config
			self.resultsConfig = uiHelper.makeDatatableConfig(searchResultsConfig);

			self.doCreatedByCustomSort4Search = function () {
				var oTable = $('#tblAddStudentSearchResults').dataTable();
				var createdByOnHeader = $('#createdByOnHeader');
				if (self.customSortingFlag) {
					oTable.fnSort([[11, 'asc'], [12, 'desc']]);
					createdByOnHeader.removeClass("sorting_desc").addClass("sorting_asc");
				} else {
					oTable.fnSort([[11, 'desc'], [12, 'asc']]);
					createdByOnHeader.removeClass("sorting_asc").addClass("sorting_desc");
				}
				self.customSortingFlag = !self.customSortingFlag;
				self.doneTheStuff;
				if (!self.doneTheStuff) {
					self.doneTheStuff = true;
					$('#tblAddStudentSearchResults th:not(#createdByOnHeader)').click(function () { createdByOnHeader.removeClass("sorting_desc").removeClass("sorting_asc"); });
				}
			};
			
			self.onNavigateToView = function (student) {
				var confirmMessage = resourceService.get("MessageCode_Confirmation_DiscardRecordAddition_033");
				app.showMessage('', confirmMessage, ['Yes', 'No']).then(function (result) {
					if (result === 'Yes') {
						router.navigate('/students/view/' + student.NSN);;
					}
				});
			};
			
			this.resources2 = resourceService.resources;

			self.compositionComplete = function () {
				
				//self.configureTable();
			};

			// Listen for updates to the search result set
			self.searchResults.records.subscribe(function (newValue) {
				if (!newValue || newValue.length === 0) {
					return;
				}

			});
		};

		return viewModel;
	}
);