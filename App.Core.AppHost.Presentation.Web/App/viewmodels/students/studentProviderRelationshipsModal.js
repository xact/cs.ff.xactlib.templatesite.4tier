define(['plugins/dialog',
		'services/logger',
		'services/resourceService',
		'services/studentProviderRelationshipService',
		'services/sessionService',
		//'singletons/student',
		'data/settings',
		'libs/validationLib'],
    function (dialog, logger, resourceService, sprService, sessionService,
				settings, validationLib) {


    	var StudentProviderRelationship = function () {
    		var self = this;

    		self.ProviderCode = ko.observable();
    		self.ProviderName = ko.observable();
    		self.resources = resourceService.resources;
    		
    		self.ProviderCode.extend({
    			validProviderCode: {
    				message: resourceService.get("MessageCode_Validation_InvalidProviderCode_339"),
    				onlyIf: function () { return self.ProviderCode(); }
    			}

    		});
    	};

    	var CustomModal = function (options) {
    		var self = this;

    		if (!options) {
    			options = {};
    		}

    		//document.addEventListener('keydown', function (e) {
    		//	if (e.which == 9) {
    		//		return true;
    		//	}
    		//});

    		// -------------
    		// -- Properties
    		self.studentId = options.studentId;
    		self.studentNsn = options.studentNsn;
    		self.studentName = options.studentName;

    		self.studentProviderRelationships = ko.observableArray();
    		self.sprOriginalData = ko.observableArray();

    		self.sprsToAdd = ko.observableArray();

    		self.settings = settings;

    		// -------------------
    		// -- State properties
    		self.isInEditMode = ko.observable(false);
    		self.isInAddMode = ko.observable(false);

    		self.checks = {
    			isEditModeAvailable: ko.computed(function () {
    				return (sessionService.isAuthorised(settings.securityClaims.NSI2_PROVIDER)
						&& !self.isInEditMode());
    			})
    		};



    		// ----------------------
    		// -- Validation & config
    		self.validationErrors = ko.computed(function () {
    			return ko.validation.group([self.studentProviderRelationships, self.sprsToAdd], { observable: true, deep: true })();
    		});

    		self.notValidBorderCss = function (boundField, action) {
    			return boundField.isModified() && !boundField.isValid() && self.canShowErrors() && action == self.action() ? 'errorBorder' : 'normalBorder';
    		};

    		// Allow for manually inserted errors
    		self.extraValidationErrors = ko.observableArray();

    		// Combines realtime + manual validations
    		self.allValidationErrors = ko.computed(function () {

    			// Don't show validation messages when not in add or edit mode
    			if (!self.isInAddMode() && !self.isInEditMode()) {
    				return [];
    			}

    			var validationErrors = self.validationErrors();

    			validationErrors = validationErrors.concat(self.extraValidationErrors());



    			// Flatten to non-observables, to allow duplicate-checking below
    			validationErrors = ko.utils.arrayMap(validationErrors, function (item) {
    				return ko.utils.unwrapObservable(item);
    			});

    			// Remove duplicates - gives a tidier validation for multiple required fields in Edit mode.
    			validationErrors = ko.utils.arrayGetDistinctValues(validationErrors);
    			//Clear the validation from the static array
    			self.extraValidationErrors([]);
    			return validationErrors;
    		});

    		var sprTableconfig = {
    			dataSource: self.studentProviderRelationships,
    			rowTemplate: "studentProviderRelationshipModalTemplate",
    			columns: [
				    { mDataProp: 'ProviderName' },
				    { mDataProp: 'DateCreated' },
				    { mDataProp: 'UntilDate', sClass: 'width150' }
    			],
    			options: {
    				// initial sorting
    				"aaSorting": [[2, 'desc']], // By 'Until date', descending

    				'iDisplayLength': 6, //self.settings.defaultShortGridviewLength,

    				"bAutoWidth": false,

    				//Overriding text templates
    				"oLanguage": {
    					//"sInfo": self.studentProviderRelationshipsModal_GridRelationshipInfo + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong>",
    					"sInfo": "Displaying Relationship(s) " + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong>",
    					//"sInfoEmpty": self.studentProviderRelationshipsModal_NoRelationships
    					//"sInfoEmpty": "No relationships",
    					"sEmptyTable": "No relationships",
    					"sInfoEmpty": ""
    				}//,

    				//customising DOM
    				//"sDom": '<"paging"i<"nav"p>>rt<"paging"i<"nav"p>>'
    			}
    		};

    		var sprTableConfig = uiHelper.makeDatatableConfig(sprTableconfig);

    		self.sprTableConfig = sprTableConfig;
    	};

    	//#region viewModel events
    	//self.activate = function (context) { activate(context); };

    	// -- Main entity - Current student

    	//#region resources grab

    	////Page elements resource packs
    	//var resource = function() {
    	//    self.studentProviderRelationshipsModal_Title = resourceService.get('studentProviderRelationshipsModal_Title');
    	//	self.studentProviderRelationshipsModal_ProviderName = resourceService.get('studentProviderRelationshipsModal_ProviderName');
    	//	self.studentProviderRelationshipsModal_DateCreated = resourceService.get('studentProviderRelationshipsModal_DateCreated');
    	//	self.studentProviderRelationshipsModal_UntilDate = resourceService.get('studentProviderRelationshipsModal_UntilDate');
    	//	self.studentProviderRelationshipsModal_GridRelationshipInfo = resourceService.get('studentProviderRelationshipsModal_GridRelationshipInfo');
    	//	self.studentProviderRelationshipsModal_NoRelationships = resourceService.get('studentProviderRelationshipsModal_NoRelationships');
    	//};
    	//resource();


    	CustomModal.prototype.init = function (options) {
    		var self = this;
			
    		$("*").on('focusin', function (e) {
    			var modalWindow = $('#simplemodal-container')[0];
    			if (modalWindow !== e.target && !modalWindow.contains(e.target)) {
				    e.target.blur();
				    //modalWindow.attr("tabindex", -1).focus();
			    }
		    });

    		// Load the SPR data
    		var loadDataPromise = sprService.getSPRs(options.studentId).done(function (data) {

    			// Keep original data, to allow 'cancel' to reset the page
    			var clonedData = ko.toJS(data);
    			self.sprOriginalData(clonedData);

    			attachValidation(data);

    			self.studentProviderRelationships(data);
    		});

    		return loadDataPromise;
    	};

    	// ----------
    	// -- Actions

    	/// Save edited SPRs (not the added ones)
    	CustomModal.prototype.onSave = function () {

    		var self = this;

    		if (self.validationErrors().length > 0) {
    			return;
    		}

    		var updatedRelationships = ko.utils.arrayFilter(this.studentProviderRelationships(), function (item) {
    			return item.UntilDate.isDirty();
    		});

    		if (updatedRelationships.length === 0) {
    			dialog.close(self);
    			return;
    		}

    		var saveResult = sprService.updateSPRs(updatedRelationships);

    		saveResult.done(function (result) {
    			processSaveResult(result, self);
    		});

    	};

    	CustomModal.prototype.onClose = function () {
    		//document.addEventListener('keydown', function (e) {
    		//	if (e.which == 9) {
    		//		return true;
    		//	}
    		//});
    		dialog.close(this);
    		
    	};

    	CustomModal.prototype.onCancelEdit = function () {
    		var self = this;

    		// Reset values (Take a copy of the original, and re-apply validation)
    		var originalData = ko.toJS(self.sprOriginalData());
    		attachValidation(originalData);

    		self.studentProviderRelationships(originalData);

    		// Revert to view mode
    		self.isInEditMode(false);
    	};

    	CustomModal.prototype.onEnableEditMode = function () {
    		this.isInEditMode(true);
    		$.placeholder.shim();
    		$('input.hasDatepicker:first').focus();

    	};

    	CustomModal.prototype.onEnableAddMode = function () {
    		var self = this;
    		this.isInAddMode(true);
    		this.isInEditMode(false);

    		this.sprsToAdd.push(new StudentProviderRelationship());

    		$.placeholder.shim();
    		$('#txtProviderCode').focus();

		    $('#txtProviderCode').addClass('error');

		    //$("#txtProviderCode").on("keyup", function (event) {
		    //	if (event.keyCode == 13) {
		    //		$("#txtProviderCode").blur();  //fix for IE9
		    //		self.onAddProvider();
		    //	}
		    //});

	    };

    	CustomModal.prototype.onAddProvider = function () {

    		var self = this;

    		if (self.validationErrors().length > 0) {
    			return;
    		}

    		var firstSPR = self.sprsToAdd()[0];

    		// Pre-validation
    		var alreadyContainsProviderCode = ko.utils.arrayFirst(this.studentProviderRelationships(), function (item) {
    			return item.ProviderCode == firstSPR.ProviderCode() && moment(item.UntilDate()).isAfter(moment());
    		});

    		if (alreadyContainsProviderCode) {
    			var sprAlreadyExists = resourceService.get("MessageCode_Error_StudentProviderRelationshipExists_947");

    			// Reset with single message
    			self.extraValidationErrors([sprAlreadyExists]);

    			return;
    		} else if (!firstSPR.ProviderCode() || firstSPR.ProviderCode().trim() == "") {
    			var mandatoryFieldsMessage = resourceService.get("MessageCode_Validation_MissingMandatoryFields_257").format('');
    			var fieldMsg = "<ul><li>Provider code</li></ul>";

    			var msg = mandatoryFieldsMessage + fieldMsg;
    			self.extraValidationErrors([msg]);
    			return;
    		} else {
    			// Clear extra validation
    			self.extraValidationErrors([]);
    		}

    		sprService.addSPR(self.studentId, firstSPR.ProviderCode()).done(function (result) {
    			processSaveResult(result, self);
    		}).fail(function (result) {

    			// Make sure to close modal on session timeout
    			if (!result || result.status === 401) {
    				dialog.close(self);
    			}
    		});

    	};



    	// Dialog show
    	CustomModal.show = function (options) {
    		var newModal = new CustomModal(options);

    		var initPromise = newModal.init(options);

    		//document.addEventListener('keydown', function (e) {
    		//	if (e.which == 9) {
    		//		e.preventDefault();
    		//	}
    		//});

    		return initPromise.then(function () { return dialog.show(newModal); });
    	};

    	// -- Private functions
    	var processSaveResult = function (response, self) {
    		var hasSuccessMessage = validationLib.hasMessageCode(response, 948);

    		// The result may also have 0 messages - a gap in the specs where we close the modal, but don't give a message
    		// (To avoid giving a message about something that didn't actually happen)
    		var isSuccess = hasSuccessMessage || response.Messages.length === 0;

    		// Validation
    		if (!isSuccess) {
    			//logger.logError(result.Messages[0].PresentationAttributes.Text, null, null, true);
    			var validationMessages = validationLib.getAllMessagesAsText(response);

    			self.extraValidationErrors(validationMessages);

    			return false;
    		}

    		// Show success message, if it was sent
    		if (hasSuccessMessage) {
    			var successMessage = resourceService.get("MessageCode_Confirmation_StudentProviderRelationshipCreated_948");
    			logger.log(successMessage, null, null, true);
    		}


    		dialog.close(self);

    		return true;
    	};

    	var attachValidation = function (sprList) {

    		var self = this;

    		ko.utils.arrayForEach(sprList, function (item) {

    			item.OriginalUntilDate = item.UntilDate;

    			// Convert to observable
    			item.UntilDate = ko.observable(item.UntilDate);



    			// Add tracking property
    			item.UntilDate.extend({ trackChange: true });

    			item.UntilDate.extend({
    				validNzDate: {
    					message: resourceService.get("MessageCode_Validation_Date_001").format("Until date")
    					//onlyIf: function () {
    					//	return student.NameBirthDateVerificationDateTime()
    					//			&& checks.canModifyNameDobVerification();
    					//}
    				},
    				nzDate: {
    					message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Until date"),
    					onlyIf: function () {
    						return item.UntilDate();
    					}
    				},
    				maxDate: {
    					params: moment().add('days', item.ActiveDurationDays),
    					message: resourceService.get("MessageCode_Info_ActiveUntilDateOutOfRange_423"),
    					onlyIf: function () {
    						return item.UntilDate() && item.UntilDate.isDirty(); // Only invalidate when they've changed it
    					}
    				},
    				minDateIsToday: {
    					params: moment(),
    					message: resourceService.get("MessageCode_Info_ActiveUntilDateOutOfRange_423"),
    					onlyIf: function () {
    						return item.OriginalUntilDate !== item.UntilDate();
    					}
    				},
    				required: {
    					message: function () {
    						var mandatoryFieldsMessage = resourceService.get("MessageCode_Validation_MissingMandatoryFields_257").format('');
    						var fieldMsg = "<ul><li>Until date</li></ul>";

    						var msg = mandatoryFieldsMessage + fieldMsg;
    						return msg;
    					}
    				}
    			});

    		});
    	};

    	return CustomModal;


    	//#endregion
    });