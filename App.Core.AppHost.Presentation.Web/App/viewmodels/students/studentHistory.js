define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/resourceService',
		'services/studentService',
		'services/sessionService',
		'services/studentProviderRelationshipService',
		'singletons/student',
		'singletons/studentUpdateHistory',
		'viewModels/students/studentProviderRelationshipsModal',
		'data/settings',
        'data/enums'],
    function (router, app, logger, resourceService, studentService, sessionService, sprService,
			student, studentUpdateHistory, sprModal, settings, enums) {
	    
        var viewModel = function () {
            var self = this;
            self.updateFieldFormat = false;
            //#region viewModel events
            //self.activate = function (context) { activate(context); };
            self.settings = com.nsi.frontend.data.settings;

            //self.resourceService = resourceService;
            self.resources2 = resourceService.resources;

            //#region Internal Methods
            self.activate = function (nsn) {
                self.nsn = nsn;
                var promise = studentService.setCurrentStudentByNSN(nsn).done(function() {
                	updateSprCount();

                });
                // Set the current (app-wide) student to the requested NSN

                //studentService.setStudentAuditHistoryByNSN(nsn);

                studentService.setStudentUpdateHistory(nsn);


                return promise;
            };

            // -- Main entity - Current student
            self.currentStudent = student;
            self.studentUpdateHistory = studentUpdateHistory;
            //self.studentAuditUpdateGroup = studentAuditUpdateGroup;

	        self.sprCount = ko.observable("...");

            self.navigateToSlaveDetails = function (nsn) {
                router.navigate('/students/view/' + nsn);
            };

        	// -- Actions

	        self.onShowSprModal = function() {
	        	sprModal.show({
	        		studentId: self.currentStudent.Id(),
	        		studentNsn: self.currentStudent.NSN(),
	        		studentName: self.currentStudent.GivenName1() + " " + self.currentStudent.FamilyName()
	        	}).done(updateSprCount); // Update SPR count after
	        };

	        var updateSprCount = function () {
	        	return sprService.getSprCounts(student.Id()).done(function (sprCounts) {
	        		var sprCountRecord = sprCounts[0];

	        		self.sprCount(sprCountRecord.SprCount);
	        	});
	        };

            // -- Security trimming
            self.shouldShowRelationships = sessionService.isAuthorised(settings.securityClaims.NSI2_VIEW_SPR);

            // -- Properties
            self.setasPreferredName = ko.computed(function () {
                return self.currentStudent.PreferredNameIndicator() == true ? resourceService.get('resYes') : resourceService.get('resNo');
            }, this);

            self.nameAndDOBVerification = ko.computed(function () {
                var nameDobVerification = self.currentStudent.NameBirthDateVerificationMethod();
                var nameDobVerifiedByProvider = self.currentStudent.NameBirthDateVerifiedByProvider();
                var result = (nameDobVerification === ""
    				|| typeof nameDobVerification === 'undefined'
    				|| nameDobVerification == null
    				|| nameDobVerifiedByProvider === ""
    				|| typeof nameDobVerifiedByProvider === 'undefined'
    				|| nameDobVerifiedByProvider == null) ?
                    'Unknown' : (self.currentStudent.NameBirthDateVerifiedByProvider().ProviderName
	                    + ' on sight of '
	                    + self.currentStudent.NameBirthDateVerificationMethod().description
	                    + ' on '
	                    + moment(new Date(self.currentStudent.NameBirthDateVerificationDateTime())).format("DD MMM YYYY"));
                return result;
            }, this);

            self.residentialStatusVerification = ko.computed(function () {
                var residentialStatusVerification = self.currentStudent.ResidentialStatusVerificationMethod();
                var residentialStatusVerifiedByProvider = self.currentStudent.ResidentialStatusVerifiedByProvider();
                var result = (residentialStatusVerification === ""
    					|| typeof residentialStatusVerification === 'undefined'
    					|| residentialStatusVerification == null
    					|| residentialStatusVerifiedByProvider === ""
    					|| typeof residentialStatusVerifiedByProvider === 'undefined'
    					|| residentialStatusVerifiedByProvider == null)
    				? 'Unknown'
    				: (self.currentStudent.ResidentialStatusVerifiedByProvider().ProviderName
    					+ ' on sight of '
    					+ self.currentStudent.ResidentialStatusVerificationMethod().description
    					+ ' on '
    					+ moment(new Date(self.currentStudent.ResidentialStatusVerificationDateTime())).format("DD MMM YYYY"));
                return result;
            }, this);

            /*
            Story:
            Message#018 comes with parameter {0}, on which MasterNSN will be applied here.
            But the issue is need to provide hyper-link only to part of the string (which is MasterNSN)
            So the tweak is, need to format with MasterNSN value with the original message#018 to avoid {0} displaying in page,
            then split and send the part message only except MasterNSN
            and then link the MasterNSN in html seperately.
            So easy isnt it? Not feel good, then please feel free to change it with your expertise..
            */
            self.slaveMessage = function () {
                var fullString = self.resources2.get('MessageCode_Information_SlaveOfMaster_018')().format(self.currentStudent.MasterNSN());
                var indexOfMasterNsn = fullString.indexOf(self.currentStudent.MasterNSN());
                return fullString.substring(0, indexOfMasterNsn);
            };
            

			// ------------
            // -- Visibility trimming
            self.isSlaveRecordsVisible = ko.computed(function () {
                return (self.currentStudent.Slaves().length !== 0); // GB - Test
            }, this);

            self.isDeathDateVisible = ko.computed(function () {
                return self.currentStudent.StudentStatusFK() === enums.studentStatus.inactive && self.currentStudent.StudentStatusReasonFK() === enums.studentStatusReason.deceased;
            }, this);

            self.isButtonViewVisible = ko.computed(function () {
                return true;
            }, this);

            //self.isButtonRollbackVisible = ko.observable(sessionService.isAuthorised(settings.securityClaims.NSI2_ROLLBACK_STUDENT));
            self.isButtonRollbackVisible = false;

            self.isButtonRollbackEnable = ko.computed(function () {
                return self.currentStudent.isMaster() ? true : false;
            }, this);

            self.shouldShowNameDobConfirmed = ko.computed(function () {
                return self.currentStudent.NameBirthDateVerificationMethodFK() !== enums.nameBirthDateVerificationMethod.unverified;
            }, this);

            self.shouldShowResidentialStatusConfirmed = ko.computed(function () {
                return self.currentStudent.ResidentialStatusVerificationMethodFK() !== enums.residentialStatusVerificationMethod.unverified;
            }, this);

			// -------------
			// -- Properties - Calculated
            self.sortedAltNames = ko.computed(function () {
                return self.currentStudent.AltNames().sort(function (left, right) {
                    return left.CreatedDateTime() == right.CreatedDateTime() ?
                        0 :
                        (moment(left.CreatedDateTime()) < moment(right.CreatedDateTime()) ? -1 : 1);
                });
            });

            self.sortedSlaveRecords = ko.computed(function () {
                return self.currentStudent.Slaves().sort(function (left, right) {
                    return left.Id() == right.Id() ?
                        0 :
                        (left.Id() > right.Id() ? -1 : 1);
                });
            });


			// ------------
            // -- Functions

            self.isVerified = function(nameBirthDateVerificationMethodFkValue) {
                return nameBirthDateVerificationMethodFkValue === enums.nameBirthDateVerificationMethod.unverified ? 
                self.resources2.get('StudentHistoryView_StudentHistory_Unverified')()
                    :
                self.resources2.get('StudentHistoryView_StudentHistory_Verified')();
            };

            self.fieldValue = function (fieldUpdateType, oldValue, newValue) {
                var returnString;
                if ($.trim(fieldUpdateType) == "Updated") {
                    returnString = 'from ' + oldValue + ' to ' + newValue;
                    self.updateFieldFormat = false;
                }
                else if ($.trim(fieldUpdateType) == "Created") {
                    returnString = "";
                    self.updateFieldFormat = false;
                } else {
                    returnString = newValue;
                    self.updateFieldFormat = true;
                }
                return returnString;
            };
            
            // Setting up the recordupdatehistory datatable
            var recordUpdateHistoryTableConfig = {
                dataSource: self.studentUpdateHistory.records,
                rowTemplate: 'RecordUpdateHistoryRowTemplate',
                columns: [
					{ bSortable: true,  mDataProp: 'ModifiedDateTime', sDefaultContent: "" }
					, { bSortable: true, mDataProp: 'ModifiedByProviderName', sDefaultContent: "" }
                    , { bSortable: true,  mDataProp: 'UpdateType', sDefaultContent: "" }
					, { bSortable: true, mDataProp: 'FieldUpdated', sDefaultContent: "" }
                    , { bSortable:false, mDataProp: 'OldValue', sDefaultContent: "" }
                    , { bSortable: false, mDataProp: 'NewValue', sDefaultContent: "" }
                ],
                options: {
                    // initial sorting
                    "aaSorting": [[0, 'desc']], // Default - Sort by ModifiedDate and ModifiedBy Desc

                    //"aoColumnDefs": [{ "aDataSort": [0, 1], "aTargets": [0] },
                    //{ "aDataSort": [0, 1], "aTargets": [1] }
                    //],
                    //Gridview length
                    'iDisplayLength': settings.defaultHistoryGridviewLength,

                    "fnDrawCallback": function() {
                    	groupTable();
                    	if (self.studentUpdateHistory.records().length == 0) {
                    		$('#modifyDateHeader').removeClass("sorting_asc").removeClass("sorting_desc");
                    	}
                    },
                    
                    //"fnDrawCallback": function() {
                    //    this.rowGrouping();
                        //"iGroupingColumnIndex": 2,
                        //"sGroupBy": "letter",
                        //"bHideGroupingColumn": false}], 
                    
                        //Overriding text templates
                    "oLanguage": {
                    	"sInfo": self.resources2.get('StudentHistoryView_StudentHistoryView_RecordUpdateHistoryPagingInfo')() + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong>",
                        "sEmptyTable": " ",
                        "sInfoEmpty": " " //resourceService.get('StudentDetailsView_ViewStudent_GridAltInfo')
                    }
                }
            };

            self.recordUpdateHistoryTableConfig = uiHelper.makeDatatableConfig(recordUpdateHistoryTableConfig);

            var groupTable = function () {
            // combine identical dimension fields in product data tables
                $('#tbl_RecordUpdateHistory').each(function () {

                    var dimension_cells = new Array();
                    var dimension_col = null;

                    var i = 1;
                    // First, scan first row of headers for the "Dimensions" column.
                    $(this).find('th').find('span').each(function () {
                        if ($(this).text() == 'Modified by') {
                                dimension_col = i;
                            }
                            i++;
                    });

                    // first_instance holds the first instance of identical td
                    var first_instance = null;
                    var first_date = null;
                    // iterate through rows
                    $(this).find('tr').each(function () {

                        // find the td of the correct column (determined by the dimension_col set above
                        if (dimension_col != null) {
                            var dimension_td = $(this).find("td:nth-child(" + dimension_col + ")");

                            if (first_instance == null) {
                                // must be the first row
                                first_instance = dimension_td;
                                first_date = dimension_td.closest('td').prev('td');
                            } else if (dimension_td.html() == first_instance.html() && dimension_td.closest('td').prev('td').html() == first_date.html()) {
                                // the current td is identical to the previous
                                // remove the current td
                                //dimension_td.remove();
                                //dimension_td.append('<td> </td>');
                                $(this).closest('tr').prev('tr').addClass('noBorder');
                                dimension_td.html(' ');
                                dimension_td.closest('td').prev('td').html(' ');
                                // increment the rowspan attribute of the first instance
                                //first_instance.attr('rowspan', first_instance.attr('rowspan') + 1);
                            } else {
                                // this cell is different from the last
                                first_instance = dimension_td;
                                first_date = dimension_td.closest('td').prev('td');
                            }
                        }

                    });
                });
             };
            
            self.attached = function () { groupTable(); };
            
        };
        
        return viewModel;


        //#endregion
    });