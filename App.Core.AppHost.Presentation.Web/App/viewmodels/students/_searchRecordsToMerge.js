﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/resourceService',
		'services/sessionService',
		'singletons/searchResults',
		'singletons/mergeRequest',
		'data/settings'],
	function (router, app, logger, resourceService, sessionService, searchResults, mergeRequest, settings) {

	    var viewModel = function () {
	        var self = this;

	        //#region viewModel events
	        self.activate = function () {

	            // Subscribe to 'search reset' event
	            app.on("studentSearch:reset").then(function () {
	                self.mergeRequest.clear();
	            });

	            app.on("studentSearch:resetRecordsToMerge").then(function () {
	                self.mergeRequest.clear();
	            });
	        };

	        //#region properties

	        self.mergeRequest = mergeRequest;

	        self.resources = resourceService.resources;

	        self.isVisible = ko.computed(function () {

	            return (self.mergeRequest
					&& self.mergeRequest.records
					&& self.mergeRequest.records()
					&& self.mergeRequest.mergeIsActivated()
				//	&& self.mergeRequest.records().length > 0
				);
	        });



	        // Security trimming
	        self.isRequestMergeAvailable = sessionService.isAuthorised(settings.securityClaims.NSI2_MERGE_REQUEST);

	        self.isRequestMergeEnabled = ko.computed(function () {
	            // We require 2 or more records to allow a merge request
	            return self.isRequestMergeAvailable && mergeRequest.records().length >= 2;
	        });

	        // -- Actions

	        self.onCancel = function () {
	            app.showMessage('', 'Would you like to discard the selection of students to merge?', ['Yes', 'No']).then(function (result) {
	                if (result === 'Yes') {
	                    mergeRequest.clear();
	                    searchResults.resetIsSelectedForMerge();
	                    mergeRequest.showMaxAllowedRecordsError(false);
	                    mergeRequest.mergeIsActivated(false);
	                }
	            });
	        };

	        self.onMerge = function () {
	            router.navigate("#/merge/entry");
	        };

	        self.onRemoveItem = function (student) {
	            //mergeRequest.records.remove(student);

	            // Remove by NSN match. The plain 'remove(student)' doesn't work...
	            mergeRequest.records.remove(function (item) {
	                return item.NSN === student.NSN;
	            });

	            // In the corresponding search result, set it as no longer selected for merge
	            var matchedSearchRecords = ko.utils.arrayFilter(searchResults.records(), function (item) {
	                return student.NSN === item.NSN;
	            });

	            ko.utils.arrayForEach(matchedSearchRecords, function (item) {
	                item.isSelectedForMerge(false);
	                mergeRequest.showMaxAllowedRecordsError(false); // ? No thrilled about this...
	            });

	        };

	        var mergeTableConfig = {
	            dataSource: mergeRequest.records,
	            rowTemplate: 'RecordsToMergeRowTemplate',
	            columns: [
					{ bSortable: false, mDataProp: 'MatchScore' } // Note: The plugin currently insists that mDataProp be set, even though it's not relevant
					, { bSortable: false, mDataProp: 'MatchScore' }
					, { bSortable: false, mDataProp: 'MatchScore' }
					, { bSortable: false, mDataProp: 'MatchScore' }
					, 'MatchScore'
					, 'NSN'
					, 'FamilyName'
					, 'GivenNamesSummary'
					, { mDataProp: 'BirthDate', "asSorting": ["desc", "asc"] } // Sort in reverse order, by default
					, 'Gender'
					, 'ResidentialStatus'
					, 'CreatedByProviderName'
				    , { bVisible: false, mDataProp: 'CreatedByProviderName' } // Hidden fields, for sorting
	                , { bVisible: false, mDataProp: 'CreatedOn' }
	            ],
	            options:
				{
				    // Initial sorting - By IQ Office score
				    "aaSorting": [], //enforce order by selection.

				    "fnCreatedRow": function (nRow, aData, iDataIndex) {
				        // Highlight records over the 'IQ Office score' threshold
				        if (aData.Score && aData.Score > settings.iqOfficeMatchHighlightThreshold) { // TODO: Switch to use 'Type' property from IQ Office, once ready
				            $(nRow).attr('class', 'highlight');
				        }
				    },

				    //Overriding text templates
				    "oLanguage": {
				        "sInfo": "<span>Displaying records to merge <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
				        "sInfoEmpty": " ",
				        "sEmptyTable": " "
				    }
				}
	        };

	        // Custom search - #MB
	        self.doCreatedByOnCustomSort4Merge = function () {
	            var oTable = $('#tblMergeRequest').dataTable();
	            var createdByOnHeader = $('#recordCreatedByOnHeader');
	            if (self.customSortingFlag) {
	                oTable.fnSort([[12, 'asc'], [13, 'desc']]);
	                createdByOnHeader.removeClass("sorting_desc").addClass("sorting_asc");
	            } else {
	                oTable.fnSort([[12, 'desc'], [13, 'asc']]);
	                createdByOnHeader.removeClass("sorting_asc").addClass("sorting_desc");
	            }
	            self.customSortingFlag = !self.customSortingFlag;
	            self.doneTheStuff;
	            if (!self.doneTheStuff) {
	                self.doneTheStuff = true;
	                $('#tblMergeRequest th:not(#recordCreatedByOnHeader)').click(function () { createdByOnHeader.removeClass("sorting_desc").removeClass("sorting_asc"); });
	            }
	        };

	        //Page elements resource packs
	        var resource = function () {
	            self.valMsg = resourceService.get('ComDefaultValidationMessage');
	            self.searchResultGridTitle_NSN = resourceService.get('searchResultGridTitle_NSN');
	            self.searchResultGridTitle_Score = resourceService.get('searchResultGridTitle_Score');
	            self.searchResultGridTitle_Familyname = resourceService.get('searchResultGridTitle_Familyname');
	            self.searchResultGridTitle_Givenname = resourceService.get('searchResultGridTitle_Givenname');
	            self.searchResultGridTitle_Birthdate = resourceService.get('searchResultGridTitle_Birthdate');
	            self.searchResultGridTitle_Gender = resourceService.get('searchResultGridTitle_Gender');
	            self.searchResultGridTitle_ResidentialStatus = resourceService.get('searchResultGridTitle_ResidentialStatus');
	            self.searchResultGridTitle_CreatedBy = resourceService.get('searchResultGridTitle_CreatedBy');
	            self.searchResultGrid_MasterRecord = resourceService.get('searchResultGrid_MasterRecord');
	            self.searchResultGrid_MasterRecordTooltip = resourceService.get('searchResultGrid_MasterRecordTooltip');
	            self.searchResultGrid_AltName = resourceService.get('searchResultGrid_AltName');
	            self.searchResultGrid_AltNameTooltip = resourceService.get('searchResultGrid_AltNameTooltip');
	            self.searchResultGrid_View = resourceService.get('searchResultGrid_View');
	            self.searchResultGrid_ViewTooltip = resourceService.get('searchResultGrid_ViewTooltip');
	            self.searchResultGrid_Merge = resourceService.get('searchResultGrid_Merge');
	            self.searchResultGrid_MergeTooltip = resourceService.get('searchResultGrid_MergeTooltip');
	            self.searchResultGrid_IconLink = resourceService.get('searchResultGrid_IconLink');
	            self.searchResultGrid_IconAltName = resourceService.get('searchResultGrid_IconAltName');
	            self.searchResultGrid_IconView = resourceService.get('searchResultGrid_IconView');
	            self.searchResultGrid_IconMerge = resourceService.get('searchResultGrid_IconMerge');
	            self.searchResultGrid_IconRemove = resourceService.get('searchResultGrid_IconRemove');
	            self.searchResultGrid_RemoveTooltip = resourceService.get('searchResultGrid_RemoveTooltip');
	            self.searchResultGrid_Paging_Results = resourceService.get('searchResultGrid_Paging_Results');
	            self.searchResultGrid_ResultsHeader = resourceService.get('searchResultGrid_ResultsHeader');
	            self.searchResultGrid_Key = resourceService.get('searchResultGrid_Key');
	            self.searchResultNoResults = resourceService.get('MessageCode_General_NoSearchResultsFound_785');
	        };
	        resource();

	        // Search results table config
	        self.resultsConfig = uiHelper.makeDatatableConfig(mergeTableConfig); // Adds default settings

	        self.compositionComplete = function () {
	            //self.configureTable();
	        };


	    };

	    return viewModel;
	}


);