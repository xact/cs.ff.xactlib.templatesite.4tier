//addStudent

define(['plugins/router',
        'durandal/app',
		'services/logger',
		'services/resourceService',
        'services/studentService',
		'singletons/student',
        'singletons/searchCriteria',
		'singletons/addStudentSearchResults',
        'services/referenceService',
        'data/enums',
		'libs/studentLib',
		'libs/uiLib',
		'data/settings',
        'data/referenceData',
		'services/sessionService',
		'singletons/parameters',
		'entities/studentEntity'
],
    function (router,
	    app,
	    logger,
	    resourceService,
	    studentService,
	    student,
	    searchCriteria,
		addStudentSearchResults,
	    referenceService,
	    enums,
	    studentLib,
	    uiLib,
	    settings,
	    referenceData,
	    sessionService,
	    parameters,
		studentEntity) {

    	var viewModel = function () {
    		var self = this;

    		//injected services
    		self.resources = resourceService.resources;
    		//self.referenceData = referenceService.referenceDictionary;

    		// -------------------
    		// -- Lifecycle events
    		self.canActivate = function () {
    			if (sessionService.isAuthorised(settings.securityClaims.NSI2_ADD_STUDENT)) {
    				return true;
    			}
    			return false;
    		};

    		self.activate = function () {
    			initializeFromSearchCriteria(self);
    			// self.currentStudent = self.student;
    			var origData = ko.toJS(self.student);
    			ko.mapping.fromJS(origData, null, self.currentStudent);

		    };

    		self.attached = function () {
    			self.uiHelper = com.uiHelper;
    		};


    		self.canDeactivate = function () {
    			if (self.student.noResults() || self.cancelled()) {//cancel button is displayed on add student page in 2 different ways - in the regular one and the one where close matches are found. On regular page if no changes made then do not prompt 007. Otherwise prompt 007 everytime. self.cancelled helps create disparity.

					
					// Allow deactivation if no changes are made
    				if (self.student.noResults() && !hasModifiedStudent()) {
						return true;
					}

					// If changes have been made - prompt to confirm
					var navigateAwayMessage = resourceService.get("MessageCode_Confirmation_NavigateAway_007");

					return app.showMessage("", navigateAwayMessage, ['Yes', 'No']).done(function (result) {
						if (result === "No") { // 'Stay on page' option
							self.cancelled(false);
							return false;
							}

						return true;
					});
				}
			    return true;
		    };

    		var hasModifiedStudent = function () {
			    return ko.utils.hasChanges(self.currentStudent, self.student) // Student fields have changed
				    || self.newAltNames().length != 1 // Any alt names have been added or the default add name widget has been deleted.
				    || self.hasAddedAltnames();
			    
		    };

    		self.compositionComplete = function () {

    			// Ensure placeholder text
    			// Must happen once page is visible, to support IE

    			$.placeholder.shim();
    			uiHelper.focusFirstInputElement();

    		};

    		// -------------
    		// -- Properties

    		self.valMsg = "!";
    		self.notValidField = uiHelper.notValidField;
    		self.altNamesChanged = ko.observable(false);
		    self.cancelled = ko.observable(false);
    		self.isRunningSave = ko.observable(false);
    		self.isRunningConfirm = ko.observable(false);
		    self.currentStudent = new studentEntity();
    		self.student = {
    			FamilyName: ko.observable(),
    			GivenName1: ko.observable(),
    			GivenName2: ko.observable(),
    			GivenName3: ko.observable(),
    			dob: ko.observable(""),
    			preferredName: ko.observable(false),
    			NameBirthDateVerificationMethodFK: ko.observable(),
    			isDobUnknown: ko.observable(false),
    			nameDobVerification: ko.observable(),
    			residentialStatus: ko.observable(),
    			residentialStatusVerificationMethod: ko.observable(),
    			gender: ko.observable()
    		};

    		self.student.preferredNameDisplay = ko.computed(function () {
    			return self.student.preferredName() ? "Yes" : "No";
    		});

    		self.student.GivenNames = ko.observable();
    		self.student.dobDisplay = ko.computed(function () {
    			return self.student.dob() != "" && moment(self.student.dob()).diff(moment("11/11/1918")) != 0 ? self.student.dob() : "Unknown";
    		});




    		self.currentParameters = parameters;
    		self.userAllowedAddDuplicate = ko.observable(sessionService.isAuthorised(settings.securityClaims.NSI2_ADD_DUPLICATE));
    		self.saveBtnClicked = ko.observable(false);
    		self.confirmBtnVisible = ko.observable(false);

    		self.saveBtnVisible = ko.computed(function () {
    			return !self.confirmBtnVisible();
    		});

    		self.saveBtnDisabled = ko.observable(false);

    		//When AddButton is clicked and results found
    		self.searchResults = addStudentSearchResults;
    		self.student.ViewResults = ko.computed(function () {

    			return (self.searchResults
	                && self.searchResults.records
	                && self.searchResults.records()
	                && self.searchResults.records().length > 0);
    		});

    		self.student.noResults = ko.computed(function () {
    			return !self.student.ViewResults();
    		});
    		// Proxy used to track when there are *no* preferred names set
    		self.hasNoPreferredName = ko.observable();

    		/* Set up subscriptions to keep 'no preferred name' set correctly.
				This also sets 'no preferred name' = false, when an alt name is deleted */
    		var trackPreferredName = ko.computed(function () {
    			//// Don't act if the student isn't loaded
    			//if (!self.student.Id()) {
    			//	return true;
    			//}

    			// Check the main student preferred name
    			if (self.student.preferredName()) {
    				self.hasNoPreferredName(false);

    				return true;
    			}

    			// Check the new alt names
    			var hasPreferredNameNew = false;

    			if (self.newAltNames) {
    				hasPreferredNameNew = ko.utils.arrayFirst(self.newAltNames(), function (altName) {
    					return altName.PreferredNameIndicator();
    				});
    			}

    			if (hasPreferredNameNew) {
    				self.hasNoPreferredName(false);
    				return true;
    			}

    			// If there are no preferred names, set both the proxy, and the actual student
    			self.hasNoPreferredName(true);
    			self.student.preferredName(false);

    			return true;

    		});


    		var referenceDataRaw = {
    			nameDobVerification: referenceService.referenceDictionary.get('NameBirthDateVerificationMethod'),
    			residentialStatusVerification: referenceService.referenceDictionary.get('ResidentialStatusVerificationMethod'),
    			studentStatusReason: referenceService.referenceDictionary.get('StudentStatusReason')
    		};

    		self.referenceData = {
    			gender: referenceService.referenceDictionary.get('Gender'),
    			nameDobVerification: ko.computed(function () {
    				// Else remove birth register from the list
    				return ko.utils.arrayFilter(referenceDataRaw.nameDobVerification(), function (item) {
    					// Allow type-coersion (.Key is currently a string...)
    					// If currently set to birth register - allow it to remain in the list
    					if (item.Key == enums.nameBirthDateVerificationMethod.birthRegister) {
    						return false;
    					}

    					return true;
    				});
    			}),
    			residentialStatusVerification: ko.computed(function () {
    				// Else remove birth register from the list
    				return ko.utils.arrayFilter(referenceDataRaw.residentialStatusVerification(), function (item) {
    					// Allow type-coersion (.Key is currently a string...)
    					// If currently set to birth register - allow it to remain in the list
    					if (item.Key == enums.residentialStatusVerificationMethod.birthRegister) {
    						return false;
    					}

    					return true;
    				});
    			}),
    			residentialStatus: referenceService.referenceDictionary.get('ResidentialStatus'),
    			studentStatus: referenceService.referenceDictionary.get('StudentStatus'),
    			studentStatusReason: referenceService.referenceDictionary.get('StudentStatusReason')
    		};

    		self.student.NameBirthDateVerificationMethodFK(self.student.nameDobVerification());

    		self.newAltNames = ko.observableArray();
    		self.hasAltNames = ko.observable(false);
    		self.editNewAltNamesWidgetConfig = {
    			title: self.resources.get('AddStudentView_Alternative_names'),
    			newAltNames: self.newAltNames,
    			parentPage: enums.parentPage.addStudent,
    			initialEmptyAltName: true,
    			triggerValidationConditions: self.saveBtnClicked,
    			nameDobVerification: self.referenceData.nameDobVerification
    		};

    		self.student.nameDobVerification.val = ko.computed(function () {
    			return referenceService.getValue('NameBirthDateVerificationMethod', self.student.nameDobVerification());
    		});
    		self.student.residentialStatus.val = ko.computed(function () {
    			return referenceService.getValue('ResidentialStatus', self.student.residentialStatus());
    		});
    		self.student.residentialStatusVerificationMethod.val = ko.computed(function () {
    			return referenceService.getValue('ResidentialStatusVerificationMethod', self.student.residentialStatusVerificationMethod());
    		});
    		self.student.gender.val = ko.computed(function () {
    			return referenceService.getValue('Gender', self.student.gender());
    		});

    		self.errorMsg = ko.observable();

    		self.definiteMatchFound = ko.observable(false);

    		self.hasAddedAltnames = function () {
    			//check only fields of the default alt name widget. Hence the [0] reference. The self.newAltNames().length property is used to validate change for added or removed alt names. Hence no need to check 
    			if (self.newAltNames().length == 1) {
    				if (self.newAltNames()[0].FamilyName() &&
					   self.newAltNames()[0].FamilyName() != "" ||
					   self.newAltNames()[0].GivenName1() &&
					   self.newAltNames()[0].GivenName1() != "" ||
					   self.newAltNames()[0].GivenName2() &&
					   self.newAltNames()[0].GivenName2() != "" ||
					   self.newAltNames()[0].GivenName3() &&
					   self.newAltNames()[0].GivenName3() != "" ||
					   self.newAltNames()[0].NameBirthDateVerificationMethodFK() != enums.residentialStatusVerificationMethod.unverified ||
					   self.newAltNames()[0].PreferredNameIndicator() &&
					   self.newAltNames()[0].PreferredNameIndicator() == true) {
    					return true;
    				} else {
    					return false;
    				}
			    }
				    return false;
			    }
		    ;
    		//reordering the dropdown values- #MB


    		// ----------
    		// -- Actions

    		/* Initial save actions (pre-search, 
			*/
    		self.onSave = function () {

    			self.saveBtnClicked(true);

    			if (self.displayErrors().length > 0) {
    				return;
    			}

    			self.hasUnverified = false;
    			// Confirming unverified info 
    			if (self.student.nameDobVerification() == enums.nameBirthDateVerificationMethod.unverified ||
    				self.student.residentialStatusVerificationMethod() == enums.residentialStatusVerificationMethod.unverified) {
    				self.hasUnverified = true;
    			}

    			if (self.newAltNames &&
    				self.newAltNames().length > 0) {
    				for (var i = 0; i < self.newAltNames().length; i++) {
    					if (self.newAltNames()[i].FamilyName() && self.newAltNames()[i].FamilyName() != "") {
    						var givenNames = self.newAltNames()[i].GivenName1();

    						if (self.newAltNames()[i].GivenName2() && self.newAltNames()[i].GivenName2().trim() != "") {
    							givenNames += " " + self.newAltNames()[i].GivenName2();
    						}

    						if (self.newAltNames()[i].GivenName3() && self.newAltNames()[i].GivenName3().trim() != "") {
    							givenNames += " " + self.newAltNames()[i].GivenName3();
    						}

    						self.newAltNames()[i].GivenNames(givenNames);
    						self.newAltNames()[i].NameBirthDateVerificationMethod(referenceService.getValue('NameBirthDateVerificationMethod', self.newAltNames()[i].NameBirthDateVerificationMethodFK()));
    						self.newAltNames()[i].PreferredNameIndicatorDisplay(self.newAltNames()[i].PreferredNameIndicator() ? 'Yes' : 'No');

    						if (self.newAltNames()[i].FamilyName() &&
    							self.newAltNames()[i].FamilyName() != "" &&
    							self.newAltNames()[i].NameBirthDateVerificationMethodFK() == enums.residentialStatusVerificationMethod.unverified &&
    							!self.hasUnverified) {
    							self.hasUnverified = true;
    						}
    					}
    				}
    			}

    			if (self.hasUnverified) {
    					var confirmMessage = resourceService.get("MessageCode_Confirmation_UpdateUnverifiedStudent_927");
    					app.showMessage('', confirmMessage, ['Yes', 'No']).then(function (result) {
    						if (result === 'Yes') {
    							self.searchStudents();
    						}
    					});
    				}
    				else {
    					//search for students with the same details
    					self.searchStudents();
    				}


    			};

    		/* Cancel action
			*/
    		self.onCancel = function () {
			    if (self.confirmBtnVisible) {
			    	self.cancelled(true);//sets self.cancelled to true only when viewed from close match found add page.
			    	};
				    
    				//self.originalStudent.hasBeenJustAddedOrUpdated(false);
    				router.navigate("#/students/search");
    			};

    			self.confirmDOB = function () {
    				if (self.student.isDobUnknown()) {
    					var confirmMessage = resourceService.get("MessageCode_Error_ProxyBirthDate_028");
    					app.showMessage('', confirmMessage, ['Yes', 'No']).then(function (result) {
    						if (result === 'No') {
    							self.student.isDobUnknown(false);
    						} else {
    							self.student.dob("");
    						}
    					});
    				}
    				return true;
    			};

    		/* Confirm action - Post-search results
			*/
    			self.attemptAdd = function () {

    				if (self.userAllowedAddDuplicate) {
    					var params = {
    						FamilyName: self.student.FamilyName().trim(),
    						GivenName1: self.student.GivenName1(),
    						GivenName2: self.student.GivenName2(),
    						GivenName3: self.student.GivenName3(),
    						BirthDate: self.student.dob() ? moment(self.student.dob()).diff(moment("11/11/1918")) != 0 ? self.student.dob().trim() : "" : "",
    						GenderFK: self.student.gender(),
    						ResidentialStatusFK: self.student.residentialStatus(),
    						ResidentialStatusVerificationMethodFK: self.student.residentialStatusVerificationMethod(),
    						NameBirthDateVerificationMethodFK: self.student.nameDobVerification(),
    						PreferredNameIndicator: self.student.preferredName()
    					};

    					if (self.confirmBtnVisible()) {
    						// Only enable confirm spinner when the button is visible.
    						// Otherwise the first 'save' operation is still running. (Adding a spinner to the invisible 'Confirm' would show 2x spinners)
    						self.isRunningConfirm(true); // Enable spinner
    					}

    					studentService.addStudent(params, self.newAltNames).done(function (result) {


    						//Student has been added sucessfully
    						if (result.Messages.length > 0) {
    							self.isRunningConfirm(false); // Disable spinner, on error
    							self.isRunningSave(false); // Disable spinner, on error

    							ko.utils.arrayForEach(result.Messages, function (item) {
    								logger.logError(item.PresentationAttributes.Text, null, null, true);
    							});

    							return false;
    						}

    						student.hasBeenJustAddedOrUpdated(true);

    						var newStudentNSN = result.Data; //todo: update to get from result

						    self.canDeactivate = null; // Clear 'click to navigate away' requirement
    						router.navigate('/students/view/' + newStudentNSN);

    					}).fail(function () {
    						self.isRunningConfirm(false); // Disable spinner, on error
    						// - Leave it running for success - the page will navigate to View Student.
    					});
    				}
    			};

    		/* 2nd part of 'onSave' - runs the search-check for adding a student
			*/
    			self.searchStudents = function () {

    				//Delete empty altnames

    				var list = ko.observableArray();
    				var i;
    				if (self.newAltNames &&
						self.newAltNames().length > 0) {
    					for (i = 0; i < self.newAltNames().length; i++) {
    						if (!self.newAltNames()[i].FamilyName() || self.newAltNames()[i].FamilyName() == "") {
    							list.push(self.newAltNames()[i]);
    						}
    					}
    				}

    				for (i = 0; i < list().length; i++) {
    					self.newAltNames.remove(list()[i]);
    				}
					// end of Delete empty Altnames

    				var fullName = self.student.GivenName1();
    				var givenNames = self.student.GivenName1();
    				self.student.GivenNames(self.student.GivenName1());

    				if (self.newAltNames &&
						self.newAltNames().length > 0) {
    					self.hasAltNames(true);
    				}
    				if (self.student.GivenName2() && self.student.GivenName2().trim() != "") {
    				    fullName += " " + self.student.GivenName2();
    				    givenNames += " " + self.student.GivenName2();
    					self.student.GivenNames(self.student.GivenNames() + " " + self.student.GivenName2());
    				}

    				if (self.student.GivenName3() && self.student.GivenName3().trim() != "") {
    					fullName += " " + self.student.GivenName3();
    					givenNames += " " + self.student.GivenName3();
    					self.student.GivenNames(self.student.GivenNames() + " " + self.student.GivenName3());
    				}

    				fullName += " " + self.student.FamilyName();

    				var params = {
    				    Extended: true,
    				    Name: null,//MIST BE NULL:fullName.trim(),
    				    FamilyName: self.student.FamilyName(),
    				    GivenName1: self.student.GivenName1(),
    				    GivenName2: self.student.GivenName2(),
    				    GivenName3: self.student.GivenName3(),
    				    DOB: self.student.dob() ? self.student.dob().trim() : "", //.toUTCString() : "",
    					Gender: self.student.gender(),
    					ResidentialStatus: self.student.residentialStatus().trim()
    				};

    				self.isRunningSave(true); //Enable spinner

    				studentService.doAddStudentSearch(params).done(function (data) {

    					$.placeholder.shim(); // Refresh - Damn IE

    					//Results Found
    					if (data.Data.length) {

    						// Disable spinner. Only do this for the 'non proceeding' cases. It must remain closed for the 'attemptSave' case.
    						self.isRunningSave(false);


    						// If Highest score bigger than the Definite match
    						// This is based on the fact that results are returned sorted descending by Score

    						// Case: Definate match found

    						var definateMatch = self.currentParameters.get('IQOfficeDefiniteMatchThreshold')() == null ? 92 : self.currentParameters.get('IQOfficeDefiniteMatchThreshold')();
    						if (data.Data[0].rawMatchScore > definateMatch) {
    							self.definiteMatchFound(true);

    							//User allowed to add duplicate
    							if (self.userAllowedAddDuplicate()) {
    								self.confirmBtnVisible(true);
    								self.errorMsg(resourceService.get("MessageCode_Error_ExactMatch_022"));
    							}
    								//User is NOT allowed to add duplicate records
    							else {
    								self.saveBtnDisabled(true);
    								self.errorMsg(resourceService.get("MessageCode_Information_matchingRecordsFound_921"));
    							}
    						}
    						else {
    							self.definiteMatchFound(false);
    							self.confirmBtnVisible(true);
    							self.errorMsg(resourceService.get("MessageCode_Error_CloseMatch_021"));
    							self.userAllowedAddDuplicate(true);
							  //  self.cancelled(true);
						    }
    					} else {
							// No block actions have occurred - Follow through to add the student
    						self.attemptAdd();
    					}
    				});
    			};


    			// -------------
    			// -- Validation

    			self.requiredFields = ko.observableArray();

    			self.student.FamilyName.extend({
    				required: {
    					message: "<li>Family name</li>",
    					onlyIf: function () { return self.saveBtnClicked(); }
    				}
    			});

    			self.student.GivenName1.extend({
    				required: {
    					message: "<li>Given name (1)</li>",
    					onlyIf: function () { return self.saveBtnClicked(); }
    				}
    			});

    			//InvalidFormat_272 and InvalidNameStart_931 and InvalidHyphenApostrophe_278
    			uiLib.setCommonNameValidation(self.student.FamilyName, "Family name");
    			uiLib.setCommonNameValidation(self.student.GivenName1, "Given name (1)", { allowSingleTilde: true });
    			uiLib.setCommonNameValidation(self.student.GivenName2, "Given name (2)");
    			uiLib.setCommonNameValidation(self.student.GivenName3, "Given name (3)");

    			uiLib.setTildeRuleValidation(self.student);

    			self.student.GivenName2.extend({
    				required: {
    					message: "<li>Given name (2)</li>",
    					onlyIf: function () { return self.student.GivenName3() && self.saveBtnClicked(); }
    				}
    			});

    			//InvalidDate_305 and CannotBeLaterThatCurrentDate_013
    			uiLib.setCommonBirthDateValidation(self.student.dob, "Birth date", !self.student.isDobUnknown());

    			self.student.dob.extend({
    				required: {
    					message: "<li>Birth date</li>",
    					onlyIf: function () { return !self.student.isDobUnknown() && self.saveBtnClicked(); }
    				},
    				validNzDate: {
    					message: resourceService.get("MessageCode_Validation_Date_001").format("Birth date"),
    					onlyIf: function () { return !self.student.isDobUnknown() && self.saveBtnClicked(); }
    				},
    				dateCannotBeToday: {
    					message: resourceService.get("MessageCode_Validation_BirthdateCannotBeToday_945").format("Birth date"),
    					onlyIf: function () { return !self.student.isDobUnknown(); }
    				}
    			});

    			studentLib.attachBirthDate029Confirmation(self.student.dob);

    			self.student.nameDobVerification.extend({
    				validation:
				  {
				  	validator: function () {
				  		// Will show validation error if FALSE - #MB
				  		// FS1153
				  		if (self.student.dob() && moment(self.student.dob()).diff(moment("11/11/1918")) != 0) {
				  			return true;
				  		}
				  		return self.student.nameDobVerification() == enums.nameBirthDateVerificationMethod.unverified || moment(self.student.dob()) == moment("11/11/1918");
				  	},
				  	message: resourceService.get("MessageCode_Error_UnverifiedBlank_023")
				  }
    			});

    			self.student.residentialStatusVerificationMethod.extend({
    				validation:
				  {
				  	validator: function () {
				  		// Will show validation error if FALSE - #MB
				  		// FS1153
				  		if (self.student.residentialStatus() != enums.residentialStatus.unknown) {
				  			return true;
				  		}

				  		return self.student.residentialStatusVerificationMethod() == enums.residentialStatusVerificationMethod.unverified;
				  	},
				  	message: resourceService.get("MessageCode_Error_UnverifiedBlank_023")
				  }
    			});

    			// Had to change grouping initial configuration to do a deep grouping to be able to use it for both self.student and self.newAltName
    			self.validationGroup = ko.computed(function () {
    				return ko.validation.group([self.student, self.newAltNames], { observable: true, deep: true })();
    			});
    			self.displayErrors = ko.computed(function () {

    				var requiredFieldsCollection = "";
    				var allValidationErrors = [];

    				//validating after the Onclick 
    				if (self.saveBtnClicked()) {
						//msg 026 has been deprecated. msg 257 supercedes 026.
    					//fixing multiple "required" validation issue #MB
    				//if ((self.student.FamilyName() === ""
					//	|| self.student.GivenName1() === ""
					//	|| !self.student.FamilyName.isValid()
					//	|| !self.student.GivenName1.isValid()) 
    				//	&&
    				//	(self.student.preferredName() || self.hasNoPreferredName())
					//	) {

    				//	var msg = resourceService.get("MessageCode_Error_FamilyNameOrGivenName_026");
    				//	//Make sure there are no duplicate error messages.
    				//	if (allValidationErrors.indexOf(msg) == -1) {
    				//		allValidationErrors.push(msg);
    				//	}
    				//}

    					var hasDuplicates = studentLib.hasDuplicateNames(self.student, self.newAltNames);

    					if (hasDuplicates) {
    						var duplicatesMessage = resourceService.get("MessageCode_Error_ExactMatchCannotBeAdded_027");
    						//todo: refactor this
    						//Make sure there are no duplicate error messages.
    						if (allValidationErrors.indexOf(duplicatesMessage) == -1) {
    							allValidationErrors.push(duplicatesMessage);
    						}
    					}
    				}


    				// split the validation messages to two variables based on their type (required/others)
    				if (self.validationGroup().length > 0) {
    					$.each(self.validationGroup(), function (index, message) {
    						if (message() && message().indexOf("<li>") !== -1) {
    							requiredFieldsCollection += message();
    						}
    						else {
    							//todo: refactor this
    							//Make sure there are no duplicate error messages.
    							if (allValidationErrors.indexOf(message()) == -1) {
    								allValidationErrors.push(message());
    							}
    						}
    					});

    					if (requiredFieldsCollection) {
    						allValidationErrors.unshift(resourceService.get("MessageCode_Validation_MissingMandatoryFields_257").format("<ul>" + requiredFieldsCollection + "</ul>"));
    					}
    				}

    				return allValidationErrors.length > 0 ? allValidationErrors : ko.observableArray();
    			});

    			// ---------------------------
    			// -- Table paging - Alt names

    			var pagingText = resourceService.get('AddStudentView_AltNamesPagingInfo');

    			self.addStudentAltNamesTableConfig = {
    				dataSource: self.newAltNames,
    				rowTemplate: 'addStudentAltNamesRowTemplate',
    				columns: [
						{ bSortable: false, mDataProp: 'FamilyName' }
						, { bSortable: false, mDataProp: 'GivenNames' }
						, { bSortable: false, mDataProp: 'NameBirthDateVerificationMethod' }
						, { bSortable: false, mDataProp: 'PreferredNameIndicatorDisplay' }
    				],
    				options: {
    					// initial sorting
    					"aaSorting": [],

    					//Gridview length
    					'iDisplayLength': settings.defaultShortGridviewLength,

    					//Overriding text templates
    					"oLanguage": {
    						"sInfo": pagingText + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong>",
    						"sInfoEmpty": resourceService.get('MessageCode_Information_NoAlternativeNames_019')
    					}
    				}
    			};

    			self.addStudentAltNamesTableConfig = uiHelper.makeDatatableConfig(self.addStudentAltNamesTableConfig);

    		};


    		// -----------------
    		// -- Helper Methods

    		//parse the name and fill up the Family name and GivenNames
    		function initializeFromSearchCriteria(self) {
    			var split = searchCriteria.nameForAdd().split(" ");

    			//last value in the array is the family name
    			self.student.FamilyName(split[split.length - 1]);

    			if (split.length > 1) { //given name1 is defined?
    				self.student.GivenName1(split[0]);
    			} else {
    				self.student.GivenName1("");
    			}

    			if (split.length > 2) { //given name2 is defined?
    				self.student.GivenName2(split[1]);
    			}
    			else {
    				self.student.GivenName2("");
    			}
    			if (split.length > 3) { //more given names are defined?
    				//concatenate the rest of the given names into one
    				var givenNamesRest = split[2];
    				for (var i = 3; i < split.length - 1; i++) {
    					givenNamesRest = givenNamesRest + " " + split[i];
    				}
    				self.student.GivenName3(givenNamesRest.trim());
    			}
    			else {
    				self.student.GivenName3("");
    			}

    			self.student.dob(searchCriteria.dobForAdd());
				//if (!self.student.dob) {
				//	self.student.dob("");
				//}

    			var gender = searchCriteria.genderForAdd && searchCriteria.genderForAdd() > 0 ? searchCriteria.genderForAdd() : enums.gender.unknown;
    			var residentialStatus = searchCriteria.residentialStatusForAdd && searchCriteria.residentialStatusForAdd() > 0 ? searchCriteria.residentialStatusForAdd() : enums.residentialStatus.unknown;

    			//set defaults
    			self.student.gender(gender);
    			self.student.residentialStatus(residentialStatus);
    			self.student.nameDobVerification(enums.nameBirthDateVerificationMethod.unverified);
    			self.student.residentialStatusVerificationMethod(enums.nameBirthDateVerificationMethod.unverified);

    			self.searchResults.clear();
    		}


    		self.notValidCss = uiLib.notValidCssForArray; //takes an array of fields



    		return viewModel;


    	});