﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/resourceService',
		'services/sessionService',
		'singletons/searchResults',
		'singletons/searchCriteria',
		'singletons/mergeRequest',
		'data/settings'],
	function (router, app, logger, resourceService, sessionService, searchResults, searchCriteria, mergeRequest, settings) {
		//function (router, app, logger, resourceService, sessionService, searchResults, mergeRequest, settings) {

		var viewModel = function () {
			var self = this;
			//var isRecordCreatedBySorted = false;
			//#region viewModel events
			self.activate = function () {
				// Subscribe to 'search reset' event
				app.on("studentSearch:reset").then(function () {
					self.searchResults.clear();
					self.isRequestMergeModeEnabled(false); // Reset 'request merge' mode
				});

				app.on("studentSearch:resetResultsGrid").then(function () {
					var oTable = $('#tblSearchResults').dataTable();
					if (oTable) {
						oTable.fnSort([[4, 'desc'], [14, 'desc']]);
						$('#NSNheader').removeClass("sorting_desc");
						$('#createdByOnHeader').removeClass("sorting_asc");
						$('#createdByOnHeader').removeClass("sorting_desc");
					}
				});
			};
			//#region properties
			self.searchResults = searchResults;
			self.searchCriteria = searchCriteria;
			//self.mergeRequest = mergeRequest;

			self.isVisible = ko.computed(function () {

				return (self.searchResults
	                && self.searchResults.records
	                && self.searchResults.records()
	                && self.searchResults.records().length > 0);
			});

			// Security trimming
			self.isRequestMergeAvailable = ko.computed(function () {
				//finding out if there are unique results.
				if (self.searchResults.records
					&& self.searchResults.records()
					&& self.searchResults.records().length > 1
					// Security trimming
					&& sessionService.isAuthorised(settings.securityClaims.NSI2_MERGE_REQUEST)) {
					//Getting first NSN
					var firstNSN = self.searchResults.records()[0].NSN;
					// Only show when there are 2+ unique search results
					for (var i = 1; i < self.searchResults.records().length; i++) {
						if (firstNSN != self.searchResults.records()[i].NSN) {
							return true;
						}
					};
				}
				return false;
			});

			self.isAddStudentAvailable = ko.computed(function () {
				return (sessionService.isAuthorised(settings.securityClaims.NSI2_ADD_STUDENT)
					&& (searchResults.foundNoResults() ||
					(self.searchResults
	                && self.searchResults.records
	                && self.searchResults.records()
	                && self.searchResults.records().length > 0))
				);
			});

			// Feature - Request merge
			self.isRequestMergeModeEnabled = mergeRequest.mergeIsActivated;
			self.hasShowAllowedWarning = ko.observable(false);

			// -- Actions
			self.onEnableRequestMerge = function () {
				self.isRequestMergeModeEnabled(true);
			};

			// For resetting the entire search page
			self.onReset = function () {
				app.showMessage('', 'Clear all fields?', ['Yes', 'No']).then(function (result) {
					if (result === 'Yes') {
						app.trigger("studentSearch:reset");
						mergeRequest.mergeIsActivated(false);
						mergeRequest.showMaxAllowedRecordsError(false);
						self.searchCriteria.canShowErrors(false);
					}
				});
			};

			self.onAddStudent = function () {
				router.navigate("#/students/add");
			};

			//Handler for StudentsFound row button, to add to ToMerge list
			self.onAddToMergeList = function (studentInfo, y) {

				//If you want the warning to disapear when clicking on the first merge record
				//self.hasShowAllowedWarning(false);

				// BR - Only allow 10 records for merge
				if (mergeRequest.records().length >= 10) {
					//logger.logError('', resourceService.get('MessageCode_Information_UpTo10StudentMerge_015'), 'searchResultsGrid', true);
					//logger.log('', 'Select up to ten student records to include in a merge request', 'searchResultsGrid', true);
					mergeRequest.showMaxAllowedRecordsError(true);
					return;
				}

				//Add it to the array backing the toMerge table
				//which is mapped to this.toMerge array...so shows up on grid.

				// Adding a selectionOrder to the added students to be able to sort them in the merge datatable. #MB
				//studentInfo.selectionOrder = mergeRequest.records().length;
				mergeRequest.records.push(studentInfo);

				// Find all student results with that NSN, and remove the ability to 'pick' them
				var allStudentsWithThatNsn = ko.utils.arrayFilter(searchResults.records(), function (item) {
					return studentInfo.NSN === item.NSN;
				});

				ko.utils.arrayForEach(allStudentsWithThatNsn, function (item) {
					item.isSelectedForMerge(true);
				});


			};

			self.customSortingFlag = ko.observable(true);

			var customDateDDMMMYYYYToOrd = function (date) {
				"use strict"; //let's avoid tom-foolery in this function
				// Convert to a number YYYYMMDD which we can use to order
				var dateParts = date.split(" ");
				return (dateParts[2] * 10000) + ($.inArray(dateParts[1].toUpperCase(), ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]) * 100) + dateParts[0];
			};

			// This will help DataTables magic detect the "dd-MMM-yyyy" format; Unshift so that it's the first data type (so it takes priority over existing)
			//jQuery.fn.dataTableExt.aTypes.unshift(
			//	function (sData) {
			//		"use strict"; //let's avoid tom-foolery in this function
			//		if (sData === "Unknown") {
			//			sData = "11 NOV 1918";
			//		}
			//		if (/^([0-2]?\d|3[0-1])\s(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)\s\d{4}/i.test(sData)) {
			//			return 'date-dd-mmm-yyyy';
			//		}
			//		return null;
			//	}
			//);

			// define the sorts
			jQuery.fn.dataTableExt.oSort['date-dd-mmm-yyyy-asc'] = function (a, b) {
				"use strict"; //let's avoid tom-foolery in this function
				if (a === "Unknown") {
					a = "01 JAN 0001";
				}
				if (b === "Unknown") {
					b = "01 JAN 0001";
				}
				var ordA = customDateDDMMMYYYYToOrd(a),
					ordB = customDateDDMMMYYYYToOrd(b);
				return (ordA < ordB) ? -1 : ((ordA > ordB) ? 1 : 0);
			};

			jQuery.fn.dataTableExt.oSort['date-dd-mmm-yyyy-desc'] = function (a, b) {
				"use strict"; //let's avoid tom-foolery in this function
				if (a === "Unknown") {
					a = "01 JAN 0001";
				}
				if (b === "Unknown") {
					b = "01 JAN 0001";
				}
				var ordA = customDateDDMMMYYYYToOrd(a),
					ordB = customDateDDMMMYYYYToOrd(b);
				return (ordA < ordB) ? 1 : ((ordA > ordB) ? -1 : 0);
			};


			var searchResultsConfig = {
				resources: resourceService.resources,
				dataSource: searchResults.records,
				rowTemplate: 'studentResultRowTemplate',
				columns: [
	                { bSortable: false, mDataProp: 'MatchScore' } // Note: The plugin currently insists that mDataProp be set, even though it's not relevant
	                , { bSortable: false, mDataProp: 'MatchScore' }
	                , { bSortable: false, mDataProp: 'MatchScore' }
	                , { bSortable: false, mDataProp: 'MatchScore' }
	                , 'MatchScore'
	                , 'NSN'
	                , 'FamilyName'
	                , 'GivenNamesSummary'
	                //, { mDataProp: 'BirthDate', "asSorting": ["desc", "asc"] } // Sort in reverse order, by default
	        		, { mDataProp: 'BirthDate', bSortable: true, "sType": "date-dd-mmm-yyyy" } // Sort in reverse order, by default
	                , 'Gender'
	                , 'ResidentialStatus'
	                , { bSortable: false, mDataProp: 'CreatedByProviderName' }
	                , { bVisible: false, mDataProp: 'CreatedByProviderName' } // Hidden fields, for sorting
	                , { bVisible: false, mDataProp: 'CreatedOn' }
	                , { bVisible: false, mDataProp: 'NSN' }
				],
				options: {
					// Initial sorting - By IQ Office score, then by the (hidden) NSN field
					"aaSorting": [[4, 'desc'], [14, 'desc']],

					"fnCreatedRow": function (nRow, aData, iDataIndex) {
						// Highlight records over the 'IQ Office score' threshold
						if (aData.MatchType && aData.MatchType === 1) {
							$(nRow).attr('class', 'highlight');
						}
					},

					//Overriding text templates
					"oLanguage": {
						"sInfo": "<span>" + resourceService.get('SearchResultsGridView_SearchResultsGrid_Paging_Results') + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
						"sInfoEmpty": self.searchResultNoResults
					},
					//customising DOM
					"sDom": 'r<"paging" ip>t<"paging" ip>'
				}
			};

			// Search results table config
			self.resultsConfig = uiHelper.makeDatatableConfig(searchResultsConfig);

			self.doCreatedByCustomSort4Search = function () {
				var oTable = $('#tblSearchResults').dataTable();
				var createdByOnHeader = $('#createdByOnHeader');
				if (self.customSortingFlag) {
					oTable.fnSort([[12, 'asc'], [13, 'desc']]);
					createdByOnHeader.removeClass("sorting_desc").addClass("sorting_asc");
				} else {
					oTable.fnSort([[12, 'desc'], [13, 'asc']]);
					createdByOnHeader.removeClass("sorting_asc").addClass("sorting_desc");
				}
				self.customSortingFlag = !self.customSortingFlag;
				self.doneTheStuff;
				if (!self.doneTheStuff) {
					self.doneTheStuff = true;
					$('#tblSearchResults th:not(#createdByOnHeader)').click(function () {
						createdByOnHeader.removeClass("sorting_desc").removeClass("sorting_asc");
						self.customSortingFlag = ko.observable(true);
					});
				}
			};

			//self.sortRecordCreatedBy = function() {
			//	var oTable = $('#tblSearchResults').dataTable();
			//	oTable.fnSort([[12, 'desc']]);
			//};

			self.messages = {
				noSearchResults: "No search results found"
			};

			self.currentSearchInfoMessage = ko.computed(function () {
				if (searchResults.foundNoResults()) {
					return resourceService.get('MessageCode_General_NoSearchResultsFound_785');
				}

				if (searchResults.records().length >= 100) {
					//return resourceService.get('MessageCode_General_TopRecordsOnly_784').format('100'); // TODO: JS string replace of {0} characters //Replaced #VM
					return "More than 100 records have been found matching the search criteria. The top 100 records have been returned. Please refine your search criteria.";
				}

			});

			this.resources2 = resourceService.resources;

			// Listen for updates to the search result set
			self.searchResults.records.subscribe(function (newValue) {
				if (!newValue || newValue.length === 0) {
					return;
				}

				//configureTable(self);
			});
		};

		return viewModel;
	}


);