﻿define(['plugins/router',   
		'durandal/app',
		'services/logger',
        'services/commService',
		'services/resourceService',
		'services/referenceService',
		'services/changeNotificationService',
		'services/mergeService',
		'services/providerService',
		'singletons/session'

],
	function (router, app, logger, commService, resourceService, referenceService, changeNotificaitonService, mergeService, providerService, session) {

		//Comments regarding Code:
		//* Separate Method Definitions by at least one space.
		//* Create methods definitions using object name, not self. Reserve self statement
		//  for getting around thorny 'this' syntax conditions, not method definitinos.
		//* prefer using full class definitions, so that init methods can invoke methods
		//  without having to define them higher on the page (code spa

		var viewModel = function () {

			var self = this;


			self.activate = function () {
				var providerCode = session.user().OrgCode;

				return providerService.getProviderByCode(providerCode).done(function(provider) {
					self.lastChangeGeneratedNotificationDate(provider.LastChangeNotificationDownloadDateTime);

					return true;
				});
			};

			self.compositionComplete = function () {
				// Ensure placeholder text
				// Must happen once page is visible, to support IE
				$.placeholder.shim();
				uiHelper.focusFirstInputElement();
			};
			// -------------
			// -- Properties

			self.useCustomFromDate = ko.observable(false);

			self.customFromDate = ko.observable();

			self.customFromDate.subscribe(function() {

				// Whenever the custom date is set, select the radio button
				if (self.customFromDate()) {
					self.useCustomFromDate(true);
				}
			});

			self.lastChangeGeneratedNotificationDate = ko.observable();
			self.lastDownloadedDateDisplay = ko.computed(function() {
				var date = moment(self.lastChangeGeneratedNotificationDate());
				
				if (!date.isValid()) {
					return "";
				}
				
				return date.format("D MMM YYYY [at] h:mm a");

			});

			self.dateForSearch = ko.computed(function() {
				var fromDate = self.useCustomFromDate() ? self.customFromDate() : self.lastChangeGeneratedNotificationDate();

				return fromDate;
			});

			self.isLoadingData = ko.observable(false);
			self.isCheckingDownload = ko.observable(false);


			self.changeNotificationResults = ko.observableArray();

			self.mergeRequestDeniedResults = ko.observableArray();

			self.savedSearchDate = ko.observable();


			// -- Displaying the current time
			self.currentDateTime = ko.observable(moment());
			
			// Update the time once a minute
			setInterval(function() { self.currentDateTime(moment()); }, 60 * 1000);

			

			self.currentDateTimeDisplay = ko.computed(function() {				
				return self.currentDateTime().format("D MMM YYYY [at] h:mm a");
			});

			// -------------
			// -- Validation

			self.hasSubmitted = ko.observable(false);

			self.isCustomFromDateSelected = ko.observable(false);

			self.customFromDate.extend({
				required: {
					message: function() {
						var mandatoryFieldsMessage = "Please enter values for the following mandatory field(s)";
						var fromFieldHtml = "<ul><li>From</li></ul>";

						var msg = mandatoryFieldsMessage + fromFieldHtml;
						return msg;
					},
					onlyIf: function() {
						return self.useCustomFromDate() && self.hasSubmitted();
					}
				},
				validNzDate: {
					message: resourceService.get("MessageCode_Validation_Date_001").format("From"),
					onlyIf: function () {
						return self.useCustomFromDate();
					}
				},
				nzDate: {
				    //message: 'If is not a valid date',
				    message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("From"),
				    onlyIf: function () {
				    	return self.useCustomFromDate() && self.customFromDate();
				    }
				},
				dateRangeGreaterThanPast: {
				    params: self.lastChangeGeneratedNotificationDate,
					message: resourceService.get("MessageCode_Error_DateRangeOutOfBounds_910"),
					onlyIf: function () { return self.useCustomFromDate() && self.customFromDate(); }
				},
				dateCannotBeLaterThan: {
					params: self.lastChangeGeneratedNotificationDate,
					message: resourceService.get("MessageCode_Error_InvalidDateRange_094").format("Last change notification generated", "From"),
					onlyIf: function () { return self.useCustomFromDate(); }
				}
				
			});
			
			self.validationErrors = ko.validation.group([
    			self.customFromDate
			], { observable: true });
			
			self.allValidationErrors = ko.computed(function () {
				return self.validationErrors();
			});

			// -- Manage tabs:
			self.resultMode = ko.observable();
			
			var changeNotificationMode = 1;
			var mergeRequestDeniedMode = 2;

			self.isChangeNotificationMode = ko.computed(function() {
				return self.resultMode() === changeNotificationMode;
			});

			self.isMergeRequestDeniedMode = ko.computed(function() {
				return self.resultMode() === mergeRequestDeniedMode;
			});

			self.setChangeNotificationMode = function() {
				self.resultMode(changeNotificationMode);
			};

			self.setMergeRequestDeniedMode = function() {
				self.resultMode(mergeRequestDeniedMode);
			};


			

			// ----------
			// -- Actions
			self.onView = function() {

				self.hasSubmitted(true);

				if (self.allValidationErrors().length > 0) {
					return;
				}

			    commService.checkIsAuthenticated(function() {
			        // TODO: Validation check

			        self.isLoadingData(true);

			        // Save date, to allow subsequent paging to work off original search date
			        self.savedSearchDate(self.dateForSearch());

			        // Load table data
			        $("#ChangeNotificationTable").dataTable().fnDraw();

			        getMergeRequestDenied();
			    });
			    

			};
		    
            //NSIPRJ-1384
			self.onDownload = function() {

				self.hasSubmitted(true);

			    if (self.allValidationErrors().length > 0) {
			        return;
			    }

			    commService.checkIsAuthenticated(function() {

			        var fromDate = self.dateForSearch();

			        var hasTimeInfo = fromDate === self.lastChangeGeneratedNotificationDate();

			        var fromDateDisplay = hasTimeInfo ?
			            moment(fromDate).format("D MMM YYYY [at] h:mm a")
			            : moment(fromDate).format("D MMM YYYY");


			        // We can only show the spinner for the time that it spends checking the file. 
			        // It can't monitor period between requesting the file & getting it.
			        self.isCheckingDownload(true);

			        
			        $.fileDownload("uiapi/ChangeNotificationsFile/?FromDate=" + fromDate).done(function(data) {
			            // Placed after the window.location, in hope it may keep the loading spinner open for a more correct period of time.
			            //self.isCheckingDownload(false);

			        }).fail(function() {
			            var noChangesMessage = resourceService.get("MessageCode_Information_NoChangeNotifications_952").format(fromDateDisplay, self.currentDateTimeDisplay());
			            logger.logError(noChangesMessage, null, null, true);
			        }).always(function() {
			            self.isCheckingDownload(false);
			        });
			        

			    });
			};

			//self.isElementChanged = function(o) {
			//	return true;
			//};

			/* Returns true if a bitwise flag is present in the 'change indicator' summary value
			*/
			var hasChanges = function(changeIndicator, bitValue) {
				return (changeIndicator & bitValue) > 0;
			};
			

			var attachIsChanged = function(resultData) {
				// Create 'IsChanged' list on the , pre-calculating fields to highlight.
				ko.utils.arrayForEach(resultData, function(item) {

					item.IsChanged = {
						MasterNsn: hasChanges(item.ChangeIndicator, 1),
						FamilyName: hasChanges(item.ChangeIndicator, 2),
						GivenNames: hasChanges(item.ChangeIndicator, 28), // Combination of GivenName1,2,3 (4 + 8 + 16). A change to any field will reutrn true
						Gender: hasChanges(item.ChangeIndicator, 32),
						PreferredNameIndicator: hasChanges(item.ChangeIndicator, 64),
						BirthDate: hasChanges(item.ChangeIndicator, 128),
						DateOfDeath: hasChanges(item.ChangeIndicator, 256),
						NameDobVerification: hasChanges(item.ChangeIndicator, 512),
						ResidentialStatus: hasChanges(item.ChangeIndicator, 1024),
						ResidentialStatusVerification: hasChanges(item.ChangeIndicator, 2048),
						NzqaPaid: hasChanges(item.ChangeIndicator, 4096),
						StudentStatus: hasChanges(item.ChangeIndicator, 8192)
					};

				});
			};

			// -------------
			// -- Behaviours

			// When selecting the 'custom from date', select the text box
			self.useCustomFromDate.subscribe(function(x) {
				if (self.useCustomFromDate()) {
					self.isCustomFromDateSelected(true);
				}
			});

			self.isCustomFromDateSelected.subscribe(function(x) {
				if (!self.useCustomFromDate()) {
					self.useCustomFromDate(true);
				}
			});

			// -------------------
			// -- Table definitions
			
			// Change notifications table
			var changeNotificationTableConfig = {
				dataSource: function (sortInfo, callback) {


					var fromDate = self.savedSearchDate();

					// Don't display/do anything before the first search is run
					if (!fromDate) {

						// Convert to the format DataTables wants
						var datatableResult = {
							Data: [],
							TotalRecords: 0,
							TotalDisplayRecords: 0
						};

						callback(datatableResult);
						self.isLoadingData(false);

						return;
					}

					var criteria = {
						FromDate: fromDate
					};

					
					commService.checkIsAuthenticated(
					    function () {

								changeNotificaitonService.getChangeNotifications(criteria, sortInfo).then(function(result) {

					            // Convert to the format DataTables wants
					            var datatableResult = {
					                Data: result.Data,
					                TotalRecords: result.TotalCount,
					                TotalDisplayRecords: result.PageSize
					            };

					            // Enable the screen, if not so already
					            if (!self.resultMode()) {
					                self.resultMode(changeNotificationMode);
					            }

					            attachIsChanged(result.Data);

					            self.changeNotificationResults(result.Data);

					            // Send to datatables
					            callback(datatableResult);
					        }).always(function() {
					        	self.isLoadingData(false);

								});


					    }).fail(function() {
						var emptyResult = {
							Data: []
						};

						// Hide the results all together
						self.resultMode("");

						callback(emptyResult);
					}).always(function () {
						//self.isLoadingData(false);
					});

				},
				rowTemplate: 'ChangeNotificationRowTemplate',
				columns: [
					// This requires at least 1 entry, to ensure the headers don't default to sortable
					// (Other than that, it doesn't need anything extra)
			        { bSortable: false, mDataProp: 'Student.NSN' }
				],
				options: {
					// Initial sorting
					"aaSorting": [[0, 'asc']], // TODO: This seems to be defeated by the 'non sortable' declaration above. I need an always-on sort icon here...

					//Gridview length
					"iDisplayLength": 10,
					"iRecordsDisplay": 10,

					//Overriding text templates
					"oLanguage": {
						"sInfo": "<span>" + "Results " + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
						"sInfoFiltered": "",// " of <strong>_MAX_</strong></span>", // Split 'max' output to this
						"sInfoEmpty": resourceService.get("MessageCode_Information_NoNSIRecordChanges_949")
					}
					
				}

			};
			
			self.changeNotificationResultsConfig = uiHelper.makeDatatableConfig(changeNotificationTableConfig);

			// Merge Request denied table
			var getMergeRequestDenied = function() {

				var fromDate = self.savedSearchDate();

				// Don't display/do anything before the first search is run
				if (!fromDate) {
					return;
				}
				
				mergeService.getMergeRequestDenied(fromDate).done(function(result) {					
					self.mergeRequestDeniedResults(result);
				});
			};

			// -- Table definition
			// Search results table config
			var mergeRequestDeniedTableConfig = {
				dataSource: self.mergeRequestDeniedResults,
				rowTemplate: 'MergeRequestDeniedRowTemplate',
				columns: [
			        { bSortable: false, mDataProp: 'Nsns' }
				],
				options: {
					// Initial sorting
					"aaSorting": [[0, 'asc']],

					//Gridview length
					"iDisplayLength": 10,
					"iRecordsDisplay": 10,

					//Overriding text templates
					"oLanguage": {
						"sInfo": "<span>" + "Results " + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
						"sInfoFiltered": "",// " of <strong>_MAX_</strong></span>", // Split 'max' output to this
						"sInfoEmpty": resourceService.get("MessageCode_Information_NoDeniedRecords_950")
					}

				}

			};

			self.mergeRequestDeniedTableConfig = uiHelper.makeDatatableConfig(mergeRequestDeniedTableConfig);

		};

		return viewModel;

	});