﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
        'services/commService',
		'services/resourceService',
		'services/referenceService',
		'services/providerService',
		'services/helpTextService',
        'services/batchService',
		'data/enums',
		'entities/batchFileEntity'

],
	function (router, app, logger, commService, resourceService, referenceService, providerService, helpTextService, batchService, enums, batchFileEntity) {

		//Comments regarding Code:
		//* Separate Method Definitions by at least one space.
		//* Create methods definitions using object name, not self. Reserve self statement
		//  for getting around thorny 'this' syntax conditions, not method definitinos.
		//* prefer using full class definitions, so that init methods can invoke methods
		//  without having to define them higher on the page (code spa

		var viewModel = function() {

			var self = this;

			//#region Properties
			// -- Properties
			//var origData = ko.toJS(boundedFile);
			//ko.mapping.fromJS(origData, null, batchFileEntity);

			//self.fileType = batchFileEntity.FileType();

			//switch (self.fileType) {
			//	case "Search":
			//		self.fileType = "Search Results";
			//		break;
				
			//	default:
					
			//		break;
			//}
			//Make a local reference to global Resources dictionary cache:
			//Use this to bind to from the ViewModel using syntax similar to
			//data-bind="text: resources.get("XYZ")"
			self.resources = resourceService.resources;
			//self.parameterEntity = parameterEntity;
			//Make a local reference to global Reference dictionary cache:
			//Use this to bind to from the ViewModel using syntax similar to
			//data-bind="text: referenceData.get("XYZ")"
			self.referenceData = referenceService.referenceDictionary;
			self.requests = ko.observable();
			self.hasPageLoaded = ko.observable(false);
			self.showSuccessMessage = ko.observable(false);
			self.batchRequestsResults = ko.observableArray();
			self.batchFileEntity = batchFileEntity;
			self.noResultsMessage = 'No Files Found';
			self.successMessage = ko.observable(
				 resourceService.get('MessageCode_Confirmation_FileSuccessfullyUploaded_078')
			);

			var triggerTableUpdate = function() {
				$("#batchResultsTable").dataTable().fnDraw();
			};


			var getParams = {
				GetNonDownloadedOnly: true,
				FilterByStatuses: [
					enums.batchStatus.processed
				]

			};
			


			self.onDownload = function (boundedFile) {
				var origData = ko.toJS(boundedFile);
				ko.mapping.fromJS(origData, null, batchFileEntity);
				var batchId = batchFileEntity.Id();

				commService.checkIsAuthenticated(
				    function() {
				        $.fileDownload("interface/batch_download.asp?id=" + batchId)
				            .done(function () {
				                //$.fileDownload("interface/batch_download.asp?file=" + batchFileEntity.ResponseFileName()).done(function () {
				                self.setDownloaded(batchId);
				                triggerTableUpdate();
				            })
                            .fail(function () {
                                alert('failed');
                            });

				    });


				
				
				
			};

			self.setDownloaded = function (batchId) {
				batchService.setDownloaded(batchId);
				//setTimeout(function () { triggerTableUpdate(); }, 5000);
			};

			//TODO:Change to better code.
			var processBatchResultResults = function (result) {

				for (var i = 0; i < result.length; ++i) {

					switch (result[i].FileType) {

						case "Search":
							result[i].FileType = "Search Results";
							break;

						case "Merge":
							result[i].FileType = "Merge Results";
							break;

						case "Update/Insert":
							result[i].FileType = "Update/Insert Results";
							break;

						case "Active At Update":
							result[i].FileType = "Active At Results";
							break;

						default:

							break;
					}
				}


			};


			//#region DataTables config
			// Search results table config
			self.batchResultsTableConfig = {
				dataSource: function (sortInfo, callback) {

				    commService.checkIsAuthenticated(
				        function() {
				            batchService.getBatchRequests(sortInfo, getParams).done(function(result) {

				                // Convert to the format DataTables wants
				                var datatableResult = {
				                    Data: result.Data,
				                    TotalRecords: result.TotalCount,
				                    TotalDisplayRecords: result.PageSize
				                };

				                self.batchRequestsResults(result.Data);
				                processBatchResultResults(result.Data);

				                callback(datatableResult);

				                if (!self.hasPageLoaded()) {
				                    self.hasPageLoaded(true);
				                }
				            });
				        }
				    );
				        ;
					//   }
				},
				rowTemplate: 'BatchResultsRowTemplate',
				columns: [
					{ bSortable: false, mDataProp: 'FileType' }, // Note: The plugin currently insists that mDataProp be set, even though it's not relevant
					{ bSortable: false, mDataProp: 'ResponseFileName' },
					{ bSortable: false, mDataProp: 'ReceivedOn' },
					{ bSortable: false, mDataProp: 'UserId' },
					{ bSortable: false, mDataProp: 'Status' },
					{ bSortable: false, mDataProp: 'Downloaded' }
				],
				options: {
					// Initial sorting - ProviderName - Ascending alphabetically
					//SORTING IS DONE AT THE BACK END ASC BY PROVIDER NAME!!
					"aaSorting": [[2, 'asc']],

					//Gridview length
					"iDisplayLength": 4,
					"iRecordsDisplay": 5,

					//Overriding text templates
					"oLanguage": {
						"sInfo": "<span>" + "Displaying batch file history  " + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
						"sInfoFiltered": "", // " of <strong>_MAX_</strong></span>", // Split 'max' output to this
						"sInfoEmpty": ""
					}
				}

			};


			self.resultsConfig = ko.observable(uiHelper.makeDatatableConfig(self.batchResultsTableConfig));


			//#endregion

		};

		return viewModel;

	});