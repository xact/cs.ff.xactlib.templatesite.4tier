﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
        'services/commService',
		'services/resourceService',
		'services/referenceService',
		'services/providerService',
		'services/helpTextService',
        'services/batchService',
		'singletons/session',
        'singletons/parameters'


],
	function (router, app, logger, commService, resourceService, referenceService, providerService, helpTextService, batchService, session, parameters) {

		//Comments regarding Code:
		//* Separate Method Definitions by at least one space.
		//* Create methods definitions using object name, not self. Reserve self statement
		//  for getting around thorny 'this' syntax conditions, not method definitinos.
		//* prefer using full class definitions, so that init methods can invoke methods
		//  without having to define them higher on the page (code spa

		var viewModel = function () {

			var self = this;

			self.parameters = parameters;
		    
			self.canAccessChangeNotifications = function () {
				
				if (!currentProvider || !currentProvider()) {
					return false;
				}				
				

				return (currentProvider().ReceiveChangeNotifications === true
					|| currentProvider().ReceiveChangeNotifications === "Yes"); // Why the hell is this returning strings?
			};

			var childRouter = router.createChildRouter()
				.makeRelative({
					moduleId: 'viewmodels/batch',
					fromParent: true
				}).map([
					{ route: ['', 'download'],			moduleId: '_batchResultsDownload',	title: 'For download', nav: true }
					, { route: 'all',					moduleId: '_batchResultsAll',		title: 'All', nav: true }
					, { route: 'changeNotifications', moduleId: '_changeNotifications', title: 'Change notifications', nav: true, canViewFunc: self.canAccessChangeNotifications }
				]).buildNavigationModel();

			self.router = childRouter;

			var routeNavigationModel = childRouter.navigationModel();

			var currentProvider = ko.observable();

			self.routeTabs = ko.computed(function() {
				var provider = currentProvider(); // reference, to enable computed

				var filteredItems = ko.utils.arrayFilter(routeNavigationModel, function (item) {
					if (!item.canViewFunc) {
						return true;
					}

					return item.canViewFunc();
				});

				return filteredItems;
			});


			self.activate = function () {
				
				var providerCode = session.user().OrgCode;

				return providerService.getProviderByCode(providerCode).done(function (provider) {
					currentProvider(provider);

					return true;
				});

			};

			self.compositionComplete = function () {
				

			};

			self.canDeactivate = function () {
				return true;
			};



			//#endregion

			//#region Properties
			// -- Properties


			//Make a local reference to global Resources dictionary cache:
			//Use this to bind to from the ViewModel using syntax similar to
			//data-bind="text: resources.get("XYZ")"
			self.resources = resourceService.resources;
			//self.parameterEntity = parameterEntity;
			//Make a local reference to global Reference dictionary cache:
			//Use this to bind to from the ViewModel using syntax similar to
			//data-bind="text: referenceData.get("XYZ")"
			self.referenceData = referenceService.referenceDictionary;
			self.requests = ko.observable();
			self.hasPageLoaded = ko.observable(false);
			self.success = ko.observable(false);
			self.batchRequestsResults = ko.observableArray();

			


			//#endregion

		};

		return viewModel;

	});