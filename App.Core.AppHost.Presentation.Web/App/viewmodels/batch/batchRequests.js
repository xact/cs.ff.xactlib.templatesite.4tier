﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/commService',
		'services/resourceService',
		'services/referenceService',
		'services/providerService',
		'services/helpTextService',
        'services/batchService',
		'data/enums'
		

],
	function (router, app, logger, commService, resourceService, referenceService, providerService, helpTextService, batchService,enums) {

		//Comments regarding Code:
		//* Separate Method Definitions by at least one space.
		//* Create methods definitions using object name, not self. Reserve self statement
		//  for getting around thorny 'this' syntax conditions, not method definitinos.
		//* prefer using full class definitions, so that init methods can invoke methods
		//  without having to define them higher on the page (code spa

		var viewModel = function () {

			var self = this;
			
			self.activate = function () {
				app.on("batchRequests:reset").then(function () {
					//self.initViewModelProperties();
					self.reset();
					uiHelper.focusFirstInputElement();
				});


			};

			self.attached = function () {
				//var browserType = navigator.sayswho;
				//var a = (navigator.sayswhoParts[0] == 'MSIE' && navigator.sayswhoParts[1] < 10);
				$('#fileupload').fileupload(
					
					{

					url: 'interface/batch_upload.asp',
					//dataType: 'json',
					//To display selected file name
					replaceFileInput: false,
					//Allow single file uploads
					singleFileUploads: true,

					forceIframeTransport: true,

					

					add: function (e, data) {
						//Clear upload buffer. Otherwise it sends across all the files which were selected(clicked on) but not uploaded.
						$('#btnUpload').unbind('click');

						$('#btnUpload').on('click', function () {

						    if (self.displayErrors().length > 0 && self.filename() == "") {
						        return;
						    }

						    self.uploadData(data);

						});
					},
					done: function (e,data) {
						//if server returns no errors
						var result = $('pre', data.result).text();
						if (result.indexOf("SUCCESSFUL") > 0) {
							$('<p id = "UPL" ></p>').replaceAll('.upl');
							setTimeout(function () { $('#UPL').hide(); }, 100);
							self.showSuccessMessage(true);
							triggerTableUpdate();
							//$("input[type='file']").val("");
						} else {
							$('<p id = "UPL" ></p>').replaceAll('.upl');
							setTimeout(function () { $('#UPL').hide(); }, 100);
							//display errors from server
							//var serverMessage = data._response.jqXHR.responseText;
							//IE<10 doesnt provide a richer jqXHR object to slice.... This hack is only for failure to upload files exceeding a certain siez(2Mb). It will however trigger for even back end validation messages.
							if (!result) {

								logger.logError("Unexpected Validation Error", "", null, true);
							} else {
								result.slice(4, result.length - 4);//trim string to include only the message
								logger.logError(result, "", null, true);
							}


						}
					},
			
					fail: function (e, data) {
					



					}
				});

				


				$('#fileupload').change(function () {
					//Clear errors
					self.displayErrors.removeAll();
					
					//Get name of file. The 'file' type gives a fake path which needs to be split to get acutal file name

					self.filename = ko.observable($('input[type=file]').val().split('\\').pop());

					//Get length of characters between 4th character to '.' in the file name
					self.last = ko.observable(self.filename().indexOf('.'));
					self.operation = ko.observable(self.filename().substring(0, 3));
					self.extension = ko.observable(self.filename().split('.')[1]);
					self.providerReference = ko.observable(self.filename().substring(3, self.last()));
					

						if (self.operation() != "") {
							if (self.providerReference().length != 5) {
								self.displayErrors.push(resourceService.get('MessageCode_Error_InvalidFilePrefixAndExtension_076'));
							}

							switch (self.operation().toUpperCase()) {
								case "ATA":

									break;

								case "SEA":
									break;

								case "UPI":
									break;

								case "MER":
									break;

								case "":
									break;

								default:
									self.displayErrors.push(resourceService.get('MessageCode_Error_InvalidFilePrefix_075'));

									break;
							}

							switch (self.extension().toLowerCase()) {
								case "txt":
									break;
								case "xml":
									break;

								case undefined:
									break;

								default:
									self.displayErrors.push(resourceService.get('MessageCode_Error_InvalidFileExtension_074'));

									break;
							}

							if (self.displayErrors().length > 0) {
								$('#fileupload').addClass('error');
								}
							else {
								$('#fileupload').removeClass('error');
							}

						}
						else {
							$('#fileupload').removeClass('error');
						}
					
						self.fileName(self.filename());

					self.getFileName();

				}
				);

			};


		    self.uploadData = function(data) {
		        commService.checkIsAuthenticated(
                        function () {
                            data.submit();
                            $('#btnUpload').after('<p class ="upl">Uploading...</p>');
                        });
		    };
			
			self.compositionComplete = function () {
				// Load data to table
				// Enable show/hide button
				uiHelper.hider();
				$.placeholder.shim();
				uiHelper.focusFirstInputElement();
			};

			self.canDeactivate = function () {
				return true;
			};
			
			//#endregion

			//#region Properties
			// -- Properties
			
			self.resources = resourceService.resources;
			//self.parameterEntity = parameterEntity;
			//Make a local reference to global Reference dictionary cache:
			//Use this to bind to from the ViewModel using syntax similar to
			//data-bind="text: referenceData.get("XYZ")"
			self.referenceData = referenceService.referenceDictionary;
			self.requests = ko.observable();
			
			self.valMsg = "!";
			self.notValidField = uiHelper.notValidField;
			self.hasPageLoaded = ko.observable(false);
			self.showSuccessMessage = ko.observable(false);
			self.batchRequestsResults = ko.observableArray();
			self.fileName = ko.observable();
			self.noResultsMessage = 'No Files Found';
			self.successMessage = ko.observable(
				 resourceService.get('MessageCode_Confirmation_FileSuccessfullyUploaded_078')
			);

			//#endregion

			//#region Actions
			// -- Actions
			self.getFileName = ko.computed(function () {
				self.showSuccessMessage(false);
				return self.successMessage().format(self.fileName());
			}
				);


			var triggerTableUpdate = function () {
				var dt = $("#batchRequestsTable").dataTable();
				if (dt != null && dt.length > 0) {
					dt.fnDraw();
				}
				};

			//TODO:Change to better code.
			var processBatchRequestResults = function (result) {

				for (var i = 0; i < result.length; ++i) {

					switch (result[i].FileType) {

						case "Search":
							result[i].FileType = "Search Requests";
							break;

						case "Merge":
							result[i].FileType = "Merge Requests";
							break;

						case "Update/Insert":
							result[i].FileType = "Update/Insert Requests";
							break;
						default:

							break;
					}
				}


			};

			var params = {
				GetNonDownloadedOnly: false,
				FilterByStatuses: [
					enums.batchStatus.uploaded,
					enums.batchStatus.processed,
					enums.batchStatus.processing

				]

			};

			//#endregion
			self.reset = function() {
				triggerTableUpdate();
				controls = $('#fileupload');
				controls.replaceWith(controls = controls.clone(true));
				self.displayErrors.removeAll();
				$('#fileupload').removeClass('error');
				self.showSuccessMessage(false);
			};

			//#region Validation
			// -- Validation
			self.refreshErrors = ko.observableArray();

			// Validation - Check if the current valdiation group is 
			self.validationGroup = ko.validation.group(self);

    		self.displayErrors = ko.observableArray();
			
			//#endregion

			//#region DataTables config
			// Search results table config
			self.batchRequestsTableConfig = {
				dataSource: function (sortInfo, callback) {



				    commService.checkIsAuthenticated(
				        function() {
				            batchService.getBatchRequests(sortInfo, params)
				                .done(function(result) {
				                    // Convert to the format DataTables wants
				                    var datatableResult = {
				                        Data: result.Data,
				                        TotalRecords: result.TotalCount,
				                        TotalDisplayRecords: result.PageSize
				                    };

				                    self.batchRequestsResults(result.Data);
				                    processBatchRequestResults(result.Data);

				                    callback(datatableResult);

				                    if (!self.hasPageLoaded()) {
				                        self.hasPageLoaded(true);
				                    }
				                });
				        }
				    );
					//   }
				},
				rowTemplate: 'BatchRequestsRowTemplate',
				columns: [
					{ bSortable: false, mDataProp: 'FileType' }, // Note: The plugin currently insists that mDataProp be set, even though it's not relevant
					{ bSortable: false, mDataProp: 'RequestFileName' },
					{ bSortable: false, mDataProp: 'ReceivedOn' },
					{ bSortable: false, mDataProp: 'UserId' },
					{ bSortable: false, mDataProp: 'Status' }
				],
				options: {
					// Initial sorting - ProviderName - Ascending alphabetically
					//SORTING IS DONE AT THE BACK END ASC BY PROVIDER NAME!!
					"aaSorting": [[2, 'asc']],

					//Gridview length
					"iDisplayLength": 4,
					"iRecordsDisplay": 5,

					//Overriding text templates
					"oLanguage": {
						"sInfo": "<span>" + "Displaying batch file history  " + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
						"sInfoFiltered": "", // " of <strong>_MAX_</strong></span>", // Split 'max' output to this
						"sInfoEmpty": ""
					}
				}

			};

			
			self.resultsConfig = uiHelper.makeDatatableConfig(self.batchRequestsTableConfig);


			//#endregion

		};
		//navigator.sayswho = (function () {
		//	var ua = navigator.userAgent, tem,
		//	M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
		//	if (/trident/i.test(M[1])) {
		//		tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
		//		return 'IE ' + (tem[1] || '');
		//	}
		//	if (M[1] === 'Chrome') {
		//		tem = ua.match(/\bOPR\/(\d+)/)
		//		if (tem != null) return 'Opera ' + tem[1];
		//	}
		//	M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
		//	if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
		//	navigator.sayswhoParts = M;
		//	return M.join(' ');
		//})();

		return viewModel;

	});