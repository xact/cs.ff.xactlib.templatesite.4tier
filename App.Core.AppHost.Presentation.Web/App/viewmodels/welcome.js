﻿define(['services/logger',
		'services/resourceService',
		'services/sessionService',
		'services/helpTextService',
		'services/parameterService',
		'singletons/parameters'],
    function (logger, resourceService, sessionService, helpService, parameterService, parameters) {
        
        var vm = function () {
            var self = this;
            var session = sessionService.getSession();
            this.user = (session && session.user)
                ? session.user
                : null;

            this.resources2 = resourceService.resources;
            this.temp = helpService.getStringsForView("WelcomeView", null);
            //parameters.set('MOTD', parameterService.getParameterData('MOTD'));
            self.currentParameters = parameters;
            /* this.resourceFetch = function(key) {
                return resourceService.get(key);
            };*/
            //LabelTextMessageOfTheDayBody
            //LabelTextMessageOfTheDayTitle
            //LabelTextMyNSI
            //LabelTextNavActionChangeChallengePhase
            //LabelTextNavActionChangeInstitution
            //LabelTextNavActionChangeMyDetails
            //LabelTextNavActionChangePassword
            //LabelTextNavActionWarning1
            //LabelTextNavActionWarning2
            //ViewTitle
            this.compositionComplete = function() {
		        //For IE8/9 Make the nav menu lose focus of its current item as there
		        //is nothing else to focus on, so it can get the right colour
		        $('.current a').blur();
	        };
	        // Viewmodel events
            //this.activate = function () {
	        	
	        //};

	        this.deactivate = function() {
		        
	        };
        };

        return vm;
        

        
    });