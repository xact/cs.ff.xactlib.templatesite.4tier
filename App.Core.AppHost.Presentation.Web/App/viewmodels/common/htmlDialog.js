﻿define(['plugins/dialog', 'knockout'], function (dialog, ko) {

	var CustomModal = function (options) {

		var self = this;

		self.title = options.title;
		self.message = options.message;
		self.options = options.options;

		self.close = function () { dialog.close(this); }

		self.compositionComplete = function () {

			// Gives the option to add a handler to modal items, and have the dialog close when they are clicked
			// (Handing KO bindings in 'message' don't get KO-bound)
			if (options.closeDialogOnClickItems) {
				$(options.closeDialogOnClickItems).click(function() {
					dialog.close(self);
				});
			}
		}
	};

	CustomModal.prototype.selectOption = function (data) {
		dialog.close(this, data);
	};

	CustomModal.prototype.close = function () {
		dialog.close(this);
	};

	CustomModal.show = function (options) {
		return dialog.show(new CustomModal(options));
	};

	return CustomModal;
});