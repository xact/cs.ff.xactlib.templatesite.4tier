﻿define(['durandal/system',
		'plugins/router',
		'services/logger',
        'plugins/dialog',
		'services/resourceService',
		'services/sessionService',
		'data/routes',
        'services/helpTextService',
        ],
    function (system, router, logger, dialog, resourceService, sessionService, routes, helpTextService) {
        
        //Setting path to custom messagebox - Moved this fileset into prototype directory
        //dialog.MessageBox.setViewUrl('views/common/messageBox.html');


        helpTextService.getStringsForView("ShellView", "");
        this.resources = resourceService.resources;
        
        var shell = {
            activate: activate,
            router: router
        };

        return shell;

        //#region Internal Methods
        function activate() {
			
        	return router.activate();
        }
		
        

        function boot() {

        	// TODO: Move these to a debug-only load-file
	        
            //router.mapNav('prototype/test');
            //router.mapNav('prototype/SearchValidation');
            //router.mapNav('prototype/SearchValidationOnSubmit');
            //router.mapNav('prototype/systemErrors');
            //router.mapNav('prototype/ModalView');
            //router.mapNav('prototype/Gridview');
            //router.mapNav('prototype/viewStudent');

        }

        function log(msg, data, showToast) {
            logger.log(msg, data, system.getModuleId(shell), showToast);
        }
        //#endregion
    });