﻿define(['durandal/app',
		'services/logger',
		'services/transactionReportsService',
		'services/resourceService',
		'services/referenceService',
		'services/helpTextService',
		'services/providerService',
		'singletons/session',
		'services/sessionService',
		'services/commService',
        'data/settings',
        'data/enums'
],
    function (
	    app,
        logger,
        transactionReportsService,
		resourceService,
        referenceService,
        helpTextService,
		providerService,
        currentSession,
        sessionService,
		commService,
        settings,
        enums) {

    	var viewModel = function () {

    		var self = this;
    		//#region ViewModelEvents
		

		    self.activate = function () {
    			helpTextService.getStringsForView("TransactionActivityReportView", "");
    			self.providerToGet(self.canShowProviderSelector() ? "" : currentSession.user().OrgCode);
    			    			
    		};

    		self.attached = function () {
    			self.startDate(firstDayOfCurrentMonth.format("D MMM YYYY"));
    			self.endDate(today.format("D MMM YYYY"));
    		};

    		self.compositionComplete = function () {
    			// Load data to table

    			// Enable show/hide button
    			uiHelper.hider();
    			self.userToGet("");
    			$.placeholder.shim();
    			uiHelper.focusFirstInputElement();
 
    		};
    		//#endregion

    		//#region Properties
    		self.isRunningSearch = ko.observable(false);
    		self.mandatoryFieldsMessage = ko.observable("");
    		self.generateBtnClicked = ko.observable(false);
    		//self.resultCount = ko.observable(0);
    		self.showResult = ko.observable(true);
    		/*self.showResult = ko.computed(function () {
    			return (self.generateBtnClicked() && self.mergeTableConfig && self.resultCount() > 0);
    		}, self);*/
    		self.showResultsContainer = ko.observable(false);
    		//Make a local reference to global Resources dictionary cache:
    		//Use this to bind to from the ViewModel using syntax similar to
    		//data-bind="text: resources.get("XYZ")"
    		self.resources = resourceService.resources;
    		self.referenceData = referenceService.referenceDictionary;
    		//Make a local reference to global Reference dictionary cache:
    		//Use this to bind to from the ViewModel using syntax similar to
    		//data-bind="text: referenceData.get("XYZ")"
    		var optionsTransactionType = self.referenceData.get('TransactionType_wAlltransactions')();
		    
    		self.transactionTypeOptions = ko.computed(function () {
    			if (!sessionService.isAuthorised(settings.securityClaims.NSI2_ADMIN_REPORTS) && optionsTransactionType.length!= 4) {
    				
    				optionsTransactionType.splice(2, 2);

    				optionsTransactionType.splice(3, 2);

				   return optionsTransactionType;
    				
    			} else {

    				return optionsTransactionType;
    			}
    		});

    		// Provider check box by provider role switch
    		self.canShowProviderSelector = ko.computed(function () {
    			return sessionService.isAuthorised(settings.securityClaims.NSI2_ADMIN_REPORTS);
    		}, self);

    		self.currentProvider =
				ko.computed(function () {
					return self.canShowProviderSelector() ? "" : currentSession.user().OrgName + " (" + currentSession.user().OrgCode + ")";
				}, self);

    		self.isAllProvidersEnabled = ko.observable(true);
    		self.isAllUsersChecked = ko.observable(true);

    		self.isAllUsersEnabled = ko.computed(function () {
    			if (self.isAllProvidersEnabled() && self.canShowProviderSelector()) {
    				self.isAllUsersChecked(true);
    				return true;

    			} else {
    				//self.isAllUsersChecked(true);
    				return false;
    			}
    		});
    		self.valMsg = "!";
    		self.showNoResultsMessage = ko.observable(false);
    		self.providerToGet = ko.observable();
    		self.userToGet = ko.observable();
    		self.setFocus = ko.observable(false);
    		self.retrievedProvider = ko.observable("");
		    self.resultCount = ko.observable("");
    		self.selectedDateRange = ko.observable();
    		self.noResultsMessage = ko.computed(function () {
    			return resourceService.resources.get("MessageCode_General_NoSearchResultsFound_785")();
    		});
    		self.selectedTransactionType = ko.observable();

    		self.dateRange = ko.observableArray(["Current month", "Current week", "Custom", "Previous month", "Previous week", "Today", "Yesterday"]);


			//Calculate dates.
		    var today = moment();
			
    		//Use moment().isoWeekday to set start of week to monday (1- momday 7-sunday).
			//Moment().weekday sets Sunday as start of week by assinging it 0. This caused a nuisacnce in other date calculations.JIRA 1593 and 1582
    		//var today = moment().subtract('d', 1);
		    
    		var yesterday = today.clone().subtract('d', 1);
    		var firstDayOfWeek = today.clone().isoWeekday(1);
    		var firstDayOfLastWeek = today.clone().isoWeekday(-6);
    		var lastDayOfLastWeek = today.clone().isoWeekday(0);
    		var firstDayOfCurrentMonth = today.clone().date(1);
    		var firstDayOfLastMonth = today.clone().subtract('M', 1).date(1);
    		var lastDayOfLastMonth = today.clone().date(0);

    		self.startDate = ko.observable();
    		self.endDate = ko.observable();
    		self.DateDisable = ko.observable(false);

    		self.dateSet = ko.computed(function () {

    			switch (self.selectedDateRange()) {
    				case "Today":
    					self.DateDisable(true);
    					self.startDate(today.format("D MMM YYYY"));
    					self.endDate(today.format("D MMM YYYY"));

    					break;

    				case "Yesterday":
					    self.DateDisable(true);
					    self.startDate(yesterday.format("D MMM YYYY"));
					    self.endDate(yesterday.format("D MMM YYYY"));

    					break;

    				case "Current week":
    					self.DateDisable(true);
					
    					self.startDate(firstDayOfWeek.format("D MMM YYYY"));
    					self.endDate(today.format("D MMM YYYY"));

    					break;
    				case "Previous week":
    					self.DateDisable(true);
    					self.startDate(firstDayOfLastWeek.format("D MMM YYYY"));
    					self.endDate(lastDayOfLastWeek.format("D MMM YYYY"));

    					break;

    				case "Current month":
    					self.DateDisable(true);
    					self.startDate(firstDayOfCurrentMonth.format("D MMM YYYY"));
    					self.endDate(today.format("D MMM YYYY"));

    					break;

    				case "Previous month":
    					self.DateDisable(true);
    					self.startDate(firstDayOfLastMonth.format("D MMM YYYY"));
    					self.endDate(lastDayOfLastMonth.format("D MMM YYYY"));

    					break;

    				case "Custom":

    					self.startDate("");
    					self.endDate("");
    					self.DateDisable(false);



    				default:
    					self.DateDisable(false);


    			}
    		});

    		

    		self.reset = function () {
    			self.mandatoryFieldsMessage("");
    			self.generateBtnClicked(false);
    			self.isAllProvidersEnabled(true);
    			self.isAllUsersChecked(true);
    			self.providerToGet("");
    			self.userToGet("");
    			self.selectedDateRange("Current month");
    			self.selectedTransactionType("0");
    			self.showNoResultsMessage(false);
    			self.isRunningSearch(false);

    		};

    		//#endregion

    		//#region Actions

    		app.on("transactionActivityReport:reset").then(function () {
    			//self.initViewModelProperties();
    			self.reset();
    			uiHelper.focusFirstInputElement();
    		});

    		self.clearRetrievedProvider = function () {
    			if (self.isAllProvidersEnabled()) {
    				self.providerToGet("");
    				self.userToGet("");
    			}
    			return true;
    		};

    		self.clearRetrievedUser = function () {
    			if (self.isAllUsersChecked()) {
    				self.userToGet("");
    			}
    			return true;
    		};


		    self.doGenerate = function() {
		    	self.generateBtnClicked(true);
		    	self.showNoResultsMessage(false);


			    if (self.displayErrors().length > 0) {
				    return;
			    }


		    	//self.isRunningSearch(false); // Disable spinner. This will be disabled by completion of searches.
			    //var a = sessionService.loadSession();

			    commService.checkIsAuthenticated(function(){self.continueWithDownload();});
				
			    //self.continueWithDownload();


		    };
			
		    self.continueWithDownload = function() {

			    //self.isRunningSearch(true);
			    var transactionSearch = {
				    //FindByAllProviders: self.isAllProvidersEnabled,
				    ProviderCode: self.canShowProviderSelector() ? self.providerToGet() : currentSession.user().OrgCode,
				    //FindByAllUsers: self.isAllUsersChecked,
				    UserName: self.userToGet(),
				    StartDate: self.startDate(),
				    EndDate: self.endDate(),
				    TransactionType: self.selectedTransactionType()
			    };

			    if (!sessionService.isAuthenticated()) {
				    return;
			    }

			    if (transactionSearch.ProviderCode != "") {
			    	providerService.getProviderByCode(transactionSearch.ProviderCode)
						.done(function (data) {
					    if (data == null) {
						    logger.logError(resourceService.get("MessageCode_Error_NoProviderExists_060"), "", "providerList", true);
					    } else {
					    	$.fileDownload("uiapi/TransactionReports/?" + $.param(transactionSearch)).done(function () {
								   
								    self.showNoResultsMessage(false);
							    })
							    .fail(function (response) {
							   
								    if (response.indexOf("528") > -1) {
								    	logger.logError(resourceService.get("MessageCode_Security_SessionTimedOutNotEstablished_528"), "", null, true);
									    return;
								    }

								    self.showNoResultsMessage(true);
							    });
					    }
				    });
			    } else {
			    	$.fileDownload("uiapi/TransactionReports/?" + $.param(transactionSearch))
						.done(function () {
			    		
						    self.showNoResultsMessage(false);
					    })
					    .fail(function (response) {
					   
						    if (response.indexOf("528") > -1) {
						    	logger.logError(resourceService.get("MessageCode_Security_SessionTimedOutNotEstablished_528"), "", null, true);
							    return;
						    } 
							    self.showNoResultsMessage(true);
						    
					    });
			    }

		    };
    		//#endregion

    		//#region Validation
    		self.backendProvider = ko.observable(-25);
    		self.backendNSN = ko.observable(-25);
    		self.requiredFields = ko.observableArray();

    		self.providerToGet.extend({
    			required: {
    				message: "<li>Provider code</li>",
    				onlyIf: function () { return !self.isAllProvidersEnabled() && self.generateBtnClicked() && self.canShowProviderSelector; }
    			},
    			validProviderCode: {
    				message: resourceService.get("MessageCode_Validation_InvalidProviderCode_339"),
    				onlyIf: function () { return !self.isAllProvidersEnabled() && self.generateBtnClicked(); }
    			},
    			validation: //custom validation for back end NOT FOUND message - #MB
                {
                	validator: function () {
                		
                		// Will show validation error if FALSE - #MB
                		if (self.generateBtnClicked() && self.backendProvider() !== -25) {
                			return self.providerToGet() != self.backendProvider();
                		}
                		return true;
                	},
                	message: resourceService.get("MessageCode_Error_NoProviderExists_060")
                }
    		});


    		self.userToGet.extend({
    			required: {
    				message: "<li>User id</li>",
    				onlyIf: function () { return !self.isAllUsersEnabled() && !self.isAllUsersChecked() && self.generateBtnClicked(); }
    			}
    		});

    		self.startDate.extend({
    			required: {
    				message: "<li>Start date</li>",
    				onlyIf: function () { return !self.DateDisable() && !self.startDate() && self.generateBtnClicked(); }
    			},
    			validNzDate: {
    				message: resourceService.get("MessageCode_Validation_Date_001").format("Start date"),
    				onlyIf: function () { return !self.DateDisable() && self.startDate(); }
    			},
    			nzDate: {
    				//message: 'If is not a valid date',
    				message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Start date"),
    				onlyIf: function () { return !self.DateDisable() && self.startDate(); }
    			},
    			maxDateIsToday: {
    				//message: "It cannot be later than current date",
    				message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Start date"),
    				onlyIf: function () { return !self.DateDisable() && self.startDate() && self.generateBtnClicked(); }
    			}
    		});

    		self.endDate.extend({
    			required: {
    				message: "<li>End date</li>",
    				onlyIf: function () { return !self.DateDisable() && !self.endDate() && self.generateBtnClicked(); }
    			},
    			validNzDate: {
    				message: resourceService.get("MessageCode_Validation_Date_001").format("End date"),
    				onlyIf: function () { return !self.DateDisable() && self.endDate(); }
    			},
    			nzDate: {
    				//message: 'If is not a valid date',
    				message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("End date"),
    				onlyIf: function () { return !self.DateDisable() && self.endDate(); }
    			},
    			maxDateIsToday: {
    				//message: "It cannot be later than current date",
    				message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("End date"),
    				onlyIf: function () { return !self.DateDisable() && self.endDate() && self.generateBtnClicked(); }
    			},
    			dateRangeGreaterThan: {
    				params: self.startDate,
    				message: resourceService.get("MessageCode_Error_DateRangeOutOfBounds_910"),
    				onlyIf: function () { return !self.DateDisable() && self.endDate() && self.startDate() && self.startDate.isValid() && self.generateBtnClicked(); }
    			},
    			dateCannotEarlierThan: {
    				params: self.startDate,
    				onlyIf: function () { return !self.DateDisable() && self.endDate() && self.startDate() && self.generateBtnClicked(); },
    				message: resourceService.get("MessageCode_Error_InvalidDateRange_094").format("End date", "Start date")
    			}
    		});

    		self.validationGroup = ko.validation.group(self);

    		self.displayErrors = ko.computed(function () {

    			var requiredFieldsCollection = "";
    			var allValidationErrors = [];

    			// split the validation messages to two variables based on their type (required/others)
    			if (self.validationGroup().length > 0) {
    				$.each(self.validationGroup(), function (index, message) {
    					if (message() && message().indexOf("<li>") !== -1) {
    						requiredFieldsCollection += message();
    					}
    					else {
    						//Make sure there are no duplicate error messages.
    						if (allValidationErrors.indexOf(message()) == -1) {
    							allValidationErrors.push(message());
    						}
    					}
    				});

    				if (requiredFieldsCollection) {
    					allValidationErrors.unshift(resourceService.get("MessageCode_Validation_MissingMandatoryFields_257").format("<ul>" + requiredFieldsCollection + "</ul>"));
    				}
    			}

    			return allValidationErrors.length > 0 ? allValidationErrors : ko.observableArray();
    		});
    		//#endregion

    		//#region Helper Methods
    		//self.notValidCss = function (bountItem) {
    		//	return !bountItem.isValid() ? 'errorLabel' : 'normalColourLabel';
    		//};

    		//self.processMergeStatusResults = function (result) {
    		//	// Check for failure messages
    		//	if (!result.Success && result.Messages.length > 0) {
    		//		self.showResultsContainer(false);
    		//		result.Messages.forEach(function (message) {
    		//			if (message.MessageCode.Id === 60) {
    		//				self.backendProvider(message.Arguments[0]);
    		//			}
    		//			else if (message.MessageCode.Id === 337) {
    		//				self.backendNSN(message.Arguments[0]);
    		//			}
    		//		});
    		//		self.doesResultHasAnyErrors = true;
    		//		self.mergeStatusResults.removeAll();
    		//		//return;
    		//	} else {
    		//		// Update results
    		//		self.doesResultHasAnyErrors = false;
    		//		self.mergeStatusResults(result.Data);
    		//	}
    		//};

    		//#endregion


    	};

    	return viewModel;
    });