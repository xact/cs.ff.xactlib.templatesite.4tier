﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/mergeService',
		'services/sessionService',
		'singletons/mergeRequest',
		'services/resourceService',
		'services/referenceService',
		'services/helpTextService',
		'data/referenceData',
		'data/settings',
		'viewmodels/common/htmlDialog',
		'viewModels/merge/mergeMessageModal',
		'viewModels/merge/mergeDenialReasonModal',
		'singletons/mergeStudent'
],
    function (router, app, logger,
				mergeService, sessionService, mergeRequest,
				resourceService, referenceService, helpTextService,
				referenceData, settings,
				htmlDialog,
				mergeMessageModal, mergeDenialReasonModal,mergeStudent) {

        //Comments regarding Code:
        //* Separate Method Definitions by at least one space.
        //* Create methods definitions using object name, not self. Reserve self statement
        //  for getting around thorny 'this' syntax conditions, not method definitinos.
        //* prefer using full class definitions, so that init methods can invoke methods
        //  without having to define them higher on the page (code spa

        var viewModel = function() {

	        var self = this;
	        self.showSuccessMsg = ko.observable(false);
	        self.processingNSNs = ko.observable("");

	        self.activate = function() {
		        self.hasUnprocessedRequestsByStudentsError = ko.observable("");
		        // If them merge request has items in it already (ie. entering from the search page), fetch the corresponding merge denied pairs
		        if (mergeRequest.records().length > 0) {
			        // Check if NSNs have unprocessed requests
			        var checkHasUnprocessedRequest = function() {
				        ko.utils.arrayForEach(mergeRequest.records(), function(student) {
					        mergeService.hasUnprocessedRequestsByStudents(student.NSN).done(function(response) {
						        if (response.Messages && response.Messages.length > 0) {
							        $.each(response.Messages, function(index, message) {
								        if (message.MessageCode.Id === 930) {
									        if (self.processingNSNs().length > 1) {
										        self.processingNSNs(self.processingNSNs() + ", ");
									        }
									        self.processingNSNs(self.processingNSNs() + student.NSN);
									        mergeRequest.records.remove(student);
								        }
							        });
						        }
					        });
				        });
			        };

			        $.when(checkHasUnprocessedRequest()).done(function() {
				        self.hasUnprocessedRequestsByStudentsError = ko.computed(function() {
				        	return self.processingNSNs().length > 0 ? resourceService.get("MessageCode_Information_RequestInManualIntervention_897").format(self.processingNSNs()) : "";
				        });

				        var nsns = mergeRequest.getAllNSNs();
				        mergeService.getMergeDeniedPairs(nsns).done(function(result) {
					        addMergeDeniedPairsToList(self.mergeDeniedPairs, result);
				        });
			        });
		        }

		        self.showSuccessMsg(false);
	        };

	        self.attached = function() {
	        	$("#numNSN").keyup(function (event) {
	        		if (event.keyCode == 13) {
	        			$("#numNSN").blur();  //fix for IE9
	        			self.onAddNsn();
	        		}
	        		//if (event.keyCode == 8 ) {
	        		//    if (!self.nsnInput()) {
	        		//        self.displayErrors();
	        		//    }
	        		//}
	        	});

	        };

	        self.compositionComplete = function() {
		        self.nsnInput.isModified(false);
		        $.placeholder.shim();
		        uiHelper.focusFirstInputElement();
	        };

	        // -------------
        	// -- Properties


	        self.nsnInput = mergeStudent.mergeEntryNSN;

	        self.mergeRequest = mergeRequest;

	        self.mergeDeniedPairs = mergeRequest.mergeDeniedPairs;

	        self.sortedMergeDeniedPairs = ko.computed(function() {
		        return self.mergeDeniedPairs().sort(function(l, r) {
			        if (l.Student1 === r.Student2) {
				        return l.Student2 > r.Student2 ? 1 : -1;
			        }

			        return l.Student1 > r.Student1 ? 1 : -1;
		        });
	        });

	        self.isRunningAddNsn = ko.observable(false);
	        self.isRequestingMerge = ko.observable(false);
	        

	        self.showSuccessMsg = ko.observable(false);
	        //self.resources = resourceService;

	        self.mergeRequestWarnings = ko.observable();

	        //Make a local reference to global Reference dictionary cache:
	        //Use this to bind to from the ViewModel using syntax similar to
	        //data-bind="text: referenceData.get("XYZ")"
	        self.referenceData = referenceService.referenceDictionary;

	        self.resourceService = resourceService;

	        self.resetView = function() {
		        self.nsnInput("");
		        self.nsnInput.isModified(false); // Reset 'required field' validator
			
		        self.mergeRequest.clear();
		        self.mergeDeniedPairs([]);
		        self.mergeRequestWarnings([]);
		        self.showSuccessMsg(false);
		        self.processingNSNs("");
	        };
            
	        app.on("mergeRequest:reset").then(function () {
	            self.extraValidationError("");
	            self.extraSubmitError.removeAll();
	            self.nsnInput('');
		        self.processingNSNs("");
	        });
            
	        // -------------------
	        // -- Display trimming

	        self.canRequestMerge = ko.computed(function() {
		        return self.mergeRequest.records().length >= 2
			        && self.mergeDeniedPairs().length === 0;
	        });

	        self.canDenyMerge = ko.computed(function() {

		        var selectedStudentIDs = self.mergeRequest.allSelectedStudentIDs();

		        // Only allow when 2 records are selected
		        if (selectedStudentIDs.length !== 2) {
			        return false;
		        }

		        // Check for a pre-existing merge deny pair for those 2 StudentIDs.
		        var existingDenyPair = ko.utils.arrayFirst(self.mergeDeniedPairs(), function(item) {
			        return ((item.Student1Id === selectedStudentIDs[0]
					        && item.Student2Id === selectedStudentIDs[1])
				        || (item.Student2Id === selectedStudentIDs[0]
					        && item.Student1Id === selectedStudentIDs[1]));
		        });

		        // Allow 'deny pair' action if there isn't already a deny pair
		        return !existingDenyPair;
	        });

	        // --------------------
	        // -- Security trimming

	        self.showDenyMerge = ko.computed(function() {
		        return sessionService.isAuthorised(settings.securityClaims.NSI2_MERGE_DENY) && self.mergeRequest.records().length >= 2;
	        });

	        self.showRemoveMergeDenied = sessionService.isAuthorised(settings.securityClaims.NSI2_MERGE_DENY);
	        self.isDeleteKeyVisible = sessionService.isAuthorised(settings.securityClaims.NSI2_MERGE_REMOVE_DENIED);

	        // ----------
	        // -- Actions
	        self.onAddNsn = function() {
				// Stop multiple submit
	        	if (self.isRunningAddNsn()) {
					return false;
				}

		        self.showSuccessMsg(false);

		        // Check validation rules are met
		        if (!self.nsnInput.isValid()) {
			        return false;
		        }

		        // Manual update of 'required field' validation - due to other issues...
		        if (!self.nsnInput()) {
			        //var mandatoryFieldsMessage = "Please enter values for the following mandatory field(s)";
			        var mandatoryFieldsMessage = resourceService.get("MessageCode_Validation_MissingMandatoryFields_257").format('');
			        var NSNerrorMsg = "<ul><li>NSN</li></ul>";

			        var msg = mandatoryFieldsMessage + NSNerrorMsg;

			        self.extraValidationError(msg);
			        return false; // Stop submit - New validation error.
		        } else {
			        self.extraValidationError("");
		        }

		        var nsn = self.nsnInput();

		        // Clear textbox
		        self.nsnInput("");
		        self.nsnInput.isModified(false); // Reset 'required field' validator

		        // Check - Already added?
		        if (self.mergeRequest.containsNsn(nsn)) {
		        	self.extraValidationError(resourceService.get("MessageCode_Error_NSNAlreadyRetrieved_049").format(nsn));
		        	
			        return false;
		        }

		        self.isRunningAddNsn(true); // Is 
	            
		        if (self.isRunningAddNsn()) {
		            self.extraValidationError("");
		            self.extraSubmitError.removeAll();
		        }

		        return $.when(mergeService.addStudentToMergeRequest(nsn), mergeService.getMergeDeniedPairs(nsn)).done(function(studentSummaryResponse, mergeDeniedPairsResponse) {

			        var studentSummary = studentSummaryResponse[0];

			        if (studentSummary.Messages && studentSummary.Messages.length > 0) {
				        $.each(studentSummary.Messages, function(index, message) {
					        var displayMessage = (message.PresentationAttributes && message.PresentationAttributes.Text)
						        ? message.PresentationAttributes.Text
						        : "Cannot add student to merge: Error #" + message.Id;

					        self.extraValidationError(displayMessage);

					        //logger.logError(displayMessage, null, null, true);
				        });
				        return;
			        }

			        addMergeDeniedPairsToList(self.mergeDeniedPairs, mergeDeniedPairsResponse[0]);

		        }).always(function() {
			        self.isRunningAddNsn(false);
		        });
	        };
	        
	        //
            self.onRemoveFromMergeRequest = function (student) {
                // Remove by NSN match. The plain 'remove(student)' doesn't work...
                mergeRequest.records.remove(function (item) {
                    return item.NSN === student.NSN;
                });

                // TODO: Does the search result also need to be updated?
            };

            //
            self.onDeleteMergeDeniedPair = function (mergeDeniedPair) {
                mergeService.deleteMergeDeniedPair(mergeDeniedPair.Id).done(function () {
                    self.mergeDeniedPairs.remove(mergeDeniedPair);

	                var deniedPairDeletedMessage = self.resourceService.get("MessageCode_Confirmation_DeniedRemovedNSN_058").format(mergeDeniedPair.Student1Nsn, mergeDeniedPair.Student2Nsn);
	                logger.log(deniedPairDeletedMessage, null, "mergeEntry", true);
                });
            };

        	/* Request merge action - submission of the final group
			*/
            self.onRequestMerge = function () {
            	
            	// Stop multiple submit
            	if (self.isRequestingMerge()) {
            		return false;
            	}

                var selectedRecords = self.mergeRequest.allSelectedStudentIDs();
                self.extraValidationError("");
                
				// Pre-Validation
                self.extraSubmitError.removeAll();

                if (selectedRecords.length < 2) {
                    self.extraSubmitError.push(self.resourceService.get("MessageCode_Error_TwoNSNRecordsRequiredMerge_819"));
                    return;
                }

                if (selectedRecords.length > 10) {
                    self.extraSubmitError.push(self.resourceService.get("MessageCode_Validation_MaxTenStudentRecords_353"));
                    return;
                }

            	// Submit merge request
	            self.isRequestingMerge(true); // Turned back off in .always()
                mergeService.submitCurrentMergeRequest().done(function (result) {
                    // Show result of merge fail/pass on screen

                    var firstMessage = result.Messages.length >= 1 ? result.Messages[0] : null;

                    // Successful automatic merge - No message is sent through?
                    if (result.Success
						|| (firstMessage && firstMessage.MessageCode.Id === 55)) {

                        var message = "Merge Request Successful";
                        if (result.Messages && result.Messages.length > 0) {
                        	message = firstMessage.PresentationAttributes.Text;

	                        var successMessage = resourceService.get("MessageCode_Confirmation_AutomaticMerge_055");
	                        var masterNsn = firstMessage.Arguments.length ? firstMessage.Arguments[0] : "(Unknown)";

	                        var masterNsnLink = "<a href=\"#students/view/{0}\" id=\"mergeResultNsnLink\"\">{0}</a>".format(masterNsn);
	                        successMessage = successMessage.format(masterNsnLink);
                        	
                            //NSIPRJ-1091
                        	//message = message.replace(/(\d+)/g, "<a href=\"#students/view/$1\">$1</a>");
	                        message = successMessage;
                        }

						var htmlDialogOptions = {
							title: "Merge Request Successful",
							message: message,
							options: ["Ok"],
							closeDialogOnClickItems: "#mergeResultNsnLink"
                        }

	                    htmlDialog.show(htmlDialogOptions).done(function() {
                            // Reset screen
                            self.resetView();
                        });

	                    //app.showMessage(message, "Merge Request Successful").done(function () {
	                    //    // Reset screen
	                    //    self.resetView();
	                    //});
                    }

                        // Merge has gone to Manual Intervention
                    else if (result.Messages.length === 1
								&& (firstMessage.MessageCode.Id === 817
									|| firstMessage.MessageCode.Id === 915)) {

                    	//Defect:490 - Fixed to display combination of messages with messagecode#817 and with the expected title
                        var titleMessage = "Merge Request Successful"; // Fallback message
                        var listofmessages = ko.observableArray();
                        listofmessages = result.Messages;

                        mergeMessageModal.show(listofmessages, titleMessage).done(function () {
                            // Reset screen
                            self.resetView();
                        });
                    }

                        // Show blocking warnings, and keey merge request on the screen
                    else {
                        self.mergeRequestWarnings(result.Messages);
                    }
                }).always(function() {
	                self.isRequestingMerge(false);
                });
            };

            self.onDeny = function () {
                mergeDenialReasonModal.show().done(function (denialReasonId) {
                    // If nothing was selected, close
                    if (!denialReasonId) {
                        return false;
                    }

                    var selectedStudentIDs = mergeRequest.allSelectedStudentIDs();
                    if (selectedStudentIDs.length !== 2) {

                        logger.logError("Error: 2 NSNs must be selected", null, "mergeEntry", true);
                        return false;
                    }

                    mergeService.createMergeDeniedPair(selectedStudentIDs[0], selectedStudentIDs[1], denialReasonId);
                    //.done(function() {var successMessage = resourceService.get("MessageCode_Confirmation_StudentRecordsDenied_052");

                    // Show success message
                    //app.showMessage("", successMessage).done(function () {
                    self.resetView();
                    self.showSuccessMsg(true);
                    //});
                    //});
                });
                // Remember: Also add the merge denied pair back to this page's list
            };


			// -------------
            // -- Validation
            self.nsnInput.extend({
                isNumeric: {
                    message: self.resourceService.get("MessageCode_Validation_Numeric_003").format('NSN'),
                    onlyIf: function () { return self.nsnInput(); }
                },
                validNsn: {
                    message: self.resourceService.get("MessageCode_Validation_InvalidNSN_336"),
                    onlyIf: function () { return self.nsnInput(); }
                }
	            //,
                //required: {
                //	message: function () {
                //		var mandatoryFieldsMessage = "Please enter values for the following mandatory field(s)";
                //		var NSNerrorMsg = "<ul><li>NSN is required.</li></ul>";

                //		var msg = mandatoryFieldsMessage + NSNerrorMsg;
                //		return msg;
                //	}
                //}
                // 003 - Non numeric
                // 257 - NSN is blank
                // 336 Invalid NSN
            });

            self.validationGroup = ko.validation.group([self.nsnInput], { observable: true });

            // Extra property to handle 'required' field properly... (for now)
            self.extraValidationError = ko.observable();
            self.extraSubmitError = ko.observableArray();

            // Validation errors - combination of KO validation + custom inserts
            self.displayErrors = ko.computed(function () {
                var validationGroup = self.validationGroup().splice(0);

                if (self.extraValidationError()) {
                    if (self.nsnInput.isValid()) {
                        if (!self.nsnInput() || self.nsnInput() === "") {
                            validationGroup.pop(self.nsnInput.error());
                            validationGroup.push(self.extraValidationError);
                        } else {
                            self.extraValidationError("");
                            self.extraSubmitError.removeAll();
                            validationGroup = self.validationGroup().splice(0);
                            //return "";
                        }
                    } else {
                        validationGroup.pop(self.extraValidationError);
                        validationGroup.push(self.nsnInput.error());
                    }
                }
                
                    if (validationGroup.length > 0) {
                        self.extraSubmitError.removeAll();
                    }
                
                return validationGroup;
            });

            // Search results table config
            var mergeTableConfig = {
                dataSource: self.mergeRequest.records,
                rowTemplate: 'MergeEntryRowTemplate',
                columns: [
					{ bSortable: false, mDataProp: 'NSN' } // Must not be blank - Datatables defaults to the first column. (This needs to be left as 'order added')
					, { bSortable: false, mDataProp: 'NSN' }
					, { bSortable: false, mDataProp: 'NSN' }
					, { bSortable: false, mDataProp: 'NSN' }
					, { bSortable: false, mDataProp: 'FamilyName' }
					, { bSortable: false, mDataProp: 'GivenNamesSummary' }
					, { bSortable: false, mDataProp: 'BirthDate', "sType": "date-dd-mmm-yyyy" }
					, { bSortable: false, mDataProp: 'NameBirthDateVerificationSummary' }
					, { bSortable: false, mDataProp: 'Gender' }
					, { bSortable: false, mDataProp: 'ResidentialStatus' }
					, { bSortable: false, mDataProp: 'ResidentialStatusVerificationSummary' }
					, { bSortable: false, mDataProp: 'CreatedOn' }
                ],
                options: {
                    // Initial sorting - By NSN
                    "aaSorting": [], // Force no sorting (aka - sort by order they were added)

                    //Gridview length
                    "iDisplayLength": 10,
                    //Overriding text templates
                    "oLanguage": {
                        "sInfo": "<span>Displaying Student record(s) <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
                        "sInfoEmpty": " ",
                        "sEmptyTable": " "
                    }
                }
            };

            self.resultsConfig = uiHelper.makeDatatableConfig(mergeTableConfig);
        };

        return viewModel;

        function addMergeDeniedPairsToList(mergeDeniedPairs, mergeDeniedPairsResponse) {

            if (mergeDeniedPairsResponse) {
                //// Remove items already in the display set
                //var nonDuplicates = $.grep(mergeDeniedPairsResponse, function (item) {
                //    // Return false if the item already exists
                //    return !ko.utils.arrayFirst(mergeDeniedPairs(), function (mergeDeniedItem) {
                //        // Comparison test - based off ID
                //        var result = mergeDeniedItem.Id === item.Id;

                //        return result;
                //    });
                //});

                //// Push *contents* of the array into the array
                //mergeDeniedPairs.push.apply(mergeDeniedPairs, nonDuplicates);
                //mergeDeniedPairs.valueHasMutated();

                mergeRequest.addMergeDeniedPairs(mergeDeniedPairsResponse);

            }
        };


    });