﻿define(['plugins/dialog',
		'knockout'],
	function(dialog, ko) {
		var CustomModal = function(messageArray, title) {
			this.messages = ko.observable(messageArray);
			this.title = title;
		};

		CustomModal.prototype.ok = function() {
			dialog.close(this);
		};

		CustomModal.show = function(messageArray, title) {
			return dialog.show(new CustomModal(messageArray, title));
		};

		return CustomModal;
	});