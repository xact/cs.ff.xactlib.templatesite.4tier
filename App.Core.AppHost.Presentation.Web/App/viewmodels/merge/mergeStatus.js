﻿define(['durandal/app',
		'services/logger',
		'services/mergeService',
		'services/resourceService',
		'services/referenceService',
		'services/helpTextService',
		'singletons/session',
		'services/sessionService',
        'data/settings',
        'data/enums',
		'services/providerService',
		'singletons/provider'
		
],
    function (
	    app,
        logger,
        mergeService,
		resourceService,
        referenceService,
        helpTextService,
        currentSession,
        sessionService,
        settings,
        enums,
	    providerService,
	    provider
		
    ) {

    	var viewModel = function () {

    		var self = this;

    		// ------------------
    		// -- Lifecycle events

    		self.activate = function () {
    			helpTextService.getStringsForView("MergeStatusView", "");
    			//self.providerToGet(self.canShowProviderSelector() ? "" : currentSession.user().OrgCode);
    			
    			
    		};

    		self.compositionComplete = function () {
    			// Load data to table
    			//$("#MergeListTable").dataTable().fnDraw();

    			// Enable show/hide button
    			uiHelper.hider();
    			$.placeholder.shim();
    			uiHelper.focusFirstInputElement();

    			
    		};

    		// -------------
    		// -- Properties

    		self.isRunningSearch = ko.observable(false);
		 
    		self.mandatoryFieldsMessage = ko.observable("");
    		self.generateBtnClicked = ko.observable(false);
    		self.resultCount = ko.observable(0);
    		self.showResult = ko.observable(false);
    		/*self.showResult = ko.computed(function () {
    			return (self.generateBtnClicked() && self.mergeTableConfig && self.resultCount() > 0);
    		}, self);*/
    		self.showResultsContainer = ko.observable(false);
    		//Make a local reference to global Resources dictionary cache:
    		//Use this to bind to from the ViewModel using syntax similar to
    		//data-bind="text: resources.get("XYZ")"
    		self.resources = resourceService.resources;
		    self.hasDoneSearch = ko.observable(false);
    		//Make a local reference to global Reference dictionary cache:
    		//Use this to bind to from the ViewModel using syntax similar to
    		//data-bind="text: referenceData.get("XYZ")"
    		self.referenceData = referenceService.referenceDictionary;

    		var mergeStatusesDropDownRaw = self.referenceData.get('MergeStatus_wAll');
    		self.mergeStatusesDropDown = ko.computed(function () {
    			// Else remove birth register from the list
    			return ko.utils.arrayFilter(mergeStatusesDropDownRaw(), function (item) {
    				// Allow type-coersion (.Key is currently a string...)
    				// If currently set to birth register - allow it to remain in the list
    				if (item.Key == enums.mergeRequestStatus.requested || item.Key == enums.mergeRequestStatus.rejectedAutomatically) {
    					return false;
    				}

    				return true;
    			});
    		});

		  
    		// Merge requested by provider role switch
    		self.canShowProviderSelector = ko.computed(function () {
    			return sessionService.isAuthorised(settings.securityClaims.NSI2_MERGESTATUS_ALL_PROVIDERS);
    		}, self);

    		self.currentProvider =
				ko.computed(function () {
					return self.canShowProviderSelector() ? "" : currentSession.user().OrgName + " (" + currentSession.user().OrgCode + ")";
				}, self);

		    self.isAllProvidersEnabled =  ko.observable(false);

		    self.providerToGet =  ko.observable(null);

    		self.retrievedProvider = ko.observable("");
		    self.MergeNSN =  ko.observable();

		    self.isAllUsersEnabled = ko.observable(true);
		    self.UserId =  ko.observable();
    		self.AllUsersUnchecked = ko.computed(function () {
    			return self.isAllUsersEnabled() ? "" : self.UserId();
    		});
		    self.reqDateFrom =  ko.observable("");
		    self.reqDateTo =  ko.observable();
    		self.reqDateTo.disabled = ko.computed(function () {
    			return (self.reqDateFrom() || '').length === 0;
    		});
		    self.lastChangeDateFrom = ko.observable("");
		    self.lastChangeDateTo =  ko.observable();
    		self.lastChangeDateTo.disabled = ko.computed(function () {
    			return (self.lastChangeDateFrom() || '').length === 0;
    		});
		    self.selectedRequestStatus =  ko.observable();

    		self.statusSearch = {};
		    self.mergeStatusResults =  ko.observableArray();
    		self.doesResultHasAnyErrors = ko.observable(false);
    		self.hasPageLoaded = ko.observable(false);
    		self.shouldDisplayNoResultsMessage = ko.computed(function () {
    			return (self.mergeStatusResults().length == 0 && self.hasPageLoaded() && self.doesResultHasAnyErrors == false);
    		}, this);

    		self.initViewModelProperties = function () {
				
    			self.mandatoryFieldsMessage("");
    			self.generateBtnClicked(false);
    			self.resultCount(0);
    			self.showResultsContainer(false);
    			self.isAllProvidersEnabled(false);
    			self.providerToGet(self.canShowProviderSelector() ? "" : currentSession.user().OrgCode);
    			self.MergeNSN("");
    			self.isAllUsersEnabled(true);
    			self.reqDateFrom("");
    			self.reqDateTo("");
    			self.lastChangeDateFrom("");
    			self.lastChangeDateTo("");
    			self.selectedRequestStatus(0);
    			//self.statusSearch = {};
    			self.mergeStatusResults("");
    		};

		    self.onClear = function() {
			    self.initViewModelProperties();
		    };

    		// ----------
    		// -- Actions

    		app.on("mergeStatus:reset").then(function () {
    			self.initViewModelProperties();
    			uiHelper.blurAllInputs(); //$.placeholder.shim(); Doesnt work for getting the shim to reload after clearing
    			uiHelper.focusFirstInputElement();
    		});

    		self.clearRetrievedProvider = function () {
    			if (self.isAllProvidersEnabled()) {
    				self.providerToGet("");
					//Clears the name of the PRovider Retrieved
				    self.retrievedProvider("");
			    }
    			return true;
    		};

    		self.providerName = ko.observable("");

    		//    		self.retrievedProvider = ko.computed(function () {
    		//    			if (self.isAllProvidersEnabled() || !self.showResult() || self.providerToGet() == "") {
    		//    				return "";
    		//    			}
    		//    			return self.providerName();
    		//    		});


    		self.OnClickAllUsers = function() {
    			if (self.isAllUsersEnabled()) {
    				self.UserId("");
    			}
    			return true;
    		};
    		
    		self.doGenerate = function () {
    			providerService.getProviderByCode(self.providerToGet()).done(function (result) {
    				if (result != null) {
    					self.retrievedProvider(provider.ProviderName());
    				} else {
    					self.retrievedProvider("");
    				}
    			});

    			self.generateBtnClicked(true);
    			if (self.displayErrors().length > 0) {
    				return;
    			}

    			self.isRunningSearch(true); // Enable spinner. This will be disabled by completion of searches.

    			self.triggerTableUpdate();
    			self.showResultsContainer(true);
    		};

    		self.statusSearch = {
    			FindByAllProviders: self.isAllProvidersEnabled,
    			ProviderCode: self.canShowProviderSelector ? self.providerToGet : currentSession.user().OrgCode,
    			FindByAllUsers: self.isAllUsersEnabled,
    			UserId: self.UserId,
    			MergeNSN: self.MergeNSN,
    			MergeRequestFrom: self.reqDateFrom,
    			MergeRequestTo: self.reqDateTo,
    			StatusLastChangedFrom: self.lastChangeDateFrom,
    			StatusLastChangedTo: self.lastChangeDateTo,
    			MergeRequestStatus: self.selectedRequestStatus
    		};
			

    		// -------------
    		// -- Validation
    		self.backendProvider = ko.observable(-25);
    		self.backendNSN = ko.observable(-25);
    		self.requiredFields = ko.observableArray();

    		self.providerToGet.extend({
    			required: {
    				message: "<li>Provider code</li>",
    				onlyIf: function () { return !self.isAllProvidersEnabled() && !self.providerToGet() && self.generateBtnClicked() && self.canShowProviderSelector; }
    			},
    			validProviderCode: {
    				message: resourceService.get("MessageCode_Validation_InvalidProviderCode_339"),
    				onlyIf: function () { return !self.isAllProvidersEnabled() && self.generateBtnClicked(); }
    			},
    			validation: //custom validation for back end NOT FOUND message - #MB
                {
                	validator: function () {
                		// Will show validation error if FALSE - #MB
                		if (self.generateBtnClicked() && self.backendProvider() !== -25) {
                			return self.providerToGet() != self.backendProvider();
                		}
                		return true;
                	},
                	message: resourceService.get("MessageCode_Error_NoProviderExists_060")
                }
    		});


    		self.UserId.extend({
    			required: {
    				message: "<li>User id</li>",
    				onlyIf: function () { return !self.isAllUsersEnabled() && self.generateBtnClicked(); }
    			}
    		});

    		self.reqDateFrom.extend({
    			required: {
    				message: resourceService.get("MessageCode_Error_NoDateRangesProvided_911"),
    				onlyIf: function () { return !self.MergeNSN() && !self.lastChangeDateFrom() && self.generateBtnClicked(); }
    			},
    			validNzDate: {
    				message: resourceService.get("MessageCode_Validation_Date_001").format("Merges requested from"),
    				onlyIf: function () { return self.reqDateFrom() && self.generateBtnClicked(); }
    			},
    			nzDate: {
    				//message: 'If is not a valid date',
    				message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Merge requested from"),
    				onlyIf: function () { return self.reqDateFrom() && self.generateBtnClicked(); }
    			},
    			maxDateIsToday: {
    				//message: "It cannot be later than current date",
    				message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Merge requested from"),
    				onlyIf: function () { return self.reqDateFrom() && self.generateBtnClicked(); }
    			}
    		});

    		self.reqDateTo.extend({
    			required: {
    				message: resourceService.get("MessageCode_Error_NoDateRangesProvided_911"),
    				onlyIf: function () { return !self.MergeNSN() && !self.lastChangeDateTo() && self.reqDateFrom().length > 0 && self.generateBtnClicked(); }
    			},
    			validNzDate: {
    				message: resourceService.get("MessageCode_Validation_Date_001").format("Merges requested to"),
    				onlyIf: function () { return self.reqDateTo() && self.generateBtnClicked(); }
    			},
    			nzDate: {
    				//message: 'If is not a valid date',
    				message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Merge requested to"),
    				onlyIf: function () { return self.reqDateTo() && self.generateBtnClicked(); }
    			},
    			maxDateIsToday: {
    				//message: "It cannot be later than current date",
    				message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Merge requested to"),
    				onlyIf: function () { return self.reqDateTo() && self.generateBtnClicked(); }
    			},
    			dateRangeGreaterThan: {
    				params: self.reqDateFrom,
    				message: resourceService.get("MessageCode_Error_DateRangeOutOfBounds_910"),
    				onlyIf: function () { return self.reqDateTo() && self.reqDateFrom() && self.reqDateFrom.isValid() && self.generateBtnClicked(); }
    			},
    			dateCannotEarlierThan: {
    				params: self.reqDateFrom,
    				onlyIf: function () { return self.reqDateTo() && self.reqDateFrom() && self.generateBtnClicked(); },
    				message: resourceService.get("MessageCode_Error_InvalidDateRange_094").format("Merges requested to", "Merges requested from")
    			}
    		});

    		self.lastChangeDateFrom.extend({
    			required: {
    				message: resourceService.get("MessageCode_Error_NoDateRangesProvided_911"),
    				onlyIf: function () { return !self.MergeNSN() && !self.reqDateFrom() && self.generateBtnClicked(); }
    			},
    			validNzDate: {
    				message: resourceService.get("MessageCode_Validation_Date_001").format("Date status last changed from"),
    				onlyIf: function () { return self.lastChangeDateFrom() && self.generateBtnClicked(); }
    			},
    			nzDate: {
    				//message: 'If is not a valid date',
    				message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Date status last changed from"),
    				onlyIf: function () { return self.lastChangeDateFrom() && self.generateBtnClicked(); }
    			},
    			maxDateIsToday: {
    				//message: "It cannot be later than current date",
    				message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Date status last changed from"),
    				onlyIf: function () { return self.lastChangeDateFrom() && self.generateBtnClicked(); }
    			},
    			dateCannotEarlierThan: {
    				params: self.reqDateFrom,
    				onlyIf: function () { return self.reqDateFrom() && self.lastChangeDateFrom() && self.generateBtnClicked(); },
    				message: resourceService.get("MessageCode_Error_InvalidDateStatusLastChangedFrom_912").format("'Merges requested from'", "'Date status last changed from'")
    			}
    		});

    		self.lastChangeDateTo.extend({
    			required: {
    				message: resourceService.get("MessageCode_Error_NoDateRangesProvided_911"),
    				onlyIf: function () { return !self.MergeNSN() && !self.reqDateTo() && self.lastChangeDateFrom().length > 0 && self.generateBtnClicked(); }
    			},
    			validNzDate: {
    				message: resourceService.get("MessageCode_Validation_Date_001").format("Date status last changed to"),
    				onlyIf: function () { return self.lastChangeDateTo() && self.generateBtnClicked(); }
    			},
    			nzDate: {
    				//message: 'If is not a valid date',
    				message: resourceService.get("MessageCode_Validation_InvalidDate_305").format("Date status last changed to"),
    				onlyIf: function () { return self.lastChangeDateTo() && self.generateBtnClicked(); }
    			},
    			maxDateIsToday: {
    				//message: "It cannot be later than current date",
    				message: resourceService.get("MessageCode_Validation_CannotBeFutureDate_951").format("Date status last changed to"),
    				onlyIf: function () { return self.lastChangeDateTo() && self.generateBtnClicked(); }
    			},
    			dateRangeGreaterThan: {
    				params: self.lastChangeDateFrom,
    				message: resourceService.get("MessageCode_Error_DateRangeOutOfBounds_910"),
    				onlyIf: function () { return self.lastChangeDateTo() && self.lastChangeDateFrom() && self.lastChangeDateFrom.isValid() && self.generateBtnClicked(); }
    			},
    			dateCannotEarlierThan: {
    				params: self.lastChangeDateFrom,
    				onlyIf: function () { return self.lastChangeDateTo() && self.lastChangeDateFrom() && self.generateBtnClicked(); },
    				message: resourceService.get("MessageCode_Error_InvalidDateRange_094").format("Date status last changed to", "Date status last changed from")
    			}
    		});

    		self.MergeNSN.extend({
    			isNumeric: {
    				message: resourceService.get("MessageCode_Validation_Numeric_003").format("Merge requests including NSN"),
    				onlyIf: function () { return self.MergeNSN() && self.generateBtnClicked(); }
    			},

    			validNsn: {
    				//message: 'If is not a valid NSN',
    				message: resourceService.get("MessageCode_Validation_InvalidNSN_336"),
    				onlyIf: function () { return self.MergeNSN() && self.generateBtnClicked(); }
    			},
    			validation: //custom validation for back end NOT FOUND message - #MB
               {
               	validator: function () {
               		// Will show validation error if FALSE - #MB
               		if (self.generateBtnClicked() && self.backendNSN() !== -25) {
               			return self.MergeNSN() != self.backendNSN();
               		}
               		return true;
               	},
               	message: "NSN_ERROR" //DONT CHANGE - #MB
               }
    		});

    		self.validationGroup = ko.validation.group(self);

    		self.displayErrors = ko.computed(function () {

    			var requiredFieldsCollection = "";
    			var allValidationErrors = [];

    			// split the validation messages to two variables based on their type (required/others)
    			if (self.validationGroup().length > 0) {
    				$.each(self.validationGroup(), function (index, message) {
    					if (message() && message().indexOf("<li>") !== -1) {
    						requiredFieldsCollection += message();
    					} else if (message() && message().indexOf("NSN_ERROR") !== -1) {
    						allValidationErrors.push(resourceService.get("MessageCode_Validation_NSIRecordDoesNotExist_337").format(self.MergeNSN()));
    					} else {
    						//Make sure there are no duplicate error messages.
    						if (allValidationErrors.indexOf(message()) == -1) {
    							allValidationErrors.push(message());
    						}
    					}
    				});

    				if (requiredFieldsCollection) {
    					allValidationErrors.unshift(resourceService.get("MessageCode_Validation_MissingMandatoryFields_257").format("<ul>" + requiredFieldsCollection + "</ul>"));
    				}
    			}

    			return allValidationErrors.length > 0 ? allValidationErrors : ko.observableArray();
    		});


    		// -----------------
    		// -- Helper Methods
    		self.notValidCss = function (bountItem) {
    			return !bountItem.isValid() ? 'errorLabel' : 'normalColourLabel';
    		};
    		
    		self.triggerTableUpdate = function () {
    			var dt = $("#MergeStatusTable").dataTable();
    			if (dt != null && dt.length > 0) {
    				dt.fnDraw();
    			}
    		};

    		

    		self.processMergeStatusResults = function (result) {
    			// Check for failure messages
    			if (!result.Success && result.Messages.length > 0) {
    				self.showResultsContainer(false);
    				result.Messages.forEach(function (message) {
    					if (message.MessageCode.Id === 60) {
    						self.backendProvider(message.Arguments[0]);
    					}
    					else if (message.MessageCode.Id === 337) {
    						self.backendNSN(message.Arguments[0]);
    					}
    				});
    				self.doesResultHasAnyErrors = true;
    				self.mergeStatusResults.removeAll();
    				//return;
    			} else {
    				// Update results
    				self.doesResultHasAnyErrors = false;
    				self.mergeStatusResults(result.Data);
    			}
    		};
		
    		// --------------------------
    		// -- DataTables table config

    		// Search results table config
    		self.mergeTableConfig = {
    			dataSource: function (sortInfo, callback) {
    				// if (self.generateBtnClicked) {
    				mergeService.searchMergeStatuses(self.statusSearch, sortInfo).done(function (result) {
						    self.hasDoneSearch(true);
						    // Convert to the format DataTables wants
						    var datatableResult = {
							    Data: result.Data,
							    TotalRecords: result.TotalCount,
							    TotalDisplayRecords: result.PageSize
						    };

						    self.resultCount(result.TotalCount ? result.TotalCount : 0);
						    if (self.resultCount() > 0) {
							    self.showResult(true);
						    } else {
							    self.showResult(false);
						    }


						    // Also sent to 
						    self.processMergeStatusResults(result);

						    callback(datatableResult);

						    if (!self.hasPageLoaded()) {
							    self.hasPageLoaded(true);
						    }
					    }).always(function() {

						    // Always disable the spinner
						    self.isRunningSearch(false);
					    });
							
						},
    			rowTemplate: 'MergeStatusRowTemplate',
    			columns: [
			        { bSortable: false, mDataProp: 'ProviderCode' }
			        , { bSortable: false, mDataProp: 'CreatedByUser' }
			        , { bSortable: false, mDataProp: 'MergeStatusFK' }
			        , { bSortable: false, mDataProp: 'MergeStatusFK' }
			        , { bSortable: false, mDataProp: 'CreatedOn' }
    			],
    			options: {
    				// Initial sorting
    				"aaSorting": [],

    				//Gridview length
    				"iDisplayLength": 10,
    				"iRecordsDisplay": 5,

    				//Overriding text templates
    				"oLanguage": {
    					"sInfo": "<span>" + "Displaying Merge Request(s) " + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
    					"sInfoFiltered": "",// " of <strong>_MAX_</strong></span>", // Split 'max' output to this
    					"sInfoEmpty": "",
    					"sEmptyTable": self.noResult
    				}
    			}
    		};

		    self.showNoResultsMessage = ko.computed(function() {
			    if (self.hasDoneSearch())
				    self.noResult = ko.observable(resourceService.get("MessageCode_General_NoSearchResultsFound_785"));
		    });

		    

    		self.resultsConfig = uiHelper.makeDatatableConfig(self.mergeTableConfig);


    	};

    	return new viewModel();
    });