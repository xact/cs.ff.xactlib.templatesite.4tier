define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/resourceService',
		'services/studentService',
		'services/sessionService',
		'services/studentProviderRelationshipService',
		'singletons/studentMergeHistory',
		'viewModels/students/studentProviderRelationshipsModal',
		'data/settings'],
    function (router, app, logger, resourceService, studentService, sessionService, sprService,
				studentMergeHistory, sprModal, settings) {

    	var viewModel = function () {
    		var self = this;

    		self.updateFieldFormat = false;

    		self.settings = com.nsi.frontend.data.settings;

    		self.resources2 = resourceService.resources;

    		self.sprCounts = ko.observableArray();

    		self.studentMergeHistory = studentMergeHistory.records;

    		// -------------------
    		// -- viewModel events
    		self.activate = function (mergeRequestId) {
    			return studentService.getStudentMergeHistory(mergeRequestId).done(function (data) {

    				// Add unique datatable config for each student history
    				$.each(data, function (index, item) {
    					item.datatableConfig = getTemplateDatatableConfig(item.StudentUpdateHistories);
    					
						// Link to a property that figures out the SPR count
    					item.sprCount = getSprCount(item.Id);
    					
    				});

    				studentMergeHistory.update(data);

    				return true;
    				
    			}).done(loadSprCounts);

    		};

    		

    		self.compositionComplete = function () {

    			uiHelper.hider();
    		};

    		self.navigateToSlaveDetails = function (nsn) {
    			router.navigate('/students/view/' + nsn);
    		};

    		// -----
    		// -- Actions
    		self.onShowSprs = function (studentHistory) {

    			sprModal.show({
    				studentId: studentHistory.Id,
    				studentNsn: studentHistory.Nsn,
    				studentName: studentHistory.GivenName + " " + studentHistory.FamilyName //self.currentStudent.GivenName1() + " " + self.currentStudent.FamilyName()
    			}).done(loadSprCounts); // Reload SPR counts when finished
    		};


    		// --------------------
    		// -- Security trimming
    		self.shouldShowRelationships = sessionService.isAuthorised(settings.securityClaims.NSI2_VIEW_SPR);

    		
    		// -------------------
    		// -- Internal methods
    		
    		var getTemplateDatatableConfig = function (dataSource) {

    			var config = {
    				dataSource: dataSource
    			};
    			return $.extend(true, config, templateDatatableConfig);
    		};
    		
    		var mergeHistoryDatatableSettings = {
    			// Datasource will be set dyanmically - We have multiple repeating tables on this page

    			rowTemplate: 'RecordUpdateHistoryRowTemplate',
    			columns: [
					{ mDataProp: 'ModifiedDateTime', sDefaultContent: "" },
					{ mDataProp: 'ModifiedByProviderName', sDefaultContent: "" },
					{ mDataProp: 'UpdateType', sDefaultContent: "" },
					{ mDataProp: 'FieldUpdated', sDefaultContent: "" },
					{ bSortable: false, mDataProp: 'OldValue', sDefaultContent: "" },
					{ bSortable: false, mDataProp: 'NewValue', sDefaultContent: "" }
    			],
    			options: {
    				//Gridview length
    				'iDisplayLength': settings.defaultHistoryGridviewLength,
    				"aaSorting": [[0, 'desc']],
    				//Overriding text templates
    				"oLanguage": {
    					"sInfo": "",//self.resources2.get('MergeHistoryView_MergeHistory_RecordUpdateHistoryPagingInfo')() + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong>",
    					"sEmptyTable": "No Records to display", //self.resources2.get('MessageCode_Information_NoAlternativeNames_019')(),
    					"sInfoEmpty": "" //resourceService.get('StudentDetailsView_ViewStudent_GridAltInfo')
    				}
    			}
    		};

    		var templateDatatableConfig = uiHelper.makeDatatableConfig(mergeHistoryDatatableSettings);
    		
    		/* Loads the SPR counts for all students on this merge history page.
			*/
    		var loadSprCounts = function () {
    			// Load SPR counts

    			var studentIds = ko.utils.arrayMap(self.studentMergeHistory(), function (item) {
    				return item.Id;
    			});

    			return sprService.getSprCounts(studentIds).done(function (sprCounts) {
    				self.sprCounts(sprCounts);
    			});
    		};

    		/* Returns a computed field, which will display the SPR count for the given SPR
			*/
    		var getSprCount = function (studentId) {
    			return ko.computed(function () {
    				var spr = ko.utils.arrayFirst(self.sprCounts(), function (item) {
    					return studentId === item.StudentId;
    				});

    				return spr ? spr.SprCount : "...";
    			});
    		};
    	};

    	return viewModel;
    });