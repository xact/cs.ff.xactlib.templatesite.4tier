﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/mergeService',
		'services/resourceService',
		'services/referenceService',
		'services/helpTextService',
		'singletons/mergeDecision',
		'data/referenceData',
		'viewModels/merge/mergeDenialReasonModal',
		'viewModels/merge/mergeDecisionConfirmationModal',
        ],
    function (router, app, logger,
		mergeService,
		resourceService, referenceService, helpTextService,
	    mergeDecisionItem, referenceData,
		mergeDenialReasonModal, mergeDecisionConfirmationModal) {

        //Comments regarding Code:
        //* Separate Method Definitions by at least one space.
        //* Create methods definitions using object name, not self. Reserve self statement
        //  for getting around thorny 'this' syntax conditions, not method definitinos.
        //* prefer using full class definitions, so that init methods can invoke methods
        //  without having to define them higher on the page (code spa

        var viewModel = function () {

    		var self = this;

			// -----------------
        	// -- Page lifecycle
	        
    		self.activate = function (mergerequestId) {

//    			if (mergerequestId === mergeDecisionItem.mergeRequestId) {
//    				enableLockedRows(self.isNameAndDobLocked());
//    				self.hasPageLoaded(true);
//
//					return;
//				}

		        mergeService.loadMergeDecisionPage(mergerequestId)
			        .done(function(data) {
						
			        	// I'm using the 'data' variable direct, rather than mergeDecisionItem, as
			        	// stacking the deferred callbacks has proven to occasionally get this part out
				        // of sync (enabling the feature below when it shouldn't)
						//self.mergeDetails = ko.computed(function() { return mergeDecisionItem.mergeDetails; });
				        if (data) {
			        	enableLockedRows(data.ManualInterventionDetails.IsNameAndDobLocked);
				        }

				        // Finally, let the page display
			        	self.hasPageLoaded(true);
				        
			        });
	        };

			// Save behaviour subscription for later disposal
    		var selectFamilyNameSub;
	        
    		var enableLockedRows = function (isNameAndDobLocked) {
    			if (isNameAndDobLocked) {

    				// Change linked column choices, when the 3x are 'locked' to operate as one.
    				// (+ Store subscription for later disposal)
    				selectFamilyNameSub = self.selections.familyName.subscribe(function (newValue) {

    					// Sometimes this triggers really early in the page load with newValue === 'undefined'.
    					// So it messes up the values, and loses highlighting on the screen.
    					if (!newValue) {
    						return;
    					}

						// Set other rows to the same selection
				        self.selections.givenName1(newValue);
				        self.selections.givenName2(newValue);
				        self.selections.givenName3(newValue);
				        self.selections.birthDate(newValue);
    				});
    				

		        }
    		};

    		self.canDeactivate = function () {

    			var navigateAwayMessage = resourceService.get("MessageCode_Confirmation_NavigateAway_007");

    			return app.showMessage("", navigateAwayMessage, ['Yes', 'No']).done(function (result) {
    				if (result === "No") { // 'Stay on page' option
    					return false;
    				}
    				return true;
    			});
    		};
	        

	        self.deactivate = function() {

	        	// Ensure to remove the 'family selection grouping' when the page closes, so subsequent 
		        // subsequent pages don't get the functionality from the singleton
		        if (selectFamilyNameSub) {
			        selectFamilyNameSub.dispose();
		        }
		        

	        };

	        self.compositionComplete = function () {
				// Prod selections, to ensure UI is up to date
	        	self.mergeDecisionItem.mergeDeniedPairs.notifySubscribers();

	        	//self.selections.familyName.notifySubscribers();
		        
	        	self.selections.masterStudentId.notifySubscribers();
	        	self.selections.familyName.notifySubscribers();
	        	self.selections.givenName1.notifySubscribers();
	        	self.selections.givenName2.notifySubscribers();
	        	self.selections.givenName3.notifySubscribers();
	        	self.selections.birthDate.notifySubscribers();
	        	self.selections.gender.notifySubscribers();
	        	self.selections.residentialStatus.notifySubscribers();
	        };

			// --------------
			// -- Properties

	        self.mergeDecisionItem = mergeDecisionItem; // Original singleton item
	        self.resources = resourceService;

	        // Relay properties for easier access
	        self.altNames = mergeDecisionItem.altNames;
			self.mergeDeniedPairs = mergeDecisionItem.mergeDeniedPairs;
			self.mergeRequest = mergeDecisionItem.mergeRequest;
			self.mergeDetails = mergeDecisionItem.mergeDetails;
			self.manualInterventionMessages = mergeDecisionItem.manualInterventionMessages;
			self.selections = mergeDecisionItem.selections;
	        
			self.allowIndividualNameAndDob = ko.observable(true);

	        self.hasPageLoaded = ko.observable(false);

	        self.isNameAndDobLocked = mergeDecisionItem.isNameAndDobLocked;

	        self.isRunningMerge = ko.observable(false);
	        self.isRunningDeny = ko.observable(false);
	        self.isRunningDiscard = ko.observable(false);

			// Values which will show which if a given name exists.
	        self.givenName1Found = ko.numericObservable(0);
	        self.givenName2Found = ko.numericObservable(0);
	        self.givenName3Found = ko.numericObservable(0);

        	//Calualting the number of columns the given name header needs 
	        self.numberOfCol = function () {

	        	//The merge details array will loop over the last previous data for some reason 
				//so will need to reset the values 
	        	var mergegivenName1Found = 0;
	        	var mergegivenName2Found = 0;
	        	var mergegivenName3Found = 0;

		        ko.utils.arrayFirst(self.mergeDetails(), function(mergeDetail) {
		        		if (mergeDetail.GivenName1) {
		        			mergegivenName1Found = 1;
				        }
				        if (mergeDetail.GivenName2) {
				        	mergegivenName2Found = 1;
				        }
				        if (mergeDetail.GivenName3) {
				        	mergegivenName3Found = 1;
				        }
		        });

				//used for displaying/hiding the radio button/table data
		        self.givenName1Found = function () { return mergegivenName1Found; };
		        self.givenName2Found = function () { return mergegivenName2Found; };
		        self.givenName3Found = function () { return mergegivenName3Found; };

				//Sum total for the colspan attrbute in the <th> of given name
		        return mergegivenName1Found + mergegivenName2Found + mergegivenName3Found;
	       
	        };

			// -----------
        	// -- Actions

        	/* Action - Submit current merge request + selections
			*/
	        self.onMerge = function () {
	        	var selectedStudentIds = self.mergeDecisionItem.getSelectedStudentIds();

	        	// TODO: Due to WebAPI deserialisation issues, this is a an array. If I can solve this, I'd prefer a dictionary (to show field-type correlation
	        	var userSelections = [
				        self.selections.masterStudentId(),		// 0
				        self.selections.familyName(),		// 1
				        self.isNameAndDobLocked() ? self.selections.familyName() : self.selections.givenName1(),		// 2
				        self.isNameAndDobLocked() ? self.selections.familyName() : self.selections.givenName2(),		// 3
				        self.isNameAndDobLocked() ? self.selections.familyName() : self.selections.givenName3(),		// 4
				        self.isNameAndDobLocked() ? self.selections.familyName() : self.selections.birthDate(),		// 5
				        self.selections.gender(),			// 6
				        self.selections.residentialStatus()	// 7
	        	];
		        
	        	// Check - student details selected for non-included student #064
		        var hasDetailsSelectedForNonIncludedStudent = false;
	        	for (var i = 0; i < userSelections.length; i++) {
	        		if (!ko.utils.arrayFirst(selectedStudentIds, function (item) {
	        			// Knockout switches the int values to string - make sure to compare correctly
	        			return item.toString() === userSelections[i].toString();
	        		})) {
	        			hasDetailsSelectedForNonIncludedStudent = true;
	        			break;
	        		}
	        	}
		        
				if (hasDetailsSelectedForNonIncludedStudent) {
					var hasDetailsSelectedForNonIncludedStudentMessage = resourceService.get("MessageCode_Error_IncludeInMerge_064");

					logger.logError(hasDetailsSelectedForNonIncludedStudentMessage, null, null, true);
					return;
				}

		        
	        	// Check - Less than 2 students selected - #819
				if (selectedStudentIds.length < 2) {
		        	var notEnoughSelectedMessage = resourceService.get("MessageCode_Error_TwoNSNRecordsRequiredMerge_819");

		        	logger.logError(notEnoughSelectedMessage, null, null, true);
	        		return;
	        	}
		        
				// Check - Previously denied pairs
	        	if (mergeDecisionItem.hasPreviouslyDeniedPairs()) {
	        		var previouslyDeniedMessage = resourceService.get("MessageCode_Information_RecordsPreviouslyDenied_856");

	        		logger.logError(previouslyDeniedMessage, null, null, true);
	        		return;
	        	}
		        
	        	// Check - Unresolved challenge records
		        var containsUnresolvedChallenges = nsi.containsMessageCode(self.manualInterventionMessages(), 903);
	        	
	        	//var containsUnresolvedChallenges = ko.utils.arrayFirst(self.manualInterventionMessages()[0].InnerMessages, function (item) {
			    //    return item.MessageCode.Id === 903;
		        //});

	        	if (containsUnresolvedChallenges) {
	        		var containsUnresolvedChallengesMessage = resourceService.get("MessageCode_Error_RecordsWithUnresolvedChallenge_926").format(containsUnresolvedChallenges.Arguments);

					logger.logError(containsUnresolvedChallengesMessage, null, null, true);
					return;
				}

				// -----------
	        	// -- Confirm submit - #062
	        	
	        	mergeDecisionConfirmationModal.show().done(function (result) {

					// If user presses 'no'
	        		if (!result) {
				        return;
			        }

	        		$.blockUI({ message: "Merging" });

					// Set the loading spinner
			        self.isRunningMerge(true);

			        mergeService.submitManualInterventionResult(self.mergeDecisionItem.mergeRequestId, selectedStudentIds, userSelections).done(function(result) {

				        // -- Validation
				        if (!validateManualResolutionResponse(result, "Merge failed")) {
					        return;
				        }

				        // -- Success

				        self.canDeactivate = null; // Allow page to navigate away

				       
				     
				        app.trigger('mergeList:reset');

				        router.navigate("#/merge/list");

				        var successMessage = result.Messages[0].PresentationAttributes.Text;
				        logger.log(successMessage, null, null, true);
			        }).always(function() {
				        // Disable the loading notifiers
				        $.unblockUI();

				        self.isRunningMerge(false);
			        });

		        });
	        };

        	/* Action - Deny current merge request
			*/
	        self.onDeny = function() {
	        	mergeDenialReasonModal.show().done(function (denialReasonId) {
			        // If nothing was selected, close
	        		if (!denialReasonId) {
				        return false;
			        }

	        		self.isRunningDeny(true); // Enable the 'running' spinner. (Leave running on success, as it goes to another page after)

		        	mergeService.denyMergeRequest(self.mergeDecisionItem.mergeRequestId, denialReasonId).done(function (result) {
		        		// -- Validation
		        		if (!validateManualResolutionResponse(result, "Merge Deny failed")) {

		        			self.isRunningDeny(false);

		        			return false;
		        		}

		        		// -- Success

				        self.canDeactivate = null; // Allow page to navigate away

		        		router.navigate("#/merge/list");

			        	var successMessage = resourceService.get("MessageCode_Confirmation_StudentRecordsDenied_052");
				        logger.log(successMessage, null, null, true);
			        });
		        });
	        };

        	/* Action - Discard current merge request
			*/
	        self.onDiscard = function() {

	        	var confirmMessage = resourceService.get("MessageCode_Confirmation_DiscardMergeRequest_061");

	        	app.showMessage('', confirmMessage, ['Yes', 'No']).then(function (result) {
	        		if (result === 'Yes') {

	        			self.isRunningDiscard(true); // Enable the 'running' spinner. (Leave running on success, as it goes to another page after)

	        			mergeService.discardMergeRequest(self.mergeDecisionItem.mergeRequestId).done(function (result) {
	        				// -- Validation
	        				if (!validateManualResolutionResponse(result, "Merge Discard failed")) {
	        					self.isRunningDiscard(false);
	        					return;
	        				}

	        				// -- Success

	        				self.canDeactivate = null; // Allow page to navigate away
	        				router.navigate("#/merge/list");

	        				var successMessage = resourceService.get("MessageCode_Confirmation_MergeRequestDiscarded_914");
	        				logger.log(successMessage, null, null, true);
	        			});
	        
	        		}
	        	});
	        };

	        self.onCancel = function () {
		        var confirmMessage = resourceService.get("MessageCode_Confirmation_NavigateAway_007");
		        app.showMessage('', confirmMessage, ['Yes', 'No']).then(function (result) {
		        	if (result === 'Yes') {

		        		self.canDeactivate = null; // Allow page to navigate away

			        	router.navigate("#/merge/list");
						
			        	self.mergeDecisionItem.mergeRequestId = 0; // Discard singleton, forcing it to load again
				        self.hasPageLoaded(false); // Hide details of page
			        }
		        });
	        };

        	// -- Display / security trimming

	        self.isMergeEnabled = function() {
		        return true;
	        };

	        self.isDenyEnabled = ko.computed(function () {
	        	return self.mergeDecisionItem.getSelectedStudentIds().length >= 2;
	        });

            //Make a local reference to global Resources dictionary cache:
            //Use this to bind to from the ViewModel using syntax similar to
            //data-bind="text: resources.get("XYZ")"
            self.resources = resourceService.resources;

            //Make a local reference to global Reference dictionary cache:
            //Use this to bind to from the ViewModel using syntax similar to
            //data-bind="text: referenceData.get("XYZ")"
            self.referenceData = referenceService.referenceDictionary;

            self.navToMergeHistory = function () {
                router.navigate('/merge/history/' + self.mergeDecisionItem.mergeRequestId);

            };

    		

        	// Search results table config
	        var mergeTableConfig = {
		        dataSource: function(sortInfo, callback) {

			        mergeService.searchMergeRequests(self.savedSearch, sortInfo).done(function (result) {

		        		//// Check for failure messages
		        		//if (!result.Success && result.Messages.length > 0) {
		        		//	var message = result.Messages[0].PresentationAttributes.Text;

		        		//	logger.logError(message, null, "mergeList", true);
		        		//	//return;
		        		//}

		        		// Convert to the format DataTables wants
			        	var datatableResult = {
			        		Data: result.Data,
			        		TotalRecords: result.TotalCount,
			        		TotalDisplayRecords: result.PageSize
			        	};
				        
						// Also sent to 
			        	processMergeRequestResults(result);
				        
			        	callback(datatableResult);
			        });
		        	
		        },
		        rowTemplate: 'MergeListRowTemplate',
		        tableTemplate: 'MergeListTableTemplate',
	        	tableDrawCallback: function(bindingContext, oSettings) {

	        		var table = $(oSettings.nTable);
	        		var tbody = $("tbody", table);

	        		ko.renderTemplate('MergeListTableTemplate', bindingContext.createChildContext(self.mergeRequestResults()), null, tbody[0], "replaceChildren");
	        	},
		        columns: [
			        { bSortable: false, mDataProp: 'MergeRequestId' } // Note: The plugin currently insists that mDataProp be set, even though it's not relevant
			        , { bSortable: true, mDataProp: 'Nsn' } //NSN
			        , { bSortable: true, mDataProp: 'FamilyName' } // Family name
			        , { bSortable: true, mDataProp: 'GivenName1' } // Given Name1
			        , { bSortable: true, mDataProp: 'BirthDate' } // Birth date
			        , { bSortable: true, mDataProp: 'ResidentialStatus' } // Residential status
			        , { bSortable: true, mDataProp: 'RecordCreatedOn' } // Record created by/on
			        , { bSortable: true, mDataProp: 'CreatedOn' } // Merge requested on
			        , { bSortable: true, mDataProp: 'AssignedToUser' } // Assigned to
			        , { bSortable: false, mDataProp: 'IsAssignedToMe' } // Assign to me
		        ],
		        options: {
			        // Initial sorting - MergeRequestedDate (7) - Show earliest first
    				"aaSorting": [[7, 'asc']],

    				//Gridview length
    				"iDisplayLength": 5,
		        	"iRecordsDisplay": 5,

		        	//Overriding text templates
    				"oLanguage": {
    					"sInfo": "<span>" + "Displaying merge request(s)  " + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
    					"sInfoFiltered": "",// " of <strong>_MAX_</strong></span>", // Split 'max' output to this
    					"sInfoEmpty": resourceService.get("MessageCode_General_NoSearchResultsFound_785")
    				}
    				
		        }
		        
	        };

	        var mergeDecisionAltNamesTableConfig = {
	            dataSource: self.altNames,
	            rowTemplate: 'MergeDecisionAltNamesRowTemplate',
	            columns: [
					{ bSortable: false, mDataProp: 'Nsn' }
					, { bSortable: false, mDataProp: 'FamilyName' }
                    , { bSortable: false, mDataProp: 'GivenNames' }
					, { bSortable: false, mDataProp: 'NameBirthDateVerificationMethod' }
					, { bSortable: false, mDataProp: 'FamilyName' }
	            ],
	            options: {
	                // initial sorting
	                "aaSorting": [], // Default - Sort by ModifiedDate and ModifiedBy Desc
                
	                //Gridview length
	                'iDisplayLength': 4,
	                
	                "fnDrawCallback": function () {
	                    groupTable();
	                },

	                //Overriding text templates
	                "oLanguage": {
	                	"sInfo": self.resources.get('MergeDecisionView_MergeDecision_AltNamesPagingInfo')() + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong>",
	                    "sEmptyTable": self.resources.get('MessageCode_Information_NoAlternativeNames_019')(),
	                    "sInfoEmpty": "" //resourceService.get('StudentDetailsView_ViewStudent_GridAltInfo')
	                }
	            }
	        };
	        self.mergeDecisionAltNamesTableConfig = uiHelper.makeDatatableConfig(mergeDecisionAltNamesTableConfig);
            
	        var groupTable = function () {
	            // combine identical dimension fields in product data tables
	            $('#tbl_AltNames').each(function () {

	                var dimension_cells = new Array();
	                var dimension_col = null;

	                var i = 1;
	                // First, scan first row of headers for the "Dimensions" column.
	                $(this).find('th').each(function () {
	                    if ($(this).text() == 'NSN') {
	                        dimension_col = i;
	                    }
	                    i++;
	                });

	                // first_instance holds the first instance of identical td
	                var first_instance = null;
	                // iterate through rows
	                $(this).find('tr').each(function () {

	                    // find the td of the correct column (determined by the dimension_col set above
	                    if (dimension_col != null) {
	                        var dimension_td = $(this).find("td:nth-child(" + dimension_col + ")");

	                        if (first_instance == null) {
	                            // must be the first row
	                            first_instance = dimension_td;
	                        } else if (dimension_td.html() == first_instance.html()) {
	                            // the current td is identical to the previous
	                            // remove the current td
	                            //dimension_td.remove();
	                            //dimension_td.append('<td> </td>');
	                            $(this).closest('tr').prev('tr').addClass('noBorder');
	                            dimension_td.html(' ');
	                            dimension_td.closest('td').prev('td').html(' ');
	                            // increment the rowspan attribute of the first instance
	                            //first_instance.attr('rowspan', first_instance.attr('rowspan') + 1);
	                        } else {
	                            // this cell is different from the last
	                            first_instance = dimension_td;
	                        }
	                    }

	                });
	            });
	        };
            
	        self.attached = function () { groupTable(); };

	        self.noResultsMessage = resourceService.get("MessageCode_General_NoSearchResultsFound_785");

    		self.resultsConfig = uiHelper.makeDatatableConfig(mergeTableConfig);

        	// Common validation - Check result of manual resolution.
	        // Results 'false' if validation messages from the 
	        var validateManualResolutionResponse = function(serverResponse, fallbackMessage) {

	        	if (serverResponse.Success
	        		|| serverResponse.Messages.length === 0 // TODO: Wait for Sky to fix 'Success' flag. For now, treat '0 messages' as success too.
	        		|| serverResponse.Messages[0].MessageCode.Id === 65) { // Expected success message
			        return true;
		        }

		        // Display server errors
	        	logger.logResponseMessageErrors(serverResponse, fallbackMessage);

		        // '040' is 'MergeRequest is closed for manual resolution', so it goes away to a different page
	        	if (uiHelper.hasErrorCode(serverResponse, 40)) {
			        router.navigate("#/merge/list");
		        }

		        return false;

	        };
        };

        return viewModel;
		
    });