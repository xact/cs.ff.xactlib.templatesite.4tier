﻿define(['plugins/dialog',
		'knockout',
		'services/referenceService',
		'services/resourceService',
		'data/referenceData'],
	function (dialog, ko, referenceService, resourceService, referenceData) {
		var CustomModal = function () {

			var self = this;

			var confirmSubmitMessage = resourceService.get("MessageCode_Confirmation_MergeSelectedStudents_062");

			var lines = confirmSubmitMessage.split("\n*");

			//this.modalTitle = "Please confirm you wish to merge the selected student records.";
			this.modalTitle = lines[0];

			// Clone reference data, so we can edit it safely
			//var denialReasonList = referenceData.get("MergeDeniedReason").splice(0);

			//this.bulletPoints = [
			//	"All records with 'Include in merge' selected will be merged.",
			//	"The master record will be the NSN you have selected ",
			//	"All records with 'Include in merge' not selected will not be merged ",
			//	"Future automatic merges including the students not included in this merge and the master record will be denied with a reason of 'Different students'."];
			this.bulletPoints = [lines[1],
								 lines[2],
								 lines[3],
								 lines[4]];
				

		};

		// Initialise required properties first
		//this.resources2 = resourceService.resources;
		self.resources2 = resourceService.resources;
		CustomModal.prototype.init = function () {
			var self = this;


			$("*").on('focusin', function (e) {
				var modalWindow = $('#simplemodal-container')[0];
				if (modalWindow !== e.target && !modalWindow.contains(e.target)) {
					e.target.blur();
					//modalWindow.attr("tabindex", -1).focus();
				}
			});

			//// Ensure dropdown content is loaded first
			//var promise = referenceService.getReferenceData("MergeDeniedReason").then(function (data) {
			//	var clone = data.splice(0);
			//	clone[0].Value = clone[0].Value + " (default)";
				
			//	self.denialReasonList(clone);
			//});

			//return promise;
		};

		// Page methods
		CustomModal.prototype.onYes = function () {
			dialog.close(this, true);
		};

		CustomModal.prototype.onCancel = function () {
			dialog.close(this);
		};

		// Dialog
		CustomModal.show = function () {
			var newModal = new CustomModal();

			newModal.init();

			return dialog.show(newModal);
		};

		return CustomModal;
	});