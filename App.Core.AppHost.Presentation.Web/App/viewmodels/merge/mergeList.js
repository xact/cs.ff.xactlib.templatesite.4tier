﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/mergeService',
		'services/resourceService',
		'services/referenceService',
		'services/helpTextService',
		'data/referenceData',
		'singletons/mergeStudent'
        ],
    function (router, app, logger,
		mergeService,
		resourceService, referenceService, helpTextService, referenceData, mergeStudent) {

        //Comments regarding Code:
        //* Separate Method Definitions by at least one space.
        //* Create methods definitions using object name, not self. Reserve self statement
        //  for getting around thorny 'this' syntax conditions, not method definitinos.
        //* prefer using full class definitions, so that init methods can invoke methods
        //  without having to define them higher on the page (code spa


		
        var viewModel = function () {

    		var self = this;

    			
    		self.nsnSearchInput = ko.observable();
	        self.compositionComplete = function () {
		        // Load data to table
	        	//$("#MergeListTable").dataTable().fnDraw();

				// Enable show/hide button
	        	uiHelper.hider();
	        	$.placeholder.shim();
		        uiHelper.focusFirstInputElement();
	        };

		    self.onClear = function() {
		    	self.nsnSearchInput("");
		    	self.searchCriteria.providerCode("");
		    	self.searchCriteria.familyName("");
		    	self.searchCriteria.givenNames("");
		    	//have to re-set this as it will be used from
		    	//within xxx
		    	self.savedSearch = {
		    		Nsn: self.nsnSearchInput()
		    	};
		    	//this triggers onNsnSearch
		    	self.triggerTableUpdate();

		    };
        	// -- Properties
	        self.resources2 = resourceService.resources;
	       


	        self.searchCriteria = {
	        	providerCode: ko.observable(),
	        	familyName: ko.observable(),
	        	givenNames: ko.observable()
	        };

	    	//Register an broadcast message handler for a message
	    	//raised from the routes.js. 
			//In this case it ensures that the screen is cleared.
	        app.on("mergeList:reset").then(function () {
	        	self.onClear();
	        	
	        });

	        self.mergeRequestResults = ko.observableArray();

	        self.savedSearch = {
		        
	        };

	        self.hasPageLoaded = ko.observable(false);

	        self.isLoadingNsnSearch = ko.observable(false);
	        self.isLoadingCriteriaSearch = ko.observable(false);

        	// -- Actions
	

	        self.triggerTableUpdate = function () {
	        	var dt = $("#MergeListTable").dataTable();
	        	if (dt != null && dt.length > 0) {
	        		dt.fnDraw();
	        	}
	        };

	        //var triggerTableUpdate = function() {
	        //	$("#MergeListTable").dataTable().fnDraw();
	        //};
	        
	        self.onNsnSearch = function () {

	        	//if (self.staticErrors().length > 0) {
	        	//	return false;
	        	//}

	        	if (!self.checkValid(self.staticErrors)) {
	        	    return;
	        	}

				// Set the saved search for subsequent pagination/etc
	        	self.savedSearch = {
			        Nsn: self.nsnSearchInput()
		        };

				// Trigger datatables update - It will use the saved search
		        self.isLoadingNsnSearch(true); // Activate button spinner - Datatables will close it
	        	self.triggerTableUpdate();
		        
	            self.shouldDisplayNoResultsMessage();

	            //mergeService.searchMergeRequestsByNsn(self.nsnSearchInput()).then(function(result) {

	            //	processMergeRequestResults(result);
	            //});
	        };



	        self.onCriteriaSearch = function () {
		        
	        	//if (self.staticErrors().length > 0) {
	        	//	return false;
	        	//}
		        
	        	if (!self.checkValid(self.staticErrors)) {
	        	    return;
	        	}
	        	

		        self.savedSearch = {
			        ProviderCode: self.searchCriteria.providerCode(),
			        FamilyName: self.searchCriteria.familyName(),
			        GivenNames: self.searchCriteria.givenNames()
		        };

		        self.isLoadingCriteriaSearch(true);
		        self.triggerTableUpdate();
		        self.shouldDisplayNoResultsMessage();
		        
	        	//mergeService.searchMergeRequests(
	        	//	self.searchCriteria.providerCode(),
	        	//	self.searchCriteria.familyName(),
	        	//	self.searchCriteria.givenNames()).then(function (result) {
	        			
	        	//		processMergeRequestResults(result);
	        	//});
	        };

	    

	        self.onViewMergeRequest = function(mergeRequest) {
		        logger.log("View merge request not yet available. Merge request ID: " + mergeRequest.MergeRequestId, null, null, true);
	        };

	        self.doesResultHasAnyErrors = ko.observable(false);
            
	        var processMergeRequestResults = function (result) {
		        
				// Check for failure messages
	            if (!result.Success && result.Messages.length > 0) {
	                var message = result.Messages[0].PresentationAttributes.Text;

	                logger.logError(message, null, "mergeList", true);
	                self.doesResultHasAnyErrors(true);
	                self.mergeRequestResults.removeAll();
	                //return;
	            } else {
	                // Update results
	                self.doesResultHasAnyErrors(false);
	                self.mergeRequestResults(result.Data);
	            }
	        };

	        self.onChangeAssignToMe = function(data) {
	        	mergeService.setMergeRequestAssignment(data.MergeRequestId, data.IsAssignedToMe()).done(function (result) {
			        if (typeof (console) !== "undefined" && console.log !== undefined) {
				        console.log("Assign result: " + result);
			        }
			        data.AssignedToUser(result);
		        });

		        return true;
	        };

            //Make a local reference to global Resources dictionary cache:
            //Use this to bind to from the ViewModel using syntax similar to
            //data-bind="text: resources.get("XYZ")"
            self.resources = resourceService.resources;

            //Make a local reference to global Reference dictionary cache:
            //Use this to bind to from the ViewModel using syntax similar to
            //data-bind="text: referenceData.get("XYZ")"
            self.referenceData = referenceService.referenceDictionary;

    		self.resources2 = resourceService.resources;
    	    
        	// -- Validation
    		self.staticErrors = ko.validation.group([
    			self.nsnSearchInput,
    			self.searchCriteria.providerCode,
    			self.searchCriteria.familyName,
    			self.searchCriteria.givenNames]
    			, { observable: true });

    		self.refreshErrors = ko.observableArray();

    		self.nsnSearchInput.extend({
    			isNumeric: {
    				message: resourceService.get("MessageCode_Validation_Numeric_003").format("NSN"), // TODO: update 'NSN' from resources
    				onlyIf: function () { return self.nsnSearchInput(); }
    			},
    			validNsn: {
    				message: resourceService.get("MessageCode_Validation_InvalidNSN_336"),
    				onlyIf: function () { return self.nsnSearchInput(); }
    			}
    			
    			// Message #337 (no NSN) comes from the server
    		});

    		self.searchCriteria.providerCode.extend({
	            validation: [
                   {
	                   validator: function () {
                           var isValid = self.nsnSearchInput() && self.searchCriteria.providerCode();
                           return !isValid;
                   },
                       //message#913: "Provider code, Family name and Given Name(s) cannot be used in conjunction with NSN."
                       message: resourceService.get("MessageCode_Error_CannotBeUsedWithNSN_913")
                   }],
	        	validProviderCode: {
	        		message: resourceService.get("MessageCode_Validation_InvalidProviderCode_339"),
	        		onlyIf: function () { return self.searchCriteria.providerCode(); }
		        }
	        });

			// Common name field validation
			function setNameValidation(field, fieldName) {
				field.extend({
					validation: [
						{
							validator: function () {
								var isValid = self.nsnSearchInput() && field();
								return !isValid;
							},
							//message#913: "Provider code, Family name and Given Name(s) cannot be used in conjunction with NSN."
							message: resourceService.get("MessageCode_Error_CannotBeUsedWithNSN_913")
						}],
					
					containsValidCharacters: {
						message: resourceService.get("MessageCode_Validation_InvalidFormat_272").format(fieldName),
						onlyIf: function () { return field() && fieldName==="Family name"; }
					},
					
					containsValidCharactersForOneSingleName: {
				    message: resourceService.get("MessageCode_Validation_InvalidFormat_272").format(fieldName),
				    onlyIf: function () { return field() && fieldName === "Given name(s)"; }
				}
//					,
//	        	
//					notJustAllowedSymbols: {
//						message: resourceService.get("MessageCode_Validation_InvalidHyphenApostrophe_278")
//					}
//					
				});
			}

			// Apply common name validation to both name fields
			setNameValidation(self.searchCriteria.familyName, "Family name");
			setNameValidation(self.searchCriteria.givenNames, "Given name(s)");

            // Validation - Check if the current valdiation group is 
	        self.checkValid = function (validationGroup) {
	            validationGroup.showAllMessages();

	            if (validationGroup().length !== 0) {
	                
	                // Manual clear & populate with *non-observable* validation messages
	                var updatedErrors = new Array();
	                ko.utils.arrayForEach(validationGroup(), function (validationMessage) {
	                    updatedErrors.push(ko.utils.unwrapObservable(validationMessage));
	                });
	                self.refreshErrors(updatedErrors); // Move errors to display list

	                $.placeholder.shim(); // Refresh - Damn IE
	                return false;
	            } else {
	                self.refreshErrors.removeAll();

	                $.placeholder.shim(); // Refresh - Damn IE
	                return true;
	            }
	        };

            
	       self.xxx = function(sortInfo, callback) {

			        mergeService.searchMergeRequests(self.savedSearch, sortInfo).done(function (result) {

		        		//// Check for failure messages
		        		//if (!result.Success && result.Messages.length > 0) {
		        		//	var message = result.Messages[0].PresentationAttributes.Text;

		        		//	logger.logError(message, null, "mergeList", true);
		        		//	//return;
		        		//}

		        		// Convert to the format DataTables wants
			        	var datatableResult = {
			        		Data: result.Data,
			        		TotalRecords: result.TotalCount,
			        		TotalDisplayRecords: result.PageSize
			        	};
				        
						// Also sent to 
			        	processMergeRequestResults(result);
				        
			        	callback(datatableResult);
				        
			        	if (!self.hasPageLoaded()) {
			        		self.hasPageLoaded(true);
			        	}
			        }).always(function () {
				        // Always disable the spinner
				        self.isLoadingNsnSearch(false);
			        	self.isLoadingCriteriaSearch(false);
			        });
		        	
	        }
            
        	// Search results table config
	        var mergeTableConfig = {
		        dataSource: self.xxx,
		        rowTemplate: 'MergeListRowTemplate',
		        tableTemplate: 'MergeListTableTemplate',
	        	tableDrawCallback: function(bindingContext, oSettings) {

	        		// The merge list page displays multiple rows for each 'MergeRequest', so this handler allows us to override the
	        		// simpler table row templates we usually use
	        		var table = $(oSettings.nTable);
	        		var tbody = $("tbody", table);

	        		ko.renderTemplate('MergeListTableTemplate', bindingContext.createChildContext(self.mergeRequestResults()), null, tbody[0], "replaceChildren");
	        	},
		        columns: [
			        { bSortable: false, mDataProp: 'MergeRequestId' } // Note: The plugin currently insists that mDataProp be set, even though it's not relevant
			        , { bSortable: true, mDataProp: 'Nsn' } //NSN
			        , { bSortable: true, mDataProp: 'FamilyName' } // Family name
			        , { bSortable: true, mDataProp: 'GivenNames' } // Given Name
			        , { bSortable: true, mDataProp: 'BirthDate', "sType": "date-dd-mmm-yyyy" } // Birth date
			        , { bSortable: true, mDataProp: 'ResidentialStatus' } // Residential status
			        , { bSortable: true, mDataProp: 'RecordCreatedOn' } // Record created by/on
			        , { bSortable: true, mDataProp: 'CreatedOn' } // Merge requested on
			        , { bSortable: true, mDataProp: 'AssignedToUser' } // Assigned to
			        , { bSortable: false, mDataProp: 'IsAssignedToMe' } // Assign to me
		        ],
		        options: {
			        // Initial sorting - MergeRequestedDate (7) - Show earliest first
    				"aaSorting": [[7, 'asc']],

    				//Gridview length
    				"iDisplayLength": 5,
		        	"iRecordsDisplay": 5,

		        	//Overriding text templates
    				"oLanguage": {
    					"sInfo": "<span>" + "Displaying merge request(s)  " + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
    					"sInfoFiltered": "",// " of <strong>_MAX_</strong></span>", // Split 'max' output to this
    					"sInfoEmpty": resourceService.get("MessageCode_General_NoSearchResultsFound_785")
    				},
		        	//customising DOM
    				"sDom": 'r<"paging" ip>t<"paging" ip>'
		        }
		        
	        };

	        self.noResultsMessage = resourceService.get("MessageCode_General_NoSearchResultsFound_785");
	        self.shouldDisplayNoResultsMessage = ko.computed(function () {
	        	return (self.mergeRequestResults().length === 0
	        			&& self.hasPageLoaded()
	        			&& self.doesResultHasAnyErrors() === false);
	        }, this);
	        

    		self.resultsConfig = uiHelper.makeDatatableConfig(mergeTableConfig);
	        

        };

        return new viewModel();

        
		
		
    });