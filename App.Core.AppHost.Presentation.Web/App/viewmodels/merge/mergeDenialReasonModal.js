﻿define(['plugins/dialog',
		'knockout',
		'services/referenceService',
		'services/resourceService',
		'data/referenceData'],
	function (dialog, ko, referenceService, resourceService, referenceData) {
		var CustomModal = function () {

			var self = this;

			this.denialReason = ko.observable();
			self.referenceData = referenceService.referenceDictionary;
			// Clone reference data, so we can edit it safely
			//var denialReasonList = referenceData.get("MergeDeniedReason").splice(0);

			this.denialReasonList = ko.observable();

			
		};

		// Initialise required properties first
		//this.resources2 = resourceService.resources;
		self.resources2 = resourceService.resources;
		CustomModal.prototype.init = function () {
			var self = this;

			$("*").on('focusin', function (e) {
				var modalWindow = $('#simplemodal-container')[0];
				if (modalWindow !== e.target && !modalWindow.contains(e.target)) {
					e.target.blur();
					//modalWindow.attr("tabindex", -1).focus();
				}
			});

			// Ensure dropdown content is loaded first
			var promise = referenceService.getReferenceData("MergeDeniedReason").then(function (data) {
				if (data && data.length > 0) {
					var clone = data.splice(0);
					//clone[0].Value = clone[0].Value + " (default)";

					self.denialReasonList(clone);
				}
			});

			return promise;
		};

		// Page methods
		CustomModal.prototype.onDeny = function () {
			dialog.close(this, this.denialReason());
		};

		CustomModal.prototype.onCancel = function () {
			dialog.close(this);
		};

		// Dialog
		CustomModal.show = function () {
			var newModal = new CustomModal();

			newModal.init().done(function () {
				//$("select").each(function () {
				//	$(this.options).first().attr("selected", "selected");
				//});;
			});
			 
			return dialog.show(newModal);
		};
		
		return CustomModal;
	});