﻿define(['plugins/router',
		'services/logger',
		'services/resourceService',
		'services/sessionService',
		'services/helpTextService',
		'services/parameterService',
		'singletons/parameters'
        ],
    function (router, logger, resourceService, sessionService, helpTextService, parameterService, parameters) {
        var model = function () {
            var self = this;
            // Homepage Links (title and URL)
            this.homePage_ListOfLinks = resourceService.get('HomeView_ListOfLinks');
            // retrieve the Help Text for the view
            helpTextService.getStringsForView("HomeView", "en");
            // set the parameters for local availability
            self.currentParameters = parameters;
            // set the resources for local availability
            this.resources2 = resourceService.resources;
            //self.isGoLiveDateNotReached = ko.observable(false);
            //self.sessionWasTimedOut = ko.observable(false);

            self.messageCodeText = ko.observable(null);
            
            self.messageCodeTextVisible = ko.computed(function(){return self.messageCodeText()!=null});
            
            //Page elements resource packs
            var resource = function () {
                this.HomeView_ViewTitle = resourceService.get('HomeView_ViewTitle');
                this.HomeView_AboutNsiContent1 = resourceService.get('HomeView_AboutNsiContent1');
                this.HomeView_AboutNsiContent2 = resourceService.get('HomeView_AboutNsiContent2');
                this.HomeView_AboutNsiContent3 = resourceService.get('HomeView_AboutNsiContent3');
                this.HomeView_AccessNSIActionText = resourceService.get('HomeView_AccessNSIActionText');
                this.HomeView_NSIGeneralInfoNavText = resourceService.get('HomeView_NSIGeneralInfoNavText');
                this.HomeView_StudentGuideNavText = resourceService.get('HomeView_StudentGuideNavText');
                this.HomeView_AuthorisedInfoMatchingProgrammeNavText = resourceService.get('HomeView_AuthorisedInfoMatchingProgrammeNavText');
                this.HomeView_RequestACopyNavText = resourceService.get('HomeView_RequestACopyNavText');

            };
            
            // This calls resources to get the text and parameters to get the links
    		self.listOfLinks = ko.observableArray([
                { title: self.resources2.get('HomeView_NSIGeneralInfoNavText'), target: self.currentParameters.get('LINK_1') },
                { title: self.resources2.get('HomeView_StudentGuideNavText'), target: self.currentParameters.get('LINK_2') },
                { title: self.resources2.get('HomeView_RequestACopyNavText'), target: self.currentParameters.get('LINK_3') },
                { title: self.resources2.get('HomeView_AuthorisedInfoMatchingProgrammeNavText'), target: self.currentParameters.get('LINK_4') }

            ]);

            resource();

            // Viewmodel events
    		self.compositionComplete = function () {
            	// TEMP - Better way to cross-communicate with viewModels?
            	$('div.title').hide();
                $('ul.footer li:first-child').hide();
                $('ul.footer li:nth-child(2)').css("border", "0");

                $('header div.logo a').attr("href", "#"); // TEMP - Short way to manage these links...
                
                var messageCode = this.getUrlParameters("MessageCode", "", false);

    		    if (messageCode) {
    		        switch (messageCode) {
    		            case "1283":
    		                self.messageCodeText(self.resources2.get('MessageCode_General_NSICannotBeAccessed_1283'));
    		                break;
    		            case "088":
    		                self.messageCodeText("You have been logged out of the NSI.");
    		                break;
    		            case "090":
    		                self.messageCodeText('Your NSI session has timed out. Please log in again.');
    		                break;
    		            default:
    		                self.messageCodeText("MessageCode: " + messageCode);
    		        }
    		    }


    		    //self.isGoLiveDateNotReached(messageCode == "1283");
                //self.sessionWasTimedOut(messageCode == "090");
    		    //Update page url:
    		    //window.location = "#";// window.location.href.split('?')[0];
    		    
                //var hash = window.location.hash;
                //hash = hash.split('?')[0];
                //if (!hash) { hash = "#"; }
    		    //window.location.hash = hash;
    		};

            
    		self.deactivate = function () {
            	// TEMP - Better way to cross-communicate with viewModels?
	            $('div.title').show();
	            $('ul.footer li:first-child').show();
	            $('ul.footer li:nth-child(2)').removeAttr('style');
	            
	            $('header div.logo a').attr("href", "#/welcome"); // TEMP - Short way to manage these links...
            };
            

                /*
                 Function: getUrlParameters
                 Description: Get the value of URL parameters either from 
                              current URL or static URL
                 Author: Tirumal
                 URL: www.code-tricks.com
                */
            //Used to check messagecodes and to block unauthorized (with future golivedate) providers
            this.getUrlParameters = function (parameter, staticURL, decode) {
                var currLocation = (staticURL.length) ? staticURL : window.location.href;
                if (currLocation) {
                    firstParameter = currLocation.split("?")[1];
                    if (firstParameter) {
                        parArr = firstParameter.split("&");
                    } else {
                        parArr = "";
                    }
                }
                returnBool = true;

                for (var i = 0; i < parArr.length; i++) {
                    parr = parArr[i].split("=");
                    if (parr[0] == parameter) {
                        return (decode) ? decodeURIComponent(parr[1]) : parr[1];
                        returnBool = true;
                    } else {
                        returnBool = false;
                    }
                }

                if (!returnBool) return false;
            };
        };

    	model.prototype.onLogin = function () {
	   		
    		var signOnHandlerLocation = "HttpHandlers/SignOn.ashx";

    		var pageHash = window.location.hash;
    		var indexOfQueryString = pageHash.indexOf("?ReturnUrl");
    		if (pageHash && indexOfQueryString >= 0) {
    			var returnUrl = pageHash.substr(indexOfQueryString);

			    signOnHandlerLocation = signOnHandlerLocation + returnUrl;
		    }

    		window.location = signOnHandlerLocation;
        };

        return model;
    });