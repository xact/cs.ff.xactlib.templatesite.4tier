﻿define(['plugins/router',
        'durandal/app',
		'services/logger',
        'services/referenceService',
		'services/resourceService',
		'services/providerService',
		'singletons/provider',
		'entities/providerEntity'
],
	function (router, app, logger, referenceService, resourceService, providerService, originalprovider, providerEntity) {
		var viewModel = function () {
			var self = this;

			// -------------------
			// -- Lifecycle events
			self.canActivate = function (providerCode) {
				var promise = $.Deferred();

				providerService.getProviderByCode(providerCode).done(function () {
					promise.resolve(true);
				});
				return promise.promise();

				//return providerService.getProviderByCode(providerCode);
			};

			self.attached = function () {
				var uiHelper = com.uiHelper;
				// adds jQuery events to be bound to controls that hides/shows sections (added by Jo) -MB
				uiHelper.hider();
			};

			
			self.activate = function() {

				// Clone provider
				var origData = ko.toJS(originalprovider);
				ko.mapping.fromJS(origData, null, self.copiedProvider);
				
				// Track changes to live date (must happen before validation is attached)
				self.provider.NSILiveDate.extend({
					trackChange: true
				});
				self.provider.SPRDuration.extend({
					trackChange: true
				});
				
				attachValidation();

				
			};
			
			self.canDeactivate = function () {

				// Allow deactivation if no changes are made
				if (!hasModifiedProvider()) {
					return true;
				}

				// If changes have been made - prompt to confirm
				var navigateAwayMessage = resourceService.get("MessageCode_Confirmation_NavigateAway_007");

				return app.showMessage("", navigateAwayMessage, ['Yes', 'No']).done(function (result) {
					if (result === "No") { // 'Stay on page' option
						return false;
					}

					return true;

					
				});
			};

			self.deactivate = function () {
				// Subscribe to 'search reset' event
				//self.provider.clear();
				self.canShowErrors(false);
				//self.staticErrors.removeAll();
			};


			self.compositionComplete = function () {
				// Ensure placeholder text
				// Must happen once page is visible, to support IE
				$.placeholder.shim();
				uiHelper.focusFirstInputElement();
			};


			// -------------
			// -- Properties
			self.valMsg = "!";
			self.provider = originalprovider;
			self.notValidField = uiHelper.notValidField,
			self.copiedProvider = new providerEntity(); //{}; //originalprovider;

			//	//Runtime resource management
			self.resources = resourceService.resources;
			//self.requiredFields = ko.observableArray();
			//For cancel button
			self.canShowErrors = ko.observable(false);


			self.isSaving = ko.observable(false);

			// -------------
			// -- Validation

			self.validationGroup = ko.validation.group(self.provider);

			var hasModifiedProvider = function () {

				return ko.utils.hasChanges(self.copiedProvider, self.provider);
			}; // Provider fields have changed

			
			var attachValidation = function() {

				//self.requiredFields = ko.observableArray();
			
				self.provider.SPRDuration.extend({
					required: {
						message: resourceService.get('MessageCode_Validation_MissingMandatoryFields_257').format("<ul>" + "<li>" + "SPR duration" + "</li>" + "</ul>"),
						onlyIf: function () { return !self.provider.SPRDuration() && self.canShowErrors(); }
					},

					isNumeric: {
						message: resourceService.get("MessageCode_Validation_Numeric_003").format("SPR duration"),
						onlyIf: function () { return self.provider.SPRDuration(); }
					}
				});


				self.provider.NSILiveDate.extend({
					validNzDate: {
						message: resourceService.get('MessageCode_Validation_Date_001').format("NSI live date"),
						onlyIf: function() { return self.provider.NSILiveDate(); }
					},
					nzDate: {
						message: resourceService.get('MessageCode_Validation_InvalidDate_305').format("NSI live date"),
						onlyIf: function() { return self.provider.NSILiveDate(); }
					},
					minDateIsToday: {
						message: resourceService.get('MessageCode_Error_NSILiveDateInThePast_072'),
						onlyIf: function() {

							//var hasChanged = self.provider.NSILiveDate() && self.provider.NSILiveDate() !== self.copiedProvider.NSILiveDate();

							var hasChanged = self.provider.NSILiveDate.isDirty();

							return hasChanged;
						}
					}
				});
			};

			// ----------
			// -- Actions

			self.saveProvider = function () {
				self.canShowErrors(true);
				//if (self.displayErrors().length > 0) {
				//	return false;
				//}
				if (self.validationGroup().length > 0) {
					return false;
				}

				var today = new Date();
				var currentDateTime = today.getDate + today.getMonth + today.getFullYear + today.getHours + today.getMinutes + today.getSeconds;
				var params = {
					ReceiveChangeNotifications: originalprovider.ReceiveChangeNotifications() === "Yes" ? true : false,
					FileFormatFK: referenceService.getKeyByValue('FileFormat', originalprovider.FileFormatForChangeNotifications()),
					UseMacrons: originalprovider.UseMacrons() === "Yes" ? true : false,
					ManualMergeRequired: originalprovider.ManualMergeRequired() === "Yes" ? true : false,
					InterfaceXml: originalprovider.InterfaceXML(),
					InterfaceSoap: originalprovider.InterfaceSOAP(),
					InterfaceBatch: originalprovider.InterfaceBatch(),
					InterfaceRest: originalprovider.InterfaceRest(),
					OnBehalf: originalprovider.OnBehalf() === "Yes" ? true : false,
					Code: originalprovider.ProviderCode(),
					Name: originalprovider.ProviderName(),
					ActiveDurationDays: originalprovider.SPRDuration() * 365,
					GoliveDateTime: originalprovider.NSILiveDate(),
					LastChangeNotificationDownloadDateTime: (self.copiedProvider.ReceiveChangeNotifications() === "No" && originalprovider.ReceiveChangeNotifications() === "Yes") ? today.toISOString() : self.copiedProvider.LastChangeNotificationDownloadDateTime()
					
				};

				self.isSaving(true); // Enable spinner
				// - Spinner should remain 'pending' on successful save, while it redirects (to stop possible 2nd update)

				providerService.updateProvider(params).done(function (result) {
					if (result.Messages.length > 0) {
						ko.utils.arrayForEach(result.Messages, function (item) {
							logger.logError(item.PresentationAttributes.Text, null, null, true);
						});

						self.isSaving(false); // Disable spinner

						return false;

					};

					var providerSaved = function () {

						self.canDeactivate = null; // TODO: Is this the correct way to cancel the deactivation check?

						var saveSuccessMessage = resourceService.get("MessageCode_Confirmation_ProviderSaved_073");
						logger.log(saveSuccessMessage, null, null, true);
					};
					router.navigate('/admin/providers/');

					return providerSaved();




				});
				return true;
			};


			//Click Cancel - Revert back to Provider list page
		
			self.revertBackToProviderList = function () {
				
				if (!hasModifiedProvider()) {
					router.navigate('/admin/providers/');
				}

					// If changes have been made - prompt to confirm
					//NOTE : canDeactivate method gets called for all navigations away from page after changes. Hence no need for new confirmation box.
				
				else if (hasModifiedProvider()) {
			
						router.navigate('/admin/providers/');
					}

			};
		};
		return viewModel;

		//#endregion
	});