﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/resourceService',
		'services/referenceService',
		'services/providerService',
		'services/helpTextService',
        'viewModels/admin/parameterDetails',
        'entities/parameterEntity',
		'services/parameterService',
		'services/sessionService',
		'data/settings'

],
	function (router, app, logger, resourceService, referenceService, providerService, helpTextService, parameterDetailsModal, parameterEntity, parameterService, sessionService, settings) {

		//Comments regarding Code:
		//* Separate Method Definitions by at least one space.
		//* Create methods definitions using object name, not self. Reserve self statement
		//  for getting around thorny 'this' syntax conditions, not method definitinos.
		//* prefer using full class definitions, so that init methods can invoke methods
		//  without having to define them higher on the page (code spa

		var viewModel = function () {

			var self = this;
			self.id = 0;
			//#region ViewModelEvents
			self.canActivate = function () {
				var promise = $.Deferred();
				parameterService.getAllParameterSettings().done(function (result) {

					self.results(result);
					promise.resolve(true);

				});

				return promise.promise();

			};

			self.activate = function () {
				app.on("parameterList:reset").then(function () {
					$('#tab-messages').removeClass('pie current').addClass('pie');
					$('#tab-params').removeClass('pie').addClass('pie current');
					self.help = resourceService.resources.get('ParametersView_ParametersViewEditSymbolButtonTip')();
					self.GetParams();


					$.placeholder.shim(); // Refresh - Damn IE

				});


			};

			self.GetParams = function () {
				parameterService.getAllParameterSettings(self.id).done(function (result) {
					self.results(result);
				});
			};

			self.canDeactivate = function () {
				return true;
			};


			self.compositionComplete = function () {

				//For IE8/9 Make the nav menu lose focus of its current item as there
				//is nothing else to focus on, so it can get the right colour
				$('.current a').blur();

			};

			//#endregion

			//#region Properties
			// -- Properties


			//Make a local reference to global Resources dictionary cache:
			//Use this to bind to from the ViewModel using syntax similar to
			//data-bind="text: resources.get("XYZ")"
			self.resources = resourceService.resources;
			self.parameterEntity = parameterEntity;
			//Make a local reference to global Reference dictionary cache:
			//Use this to bind to from the ViewModel using syntax similar to
			//data-bind="text: referenceData.get("XYZ")"
			self.referenceData = referenceService.referenceDictionary;
			self.results = ko.observableArray();
			self.hasPageLoaded = ko.observable(false);
			self.success = ko.observable(false);
			self.help = resourceService.resources.get('ParametersView_ParametersViewEditSymbolButtonTip')();
			self.canModifyParams = sessionService.isAuthorised(settings.securityClaims.NSI2_EDIT_PARAMETER);

			//#endregion

			//#region Actions
			// -- Actions


			self.onViewParameterClick = function (boundedParameters) {
				var origData = ko.toJS(boundedParameters);
				ko.mapping.fromJS(origData, null, parameterEntity);

				parameterDetailsModal.show(parameterEntity).done(function () {

					//you need to unwrap, as some come as observable, others come as properties.
					boundedParameters.Value = ko.utils.unwrapObservable(parameterEntity.Value);
					var oTable = $('#params').dataTable();

					// Search only column 0 for the name of the param and return an array of indexes
					// used instead of the following line as it doesn't work.
					// var rowIndex = self.results.indexOf(boundedParameters);
					var rowIndexes = oTable.fnFindCellRowIndexes(boundedParameters.Parameter, 0);

					if (rowIndexes.length > 0) {

						//update only the value of the selected item, results in losing the edit icon (redraw needed)
						oTable.fnUpdate(boundedParameters, rowIndexes[0], null, false, false);
						//Redraw without losing current paging 
						oTable.fnStandingRedraw();
					}

					// alternative manual update:

					//					self.results.remove(function (param) { return param.Parameter == boundedParameters.Parameter; });
					//					boundedParameters.Value = parameterEntity.Value();
					//					self.results.unshift(boundedParameters);
				});

				//    		    parameterDetailsModal.show(parameterEntity).always(function () {
				//    		        //parameterService.getAllParameterSettings(self.id).always(function (result) {
				//    		            //self.results(result);
				//    		            //triggerTableUpdate();
				//    		        //});
				//		        });
			};


			self.onParameterTabClick = function () {

				self.id = 0;
				parameterService.getAllParameterSettings(self.id).done(function (result) {
					self.results(result);
					self.setPagination("Parameter");
				});
				$('#tab-messages').removeClass('pie current').addClass('pie');
				$('#tab-params').removeClass('pie').addClass('pie current');
				self.help = resourceService.resources.get('ParametersView_ParametersViewEditSymbolButtonTip')();

			};


			self.onMessagesTabClick = function () {

				self.id = 1;
				parameterService.getAllParameterSettings(self.id).done(function (result) {
					self.results(result);
					self.setPagination("Message");
				});
				$('#tab-messages').removeClass('pie').addClass('pie current');
				$('#tab-params').removeClass('pie current').addClass('pie');
				self.help = resourceService.resources.get('ParameterMessagesView_ParameterMessagesViewEditSymbolButtonTip')();
			};

			self.setPagination = function (type) {
				var oTable = $('#params').dataTable();
				oTable.fnSettings().oLanguage.sInfo = "<span>" + "Displaying " + type + "(s)" + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>";
				//Redraw without losing current paging 
				oTable.fnStandingRedraw();
			};



			//#endregion


			//#region Validation
			// -- Validation
			self.refreshErrors = ko.observableArray();

			// Validation - Check if the current valdiation group is 

			//#endregion

			//#region DataTables config
			// Search results table config
			self.parameterTableConfig = {
				dataSource: self.results,
				rowTemplate: 'ParametersRowTemplate',
				columns: [
					{ bSortable: false, mDataProp: 'Parameter' }, // Note: The plugin currently insists that mDataProp be set, even though it's not relevant
					{ bSortable: false, mDataProp: 'Value' },
					{ bSortable: false, mDataProp: 'Description' },
					{ bSortable: false, mDataProp: 'Datatype' }
				],
				options: {
					// Initial sorting - Parameter - Ascending alphabetically

					"aaSorting": [[0, 'asc']],

					//Gridview length
					"iDisplayLength": 10,
					"iRecordsDisplay": 10,

					//Overriding text templates
					"oLanguage": {
						"sInfo": "<span>" + "Displaying Parameter(s)" + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
						"sInfoFiltered": "", // " of <strong>_MAX_</strong></span>", // Split 'max' output to this
						"sInfoEmpty": "",
					}
				}

			};



			self.resultsConfig = uiHelper.makeDatatableConfig(self.parameterTableConfig);


			//#endregion

		};

		return viewModel;

	});