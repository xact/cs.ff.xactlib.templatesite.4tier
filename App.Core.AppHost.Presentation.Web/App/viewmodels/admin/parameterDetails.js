﻿define(['plugins/dialog',
        'plugins/router',
        'durandal/app',
		'services/logger',
        'services/referenceService',
		'services/resourceService',
		'services/parameterService',
		'singletons/parameters',
		'entities/parameterEntity'
],
	function (dialog, router, app, logger, referenceService, resourceService, parameterService, originalparameter, parameterEntity) {

	    var CustomModal = function () {
	        var self = this;

	        //Runtime resource management
	        self.resources = resourceService.resources;
	        this.parameterDetails = ko.observable();
	        //Duplicate local parameter object to compare whether the original got changed or not
	        self.copiedParameter = function () {
	            var entity = function () {
	                var self = this;
	                self.Parameter = ko.observable();
	                self.Value = ko.observable();
	                self.Description = ko.observable();
	                self.Datatype = ko.observable();
	                self.Parametertype = ko.observable();
	                self.IsEditable = ko.observable();
	            };
	            return entity;
	        };
	        self.valueBoolean = ko.observable();
	        self.labelYesBoolean = ko.observable();
	        self.labelNoBoolean = ko.observable();
	    };

	    CustomModal.prototype.init = function () {

	    	$("*").on('focusin', function (e) {
	    		var modalWindow = $('#simplemodal-container')[0];
	    		if (modalWindow !== e.target && !modalWindow.contains(e.target)) {
	    			e.target.blur();
	    			//modalWindow.attr("tabindex", -1).focus();
	    		}
	    	});


	        var self = this;
	        var promise = $.Deferred();
	        var origData = ko.toJS(parameterEntity);
	        ko.mapping.fromJS(origData, null, self.copiedParameter);
	        self.parameter = parameterEntity;
	        attachValidation(self.parameter);
	        self.isEmailURLVisible = ko.computed(function () {
	            if (self.parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_Email") || self.parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_URL")) {
	                return true;
	            } else {
	                return false;
	            }
	        });
	        self.isStringVisible = ko.computed(function () {
	            if (self.parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_String")) {
	                return true;
	            } else {
	                return false;
	            }
	        });
	        self.isNumericVisible = ko.computed(function () {
	            if (self.parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_Numeric")) {
	                return true;
	            } else {
	                return false;
	            }
	        });
	        self.isDateVisible = ko.computed(function () {
	            if (self.parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_Date")) {
	                return true;
	            } else {
	                return false;
	            }
	        });
	        self.isDateTimeVisible = ko.computed(function () {
	            if (self.parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_DateTime")) {
	                return true;
	            } else {
	                return false;
	            }
	        });
	        self.isBooleanVisible = ko.computed(function () {
	            if (self.parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_Boolean")) {
	                self.labelYesBoolean = ko.computed(function () {
	                    if (self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_Y")) {
	                        self.labelNoBoolean = resourceService.get("ParameterDetailsView_Boolean_No");
	                        return resourceService.get("ParameterDetailsView_Boolean_Yes");
	                    }
	                    else if (self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_On")) {
	                        self.labelNoBoolean = resourceService.get("ParameterDetailsView_Boolean_Off");
	                        return resourceService.get("ParameterDetailsView_Boolean_On");
	                    }
	                });
	                self.labelNoBoolean = ko.computed(function () {
	                    if (self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_N")) {
	                        self.labelYesBoolean = resourceService.get("ParameterDetailsView_Boolean_Yes");
	                        return resourceService.get("ParameterDetailsView_Boolean_No");
	                    }
	                    else if (self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_Off")) {
	                        self.labelYesBoolean = resourceService.get("ParameterDetailsView_Boolean_On");
	                        return resourceService.get("ParameterDetailsView_Boolean_Off");
	                    }
	                });
	                self.valueBoolean = ko.computed({
	                    read: function () {
	                        if (self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_Y") || self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_On")) {
	                            return resourceService.get("ParameterDetailsView_Boolean_Yes");
	                        } else if (self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_N") || self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_Off")) {
	                            return resourceService.get("ParameterDetailsView_Boolean_No");
	                        }
	                    },
	                    write: function (value) {
	                        if (value === resourceService.get("ParameterDetailsView_Boolean_Yes")) {
	                            if (self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_N")) {
	                                self.parameter.Value = resourceService.get("ParameterDetailsView_Boolean_Y");
	                            }
	                            else if (self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_Off")) {
	                                self.parameter.Value = resourceService.get("ParameterDetailsView_Boolean_On");
	                            }
	                        }
	                        if (value === resourceService.get("ParameterDetailsView_Boolean_No")) {
	                            if (self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_Y")) {
	                                self.parameter.Value = resourceService.get("ParameterDetailsView_Boolean_N");
	                            }
	                            else if (self.parameter.Value() === resourceService.get("ParameterDetailsView_Boolean_On")) {
	                                self.parameter.Value = resourceService.get("ParameterDetailsView_Boolean_Off");
	                            }
	                        }
	                    }
	                });
	                return true;
	            } else {
	                return false;
	            }
	        });
	        self.canShowErrors = ko.observable(false);
	        self.notValidField = function (boundField, action) {
	        	return boundField.isModified() && !boundField.isValid() && self.canShowErrors() && action == self.action();
	        };

	    	
	        self.valMsg = "!"; //resourceService.get('ComDefaultValidationMessage');

	        self.validationGroup = ko.validation.group(self.parameter);
	        promise.resolve(true);
	        return promise;
	    };

	    var hasModifiedParameter = function (CopiedParameter, OriginalParameter) {

	        return ko.utils.hasChanges(CopiedParameter, OriginalParameter);
	    };

	    var attachValidation = function (parameter) {
	        if (parameter) {
	            parameter.Value.extend({
	                validNzDate: {
	                    message: resourceService.get('MessageCode_Validation_Date_001').format(resourceService.get('ParameterDetailsView_ParameterValue_Label')),
	                    onlyIf: function () { return parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_Date") && parameter.Value(); }
	                },
	                nzDate: {
	                    message: resourceService.get('MessageCode_Validation_InvalidDate_305').format(resourceService.get('ParameterDetailsView_ParameterValue_Label')),
	                    onlyIf: function () { return parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_Date") && parameter.Value(); }
	                },
	                validNzDateTime: {
	                    message: resourceService.get('MessageCode_Validation_DateFormat_084').format(resourceService.get('ParameterDetailsView_ParameterValue_Label')),
	                    onlyIf: function () { return parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_DateTime") && parameter.Value(); }
	                },
	                nzDateTime: {
	                    message: resourceService.get('MessageCode_Validation_ValueNotAValidDateTime_085').format(resourceService.get('ParameterDetailsView_ParameterValue_Label')),
	                    onlyIf: function () { return parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_DateTime") && parameter.Value(); }
	                },
	                isDecimalNumeric: {
	                    params: '^[0-9]+(?:\.[0-9]{1,5})?$',
	                    message: resourceService.get("MessageCode_Validation_Numeric_003").format(resourceService.get('ParameterDetailsView_ParameterValue_Label')),
	                    onlyIf: function () { return parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_Numeric"); }
	                },
	                validURL: {
	                    message: resourceService.get("MessageCode_Validation_ValueNotAValidURL_087").format(resourceService.get('ParameterDetailsView_ParameterValue_Label')),
	                    onlyIf: function () { return parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_URL"); }
	                },
	                validEmailAddress: {
	                    message: resourceService.resources.get("MessageCode_Validation_Email_004")(),
	                    onlyIf: function () { return parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_Email"); }
	                }
	            });
	        };
	    };

	    //On Clicking Save button - Update Parameter Value
	    CustomModal.prototype.onSave = function () {
	        var self = this;
	        self.canShowErrors(true);

	        if (self.validationGroup().length > 0) {
	            return false;
	        }

	        if (self.parameter.Datatype() === resourceService.get("ParameterDetailsView_Valuetype_String")) {
	            self.parameter.Value = self.parameter.Value().trim();
	        }

	        parameterService.updateParameter(self.parameter).done(function (result) {
	            if (result.Messages.length > 0) {
	                ko.utils.arrayForEach(result.Messages, function (item) {
	                    logger.logError(item.PresentationAttributes.Text, null, null, true);
	                });
	                return false;
	            };

	            var parameterSaved = function () {
	                self.canDeactivate = null;
	                var saveSuccessMessage = resourceService.get("MessageCode_Confirmation_ParameterUpdated_083");
	                logger.log(saveSuccessMessage, null, null, true);
	            };

	            return parameterSaved();
	        });
	        dialog.close(this, this.parameterDetails());
	        return true;
	    };

	    //On Clicking Cancel button
	    CustomModal.prototype.onCancel = function () {
	        var self = this;
	        self.canShowErrors(false);
	        if (hasModifiedParameter(self.copiedParameter, self.parameter)) {
	            var confirmMessage = resourceService.get("MessageCode_Confirmation_NavigateAway_007");
	            app.showMessage('', confirmMessage, [resourceService.get("ParameterDetailsView_Boolean_Yes"), resourceService.get("ParameterDetailsView_Boolean_No")])
	                .then(function (result) {
	                    if (result === resourceService.get("ParameterDetailsView_Boolean_Yes")) {
	                    	self.parameter.Value(self.copiedParameter.Value());
	                    	dialog.close(self);
	                    }
	                       
	                    return true;
	                });
	        } else {
	            dialog.close(self);
	            return true;
	        }
	    };

	    // Dialog
	    CustomModal.show = function () {
	        var newModal = new CustomModal();

	        newModal.init();

	        return dialog.show(newModal);
	    };

	    CustomModal.prototype.onClose = function () {
	        var confirmMessage = resourceService.get("MessageCode_Confirmation_NavigateAway_007");
	        app.showMessage('', confirmMessage, [resourceService.get("ParameterDetailsView_Boolean_Yes"), resourceService.get("ParameterDetailsView_Boolean_No")])
                .then(function (result) {
                    if (result === resourceService.get("ParameterDetailsView_Boolean_Yes")) {
                    	self.parameter.Value(self.copiedParameter.Value());
                    	dialog.close(self);
                    }
                       
                });
	    };

	    return CustomModal;

	});