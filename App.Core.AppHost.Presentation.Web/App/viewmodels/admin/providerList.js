﻿define(['plugins/router',
		'durandal/app',
		'services/logger',
		'services/resourceService',
		'services/referenceService',
		'services/providerService',
		'services/helpTextService',
		'entities/providerEntity'

],
    function (router, app, logger, resourceService, referenceService, providerService, helpTextService, provider) {

    	//Comments regarding Code:
    	//* Separate Method Definitions by at least one space.
    	//* Create methods definitions using object name, not self. Reserve self statement
    	//  for getting around thorny 'this' syntax conditions, not method definitinos.
    	//* prefer using full class definitions, so that init methods can invoke methods
    	//  without having to define them higher on the page (code spa

    	var viewModel = function () {

    		var self = this;

    		// -------------------
    		// -- Lifecycle events
    		self.activate = function () {
    			app.on("providerList:reset").then(function () {
    				self.resetView();
    				$.placeholder.shim(); // Refresh - Damn IE

    			});

    		};

    		self.compositionComplete = function () {
    			// Load data to table
    			//$("#ProviderListTable").dataTable().fnDraw();

    			// Enable show/hide button
    			uiHelper.hider();
    			$.placeholder.shim();

    			//Finds the first input element which isn't a button and not disabled to focus
    			uiHelper.focusFirstInputElement();
    		};


    		// -------------
    		// -- Properties

    		self.providerCode = ko.observable("");
    		self.providerName = ko.observable("");
    		self.doesResultHasAnyErrors = ko.observable(false);
    		self.hasDoneSearch = ko.observable(false);
    		//self.providerCodeResults = ko.observableArray();
    		self.providerRequestResults = ko.observableArray();
    		//self.searchByProviderCode = ko.observable(false);
    		//Make a local reference to global Resources dictionary cache:
    		//Use this to bind to from the ViewModel using syntax similar to
    		//data-bind="text: resources.get("XYZ")"
    		self.resources = resourceService.resources;
    		self.provider = provider;
    		self.resetView = function () {
    			self.providerCode("");
    			self.providerName("");
    			self.savedSearch.ProviderCode = "";
    			self.onProviderNameSearch();
    			//self.nsnInput.isModified(false); // Reset 'required field' validator
    			//self.showSuccessMsg(false);
    		};

    		//Make a local reference to global Reference dictionary cache:
    		//Use this to bind to from the ViewModel using syntax similar to
    		//data-bind="text: referenceData.get("XYZ")"
    		self.referenceData = referenceService.referenceDictionary;

    		//self.mergeRequestResults = ko.observableArray();

    		self.savedSearch = {
    			ProviderName: self.providerName()
    		};

    		self.hasPageLoaded = ko.observable(false);

    		self.isRunningProviderCodeSearch = ko.observable(false);
    		self.isRunningProviderNameSearch = ko.observable(false);

		    self.onClear = function() {
			    self.resetView();
		    };

    		// ----------
    		// -- Actions

    		self.triggerTableUpdate = function () {
    			var dt = $("#ProviderListTable").dataTable();
    			if(dt != null && dt.length > 0) {
    				dt.fnDraw();
    			}
    		};

    		self.onViewProviderClick = function (boundProvider) {
    			self.findProviderByID(boundProvider.ProviderCode);
    		};

    		self.onProviderCodeSearch = function () {

    			//self.searchByProviderCode = true;
    			if (!self.checkValid(self.staticErrors) || self.providerCode().trim() == "") {
    				return;
    			}


    			//self.savedSearch = {
    			//	ProviderCode: self.providerCode()
    			//};

    			self.isRunningProviderCodeSearch(true);
    			self.findProviderByID(self.providerCode()).always(function () {
    				self.isRunningProviderCodeSearch(false); // Disable spinner
    			});;
    		};

    		self.findProviderByID = function (providerCode) {
    			return providerService.getProviderByCode(providerCode).done(function (data) {
    				if (data !== null) {
    					router.navigate('/admin/provider/details/' + providerCode);
    				} else {
    					logger.logError(resourceService.get("MessageCode_Error_NoProviderExists_060"), "", "providerList", true);
    				}
    			});
    		};

    		self.onProviderNameSearch = function () {
    			//self.searchByProviderCode = false;
    			if (!self.checkValid(self.staticErrors)) {
    				//|| self.providerName().trim() == "") {
    				return;
    			}

    			self.savedSearch = {
    				ProviderName: self.providerName()
    			};

    			self.isRunningProviderNameSearch(true); // Display loading spinner
    			self.triggerTableUpdate();
    			//self.noResultsMessage()();
    			self.shouldDisplayNoResultsMessage();

    		};

    		var processProviderRequestResults = function (result) {
    			// Check for failure messages
    			if (!result.Success && result.Messages.length > 0) {
    				var message = result.Messages[0].PresentationAttributes.Text;

    				logger.logError(message, null, "providerList", true);
    				self.doesResultHasAnyErrors(true);
    				self.providerRequestResults.removeAll();
    				//return;
    			} else {
    				// Update results
    				self.doesResultHasAnyErrors = false;
    				self.providerRequestResults(result.Data);
    			}
    		};


    		// -------------
    		// -- Validation
    		self.staticErrors = ko.validation.group([
    			self.providerCode,
    			self.providerName
    		], { observable: true });

    		self.refreshErrors = ko.observableArray();

    		self.providerCode.extend({

    			validProviderCode: {
    				message: resourceService.get("MessageCode_Validation_InvalidProviderCode_339"),
    				onlyIf: function () { return self.providerCode(); }
    			}


    		});

    		self.providerName.extend({
    			// Currently no validation required
    		});


    		// Validation - Check if the current valdiation group is 
    		self.checkValid = function (validationGroup) {
    			validationGroup.showAllMessages();

    			if (validationGroup().length !== 0) {

    				// Manual clear & populate with *non-observable* validation messages
    				var updatedErrors = new Array();
    				ko.utils.arrayForEach(validationGroup(), function (validationMessage) {
    					updatedErrors.push(ko.utils.unwrapObservable(validationMessage));
    				});
    				self.refreshErrors(updatedErrors); // Move errors to display list

    				$.placeholder.shim(); // Refresh - Damn IE
    				return false;
    			} else {
    				self.refreshErrors.removeAll();

    				$.placeholder.shim(); // Refresh - Damn IE
    				return true;
    			}
    		};

    		// --------------------
    		// -- DataTables config
    		// Search results table config
    		self.providerTableConfig = {
    			dataSource: function (sortInfo, callback) {

    				providerService.searchProviderRequests(self.savedSearch, sortInfo).done(function (result) {
    					// Convert to the format DataTables wants
    					var datatableResult = {
    						Data: result.Data,
    						TotalRecords: result.TotalCount,
    						TotalDisplayRecords: result.PageSize
    					};

    					processProviderRequestResults(result);

    					callback(datatableResult);

    					if (!self.hasPageLoaded()) {
    						self.hasPageLoaded(true);
    					}
    				}).always(function () {
    					// Disable all spinners
    					self.isRunningProviderCodeSearch(false);
    					self.isRunningProviderNameSearch(false);
    					self.hasDoneSearch(true);
    				});

    			},
    			rowTemplate: 'ProviderListRowTemplate',
    			columns: [
			        { bSortable: false, mDataProp: 'ProviderCode' } // Note: The plugin currently insists that mDataProp be set, even though it's not relevant
			        , { bSortable: false, mDataProp: 'ProviderName' }
			        , { bSortable: false, mDataProp: 'NSILiveDate' }
			        , { bSortable: false, mDataProp: 'FileFormatForChangeNotifications' }
			        , { bSortable: false, mDataProp: 'UseMacrons' }
			        , { bSortable: false, mDataProp: 'ManualMergeRequired' }
			        , { bSortable: false, mDataProp: 'Interfaces' }
			        , { bSortable: false, mDataProp: 'OnBehalf' }
			        , { bSortable: false, mDataProp: 'SPRDuration' }
			        , { bSortable: false, mDataProp: 'ReceiveChangeNotifications' }
    			],
    			options: {
    				// Initial sorting - ProviderName - Ascending alphabetically
    				//SORTING IS DONE AT THE BACK END ASC BY PROVIDER NAME!!
    				"aaSorting": [[1, 'asc']],

    				//Gridview length
    				"iDisplayLength": 20,
    				"iRecordsDisplay": 20,

    				//Overriding text templates
    				"oLanguage": {
    					"sInfo": "<span>" + "Displaying Providers  " + " <strong>_START_ - _END_</strong> of <strong>_TOTAL_</strong></span>",
    					"sInfoFiltered": "",// " of <strong>_MAX_</strong></span>", // Split 'max' output to this
    					"sInfoEmpty": ""
    				},
    				//customising DOM
    				"sDom": 'r<"paging" ip>t<"paging" ip>'
    			}

    		};

    		//self.noResultsMessage = resourceService.resources.get("MessageCode_General_NoSearchResultsFound_785")();

    		//self.shouldDisplayNoResultsMessageForProviderCode = ko.computed(function () {
    		//	return (self.providerRequestResults().length == 0 && self.hasPageLoaded() && self.doesResultHasAnyErrors == false && self.providerCode() == null  && (self.providerName() != undefined || self.providerName() != ''));
    		//}, this);

    		self.shouldDisplayNoResultsMessage = ko.computed(function () {
    			return (self.providerRequestResults().length == 0 && self.hasPageLoaded() && self.doesResultHasAnyErrors == false);
    		}, this);

    		self.noResultsMessage = (resourceService.resources.get("MessageCode_General_NoSearchResultsFound_785")());

    		self.resultsConfig = uiHelper.makeDatatableConfig(self.providerTableConfig);

    	};

    	return viewModel;




    });