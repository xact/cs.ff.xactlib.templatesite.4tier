//SessionService
define(['services/logger',
		'repositories/mergeRepository',
		//'repositories/studentRepository',
		'singletons/mergeRequest',
		'singletons/mergeDecision',
        'data/enums',
        'services/referenceService',
		'singletons/student',
        'services/resourceService'],
		function (logger, mergeRepository, mergeRequest, mergeDecision, enums, referenceService, currentStudent, resourceService) {

			var service = function () {
			};
			
			var referenceService = referenceService.referenceDictionary;
			self.resources = resourceService.resources;

			//#region Merge Status

			service.prototype.searchMergeStatuses = function (criteria, sortInfo) {
				return mergeRepository.searchMergeStatuses(criteria, sortInfo).then(processsearchMergeStatusesResults);
			};

			//TODO: PLEASE MOVE THE LOGIC TO FRONT TIER MAPPING AND PASS THE VALUE AS STRING
			var processsearchMergeStatusesResults = function (result) {
				$.each(result.Data, function (index, item) {
					// Create compount fields
					item.RequestedBySummary = item.CreatedByUser + " " + item.ProviderName + " on " + item.CreatedOn.asDisplayDate();
				});

				return result;
			};

			//#endregion

			// -- Merge requests
			service.prototype.submitCurrentMergeRequest = function () {
				var studentIDs = mergeRequest.allSelectedStudentIDs();

				return mergeRepository.submitMergeRequest(studentIDs);
			};

			service.prototype.searchMergeRequests = function (criteria, sortInfo) {

				return mergeRepository.searchMergeRequests(criteria, sortInfo).then(processSearchMergeRequestsResults);
			};

			var processSearchMergeRequestsResults = function (result) {
				$.each(result.Data, function (index, item) {
					// Create compount fields
					item.CreatedBySummary = item.CreatedByUser + ", " + item.CreatedByProvider + " on " + item.CreatedOn.asDisplayDate();

					// Alter each MergeDetails record
					$.each(item.MergeDetails, function (mdIndex, mergeDetail) {
					    mergeDetail.RecordCreatedBySummary = mergeDetail.RecordCreatedByUser + " " + mergeDetail.RecordCreatedByProvider + " on " + mergeDetail.RecordCreatedOn.asDisplayDate();
					});

					// Convert these 2 properties to observables
					item.IsAssignedToMe = ko.observable(item.IsAssignedToMe);
					item.AssignedToUser = ko.observable(item.AssignedToUser);

					// Datatables insists on having access to the sorting properties, so copy them over from the first MergeDetails row

					var firstDetail = item.MergeDetails[0];
					item.Nsn = firstDetail.Nsn;
					item.FamilyName = firstDetail.FamilyName;
					item.GivenNames = firstDetail.GivenNames;
					item.BirthDate = firstDetail.BirthDate;
					item.ResidentialStatus = firstDetail.ResidentialStatus;
					item.RecordCreatedOn = firstDetail.RecordCreatedOn;

				});

				return result;
			};

			service.prototype.setMergeRequestAssignment = function (mergeRequestId, setAssignment) {
				return mergeRepository.setMergeRequestAssignment(mergeRequestId, setAssignment);
			};

			service.prototype.checkNSNIncludedInMergeRequest = function (nsn) {
				return mergeRepository.checkNSNIncludedInMergeRequest(nsn);
			};

			service.prototype.discardMergeRequest = function (mergeRequestId) {
				return mergeRepository.discardMergeRequest(mergeRequestId);
			};

			service.prototype.denyMergeRequest = function (mergeRequestId, mergeDenialReason) {
				return mergeRepository.denyMergeRequest(mergeRequestId, mergeDenialReason);
			};

			service.prototype.submitManualInterventionResult = function (mergeRequestId, selectedNsns, fieldSelections) {
				return mergeRepository.submitManualInterventionResult(mergeRequestId, selectedNsns, fieldSelections);
			};

			/** Add student to merge request
			*/
			service.prototype.addStudentToMergeRequest = function (nsn) {

				return mergeRepository.getStudentSummary(nsn).done(function (response) {
					if (response.Messages) {
						logger.logError("x: " + response.Messages);
					}

					if (response.Data) {
						mergeRequest.addStudent(response.Data);
					}
				});
			};

			// Check student to merge #MB
			service.prototype.hasUnprocessedRequestsByStudents = function (nsn) {
				return mergeRepository.getStudentSummary(nsn);
			};

			// -- Merge denied pairs

			/** 
			*/
			service.prototype.getMergeDeniedPairs = function (nsns) {
				return mergeRepository.getMergeDeniedPairs(nsns);
			};

			/** Create merge denied pair
			*/
			service.prototype.createMergeDeniedPair = function (studentId1, studentId2, reasonId) {
				return mergeRepository.createMergeDeniedPair(studentId1, studentId2, reasonId);
			};

			/** Delete merge denied pair
			*/
			service.prototype.deleteMergeDeniedPair = function (mergeDeniedPairId) {
				return mergeRepository.deleteMergeDeniedPair(mergeDeniedPairId);
			};

			// -- Unmerge

			service.prototype.unmergeStudent = function (slaveStudentId, mergeDenialReason) {
				var promise = $.Deferred();

				mergeRepository.unmergeStudent(slaveStudentId, mergeDenialReason).done(function (updatedStudent) {

					if (!updatedStudent) {
						promise.reject();
						return;
					}

					// Map updated result to current student
					ko.mapping.fromJS(updatedStudent, null, currentStudent);
					promise.resolve(true);
				});

				return promise.promise();
			};

			// -- Merge decision page

			service.prototype.loadMergeDecisionPage = function (mergeRequestId) {

//				if (mergeRequestId === mergeDecision.mergeRequestId) {
//					var promise = $.Deferred();
//					promise.resolve();
//					return promise.promise();
//				}

				return mergeRepository.getMergeDecisionPage(mergeRequestId).done(function (result) {

					// -- Set up extra fields for view model

					$.each(result.MergeRequest.MergeDetails, function (index, item) {
						item.isSelectedForMerge = ko.observable(true);
					});

					// Transfer 'enabled fields' to matching row-level items
					var enabledFields = result.ManualInterventionDetails.EnabledFields;

					$.each(result.MergeRequest.MergeDetails, function (index, item) {
						item.enableName = enabledFields.NameStudentId.indexOf(item.StudentId) >= 0;
						item.enableResidentialStatus = enabledFields.ResidentialStatusStudentId.indexOf(item.StudentId) >= 0;
					});

					// Create summary text
					$.each(result.MergeRequest.MergeDetails, function (index, item) {
						item.studentStatusSummary = item.StudentStatus + " (" + item.StudentStatusReason + ")";
					});

					

					// Set all data
					mergeDecision.mergeRequestId = mergeRequestId;

					mergeDecision.altNames(result.AltNames);
					mergeDecision.mergeDeniedPairs(result.MergeDeniedPairs);
					mergeDecision.mergeRequest(result.MergeRequest);
					mergeDecision.mergeDetails(result.MergeRequest.MergeDetails);
					mergeDecision.manualInterventionMessages(result.ManualInterventionMessages);
					mergeDecision.isNameAndDobLocked(result.ManualInterventionDetails.IsNameAndDobLocked);

					// Set default selections

					var defaults = result.ManualInterventionDetails.Defaults;
					mergeDecision.selections.masterStudentId(defaults.MasterStudentId);
					mergeDecision.selections.familyName(defaults.NameStudentId);
					mergeDecision.selections.givenName1(defaults.NameStudentId);
					mergeDecision.selections.givenName2(defaults.NameStudentId);
					mergeDecision.selections.givenName3(defaults.NameStudentId);
					mergeDecision.selections.birthDate(defaults.NameStudentId);
					mergeDecision.selections.gender(defaults.GenderStudentId);
					mergeDecision.selections.residentialStatus(defaults.ResidentialStatusStudentId);

					return true;
				});
			};

			service.prototype.getMergeRequestDenied = function(fromDate) {
				return mergeRepository.getMergeRequestDenied(fromDate);
			};

			return new service();
		}
);