﻿define(['plugins/router',
		'services/sessionService',
		'data/routes'],
	function(router, sessionService, routes) {

		/** Why is this class complex?
			We have to mix Durandal's flat routing scheme, and our intended display schame: 2-tier menus, including 
			'header stubs' - Items that default to their first available parent.

			There are 2 main sets of routes: The one we define in data/routes, and the one we pull back out of 
			router.routes, which has the calculated properties - namely: hash, isActive
		**/
		
		var service = function() {
			
		};

		/** Gets visible routes, intended for navigation display
			Includes 'header stub' nodes
		**/
		service.prototype.getVisibleRoutes = function () {

			var self = this;

			// The router's copy of the navigation - features isActive observables & actual hashes
			var navModel = router.routes; // All registered routes, don't use navigationModel();

			// Filter original nav list down to just the parent nodes
			var parentNodes = ko.utils.arrayFilter(routes.navRoutes, function (routeInfo) {
				// Only return first-level nodes (they don't have 'parent' defined)

				return !routeInfo.parent;
			});

			// Sort out child routes, default links, etc
			$.each(parentNodes, function (index, parentRoute) {

				// Re-find all child nodes from the nav model
				var allRouteChildren = ko.utils.arrayFilter(navModel, function(routeInfo) {
					if (!routeInfo.parent
						|| !routeInfo.parent.rootPath
						|| !parentRoute.rootPath) {
						return false;
					}
					
					return routeInfo.parent.rootPath === parentRoute.rootPath;
				});

				var visibleChildren = self.findVisibleRoutesFor(allRouteChildren);

				// Override childRoutes with just the visible ones
				parentRoute.visibleChildRoutes = visibleChildren;

				// Re-find the parent
				var matchedParentNavRoute = ko.utils.arrayFirst(navModel, function(item) {
					return parentRoute.route === item.route;
				});
				
				// Copy actual route properties to the display route
				if (matchedParentNavRoute) {
					parentRoute.hash = matchedParentNavRoute.hash;
					parentRoute.isActive = matchedParentNavRoute.isActive;
				} else if (!parentRoute.route || parentRoute.isDisplayShell) {
					// Set the navigation hash to the first available child
					parentRoute.hash = (parentRoute.visibleChildRoutes && parentRoute.visibleChildRoutes.length > 0
							? parentRoute.visibleChildRoutes[0].hash
							: "#");

					// TODO: Remove before go-live
					if (parentRoute.childRoutes && parentRoute.childRoutes.length > 0
						&& parentRoute.childRoutes[0].notYetImplemented) {
						parentRoute.notYetImplemented = parentRoute.childRoutes[0].notYetImplemented;
					}

				} else {
					// Failure fallback
					parentRoute.isActive = ko.observable(false);
					parentRoute.hash = "#";
				}

				// Fallback - ensure the isActive property is available
				if (!matchedParentNavRoute) {
					parentRoute.isActive = ko.observable(false);
				}

				// -- Add property to parent nodes to determine if they have active children
				attachHasActiveChildProperty(parentRoute, allRouteChildren);
				
			});
			
			return self.findVisibleRoutesFor(parentNodesSecurityTrimmed(parentNodes));
		};

		// Attach 'hasActiveChild' property (recursively), used for nav display. 
		var attachHasActiveChildProperty = function(parentRoute, allRouteChildren) {
			parentRoute.hasActiveChild = ko.computed(function() {
				return searchForActiveChild(parentRoute, allRouteChildren);
			});

			// Recurse, giving child properties the 'hasActiveChild' property
			if (parentRoute.childRoutes
				&& parentRoute.childRoutes.length > 0) { // Check for empty arrays
				ko.utils.arrayForEach(allRouteChildren, function(item) {
					attachHasActiveChildProperty(item, item.childRoutes);
				});
			}
		};

		// Calculates if the current item has active children, recursively
		var searchForActiveChild = function(parentRoute, allRouteChildren) {
			if (!parentRoute.childRoutes) { // Ignore empty child sets
				return false;
			}

			// Search all children for an active route
			var hasActiveChild = false;
			ko.utils.arrayForEach(allRouteChildren, function(childRoute) {
				if (childRoute.isActive()) {
					hasActiveChild = true;
				}

				if (childRoute.childRoutes) {
					var search = searchForActiveChild(childRoute, childRoute.childRoutes);
					if (search) {
						hasActiveChild = true;
					}
				}
			});

			return hasActiveChild;
		};

		// Trim back out parent routes with 
		var parentNodesSecurityTrimmed = function(parentNodes) {
			return ko.utils.arrayFilter(parentNodes, function(parentNode) {
				if (parent.settings && parent.settings.authorise
					&& !sessionService.isAuthorised(parent.settings.authorise)) {

					return false;
				};

				// Also trim out *'display shell'* entries with no child routes
				if (typeof parentNode.isDisplayShell != "undefined"
					&& parentNode.isDisplayShell
					&& parentNode.visibleChildRoutes.length === 0) {
					return false;
				}

				return true;
			});
		};

		/** Find all visible routes for a given array. 
			(Tests for nav: true + auth checks) 
		**/
		service.prototype.findVisibleRoutesFor = function(routeList) {

			var self = this;

			// Filter down to the 
			var result = ko.utils.arrayFilter(routeList, function (routeInfo) {
				// Exclude items set as non-visible ('nav' = 'visible' for Durandal)
				if (!routeInfo.nav) {
					return false; 
				}

				// Include non-authenticated routes
				var routeInfoSettings = routeInfo.settings;
				if (!routeInfoSettings.authorise) {
					return true;
				}

				// Include only items they're authorised to view
				return (sessionService.isAuthorised(routeInfoSettings.authorise));
			});
			
			return result;

		};

		
		/** Gets all routes for binding to the router.
			Ignores 'stub' headers, and flattens the 2-tier list to 1-tier
		**/
		service.prototype.getAllRoutesAsFlatNav = function() {
			// Load routes - Create map that includes the child routes
			var navRoutes = routes.navRoutes;

			navRoutes = addChildNodes(navRoutes);

			// Filter list to only exclude 'stub parent' display nodes with no 'route' set
			navRoutes = ko.utils.arrayFilter(navRoutes, function(routeInfo) {
				return routeInfo.route;
			});

			// Ensure the title (window title) is defaulted to the name property 
			// ('name' is primarily what will appear in the nav tag)
			ko.utils.arrayForEach(navRoutes, function(item) {
				if (!item.title) {
					item.title = item.name;
				}
			});

			return navRoutes;
		};

		var addChildNodes = function(navRoutes) {
			// Add child routes
			$.each(navRoutes, function(routeIndex, route) {
				navRoutes = navRoutes.concat(getAllChildNodes(route));
			});

			return navRoutes;
		};

		var getAllChildNodes = function(route) {
			if (!route.childRoutes) {
				route.childRoutes = null;
				return [];
			}
			var foundSubChildren = [];

			$.each(route.childRoutes, function(childRouteIndex, childRoute) {
				// Ignore these for now - All properties are already set
				//childRoute.route = route.route + '/' + childRoute.route;
				//childRoute.moduleId = route.moduleRootId + '/' + childRoute.moduleId;
				//childRoute.title = route.title + '/' + childRoute.title;
				//childRoute.hash = route.hash + '/' + childRoute.hash;

				childRoute.parent = route; // Use the parent route to mark the child

				// Recursively add sub-child nodes
				if (childRoute.childRoutes) {
					var subChildNodes = getAllChildNodes(childRoute);
					foundSubChildren = foundSubChildren.concat(subChildNodes);
				}
			});

			// Copy child routes into the main route list
			return route.childRoutes.concat(foundSubChildren);
			
		};
	
		return new service();
	});
    