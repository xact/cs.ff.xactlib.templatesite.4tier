define(['durandal/system'],
    function (system) {
        var logger = {
            log: log,
            logError: logError,
            logResponseMessageErrors: logResponseMessageErrors
        };

        return logger;

        function log(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'info');
        }

        function logError(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'error');
        }

		function logResponseMessageErrors(serverResponse, fallbackMessage) {
			
			if (serverResponse.Messages.length === 0) {
				if (fallbackMessage) {
					logError(fallbackMessage, null, null, true);
				}

				return;
			}
			
			for (var i = 0; i < serverResponse.Messages.length; i++) {
				var message = serverResponse.Messages[i];

				if (message.InnerMessages && message.InnerMessages.length === 0) {

					logError(message.PresentationAttributes.Text, null, null, true);

				} else {
					var innerMessages = ko.utils.arrayMap(message.InnerMessages, function(item) {
						return item.PresentationAttributes.Text;
					});

					var innerMessagesText = innerMessages.join("<br/>");

					logError(innerMessagesText, message.PresentationAttributes.Text, null, true);
				}
			}
			

		}


        function logIt(message, data, source, showToast, toastType) {
            source = source ? '[' + source + '] ' : '';
            if (data) {
                system.log(source, message, data);
            } else {
                system.log(source, message);
            }
            if (showToast) {
                if (toastType === 'error') {
                    toastr.error(message, data);
                } else {
                    toastr.info(message, data);
                }

            }

        }
    });