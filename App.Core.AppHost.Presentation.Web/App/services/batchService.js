﻿//ProviderService
define(['services/commService',
	'repositories/batchRepository',
    //'singletons/batch',
    'services/logger'
],
		function (commService, batchRepository,batchRequest, logger) {
			var service = function () { };


			
			service.prototype.getBatchRequests = function (sortInfo,params) {

				return batchRepository.getBatchRequests(sortInfo,params);

			};


			service.prototype.setDownloaded = function (batchId) {
				
				return batchRepository.setDownloaded(batchId);
			};

			return new service();
		}
);