﻿
define(['data/settings', 'singletons/resources', 'services/commService'],
		function (settings, resources, commService) {

		    createNamespace('com.nsi.app.data.settings');
		    var self = this;

		    
		    service = function() {
		        this.cache = null;
		        this.resources = resources;
		        this.init();
		        


		    };

            
		    service.prototype.init = function() {
		        
		        //Could later move to json to be loaded per language:
		        this.cache = {
		            //homePage_Title: 'National Student Index',
		            //homePage_Btn_AccessNSI: 'Access NSI',
		            //homePage_ListOfLinks: [
		            //        { title: "General Information about the NSI and how to connect to it", target: "#" },
		            //        { title: "A student's guide to the NSI", target: "#" },
		            //        { title: "Request a copy of the information held about you on the NSI", target: "#" },
		            //        { title: "Authorised information matching programme", target: "#" }
		            //],
		            shell_Logo_Title: 'Back to NSI home.',
		            shell_Logo_Text: 'Logo: Ministry of Education',
		            nav_LinkText_Home: 'Home',
		            nav_LinkText_Search: 'Search',
		            nav_LinkText_Merge: 'Merge',
		            nav_LinkText_Admin: 'Admin',
		            nav_LinkText_Reports: 'Reports',
		            nav_LinkText_Batch: 'Batch',
		            //welcome_Title: 'National Student Index - Welcome',
		            //welcome: 'Welcome',
		            //welcome_MessageOfTheDay: 'Message of the day',
		            //welcome_MessageOfTheDayBody: '<p>Welcome to the National Student Index.</p><p> If you have concerns or problems using the NSI please contact the Contact Centre. </p> <p>Phone: 0200422599</p> <p>Email: <a href="mailto:contact.centreminedu.govt.nz">contact.centreminedu.govt.nz</a></p><p>To continue, please select an option from the menu bar above.</p>',
		            //myNSI: 'My NSI',
		            //welcome_Warning1: 'Users of the National Student Index applications should be aware of and familiar with the',
		            //welcome_Warning2: 'Past Compulsory Education Unique Identifier Code of Practice 2001',
		            //changePassword: 'Change Password',
		            //welcome_Nav_changeInstitution: 'Change Institution',
		            //welcome_Nav_changeChallengePhase: 'Change Challenge Phase',
		            //welcome_Nav_changeMyDetails: 'Change My Details'
		        };
		        return self;
		    };
			
		    service.prototype.getStringsForView = function (viewName, lang) {
		        return commService.get(
                {
                    operationTitle: "Getting HelpText",
                    url: "uiapi/helpText/" + viewName,
                    onSuccess: function (data) {
                        self.processResults(viewName, data, resources);
                    }
                });
		    };

		    self.processResults = function (viewname, data, resources2) {

		        for (var attrname in data) {

		            var key = viewname + "_" + attrname;
		            var value = data[attrname];
		            //Stick it in the observable dictionary:		            
		            resources2.set(key, value);
		        }
		    };
		    
		    //ShortHand
		    service.prototype.get = function(key, lang) {
		        return this.getString(key, lang);
		    };

		    service.prototype.getByPage = function(page, lang) {
		        return this.getStrings(key, lang);
		    };


		    service.prototype.getString = function(key, lang) {
		        lang = lang || 'en';
		        try {
		            var retVal = this.cache[key];
		            return retVal;
		        } catch(e) {
		            app.services.tracingService.error('could not find helptext with key:' + key);
		        }
		    };

		   

		    service.prototype.getStrings = function(page, lang) {
		        lang = lang || 'en';
		        try {
		            return this.cache;
		        } catch(e) {
		            app.services.tracingService.error('could not find page with key:' + key);
		        }
		    };


		    
		    //-----------------------------------------------------------------
		    //Instantiate as singleton:
		    return new service();
		});

