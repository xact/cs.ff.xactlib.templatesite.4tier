//SessionService
define(['repositories/studentRepository',
		'services/challengeService',
		'singletons/student',
		'singletons/searchResults',
		'singletons/addStudentSearchResults',
		'singletons/studentUpdateHistory',
        'singletons/studentMergeHistory'],
		function (
			studentRepository,
			challengeService,
			currentStudent,
			currentSearchResults,
			addStudentSearchResults,
			studentUpdateHistory,
			studentMergeHistory) {

		    var service = function () { };

		    // Performs a student search, and places the result into the 'searchResults' system singleton
		    service.prototype.doStudentSearch = function (searchCriteria, callback, error) {
		        //results will be an array of `entities/student`

		        //	//Clear the searchResponse before filling up with array results:
		        //	searchResponse.clear();

		        //	//Get and fill array:
		        return studentRepository.searchStudents(searchCriteria).done(function (data) {
		            // Set searchResults system singleton - the View that is bound to it will now refresh.
		            currentSearchResults.update(data.Data);
		        });
		    };

			// Performs an addstudent search, and places the result into the 'addStudentSearchResults' system singleton
		    service.prototype.doAddStudentSearch = function (searchCriteria) {
		    
		    	//	//Get and fill array:
		    	return studentRepository.searchStudents(searchCriteria).done(function (data) {
		    		// Set searchResults system singleton - the View that is bound to it will now refresh.
		    		addStudentSearchResults.update(data.Data);
		    	});
		    };

		    // Sets the app-wide 'student' to the given NSN
		    service.prototype.setCurrentStudentByNSN = function (nsn) {

		    	return loadStudent(nsn, 'getStudentByNSN');

		    };

		    // Sets the app-wide 'student' to the given NSN
		    service.prototype.checkAndSetCurrentStudentByNSN = function (nsn) {

			    return loadStudent(nsn, 'checkStudentByNSN');

		        // Note: Do not remove & consolidate with getAndSet[...].
		    };

			function loadStudent(nsn, repositoryCall) {
				
				//results will be a single `entities/student`
				return studentRepository[repositoryCall](nsn).done(function (data) {
					if (data) {
						// Update the 'current student' singleton
						ko.mapping.fromJS(data, null, currentStudent);

						// Add change tracking to this property
						ko.utils.arrayForEach(currentStudent.Challenges(), function (item) {
							item.ChallengeStatusFK.extend({ trackChange: true });
						});

						
					}

					return true;
				});
			}

			service.prototype.updateStudent = function (student, newAltNames) {

				// Cast to plain object
				var updatedStudent = ko.toJS(student);

				updatedStudent.BirthDate = student.BirthDate()
    					? moment(student.BirthDate()).diff(moment("11/11/1918")) != 0
    						? student.BirthDate().trim()
    						: ""
    					: "";

				var newAltNamesPlain = ko.toJS(newAltNames);
				
				if (newAltNamesPlain.length > 0) {
					updatedStudent.AltNames = updatedStudent.AltNames.concat(newAltNamesPlain);
				}

				//return studentRepository.updateStudent(updatedStudentToSend);
				return studentRepository.updateStudent(updatedStudent);

			};

			service.prototype.addStudent = function (student, altNames) {
				// Cast to plain object
				var plainStudent = ko.toJS(student);

				var altNamesPlain = ko.toJS(altNames);

				plainStudent.AltNames = altNamesPlain;
				

				//return studentRepository.updateStudent(updatedStudentToSend);
				return studentRepository.addStudent(plainStudent);
			};

			// --------------------
			// -- Student histories

		    service.prototype.setStudentUpdateHistory = function (nsn) {
		        return studentRepository.getStudentUpdateHistory(nsn).done(function (data) {
		            // Update the 'studentUpdateHistory' singleton
		            studentUpdateHistory.records(data.Data);
		        });
		    };

			service.prototype.getStudentMergeHistory = function (mergeRequestId) {
		        return studentRepository.getStudentMergeHistory(mergeRequestId);
		    };

		   

		    return new service();
		}
);