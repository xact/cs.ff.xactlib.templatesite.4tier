define([
		'repositories/changeNotificationRepository'
		],


	function (changeNotificationRepository) {

		var service = function () { };

		service.prototype.getChangeNotifications = function (criteria, sortInfo) {
			
			var newCriteria = {
				PageIndex: sortInfo.PageIndex,
				PageSize: sortInfo.RecordsToTake
			};
			
			// Copy over criteria properties
			$.extend(newCriteria, criteria);

			return changeNotificationRepository.getChangeNotifications(newCriteria);
		};

		service.prototype.hasAnyChangeNotifications = function(fromDate) {
			return changeNotificationRepository.hasAnyChangeNotifications(fromDate);
		};

		return new service();
	}
);