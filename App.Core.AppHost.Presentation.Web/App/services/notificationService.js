﻿define(['data/settings'],
    function (settings) {

        var service = function() {

        };

        service.prototype.notifiy = function(message,title) {
            this.info(message,title);
        };


        service.prototype.info = function(message, title) {
            // Display a info toast, with no title
            if (title) {
                toastr.info(message, title);
            } else {
                toastr.info(message);
            }
        };

        service.prototype.warning = function (message, title) {
            if (title) {
                toastr.warning(message,title);
            } else {
                toastr.warning(message);
            }
        };
        
        service.prototype.success = function (message, title) {
            if (title) {
                toastr.success(message, title);
            } else {
                toastr.success(message);
            }
        };
        
        service.prototype.error = function (message, title) {
            if (title) {
                toastr.error(message, title);
            } else {
                toastr.error(message);
            }
        };

        return new service();
    }
);