﻿/// <reference path="commService.js" />
define(['durandal/system',
		'plugins/router',
		'services/resourceService',
        'services/sessionService',
		'singletons/session',
		'data/settings',
		'services/notificationService'
        ],
    function(system, router, resourceService, sessionService, session, settings, notificationService) {

        var service = function () {


            //Configure toastr:
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 5000,
                "extendedTimeOut": 1000
            };

        };

        service.prototype.get = function (config) {
            config.verb = "GET";
            return this.ajax(config);
        };
        
        service.prototype.post = function (config) {
            config.verb = "POST";
            return this.ajax(config);
        };
        service.prototype.put = function (config) {
            config.verb = "PUT";
            return this.ajax(config);
        };
	    
		// This *can't* be called 'delete' - It's a reserved word of sorts in IE < 9 + jQuery.
        service.prototype.doDelete = function (config) {
            config.verb = "DELETE";
            return this.ajax(config);
        };

	    service.prototype.patch = function(config) {
		    config.verb = "PATCH";
		    return this.ajax(config);
	    };


        //VERBS:
        //If a DELETE request succeeds, it can return status 200 (OK) with an entity-body that describes the status; status 202 (Accepted) if the deletion is still pending; or status 204 (No Content)
        
        //example: 'api/v0.1/students/' + id
        service.prototype.ajax = function (configIn) {

            var self = this;
            
            if (!configIn) { throw new Error('Required params: configIn'); }
        	if (!configIn.url) { throw new Error('Required param: url'); }

	        system.log(configIn.operationTitle ? configIn.operationTitle : "Calling: " + configIn.url);

            var config = {
                verb: 'GET',
                url: '',
                data: null,
                onSuccess: null,
                onFailure: null,
                alertOnSuccess: true,
                alertOnFailure: true,
                operationTitle: ''
            };
            
            //Crush config with configIn:
            for (var attrname in configIn) {
                config[attrname] = configIn[attrname];
            }

            //operationTitle, verb, url, onSuccess, json // TODO - Sky

            var completeAction = function (jqXHR, textStatus) {

                try {
			        if (jqXHR.status != 302) {
				        data = jQuery.parseJSON(jqXHR.responseText);
			        }
		        } catch(e) {

		        }

		        switch (jqXHR.status) {
		        case 200 /*OK*/:
			        //VERBS: GET
		            if (config.verb == "GET") {
		                
				        //notificationService.success("Request processed successfully.<br/>HTTP/1.1 200 OK", (config.operationTitle) ? config.operationTitle : "Request received.");
			        } else if (config.verb == "DELETE") {
				        //notificationService.success("Record deleted as requested.<br/>HTTP/1.1 200 OK", (config.operationTitle) ? config.operationTitle : "Request received.");
			        }
			        //Invoke Callback:
			        if (config.onSuccess) {
				        config.onSuccess(data);
			        }
			        break;
		        case 201 /*Created*/:
			        //VERBS: POST, PUT
			        if (config.verb == "POST") {
				        //notificationService.success("Response Received. Record created.", "Http Code: 201");
			        } else if (config.verb == "PUT") {
				        //Guess the record did not exist...so was created.
				        //if there is payloady in response, push data onto ViewModel.
				        //but if not, get location:

				        //notificationService.success("Update accepted.<br/>HTTP/1.1 201 Created", (config.operationTitle) ? config.operationTitle : "Record Created.");

				        //if CORS, see: http://stackoverflow.com/a/14755417/1052767
				        //start a GET request to the new location...
			        }
			        //if there is payloady in response, push data onto ViewModel.
            			//but if not, get location:
			        var redirectionUrl = request.getResponseHeader("Location");
			        //if CORS, see: http://stackoverflow.com/a/14755417/1052767
            			//start a GET request to the new location...

            			//Invoke Callback, passing returned data:
			        if (config.onSuccess) {
				        config.onSuccess(data);
			        }
			        break;
		        case 202 /*Accepted*/:
			        //VERBS: PUT, DELETE
            			//notificationService.success("Response Received. Update accepted.", "Http Code: 202");


            			//if there is payloady in response, push data onto ViewModel.
            			//but if not, get location:
			        var redirectionUrl = request.getResponseHeader("Location");
			        //if CORS, see: http://stackoverflow.com/a/14755417/1052767
            			//start a GET request to the new location...

			        if (config.verb == "DELETE") {
				        //It's accepted -- but potentially pending.
			        }

			        //Invoke Callback, passing returned data:
			        if (config.onSuccess) {
				        config.onSuccess(data);
			        }
			        break;
		        case 302 /**/:
			        //VERBS: POST, PUT
			        var redirectionUrl = request.getResponseHeader("Location");
			        //notificationService.success("Response Received. Redirecting to:<br/>" + redirectionUrl, "Http Code: 302");

            			//OK. Now redirecting to new location as per Location header
            			//Note that 302 shouldn't be the way it's done. 201 is best practices
            			//whereas 302 is more a browser concept.
			        break;
		        case 400 /*BadRequest from Model Validation)*/:
			        //VERBS: POST, PUT
			        var validationResult = responseText;
			        //$validator.unobtrusive.revalidate(form, validationResult);
			        break;
		        case 401 /*Not Authenticated*/:
			        //VERBS: GET, POST, PUT, DELETE
		        	//var loggedOutMessage = resourceService.get("MessageCode_Error_SessionTimedOut_090");
					// TODO: I think I can't access resource service from this infrastructure service? RequireJS may have a cyclic dependency
			        sessionService.logout("090");
            			//Requires authentication. The WWW-Authenticate header should contain details of how to perform the authentication
			        break;
		        case 403 /*Not Authorized*/:
		        	//VERBS: GET, POST, PUT, DELETE
						// Error #529 
            			notificationService.error("Access denied.", ""); 

            			//Requires authorisation. The WWW-Authenticate header should contain details of how to perform the authentication
			        break;
		        case 404 /*Not Found*/:
			        //VERBS: GET, DELETE
            			//notificationService.error("Not Found.", "Http Code: 404/Not Found");
			        break;
		        case 409 /*Not Found*/:
			        //VERBS: DELETE
            			//notificationService.error("Not Found.", "Http Code: 404/Confict");
			        break;
		        case 410 /*Gone*/:
		        //VERBS: GET, DELETE
		        //notificationService.error("Not Found.", "Http Code: 410/Gone");
		        default:
		        //notificationService.error("Not Found.", "Http Code: 410/Gone");
		        } //~switch
	        };//~complete method

	        var headers = {
	        	//May still need to check this...

				// No longer required, due to WebAPI's workings
	            //"Accept": "application/json", //"text/plain; charset=utf-8" 
	            //"Content-Type": 'application/json; charset=utf-8' //"text/plain; charset=utf-8"
	        };
            // In order to prevent CSRF attacks we have a hidden field in the page that needs to be passed to the server
	        var token = $('input[name="__RequestVerificationToken"]').val();
	        headers['__RequestVerificationToken'] = token;
            var configInfo = {
                headers: headers,
                url: config.url,
                type: config.verb,
                cache: false,
            	complete: completeAction,
                error: function (jqXHR, textStatus, errorThrown) {
	                if (config.onFailure) {
		                config.onFailure(jqXHR, textStatus, errorThrown);
	                } else if (config.alertOnFailure) {
		                // TODO: Give detail in debug mode, but only once the exceptions are sane

						// Only relay failure message for server error. Allow 401/403 auth to do their own thing.
	                	if (jqXHR.status !== 401
	                		&& jqXHR.status !== 403) {

	                		// As this is a core part of the system / infrastructure level, we need this hardcoded 
			                var fallbackSystemMessage = "A system fault means that the NSI is temporarily unavailable. If the problem persists contact the Ministry of Education Contact Centre .  Please accept our apologies for any inconvenience.";
			                notificationService.error("", fallbackSystemMessage);
		                }
	                }
                }
            };//~configInfo

            //Invoke method
            if (["POST,PUT"].indexOf(config.verb)) {
                configInfo.data = config.data;
            }
	        
            return $.ajax(configInfo);
        };//~ajax method
        
        


    	//whereas session.isauthenticated() only checks the singleton,
    	//this method calls back service.
    	//if it fails, commservice will redirect user to home page.
    	//if it passes, will invoke callack.
        service.prototype.checkIsAuthenticated = function (callback) {
        	return this.get(
				{
					operationTitle: "Getting All Resources",
					url: "uiapi/SessionCheck/IsStillAlive"
							, onSuccess: function (data) {
								if (callback) { callback(); }
							}
				});
        };




	    
        return new service();
	    
		
    });