﻿
define(['services/commService',
		'services/sessionService',
		'data/enums',
		'data/settings'],
    function (commService, sessionService, enums, settings) {


    	var referenceDictionary = new ko.observableDictionary({});


    	var service = function () {

    		this.referenceDictionary = referenceDictionary;
    	};

    	service.prototype.getSortedReferenceData = function (referenceDataType) {

    		var self = this;

    		return commService.get(
            {
            	operationTitle: "Getting Reference Data: " + referenceDataType,
            	url: "uiapi/ImmutableReferenceData/" + referenceDataType,
            	onSuccess: function (data) {
            		self.processResultsSorted(referenceDataType, data);
            	}
            });
    	};

    	service.prototype.loadAll = function () {
    		var self = this;

    		return commService.get(
            {
            	operationTitle: "Getting all Reference Data: ",
            	url: "uiapi/ImmutableReferenceData/"

            }).done(function (result) {
            	processResultsArray(result);

            	return true;
            });
    	};

    	service.prototype.getReferenceData = function (referenceDataType) {

    		var self = this;

    		return commService.get(
            {
            	operationTitle: "Getting Reference Data: " + referenceDataType,
            	url: "uiapi/ImmutableReferenceData/" + referenceDataType,
            	onSuccess: function (data) {
            		//processResults(referenceDataType, data);
            	}
            });
    	};

    	var processResultsArray = function (data) {
    		var self = this;

    		$.each(data, function (key, item) {
    			processResults(key, item);
    		});
    	};

    	var processResults = function (referenceDataType, data) {
    		//Stick it in the observable dictionary:		            
    		//Use set not this.cache[key] = value, or observable triggers don't work.
    		referenceDictionary.set(referenceDataType, data);

    		var dataWBlank, dataWAll;

    		// Create a corresponding set of ref data, which has a blank value at the start
    		// Same retrieval method - just use '_wBlank' suffix
    		if (data && data.length > 0) {
    			//Adding Blank at the begining of the array
    			dataWBlank = data.slice(0);
    			dataWBlank.unshift({ "Key": "", "Value": "" });

    			//Adding All at the begining of the array -#MB (required for MergeStatus dropdown)
    			dataWAll = data.slice(0);
    			dataWAll.unshift({ "Key": "0", "Value": "All" });

    			//Adding All at the begining of the array -#MB (required for MergeStatus dropdown)
    			dataWPleaseselect = data.slice(0);
    			dataWPleaseselect.unshift({ "Key": "0", "Value": "Please Select" });

				//Adding for Transaction Activity Drop down -#VM
    			dataWAllTransactions = data.slice(0);
    			dataWAllTransactions.unshift({ "Key": "0", "Value": "All transactions" });

    			referenceDictionary.set(referenceDataType + "_wBlank", dataWBlank);
    			referenceDictionary.set(referenceDataType + "_wAll", dataWAll);
    			referenceDictionary.set(referenceDataType + "_wPleaseselect", dataWPleaseselect);
    			referenceDictionary.set(referenceDataType + "_wAlltransactions", dataWAllTransactions);
    		}
    	};


    	service.prototype.processResultsSorted = function (referenceDataType, data) {
    		//Stick it in the observable dictionary:		            
    		//Use set not this.cache[key] = value, or observable triggers don't work.
    		this.referenceDictionary.set(referenceDataType, data);
    		this.referenceDictionary.sort(function (a, b) {
    			return defaultComparison(a.value(), b.value());
    		});
    		var dataWBlank, dataWAll;
    		// Create a corresponding set of ref data, which has a blank value at the start
    		// Same retrieval method - just use '_wBlank' suffix
    		if (data) {
    			//Adding Blank at the begining of the array
    			dataWBlank = data.slice(0);
    			dataWBlank.unshift({ "Key": "", "Value": "" });

    			//Adding All at the begining of the array -#MB (required for MergeStatus dropdown)
    			dataWAll = data.slice(0);
    			dataWAll.unshift({ "Key": "0", "Value": "All" });
    		}
    		this.referenceDictionary.set(referenceDataType + "_wBlank", dataWBlank);
    		this.referenceDictionary.set(referenceDataType + "_wAll", dataWAll);

    	};

    	service.prototype.getAvailableNameDobVerifications = function () {
    		var values = referenceDictionary.get('NameBirthDateVerificationMethod')();

    		if (sessionService.isAuthorised(settings.securityClaims.NSI2_MODIFY_BDM_VERIFIED)) {
    			return values;
    		}

    		return ko.utils.arrayFilter(values, function (item) {
    			// Allow type-coersion (.Key is currently a string...)
    			return item.Key != enums.nameBirthDateVerificationMethod.birthRegister;
    		});
    	};

    	service.prototype.getKeyByValue = function (referenceDataType, val) {
    		var dict = referenceDictionary.get(referenceDataType)();
    		for (var prop in dict) {
    			if (dict.hasOwnProperty(prop)) {
    				if (dict[prop].Value == val)
    					return dict[prop].Key;
    			}
    		}
    		return "";
    	};

    	service.prototype.getValue = function (referenceDataType, val) {
    		var dict = referenceDictionary.get(referenceDataType)();
    		for (var prop in dict) {
    			if (dict.hasOwnProperty(prop)) {
    				if (dict[prop].Key == val)
    					return dict[prop].Value;
    			}
    		}
    		return "";
    	};

    	//service.prototype.getAvailableNameDobVerificationsForNewItem = function () {
    	//	var values = referenceDictionary.get('NameBirthDateVerificationMethod')();

    	//	return ko.utils.arrayFilter(values, function (item) {
    	//		// Allow type-coersion (.Key is currently a string...)
    	//		return item.Key != enums.nameBirthDateVerificationMethod.birthRegister;
    	//	});
    	//};

    	return new service();
    });