﻿
define(['durandal/system',
		'repositories/studentProviderRelationshipRepository'],
	function (system, sprRepository) {

		var service = function() {
		};

		service.prototype.getSPRs = function (studentId) {

			return sprRepository.getSPRs(studentId);

		};

		service.prototype.getSprCounts = function(studentIds) {

			return sprRepository.getSprCounts(studentIds);
		};

		service.prototype.addSPR = function(studentId, providerCode) {
			return sprRepository.addSPR(studentId, providerCode);
		};

		service.prototype.updateSPRs = function(sprs) {
			var combinedUpdatePromise;

			ko.utils.arrayForEach(sprs, function(updatedSpr) {

				var updatePromise = sprRepository.updateSPR(updatedSpr);


				combinedUpdatePromise = !combinedUpdatePromise ? updatePromise : combinedUpdatePromise.done(function () {
					return updatePromise;
				});
			});

			return combinedUpdatePromise;
		};

		return new service();
	});
