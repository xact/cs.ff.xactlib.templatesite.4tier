﻿
define(['data/settings', 'services/notificationService'],
		function (settings, notificationService) {

		    var self = this;

		    var service = function () {
		        var self = this;

		        self._initialized = false;
		        self._hub = null;

		        self.callbacks = new {};

		        self.initialize();

		    };

		    service.prototpe.registerCallback = function(name, callback) {
		        this.callbacks[name] = callback;
		    };

		    //Initialize the connection to the server hub
		    service.prototype.initialize = function () {
		        var me = this;
		        //as named in attribute on MessageHub.cs
		        me._hub = $.connection.messageHub;

		        //Wire up callbacks that the server can call
		        //from within the servershide MessageHub.cs:
		        me._hub.client.sendMessage = me.sendMessage;
		        me._hub.client.sendSignal = me.sendSignal;


		        $.connection.hub.start().done(function (me) {
		                
		            me._initialized = true;
		        });
		    };
		    



            //A method that you can invoke...
		    service.prototype.broadcastMessage = function (sender, message) {
		        //don't know what will happen if sending while _initialized still is false...
		        //worth a try...
		        //if (!this._initialized) { return;}
		        
		        //Send message to server, for it to broadcast to clients:
		        _hub.server.sendMessage(sender, message);
		    };
		    
		    //A method that you can invoke...
		    service.prototype.broadcastSignal = function (type, data) {
		        //don't know what will happen if sending while _initialized still is false...
		        //worth a try...
		        //if (!this._initialized) { return;}

		        //Send message to server, for it to broadcast to clients:
		        _hub.server.sendSignal(type, data);

		    };



		    //The method that the server invokes:
		    service.prototype.sendMessage = function (sender, message) {
		        //Toast the message or something....
		        notificationService.info("Signal",message);
		    };


            //The method that the server invokes:
		    service.prototype.sendSignal = function (type, data) {
		        //Depending on type...invoke some callback...

		        //Invoke method in callback:
		        //Cleanup required is ...check for callback before firing it...
		        //this.callbacks[type]();
		    };
		    
		    var r = new service();
		    //r.initialize();
		    return r;
		}
);