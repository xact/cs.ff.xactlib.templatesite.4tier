//ProviderService
define(['services/commService',
	'repositories/providerRepository',
    'singletons/provider',
    'services/logger'
],
		function (commService, providerRepository,currentProvider, logger) {

			var service = function () { };
			
		    service.prototype.getProviderByCode = function (providerCode) {
		    	var self = this;
		        return commService.get({
		            operationTitle: "Getting Provider with code: " + providerCode,
		            url: "uiapi/Provider/?id=" + providerCode
		        }).done(function (data) {
		            if (currentProvider) {
		            	ko.mapping.fromJS(data, null, currentProvider);


		            } else {
		                logger.logError('asd' + providerCode);
		            }

		        });
		    };


		    service.prototype.searchProviderRequests = function (criteria, sortInfo) {

			    return providerRepository.searchProviderRequests(criteria, sortInfo);
			    
		    };

		    service.prototype.updateProvider = function (provider) {

		    	//var updatedProvider = ko.toJS(provider);

				return providerRepository.updateProvider(provider);
			};

		 

		    return new service();
		}
);