﻿define(['services/logger',
		'data/settings',
		'singletons/resources',
		'services/commService'],

		function (logger, settings, resources, commService) {

		    createNamespace('com.nsi.app.data.settings');

		    //var self = this;


		    var service = function () {
		    	this.hardcodedCache = null;
		        this.resources = resources;

		    	//this.init(); // Now called on load, as deferred
			    this.initHardCoded();
		    };

			service.prototype.init = function() {


				return commService.get(
					{
						operationTitle: "Getting All Resources",
						url: "uiapi/ResourceSet"
						,onSuccess: function(data) {
							self.processMultipleViewResults(views, data);
						}
					});

			};
			
			service.prototype.initHardCoded = function() {
			//Could later move to json to be loaded per language:
		        this.hardcodedCache = {
		            
		            //General
		            resYes: 'Yes',
		            resNo: 'No',
		            ComDefaultValidationMessage: '!',

		            //SearchPage
		            searchResultGridTitle_Score: 'Score',
		            searchResultGridTitle_NSN: 'NSN',
		            searchResultGridTitle_Familyname: 'Family name',
		            searchResultGridTitle_Givenname: 'Given name(s)',
		            searchResultGridTitle_Birthdate: 'Birth date',
		            searchResultGridTitle_Gender: 'Gender',
		            searchResultGridTitle_ResidentialStatus: 'Residential status',
		            searchResultGridTitle_CreatedBy: 'Record created by',
		            searchResultGrid_MasterRecord: 'Master Record',
		            searchResultGrid_MasterRecordTooltip: 'This record has slave records.',
		            searchResultGrid_AltName: 'alternative name',
		            searchResultGrid_AltNameTooltip: 'This record is an alternative name record.',
		            searchResultGrid_View: 'View',
		            searchResultGrid_ViewTooltip: 'View this student.',
		            searchResultGrid_Merge: 'Merge',
		            searchResultGrid_MergeTooltip: 'Merge this record. ',

		            searchResultGrid_IconLink: 'Master with slaves',
		            searchResultGrid_IconAltName: 'Alternative name',
		            searchResultGrid_IconView: 'View',
		            searchResultGrid_IconMerge: 'Merge',
		            searchResultGrid_IconRemove: 'Remove',
		            searchResultGrid_RemoveTooltip: 'Remove',

		            searchResultGrid_Key: 'Key',

		            searchResultGrid_Paging_Results: 'Displaying search results',
		            //footer_NzGovtLink: '<a href="http://www.newzealand.govt.nz" title="newzealand.govt.nz - connecting you to New Zealand central &amp; local government services" target="_blank" class="logo">Logo: New Zealand Government</a>',
		            //searchResultGrid_ResultsHeader: 'Search results',
		            //searchResultNoResults: "No search results found",


		            //viewStudent page
		            viewStudent_Title: "Student Details",
		            viewStudent_AltNamesTitle: "Alternative name(s)",

		            viewStudent_GridFamilyName: "Family name",
		            viewStudent_GridGivenName: "Given name(s)",
		            viewStudent_GridVerification: "Verification",
		            viewStudent_GridPreferredName: "Set to preferred name",
		            viewStudent_GridAltInfo: "Displaying alternative name(s)",
		            viewStudent_GridNoAltNameResults: "No alternative names",
		            
		            viewStudent_ChallengeChangeTitle: "Challenge",
		            viewStudent_GridStatus: "Status",
		            viewStudent_GridDate: "Date",
		            viewStudent_GridRaisedBy: "Raised by",
		            viewStudent_GridChallengeInfo: "Displaying challenge change(s)",
		            viewStudent_CurrentVersion: "Current",
		            viewStudent_ModifiedOn: "Modified on",
		            viewStudent_NSN: "NSN",
		            viewStudent_FamilyName: "Family name",
		            viewStudent_GivenNames: "Given name(s)",
		            viewStudent_PreferredName: "Set as preferred name",
		            viewStudent_BirthDate: "Birth date",
		            viewStudent_NameandBirthDateVerification: "Name & birth date verification",
		            viewStudent_ResidentialStatus: "Residential status",
		            viewStudent_ResidentalStatusVerification: "Residential status verification",
		            viewStudent_Gender: "Gender",
		            viewStudent_RecordStatus: "Record status",
		            viewStudent_DateOfDeath: "Date of death",
		            viewStudent_AssociatedSlaveRecords: "Associated slave record(s)",
		            viewStudent_RecordCreatedBy: "Record created by",
		            viewStudent_RecordCreatedOn: "on",
		            viewStudent_RecordUpdateHistory: "Record update history",
		            


		            //StudentHistory page
		            studentHistory_Title: "Student History",
		            studentHistory_CurrentVersion: "Current",
		            studentHistory_ModifiedOn: "Modified on",
		            studentHistory_NSN: "NSN",
		            studentHistory_FamilyName: "Family name",
		            studentHistory_GivenNames: "Given name(s)",
		            studentHistory_PreferredName: "Set as preferred name",
		            studentHistory_BirthDate: "Birth date",
		            studentHistory_NameandBirthDateVerification: "Name & birth date verification",
		            studentHistory_ResidentialStatus: "Residential status",
		            studentHistory_ResidentalStatusVerification: "Residential status verification",
		            studentHistory_Gender: "Gender",
		            studentHistory_RecordStatus: "Record status",
		            studentHistory_DateOfDeath: "Date of death",
		            studentHistory_AssociatedSlaveRecords: "Associated slave record(s)",
		            studentHistory_RecordCreatedBy: "Record created by",
		            studentHistory_RecordCreatedOn: "on",
		            studentHistory_RecordUpdateHistory: "Record update history",
		        	studentHistory_EmptyRecordUpdateHistory: "No updates have been applied to this record",

		            studentHistoryModal_Title: "View History",
		            studentHistoryModal_Version: "Version",
		        	studentHistoryModal_UpdateReason: "Update Reason",
		            studentHistoryModal_ModifiedDate: "Modified date",
		            studentHistoryModal_ModifiedBy: "Modified by",
		            studentHistoryModal_MasterNSN: "Master NSN",
		            studentHistoryModal_Name: "Name",
		            studentHistoryModal_BirthDate: "Birth date",
		            studentHistoryModal_NameandBirthDateVerification: "Name & birth date verification",
		            studentHistoryModal_Altnames: "Alt. name(s)",
		            studentHistoryModal_ResidentalStatus: "Residential status",
		            studentHistoryModal_ResidentalStatusVerification: "Residential status verification",
		            studentHistoryModal_Gender: "Gender",
		            studentHistoryModal_RecordStatus: "Record status",
		            studentHistoryModal_DateOfDeath: "Date of death",
		            
		            studentProviderRelationshipsModal_Title: "Student-Provider Relationships",
		            studentProviderRelationshipsModal_ProviderName: "Provider name",
		            studentProviderRelationshipsModal_DateCreated: "Date created",
		            studentProviderRelationshipsModal_UntilDate: "Until date",
		            studentProviderRelationshipsModal_GridRelationshipInfo: "Displaying relationship(s)",
		            studentProviderRelationshipsModal_NoRelationships: "No Relationships"

		        };
		        return self;
		    };

		    //ShortHand
		    service.prototype.get = function (key, lang) {
		        return this.getString(key, lang);
		    };

		    service.prototype.getString = function (key, lang) {
		        lang = lang || 'en';
		    	try {
		    		// First try to get it from the resource set from the server
		    		// NOTE: Be sure to evalute the result '()' - A bug (?) in the dictionary will replace the value with '0' otherwise
		    		var fromResources = self.resources.get(key)();
		    		
		    		
		    		if (fromResources) {
		    			return fromResources;
		    		}
		    		
					// Fallback - Old cache values. These will be refactored out at some point
		    		var fromHardCodedCache = this.hardcodedCache[key];
		    		if (fromHardCodedCache) {
		    			logger.log("Resource still in hardcoded cache: '" + key + "'");

		    			return fromHardCodedCache;
		    		}

					// Log to make it easier to find missing items
		    		logger.log("Resource not found: '" + key + "'");

		    		return "(Unknown)"; // Temp: See how this pans out as a debugging feature
		    	} catch (e) {
		            logger.logError('Could not find resource with key:' + key);
		        }
		    };

		    service.prototype.processResults = function (viewname, data) {
		        
		        for (var attrname in data) {
		            
		            var key = viewname + "_" + attrname;
		            var value = data[attrname];
                    //Stick it in the observable dictionary:		            
		            this.resources.set(key, value);
		            
		            //For now, duplicate:
		            //this.cache[key] = value;
		        }
		    };
		    
		    var views = [
			    "WelcomeView",
			    "HomeView",
			    "ShellView",
			    "StudentSearchView",
			    "MessageCode",
			    "FooterView",
		        "HeaderSessionInfo",
			    "SearchResultsGridView",
			    "StudentDetailsView",
			    "StudentHistoryView",
			    "StudentHistoryModalView",
				"MergeEntryView",
				"MergeDenialReasonModalView",
				"MergeListView",
		        "MergeDecisionView",
				"MergeHistoryView",
				"MergeStatusView",
                "AddStudentView",
				"SearchRecordsToMergeView",
		        "CHCHView",
                "MergeDeniedPairWidgetView",
				"ModifyStudentView",
				"ProviderListView",
				"ProviderDetailsView",
				"ParametersView",
				"ParameterDetailsView",
				"ParameterMessagesView",
				"BatchRequestsView",
				"TransactionActivityReportView",
			    "StudentProviderRelationshipsView",
				"BatchResultsView"
				
		        
				//"StudentProviderRelationshipsModalView"
		    ];
			
		    service.prototype.processMultipleViewResults = function(viewNames, data) {
		        for (var inx = 0; inx < viewNames.length ; inx++) {
		            var view = viewNames[inx];
		            // Data is Dictionary<string, Dictionary<string,string> - we use the view name as the key
		            // and pass the resulting dictionary to the processResults function: ActualData = Dictionary<string, string>
		            var actualData = data[view];
		            // We have the view - now populate
		            this.processResults(view, actualData);
		        }
		    };
		    
		    //-----------------------------------------------------------------
			//Instantiate as singleton:

			var self = new service();
		    return self;
		});

