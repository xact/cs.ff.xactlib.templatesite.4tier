﻿//ProviderService
define(['services/commService',
	'repositories/transactionRepository',
    'singletons/provider',
    'services/logger'
],
		function (commService, transactionRepository, currentProvider, logger) {

			var service = function () { };

			service.prototype.getTransactionResults = function (params) {

				return transactionRepository.getTransactionResults(params);
			};

			
			return new service();
		}
);