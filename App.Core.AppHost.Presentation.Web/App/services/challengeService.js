define([
		'repositories/challengeRepository',
		'singletons/student'],


	function (challengeRepository, currentStudent) {

		var service = function () { };

		service.prototype.setChallengeChangeForCurrentStudent = function (nsn) {
			return challengeRepository.getChallengeChangeForStudent(nsn).done(function(data) {
				currentStudent.Challenges(data);
			});
		};

		service.prototype.submitChallengeChange = function (nsn, changeType, reason, emailaddress) {
			return challengeRepository.submitChallengeChange(nsn, changeType, reason, emailaddress);
		};

		service.prototype.updateChallengeChangesForStudent = function(challenges) {

			// Filter down to only the updated statuses that are marked as having changed
			var updatedChallengeChanges = ko.utils.arrayFilter(challenges(), function (item) {
				return item.ChallengeStatusFK.isDirty();
			});

			// String together successive promises to save the data sequentially
			var updateChallengesPromise = null;
			ko.utils.arrayForEach(updatedChallengeChanges, function(item) {
				var promise = challengeRepository.updateChallengeChange(item);

				updateChallengesPromise = updateChallengesPromise ? updateChallengesPromise.done(promise) : promise;
			});

			return updateChallengesPromise;
		};

		service.prototype.updateChallengeChange = function(challenge) {
			return challengeRepository.updateChallengeChange(challenge);
		};

		return new service();
	}
);