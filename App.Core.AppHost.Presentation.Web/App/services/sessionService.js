//SessionService
define([
		'repositories/sessionRepository',
		'singletons/session',
		'plugins/router',
		'services/logger'],
		function (
			sessionRepository, session, router, logger) {
			var self = this;

			var serviceBase = "uiapi/Session";

			var service = function () {
				//this.isAuthenticated = ko.observable(false);
			};

			service.prototype.getSession = function () {
				
				return session;
		    };

		    service.prototype.clearSession = function() {

		        return session.reset();
		    };

		    service.prototype.isAuthenticated = function () {

		    	return session.user().IsAuthenticated;
		    };

			// Loads the details of the currently authenticated user, if there is one
			service.prototype.loadSession = function() {
				var promise = $.get(serviceBase)
					.done(function (data) {
						// Note: data can be null, which is correct - updateUser will handle it correctly
						session.updateUser(data);
					});

				return promise;

			};

			service.prototype.logout = function (messageCode) {
			    var messageCodeParameters = (messageCode) ? "?MessageCode=" + messageCode : "";
				var promise =
					$.ajax(serviceBase, {type: 'DELETE'})
						.done(function() {
							session.reset();
						    //router.navigate("/SignOut.ashx"); // Back to the home page
							window.location = "/Nsi/HttpHandlers/SignOff.ashx" + messageCodeParameters;
						})
						.fail(function() {
						    session.reset();
						    //router.navigate("/SignOut.ashx"); // Back to the home page
						    window.location = "/Nsi/HttpHandlers/SignOff.ashx" + messageCodeParameters;
						    //logger.logError("Unable to logout", null, null, true);
						});
				return promise;
			};


			
			/**
		 * Check if an user is in a role
		 * @method
		 * @param {array} roles - Array of roles
		 * @return {bool}
		*/
			service.prototype.isAuthorised = function(requestedClaim) {

				// Argument check - Assume this is an error on their part, and deny authorisation
				if (!requestedClaim) {
					return false;
				}
				
				//var self = this,
				var isUserAuthorised = false;
				
				if (requestedClaim.length === 0) {
					return true; // For empty sets, consider the user authorised
				}

				var usersClaims = session.user().Claims;
				
				if (!usersClaims) {
					return false;
				}


				// Parsing - Assuming a comma separated list of claims
				if (usersClaims.length === 1 && usersClaims[0].indexOf(",") != -1) {
					usersClaims = usersClaims[0];

					// If a single role is specified
					if (typeof requestedClaim === 'string') {
						if (usersClaims && usersClaims.indexOf(requestedClaim) != -1) {
							isUserAuthorised = true;
						}
					} else {
						// If an array of roles is specified
						for (var i = 0; i < requestedClaim.length; i++) {
							if (usersClaims && usersClaims.indexOf(requestedClaim[i]) != -1) {
								isUserAuthorised = true;
							}
						}
					}
				} else {

					// -- Parsing - Assuming a array of user claims

					// If a single role is specified
					if (typeof requestedClaim === 'string') {
						if (usersClaims && usersClaims.indexOf(requestedClaim) != -1) {
							isUserAuthorised = true;
						}
					} else {
						// If an array of roles is specified
						for (var j = 0; j < requestedClaim.length; j++) {
							if (usersClaims && usersClaims.indexOf(requestedClaim[j]) != -1) {
								isUserAuthorised = true;
							}
						}
					}
				}
				
				return isUserAuthorised;
			};

			return new service();
		}
);