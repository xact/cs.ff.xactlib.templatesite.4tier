﻿/// <reference path="commService.js" />
define(['singletons/parameters',
	'services/commService',
	'repositories/parameterRepository'],
    
    function (parameters, commService, parameterRepository) {

        var service = function() {

            this.parameters = parameters;

            this.init();

        };

        service.prototype.init = function () {
        	// This can be refined later - at the moment it gets all the Parameter data
        	//NSIPRJ-2198: Removed esaa_url_context 
        	//return this.getAllParameters('MOTD,MOTD2,MOTD3,esaa_url_challenge,esaa_url_context,esaa_url_details,esaa_url_password,LINK_1,LINK_2,LINK_3,LINK_4,Challenge_Change_Text,Birth_date_edit_period,SSRS_landing_page');
            return this.getAllParameters('MOTD,MOTD2,MOTD3,esaa_url_challenge,esaa_url_details,esaa_url_password,LINK_1,LINK_2,LINK_3,LINK_4,Challenge_Change_Text,Birth_date_edit_period,SSRS_landing_page,Upload_Status_Text2,DirectDbLookup');
        };



        // Used to retrieve individual parameter values.
        service.prototype.getParameterData = function (parameterKey) {

            var self = this;

            return commService.get(
            {
                operationTitle: "Getting Parameter: " + parameterKey,
                url: "uiapi/parameter/"  + parameterKey,
                onSuccess: function (data) {
                    self.processResults(parameterKey, data);
                }
            });
        };


        service.prototype.getAllParameterSettings = function (id) {

        id = (id == null) ? 0 : id;
		        
        	return parameterRepository.getAllParameterSettings(id);

        };
        
        // Gets all the parameter data by calling parameterset controller
        service.prototype.getAllParameters = function(requiredParameters) {
            var self = this;
            return commService.get(
            {
            	operationTitle: "Getting All Parameters: " + requiredParameters,
            	url: "uiapi/parameterset/" + requiredParameters,
                onSuccess: function (data) {
                    self.processMultipleResults(data);
                }
            });
        };
        
        // Adds to Parameters ObservableDictionary
        service.prototype.processResults = function (parameterKey, data) {
            //Stick it in the observable dictionary:		            
            //Use set not this.cache[key] = value, or observable triggers don't work.
            this.parameters.set(parameterKey, data);
        };

        // Loops through the returned dictionary and add data to the parameters observable dictionary.
        service.prototype.processMultipleResults = function(data) {
            for (var key in data) {
                this.processResults(key, data[key]);
            }
        };

        service.prototype.processMultipleResultsforList = function (data) {
        	for (var key in data) {
        		this.processResults(key, data[key]);
        	}
        };

        service.prototype.updateParameter = function (updatedParameter) {
            return commService.put({
                operationTitle: "Updating Parameter",
                url: "uiapi/ParameterSet/",
                data: updatedParameter
            });
        };

        return new service();
    });

