﻿using System.Web;

namespace App.Core.API.Legacy.Single.POX
{
    public class TestHttpHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Output.Write("Yeah...");
            context.Response.StatusCode = 200;
        }
    }
}