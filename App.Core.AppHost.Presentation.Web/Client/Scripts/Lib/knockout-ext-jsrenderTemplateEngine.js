﻿ko.jsrenderTemplateEngine = function () { };

ko.jsrenderTemplateEngine.prototype = ko.utils.extend(new ko.templateEngine(), {
    renderTemplateSource: function (templateSource, bindingContext, options) {
        // Precompile the wrapping div for templating
        var precompiled = templateSource['data']('precompiled');
        if (!precompiled) {
            precompiled = $('<div>', { html: templateSource.text() });
            templateSource['data']('precompiled', precompiled);
        }
        // Unwrap observables
        var unwrapped = ko.mapping.toJS(bindingContext.$data);
        // Render and parseHTMLFragment
        return ko.utils.parseHtmlFragment(precompiled.render(unwrapped));
    }
});


ko.setTemplateEngine(new ko.jsrenderTemplateEngine());

