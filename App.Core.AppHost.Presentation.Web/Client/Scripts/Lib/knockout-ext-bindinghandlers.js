﻿//Dependencies:
//Nuget: moment.js


//A custom knockout binding:
//usage: <input ... data-bind="datevalue:MyModelsDate"/>
ko.bindingHandlers.datevalue = {
    init: function (element, valueAccessor, allBindingsAccessor) {

        // Use the value binding
        ko.bindingHandlers.value.init(element, valueAccessor, allBindingsAccessor);

        // Provide a custom text value
        var value = valueAccessor();
        var allBindings = allBindingsAccessor();
        var dateFormat = allBindingsAccessor.dateFormat || "D/M/YYYY";
        var strDate = ko.utils.unwrapObservable(value);


        if (strDate) {
            var date = moment(strDate).format(dateFormat);
            $(element).val(date);
        }

        $(element).datepicker({ formatDate: dateFormat ,  altFormat: "dd-mm-yyyy" });
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        // Use the value binding
        ko.bindingHandlers.value.update(element, valueAccessor, allBindingsAccessor);

        // Provide a custom text value
        var value = valueAccessor(), allBindings = allBindingsAccessor();
        var dateFormat = allBindingsAccessor.dateFormat || "D/M/YYYY";
        var strDate = ko.utils.unwrapObservable(value);
        if (strDate) {
            var date = moment(strDate).format(dateFormat);
            $(element).val(date);
        }
    }
};




//A custom knockout binding:
//usage: <input ... data-bind="dollarvalue:MyModelsDate"/>
ko.bindingHandlers.dollarvalue = {
    init: function (element, valueAccessor, allBindingsAccessor) {

        var underlyingObservable = valueAccessor();


        var interceptor = ko.computed({
            read: function () {
                // this function does get called, but it's return value is not used as the value of the textbox.
                // the raw value from the underlyingObservable, or the actual value the user entered is used instead, no   
                // dollar sign added. It seems like this read function is completely useless, and isn't used at all
                return "$" + underlyingObservable();
            },

            write: function (newValue) {
                var current = underlyingObservable(),
                    valueToWrite = Math.round(parseFloat(newValue.replace("$", "")) * 100) / 100;

                if (valueToWrite !== current) {
                    // for some reason, if a user enters 20.00000 for example, the value written to the observable
                    // is 20, but the original value they entered (20.00000) is still shown in the text box.
                    underlyingObservable(valueToWrite);
                } else {
                    if (newValue !== current.toString())
                        underlyingObservable.valueHasMutated();
                }
            }
        });

        ko.applyBindingsToNode(element, { value: interceptor });
    }
};


