﻿xact.namespace("app.views.defs");

app.views.defs.Search = function () {
    var self = this;
    self.searchViewModel = null;
};


app.views.defs.Search.prototype.init = function (simpleModel) {
    var self = this;
    //$(".datetime").datepicker();

    //var simpleModel = @Html.Raw(new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(Model));

    //Add more var on json'ed model that came in from server:
    simpleModel.statusMsg = [];
    simpleModel.resultRecords = [];
    simpleModel.pageSize = 10;
    simpleModel.pageIndex = 0;

    //Use the Mapping Extension to make it into an observable mmodel:
    //viewModel.DOB2// =  ko.observable(simpleModel.DOB).extend({ datevalue: 0 }); 
    var vm = self.searchViewModel = ko.mapping.fromJS(simpleModel);

    //Add some more values (such as dropdown Options from the ViewBag):
    //var t = @Html.Raw(Newtonsoft.Json.JsonConvert.SerializeObject(ViewBag.GenderOptions));
    //vm.GenderOptions = ko.observableArray(@Html.Raw(Newtonsoft.Json.JsonConvert.SerializeObject(ViewBag.GenderOptions)));


    //Paging methods:
    vm.previousPage = function () {
        var self = this;
        self.pageIndex(self.pageIndex() - 1);
    };

    vm.nextPage = function () {
        var self = this;
        self.pageIndex(self.pageIndex() + 1);
    };

    //Paging vars:
    vm.maxPageIndex = ko.dependentObservable(function () {
        var self = this;
        return Math.ceil(self.resultRecords().length / self.pageSize()) - 1;
    },
        vm);

    vm.pagedRecords = ko.dependentObservable(function () {
        var self = this;
        var size = self.pageSize();
        var start = self.pageIndex() * size;
        var result = self.resultRecords.slice(start, start + size);

        //alert(result.length);

        return result;
    }, vm);


    vm.search = function () {
        app.views.search.search();
        //Cancel submit:
        return false;
    };



};


app.views.defs.Search.prototype.search = function () {
    var self = this;

    //Post:
    var url = "/Student/Search";

    var t = new Date(self.searchViewModel.DOB()).toISOString();
    //var dt = moment(t, 'D/M/YYYY');

    //Create a postback object:
    var postbackData = {
        UseNSN: self.searchViewModel.UseNSN(),
        NSN: self.searchViewModel.NSN(),
        Name: self.searchViewModel.Name(),
        DOB: t,
        Gender: self.searchViewModel.Gender()
    };

    //Clear error messages in the form:
    $(':input').removeClass('validation-error');


    $.ajax({
        type: 'POST',
        url: url,
        cache: false,
        data: postbackData,
        //contentType:'application/json; charset=utf-8',
        statusCode: {
            200: function (data, textStatus) { self.processResponse(data); }
        },
        error: function (jqXHR, textStatus, errorThrown) { alert('damn!'); }
    });

};

app.views.defs.Search.prototype.processResponse = function (result) {
    var self = this;

    alert("Back:" + result.StatusCode);

    //if (!result.StatusCode!=200) {
    //    searchViewModel.statusMsg(result.Message);
    //}else{
    //    alert('good');

    self.searchViewModel.statusMsg(result.StatusMessage);


    //as posted, set fields as unreadable:
    //searchviewmodel.editfields(false);
    //searchviewmodel.readonlymode(true);
    //}

    //set exceptions:
    for (var val in result.validationerrors) {
        var element = "#" + val;
        $(element).addclass('validation-error');
    }


    //Use method to replace observable elements:

    self.searchViewModel.resultRecords.removeAll();
    self.searchViewModel.resultRecords(result.List);
    self.searchViewModel.pageIndex(0);
};
