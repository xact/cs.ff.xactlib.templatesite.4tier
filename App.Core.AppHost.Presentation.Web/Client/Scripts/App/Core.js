﻿
var namespace = function() {
    var ln = arguments.length,
        i, value, split, x, xln, parts, object;

    for (i = 0; i < ln; i++) {
        value = arguments[i];
        parts = value.split(".");
        object = (window.Ext)
             ? window[parts[0]] = Object(window[parts[0]])
               : arguments.callee.caller.arguments[0];
        for (x = 1, xln = parts.length; x < xln; x++) {
            object = object[parts[x]] = Object(object[parts[x]]);
        }
    }
    return object;
}   


namespace(
    'com.moe.nsi.ui.views.defs',
    'com.moe.nsi.ui.controllers.defs',
    'com.moe.nsi.ui.models.defs');




