﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=169433
  -->
<configuration>
  <configSections>
    <!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->
    <sectionGroup name="elmah">
      <section name="security" requirePermission="false" type="Elmah.SecuritySectionHandler, Elmah" />
      <section name="errorLog" requirePermission="false" type="Elmah.ErrorLogSectionHandler, Elmah" />
      <section name="errorMail" requirePermission="false" type="Elmah.ErrorMailSectionHandler, Elmah" />
      <section name="errorFilter" requirePermission="false" type="Elmah.ErrorFilterSectionHandler, Elmah" />
    </sectionGroup>
    
    <section name="Federation" type="dk.nita.saml20.config.ConfigurationReader, dk.nita.saml20" />
    <section name="SAML20Federation" type="dk.nita.saml20.config.ConfigurationReader, dk.nita.saml20" />
  <sectionGroup name="dotNetOpenAuth" type="DotNetOpenAuth.Configuration.DotNetOpenAuthSection, DotNetOpenAuth.Core">

    <section name="oauth" type="DotNetOpenAuth.Configuration.OAuthElement, DotNetOpenAuth.OAuth" requirePermission="false" allowLocation="true" />
    <section name="openid" type="DotNetOpenAuth.Configuration.OpenIdElement, DotNetOpenAuth.OpenId" requirePermission="false" allowLocation="true" /><section name="messaging" type="DotNetOpenAuth.Configuration.MessagingElement, DotNetOpenAuth.Core" requirePermission="false" allowLocation="true" /><section name="reporting" type="DotNetOpenAuth.Configuration.ReportingElement, DotNetOpenAuth.Core" requirePermission="false" allowLocation="true" /></sectionGroup><section name="glimpse" type="Glimpse.Core.Configuration.Section, Glimpse.Core" /></configSections>

  <!--<entityFramework>
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.LocalDbConnectionFactory, EntityFramework">
      <parameters>
        <parameter value="v11.0" />
      </parameters>
    </defaultConnectionFactory>
    <providers>
      <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer" />
    </providers>
  </entityFramework>-->

  <appSettings>
    <add key="webpages:Version" value="3.0.0.0" />
    <add key="webpages:Enabled" value="false" />
    <add key="PreserveLoginUrl" value="true" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
    <add key="GlimpseAllowMachines" value="APP," />
    <add key="GlimpseAllowRoles" value="Administrator,Developer,Whatever" />

    <add key="EnvironmentIdentifier" value="" />
    
    <!-- Controls where NConfig looks for dev config files -->
    <!-- As per the latest url changes before UAT, there is no sub virtual directory called "APP" in front-->
    <add key="VDir" value="" />

<!-- with terminating slash -->
    <add key="BackTier" value="http://localhost:19991/" />
    
    <!-- Name of Application specific EventLog. Matches NLog tracing Name given in nlog.config -->
    <add key="EventLogSourceNames" value="APP" />
    <!-- AllowDefaultTraceListenerInDebugMode is a WellKnown key, used by EnsureDefaultTracerWhenInDebugModeInitializationStep
         but as it is soooooo slow, put it only in your Config/MachineSpecificSettings file-->
    <add key="EnsureDefaultTraceListenerIsActiveInDebugMode" value="false" />

    <!-- Version determines which Endpoint is used for OAuth -->
    <add key="Services/OAuth/ESAA/Version" value="1" />
    
    <add key="Services/OAuth/ESAA/V1/Endpoint" value="https://lsi.security.ORG" />
    <add key="Services/OAuth/ESAA/V1/Scope" value="APP_NSI2" />

    <add key="Services/OAuth/ESAA/V2/Endpoint" value="NOT_SET" />
    <add key="Services/OAuth/ESAA/V2/Scope" value="APP_NSI2" />

    <!-- For Owin Startup sequence -->
    <add key="owin:appStartup" value="App.Core.AppHost.Initialization.OWinStartup" />
  </appSettings>

  <elmah>
    <!--
			 See http://code.google.com/p/elmah/wiki/SecuringErrorLogPages for 
			 more information on remote access and securing ELMAH.
			-->
    <security allowRemoteAccess="false" />
    <errorLog type="Elmah.XmlFileErrorLog, Elmah" logPath="~/App_Data/Logs-Elmah" size="200" />
  </elmah>
  
  
    <Federation xmlns="urn:dk.nita.saml20.configuration">
    <SigningCertificate findValue="CN=MOE.APP.DEV" storeLocation="LocalMachine" storeName="My" x509FindType="FindBySubjectDistinguishedName" />
    <AllowedAudienceUris>
      <!--<Audience>This Service Provider's ID</Audience>-->
      <Audience>https://localhost/APP</Audience>
    </AllowedAudienceUris>
    <Actions>
      <!-- Do Clear, and re-add what you want, rather than Delete+Add (or else Redirect will be still before the new SetPrincipal) -->
      <clear />
      <add name="SetSamlPrincipal" type="App.Core.AppHost.HttpModules.SetAppSamlPrincipal, App.Core.Front.AppHost.Web" />
      <add name="NsiRedirect" type="App.Core.AppHost.HttpModules.NsiRedirectAction, App.Core.Front.AppHost.Web" />			
    </Actions>
    </Federation>
  
  <!-- Although in most cases I recommend Config files be organised Alphabetically
       Federation and SAML20Federeation are tightly coupled. Less errors keeping 
       them side by side -->
  <SAML20Federation xmlns="urn:dk.nita.saml20.configuration">
    <!-- may not be set to TRUE in production -->
    <ShowError>false</ShowError>
    <!--<ServiceProvider id="https://[YourServerName]/[VirtualPath]" server="https://[YourServerName]:[YourServerPort]">-->
    <ServiceProvider id="https://localhost/APP" server="https://localhost:44300/APP/">
      <!-- WITHOUT PREFIX SLASH (to align with FormHandler url that cannot have prefix slash either) -->
      <ServiceEndpoint localpath="signon.ashx" type="signon" redirectUrl="/#/welcome" index="0" />
      <ServiceEndpoint localpath="signoff.ashx" type="logout" redirectUrl="/" index="1" />
      <ServiceEndpoint localpath="metadata.ashx" type="metadata" />
      <md:ContactPerson contactType="administrative" xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata">
        <md:Company>MOE/APP</md:Company>
        <md:GivenName>Sky</md:GivenName>
        <md:SurName>Sigal</md:SurName>
        <md:EmailAddress>sky.sigal@minedu.govt.nz</md:EmailAddress>
        <md:TelephoneNumber>021 159 6440</md:TelephoneNumber>
      </md:ContactPerson>
    </ServiceProvider>
    <RequestedAttributes>
      <!-- 
      The attributes that the demonstration identity provider issues. 
			<att name="urn:FirstName" isRequired="true"/>
			<att name="urn:LastName" isRequired="true"/>
			<att name="urn:Age"/>
      -->
    </RequestedAttributes>
    <!-- 
    Specify the format of the NameId sent back, either as
    urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName (DEV)
    urn:oasis:names:tc:SAML:2.0:nameid-format:transient (LSI)
    -->
    <!--<NameIdFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName</NameIdFormat>-->
    <NameIdFormat>urn:oasis:names:tc:SAML:2.0:nameid-format:transient</NameIdFormat>
    <!--<IDPEndPoints metadata="[Your App Configuration Directory]\metadata\">-->
    <IDPEndPoints metadata="Config\SamlMetadata\IdP\DEV-LSI2\">
      <!--<add id="[Your IdP Entity Id]">-->
      <!-- will be either https://development.security.ORG/opensso or https://lsi.security.ORG/opensso-->
      <add id="https://lsi.security.ORG/opensso">
        <CertificateValidation>
          <add type="dk.nita.saml20.Specification.SelfIssuedCertificateSpecification, dk.nita.saml20" />
        </CertificateValidation>
      </add>
    </IDPEndPoints>
    <!--<CommonDomain enabled="false" localReaderEndpoint="https://[YourServerName]:[YourServerPort]/[VirtualPath]/cdcreader.ashx"/>-->
    <!--<CommonDomain enabled="false" localReaderEndpoint="https://localhost:44300/HttpHandlers/cdcreader.ashx"/>-->
  </SAML20Federation>
  <glimpse defaultRuntimePolicy="On" endpointBaseUri="~/Glimpse.axd">
    <runtimePolicies>
      <ignoredTypes>
        <!-- Want to use Glimpse on a remote server? Ignore the LocalPolicy by removing this comment. -->
        <add type="Glimpse.AspNet.Policy.LocalPolicy, Glimpse.AspNet" />
      </ignoredTypes>
    </runtimePolicies>
    <!-- 
          For more information on how to configure Glimpse, please visit http://getglimpse.com/Help/Configuration
          or access {your site}/Glimpse.axd for even more details and a Configuration Tool to support you. 
      --></glimpse>
  
    <location path="elmah.axd" inheritInChildApplications="false">
    <system.web>
      <httpHandlers>
        <add verb="POST,GET,HEAD" path="elmah.axd" type="Elmah.ErrorLogPageFactory, Elmah" />
      </httpHandlers>
      <!-- 
        See http://code.google.com/p/elmah/wiki/SecuringErrorLogPages for 
        more information on using ASP.NET authorization securing ELMAH.

      <authorization>
        <allow roles="admin"/>
        <deny users="*"/>  
      </authorization>
      -->
    </system.web>
      
    <system.webServer>
      
      <handlers accessPolicy="Read, Execute, Script">
				<add name="ELMAH" verb="POST,GET,HEAD" path="elmah.axd" type="Elmah.ErrorLogPageFactory, Elmah" preCondition="integratedMode" />
      
				<!--<remove name="AboMapperCustom-22698" />
				<add name="SOAP Toolkit" path="*.wsdl" verb="*" modules="IsapiModule" scriptProcessor="C:\Program Files (x86)\Common Files\MSSoap\Binaries\SOAPISAP.dll" 
						 resourceType="Unspecified" requireAccess="Execute" preCondition="bitness32" />-->
			</handlers>
    </system.webServer>
  </location>
  
  <runtime>
    
    
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Antlr3.Runtime" publicKeyToken="eb42632606e9261f" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.5.0.2" newVersion="3.5.0.2" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="DotNetOpenAuth.AspNet" publicKeyToken="2780ccd10d57b246" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.3.0.0" newVersion="4.3.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="DotNetOpenAuth.Core" publicKeyToken="2780ccd10d57b246" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.3.0.0" newVersion="4.3.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.Edm" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.5.0.0" newVersion="5.5.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.OData" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.6.0.0" newVersion="5.6.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.0.1.0" newVersion="3.0.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin.Security" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.0.1.0" newVersion="3.0.1.0" />
      </dependentAssembly>
      <!--<dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.Unity.Configuration" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>-->
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.ServiceLocation" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.3.0.0" newVersion="1.3.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.Unity" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.5.0.0" newVersion="3.5.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-7.0.0.0" newVersion="7.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="NLog" publicKeyToken="5120e14c03d0593c" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Spatial" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.6.0.0" newVersion="5.6.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http.OData" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.0.0.0" newVersion="5.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <!--<bindingRedirect oldVersion="0.0.0.0-1.5.1.25624" newVersion="1.5.1.25624"/>-->
        <bindingRedirect oldVersion="0.0.0.0-1.6.5135.21930" newVersion="1.6.5135.21930" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Quartz" publicKeyToken="f6b8c98a402cc8a4" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.3.3.0" newVersion="2.3.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="AutoMapper" publicKeyToken="be96cd2c38ef1005" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.3.1.0" newVersion="3.3.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="AutoMapper.Net4" publicKeyToken="be96cd2c38ef1005" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.2.1.0" newVersion="3.2.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Practices.Unity.Configuration" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Common.Logging" publicKeyToken="af08829b84f0328e" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.2.0.0" newVersion="3.2.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.AspNet.SignalR.Core" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.2.0.0" newVersion="2.2.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="AntiXssLibrary" publicKeyToken="d127efab8a9c114f" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.3.0.0" newVersion="4.3.0.0" />
      </dependentAssembly>
           <dependentAssembly>
    
    
                  <assemblyIdentity name="log4net" publicKeyToken="669e0ddf0bb1aa2a" culture="neutral" />
    
    
                  <bindingRedirect oldVersion="0.0.0.0-1.2.13.0" newVersion="1.2.13.0" />
    
    
           </dependentAssembly>
    
    
           <dependentAssembly>
    
    
                  <assemblyIdentity name="Common.Logging.Core" publicKeyToken="af08829b84f0328e" culture="neutral" />
    
    
                  <bindingRedirect oldVersion="0.0.0.0-3.2.0.0" newVersion="3.2.0.0" />
    
    
           </dependentAssembly>
           <dependentAssembly>
                  <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
                  <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
           </dependentAssembly>
           <dependentAssembly>
                  <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
                  <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
           </dependentAssembly>
           <dependentAssembly>
                  <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
                  <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
           </dependentAssembly>
    
    
    </assemblyBinding>
  <!-- When targeting ASP.NET MVC 3, this assemblyBinding makes MVC 1 and 2 references relink
		     to MVC 3 so libraries such as DotNetOpenAuth that compile against MVC 1 will work with it.
		<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
			<dependentAssembly>
				<assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
				<bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
			</dependentAssembly>
		</assemblyBinding>
		 --><!-- This prevents the Windows Event Log from frequently logging that HMAC1 is being used (when the other party needs it). --><legacyHMACWarning enabled="0" /></runtime>
  <system.diagnostics>
    <!--<switches>
      -->
    <!-- Switch needed for XActLib to trace using XAct.Diagnostics.ITraceListener -->
    <!-- 
      <add name="Application" value="Verbose"/>
    </switches>-->
    <trace autoflush="true">
      <listeners>
        <clear />
        <!-- Do not Add 'Default'. Know about functionality around 'EnsureDefaultTraceListenerIsActiveInDebugMode' in AppSettings above -->
        <add name="nlog" />
      </listeners>
    </trace>
    <sources>
      <!-- http://bit.ly/rpys5M -->
      <!-- http://bit.ly/oYbgKG -->
      <!-- http://bit.ly/mUr0cs -->
      <source name="System.ServiceModel" switchValue="Information,ActivityTracing" propagateActivity="true">
        <listeners>
          <add name="xml" />
        </listeners>
      </source>
      <source name="System.ServiceModel.MessageLogging">
        <listeners>
          <add name="nlog" />
          <add name="xml" />
        </listeners>
      </source>
      <source name="dk.nita.saml20" switchValue="Verbose">
        <listeners>
          <add name="nlog" />
        </listeners>
      </source>
      <source name="GlimpseSource" switchName="sourceSwitch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="GlimpseListener" />
        </listeners>
      </source>
    </sources>
    <switches>
      <add name="sourceSwitch" value="Debug" />
    </switches>
    <sharedListeners>
      <add name="nlog" type="NLog.NLogTraceListener, NLog" />
      <add name="xml" type="System.Diagnostics.XmlWriterTraceListener" initializeData="\Outputs\logs\messages.svclog" />
      <add name="GlimpseListener" type="Glimpse.Core.TraceListener, Glimpse.Core" />
      <!--<add name="myTextListener" type="System.Diagnostics.TextWriterTraceListener" initializeData="c:\TMP\Logs\TextWriterOutput.log"/>-->
    </sharedListeners>
  </system.diagnostics>
  <system.serviceModel>
    <!-- Enable aspNetCompatibility to allow the WCF service to access HttpContext -->
		<serviceHostingEnvironment aspNetCompatibilityEnabled="true" multipleSiteBindingsEnabled="true" />
		
			<!-- WCF Server contract definitions for incoming Client proxies of Back Tier Service Facade endpoints -->
    <bindings>
      <basicHttpBinding>
        <binding name="BasicHttpBinding_Binding" bypassProxyOnLocal="true" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647" maxBufferPoolSize="2147483647" receiveTimeout="00:01:00" sendTimeout="00:01:00">
          <readerQuotas maxDepth="32" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxStringContentLength="2147483647" />
        </binding>
      </basicHttpBinding>
    </bindings>
    
    <client>
      <endpoint name="Example" address="http://localhost:19991/Services/Example.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_Binding" contract="App.Core.Application.Services.Facades.IExampleServiceFacade" />
      <endpoint name="Diagnostics" address="http://localhost:19991/Services/Diagnostics.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_Binding" contract="App.Core.Application.Services.Facades.IDiagnosticsServiceFacade" />
      <endpoint name="ReferenceData" address="http://localhost:19991/Services/ReferenceData.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_Binding" contract="App.Core.Application.Services.Facades.IReferenceDataServiceFacade" />
      <endpoint name="Parameter" address="http://localhost:19991/Services/Parameter.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_Binding" contract="App.Core.Application.Services.Facades.IParameterServiceFacade" />
      <endpoint name="Setting" address="http://localhost:19991/Services/Setting.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_Binding" contract="App.Core.Application.Services.Facades.ISettingServiceFacade" />
      <endpoint name="Session" address="http://localhost:19991/Services/Session.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_Binding" contract="App.Core.Application.Services.Facades.ISessionServiceFacade" />
      <endpoint name="Resource" address="http://localhost:19991/Services/Resource.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_Binding" contract="App.Core.Application.Services.Facades.IResourceServiceFacade" />
      <endpoint name="Help" address="http://localhost:19991/Services/Help.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_Binding" contract="App.Core.Application.Services.Facades.IHelpServiceFacade" />
      <!--<endpoint name="ImmutableReference" address="http://localhost:19991/Services/CultureSpecificImmutableReferenceData.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_Binding" contract="App.Core.Application.Services.Facades.ICultureSpecificImmutableReferenceDataServiceFacade"/>-->
    </client>
    
    <services>
      <service name="App.Core.Front.API.Single.SOAP.SOAPFacade" behaviorConfiguration="ServiceWithMetadata">
        <endpoint name="NSI_SOAP" address="http://localhost:17000/APP/API/SOAP/SOAP_2SoapPort.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_Binding" contract="ISOAP_2SoapPort" />
      </service>
    </services>
    
    <behaviors>
      <serviceBehaviors>
        <behavior name="ServiceWithMetadata">
          <serviceMetadata httpGetEnabled="true" />
          <serviceDebug includeExceptionDetailInFaults="false" />
        </behavior>
      </serviceBehaviors>
    </behaviors>
  </system.serviceModel>

  <system.web>
    <authentication mode="Forms">
      <!-- Note: 
           Session_Key is the chosen cookie name for practical reasons. 
           It's so that legacy XML API clients that may have 
           accessed the cookie by name, continue to work. -->
      <forms name="Security_Key" cookieless="UseCookies" loginUrl="httphandlers/signon.ashx" timeout="20" />
      
    </authentication>
    <compilation debug="true" targetFramework="4.5" />
    
    <!-- Stop raw error messages been made public. -->
    <customErrors mode="RemoteOnly" />

		<httpRuntime targetFramework="4.5" maxUrlLength="1024" maxQueryStringLength="3000" maxRequestLength="10000" enableVersionHeader="false" />
    
    
    
    <!-- Glimpse: This can be commented in to add additional data to the Trace tab when using WebForms
        <trace writeToDiagnosticsTrace="true" enabled="true" pageOutput="false"/> --><httpModules>
      <!--<remove name="WebDAVModule"/>-->
      <add name="ErrorLog" type="Elmah.ErrorLogModule, Elmah" />
      <add name="ErrorMail" type="Elmah.ErrorMailModule, Elmah" />
      <add name="ErrorFilter" type="Elmah.ErrorFilterModule, Elmah" />
      <!--<add name="SamlIdentityConversion" type="App.Core.AppHost.HttpModules.SamlIdentityAuthenticationModule" />-->
      
    <add name="Glimpse" type="Glimpse.AspNet.HttpModule, Glimpse.AspNet" /></httpModules>
    
    <!-- OWASP security - ensure cookies have the secure and onlyHttp flags set -->
    <httpCookies httpOnlyCookies="true" requireSSL="true" />
    
      <pages>
      <namespaces>
        <add namespace="System.Web.Helpers" />
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Optimization" />
        <add namespace="System.Web.Routing" />
        <add namespace="System.Web.WebPages" />
      </namespaces>
    </pages>
    
      <webServices>
        <soapExtensionTypes>
          <!-- WebService pipeline extension required 
               to convert framework's normal soap tags (soap:Envelope) 
               to the exact tags used in APP version 1 (eg: SOAP-ENV:Envelope).
               -->
          <!-- Works for Exceptions...but gets in way of normal responses -->
          <!--<add type="App.Core.API.Legacy.Single.SOAP.WebServices.ReplaceDefaultNamespacePrefixSoapExtension, App.Legacy.Front" />-->
        </soapExtensionTypes>
      </webServices>
  
  <httpHandlers>
            <add path="glimpse.axd" verb="GET" type="Glimpse.AspNet.HttpHandler, Glimpse.AspNet" />
        </httpHandlers></system.web>
  
    
    
  <system.webServer>
    <validation validateIntegratedModeConfiguration="false" />
    <handlers>
      <!-- The POX Handler handles XML calls to Add, Update, Merge, Search for Records-->
      <remove name="LegacyPOXHandler" />
      <!--<add name="LegacyPOXHandler" verb="*" 
           path="interface/nsi_xml.asp" 
           type="App.Core.AppFacade.POXHttpHandler, App.Core.Front.AppHost.Web, Version=1.0.0.0, Culture=neutral" 
           resourceType="Unspecified"/>-->
      <add verb="GET,POST" path="interface/nsi_xml.asp" name="POXHandlerFactory" type="App.Core.API.Legacy.Single.HttpHandlers.POXAPIHttpHandlerFactory, App.Legacy.Front, Version=1.0.0.0, Culture=neutral" />      
			<add verb="GET,POST" path="interface/batch_login.asp" name="BatchLoginHandlerFactory" type="App.Core.Presentation.HttpHandlers.Batch_Login_HttpHandlerFactory, App.Legacy.Front, Version=1.0.0.0, Culture=neutral" />
      <add verb="GET,POST" path="interface/batch_upload.asp" name="BatchUploadHandlerFactory" type="App.Core.Presentation.HttpHandlers.Batch_UploadRequestFile_HttpHandlerFactory, App.Legacy.Front, Version=1.0.0.0, Culture=neutral" />
      <add verb="GET,POST" path="interface/batch_download.asp" name="BatchDownloadHandlerFactory" type="App.Core.Presentation.HttpHandlers.Batch_DownloadResultFile_HttpHandlerFactory, App.Legacy.Front, Version=1.0.0.0, Culture=neutral" />
      
      
			
			<!--<add verb="*" path="interface/batch_login.asp" name="BatchLoginLogout" type="App.Core.Presentation.HttpHandlers.Batch_Login_HttpHandlerFactory, App.Legacy.Front, Version=1.0.0.0, Culture=neutral" />
      <add verb="*" path="interface/batch_upload.asp" name="BatchOperationRequestUpload" type="App.Core.Presentation.HttpHandlers.Batch_UploadRequestFile_HttpHandlerFactory, App.Legacy.Front, Version=1.0.0.0, Culture=neutral" />
      <add verb="*" path="interface/batch_download.asp" name="BatchOperationResponseRetrieval" type="App.Core.Presentation.HttpHandlers.Batch_DownloadResultFile_HttpHandlerFactory, App.Legacy.Front, Version=1.0.0.0, Culture=neutral" />-->
      
			
			<!--<add name="QuickTest" verb="*" path="*.fin" type="App.Core.AppFacade.TestHttpHandler, App.Core.AppHost.Web.Mvc, Version=1.0.0.0, Culture=neutral"  resourceType="Unspecified"/>-->
      <add verb="GET,POST" path="backtier/*" name="Passthru" type="App.Core.API.Legacy.Single.POX.ProxyHttpHandlerFactory, App.Core.Front.AppHost.Web, Version=1.0.0.0, Culture=neutral" />

      <!-- This is here to leave the first implementation of the SOAP handler available for testing. It should be removed before go live -->
      <!--<add verb="GET,POST" path="interface/nsi_soap_replacement_v1.wsdl" name="LegacySoap" type="App.Core.API.Legacy.Single.SOAP.WebServices.LegacyApiWebService, App.Legacy.Front, Version=1.0.0.0, Culture=neutral" />-->
			
			
      <remove name="WebDAV" />
      <remove name="Glimpse" />
      
      
      
      
      
    <add name="Glimpse" path="glimpse.axd" verb="GET" type="Glimpse.AspNet.HttpHandler, Glimpse.AspNet" preCondition="integratedMode" /><remove name="ExtensionlessUrlHandler-Integrated-4.0" /><remove name="OPTIONSVerbHandler" /><remove name="TRACEVerbHandler" /><add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="*" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" /></handlers>
    <modules>
      <remove name="WebDAVModule" />
      <remove name="ErrorLog" />
      <remove name="ErrorMail" />
      <remove name="ErrorFilter" />
      <remove name="SamlIdentityConversion" />
      <remove name="Glimpse" />
      <add name="ErrorLog" type="Elmah.ErrorLogModule, Elmah" preCondition="managedHandler" />
      <add name="ErrorMail" type="Elmah.ErrorMailModule, Elmah" preCondition="managedHandler" />
      <add name="ErrorFilter" type="Elmah.ErrorFilterModule, Elmah" preCondition="managedHandler" />
      <!--<add name="SamlIdentityConversion" type="App.Core.AppHost.HttpModules.SamlIdentityAuthenticationModule" preCondition="managedHandler" />-->
			
      <add name="CloakHttpHeaderModule" type="App.Core.AppHost.HttpModules.CloakHttpHeaderModule" preCondition="managedHandler" />
    
    <add name="Glimpse" type="Glimpse.AspNet.HttpModule, Glimpse.AspNet" preCondition="integratedMode" /></modules>
    <httpProtocol>
      <customHeaders>
        <clear />
        <!-- Stop IE from falling back to compatibility mode -->
        <add name="X-UA-Compatible" value="IE=edge,chrome=1" />
      </customHeaders>
    </httpProtocol>
		<rewrite>
			<rules>
					<!-- Ensure all site traffic is over SSL -->
				<rule name="HTTP to HTTPS redirect" stopProcessing="true">
					<match url="(.*)" />
					<conditions>
						<add input="{HTTPS}" pattern="off" ignoreCase="true" />
					</conditions>
					<action type="Redirect" redirectType="Found" url="https://{HTTP_HOST}/{R:1}" />
				</rule>
				<rule name="Redirect old landing page" stopProcessing="true">
					<match url="^home.aspx" />
					<action type="Redirect" redirectType="Found" url="https://{HTTP_HOST}/" />
				</rule>
			</rules>
		</rewrite>
  </system.webServer>
  <system.net>
    <connectionManagement>
      <add address="*" maxconnection="65535" />
    </connectionManagement>

    <defaultProxy enabled="true">
      <proxy bypassonlocal="False" usesystemdefault="True" />
    </defaultProxy>
    
      
    
  <settings>
			<!-- This setting causes .NET to check certificate revocation lists (CRL) 
			     before trusting HTTPS certificates.  But this setting tends to not 
			     be allowed in shared hosting environments. -->
			<!--<servicePointManager checkCertificateRevocationList="true"/>-->
		</settings></system.net>
  
  





<dotNetOpenAuth>
		<!-- This is an optional configuration section where aspects of dotnetopenauth can be customized. --><!-- For a complete set of configuration options see http://www.dotnetopenauth.net/developers/code-snippets/configuration-options/ --><openid>
			<relyingParty>
				<security requireSsl="false">
					<!-- Uncomment the trustedProviders tag if your relying party should only accept positive assertions from a closed set of OpenID Providers. -->
					<!--<trustedProviders rejectAssertionsFromUntrustedProviders="true">
						<add endpoint="https://www.google.com/accounts/o8/ud" />
					</trustedProviders>-->
				</security>
				<behaviors>
					<!-- The following OPTIONAL behavior allows RPs to use SREG only, but be compatible
					     with OPs that use Attribute Exchange (in various formats). -->
					<add type="DotNetOpenAuth.OpenId.RelyingParty.Behaviors.AXFetchAsSregTransform, DotNetOpenAuth.OpenId.RelyingParty" />
				</behaviors>
			</relyingParty>
		</openid>
	<messaging>
			<untrustedWebRequest>
				<whitelistHosts>
					<!-- Uncomment to enable communication with localhost (should generally not activate in production!) -->
					<!--<add name="localhost" />-->
				</whitelistHosts>
			</untrustedWebRequest>
		</messaging><!-- Allow DotNetOpenAuth to publish usage statistics to library authors to improve the library. --><reporting enabled="true" /></dotNetOpenAuth><uri>
		<!-- The uri section is necessary to turn on .NET 3.5 support for IDN (international domain names),
		     which is necessary for OpenID urls with unicode characters in the domain/host name.
		     It is also required to put the Uri class into RFC 3986 escaping mode, which OpenID and OAuth require. -->
		<idn enabled="All" />
		<iriParsing enabled="true" />
	</uri></configuration>