﻿New MVC developers get caught out by the fact that 
TextBoxFor, CheckboxFor, etc. (ie, specifically bound to a specific output control)
passes through values to the CSS...but the EditorFor does not.

ie:

Html.TextBoxFor(m=>m.Name, ... new {class="...."}); passes through the class attribute, but 

Html.EditorFor(m=>m.Name, ... new {class="...."}); does not.

The reason is that EditorFor can be *any* template, such as one for a whole ViewModel, 
with lots of sub controls...which one should the class be put on? The outer div? All the controls? 

That makes sense I guess...but doesn't make it less annoying that one has to then 
provide custom templates for default scalar controls, for EditorFor to work as expected
for 90% of the cases (the strings, the ints, etc.)

The EditorFor uses templates that are built into the system. But they can be overridden by providing
your own Boolean.cshtml, string.cshtml, etc.

Note that DateTime, and Currency ones are a bit different...they rely on JS that has to be initiated
by the page load (ie, consider _layout.cshtml).




