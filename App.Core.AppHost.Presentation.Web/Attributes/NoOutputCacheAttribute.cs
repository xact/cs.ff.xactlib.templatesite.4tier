﻿using System.Net.Http.Headers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace App.Core.AppHost.Attributes
{
    public sealed class NoOutputCacheAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {

        private CacheControlHeaderValue setClientCache()
        {
            var cachecontrol = new CacheControlHeaderValue();
            cachecontrol.NoCache = true;
            cachecontrol.NoStore = true;
            return cachecontrol;
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Response != null) 
            {
                actionExecutedContext.Response.Headers.CacheControl = setClientCache();     
            }

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}