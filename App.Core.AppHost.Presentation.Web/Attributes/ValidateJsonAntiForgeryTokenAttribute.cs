﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Helpers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using App.Core.AppHost.ExtensionMethods;
using App.Core.Infrastructure.Services;
using XAct;
using AllowAnonymousAttribute = System.Web.Http.AllowAnonymousAttribute;
using AuthorizeAttribute = System.Web.Http.AuthorizeAttribute;
using IAuthorizationFilter = System.Web.Http.Filters.IAuthorizationFilter;

namespace App.Core.AppHost.Attributes
{
    public sealed class ValidateJsonAntiForgeryTokenAttribute : AuthorizeAttribute, IOverrideFilter
    {
        /// <summary>
        /// Overrides the Web.Http.AuthorizeAttribute to provide protection against Cross Site Request Forgery attacks
        /// This relies on the request containing a header that has a anti forgery token that matches the token in the antiforgery cookie that was passed to the browser
        /// </summary>
        /// <param name="actionContext">The Http Action Context</param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            ITracingService tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>();

            if (actionContext == null)
            {
                throw new ArgumentNullException("actionContext");
            }

            if (SkipAuthorization(actionContext))
            {
                return;
            }

            HttpRequestMessage request = actionContext.Request;
            HttpRequestHeaders requestHeaders = request.Headers;

            var cookie = request.GetCookie(AntiForgeryConfig.CookieName);

            // We only really care about Request Forgery checking if the user that is coming in is authenticated
            // There is a problem where the following validation method checks the Identity.User against the username added to the antiforgerytoken in the form.
            // If a timeout has occurred the Identity.User.Name will be empty and a HttpAntiForgeryException will be thrown before the 
            // According to the documentation having AntiForgeryConfig.SuppressIdentityHeuristicChecks = true; in the Application_Start() of Global.asax should prevent this check from happening
            // but it's not working!

            try
            {
                AntiForgery.Validate(cookie ?? null,
                    request.GetHeader("__RequestVerificationToken"));
            }
            catch (HttpAntiForgeryException)
            {
                System.Security.Principal.IPrincipal user = actionContext.RequestContext.Principal;
                if (user == null || user.Identity.Name.IsNullOrEmpty())
                {
                    // There is no authenticated user.  Code further down the stack will throw a NotAuthenticatedException
                    // So just swallow this exception and carry on
                }
                else
                {
                   throw;
                }
            }
        }

        private static bool SkipAuthorization(HttpActionContext actionContext)
		{
			return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any<AllowAnonymousAttribute>() 
                || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any<AllowAnonymousAttribute>();
		}

        public Type FiltersToOverride
        {
            get { return typeof(IAuthorizationFilter); }

        }
    }
}