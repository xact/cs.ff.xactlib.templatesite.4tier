﻿using System;
using System.Web.Helpers;
using System.Web.Mvc;
using App.Core.AppHost.Initialization;

namespace App.Core.AppHost.UI
{

	public class MvcApplication : System.Web.HttpApplication
	{
	    /// <summary>
	    /// Part of the initialization sequence chain.
	    /// <para>
	    /// The initialization sequence is as follows:
	    /// <code>
	    /// * Global.asax 
	    ///     * ... invokes AppHostInitializer (this class),
	    ///         * ... which first invokes PresentationInitializer
	    ///             * ... which first invokes FrontInfrastructureInitializer
	    ///               where IoC (ie Unity) is initialized with Infrastructure Services...
	    ///         * ....now, coming back up...Presentation services are registered...
	    ///     * ... now that both Infras and presentation are initialized, AppHostInitializer
	    ///       finishes up by registering AppHost level Services.
	    /// * Initialization done...
	    /// * Integration cycle then starts...
	    ///   It too goes down through the layers, and back up, in much the same way... 
	    ///   but this time actually using remote services (eg, to load caches, etc).      
	    /// </code>
	    /// </para>
	    /// </summary>
	    protected void Application_Start()
	    {
            //:TODO Investigate why AntiForgeryConfig.SuppressIdentityHeuristicChecks = true does not stop the Identity check for CSRF
	        AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;

            //Start cascading Initializer:
            AppHostInitializer.Execute();

	        MvcHandler.DisableMvcResponseHeader = true;




	    }


		protected void Application_Error(object sender, EventArgs e)
		{
			// Error logging is now implemented with filters - for both MVC & WebAPI calls. Lets see if that pans out better

			var err = Server.GetLastError();
			if (err == null)
				return;

			// CUSTOM: Some work to get MVC's 'Route not found' (Sent as a InvalidOperationException) and convert it to a 404 (not 500)
			// If we need to display a stylised message (through <customErrors> in web.config), there's some more hoops to jump through - Search StackOverflow...

			err = err.GetBaseException();

			if (err is InvalidOperationException)
			{
				HandleObjectNotFound();
			}
		}

		/// <summary>
		/// Sets the response to show a simple 404/PageNotFound
		/// </summary>
		private void HandleObjectNotFound()
		{
			Server.ClearError();
			Response.Clear();
			// new HttpExcepton(404, "Not Found"); // Throw or not to throw?
			Response.StatusCode = 404;
			Response.StatusDescription = "Not Found";
			Response.Write("This page was not found, sorry");
		}

	}
}