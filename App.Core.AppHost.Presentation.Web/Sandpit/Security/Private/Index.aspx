﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="App.Core.AppHost.Sandpit.Security.Private.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        A <span style="color:red;">private</span> page that (once Authentication is applied) should not be available.
    
    </div>
    </form>
</body>
</html>
