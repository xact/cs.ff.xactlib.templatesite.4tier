﻿//using System.Diagnostics;
//using System.Linq.Expressions;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http.Filters;
//using App.Core.Constants.Domain;
//using XAct.Diagnostics;
//using XAct.Messages;
//using TraceLevel = XAct.Diagnostics.TraceLevel;

//namespace App.Core.AppHost
//{
//    /// <summary>
//    /// WebAPI Exception Handler.
//    /// <para>
//    /// purpose is to trap AuthorisationExceptions
//    /// returned by calls to the back API, and convert them 
//    /// to 401/403 HttpCode Response.
//    /// </para>
//    /// <para>
//    /// Registered in App_Start/WebApiConfig.cs using
//    /// <code>
//    /// <![CDATA[
//    /// ...
//    /// config.Filters.Add(new AuthorizationExceptionFilterAttribute());
//    /// ]]>
//    /// </code>
//    /// </para>
//    /// </summary>
//    public class NsiExceptionFilterAttribute : ExceptionFilterAttribute
//    {
//        public override void OnException(HttpActionExecutedContext context)
//        {
//            // -- Auth checks 
//            // - These particular Xact.Security messages are used by the back tier.
//            // - We simply convert them to their correct HTTP response code
//            // Logging - Don't log auth exceptions, as they're not exceptional state. IIS Log reporting is where they should be seen.

//            if (context.Exception is XAct.Security.NotAuthenticatedException)
//            {
//                context.Response = MakeSafeReturnMessage(context.Request, HttpStatusCode.Unauthorized, MessageCodes.Security_SessionTimedOutNotEstablished_528);
//                context.Response.Headers.Add("WWW-Authenticate", "NOT SET");

//                return;
//            }

//            if (context.Exception is XAct.Security.NotAuthorizedException)
//            {
//                context.Response = MakeSafeReturnMessage(context.Request, HttpStatusCode.Forbidden, MessageCodes.Security_AccessDenied_529);

//                return;
//            }

//            if (context.Exception is System.Web.Mvc.HttpAntiForgeryException)
//            {
//                // Log it as a point of interest that it happened
//                Trace.WriteLine(string.Format("HTTP Anti forgery token was invalid - {0}", context.Exception.Message));

//                context.Response = MakeSafeReturnMessage(context.Request, HttpStatusCode.Forbidden, MessageCodes.Security_AccessDenied_529);
//                return;
//            }


//            // -- General error handling
//            // - Hide the exception detail from going out to the user

//            // Log exception first
//            LogException(context);

//            // Send a HTTP 500 message, and 
//            // As this is used by the REST API as well, they too need the App message code

//            context.Response = MakeSafeReturnMessage(context.Request, HttpStatusCode.InternalServerError, MessageCodes.General_SystemFault_789);
//        }

//        private static void LogException(HttpActionExecutedContext context)
//        {
//            try
//            {
//                var tracingService = XAct.Shortcuts.ServiceLocate<ITracingService>();
//                if (tracingService != null)
//                {
//                    tracingService.TraceException(TraceLevel.Error, context.Exception,
//                        "Unhandled exception while accessing: {0}",
//                        context.Request.RequestUri);
//                }
//            }
//            catch
//            {
//                // If tracing is somehow broken, try call out

//                Trace.TraceError("Unable to trace exception in NsiExceptionFilterAttribute - Error accessing ITracingService");
//            }
//        }

//        private static HttpResponseMessage MakeSafeReturnMessage(HttpRequestMessage incomRequestMessage, HttpStatusCode statusCode, MessageCode messageCode)
//        {
//            // Be extra careful in constructing the reply message. 
//            try
//            {
//                var response = new Response();
//                response.AddMessage(messageCode);

//                // This is the line that could potentially bottom out. One day, maybe.
//                var errorResultList = RestUtil.ConvertToMessageList(response.Messages);

//                return incomRequestMessage.CreateResponse(statusCode, errorResultList);
//            }
//            catch
//            {
//                // Just incase the resource translation didn't work, still send the 500, with 
//                return incomRequestMessage.CreateErrorResponse(statusCode, "");
//            }
//        }
//    }
//}