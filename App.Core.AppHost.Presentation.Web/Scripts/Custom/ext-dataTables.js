﻿/*************************************************
        jQuery datatables APP custom pagination        
*************************************************/

/*      Custom CSS classes        */
$.extend($.fn.dataTableExt.oStdClasses, {
	'sPageEllipsis': 'paginate_ellipsis',
	'sPageNumber': 'paginate_number',
	'sPageNumbers': 'paginate_numbers',
	'sPageIcon': 'icon',
	'sPagePrevious': 'first',
	'sPageNext': 'last'
});

$.fn.dataTableExt.oPagination.NSIPaging = {
	'oDefaults': {
		'iShowPages': 5
	},
	'fnClickHandler': function (e) {
		var fnCallbackDraw = e.data.fnCallbackDraw,
            oSettings = e.data.oSettings,
            sPage = e.data.sPage;

		if ($(this).is('[disabled]')) {
			return false;
		}

		oSettings.oApi._fnPageChange(oSettings, sPage);
		fnCallbackDraw(oSettings);

		return true;
	},
	// fnInit is called once for each instance of pager
	'fnInit': function (oSettings, nPager) {
		var oClasses = oSettings.oClasses;

		var iShowPages = oSettings.oInit.iShowPages || this.oDefaults.iShowPages,
            iShowPagesHalf = Math.floor(iShowPages / 2);

		$.extend(oSettings, {
			_iShowPages: iShowPages,
			_iShowPagesHalf: iShowPagesHalf,
		});

		var oNumbers = $('<ul class="' + oClasses.sPageNumbers + '"></ul>');

		// Draw
		$(nPager).append($('<nav></nav>').append(oNumbers));
	},
	// fnUpdate is only called once while table is rendered
	'fnUpdate': function (oSettings, fnCallbackDraw) {
		var oClasses = oSettings.oClasses,
            oLang = oSettings.oLanguage.oPaginate,
            that = this;

		var tableWrapper = oSettings.nTableWrapper;

		// Update stateful properties
		this.fnUpdateState(oSettings);

		// Toggle the pagination section (don't show when there aren't 2+ pages)
		if (oSettings.aanFeatures.p) {
			var pagers = $(oSettings.aanFeatures.p);
			(!oSettings._iTotalPages || oSettings._iTotalPages <= 1) ? pagers.hide() : pagers.show();
		}

		// Draw/setup numbers
		if (oSettings._iCurrentPage === 1) {
			$('.' + oClasses.sPageFirst, tableWrapper).attr('disabled', true);
			$('.' + oClasses.sPagePrevious, tableWrapper).attr('disabled', true);
		} else {
			$('.' + oClasses.sPageFirst, tableWrapper).removeAttr('disabled');
			$('.' + oClasses.sPagePrevious, tableWrapper).removeAttr('disabled');
		}

		if (oSettings._iTotalPages === 0 || oSettings._iCurrentPage === oSettings._iTotalPages) {
			$('.' + oClasses.sPageNext, tableWrapper).attr('disabled', true);
			$('.' + oClasses.sPageLast, tableWrapper).attr('disabled', true);
		} else {
			$('.' + oClasses.sPageNext, tableWrapper).removeAttr('disabled');
			$('.' + oClasses.sPageLast, tableWrapper).removeAttr('disabled');
		}

		var i, oNumber,
            oNumbers = $('.' + oClasses.sPageNumbers, tableWrapper),
            oPrevious = $('<a class="' + oClasses.sPageIcon + ' ' + oClasses.sPagePrevious + '">' + oLang.sPrevious + '</a>'),
            oNext = $('<a class="' + oClasses.sPageIcon + ' ' + oClasses.sPageNext + '">' + oLang.sNext + '</a>')
		;

		oPrevious.click({ 'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': 'previous' }, that.fnClickHandler);
		oNext.click({ 'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': 'next' }, that.fnClickHandler);

		// Erase
		oNumbers.html('');

		for (i = oSettings._iFirstPage; i <= oSettings._iLastPage; i++) {
			oNumber = $('<span>' + oSettings.fnFormatNumber(i) + '</span>');
			if (oSettings._iCurrentPage !== i) {
				oNumber = $('<a>' + oSettings.fnFormatNumber(i) + '</a>');
				oNumber.click({ 'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': i - 1 }, that.fnClickHandler);
			}

			// Draw
			oNumbers.append($('<li></li>').append(oNumber));
		}

		// Add ellipses
		if (1 < oSettings._iFirstPage) {
			if (oSettings._iFirstPage !== 2)
				oNumbers.prepend('<li><span class="' + oClasses.sPageEllipsis + '">...</span></li>');
			oNumber = $('<a>' + oSettings.fnFormatNumber(1) + '</a>');
			oNumber.click({ 'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': 0 }, that.fnClickHandler);
			oNumbers.prepend($('<li></li>').append(oNumber));
		}

		if (oSettings._iLastPage < oSettings._iTotalPages) {
			if (oSettings._iLastPage !== (oSettings._iTotalPages - 1))
				oNumbers.append('<li><span class="' + oClasses.sPageEllipsis + '">...</span></li>');
			oNumber = $('<a>' + oSettings.fnFormatNumber(oSettings._iTotalPages) + '</a>');
			oNumber.click({ 'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': oSettings._iTotalPages - 1 }, that.fnClickHandler);
			oNumbers.append($('<li></li>').append(oNumber));
		}

		//adding Previous and Next
		if (oSettings._iTotalPages !== 0) {
			$('<li></li>').append(oPrevious.last()).prependTo(oNumbers);
			$('<li></li>').append(oNext.last()).appendTo(oNumbers);
		}
	},
	// fnUpdateState used to be part of fnUpdate
	// The reason for moving is so we can access current state info before fnUpdate is called
	'fnUpdateState': function (oSettings) {
		var iCurrentPage = Math.ceil((oSettings._iDisplayStart + 1) / oSettings._iDisplayLength),
            iTotalPages = Math.ceil(oSettings.fnRecordsTotal() / oSettings._iDisplayLength),
            iFirstPage = iCurrentPage - oSettings._iShowPagesHalf,
            iLastPage = iCurrentPage + oSettings._iShowPagesHalf;

		if (iTotalPages < oSettings._iShowPages) {
			iFirstPage = 1;
			iLastPage = iTotalPages;
		} else if (iFirstPage < 1) {
			iFirstPage = 1;
			iLastPage = oSettings._iShowPages;
		} else if (iLastPage > iTotalPages) {
			iFirstPage = (iTotalPages - oSettings._iShowPages) + 1;
			iLastPage = iTotalPages;
		}

		$.extend(oSettings, {
			_iCurrentPage: iCurrentPage,
			_iTotalPages: iTotalPages,
			_iFirstPage: iFirstPage,
			_iLastPage: iLastPage
		});
	}
};

/** --------------------
	Custom sorters
---------------------**/

jQuery.fn.dataTableExt.oSort['studentSearch-Provider-asc'] = function (x, y) {

	if (x.CreatedByProviderName === y.CreatedByProviderName) {
		return x.CreatedOn < y.CreatedOn ? -1 : x.CreatedOn > y.CreatedOn ? 1 : 0;
	}

	return ((x.CreatedByProviderName < y.CreatedByProviderName) ? -1 : ((x.CreatedByProviderName > y.CreatedByProviderName) ? 1 : 0));
};

jQuery.fn.dataTableExt.oSort['studentSearch-Provider-desc'] = function (x, y) {

	if (x.CreatedByProviderName === y.CreatedByProviderName) {
		return x.CreatedOn < y.CreatedOn ? 1 : x.CreatedOn > y.CreatedOn ? -1 : 0;
	}

	return ((x.CreatedByProviderName < y.CreatedByProviderName) ? 1 : ((x.CreatedByProviderName > y.CreatedByProviderName) ? -1 : 0));
};






/**
 * Search through a table looking for a given string (optionally the search
 * can be restricted to a single column). The return value is an array with
 * the data indexes (from DataTables' internal data store) for any rows which
 * match.
 *
 *  @name fnFindCellRowIndexes
 *  @summary Search for data, returning row indexes
 *  @author [Allan Jardine](http://sprymedia.co.uk)
 *
 *  @param {string} sSearch Data to search for
 *  @param {integer} [iColumn=null] Limit search to this column
 *  @returns {array} Array of row indexes with this data
 *
 *  @example
 *    $(document).ready(function() {
 *        var table = $('#example').dataTable();
 * 
 *        var a = table.fnFindCellRowIndexes( '1.7' ); // Search all columns
 *
 *        var b = table.fnFindCellRowIndexes( '1.7', 3 );  // Search only column 3
 *    } );
 */

jQuery.fn.dataTableExt.oApi.fnFindCellRowIndexes = function (oSettings, sSearch, iColumn) {
	var
		i, iLen, j, jLen, val,
		aOut = [], aData,
		columns = oSettings.aoColumns;

	for (i = 0, iLen = oSettings.aoData.length ; i < iLen ; i++) {
		aData = oSettings.aoData[i]._aData;

		if (iColumn === undefined) {
			for (j = 0, jLen = columns.length ; j < jLen ; j++) {
				val = this.fnGetData(i, j);

				if (val == sSearch) {
					aOut.push(i);
				}
			}
		}
		else if (this.fnGetData(i, iColumn) == sSearch) {
			aOut.push(i);
		}
	}

	return aOut;
};

jQuery.fn.dataTableExt.oApi.fnStandingRedraw = function (oSettings) {
	if (oSettings.oFeatures.bServerSide === false) {
		var before = oSettings._iDisplayStart;

		oSettings.oApi._fnReDraw(oSettings);

		// iDisplayStart has been reset to zero - so lets change it back
		oSettings._iDisplayStart = before;
		oSettings.oApi._fnCalculateEnd(oSettings);
	}

	// draw the 'current' page
	oSettings.oApi._fnDraw(oSettings);
};