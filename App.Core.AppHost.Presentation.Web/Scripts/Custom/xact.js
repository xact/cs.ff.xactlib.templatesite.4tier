var com = com || {};
com.xact = com.xact || {};
xact = com.xact;

//Src:http://blog.arc90.com/2008/06/06/an-easy-way-to-implement-namespaces-in-javascript/
xact.namespace = function (ns) {
    // First split the namespace string separating each level of the namespace object.
    var splitNs = ns.split('.');
    // Define a string, which will hold the name of the object we are currently working with. Initialize to the first part.
    var builtNs = splitNs[0];
    var i, base = window;
    for (i = 0; i < splitNs.length; i++) {
        if (typeof (base[splitNs[i]]) == 'undefined') base[splitNs[i]] = {};
        base = base[splitNs[i]];
    }
    return base; // Return the namespace as an object.
}