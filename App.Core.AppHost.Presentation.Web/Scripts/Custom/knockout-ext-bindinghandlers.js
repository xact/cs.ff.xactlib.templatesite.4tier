﻿//Dependencies:
//Nuget: moment.js


// This class defines custom knockout display bindings
// E.g.: The ability to format a property as a date, integer.

ko.bindingHandlers.numericValue = {
	init: function (element, valueAccessor, allBindingsAccessor) {
		var underlyingObservable = valueAccessor();
		var interceptor = ko.dependentObservable({
			read: underlyingObservable,
			write: function (value) {
				if (!isNaN(value)) {
					underlyingObservable(parseFloat(value));
				}
			}
		});
		ko.bindingHandlers.value.init(element, function () { return interceptor; }, allBindingsAccessor);
	},
	update: ko.bindingHandlers.value.update
};

ko.bindingHandlers.checkedNumeric = {
	init: function (element, valueAccessor) {
		ko.bindingHandlers.checked.init(element, valueAccessor);
	},
	update: function (element, valueAccessor) {
		var value = ko.utils.unwrapObservable(valueAccessor());
		if (value == null) element.indeterminate = true;
		//ko.bindingHandlers.checked.update(element, valueAccessor);

		if (ko.bindingHandlers.checked.update) {

			ko.bindingHandlers.checked.update(element, valueAccessor);
		} else {
			ko.utils.unwrapObservable(valueAccessor());
		}
	}
};

/* -------
   Display date binding (value)

   usage: <input ... data-bind="valueAsDatePicker:MyModelsDate"/>
---------- */
var formatAndSetDateValue = function (element, valueAccessor, allBindingsAccessor, allowJsonDates) {
    // Provide a custom text value
    var value = valueAccessor();
    //var allBindings = allBindingsAccessor();
    //var dateFormat = allBindingsAccessor.dateFormat || "D MMM YYYY";
    var dateFormat = "D MMM YYYY";
    var strDate = ko.utils.unwrapObservable(value);

    var settings = com.app.frontend.data.settings;

    //var momentAcceptedDateInputs = ['D/M/YY', 'DD/M/YY', 'D/MM/YY', 'DD/MM/YY', 'D/M/YYYY', 'DD/M/YYYY', 'D/MM/YYYY', 'DD/MM/YYYY', 'D MMM YY', 'DD MMM YY', 'D MMM YYYY', 'DD MMM YYYY'];
    var momentAcceptedDateInputs = settings.momentAcceptedDateInputs;

    if (true) { //allowJsonDates) {
        momentAcceptedDateInputs.push("YYYY-MM-DDTHH:mm:ss.SSSS");
    }

    if (strDate) {
        //Fixing the 29th Feb invalid date issue. #MB
        var parsedDate = moment(strDate, momentAcceptedDateInputs, 'en', true);
        //Fix to not allow 3 digit year format to proceed, rather allowing all valid dates input from textbox and datepicker
        //Though this regex condition allows many characters in month column, a seperate validation (message#001) will take care of right format
        //Since the ko validation handler triggers once after this format binding handler it was allowing 3 digit year format but now restricted in both handlers.
        var acceptedFormats1 = /^\d{1,2}\/\d{1,2}\/(?:\d{4}|\d{2})$/;
        var acceptedFormats2 = /^\d{1,2} \d{1,2} (?:\d{4}|\d{2})$/;
        var acceptedFormats3 = /^\d{1,2} [a-zA-Z0-9]* (?:\d{4}|\d{2})$/;
        var result = strDate.match(acceptedFormats1) != null || strDate.match(acceptedFormats2) !== null || strDate.match(acceptedFormats3) != null;
        
        if (parsedDate.isValid() && result === true) {
                var formattedDate = parsedDate.format(dateFormat);
                {
                    var $element = $(element);
                    $element.val(formattedDate);

                    // Update underlying value
                    // Only update if parsed & different to original. Don't create an infinite loop...
                    if (strDate !== formattedDate) {
                        value(formattedDate);
                    }

                    // Focus to ensure placeholder shim continues to work

                    //Timeout for IE ignoring jquery Focus(). Damn IE
                    //setTimeout(function () {
                    $element.focus();
                    //}, 10);
                }
            }
        }
    }
    ;


    //Used for Parameter Details Page
var formatAndSetDefaultDateValue = function (element, valueAccessor, allBindingsAccessor, allowJsonDates) {
        // Provide a custom text value
        var value = valueAccessor();
        //var allBindings = allBindingsAccessor();
        var dateFormat = allBindingsAccessor.dateFormat || "YY-MM-dd";
        var strDate = ko.utils.unwrapObservable(value);

        var settings = com.app.frontend.data.settings;

        //var momentAcceptedDateInputs = ['D/M/YY', 'DD/M/YY', 'D/MM/YY', 'DD/MM/YY', 'D/M/YYYY', 'DD/M/YYYY', 'D/MM/YYYY', 'DD/MM/YYYY', 'D MMM YY', 'DD MMM YY', 'D MMM YYYY', 'DD MMM YYYY'];
        var momentAcceptedDateInputs = settings.momentAcceptedDateInputs;

        if (true) { //allowJsonDates) {
            momentAcceptedDateInputs.push("YYYY-MM-DDTHH:mm:ss.SSSS");
        }

        if (strDate) {
            //Fixing the 29th Feb invalid date issue. #MB
            var parsedDate = moment(strDate, momentAcceptedDateInputs, true);
            if (parsedDate.isValid()) {
                var formattedDate = parsedDate.format(dateFormat);
                {
                    var $element = $(element);
                    $element.val(formattedDate);

                    // Update underlying value
                    // Only update if parsed & different to original. Don't create an infinite loop...
                    if (strDate !== formattedDate) {
                        value(formattedDate);
                    }

                    // Focus to ensure placeholder shim continues to work

                    //Timeout for IE ignoring jquery Focus(). Damn IE
                    //setTimeout(function () {
                    $element.focus();
                    //}, 10);
                }
            }
        }
    };


    //Used for Parameter Details Page
    ko.bindingHandlers.valueAsDefaultDatePicker = {
	init: function (element, valueAccessor, allBindings) {

            // Use the value binding
            ko.bindingHandlers.value.init(element, valueAccessor, allBindings);

            formatAndSetDefaultDateValue(element, valueAccessor, allBindings, true);

            // Init UI datepicker
            var dateFormat = allBindings.dateFormat; // jQuery datepicker format

            $(element).datepicker({
                dateFormat: "yy-mm-dd",
                //altFormat: "dd-mm-yyyy",
                changeMonth: true, // TODO: Make configurable from the outside
                changeYear: true, // TODO: Make configurable from the outside
                yearRange: '1900:9999', // Limit max year to - Check with Gemma
                //minDate: 0, // Min date: today.
                showOn: "button",
                buttonImage: "Content/images/sprite-base/sprite/icon-calender.png",
                buttonImageOnly: true,
                constrainInput: false, // Do date parsing & Show input validation errors separately
                buttonText: ""
                //onSelect: function() {
                //	$.placeholder.shim(); // Refresh - Damn IE
                //	console.log('datepicker - onSelect()');
                //}
            });

        },
	update: function (element, valueAccessor, allBindingsAccessor) {
            // Use the value binding
            ko.bindingHandlers.value.update(element, valueAccessor, allBindingsAccessor);

            formatAndSetDefaultDateValue(element, valueAccessor, allBindingsAccessor);

            valueAccessor().valueHasMutated(); // Try trigger shim
        }
    };


    ko.bindingHandlers.valueAsDatePicker = {
	init: function (element, valueAccessor, allBindings) {
            bindDatePickerHelperInit(element, valueAccessor, allBindings, { maxDate: 0 });
        },
	update: function (element, valueAccessor, allBindingsAccessor) {
            bindDatePickerHelperUpdate(element, valueAccessor, allBindingsAccessor);
        }
    };

    ko.bindingHandlers.valueAsDOBDatePicker = {
	init: function (element, valueAccessor, allBindings) {
            bindDatePickerHelperInit(element, valueAccessor, allBindings, { maxDate: -1 });
        },
	update: function (element, valueAccessor, allBindingsAccessor) {
            bindDatePickerHelperUpdate(element, valueAccessor, allBindingsAccessor);
        }
    };


var bindDatePickerHelperInit = function (element, valueAccessor, allBindings, specificSettings) {
        // Use the value binding
        ko.bindingHandlers.value.init(element, valueAccessor, allBindings);
        formatAndSetDateValue(element, valueAccessor, allBindings, true);

        var value = valueAccessor();

        // Init UI datepicker
        var dateFormat = allBindings.dateFormat || "d MM yy"; // jQuery datepicker format
        var defaultSettings = {
            //dateFormat: "d MM yy",//dateFormat,
            dateFormat: "d M yy",
            //altFormat: "dd-mm-yyyy",
            changeMonth: true, // TODO: Make configurable from the outside
            changeYear: true, // TODO: Make configurable from the outside
            yearRange: '1900:' + new Date().getFullYear(), // Limit max year to the current year
            showOn: "button",
            buttonImage: "Content/images/sprite-base/sprite/icon-calender.png",
            buttonImageOnly: true,
            constrainInput: false, // Do date parsing & Show input validation errors separately
            buttonText: ""
            //onSelect: function() {
            //	$.placeholder.shim(); // Refresh - Damn IE
            //	console.log('datepicker - onSelect()');
            //}
        };

        $.datepicker.setDefaults(defaultSettings);
        $(element).datepicker(specificSettings);

        //Disable the datepicker if the item is disabled or enabled.
        if (allBindings.has('disable')) {
            if (allBindings.get('disable')()) {

                //Reset Value when disabled
                value("");
                $(element).datepicker('disable');
            } else {
                $(element).datepicker('enable');
            }
		var subscription = allBindings.get('disable').subscribe(function (newValue) {
                if (newValue) {

                    //Reset Value when disabled
                    value("");
                    $(element).datepicker('disable');
                } else {
                    $(element).datepicker('enable');
                }
            });

		ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                subscription.dispose();
            });
        }
    };

var bindDatePickerHelperUpdate = function (element, valueAccessor, allBindings) {
        // Use the value binding
        ko.bindingHandlers.value.update(element, valueAccessor, allBindings);

	validNzDate: { }
        formatAndSetDateValue(element, valueAccessor, allBindings);

        valueAccessor().valueHasMutated(); // Try trigger shim
};

    //ko.bindingHandlers.valueAsFutureDatePicker = {
    //	init: function (element, valueAccessor, allBindings) {

    //		// Use the value binding
    //		ko.bindingHandlers.value.init(element, valueAccessor, allBindings);

    //		formatAndSetDateValue(element, valueAccessor, allBindings, true);

    //		// Init UI datepicker
    //		var dateFormat = allBindings.dateFormat || "d MM yy"; // jQuery datepicker format

    //		$(element).datepicker({
    //			dateFormat: dateFormat,
    //			//altFormat: "dd-mm-yyyy",
    //			changeMonth: true, // TODO: Make configurable from the outside
    //			changeYear: true, // TODO: Make configurable from the outside
    //			yearRange: new Date().getFullYear() + ':9999', // Limit max year to - Check with Gemma
    //			minDate: 0, // Min date: today.
    //			showOn: "button",
    //			buttonImage: "Content/images/sprite-base/sprite/icon-calender.png",
    //			buttonImageOnly: true,
    //			constrainInput: false, // Do date parsing & Show input validation errors separately
    //			buttonText: ""
    //			//onSelect: function() {
    //			//	$.placeholder.shim(); // Refresh - Damn IE
    //			//	console.log('datepicker - onSelect()');
    //			//}

    //		});

    //	},
    //	update: function (element, valueAccessor, allBindingsAccessor) {
    //		// Use the value binding
    //		ko.bindingHandlers.value.update(element, valueAccessor, allBindingsAccessor);

    //		formatAndSetDateValue(element, valueAccessor, allBindingsAccessor);

    //		valueAccessor().valueHasMutated(); // Try trigger shim
    //	}
    //};

    // -- Display date binding (text)
    //usage: <input ... data-bind="textAsDate:MyModelsDate"/>
var formatAndSetDateText = function (element, valueAccessor, allBindingsAccessor) {
        // Provide a custom text value
        var value = valueAccessor();
        //var allBindings = allBindingsAccessor();
        var dateFormat = allBindingsAccessor.dateFormat || "D MMM YYYY"; // Display/MomentJS format
        var strDate = ko.utils.unwrapObservable(value);

        if (strDate) {
            var date = moment(strDate).format(dateFormat);
            $(element).text(date);
        }
    };

    ko.bindingHandlers.textAsDate = {
	init: function (element, valueAccessor, allBindingsAccessor) {

            // Use the value binding
            //ko.bindingHandlers.text.init(element, valueAccessor, allBindingsAccessor);
		if (ko.utils.unwrapObservable(valueAccessor()).toLowerCase() != "unknown") {
            formatAndSetDateText(element, valueAccessor, allBindingsAccessor, true);
		} else {
			$(element).text(valueAccessor());
		}
        },
	update: function (element, valueAccessor, allBindingsAccessor) {
            // Use the value binding
            //ko.bindingHandlers.text.update(element, valueAccessor, allBindingsAccessor);
		if (ko.utils.unwrapObservable(valueAccessor()).toLowerCase() != "unknown") {
            formatAndSetDateText(element, valueAccessor, allBindingsAccessor);
		} else {
			$(element).text(valueAccessor());
        }
	}
    };


var formatAndSetDateTimeText = function (element, valueAccessor, allBindingsAccessor) {
        // Provide a custom text value
        var value = valueAccessor();
        //var allBindings = allBindingsAccessor();
        var dateFormat = allBindingsAccessor.dateFormat || "D MMM YYYY HH:mm:ss"; // Display/MomentJS format
        var strDate = ko.utils.unwrapObservable(value);

        if (strDate) {
            var date = moment(strDate).format(dateFormat);
            $(element).text(date);
        }
    };

    ko.bindingHandlers.textAsDateTime = {
	init: function (element, valueAccessor, allBindingsAccessor) {

            // Use the value binding
            //ko.bindingHandlers.text.init(element, valueAccessor, allBindingsAccessor);

            formatAndSetDateTimeText(element, valueAccessor, allBindingsAccessor, true);
        },
	update: function (element, valueAccessor, allBindingsAccessor) {
            // Use the value binding
            //ko.bindingHandlers.text.update(element, valueAccessor, allBindingsAccessor);

            formatAndSetDateTimeText(element, valueAccessor, allBindingsAccessor);
        }
    };

    //--
    // Binding: textAsInt
    // Display binding for showing an integer
    // --
    ko.bindingHandlers.textAsInt = {
	init: function (element, valueAccessor, allBindingsAccessor) {
            var value = valueAccessor();
            $(element).text(Math.round(value));
        },
	update: function (element, valueAccessor, allBindingsAccessor) {
            var value = valueAccessor();
            $(element).text(Math.round(value));
        }
    };

    // --
    // Binding: Placeholder
    // Placeholdertext for input boxes (uses jquery html5 placeholder shim library
    // --
    ko.bindingHandlers.placeholder = {
	init: function (element, valueAccessor) {
            var placeholderValue = valueAccessor();
            ko.applyBindingsToNode(element, { attr: { placeholder: placeholderValue } });

            // Note: Don't try call the shim method now - let the page do it on compositionComplete.
            // Through some marvel of the DOM event cycle, the 'placeholder' attribute isn't ready for the .shim() in time on IE8/9
            //$.placeholder.shim();

        }
    };

    // --
    // Binding: Enter key
    // --

    ko.bindingHandlers.enterKey = {
	init: function (element, valueAccessor, allBindingsAccessor, viewModel) {

            var value = ko.utils.unwrapObservable(valueAccessor());

		$(element).keydown(function (e) {
                if (e.which === 13) { // If it's the enter key

                    // Execute the supplied function
                    value(viewModel);
                }
            });
        }
    };

    // TODO: I think is some example stuff that was copy/pasted. Will consider deleting when I next find it.
    //A custom knockout binding:
    //usage: <input ... data-bind="dollarvalue:MyModelsDate"/>
    ko.bindingHandlers.dollarvalue = {
	init: function (element, valueAccessor, allBindingsAccessor) {

            var underlyingObservable = valueAccessor();


            var interceptor = ko.computed({
			read: function () {
                    // this function does get called, but it's return value is not used as the value of the textbox.
                    // the raw value from the underlyingObservable, or the actual value the user entered is used instead, no   
                    // dollar sign added. It seems like this read function is completely useless, and isn't used at all
                    return "$" + underlyingObservable();
                },

			write: function (newValue) {
                    var current = underlyingObservable(),
                        valueToWrite = Math.round(parseFloat(newValue.replace("$", "")) * 100) / 100;

                    if (valueToWrite !== current) {
                        // for some reason, if a user enters 20.00000 for example, the value written to the observable
                        // is 20, but the original value they entered (20.00000) is still shown in the text box.
                        underlyingObservable(valueToWrite);
                    } else {
                        if (newValue !== current.toString())
                            underlyingObservable.valueHasMutated();
                    }
                }
            });

            ko.applyBindingsToNode(element, { value: interceptor });
        }
    };


    //Datepicker custom biding.
    ko.bindingHandlers.datepicker = {
	init: function (element, valueAccessor, allBindingsAccessor) {
            var $input = $(element);
            var defaultDatepickerSettings = {
                constrainInput: false,
                minDate: $input.attr('min'),
                maxDate: $input.attr('max'),
                defaultDate: $input.attr('placeholder'),
                dateFormat: "dd M yy", //settings.defaultDateFormat, // TODO: Inject 'settings' properly. This will just error otherwise
                showOn: "button",
                buttonImage: "Content/images/sprite-base/sprite/icon-calender.png",
                buttonImageOnly: true,
                buttonText: ""
            };

            //initialize datepicker with some optional options, if not set in the view the default set will be used
            var options = allBindingsAccessor().datepickerOptions || defaultDatepickerSettings;

            //handle the field changing
            $input.datepicker(options)
			.bind("change", function () {
                    ko.bindingHandlers.datepicker.updateValue(element, valueAccessor, allBindingsAccessor);
                });

            //        //fix the placeholder "default text" issue
            //        $input.keypress(function () {
            //            if ($input.val() != $input.attr('placeholder') && $input.hasClass('exampleText'))
            //                $input.removeClass('exampleText');
            //
            //        });
            //handle disposal (if KO removes by the template binding)
		ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).datepicker("destroy");
            });
        },

        //update the control when the view model changes
	update: function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());

            //fix the placeholder "default text" issue
            if (value != $(element).attr('placeholder') && $(element).hasClass('exampleText'))
                $(element).removeClass('exampleText');

            // If the date is coming from a Microsoft webservice.
            if (typeof value === "string" && value.indexOf('/Date(') === 0) {
                value = new Date(parseInt(value.replace(/\/Date\((.*?)\)\//gi, "$1")));
            }
            var currentDate = $(element).datepicker("getDate");

            // Check if the date has changed.
            if (value && value - currentDate !== 0) {
                $(element).datepicker("setDate", value);
            }
        },
	updateValue: function (element, valueAccessor, allBindingsAccessor) {
            var observable = valueAccessor(),
                dateValue = $(element).datepicker("getDate");

            // Two-way-binding means a writeable observable.
            if (ko.isWriteableObservable(observable)) {
                observable(dateValue);
                return;
            }
            if (allBindingsAccessor()._ko_property_writers) {
                allBindingsAccessor()._ko_property_writers.datepicker(dateValue);
            }
        }
    };

    ko.bindingHandlers.valueAsFutureDatePicker = {
	init: function (element, valueAccessor, allBindings) {

            // Use the value binding
            ko.bindingHandlers.value.init(element, valueAccessor, allBindings);

            formatAndSetDateValue(element, valueAccessor, allBindings, true);

            // Init UI datepicker
            var dateFormat = allBindings.dateFormat; // jQuery datepicker format

            $(element).datepicker({
                dateFormat: "d M yy",
                //altFormat: "dd-mm-yyyy",
                changeMonth: true, // TODO: Make configurable from the outside
                changeYear: true, // TODO: Make configurable from the outside
                yearRange: new Date().getFullYear() + ':9999', // Limit max year to - Check with Gemma
                minDate: 0, // Min date: today.
                showOn: "button",
                buttonImage: "Content/images/sprite-base/sprite/icon-calender.png",
                buttonImageOnly: true,
                constrainInput: false, // Do date parsing & Show input validation errors separately
                buttonText: ""
                //onSelect: function() {
                //	$.placeholder.shim(); // Refresh - Damn IE
                //	console.log('datepicker - onSelect()');
                //}
            });

        },
	update: function (element, valueAccessor, allBindingsAccessor) {
            // Use the value binding
            ko.bindingHandlers.value.update(element, valueAccessor, allBindingsAccessor);

            formatAndSetDateValue(element, valueAccessor, allBindingsAccessor);

            valueAccessor().valueHasMutated(); // Try trigger shim
        }
    };


    ko.bindingHandlers.slideVisible = {
	update: function (element, valueAccessor, allBindingsAccessor) {
            // First get the latest data that we're bound to
            var value = valueAccessor(), allBindings = allBindingsAccessor();

            // Next, whether or not the supplied model property is observable, get its current value
            var valueUnwrapped = ko.unwrap(value);

            // Grab some more data from another binding property
            var duration = allBindings.slideDuration || 400; // 400ms is default duration unless otherwise specified

            // Now manipulate the DOM element
            if (valueUnwrapped == true)
                $(element).slideDown(duration); // Make the element visible
            else
                $(element).slideUp(duration); // Make the element invisible
        }
    };

    ko.bindingHandlers.cssDisable = {
	init: function (element, valueAccessor, allBindingsAccessor, viewModel) {

            var value = ko.utils.unwrapObservable(valueAccessor());

            if (!value) {
                $(element).addClass("disabled");
            }
        }
    };

    ko.bindingHandlers.hidden = {
	update: function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            var isCurrentlyHidden = !(element.style.display == "");
            if (value && !isCurrentlyHidden)
                element.style.display = "none";
            else if ((!value) && isCurrentlyHidden)
                element.style.display = "";
        }
    };

    // -- Used for creating generated IDs in HTML. (For accessibility, etc)

    ko.bindingHandlers.uniqueId = {
	init: function (element, valueAccessor) {
            var value = valueAccessor();

            var prefix = value.prefix ? value.prefix : ko.bindingHandlers.uniqueId.prefix;

            element.id = element.id || prefix + (++ko.bindingHandlers.uniqueId.counter);
        },
        counter: 0,
        prefix: "unique"
    };

    // Work in progress
    ko.bindingHandlers['pendingClick'] = {
	'init': function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var $element = $(element);

		ko.utils.registerEventHandler(element, 'click', function (event) {

                var originalText = $element.text();

                $(element).text("loading");
			ko.bindingHandlers['enable']['update'](element, function () { return false; });


                var spinner = new Spinner({
                    length: 4,
                    width: 2,
                    radius: 4,
                    className: 'spinner-buttonRight'
                }).spin();
                element.appendChild(spinner.el);

                // do things
                var dfd = valueAccessor();

			dfd().always(function () {
                    $(element).text(originalText);

				ko.bindingHandlers['enable']['update'](element, function () { return true; });

                    spinner.stop();
                });
            });
        }
    };

ko.bindingHandlers.radioChecked = (function () {
        var groupObservables = {};
        return {
		init: function (element, valueAccessor, allBindings) {
			    var name = element.name,
				    groupObservable = groupObservables[name] || (groupObservables[name] = ko.observable());

			    function checkedValue() {
				    return true;
			    }

			    // Click -> Set observable
			ko.utils.registerEventHandler(element, 'click', function () {
				    valueAccessor()(checkedValue());
			    });

			var s1 = ko.computed(function () {
				    // On model update:
				    if (valueAccessor()() === checkedValue()) {

					    if (valueAccessor()() !== false) {

						    element.checked = true;
					    }

					    // Keep track of which element is selected
					    groupObservable(element);
				    }
			    });

			    // Deselection - when the group selection changes, update current checkbox
			var s2 = groupObservable.subscribe(function () {
				    var currentValue = valueAccessor()();
				    var cValue = checkedValue();

				    // Only update deselected item if it retained its checked value
				    if (!element.checked
						    && currentValue === cValue
				    ) { // (Only deselect 'true' items
					    valueAccessor()(false);
				    }

				    // If the checked item's model is not updated, update it
				    if (element.checked && currentValue !== cValue) {
					    valueAccessor()(checkedValue());
				    }

			    });

			ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
				    s1.dispose();
				    s2.dispose();
			    });
		    }
	    }
    })();

    // 'Pending' button functionality - disable button + show spinner		
    ko.bindingHandlers['isPending'] = {
	'init': function (element, valueAccessor, allBindings, viewModel, bindingContext) {

            var $element = $(element);

            //var originalText = $element.text();

            var spinner = new Spinner({
                length: 4,
                width: 2,
                radius: 4,
                className: 'spinner-buttonRight'
            });

		var updatePending = function () {
                if (ko.utils.unwrapObservable(valueAccessor())) {

                    // Show 'loading' hints in UI
				ko.bindingHandlers['enable']['update'](element, function () { return false; });

                    spinner.spin();
                    $element.after(spinner.el);

                } else {
                    // Hide UI hints
				ko.bindingHandlers['enable']['update'](element, function () { return true; });
                    spinner.stop();
                }
            };

            updatePending();

		valueAccessor().subscribe(function () {
                updatePending();
            });

        }
    };

(function () {
        var init = ko.bindingHandlers['datepicker'].init;

	ko.bindingHandlers['datepicker'].init = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

            init(element, valueAccessor, allBindingsAccessor);

            return ko.bindingHandlers['validationCore'].init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        };
    }());