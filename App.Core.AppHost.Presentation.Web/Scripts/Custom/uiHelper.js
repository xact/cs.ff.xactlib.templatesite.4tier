﻿var com = com || {};
com.uiHelper = com.uiHelper || {};
uiHelper = com.uiHelper;

// TODO: Move this (biz logic) to it's own namespace
uiHelper.hasErrorCode = function(serverReponse, errorCode) {

	// We only expect 1 message to be returned
	var firstMessage = serverReponse.Messages[0];

	return (firstMessage.MessageCode.Id === errorCode);
};

// TODO: Move this (biz logic) to it's own namespace
uiHelper.hasError = function(serverResponse) {
	// A 'failure' message from the server must also contain messages.
	// TODO: Wait for Sky to fix 'Success' property.

	return !serverResponse.Success
		&& serverResponse.Messages.length > 0;
};

uiHelper.notValidCss = function (boundField, canShowErrors) {
	return boundField.isModified() && !boundField.isValid() && canShowErrors ? 'errorLabel' : 'normalColourLabel';
};


uiHelper.notValidField = function (boundField, canShowErrors) {
    return boundField.isModified() && !boundField.isValid() && canShowErrors;
};

uiHelper.hider = function () {
    showHideToggle('.hider', '.toggleMe');
    tabs('.tabs li');

    oldBrowserCss3();
};

/* ---
   Javascript extensions
   --- */

// Allow formating with the {0} syntax
String.prototype.format = function () {
	var args = arguments;
	return this.replace(/{(\d+)}/g, function (match, number) {
		return typeof args[number] != 'undefined'
		? args[number]
		: match;
	});
};

//Define the trim fuction for IE if it doesn't exist.
if (typeof String.prototype.trim != "function") {
	String.prototype.trim = function () {
		return this.replace(/^\s+|\s+$/g, '');
	};
}

// Make date into standard display format date
String.prototype.asDisplayDate = function () {
	var dateFormat = "D MMM YYYY"; // Display/MomentJS format
	var saneString = this.toString(); // Ensure normal string - String class is a weird array
	var formattedDate = moment(saneString).format(dateFormat);

	return formattedDate;
};

uiHelper.makeDatatableConfig = function(customConfig) {
	var defaultConfig = uiHelper.getDatatablesDefaultConfig();

	var mergedConfig = $.extend(true, defaultConfig, customConfig);

	return mergedConfig;
};

uiHelper.getDatatablesDefaultConfig = function() {

	/* Required properties to fill in:
		dataSource
		rowTemplate
		columns

		options.aaSorting
		
	*/
	return {
		options: {
			//disable searchbar (filter)
			"bFilter": false,

			//disable show row number dropdown
			"bLengthChange": false,

			//Gridview length
			"iDisplayLength": 20, //self.settings.defaultGridviewLength,

			//disable the addition of the classes 'sorting_1', 'sorting_2' and 'sorting_3'
			"bSortClasses": false,

			// Custom APP Paging
			'sPaginationType': 'NSIPaging',

			//customising DOM
			"sDom": 'rt<"paging" ip>'
		}
	};
};

uiHelper.convertMarkdownListToHtml = function(markdown) {
	if (!markdown) {
		return "";
	}

	var splitByList = markdown.split("*");

	var result = splitByList[0]; // first segment

	if (splitByList.length < 2) {
		return result;
	}

	result += "<ul>";

	for (var i = 1; i < splitByList.length; i++) {
		result += "<li>" + splitByList[i] + "</li>";
	}

	result += "</ul>";

	return result;

};

//Used for IE to get the shim function to 
uiHelper.blurAllInputs = function() {
	$('*:input[type!=button][type!=hidden][style!="display: none;"]').blur();
};

uiHelper.focusFirstInputElement = function() {
	$('*:input[type!=button][type!=hidden][disabled!="disabled"][style!="display: none;"]:first').focus();
};
