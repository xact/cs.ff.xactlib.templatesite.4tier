﻿using System.Runtime.Serialization;

namespace NSI_WEB_2
{
    [DataContract(Namespace = "http://www.datacom.co.nz/nsi_soap/")]
    public class NewSoapResponse : INewSoapResponse
    {
        /// <summary>
        /// Content of reply. May be blank (e.g. Logout has no reply). This is XML in a string.
        /// </summary>
        [DataMember]
        public string Result { get; set; }

        /// <summary>
        /// If there was an error - this is its number as per GINS. Unencoded, e.g. the value you want the caller to see; no need to add Soap offsets.
        /// </summary>
        [DataMember]
        public int ErrorNumber { get; set; }

        /// <summary>
        /// If there was an error - this is the stack trace.
        /// </summary>
        [DataMember]
        public string ErrorSource { get; set; }

        /// <summary>
        /// If there was an error - this is the description. As per GINS.
        /// </summary>
        [DataMember]
        public string ErrorDescription { get; set; }
    }
}