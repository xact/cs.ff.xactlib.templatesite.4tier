﻿using System.ServiceModel;
using System.Runtime.Serialization;

namespace NSI_WEB_2
{
    [ServiceContract(Namespace = "http://www.datacom.co.nz/nsi_soap/", Name = "INewSoap")]
    public interface INewSoap
    {
        [OperationContract]
        NewSoapResponse Login(
            string userId,
            string password,
            int orgId);

        [OperationContract]
        NewSoapResponse Logout(
            string securityKey);

        [OperationContract]
        NewSoapResponse Search(
            string securityKey,
            string nsn,
            string name,
            string dob,
            string gender,
            string residentialStatus,
            string user);

        [OperationContract]
        NewSoapResponse Insert(
            string securityKey,
            string surname,
            string forename1,
            string forename2,
            string forename3,
            string dob,
            string preferredName,
            string nameDobVerification,
            string residentialStatus,
            string residentialStatusVerification,
            string gender,
            string aliasLastname,
            string aliasForename1,
            string aliasForename2,
            string aliasForename3,
            string aliasPrefered,
            string aliasNameDobVerification,
            string overrideCode,
            string user);

        [OperationContract]
        NewSoapResponse Update(
            string securityKey,
            string nsn,
            string surname,
            string forename1,
            string forename2,
            string forename3,
            string dob,
            string preferredName,
            string nameDobVerification,
            string residentialStatus,
            string residentialVerification,
            string gender,
            string user,
            string altSurname,
            string altForename1,
            string altForename2,
            string altForename3,
            string altPreferredName,
            string altNameDobVerification);

        [OperationContract]
        NewSoapResponse Merge(
            string securityKey,
            string nsn0,
            string nsn1,
            string nsn2,
            string nsn3,
            string nsn4,
            string nsn5,
            string nsn6,
            string nsn7,
            string nsn8,
            string nsn9,
            string vsUser);
    }
}
