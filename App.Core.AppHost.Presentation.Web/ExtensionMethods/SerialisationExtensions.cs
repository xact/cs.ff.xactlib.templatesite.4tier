﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace App.Core.AppHost.ExtensionMethods
{
	public static class SerialisationExtensions
	{
        /// <summary>
        /// Returns a MemoryStream for a given string
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns></returns>
		public static MemoryStream GenerateStreamFromString(this string value, Encoding encoding=null)
		{
            if (encoding == null){encoding = Encoding.UTF8;}

			return new MemoryStream(encoding.GetBytes(value ?? ""));
		}
	}
}