﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using XAct;
using XAct.Net.Messaging;


namespace App.Core.AppHost.ExtensionMethods
{
	public static class HttpResponseUtil
	{
		/// <summary>
		/// Creates an HttpResponseMessage to use to indicate a downloadable file
		/// </summary>
		/// <param name="fileContents"></param>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public static HttpResponseMessage MakeHttpDownloadResponse(string fileContents, string fileName, string mediaType = null, Encoding encoding = null)
		{
			var fileContentsStream = new StringContent(fileContents, encoding ?? Encoding.Default);

			return MakeHttpDownloadResponse(fileContentsStream, fileName, mediaType);			
		}

		/// <summary>
		/// Creates an HttpResponseMessage to use to indicate a downloadable file
		/// </summary>
		/// <param name="fileContents"></param>
		/// <param name="fileName"></param>
		/// <param name="mediaType"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static HttpResponseMessage MakeHttpDownloadResponse(Stream fileContents, string fileName, string mediaType = null, Encoding encoding = null)
		{
			return MakeHttpDownloadResponse(new StreamContent(fileContents), fileName, mediaType);
		}

		public static HttpResponseMessage MakeHttpDownloadResponse(HttpContent fileContents, string fileName, string mediaType = null)
		{
			// Set file contents
			var httpResponse = new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = fileContents
			};

			// Default media type, if not set
			if (mediaType == null)
			{
				mediaType = "application/octet-stream";
			}

			httpResponse.Content.Headers.ContentType =
				new MediaTypeHeaderValue(mediaType);

			// Set filename
			httpResponse.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
			{
				FileName = fileName
			};

			return httpResponse;
		}

	}
}

public static class HttpContentExtensions
{
    public static void AttachFile(this HttpResponseMessage httpResponseMessage, PersistedFile persistedFile,
                                  bool flush = false,
                                  bool setFileDownloadCookie = true, string cookieName = "fileDownload",
                                  string cookiePath = "/", bool changeFileStreamEncoding = false,
                                  System.Text.Encoding sourceFileStreamEncoding = null,
                                  System.Text.Encoding targetFileStreamEncoding = null)
    {

        if (cookieName == null)
        {
            cookieName = "fileDownload";
        }
        if (sourceFileStreamEncoding == null)
        {
            sourceFileStreamEncoding = System.Text.Encoding.UTF8;
        }
        if (targetFileStreamEncoding == null)
        {
            targetFileStreamEncoding = System.Text.Encoding.UTF8;
        }


        if (setFileDownloadCookie)
        {
            bool cookieHttpOnly = false;
            //Allows usage from Ajax to HttpHandlers under Https when safe is turned on.
            bool cookieSecure = false;

            SetCookieForFileDownload(HttpContext.Current.Response, true, cookieName, cookiePath, cookieHttpOnly, cookieSecure);
        }



        byte[] outgoingBuffer;
        byte[] srcBytes = persistedFile.Value;

        outgoingBuffer = srcBytes;

        if (changeFileStreamEncoding && !sourceFileStreamEncoding.Equals(targetFileStreamEncoding))
        {
            //encoding = Encoding.GetEncoding("ISO-8859-1");
            outgoingBuffer = Encoding.Convert(sourceFileStreamEncoding, targetFileStreamEncoding, srcBytes);
        }
        else
        {
            targetFileStreamEncoding = sourceFileStreamEncoding;
        }


        var s = persistedFile.Value.ToString(targetFileStreamEncoding);

        StringContent stringContent = new StringContent(s);
        httpResponseMessage.Content = stringContent;

        //int bufferSize = outgoingBuffer.Length;
        //using (Stream stream = new MemoryStream())
        //{
        //    stream.Write(outgoingBuffer, 0, bufferSize);
        //    httpResponseMessage.Content = new StreamContent(stream);
        //    if (flush)
        //    {
        //        stream.Flush();
        //    }

        //}


        httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(persistedFile.ContentType ?? "application/octet-stream"); 

        //httpResponseMessage.Content.Headers.Add("Content-Disposition", "attachment; filename=" + persistedFile.Name);
        httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
        {
            FileName = persistedFile.Name,
            Size = s.Length
        };




        //httpResponseMessage.Content.Headers.Add("Content-Length", persistedFile.Size.ToString());
        //httpResponseMessage.Content.Headers.ContentLength = persistedFile.Size;


    }

    //The JQuery Download tool is capable of using an IFrame to work around missing functionality in IE.
    //But it needs a cookie.
    private static void SetCookieForFileDownload(HttpResponse httpResponse, bool hasFile, string cookieName,
                                                 string cookiePath = "/", bool cookieHttpOnly = false,
                                                 bool cookieSecure = false)
    {
        // Set the 'download' cookie as approprate, for the jQuery download tool that uses this

        if (hasFile)
        {
            //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
            //IMPORTANT:You have t mark it as HttpOnly or front end javascript (ie SPA) will fail:
            //NOTE: Havn't set secure... it might get picked up in a security audit.
            httpResponse.SetCookie(new HttpCookie(cookieName, "true")
                {
                    Path = cookiePath,
                    HttpOnly = cookieHttpOnly,
                    Secure = cookieSecure
                });
        }
        else
        {
            //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
            if (HttpContext.Current != null)
            {
                HttpRequest httpRequest = HttpContext.Current.Request;
                if (httpRequest.Cookies[cookieName] != null)
                {
                    httpResponse.Cookies[cookieName].Expires = DateTime.Now.AddYears(-1);
                }
            }
        }
    }




}