﻿using System.Web.Optimization;
using App.Core.AppHost.XTensions;

namespace App.Core.AppHost.UI
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
			// WARNING - Unused/incorrect files & directories will crash the app when loaded under IIS.

            // Main bundle for all required JS
			var nsiMainJsBudle = new ScriptBundle("~/bundles/scripts/app-libs");
	        nsiMainJsBudle.Orderer = new AsIsBundleOrderer();

			nsiMainJsBudle.Include()
				
                // Single page web stack  
                       .Include("~/Scripts/knockout.mapping-latest.js")
                        .Include("~/Scripts/toastr.js")
                        .Include("~/Scripts/sammy-{version}.js")
                        .Include("~/Scripts/Custom/moment_NSI_Custom.js") // Date-handling / Support 'datevalue' bindinghandler: 
						.Include("~/Content/js/jquery-html5-placeholder-shim.js") // Now included in the shim bundle for IE  Before binding handlers
						.Include("~/Scripts/Custom/knockout-ext-bindinghandlers.js") //Add this after knockout and moment:
						//"~/Scripts/Custom/knockout-ext-jsrenderTemplateEngine.js", // What is using this?
						.Include("~/Scripts/knockout.validation-latest-github.js")
						
                        .Include("~/Scripts/knockout-observableDictionary.js")
						//.Include("~/Scripts/Q.js") // TODO: Q\Promises are only in as a test - Can be re-evaluated later
						//.Include("~/Scripts/Custom/knockout-validation-customRules.js") // Must be after KO Validation // NOTE: Now included by RequireJS
						//"~/Scripts/knockout.validation.debug.js",
				// Everything else
                        .Include("~/Scripts/jquery.signalR-2.0.3.min.js")
                        .Include("~/Scripts/jsrender.js") // TODO: Do we need this?
						.Include("~/Scripts/modernizr-{version}.js")
						//"~/Scripts/Custom/knockout-ext-bindinghandlers.js",
						//"~/Scripts/Custom/knockout-ext-jsrenderTemplateEngine.js",
						.Include("~/Content/js/bootstrap-tooltips.js")
						.Include("~/Scripts/Custom/jquery-ui-1.10.3.custom.min.js")
						.Include("~/Scripts/jquery.blockUI.js")
						.Include("~/Scripts/spin.js")
						.Include("~/Scripts/Custom/Xregexp.js")

							//Fancy file dowload with promise
						.Include("~/Scripts/jquery.fileDownload.js")
						//File uploads
						.Include("~/Scripts/jquery.fileupload.js")
						.Include("~/Scripts/jquery.iframe-transport.js")
					//"~/Scripts/DataTables/jquery.dataTables.1.8.2.js", // TODO: Move this out to its own bundle
						.Include("~/Scripts/DataTables/jquery.dataTables.js") // TODO: Move this out to its own bundle
						.Include("~/Scripts/DataTables/cog.utils.js") // TODO: Move this out to its own bundle			
						.Include("~/Scripts/DataTables/knockout.bindings.dataTables.js") // TODO: Move this out to its own bundle			
						
						.Include("~/Scripts/Custom/uiHelper.js")
						.Include("~/Scripts/Custom/ext-dataTables.js")
						



                //Core common APP:
                        .Include("~/Scripts/App/Core.js")
                        ;
			bundles.Add(nsiMainJsBudle);

			bundles.Add(new ScriptBundle("~/bundles/knockout").Include("~/Scripts/knockout-{version}.js"));
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));

			// -- Currently unused
			//bundles.Add(new ScriptBundle("~/bundles/scripts/jqueryui").Include(
			//			"~/Client/Scripts/jquery-ui-{version}.js"));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			//bundles.Add(new ScriptBundle("~/bundles/scripts/modernizr").Include(
			//			"~/Client/Scripts/modernizr-*"));

			// -- Main CSS bundle

			// Bundle must be same relative distance as debug mode - /Content/css == /bundles/css
            bundles.Add(new StyleBundle("~/Content/css/css-bundle").Include( 
                        "~/Content/css/screen.css",
                        "~/Content/durandal.css",
                        "~/Content/toastr.css",
						"~/Content/bootstrap-modal.css"
                        ));

            //bundles.Add(new StyleBundle("~/bundles/Content/themes/base/css").Include(
            //            "~/Client/Content/themes/base/jquery.ui.core.css",
            //            "~/Client/Content/themes/base/jquery.ui.resizable.css",
            //            "~/Client/Content/themes/base/jquery.ui.selectable.css",
            //            "~/Client/Content/themes/base/jquery.ui.accordion.css",
            //            "~/Client/Content/themes/base/jquery.ui.autocomplete.css",
            //            "~/Client/Content/themes/base/jquery.ui.button.css",
            //            "~/Client/Content/themes/base/jquery.ui.dialog.css",
            //            "~/Client/Content/themes/base/jquery.ui.slider.css",
            //            "~/Client/Content/themes/base/jquery.ui.tabs.css",
            //            "~/Client/Content/themes/base/jquery.ui.datepicker.css",
            //            "~/Client/Content/themes/base/jquery.ui.progressbar.css",
            //            "~/Client/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}