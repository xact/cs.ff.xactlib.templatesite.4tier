﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace App.Core.AppHost.UI
{
    using App.Core.AppHost.Areas.UiAPI.Controllers;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            RazorViewEngine engine = ViewEngines.Engines.OfType<RazorViewEngine>().First();
            //engine.ViewLocationFormats.Add("");

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //Need to ignore the path to the POX Handler:
            routes.IgnoreRoute("interface/{*pathInfo}");
            routes.IgnoreRoute("backtier/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                                namespaces: new[] { "App.Core.AppHost.UI.Controllers", "App.Core.AppHost.Controllers" },

                defaults: new {controller = "Index", action = "Index", id = UrlParameter.Optional}
                );


        }
    }
}