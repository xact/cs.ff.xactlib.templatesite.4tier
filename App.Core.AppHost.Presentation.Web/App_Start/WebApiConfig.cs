﻿using System.Web.Http;
using Microsoft.Practices.ServiceLocation;

namespace App.Core.AppHost.UI
{
    using App.Core.AppHost.Areas.UiAPI.Controllers;

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
			FilterConfig.RegisterHttpFilters(GlobalConfiguration.Configuration.Filters);

            //We probably don't want to register any routes here
            //and leave it to the individual API configurations to do that.

			// Web API registration is done in in:
			// * Each respective Area
			// * Initialization/InitializationSteps/
            
            //config.Filters.Add(new NsiExceptionFilterAttribute());

	        //RestApiTokenMessageHandler apiTokenMessageHandler = ServiceLocator.Current.GetInstance<RestApiTokenMessageHandler>();

			// TODO: Temp - register this here (all WebAPI requests), but later find a way to limit it to just the public API
			//config.//MessageHandlers.Add(apiTokenMessageHandler);

			// Remove XML support. The public API + private UI will use JSON.
			// Re: Public API, for now it's safer to have XML disabled rather than available as an undocumented feature.
			// (It's not difficult to re-enable XML support, but it's the documentation, defaults etc that are worth considering)
			var formatters = GlobalConfiguration.Configuration.Formatters;
			
            //formatters.Remove(formatters.XmlFormatter);

        }
    }
}
