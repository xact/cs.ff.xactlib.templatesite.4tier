﻿using System;
using System.Diagnostics;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using App.Core.Infrastructure.Front.Services;
using Microsoft.Practices.ServiceLocation;
using IExceptionFilter = System.Web.Mvc.IExceptionFilter;

namespace App.Core.AppHost.UI
{
    using App.Core.Infrastructure.Services;

    public class FilterConfig
    {
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new LogExceptionFilter()); // Must be before HandleErrorAttribute
			filters.Add(new HandleErrorAttribute());
		}

		public static void RegisterHttpFilters(HttpFilterCollection filters)
		{
			filters.Add(new LogWebApiErrorAttribute());
			filters.Add(new ElmahErrorAttribute());
		}
    }

	// TODO: Move this to a (new) Shared.Web kinda project

    /// <summary>
    /// Log unhandled MVC exceptions
    /// (This seems to be more reliable that using Application_Error event)
    /// </summary>
    public class LogExceptionFilter : IExceptionFilter
	{
        private ITracingService _tracingService;

        private ITracingService TracingService
        {
            // Lazy load tracing service, as it can't reliably be done in the constructor (it's called in the MVC boot sequence)
            get { return _tracingService ?? (_tracingService = ServiceLocator.Current.GetInstance<ITracingService>()); }
        }

		public void OnException(ExceptionContext context)
		{
			Exception ex = context.Exception;
			if (!(ex is HttpException)) // Ignore "file not found"
			{
                TracingService.TraceException(TraceLevel.Error, ex, "Unhandled web exception");
			}
		}
	}

	/// <summary>
	/// Log WebAPI Exceptions
	/// (Note - This is a different filter to the main MVC error handling)
	/// </summary>
	public class LogWebApiErrorAttribute : System.Web.Http.Filters.ExceptionFilterAttribute
	{
		private ITracingService _tracingService;
		private ITracingService TracingService
		{
			// Lazy load tracing service, as it can't reliably be done in the constructor (it's called in the MVC boot sequence
			get { return _tracingService ?? (_tracingService = ServiceLocator.Current.GetInstance<ITracingService>()); }
		}

		public override void OnException(
			System.Web.Http.Filters.HttpActionExecutedContext actionExecutedContext)
		{
			if (actionExecutedContext.Exception != null)
			{
				TracingService.TraceException(TraceLevel.Error, actionExecutedContext.Exception, "Unhandled WebAPI exception");
			}

			base.OnException(actionExecutedContext);
		}
	}

    /// <summary>
    /// Log WebAPI Exceptions to ELMAH
    /// (Note - This is a different filter to the main MVC error handling, already covered in web.config)
    /// </summary>
    public class ElmahErrorAttribute : System.Web.Http.Filters.ExceptionFilterAttribute
	{
		public override void OnException(
			System.Web.Http.Filters.HttpActionExecutedContext actionExecutedContext)
		{

			if (actionExecutedContext.Exception != null)
			{
				// NOTE - Disabled for now. the .Raise() call *hangs* under REST + IIS, and takes CPU usage to 100%
				// TODO: Evaluate the trade-off in fixing this (ELMAH is lovely for viewing messages, and we use lots of WebAPI), 
				//		versus how difficult it is to re-enable. It's easy enough to reproduce in dev environment

				//var errorSignal = Elmah.ErrorSignal.FromCurrentContext();
				//errorSignal.Raise(actionExecutedContext.Exception);
			}

			base.OnException(actionExecutedContext);
		}
	}

    
}