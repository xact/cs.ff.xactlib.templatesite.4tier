﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Core.AppHost.Controllers
{
    using App.Core.Constants.ReferenceData;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services;

    public class IndexController : ControllerBase
    {
        private readonly ITracingService _tracingService;
        private readonly ICachedImmutableReferenceDataMessageService _cachedImmutableReferenceDataMessageService;

        public IndexController(ITracingService tracingService, ICachedImmutableReferenceDataMessageService cachedImmutableReferenceDataMessageService)
        {
            _tracingService = tracingService;
            _cachedImmutableReferenceDataMessageService = cachedImmutableReferenceDataMessageService;
        }

        // GET: /Index/
        public ActionResult Index()
        {
            //var result = _cachedImmutableReferenceDataMessageService.GetReferenceData(ImmutableReferenceData.Gender);

            return View();
        }

    }
}