﻿
using System;
using System.Web.Mvc;
using System.Web.Razor.Text;
using System.Web.UI;
using App.Core.AppHost.Models;
using App.Core.Infrastructure;

namespace App.Core.AppHost.UI.Controllers
{
	//[Authorize]
    [OutputCache(NoStore = true, Duration = 0, Location = OutputCacheLocation.None, VaryByParam = "*")]
    public class HomeController : /*ControllerBase*/ Controller
    {
        public HomeController ()
        {
            
        }

        public ActionResult Index()
        {
	        var model = new HomeViewModel();

            return View("Index", model);
        }
		
    }
}
