﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR;
using Microsoft.Practices.ServiceLocation;

namespace XAct.Services.Comm.SignalR
{
    /// <summary>
    /// Service Locator based SignalR DependencyResolver.
    /// </summary>
    public class ServiceLocatorSignalRDependencyResolver : DefaultDependencyResolver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceLocatorSignalRDependencyResolver"/> class.
        /// </summary>
        public ServiceLocatorSignalRDependencyResolver()
        {
        }

        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <returns></returns>
        public override object GetService(Type serviceType)
        {
            try
            {
                return ServiceLocator.Current.GetInstance(serviceType);
            }
            catch
            {
                return base.GetService(serviceType);
            }
        }


        /// <summary>
        /// Gets the services.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <returns></returns>
        public override IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return ServiceLocator.Current.GetAllInstances(serviceType).Concat(base.GetServices(serviceType));

            }
            catch
            {
                return base.GetServices(serviceType);
            }
        }
    }
}