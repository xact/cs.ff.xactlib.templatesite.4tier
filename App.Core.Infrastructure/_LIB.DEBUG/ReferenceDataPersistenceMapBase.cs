﻿namespace XAct.Data
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;


    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public abstract class ReferenceDataPersistenceMapBase<T, TId> : EntityTypeConfiguration<T>
        where TId : struct
        where T : class, IHasReferenceData<TId>, IHasTimestamp, new()
    {
        private readonly DatabaseGeneratedOption _idDatabaseGeneratedOption;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReferenceDataPersistenceMapBase{T,TId}" /> class.
        /// </summary>
        /// <param name="tableNameSuffix">The table name suffix.</param>
        /// <param name="idDatabaseGeneratedOption">The identifier database generated option.</param>
        protected ReferenceDataPersistenceMapBase(string tableNameSuffix, DatabaseGeneratedOption idDatabaseGeneratedOption)
        {
            _idDatabaseGeneratedOption = idDatabaseGeneratedOption;
            if (!XAct.Library.Settings.Db.DefaultXActLibDbTablePrefix.IsNullOrEmpty())
            {
                this
                    .ToTable(
                        "{0}{1}".FormatStringInvariantCulture(
                            XAct.Library.Settings.Db.DefaultXActLibDbTablePrefix,
                            tableNameSuffix));
            }


            //The properties expression 'h => new <>f__AnonymousType0`2(TargetMachineId = h.MachineId, Id = h.Id)' is not valid. 
            //The expression should represent a property: C#: 't => t.MyProperty'  VB.Net: 'Function(t) t.MyProperty'. When specifying multiple properties use an anonymous type: C#: 't => new { t.MyProperty1, t.MyProperty2 }'  VB.Net: 'Function(t) New With { t.MyProperty1, t.MyProperty2 }'.
            this
                .HasKey(h => h.Id);



            int colOrder = 0;

            this
                .Property(m => m.Id)
                .HasDatabaseGeneratedOption(_idDatabaseGeneratedOption)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Timestamp)
                .IsRequired()
                .IsRowVersion()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.ResourceFilter)
                .IsOptional()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Text)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Description)
                .IsOptional()
                .HasMaxLength(4000)
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Filter)
                .IsOptional()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;


        }
    }
}
