﻿//namespace App.Core.Infrastructure.Data.DbContexts
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Data.Entity;
//    using App.Core.Infrastructure.Data.Maps;
//    using XAct.Data.EF.CodeFirst;
//    using XAct.Initialization;

//    /// <summary>
//    /// 
//    /// </summary>
//    /// <internal>
//    /// When <see cref="AppDbContext"/>
//    /// is invoked the first time, it's <see cref="AppDbContext"/>
//    /// will be invoked.
//    /// It in turn will use the Library's 'magic' method to search
//    /// the AppDomain for all Assemblies marked with <see cref="IDbModelBuilder"/>
//    /// and invoke each one to define their part of the global model.
//    /// </internal>
//    public class DbModelBuilderInitializers : IDbModelBuilder
//    {
//        public void OnModelCreating(DbModelBuilder modelBuilder)

//        {

//            //I don't see why we need this - it's slow and it's what dependency injection container ResolveAll(typeof(EntityTypeConfiguration<>))
//            //method would do for us
            

//            KeyValuePair<Type,EntityTypeMapAttribute>[] list = 
//                System.AppDomain.CurrentDomain.GetTypesDecoratedWithAttribute<EntityTypeMapAttribute>(null,true,false);

//            foreach (KeyValuePair<Type, EntityTypeMapAttribute> x in list)
//            {
//                if (x.Key.IsAbstract)
//                {
//                    //Skip the base class:
//                    continue;
//                }
//                dynamic instance = System.Activator.CreateInstance(x.Key);

//                modelBuilder.Configurations.Add(instance);
                
//            }


//        }
//    }
//}