﻿//using System.Data;
//using System.Data.Common;
//using System.Data.Entity;
//using XAct.Domain.Repositories;

//namespace App.Core.Infrastructure.Data
//{
//    using App.Core.Infrastructure.Data.DbContexts;

//    public class AppDbConnectionFactory
//    {
//        public IDbConnection CreateConnection()
//        {
//            return XAct.Data.DbHelpers.CreateConnection("AppDbContext");
//        }

//        public DbContext CreateContext()
//        {
//            IDbConnection tmp = CreateConnection();
//            DbConnection dbConnection = (DbConnection) tmp;

//            return new AppDbContext(dbConnection, true);
//        }

//        public IUnitOfWork CreateEntityDbContext()
//        {
//            return new EntityDbContext(CreateContext());
//        }
//    }
//}
