﻿//using App.Core.Infrastructure.Data.DatabaseInitializers;

//namespace App.Core.Infrastructure.Data
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Data.Entity;
//    using System.Diagnostics;
//    using App.Core.Infrastructure.Data.DbContexts;
//    using App.Core.Infrastructure.Services;
//    using App.Core.Infrastructure.Services;
//    using XAct;
//    using XAct.Commands;
//    using XAct.Data.EF.CodeFirst;
//    using XAct.Initialization;
//    using XAct.Library.Settings;

//    /// <summary>
//    /// Seed Invoker is invoked from potentially two places:
//    /// * As a migration step:
//    /// * <see cref="AppDbContextMigrateDatabaseToLatestVersionInitializer" />
//    /// * As an integration Step:
//    /// * <see cref="App.Core.Infrastructure.Integration.Steps.InvokeAllSeedersIntegrationStep" />
//    /// </summary>
//    public class SeedInvoker
//    {

//        public static bool Seeded;
//        public static bool _seeding;
//        private ITracingService _tracingService;
//        public static object _lock = new object();
//        private string _typeName;



//        /// <summary>
//        /// Invoked from:
//        /// * DbContext.Seed method (exposed for tests only)
//        /// * InvokeAllSeedersIntegrationStep
//        ///     * almost never used (only if DevFlagSeedEveryStartup=true)
//        /// * this method
//        /// </summary>
//        /// <remarks>
//        /// </remarks>
//        public void Seed(string typeName, DbContext dbContext)
//        {
//            if (Seeded)
//            {
//                //Don't do it twice (takes so long)
//                return;
//            }
//            lock (_lock)
//            {


//                var hostSettingsService = XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();

//                //Set the global var to make it easier for future checks within individual seeders:
//                XAct.Library.Settings.Db.SeedingType = hostSettingsService.SeedingType;


//                if (hostSettingsService.SeedingType == SeedingType.SkipSeeding)
//                {
//                    //Guess we do nothing...
//                    //???? Seeded = true;
//                    return;
//                }
//                SeedA(typeName, dbContext);
//            }
//        }

//        private void SeedA(string typeName, DbContext dbContext)
//        {
//            this._typeName = typeName;

//            //Should be safe to invoke it here (NOT IN CONSTRUCTOR)
//            this._tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>();

//            using (XAct.TimedScope timedScope1 = new TimedScope())
//            {

//                try
//                {
//                    Type[] seederTypes =
//                        AppDomain.CurrentDomain.GetAssemblies()
//                            //Get all instantiable -- classes -- implementing the IDbContextSeeder contract:
//                            .GetTypesInmplementingType<IDbContextSeeder>(true);

//                    Type lastOne;
//                    foreach (var seederType in seederTypes)
//                    {
//                        lastOne = seederType;
//                        var seeder = seederType.ActivateEx<IDbContextSeeder>();

//                        try
//                        {
//                            seeder.Seed(dbContext);
//                            dbContext.SaveChanges();
//                        }
//                        catch (System.Exception e)
//                        {
//                            _tracingService.TraceException(TraceLevel.Error, e, "Seeding failed for {0}!",
//                                seederType);
//                            throw;
//                        }
//                    }
//                }
//                catch (System.Exception e)
//                {
//                    _tracingService.TraceException(TraceLevel.Error, e, "Seeding failed!");
//                    throw;
//                }

//                Seeded = true;
//            }
//        }
//    }
//}
