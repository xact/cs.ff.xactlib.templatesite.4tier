﻿//namespace App.Core.Infrastructure.Data.DbContexts
//{
//    using App.Core.Infrastructure.Migrations;

//    public class MigrationsAppDbContext : AppDbContext
//    {

//        //private static bool _ioCInitialized = false;
//        //private static object _iocInitializedlock = new object();


//        /// <summary>
//        /// Initializes a new instance of the
//        ///  <see cref="MigrationsAppDbContext"/> class
//        /// which is a specialized case of the DbContext,
//        /// created especially for when invoked from the 
//        /// Continuous Integration server to 
//        /// apply Migrations (see <see cref="Configuration"/>)
//        /// </summary>
//        public MigrationsAppDbContext()
//        {

//            //By the time it gets here it will have run
//            //AppDbContext's parameterless constructor
//            //then it's single string parameter constructor.
//            //finally, do this constructor.

//            //This means that base parameterless constructor will have already 
//            //have had to 
//            //ensure IoC Container is up and running
//            //in order to set the DbInitializer, which relies on ITracingService...

//            //Conclusion:
//            //There is no way to move 
//            //EnsureServiceLocationIsInitializedAndReturnDbContextName()
//            //up to this subclass.

//            //

//        }

//        protected override void EnsureIoCContextWhenMigrating()
//        {

//            AppDbContextBootstrapper.EnsureIoCContextWhenMigrating();

//        }



//    }
//}
