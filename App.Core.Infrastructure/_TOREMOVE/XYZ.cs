//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using XAct;
//using XAct.Initialization;
//using XAct.Services;

//namespace App.Core.Infrastructure.Data.DbContexts
//{
//    public static class XYZ
//    {
//        /// <summary>
//        /// Find all class definitions in the library and current application
//        /// that are decorated with the given Attribute, that match the given
//        /// Interface, instantiate them, and invoke the given Action on them.
//        /// <para>
//        /// Useful for invoking <see cref="IInitializable" /> classes from a bootstrapper.
//        /// </para>
//        /// </summary>
//        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
//        /// <typeparam name="TClassContract">The type of the class contract.</typeparam>
//        /// <param name="appDomain">The app domain.</param>
//        /// <param name="action">The action.</param>
//        /// <param name="filter">The filter.</param>
//        /// <param name="optionalInvokeCallback">The optional invoke callback.</param>
//        /// <returns></returns>
//        /// <exception cref="System.Exception"></exception>
//        public static KeyValuePair<Type, TAttribute>[]
//            GetTypesDecoratedWithAttributeAndInstantiateAndInvokeInterfaceMethodDEBUG<TAttribute, TClassContract>(
//            this AppDomain appDomain,
//            Action<TClassContract> action,
//            Func<TAttribute, bool> filter,
//            Action<bool, Type, TAttribute, TClassContract> optionalInvokeCallback = null
//            )
//            where TAttribute : Attribute
//            where TClassContract : class
//        {

//            List<KeyValuePair<Type, TAttribute>> typesProcessed = new List<KeyValuePair<Type, TAttribute>>();

//            KeyValuePair<Type, TAttribute>[] found =
//                appDomain.GetTypesDecoratedWithAttribute<TAttribute, TClassContract>(filter);

//            bool check1 = AppDomain.CurrentDomain.GetAssemblies().Contains(a => a.GetName().Name.Contains("Assistance.Per"));

//            string[] check4 = AppDomain.CurrentDomain.GetAssemblies().Select(s => s.GetName().Name).ToArray();

//            Assembly ass = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.GetName().Name == "XAct.Assistance.Persistence.EF");

//            IDictionary<Type,DefaultBindingImplementationAttribute> tDict =  
//                ass.GetTypesDecoratedWithAttribute<XAct.Services.DefaultBindingImplementationAttribute>();


//            //XAct.DependencyResolver.BindingResults.BindingScanResults.AssemblyNames;

//            bool check2 =  XAct.DependencyResolver.BindingResults.BindingScanResults.BindingDescriptors.Contains(bd => bd.InterfaceType.Name.Contains("HelpDbModelBuilder"));
//            bool check3 = found.Contains(kvp => kvp.Key.Name.Contains("HelpDbModelBuilder"));


//            foreach (KeyValuePair<Type, TAttribute> pair in found)
//            {

//                //IMPORTANT: Remember that original collection
//                //can bring in all kinds of classes which are not of right interface


//                bool execute = true;

//                if (action != null)
//                {
//                    Type t = pair.Key;
//                    //TAttribute attribute = pair.Value;

//                    object o;


//                    TClassContract initializable;

//                    try
//                    {
//                        if (t.IsInterface)
//                        {
//                            o = XAct.DependencyResolver.Current.GetInstance(t);
//                        }
//                        else
//                        {
//                            o = Activator.CreateInstance(t);
//                        }

//                        initializable = o as TClassContract; // as TClassContract

//                        if (initializable == null)
//                        {
//                            string message = "GetTypesDecoratedWithAttributeAndInstantiateAndInvokeInterfaceMethod: Type:{0} Not of type {1}."
//                                .FormatStringCurrentCulture(pair.Key, typeof(TClassContract));
//                            throw new Exception(message);
//                        }

//                    }
//                    catch
//                    {
//                        initializable = null;
//                        execute = false;
//                    }


//                    if (execute)
//                    {
//                        try
//                        {
//                            action.Invoke(initializable);
//                        }
//                            // ReSharper disable RedundantCatchClause
//                        catch
//                        {
//                            throw;
//                        }
//                        // ReSharper restore RedundantCatchClause
//                    }

//                }
//                typesProcessed.Add(pair);
//            }
//            return typesProcessed.ToArray();
//        }



//        /// <summary>
//        ///   <para>
//        /// An XActLib Extension.
//        ///   </para>
//        /// Gets the types decorated with attribute.
//        ///   <para>
//        /// Usage Example:
//        ///   <code>
//        ///   <![CDATA[
//        /// Type[] results = appDomain.GetTypesDecoratedWithAttributes
//        ///   <DefaultServiceImplementationAttribute>(null, false,true);
//        /// ]]>
//        ///   </code>
//        ///   </para>
//        /// </summary>
//        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
//        /// <param name="appDomain">The app domain.</param>
//        /// <param name="filter">The filter.</param>
//        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
//        /// <param name="allowMultiples">if set to <c>true</c> [allow multiples].</param>
//        /// <returns></returns>
//        public static KeyValuePair<Type, TAttribute>[] GetTypesDecoratedWithAttribute<TAttribute>(
//            this AppDomain appDomain, Func<TAttribute, bool> filter = null, bool inherit = false, bool allowMultiples = false)
//            where TAttribute : Attribute
//        {

//            Assembly[] assemblies = appDomain.GetAssemblies();
//            return assemblies.GetTypesDecoratedWithAttribute(filter, inherit, allowMultiples);
//        }

//        /// <summary>
//        /// Find all class definitions in the library and current application
//        /// that are decorated with the given Attribute, that match the given
//        /// Interface, instantiate them, and invoke the given Action on them.
//        /// <para>
//        /// Useful for invoking <see cref="IInitializable" /> classes from a bootstrapper.
//        /// </para>
//        /// </summary>
//        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
//        /// <typeparam name="TClass">The type of the class contract.</typeparam>
//        /// <param name="appDomain">The app domain.</param>
//        /// <param name="filter">The filter.</param>
//        /// <returns></returns>
//        /// <exception cref="System.Exception"></exception>
//        public static KeyValuePair<Type, TAttribute>[] GetTypesDecoratedWithAttribute<TAttribute, TClass>(
//            this AppDomain appDomain,
//            Func<TAttribute, bool> filter
//            )
//            where TAttribute : Attribute
//            where TClass : class
//        {

//            Assembly[] assemblies = appDomain.GetAssemblies();
//            Assembly[] check = assemblies.OrderBy(x=>x.GetName().Name).ToArray();

//            KeyValuePair<Type, TAttribute>[] result = assemblies.GetTypesDecoratedWithAttribute<TAttribute, TClass>(filter);

//            return result;
//        }



//        public static KeyValuePair<Type, TAttribute>[] GetTypesDecoratedWithAttribute<TAttribute>(
//            this Assembly[] assemblies,
//            Func<TAttribute, bool> filter = null, bool inherit = false, bool allowMultiples = false)
//            where TAttribute : Attribute
//        {

//            List<KeyValuePair<Type, TAttribute>> results = new List<KeyValuePair<Type, TAttribute>>();

//            //Dictionary of ClassInstances to their 
//            //Dictionary<Type, TAttribute> results = new Dictionary<Type, TAttribute>();
//            foreach (Assembly assembly in assemblies)
//            {
//                results.AddRange(assembly.GetTypesDecoratedWithAttibutes(filter, inherit, allowMultiples));
//            }

//            return results.ToArray();
//        }


//        /// <summary>
//        /// Gets the types decorated with attibutes.
//        /// </summary>
//        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
//        /// <param name="assembly">The assembly.</param>
//        /// <param name="filter">The filter.</param>
//        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
//        /// <param name="allowMultiples">if set to <c>true</c> [allow multiples].</param>
//        /// <returns></returns>
//        public static List<KeyValuePair<Type, TAttribute>> GetTypesDecoratedWithAttibutes<TAttribute>(this Assembly assembly, Func<TAttribute, bool> filter, bool inherit, bool allowMultiples)
//            where TAttribute : Attribute
//        {

//            List<KeyValuePair<Type, TAttribute>> results = new List<KeyValuePair<Type, TAttribute>>();

//            foreach (Type type in assembly.GetTypesDecoratedWithAttributes<TAttribute>(filter, inherit))
//            {
//                foreach (TAttribute attribute in type.GetCustomAttributes(typeof(TAttribute), inherit))
//                {
//                    results.Add(new KeyValuePair<Type, TAttribute>(type, attribute));

//                    //We have each Type...but the Type could have several copies of the Attribute
//                    if (!allowMultiples)
//                    {
//                        break;
//                    }
//                }
//            }
//            return results;
//        }





//        public static KeyValuePair<Type, TAttribute>[] GetTypesDecoratedWithAttribute<TAttribute, TClass>(
//            this Assembly[] assemblies,
//            Func<TAttribute, bool> filter
//            )
//            where TAttribute : Attribute
//            where TClass : class
//        {

//            //Get all types in appDomain decorated with given Attribute:
//            IQueryable<KeyValuePair<Type, TAttribute>> query =
//                assemblies.GetTypesDecoratedWithAttribute<TAttribute>(filter).AsQueryable();


//            //IMPORTANT: Note that this can bring in all kinds of classes which are not of right interface
//            //So have to filter out:
//            query = query.Where(kvp => typeof(TClass).IsAssignableFrom(kvp.Key));
//            int count1 = query.Count();

//            //Organize initialization by Initialization Stage:
//            if (typeof(IHastInitializationStage).IsAssignableFrom(typeof(TAttribute)))
//            {
//                query = query.OrderBy(kvp => ((IHastInitializationStage)kvp.Value).InitializationStage);
//            }
//            int count2 = query.Count();

//            //Filter by Priority:
//            if (typeof(IHasPriorityReadOnly).IsAssignableFrom(typeof(TAttribute)))
//            {
//                query = query.OrderBy(kvp => ((IHasPriorityReadOnly)kvp.Value).Priority);
//            }
//            int count3 = query.Count();

//            //Then by Order:
//            if (typeof(IHasOrderReadOnly).IsAssignableFrom(typeof(TAttribute)))
//            {
//                query = query.OrderBy(kvp => ((IHasOrderReadOnly)kvp.Value).Order);
//            }
//            int count4 = query.Count();

//            KeyValuePair<Type, TAttribute>[] result = query.ToArray();
//            return result;
//        }

//        /// <summary>
//        ///   <para>
//        /// An XActLib Extension.
//        /// </para>
//        /// Gets all public types in the assembly that are decorated with the specified attribute.
//        /// </summary>
//        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
//        /// <param name="assembly">The assembly.</param>
//        /// <param name="filter">The filter.</param>
//        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
//        /// <returns></returns>
//        /// <internal>8/15/2011: Sky</internal>
//        public static Type[] GetTypesDecoratedWithAttributes<TAttribute>(this Assembly assembly,
//            Func<TAttribute, bool> filter = null,
//            bool inherit = true)
//            where TAttribute : class
//        {

//            Type attributeType = typeof(TAttribute);

//            //FIX: See: http://bit.ly/tL84yI
//#if NET35
//            if (assembly.ManifestModule.GetType().Namespace == "System.Reflection.Emit")
//#else
//            if (assembly.IsDynamic)
//#endif
//            {
//                return new Type[0];
//            }

//            List<Type> tmp = new List<Type>();
//            try
//            {
//                var types = GetAssemblyTypes<TAttribute>(assembly);

//                try
//                {
//                    foreach (Type type in types)
//                    {
//                        Type t = typeof(TAttribute);

//                        object[] foundAttributes = type.GetCustomAttributes(t, inherit);

//                        if (foundAttributes.Length == 0)
//                        {
//                            continue;
//                        }

//                        bool ok = true;
//                        if (filter != null)
//                        {
//                            foreach (object foundAttributeObject in foundAttributes)
//                            {

//                                TAttribute foundAttribute = foundAttributeObject as TAttribute;
//                                if (foundAttribute != null)
//                                {
//                                    if (!filter.Invoke(foundAttribute))
//                                    {
//                                        ok = false;
//                                        break;
//                                    }
//                                }
//                            }
//                        }

//                        if (!ok)
//                        {
//                            continue;
//                        }
//                        tmp.Add(type);
//                    }
//                }
//                catch
//                {
//                    //System.Diagnostics.Debug.WriteLine(e.Message);
//                }
//            }
//#pragma warning disable 168
//            catch (System.TypeLoadException e)
//#pragma warning restore 168
//            {
//                //Error is expected from some assemblies.
//                //eg: Could not load type 'Microsoft.AspNet.SignalR.Hosting.IWebSocketRequest' from 
//                //assembly 'Microsoft.AspNet.SignalR.Core, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35'.
//                //TraceExceptionIfTracingServiceAvailable(TraceLevel.Verbose, e, "Eating error.");
//            }
//#pragma warning disable 168
//            catch
//#pragma warning restore 168
//            {
//                //TraceExceptionIfTracingServiceAvailable(TraceLevel.Verbose, e, "Eating error.");
//            }
//            Type[] results =
//                tmp.ToArray();

//            return results;
//        }

//        [System.Diagnostics.DebuggerHidden]
//        private static Type[] GetAssemblyTypes<TAttribute>(Assembly assembly) where TAttribute : class
//        {
//            Type[] types = assembly.GetExportedTypes();
//            return types;
//        }
//    }
//}