﻿//// ReSharper disable CheckNamespace
//namespace App.Core.Infrastructure.Data
//// ReSharper restore CheckNamespace
//{
//    using System.Data.Entity;
//    using System.Diagnostics;
//    using App.Core.Infrastructure.Services;

//    public class AppDbDropCreateDatabaseIfModelChanges<TDbContext> : DropCreateDatabaseIfModelChanges<TDbContext>
//        where TDbContext : DbContext
//    {
//        private readonly string _typeName;
//        private readonly ITracingService _tracingService;


//        ///// <summary>
//        ///// IMPLEMENTATION OF IDatabaseInitializer<TContext> 
//        /// COPIED FROM BASE METHOD
//        ///// Executes the strategy to initialize the database for the given context.
//        ///// </summary>
//        ///// <param name="context">The context.</param>
//        //public void InitializeDatabase(TDbContext context)
//        //{
//        //  RuntimeFailureMethods.Requires((object) context != null, (string) null, "context != null");
//        //  bool flag;
//        //  using (new TransactionScope(TransactionScopeOption.Suppress))
//        //    flag = context.Database.Exists();
//        //  if (flag)
//        //  {
//        //    if (context.Database.CompatibleWithModel(true))
//        //      return;
//        //    context.Database.Delete();
//        //  }
//        //  context.Database.Create();
//        //  this.Seed(context);
//        //  context.SaveChanges();
//        //}



//        /// <summary>
//        /// Initializes a new instance of the <see cref="UnitTestDatabaseInitializer"/> class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        public AppDbDropCreateDatabaseIfModelChanges(ITracingService tracingService)
//        {
//            _typeName = this.GetType().Name;
//            //Repository settings were used for when not being built using CodeFirst.
//            //Keeping it just in case
//            _tracingService = tracingService;
//        }

//        protected override void Seed(TDbContext dbContext)
//        {


//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Begin...", _typeName, "Seed");

//            //Reminder:
//            //Core DbContext is not the entry point.
//            //Entry is via M01DbContext in this assembly.
//            //Which sets this DatabaseInitializer.
//            //No other DatabaseInitializer should be being called (as they are not the entry point).
//            //Which uses Reflection to find all the IDbContextSeeders (Core First, then Modules.

//            //This Db Initializer in turn looks around (via reflection)
//            //for all instances decorated with the IDbContextSeeder interface
//            //in order for them to have a chance to seed the database as required.

//            new SeedInvoker().Seed(_typeName,dbContext);


//            //The downside of this approach is that it won't find everything. Can't because not every test is referencing
//            //every other test, and therefore they don't all end up in the bin of the current test.

//            base.Seed(dbContext);


//            //You have to ensure all changes are Committed before continuing...
//            dbContext.SaveChanges();

//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Complete.", _typeName, "Seed");
//        }

//    }
//}

