﻿namespace XAct
{
    using XAct;
    using App.Core.Domain.Entities;

    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// IHasReferenceData is a app specific contract, so these extensions are app specific 
    /// (rather than FW).
    /// </remarks>
    public static class IHasReferenceExtensionMethods
    {

        public static T X<T,TId>(this T entity, TId id, string resourceFilter, string textOrresourceKey, string ministryCode, bool enabled = true, int order = 0,
                             string description = null) where T : IHasCodedReferenceData<TId>
            where TId:struct
        {
            entity.Id = id;
            entity.Code = ministryCode;
            entity.Enabled = enabled;
            entity.Order = order;
            //entity.ResourceFilter = resourceFilter;
            entity.Text = textOrresourceKey; //this will be the string value of the Id's enum (eg 'Male'in the case of Id being Gender.Male enum)
            entity.Description = description;
            //entity.Filter =;
            //entity.Tag =;
            
            return entity;
        }

    }
}