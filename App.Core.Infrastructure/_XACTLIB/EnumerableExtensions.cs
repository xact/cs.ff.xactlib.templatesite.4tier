﻿namespace App
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
	/// Provides a set of static methods for extending IEnumerable collections.
	/// </summary>
	public static class EnumerableExtensions
	{		
		/// <summary>
		/// Returns true when collection is empty.
		/// </summary>
		/// <typeparam name="TSource">The type of the source.</typeparam>
		/// <param name="source">The System.Collections.Generic.IEnumerable to check for emptiness.</param>
		/// <returns></returns>
		public static bool None<TSource>(this IEnumerable<TSource> source)
		{
			return !source.Any();
		}

		public static IList<TSource> Except<TSource>(this IEnumerable<TSource> source, TSource item)
		{
			return source.Except(new List<TSource> { item }).ToList();
		}
	}
}
