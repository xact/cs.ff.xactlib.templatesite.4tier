﻿namespace App
{
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity;
    using System.Linq;

    public static class DBContextExtensions
    {
        public static bool IsDirty(this DbContext context)
	    {
		    return context.ChangeTracker
		                  .Entries()
		                  .Any(IsDirty);
	    }

	    public static bool IsDirty(this DbEntityEntry entry)
	    {
		    return	  entry.State == EntityState.Added
		           || entry.State == EntityState.Modified
		           || entry.State == EntityState.Deleted;
	    }


    }
}