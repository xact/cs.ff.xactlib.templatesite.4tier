using System.Text.RegularExpressions;
using XAct;

namespace XAct{

    public static class StringExtensions

    {
        public static string ToSentenceCase(this string text)
        {
            if (text.IsNullOrEmpty())
            {
                return text;
            }

            // start by converting entire string to lower case
            string lowerCase = text.ToLower().Trim();
            // matches the first sentence of a string, as well as subsequent sentences
            Regex r = new Regex(@"(^[a-z])|\.\s+(.)", RegexOptions.ExplicitCapture);
            // MatchEvaluator delegate defines replacement of setence starts to uppercase
            string result = r.Replace(lowerCase, s => s.Value.ToUpper());

            return result;
        }
    }
}