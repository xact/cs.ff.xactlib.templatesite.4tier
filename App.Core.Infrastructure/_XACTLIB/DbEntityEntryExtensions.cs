﻿namespace App
{
    using System;
    using System.Linq;
    using System.Data.Entity.Infrastructure;

    public static class DbEntityEntryExtensions
    {
        public static TValue GetOldValue<TValue, TEntity>(this DbEntityEntry<TEntity> entry, string propertyName) where TEntity : class
        {
            var result = entry.OriginalValues[propertyName];
            if (result == null) return default(TValue);

            if (!(result is TValue)) throw new Exception(string.Format("Property {0} is of type {1} and cannot be casted to {2}", propertyName, result.GetType(), typeof(TValue)));
            return (TValue)result;
        }

        public static TValue GetNewValue<TValue, TEntity>(this DbEntityEntry<TEntity> entry, string propertyName) where TEntity : class
        {
            var result = entry.CurrentValues[propertyName];
            if (result == null) return default(TValue);

            if (!(result is TValue)) throw new Exception(string.Format("Property {0} is of type {1} and cannot be casted to {2}", propertyName, result.GetType(), typeof(TValue)));
            return (TValue)result;
        }

        public static bool IsUpdated<TValue, TEntity>(this DbEntityEntry<TEntity> entry, params string[] propertyName) where TEntity : class
        {
            var result = propertyName
                .Any(p => !entry.GetNewValue<TValue, TEntity>(p).EqualsWithNullCheck(entry.GetOldValue<TValue, TEntity>(p)));
            return result;
        }

        public static bool EqualsWithNullCheck(this object o1, object o2)
        {
            if (o1 == null) return o2 == null; //if o1 is null o2 must be as well otherwise return false
            return o1.Equals(o2); //if o1 and o2 is not null perform normal comparison
        }

        public static string ToStringWithNullCheck(this object o)
        {
            return o != null ? o.ToString() : string.Empty;
        }

        #region Non-Generic Version

        public static object GetNewValue(this DbEntityEntry entry, string propertyName)
        {
            return entry.CurrentValues[propertyName];
        }

        public static object GetOldValue(this DbEntityEntry entry, string propertyName)
        {
            return entry.OriginalValues[propertyName];
        }

        public static bool IsUpdated(this DbEntityEntry entry, params string[] propertyName)
        {
            var result = propertyName.Any(p => !entry.GetNewValue(p).Equals(entry.GetOldValue(p)));
            return result;
        }

        #endregion
    }
}