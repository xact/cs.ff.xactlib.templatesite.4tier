using System.Data.Entity;

namespace App.Core.Infrastructure.Data.DbContexts
{
    public static class DbContextUtils<TContext>
        where TContext : DbContext
    {
        static object _InitializeLock = new object();
        static bool _InitializeLoaded = false;

        /// <summary>
        /// Method to allow running a DatabaseInitializer exactly once
        /// </summary>   
        /// <param name="initializer">A Database Initializer to run</param>
        public static bool SetInitializer(IDatabaseInitializer<TContext> initializer, bool force)
        {
            if (force)
            {
                _InitializeLoaded = false;
            }

            if (_InitializeLoaded)
            {
                return false;
            }

            // watch race condition
            lock (_InitializeLock)
            {
                // are we sure?
                if (_InitializeLoaded)
                {
                    return false;
                }

                _InitializeLoaded = true;

                // force Initializer to load only once
                System.Data.Entity.Database.SetInitializer<TContext>(initializer);

                return true;
            }
        }
    }
}