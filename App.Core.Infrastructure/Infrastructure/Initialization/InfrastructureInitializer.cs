﻿
using App.Core.Infrastructure.Initialization.Steps.PostIoC;
using App.Core.Infrastructure.Initialization.Steps.PreIoC;

namespace App.Core.Infrastructure.Initialization
{
    using App.Core.Infrastructure.Initialization.Steps;

    //TODO : refactor to methods and calls or justtify why it's done this way - hard to read/debug/find mistakes/reuse/understand
    public static class InfrastructureInitializer
    {

        /// <summary>
        /// Part of the initialization sequence chain.
        /// <para>
        /// The initialization sequence is as follows:
        /// <code>
        /// * Global.asax 
        ///     * ... invokes AppHostInitializer,
        ///         * ... which first invokes ApplicationInitializer
        ///             * ... which first invokes InfrastructureInitializer  (this class)
        ///               where IoC (ie Unity) is initialized with Infrastructure Services...
        ///         * ....now, coming back up...Application layer services are registered...
        ///     * ... now that both Infras and presentation are initialized, AppHostInitializer
        ///       finishes up by registering AppHost level Services.
        /// * Initialization done...
        /// * Integration cycle then starts...
        ///   It too goes down through the layers, and back up, in much the same way... 
        ///   but this time actually using remote services (eg, to load caches, etc).      
        /// </code>
        /// </para>
        /// </summary>
        public static void Execute()
        {

            new EnsureDefaultTracerWhenInDebugModeInitializationStep().Execute();
            new EnsureAccessToEventLogInitializationStep().Execute();
            new HardReferencesToEnsureDbMigrationOccursOnCiServer().Execute();
            new RegisterAllServicesInIoCInitializationStep().Execute();
            new ConfigureNConfigInitializationStep().Execute();

        }

    }


}
