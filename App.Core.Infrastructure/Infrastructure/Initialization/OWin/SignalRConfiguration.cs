﻿using Microsoft.Owin;

//MUST: as per http://www.asp.net/aspnet/overview/owin-and-katana/owin-startup-class-detection
//Requires the following attribute/
[assembly: OwinStartup(typeof(App.Core.Infrastructure.Initialization.OWin.SignalRStartup))]


namespace App.Core.Infrastructure.Initialization.OWin
{
    using Microsoft.AspNet.SignalR;
    using Owin;
    using XAct.Services;

    public class SignalRStartup
    {
        /// <summary>
        /// Configurations the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        public void Configuration(IAppBuilder app)
        {
            InitializeSignalR(app);
        }

        private static void InitializeSignalR(IAppBuilder app)
        {
            IAppSqlScaleoutConfiguration sqlScaleoutConfiguration = new AppSqlScaleoutConfiguration();

            GlobalHost.DependencyResolver.UseSqlServer((SqlScaleoutConfiguration) sqlScaleoutConfiguration);

            GlobalHost.HubPipeline.AddModule(new AppSignalRHubPipelineModule());

            //GlobalHost.DependencyResolver = ServiceLocatorSignalRDependencyResolver
            //Create a Factory for 
            //GlobalHost.DependencyResolver.Register(
            //            typeof(AppSignalRHubPipelineModule),
            //            () => new AppSignalRHubPipelineModule(...));


            HubConfiguration hubConfiguration = new HubConfiguration
                {
                    EnableDetailedErrors = true,
                    EnableJavaScriptProxies = true,
                    EnableJSONP = true
                };

            app.MapSignalR(hubConfiguration);
        }
    }


    public interface IAppSqlScaleoutConfiguration { }
}