﻿namespace App.Core.Infrastructure.Initialization.OWin
{
    using System.Configuration;
    using Microsoft.AspNet.SignalR;

    public class AppSqlScaleoutConfiguration : SqlScaleoutConfiguration, IAppSqlScaleoutConfiguration
    {
        public AppSqlScaleoutConfiguration() : base(ConfigurationManager.ConnectionStrings["AppDbContext.SignalR"].ConnectionString) { }

        public AppSqlScaleoutConfiguration(string connectionString) : base(connectionString) { }
    }
}