﻿
namespace App.Core.Infrastructure.Initialization.Steps.PostIoC
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using XAct;
    using XAct.Services;
    using XAct.Services.IoC.Initialization;
    using XAct.Settings;


    // Note: Not decorated with DefaultBindingImplementation as invoked before Services Registered.
    public class ConfigureNConfigInitializationStep : IHasInitializationStep
    {
        // This step disables NConfig in Release mode.
		// We've had issues were sometimes NConfig can crash the app, so it must be
		// disabled outside of dev. (See Jira #1333)



        /// <summary>
        /// Initializations the step registration of services definitions into the io C.
        /// </summary>
        /// <internal>
        /// This method can't be turned into an IHasInitializationStep because
        /// the IoC it is going to initialize has not been initialized...
        /// NOTE: remain public so that it can be invoked by DbContext when necessary
        /// from DbMigrations.
        ///   </internal>
        public void Execute()
        {
            //NConfigInitializer.UseNConfig = true;

		}
    }
}