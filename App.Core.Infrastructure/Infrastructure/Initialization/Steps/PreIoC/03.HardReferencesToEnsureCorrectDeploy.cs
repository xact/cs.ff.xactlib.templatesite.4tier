﻿using XAct.Users.Initialization.Implementations;

namespace App.Core.Infrastructure.Initialization.Steps.PreIoC
{
    using App.Core.Infrastructure.Services.Implementations;
    using Common.Logging.Simple;
    using NLog;
    using System;
    using AutoMapper.Internal;
    using XAct;
    using XAct.Assistance.Initialization.Implementation;
    using XAct.Diagnostics.Initialization.Implementations;
    using XAct.Diagnostics.Performance.Implementations;
    using XAct.IO.TemplateEngines.Implementations;
    using XAct.IO.Transformations.Services.Implementations;
    using XAct.Net.Initialization.Implementations;
    using XAct.Resources.Initialization.Inplementations;
    using XAct.Scheduling.Initialization.Inplementations;
    using XAct.Security.Authorization.Initialization.Implementations;
    using XAct.Scheduling;
    using XAct.Settings.Initialization.Implementations;

    /// <summary>
    /// 
    /// </summary>
    internal class HardReferencesToEnsureDbMigrationOccursOnCiServer : IHasInitializationStep
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HardReferencesToEnsureDbMigrationOccursOnCiServer"/> class.
        /// </summary>
        public HardReferencesToEnsureDbMigrationOccursOnCiServer()
        {
        }


        public void Execute()
        {
            //This method will never be called, but creates a hard link to classes
            //within problematic assemblies that are only referred to indirectly, via
            //reflection.
            //Due to the way that MSBuild optimises, it leaves behind assemblies it sees no 
            //hard link to...the omission of causing failed WebDeploy packages.
            //Symptoms are:
            //Builds locally, but requests a Code Migration. When you deploy to Build Server
            //Publish.proj complains that model is not in sync. What is happening is that
            //Build server is seeing all code migrations. But not seeing Assembly that would
            //cause the migration (ie, from its wrong point of view, it has "one code migration too far").

            //This is a work around.
            Type[] hardReferences =
                new Type[]
                    {
                                                typeof(TracingService), 
                        typeof(HelpDbModelBuilder),
                        typeof(ResourceDbModelBuilder),
                        typeof(SecurityDbModelBuilder),
                        typeof(MessagingDbModelBuilder),
                        typeof(EFSettingsDbModelBuilder),
                        typeof(NVelocityTemplateService),
                        typeof(PersistedFileDbModelBuilder),
                        typeof(CounterDbModelBuilder),
                        typeof(SchedulingDbModelBuilder),
                        typeof(MarkdownDeepMarkdownService),
                        typeof(NullableConverterFactory),

                        typeof(UserDbModelBuilder),
                        typeof(HelpDbModelBuilder),
                        typeof(MessagingDbModelBuilder),
                        typeof(PersistedFileDbModelBuilder),
                        typeof(CounterDbModelBuilder),
                        typeof(ResourceDbModelBuilder),
                        typeof(SchedulingDbModelBuilder),
                        typeof(SecurityDbModelBuilder),
                        typeof(EFSettingsDbModelBuilder),
                        typeof(NVelocityTemplateService),
                        typeof(MarkdownDeepMarkdownService),
                        typeof(NullableConverterFactory),
                        //Not sure this is going to be a strong enough bond to get 
                        //typeof(SchedulingDbModelBuilder), across:
                        typeof(QuartzSchedulerController),
						typeof(CommonLoggingTraceListener),
                        typeof(WindowsSystemPerformanceCounterService),
						typeof(NLogTraceListener)
                    };

			// AutoMapper - Ensure that their 'Net4' DLL is *used* in the code.
			// -- This is required to have the correct DLLs packaged with a *release* build
			// -- The class references above aren't enough (they only work for debug mode releases)
			NullableConverterFactory trickDirectReferenceToAutoMapperNet4 = new NullableConverterFactory();
			bool trickUtiliseAutoMapper4 = trickDirectReferenceToAutoMapperNet4.IsEnum();




        }
    }
}
