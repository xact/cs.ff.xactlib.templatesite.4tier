﻿namespace App.Core.Infrastructure.Initialization.Steps.PreIoC
{
    using System;
    using System.Diagnostics;
    using XAct;


    /// <summary>
    /// Initialization Step used to ensure app has enough rights
    /// to successfully write to EventLog
    /// </summary>
    /// <internal>
    /// Note: Not decorated with DefaultBindingImplementation as invoked before Services Registered.
    /// </internal>
    public class EnsureAccessToEventLogInitializationStep : IHasInitializationStep
    {
        public void Execute()
        {
            string tmp = System.Configuration.ConfigurationManager.AppSettings[Constants.Infrastructure.Tracing.EventLog.AppSettingEventLogName];

            if (tmp.IsNullOrEmpty()) {return;}
            string[] sourceNames = tmp.Split(new char[] {',', '|', ';'}, StringSplitOptions.RemoveEmptyEntries);

            foreach (string sourceName in sourceNames)
            {
                try
                {
                    if (!EventLog.SourceExists(tmp))
                    {
                        EventSourceCreationData data = new EventSourceCreationData(sourceName, "Application");
                        EventLog.CreateEventSource(data);
                    }
                    EventLog.WriteEntry(sourceName, "Check:can write to EventLog.");
                }
                catch
                {
                    string name = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                    new DefaultTraceListener().TraceEvent(new TraceEventCache(), "APP", TraceEventType.Error, 0,
                                                          "Could not Create '{0}' Application EventLog or write to it."
                                                              .FormatStringInvariantCulture(sourceName));
                }
            }
        }
    }
}
