﻿namespace App.Core.Infrastructure.Initialization.Steps.PreIoC
{
    using System.Diagnostics;
    using System.Linq;
    using XAct;

    /// <summary>
    /// Implementation of a
    /// Contract for InitializationStep 
    /// that ensures that a DefaultTraceListener (emitting
    /// to VisualStudio Debug environment) is available when required -- but
    /// only in debug mode.
    /// </summary>
    /// <internal>
    /// Note: Not decorated with DefaultBindingImplementation as invoked before Services Registered.
    /// </internal>
    public class EnsureDefaultTracerWhenInDebugModeInitializationStep : IHasInitializationStep
    {

        public void Execute()
        {
#if DEBUG
            // Only add Default Debug TraceListener in Debug mode. It's 2 orders of magnitude slower than 
            // other trace listeners: http://stackoverflow.com/questions/8010255/net-tracing-what-is-the-default-listener
            // but helpful to developers who don't want to go fish in files.

            // At this early stage, HostSettingsService is not yet available
            // as we havn't registered services yet.
            // Use Configuration directly:
            string tmp = System.Configuration.ConfigurationManager.AppSettings["EnsureDefaultTraceListenerIsActiveInDebugMode"];

            if (tmp.ToBool())
            {
                if (Trace.Listeners.OfType<DefaultTraceListener>().Any() == false)
                {
                    Trace.Listeners.Add(new DefaultTraceListener());
                }
            }
#endif
		}
    }
}