﻿
using App.Core.Infrastructure.Services;

namespace App.Core.Infrastructure.Initialization.Steps.PreIoC
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    //using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Diagnostics.Performance.Services.Configuration;
    using XAct.Services;
    using XAct.Services.IoC.Initialization;

    // Note: Not decorated with DefaultBindingImplementation as invoked before Services Registered.
    public class RegisterAllServicesInIoCInitializationStep : IHasInitializationStep
    {
        //
        //REMOVED DUE TO FLAG NOW BEING WITHIN MigrationsAppDbContext.
        //
        ////This flag is for a hack needed for Code Migrations.
        ////When the Migrator runs, it creates an instance of DbContext
        ////without any app running before it -- therefore no Bootstrapping
        ////of services has initialized. 
        ////To get around it, we've done this awful hack where AppDbContext
        ////actually checks to see Bootstrapping has occurred.
        ////There must be a better way -- but not now.
        //public static bool Initialized { get; set; }





        /// <summary>
        /// Initializations the step registration of services definitions into the io C.
        /// </summary>
        /// <internal>
        /// This method can't be turned into an IHasInitializationStep because
        /// the IoC it is going to initialize has not been initialized...
        /// NOTE: remain public so that it can be invoked by DbContext when necessary
        /// from DbMigrations.
        ///   </internal>
        public void Execute()
        {


                        //KeyValuePair<Type, DefaultBindingImplementationAttribute>[] r =
                        //        CHECK_GetTypesDecoratedWithAttribute<DefaultBindingImplementationAttribute>(AppDomain.CurrentDomain, null, false, true)
                        //        .OrderByDescending(kvp => kvp.Value.Priority)
                        //        .ToArray();


            XAct.Library.Settings.IoC.UseTempAppDomainWhenScanningForServices = false;





            //Create the standard object to hold all the information returned:
            BindingScanResults bindingScanResults = new BindingScanResults();

            //Now scan all assemblies for services decorated with the 
            //Attribute:
            System.AppDomain.CurrentDomain.ScanForDefaultBindingDescriptors(
                ref bindingScanResults,
                new IBindingDescriptor[0],
                true);
    

            //For the applicatio to function, all the available 
            //services -- those that are app specific, and those
            //defined in the underlying vendor libraries  -- have
            //to be registed in the IoC engine (Unity).

            //By hand,it's a rather tedious job. 
            //The library automates this by searching the assemblies
            //by reflection, finding all class definitions that 
            //are decorated witht the DefaultBindingImplementation
            //interface, and registers them.

            //Rather elegant, actually, as the whole service registration
            //can all be handled with one line:
            XAct.Services.IoC.UnityBootstrapper.Initialize(
                null, 
                false,
                null, 
                null, 
                bindingScanResults.BindingDescriptors);



            WriteReportToHardDrive();

            //An example of a custom app specific binding registered early
            //superceding any service that would have been found by reflection
            //(ie: service class decorated with DefaultBindingImplementationAppribute)
            //and automaticall registered (by this Initialize method):.
            //new BindingDescriptor<ITracingService, MyTracingService>()

            //Temp Fix:
            XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterServiceConfiguration>()
                .RaiseExceptionsIfPerformanceCounterNotFound = false;
        }

        private static void WriteReportToHardDrive()
        {
            string[] bindingReport = XAct.DependencyResolver.BindingResults.CreateBindingReport();

            ;
            string fullName =
                System.IO.Path.Combine(XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().ApplicationBasePath,
                                       "..\\..\\bindingReport.txt");

            System.IO.File.WriteAllLines(fullName, bindingReport);
        }
    }
}