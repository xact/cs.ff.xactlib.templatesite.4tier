﻿namespace App.Core.Infrastructure.SignalR
{
    using App.Core.Infrastructure.Services;
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;

    //Created every time a UserAgent contacts the Server

    //If no HubName specified, would be camelCase of ClassName.
    //The HubName is how the javascript finds the hub.
    [HubName("appMessageHub")]
    public class AppMessageHub : Hub
    {
        // ReSharper disable NotAccessedField.Local
        private readonly ITracingService _tracingService;
        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Initializes a new instance of the <see cref="AppMessageHub"/> class.
        /// <para>
        /// As we don't yet have a ServiceLocator based HubFactory,
        /// we hack DI this way...
        /// </para>
        /// </summary>
        public AppMessageHub()
            : this(XAct.DependencyResolver.Current.GetInstance<ITracingService>())
        {
        }

        //No specific 
        // ReSharper disable RedundantBaseConstructorCall
        public AppMessageHub(ITracingService tracingService)
            : base()
        // ReSharper restore RedundantBaseConstructorCall
        {
            _tracingService = tracingService;
        }


        public void Send(string sender, string message)
        {
            // Call the JS method 'broadcastMessage', 
            // (not the C# Hub wrapper) passing the args:
            Clients.All.broadcastMessage(sender, message);
        }

    }
}