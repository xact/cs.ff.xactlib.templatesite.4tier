﻿
namespace App.Core.Infrastructure.Services
{
    using System;
    using System.Diagnostics;
    using System.ServiceModel;
    using System.ServiceModel.Dispatcher;
    using App.Core.Domain.Messages;
    using App.Core.Domain.Models;
    using App.Core.Domain.Security;
    using App.Core.Services;
    using App.Core.Shared.Services.Services.Security;
    using XAct.Messages;

    public class CorrelationInfo
    {
        public Guid? SessionToken { get; set; }
        public int RequestCounter { get; set; }
        public bool ReturnSession { get; set; }
        public Response<SessionResponse> SessionResponse { get; set; }
    }

    /// <summary>
    /// SOAP Message Inspector on Back Tier for Secured Invocations
    /// (not anonymous calls).
    /// <para>
    /// Sole purpose of the Dispatcher is to inspect incoming 
    /// messages to secured Service endpoints, looking for a SessionToken.
    /// If SessionToken, sees if Session is in Cache.
    /// If not found in cache loads Session Record from the Db,
    /// and deserializes AppIdentity, and saves it to Cache.
    /// AppIdentity is then attached to thread (whether it came from 
    /// Cache, or from Db, then from Cache).
    /// </para>
    /// </summary>
    /// <internal>
    /// </internal>
    public class SecurityDispatchMessageInspector : IDispatchMessageInspector
    {

        ITracingService TracingService
        {
            get
            {
                return XAct.DependencyResolver.Current.GetInstance<ITracingService>();
            }
        }


        IContextStateService ContextStateService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IContextStateService>(); }
        }

        IPrincipalService PrincipalService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IPrincipalService>(); }
        }

        /// <summary>
        /// Called after an inbound message has been received but before the message is dispatched to the intended operation.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <param name="channel">The incoming channel.</param>
        /// <param name="instanceContext">The current service instance.</param>
        /// <returns>
        /// The object used to correlate state. This object is passed back in the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.BeforeSendReply(System.ServiceModel.Channels.Message@,System.Object)" /> method.
        /// </returns>
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel, InstanceContext instanceContext)
        {

            CorrelationInfo correlationInfo = new CorrelationInfo();

            int securityTokenHeaderIndex = request.Headers.FindHeader(
                Constants.Security.Session.SessionTokenHeaderName,
                Constants.Security.Session.SessionTokenHeaderNamespace);

            if (securityTokenHeaderIndex == -1)
            {
                //No header applied (callling from WcfTestClient.exe, or soapUI, or WCFStorm, hitting
                //back server directly.
                throw new Exception("Incoming SOAP message has no Headers. Service requires a SessionToken Header.");
            }

            SessionTokenEnvelope securityTokenHeader = request.Headers.GetHeader<SessionTokenEnvelope>(securityTokenHeaderIndex);


            if (securityTokenHeader == null)
            {
                //This is an unexpected error, as the front tier should hve already contacted 
                //the back tier (using the service endpoint to update the Session's LastAccessedDate -- which 
                //lets it through as it is not header-checking) and therefore have a session token
                //to attach as a header to subsequent calls:
                throw new Exception("Incoming SOAP message has headers, none of which are of type SessionTokenEnvelope.");
            }

            //Therefore all calls from front to back should have a header.
            //But should not throw an exception if there is no session.
            //
            //There are some calls that it is ok to not have Info regarding user.
            //TODO: Really???
            //It's up to a later security module to check AuthZ.

            //Number or requests from front tier:
            correlationInfo.RequestCounter = securityTokenHeader.RequestCounter;
            //Session Token:
            correlationInfo.SessionToken = securityTokenHeader.SessionToken;
            //Bool indicating whether we are expected to return header:
            correlationInfo.ReturnSession = securityTokenHeader.ReturnSession;


            if (correlationInfo.SessionToken == null)
            {
                //We do not have a sessionToken
                //but we're not going to have a fit about it.
                //Get out early, without going off and getting a
                //Session, AppIdentity and setting back tier Identity.

                //return the CorrelationId (still null):
                return correlationInfo;
            }

            //We do have session token.
            //Therefore should be able to get a Session for it.

            try
            {
                ISessionService sessionService = XAct.DependencyResolver.Current.GetInstance<ISessionService>();

                SessionResponse sessionResponse;


                ContextStateService.IncrementContextOperationCounter(1,"App.CrossTierRequests");

                //A request from front to back can be one request to front, with n (1+) requests
                //to back. 
                //On the first request, we update the LastAccessed date.
                //That said, 
                //Note that in many cases, there will be only one request.
                if (correlationInfo.RequestCounter == 1)
                {
                    //First Request from Front Tier to Back Tier
                    //so we update LastAccessedDateTime.

                    //Note that in many cases, there will be only one request.
                    //AppIdentity appIdentity = securityTokenHeader.RequestCounter


                    correlationInfo.SessionResponse =
                        sessionService.UpdateSessionLastAccessedDateTime(correlationInfo.SessionToken.Value,
                                                                         correlationInfo.ReturnSession);


                    sessionResponse = correlationInfo.SessionResponse.HasData
                                          ? correlationInfo.SessionResponse.Data
                                          : null;



                }
                else
                {
                    //We are in a subsequent call:
                    sessionResponse = null;
                }


                if ((sessionResponse == null)||(sessionResponse.Session == null))
                    {
                        sessionResponse = new SessionResponse
                        {
                            Session = sessionService.GetSession(correlationInfo.SessionToken.Value)
                        };
                        
                    } 



                if (sessionResponse == null)
                {
                    //Session may be Expired, but never null at this point
                    //(an exception will be raised if it can't be found in db).
                    //The method does not update LastAccessDateTime, so it is 
                    //perfectly possible to be stale.

                    throw new Exception("No Session found for given SessionToken");
                }

                //Right...have the Session:
                correlationInfo.SessionResponse = new Response<SessionResponse>();
                correlationInfo.SessionResponse.Data = sessionResponse;



                //Retrieve identity:
                AppIdentity appIdentity = AppIdentity.DeserializeFromBase64(sessionResponse.Session.SerializedIdentity);

                //VERY IMPORTANT: Embed:
                if (!appIdentity.SessionToken.HasValue)
                {
                    appIdentity.SessionToken = correlationInfo.SessionToken.Value;
                }

                //Set the thread's Identity -- while ensure that you're 
                //setting WebContext at the same time:
                this.PrincipalService.Principal = appIdentity.CreatePrincipal();

                //That's ok. we're done.

                //return the CorrelationId (which is the Session):
                return correlationInfo;

            }
            catch (System.Exception e)
            {

                //We tried to find it in db, but it failed (it must be DAYS old...so much so 
                //that Periodic cleansing has pruned out Session table.
                this.TracingService.TraceException(TraceLevel.Error, e,
                                                   "SecurityDispatchMessengerInspector failed when looking for Session with ST:{0}",
                                                   correlationInfo.SessionToken);

                //It was unexpected:
                throw;
            }

        }

        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {

            CorrelationInfo correlationInfo = correlationState as CorrelationInfo;

            if (correlationInfo == null)
            {
                return;
            }


            SessionTokenResponseEnvelope sessionTokenResponseEnvelope = new SessionTokenResponseEnvelope(); //add session token to the headers of wcf message


            sessionTokenResponseEnvelope.SessionResponse = correlationInfo.SessionResponse;

            reply.Headers.Add(sessionTokenResponseEnvelope.ToMessageHeader());


            //Done.

        }
    }
}
