﻿
namespace App.Core.Infrastructure.Integration
{
    using App.Core.Infrastructure.Integration.Steps;
    using App.Core.Infrastructure.Services;

    public static class InfrastructureIntegrator
    {
        public static void Execute()
        {
            new EnsureBasieServicesIntegrationStep().Execute();

            new  SettingSeedingLevelIntegrationStep().Execute();


            //Scan and attach AOP virtual proxies on WCF AppFacade methods
            //so that all calls coming in are secure.
            new ScanAndRegisterSecurityBindingsInitializationStep().Execute();


            //We're looking good. Hopefully, if all things designed right,
            //no ServiceConfiguration has caused a need to hit the Db yet.

            
            //But this will, as it reads the Db for the flag indicating whether
            //scheduling is enabled. 
            //if (!XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>().DisabledSchedulingService)
            //{
            //    new StartSchedulingIntegrationStep().Execute();
            //}
        }
    }
}
