﻿using System.Diagnostics;
using System.Linq;
using System.Text;

namespace App.Core.Infrastructure.Integration.Steps
{
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services;
    using XAct;

    public class StartSchedulingIntegrationStep : IHasInitializationStep
    {
		ISchedulingManagementService SchedulingManagementService
        {
			get { return Shortcuts.ServiceLocate<ISchedulingManagementService>(); }
        }

        IApplicationTennantService ApplicationTennantService
        {
			get { return Shortcuts.ServiceLocate<IApplicationTennantService>(); }
        }

	    IHostSettingsService HostSettingsService
	    {
		    get { return Shortcuts.ServiceLocate<IHostSettingsService>(); }
	    }

        public void Execute()
        {

            SchedulingManagementService.Initialize(ApplicationTennantService.Get());

	        var registeredTasks = SchedulingManagementService.GetSchedules(ApplicationTennantService.Get());

			// Output something to show the tasks that are registered to run
	        var registeredJobsInfo = new StringBuilder();
	        registeredJobsInfo.AppendLine("Registered jobs in the scheduler:");
	        if (registeredTasks == null || !registeredTasks.Any())
	        {
		        registeredJobsInfo.AppendLine(" - (No jobs are registered in the scheduler");
	        }
	        else
	        {
		        foreach (var scheduleConfig in registeredTasks)
		        {
			        registeredJobsInfo.AppendFormat(" - Name: {0}, Schedule: {1}, Enabled: {2}\n", scheduleConfig.Name,
				        scheduleConfig.ChronSchedule,
				        scheduleConfig.Enabled);
		        }
	        }

			Trace.WriteLine(registeredJobsInfo.ToString());
        }
    }
}