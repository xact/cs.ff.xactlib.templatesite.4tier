﻿namespace App.Core.Infrastructure.Integration.Steps
{
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services;
    using XAct;

    public class InitializeSchedulingServiceIntegrationStep : IHasIntegrationStep
    {
        private IApplicationTennantService ApplicationTennantService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>(); }

        }

        private ISchedulingManagementService SchedulingManagementService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<ISchedulingManagementService>(); }
        }

        public void Execute()
        {
            var managementService = XAct.DependencyResolver.Current.GetInstance<ISchedulingManagementService>();

            managementService.Initialize(ApplicationTennantService.Get());

        }
    }
}
