﻿namespace App.Core.Infrastructure.Integration.Steps
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using App.Core.Infrastructure.Security;
    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.InterceptionExtension;
    using XAct;
    using XAct.Diagnostics.Performance;
    using XAct.Interception.CallHandlers;
    using XAct.Security;

    public class ScanAndRegisterSecurityBindingsInitializationStep : IHasIntegrationStep
    {
        static readonly ICallHandler aopSecurityCallAuthenticationHandler = new AuthenticationCallHandler();
		static readonly ICallHandler aopPerformanceCounterHandler = new PerformanceCounterCallHandler();
        private static readonly ICallHandler aopExceptionTracingHandler = new App.Core.Infrastructure.Security.ErrorTracingCallHandler();

        public void Execute()
        {
            
            //Next library push, consider erasing all of the following and using instead:

            //XAct.Services.IoC.VirtualClassInterceptor.Execute<AuthorizationAttribute, PerformanceCounterAttribute>(
            //"MyPolicy", false, 
            //new AuthenticationCallHandler(),
            //new PerformanceCounterCallHandler(),
            //new Security.ErrorTracingCallHandler()
            //);


            Microsoft.Practices.Unity.IUnityContainer unityContainer =
                XAct.DependencyResolver.DependencyInjectionContainer
                as Microsoft.Practices.Unity.IUnityContainer;


            //Configure Unity to be Interception ready:
            unityContainer.AddNewExtension<Interception>();

            //Find Service and Service Contracts decorated with our Attribute
            //(not on the methods in this case, but the actual class):
            KeyValuePair<Type,AuthorizationAttribute>[] services =
                AppDomain.CurrentDomain.GetTypesDecoratedWithAttribute<AuthorizationAttribute>(null, true, false);

            //The above list will contain both instances and interface types.
            //We only want the actual Instance Types used to back the Service Hosts
            //as ServiceHostFactory can only deal with Instance Types
            Type[] serviceFacedeInstanceTypes = 
                services
                .Where(kvp => (!kvp.Key.IsInterface && !kvp.Key.IsAbstract))
                .Select(s => s.Key).ToArray();



            //And register each as having an Interceptor:
            foreach (Type serviceInstanceType in serviceFacedeInstanceTypes)
            {

                //Have a class that is inheriting from base interface.
                //See: http://stackoverflow.com/questions/17558674/register-same-unity-interception-call-handler-for-all-registered-types

                //Watch out: IsVirtual will pick up both class methods that are marked virtual and ones
                //that were defined lower down in an interface. Therefore check to see IsFinal to ensure
                //that it is not marked with IsFinal. The reason for keeping the isVirtual is 
                //to check for methods came *from* interfaces...

                MethodInfo[] methodsNotMarkedVirtual = serviceInstanceType.GetMethods()
                    .Where(mi => (mi.IsVirtual) && (mi.IsFinal))
                    .ToArray();

                if (methodsNotMarkedVirtual.Length>0)
                {
                    throw new Exception(
                        "Development error: all Methods of '{0}' must be marked virtual in order to be able to AOP/Proxy wrap"
                        .FormatStringInvariantCulture(serviceInstanceType.FullName));
                }

                //Notice that we are registering the Service Facade *INSTANCE* type, and not the backing COntract.
                //This is because the ServiceHostFactory is going to make ServiceHosts that accept a Type -- but 
                //WCF Framework only accepts an *instance* type, and rejects Contracts.

                unityContainer.RegisterType(serviceInstanceType,
                               new Interceptor<VirtualMethodInterceptor>(),
                               new InterceptionBehavior<PolicyInjectionBehavior>());

                //Our interceptor is going to be our instance of
                //AuthenticationCallHandler (defined above), 
                //which in turn is going to invoke AuthorisationByMethodService
                //to compare method name against an instore knowledge of roles to method name.

				unityContainer.Configure<Interception>()
							  .AddPolicy("MyPolicy")
							  .AddMatchingRule(
								new XAct.Services.Interception.ExtendedCustomAttributeMatchingRule<AuthorizationAttribute>())
                                .AddCallHandler(aopExceptionTracingHandler)
                              .AddCallHandler(aopPerformanceCounterHandler)
							  .AddCallHandler(aopSecurityCallAuthenticationHandler)
							  ;

				//Done.
            }


            Type check = XAct.DependencyResolver.Current.GetInstance(serviceFacedeInstanceTypes[2]).GetType();

        }
    }
}