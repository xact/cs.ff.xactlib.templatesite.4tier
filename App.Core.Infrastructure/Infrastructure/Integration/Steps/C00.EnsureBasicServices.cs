﻿namespace App.Core.Infrastructure.Integration.Steps
{
    using System;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Library.Settings;


    public class SettingSeedingLevelIntegrationStep : IHasInitializationStep
    {
        public void Execute()
        {
            //Set this before any communication with the db.
            XAct.Library.Settings.Db.CommitRegularlyDuringSeeding = SeedingCommitLevel.Always;
        }
    }

    public class EnsureBasieServicesIntegrationStep :IHasInitializationStep 
    {
        public void Execute()
        {
            //Not many services we can check without triggering a need for DbContext, therefore seeding,etc.
            //which is handled in next step.

            EnsureTracingSevice();

            EnsureEnvironmentSevice();
            EnsureHostSettingSevice();
        }

        private static void EnsureTracingSevice()
        {
            ITracingService tracingService = 
                XAct.DependencyResolver.Current.GetInstance<ITracingService>();
        }
        private static void EnsureEnvironmentSevice()
        {
            IAppEnvironmentService environmentService = XAct.DependencyResolver.Current.GetInstance<IAppEnvironmentService>();
            object httpContext = environmentService.HttpContext;
            string report = XAct.DependencyResolver.BindingResults.CreateBindingReport().JoinSafely(Environment.NewLine);
        }
        private static void EnsureHostSettingSevice()
        {
            IHostSettingsService hostSettingsService = XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();
        }
    }
}
