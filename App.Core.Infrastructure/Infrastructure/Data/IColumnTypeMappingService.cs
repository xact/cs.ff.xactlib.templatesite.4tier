namespace App.Core.Infrastructure.Data
{
    public interface IColumnTypeMappingService
    {
        string GetColumnType(string columnType);
    }
}