﻿namespace App.Core.Infrastructure.Data
{
    using XAct.Services;

    public class NullColumnTypeMappingService : IColumnTypeMappingService
    {
        public string GetColumnType(string columnType)
        {
            return columnType;
        }
    }
}
