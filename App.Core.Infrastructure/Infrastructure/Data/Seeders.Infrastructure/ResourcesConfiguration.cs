﻿namespace App.Core.Infrastructure.Data.Seeders.Infrastructure
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Globalization;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Library.Settings;
    using XAct.Resources;

    public class ResourcesConfiguration
    {

        private static Guid GenGuid()
        {
            return XAct.DependencyResolver.Current.GetInstance<Services.IDistributedIdService>().NewGuid();
        }

        public ResourcesConfiguration(DbContext dbContext)
        {
            Resources = dbContext.Set<Resource>();
            DbContext = dbContext;
        }

        public IDbSet<Resource> Resources { get; private set; }
        public DbContext DbContext { get; private set; }
        private string _filter;

        public ResourcesConfiguration FilterBy(string filter)
        {
            _filter = filter;
            return this;
        }

        private ResourcesConfiguration AddOrUpdate(string key, string value)
        {
            Resource resource = new Resource(GenGuid(), _filter, key, value, CultureInfo.InvariantCulture);
            Resources.AddOrUpdate(m => new { m.Filter, m.Key, m.CultureCode }, resource);
            return this;
        }

        public ResourcesConfiguration AddOrUpdate(string[,] values)
        {
            int lenght = values.GetLength(0);            
            if (values.GetLength(1) != 2) throw new Exception("Each key has to contain exactly one value");
            
            for (int i = 0; i < lenght; i++)
            {
                string key = values[i, 0];
                string value = values[i, 1];
                AddOrUpdate(key, value);
            }
            return this;
        }

        public void Save()
        {

            DbContext.SaveChanges();

        }
    }
}
