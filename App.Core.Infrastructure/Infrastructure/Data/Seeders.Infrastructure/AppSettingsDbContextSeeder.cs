﻿using System.Collections.Generic;
using XAct.Data.EF.CodeFirst.Seeders.Implementations;

namespace App.Core.Infrastructure.Data.Seeders.Infrastructure
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
    using System.Linq;
    using App.Core.Domain.Models;
    using App.Core.Infrastructure.Services;
using XAct;
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;
    using XAct.Library.Settings;
    using XAct.Settings;

    public class AppSettingsDbContextSeeder : DbContextSeederBase<AppSettings>, XAct.Settings.Initialization.DbContextSeeders.IEFSettingsDbContextSeeder
	{

        IAppEnvironmentService AppEnvironmentService
        {
            get
            {
                return XAct.DependencyResolver.Current.GetInstance<IAppEnvironmentService>(); 
            }
        }

        private IDateTimeService DateTimeService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IDateTimeService>(); }
        }

        Guid  AppGuid
        {
            get
            {
                return Guid.Empty;
                //return XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>().Get();
            }
        }

        private DbContext _dbContext;

        public override void SeedInternal(DbContext dbContext)
        {
			try
			{
			    _dbContext = dbContext;

			    if (XAct.Library.Settings.Db.SeedingType >= SeedingType.ResetMutableAppSettings)
			    {
                    DbSet<SerializedApplicationSetting> settings = dbContext.Set<XAct.Settings.SerializedApplicationSetting>();
                    //DbSet<ApplicationsettingsAudit> applicationsettingsAudit = dbContext.Set<ApplicationsettingsAudit>();
                    DbSet<ApplicationSettingAudit> settingsAudit = dbContext.Set<XAct.Settings.ApplicationSettingAudit>();
                    
                    settingsAudit.RemoveRange(settingsAudit);
			        //applicationsettingsAudit.RemoveRange(applicationsettingsAudit);
                    settings.RemoveRange(settings);

			        dbContext.SaveChanges();
			    }

			    CreateSettings(dbContext);
                
                CreateParameterSettings(dbContext);
			    
                _dbContext = null;
			}

				// ReSharper disable RedundantCatchClause
#pragma warning disable 168
			catch (System.Exception e)
#pragma warning restore 168
			{
				throw;
			}
			// ReSharper restore RedundantCatchClause
		}


        public void AddOrUpdateSetting<TValue>(DbSet<SerializedApplicationSetting> settings,
                                               string key,
                                               TValue value,
                                               bool enabled = true,
                                               string description = null,
                                               string tag = null,
                                               string isWritableAuthorisationInformation = null,
                                               string environmentIdentifier = null,
                                               string zoneOrTierIdentifier = null,
                                               string hostIdentifier = null,
                                               string userIdentifier = null
            )
        {

            
            //Whether new add, or new update:
            SerializedApplicationSetting setting =
                new SerializedApplicationSetting()
                    {
                        Id = XAct.DependencyResolver.Current.GetInstance<XAct.IDistributedIdService>().NewGuid(),
                        Key = key,
                        Enabled = true,
                        Description = description,
                        Tag = tag,
                        IsWritableAuthorisationInformation = isWritableAuthorisationInformation,
                        EnvironmentIdentifier = environmentIdentifier,
                        ZoneOrTierIdentifier = zoneOrTierIdentifier,
                        TennantIdentifier = AppGuid,
                        HostIdentifier = hostIdentifier,
                        CreatedOnUtc = DateTimeService.NowUTC,
                        LastModifiedOnUtc =  DateTimeService.NowUTC,
                    };

            setting.Set(value);


            settings.AddOrUpdate(
                m =>
                new {m.EnvironmentIdentifier, m.ZoneOrTierIdentifier, m.HostIdentifier, m.TennantIdentifier, m.Key}
                , setting);


        }



        public void CreateSettings<TContext>(TContext dbContext) where TContext : DbContext
		{
            //Note that the setting is compared against ResetImmutableReferenceData (one higher
            //than SkipSeeding), but later, in the AddOrUpdateSetting method it checks
            //against ResetMutableAppSettings)
            //The reason is: at the very least ResetImmutableReferenceData, we need to ensure the variable actually exists
            //but only if we are willing to change the value, due we *overwrite*)
            if (XAct.Library.Settings.Db.SeedingType >= SeedingType.ResetImmutableReferenceData)
            {
                DbSet<SerializedApplicationSetting> settings = dbContext.Set<XAct.Settings.SerializedApplicationSetting>();

                //SerializedApplicationSetting serializedSetting;

                AddOrUpdateSetting(settings, "Example", "Works");
                AddOrUpdateSetting(settings, "PingDb", "Db Online");



                //Formatting:
                AddOrUpdateSetting(settings, "Formatting/DateFormatDefault", "d", true, "", null, null);
                AddOrUpdateSetting(settings, "Formatting/DateFormatShort", "d", true, "", null, null);
                AddOrUpdateSetting(settings, "Formatting/DateFormatLong", "D", true, "", null, null);
                AddOrUpdateSetting(settings, "Formatting/TimeDefaultFormat", "t");
                AddOrUpdateSetting(settings, "Formatting/TimeShortFormat", "t");
                AddOrUpdateSetting(settings, "Formatting/TimeLongFormat", "T");
                AddOrUpdateSetting(settings, "Formatting/DateTimeDefaultFormat", "g");
                AddOrUpdateSetting(settings, "Formatting/DateTimeShortFormat", "g");
                AddOrUpdateSetting(settings, "Formatting/DateTimeLongFormat", "F");




                //Data retrieval:
                AddOrUpdateSetting(settings, "Data/DefaultPageSize", 20, true, null, null, null);


                //Caching:
                AddOrUpdateSetting(settings, "Caching/ShortCacheTimeInSeconds", 60);
                AddOrUpdateSetting(settings, "Caching/ReferenceCacheTimeSpanInMinutes", 20);



				//Batch
				AddOrUpdateSetting(settings, "BatchProcessing/Enable", true);
				AddOrUpdateSetting(settings, "BatchProcessing/SearchOperationDelayMilliseconds", 20);
				AddOrUpdateSetting(settings, "BatchProcessing/Update|InsertDelayMilliseconds", 20);
				AddOrUpdateSetting(settings, "BatchProcessing/ActiveAtDelayMilliseconds", 20);
				AddOrUpdateSetting(settings, "BatchProcessing/MergeOperationDelayMilliseconds", 20);

                //Contact:
                AddOrUpdateSetting(settings, "Support/ContactInfo/Name", "Sky Sigal");
                AddOrUpdateSetting(settings, "Support/ContactInfo/Email", "skys@datacom.co.nz");
                AddOrUpdateSetting(settings, "Support/ContactInfo/Phone", "+64 21 159 6440");
                AddOrUpdateSetting(settings, "Support/ContactInfo/PhoneAfterHours", "");
                AddOrUpdateSetting(settings, "Support/ContactInfo/Address", "");
                AddOrUpdateSetting(settings, "Support/ContactInfo/Note", "");


                //Misc:
                //AddOrUpdateSetting(settings, "Security/EnableEsaa", "d",true,"", null, null);



                //Session TIme out:
                AddOrUpdateSetting(settings, "Security/Session/TimeOutInMinutes", 20, true, "The max amount a user can be idle between operations.", null, null, null, "BackTier");
                AddOrUpdateSetting(settings, "Security/Session/MaxLengthInMinutes", 480, true, "A session should not last longer than 8hrs * 60 minutes. Mininum allowed is 60", null, null, null, "BackTier");



                //Services/SMTP (BackTier only)
                AddOrUpdateSetting(settings, "Services/Messaging/SMTP/Enabled", true, true, "Whether to use any of these Messaging/SMTP variables or false to fallback to any settings that may be in the host config file..", null, null, null, "BackTier");
                AddOrUpdateSetting(settings, "Services/Messaging/SMTP/Host", "TODO", true, "The Email MTA/Server Name (eg: 'mta.corp.com'). Ignored if set to 0. Ignored if Enabled is False.", null, null, null, "BackTier");
                AddOrUpdateSetting(settings, "Services/Messaging/SMTP/Port", 25, true, "The Email MTA/Server Port. Ignored if set to 0. Ignored if Enabled is False.", null, null, null, "BackTier");
                AddOrUpdateSetting(settings, "Services/Messaging/SMTP/SSL", false, true, "A Flag indicating whether SSL is required to communicate with the Email MTA/Server. Ignored if Enabled is False.", null, null, null, "BackTier");

                try
                {

                    dbContext.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    throw;
                }

            }
		}


        /// <summary>
        /// Create Parameter Settings and matching AppSettings items.
        /// </summary>
        /// <typeparam name="TContext"></typeparam>
        /// <param name="dbContext"></param>
        private void CreateParameterSettings<TContext>(TContext dbContext) where TContext : DbContext
        {

            //Note that the setting is compared against ResetImmutableReferenceData (one higher
            //than SkipSeeding), but later, in the AddOrUpdateSetting method it checks
            //against ResetMutableAppSettings)
            //The reason is: at the very least ResetImmutableReferenceData, we need to ensure the variable actually exists
            //but only if we are willing to change the value, due we *overwrite*)
            if (XAct.Library.Settings.Db.SeedingType >= SeedingType.ResetImmutableReferenceData) 
            {

                DbSet<SerializedApplicationSetting> settings = dbContext.Set<XAct.Settings.SerializedApplicationSetting>();

                dbContext.SaveChanges();

                //TODO: change to constants in shared so that we can access it safely?

                //Parameters (FrontTier and BackTier)

				//Parameters last refereshed on: 13/06/14 on version 1.4 12/06/14

                //Parameters


               AddOrUpdateSetting(settings, "Parameters/Parameter/esaa_url_password", "https://identity.ORG/selfservice/", true, "DO NOT NULL THIS PARAMETER.ESAA2 SAML change password web page.", "URL", "N");
                
				AddOrUpdateSetting(settings, "Parameters/Parameter/Merge_Duplicate_Students_Job", "On", true, "DO NOT NULL THIS PARAMETER.Turn Merge Duplicates Students job off or on.", "Boolean", "Y");
				AddOrUpdateSetting(settings, "Parameters/Parameter/Merge_Match_Threshold", "92", true, "DO NOT NULL THIS PARAMETER.For both Online and Batch merge requests it uses this threshold to determine if the records in the merge group are automatically matched.When the merge match weighting is above this threshold, a record pair will be automatically merged. For merge matches below this threshold the merge pair will be flagged for manual intervention.", "Numeric", "Y");
				AddOrUpdateSetting(settings, "Parameters/Parameter/Search_Results_Limit", "100", true, "DO NOT NULL THIS PARAMETER.Maximum number of records that can be returned for a search request.", "Numeric", "N");
				AddOrUpdateSetting(settings, "Parameters/Parameter/session_timeout", "20", true, "DO NOT NULL THIS PARAMETER.Used in App web pages, as session timeout (in minutes) The timeout will clear security and security_Cache tables.", "Numeric", "Y");
				AddOrUpdateSetting(settings, "Parameters/Parameter/SPR_exclusion", "-1", true, "List of organisations for whom a student provider relationship record will not be recorded when they insert or modify a student record. ", "String", "Y");




                //Direct Database Lookup for Search
                AddOrUpdateSetting(settings, "Parameters/Parameter/DirectDBLookup","On", true, "Direct Database Search","Boolean", "Y", null, "BackTier");


                #region Variations DEV
                //VARIATIONS: DEV
                // Notice the "UAT" at the end of the line, that lines up with web.config 'HostIdentifier' value.
                AddOrUpdateSetting(settings, "Parameters/Parameter/DirectDBLookup", "On", true, "Direct Database Search (DEV)", "Boolean", "Y", "DEV", "BackTier");
                #endregion


                #region Variations UAT
                // VARIATIONS: UAT
                // Notice the "UAT" at the end of the line, that lines up with web.config 'HostIdentifier' value.


                AddOrUpdateSetting(settings, "Services/Messaging/SMTP/UserName", "NOTIMPLEMENTED", true, "Server access name if required for testing. Ignored if Enabled is False.", null, null, "UAT", "BackTier");
                AddOrUpdateSetting(settings, "Services/Messaging/SMTP/Password", "NOTIMPLEMENTED", true, "Password access name if required for testing. Ignored if Enabled is False.", null, null, "UAT", "BackTier");

                #endregion



                #region Variations COMP
                // VARIATIONS: UAT
                // Notice the "UAT" at the end of the line, that lines up with web.config 'HostIdentifier' value.

                AddOrUpdateSetting(settings, "Services/Messaging/SMTP/UserName", "NOTIMPLEMENTED", true, "Server access name if required for testing. Ignored if Enabled is False.", null, null, "COMP", "BackTier");
                AddOrUpdateSetting(settings, "Services/Messaging/SMTP/Password", "NOTIMPLEMENTED", true, "Password access name if required for testing. Ignored if Enabled is False.", null, null, "COMP", "BackTier");

                #endregion



                //Isolate and catch issues with settings till we get smarter...
                dbContext.SaveChanges();
            }
        }


        public override void CreateEntities()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SerializedApplicationSetting> Entities { get; private set; }
	}
}
