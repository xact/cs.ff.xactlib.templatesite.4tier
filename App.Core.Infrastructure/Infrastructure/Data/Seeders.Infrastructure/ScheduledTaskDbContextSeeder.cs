﻿using XAct.Data.EF.CodeFirst.Seeders.Implementations;

namespace App.Core.Infrastructure.Data.Seeders.Infrastructure
{
    using System;
    using System.Data.Entity;
    using System.Reflection;
    using App.Core.Infrastructure.Services;
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;
    using XAct.Scheduling;

    public class ScheduledTaskDbContextSeeder : DbContextSeederBase<ScheduledTask>
    {
        private readonly IAppEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;


        public ScheduledTaskDbContextSeeder(
            IAppEnvironmentService environmentService,
            IApplicationTennantService applicationTennantService,
            IPrincipalService principalService)
        {
            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
        }

        public override void SeedInternal(DbContext dbContext)
        {
            //scheduledTasks.AddOrUpdate(new ScheduledTask{})
        }


        public override void CreateEntities()
        {
            //this.InternalEntities.Add();

            //scheduledTasks.AddOrUpdate<ScheduledTask>(
            //    !@@@@x=>x.Name,
            //    ScheduledTaskFactory(
            //    "Batch",
            //    "Master",
            //    "App.Core.Application",
            //    "App.Core.Presentation.ScheduledTasks.MasterScheduledTask", 
            //    "Execute",
            //    "0 * * * * ?"
            //    ));
        }



        private void ScheduledTaskFactory(string groupName, string name, MethodInfo methodInfo,
                                          string schedule = "0 * * * * ?")
        {
            ScheduledTaskFactory(groupName, name, methodInfo.DeclaringType.Assembly.FullName, methodInfo.DeclaringType.FullName, methodInfo.Name, schedule);
        }

        private ScheduledTask ScheduledTaskFactory(string name, string groupName,string assemblyFullName, string typeFullName, string methodName,  string schedule = "0 * * * * ?")
        {
            XAct.Scheduling.ScheduledTask scheduledTask = new XAct.Scheduling.ScheduledTask();
            scheduledTask.Id = Guid.NewGuid();
            scheduledTask.ApplicationTennantId = _applicationTennantService.Get();
            scheduledTask.Enabled = true;
            //scheduledTask.Id = ...;
            scheduledTask.GroupName = "Batch";
            scheduledTask.Name = "Master";
            scheduledTask.Description = "Trigger to allocate jobs to other queues.";
            scheduledTask.StartDelay = new TimeSpan();
            scheduledTask.ChronSchedule = schedule; //EVERY MINUTE. See: http://www.quartz-scheduler.net/documentation/quartz-2.x/tutorial/crontriggers.html
            scheduledTask.IsExecutable =false;
            scheduledTask.ExecutablePath = null;
            scheduledTask.ExecutableArguments = null;
            scheduledTask.AssemblyName = assemblyFullName;
            scheduledTask.TypeFullName = typeFullName;
            scheduledTask.MethodName = methodName;


            return scheduledTask;
        }

    }
}
