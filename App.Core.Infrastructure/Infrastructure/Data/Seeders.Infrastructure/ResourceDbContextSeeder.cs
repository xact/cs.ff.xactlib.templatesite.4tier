﻿using App.Core.Infrastructure.Data.Seeders.Imported;
using XAct.Data.EF.CodeFirst.Seeders.Implementations;

namespace App.Core.Infrastructure.Data.Seeders.Infrastructure
{
    using System.Data.Entity;
    using System.Diagnostics;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Data.EF.CodeFirst;
    using XAct.Library.Settings;
    using XAct.Resources;
    using XAct.Resources.Initialization.DbContextSeeders;
    using XAct.Services;

    public partial class ResourceDbContextSeeder : DbContextSeederBase<Resource>, IResourceDbContextSeeder
    {


        private ITracingService TracingService
        {
            get { return _tracingService ?? (_tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>()); }
        }

        private ITracingService _tracingService;

        public override void SeedInternal(DbContext dbContext)
        {


        }


        public override void CreateEntities()
        {

            TracingService.Trace(TraceLevel.Verbose, "Seeding resources");

            this.InternalEntities.Add(new Seeder_MessageCodeResources().Create());
            this.InternalEntities.Add(new Seeder_Resources().Create());

            //new Seeder_HelpResources().Seed(dbContext);
        }
    }
}