using System;
using System.Collections.Generic;
using System.Data.Entity;
using XAct.Library.Settings;
using XAct;
using XAct.Messages;
using XAct.Resources;
using System.Data.Entity.Migrations;
using System.Globalization;
using App.Core.Infrastructure.Services;
using IDistributedIdService = App.Core.Infrastructure.Services.IDistributedIdService;

namespace App.Core.Infrastructure.Data.Seeders.Imported
{
	public class Seeder_MessageCodeResources_Core 
	{
        public IEnumerable<Resource> Create()
        {
	        var results = new List<Resource>
	        {
									new Resource(GenGuid(), "MessageCodes", "SUCCESS", "Operation Successful", ""),
					new Resource(GenGuid(), "MessageCodes", "SUCCESS", "Oui!", "fr-FR"),
									new Resource(GenGuid(), "MessageCodes", "UnknownError", "Unknown Error", ""),
					new Resource(GenGuid(), "MessageCodes", "UnknownError", "Merde!", "fr-FR"),
									new Resource(GenGuid(), "MessageCodes", "AuthorisationDenied", "Access denied.", ""),
									new Resource(GenGuid(), "MessageCodes", "InvalidUserNameOrPasswod", "Invalid username or password.", ""),
									new Resource(GenGuid(), "MessageCodes", "InvalidOrganisationForUser", "Invalid organisation for user.", ""),
									new Resource(GenGuid(), "MessageCodes", "InterfaceNotEnabledForUser", "", ""),
									new Resource(GenGuid(), "MessageCodes", "SessionTimout", "Your session has timed out. Please log in again.", ""),
									new Resource(GenGuid(), "MessageCodes", "SessionTooLong", "Your session has timed out. Please log in again.", ""),
									new Resource(GenGuid(), "MessageCodes", "SessionLoggedOut", "You've successfully logged out.", ""),
			};
			return results;
		}

		private Guid GenGuid()
        {
            return XAct.DependencyResolver.Current.GetInstance<IDistributedIdService>().NewGuid();
        }

	}//~class
}//~ns
