using System;
using System.Collections.Generic;
using System.Data.Entity;
using XAct.Library.Settings;
using XAct.Resources;
using System.Data.Entity.Migrations;
using App.Core.Infrastructure.Services;

namespace App.Core.Infrastructure.Data.Seeders.Imported
{
	public class Seeder_Resources_Dashboard 
	{
        public List<Resource> Create()
        {
	        var results = new List<Resource>
	        {
				    new Resource(GenGuid(), "Dashboard", "SubTitle", "An example subtitle", ""),
				    new Resource(GenGuid(), "Dashboard", "SubTitle", "Un example de sous titre!", "fr-FR"),
				    new Resource(GenGuid(), "Dashboard", "SomeKey", "Some value", ""),
			};
			return results;
		}

		private Guid GenGuid()
        {
            var result = XAct.DependencyResolver.Current.GetInstance<IDistributedIdService>().NewGuid();

            return result;
        }

	}//~class
}//~ns
