using System;
using System.Collections.Generic;
using System.Data.Entity;
using XAct.Library.Settings;
using XAct.Resources;
using System.Data.Entity.Migrations;
using App.Core.Infrastructure.Services;

namespace App.Core.Infrastructure.Data.Seeders.Imported
{
	public class Seeder_Resources_Home 
	{
        public List<Resource> Create()
        {
	        var results = new List<Resource>
	        {
				    new Resource(GenGuid(), "Home", "Title", "Home Page", ""),
				    new Resource(GenGuid(), "Home", "SubTitle", "An example subtitle", ""),
			};
			return results;
		}

		private Guid GenGuid()
        {
            var result = XAct.DependencyResolver.Current.GetInstance<IDistributedIdService>().NewGuid();

            return result;
        }

	}//~class
}//~ns
