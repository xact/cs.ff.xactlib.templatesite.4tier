namespace App.Core.Infrastructure.Data.Repositories
{
	public interface IIqOfficeRepositoryService
	{
		/// <summary>
		/// Gets the count of students/alt names that require a standardisation update.
		/// (Ideally this number would be 0, indicating no standardisation has been missed, but we must check it)
		/// </summary>
		/// <returns></returns>
		int GetUnstandardisedCount();

		void LogStandardisationCheck(int recordsAwaitingStandardisation, string consoleOutput);
	}
}