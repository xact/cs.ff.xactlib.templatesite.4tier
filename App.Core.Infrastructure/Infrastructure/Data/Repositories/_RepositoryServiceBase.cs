﻿namespace App.Core.Infrastructure.Data.Repositories
{
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services;

    /// <summary>
	/// Abstract base class for application Repositories.
	/// Ensures all services get a copy of the basic service that they will need.
	/// </summary>
	public abstract class RepositoryServiceBase
	{
		protected readonly string _typeName;


		protected readonly ITracingService _tracingService;
		protected readonly IAppEnvironmentService _environmentService;
		protected readonly IRepositoryService _repositoryService;


		/// <summary>
		/// Initializes a new instance of the <see cref="RepositoryServiceBase" /> class.
		/// </summary>
		/// <param name="tracingService">The tracing service.</param>
		/// <param name="environmentService">The environment service.</param>
		/// <param name="repositoryService">The repository service.</param>
		public RepositoryServiceBase(ITracingService tracingService, IAppEnvironmentService environmentService, IRepositoryService repositoryService)
		{
			_typeName = this.GetType().Name;

			_tracingService = tracingService;
			_environmentService = environmentService;
			_repositoryService = repositoryService;
		}
	}
}
