﻿namespace App.Core.Infrastructure.Data.Repositories
{
    using System;
    using App.Core.Domain.Messages;
    using App.Core.Domain.Models;

    public interface ISessionRepositoryService
    {
        /// <summary>
        /// Generates guid (App Session Token) for the session and session log and persists them
        /// in the database in on transaction.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <param name="signInInfo">The session log.</param>
        /// <returns>App Session Token</returns>
        Guid PersistSession(Session session, SignInInfo signInInfo);

        /// <summary>
        /// Gets the <see cref="Session" /> object.
        /// <para>
        /// May return null if sessionToken is incorrect (and <paramref name="raiseExceptionIfNotFound"/> is set to <c>false</c>).
        /// </para>
        /// <para>
        /// Note that if returned, the <see cref="Session" /> object may be stale
        /// (in that it's <c>LastAccessedDateTime</c> is too far in the past).
        /// </para>
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns>
        /// The <see cref="Session" />
        /// </returns>
        Session GetSession(Guid sessionToken, bool raiseExceptionIfNotFound=true);

        /// <summary>
        /// Updates the <see cref="Session" />'s LastAccessedTime -- if it is still valid to do so.
        /// <para>
        /// Returns false if The session was not found.
        /// </para>
        /// <para>
        /// Returns false if the session timed out (ie, that last login was Now, minus SessionTimeout duration)
        /// </para>
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <param name="expired">if set to <c>true</c> [expired].</param>
        /// <param name="sessionTooLong">if set to <c>true</c> [session too long].</param>
        /// <returns></returns>
        bool UpdateSessionLastAccessedDateTime(Guid sessionToken, out bool expired, out bool sessionTooLong);

        //TODO : these will go somewhere else
        #region
        /// <summary>
        /// Checks if ESAA Access Token has been logged.
        /// </summary>
        /// <param name="esaaAccessToken">The esaa access token.</param>
        /// <returns>[true] if passed ESSA Access Token has been logged, otherwise [false]</returns>
        bool CheckEssaAccessTokenHasBeenLogged(string esaaAccessToken);

        /// <summary>
        /// Logs the ESAA Access Token in the database.
        /// </summary>
        /// <param name="esaaAccessToken">The esaa access token.</param>
        void LogEssaAccessToken(string esaaAccessToken);
        #endregion

	    bool DoesSessionExist(Guid sessionToken);
        /// <summary>
        /// Deletes the <see cref="Session"/>
        /// from the database.
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        void DeleteSession(Guid sessionToken, bool commitNow=false);

    }
}
