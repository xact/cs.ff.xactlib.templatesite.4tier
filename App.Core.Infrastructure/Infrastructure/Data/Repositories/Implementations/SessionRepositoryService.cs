﻿// ReSharper disable CheckNamespace

using App.Core.Infrastructure.Models;

namespace App.Core.Infrastructure.Data.Repositories.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Linq.Expressions;
    using App.Core.Domain.Messages;
    using App.Core.Domain.Models;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Data.Repositories;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Services;

    public class SessionRepositoryService : RepositoryServiceBase, ISessionRepositoryService 
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly Services.IDistributedIdService _distributedIdService;
        private readonly IApplicationSettingsService _settingsService;
        private readonly IUnitOfWorkService _unitOfWorkService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionRepositoryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="distributedIdService">The distributed id service.</param>
        /// <param name="settingsService">The settings service.</param>
        /// <param name="unitOfWorkService">The unit of work service.</param>
        public SessionRepositoryService(
            ITracingService tracingService,
            IDateTimeService dateTimeService,
            IAppEnvironmentService environmentService,
            IRepositoryService repositoryService,
            Services.IDistributedIdService distributedIdService,
            IApplicationSettingsService settingsService,
            IUnitOfWorkService unitOfWorkService
            )
            : base(tracingService, environmentService, repositoryService)
        {
            _dateTimeService = dateTimeService;
            _distributedIdService = distributedIdService;
            _settingsService = settingsService;
            _unitOfWorkService = unitOfWorkService;
        }

        /// <summary>
        /// Gets the <see cref="Session" /> object.
        /// <para>
        /// May return null if sessionToken is incorrect (and <paramref name="raiseExceptionIfNotFound"/> is set to <c>false</c>).
        /// </para>
        /// <para>
        /// Note that if returned the <see cref="Session" /> object may be stale
        /// (in that it's <c>LastAccessedDateTime</c> is too far in the past).
        /// </para>
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns>
        /// The <see cref="Session" />
        /// </returns>
        public Session GetSession(Guid sessionToken, bool raiseExceptionIfNotFound = true)
        {
            Session result =
                _repositoryService.GetSingle<Session>(session => session.Id == sessionToken);

            if ((result == null) && (raiseExceptionIfNotFound))
            {
                   throw new ArgumentException("Session Record not found for Session Token: {0}".FormatStringInvariantCulture(sessionToken));   
            }
            return result;
        }

        /// <summary>
        /// Generates guid (App Session Token) for the session and session log and persists them
        /// in the database in on transaction.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <param name="signInInfo">The session log.</param>
        /// <returns>
        /// App Session Token
        /// </returns>
        public Guid PersistSession(Session session, SignInInfo signInInfo)
        {
            Guid sessionId = _distributedIdService.NewGuid();

            
            session.Id = sessionId;

            //Convert Message to Entity and complete:
            SessionLog sessionLog = signInInfo.MapTo();
            //For this first entry, set both dates to the same time:

            DateTime nowUtc = _dateTimeService.NowUTC;
            DateTime localTime = nowUtc.ToLocalTime();

            //FIX:SessionLog LocalTime:
            sessionLog.LastAccessedDateTime = sessionLog.LoggedInDateTime = localTime;

            //We are not keeping Referential integrity between SessionLog and Session (so that 
            //we can clear out old Session entries on a regular basis, without getting a cascade
            //delete or other scenario that would clear out our logs) -- but it is useful to keep
            //some means of seeing the correlation between the two recrods.
            //So we use the same Id for both types of records:
            sessionLog.Id = sessionId;
            sessionLog.OrganisationCode = signInInfo.OrganisationId;

            //Ensure they are marked as new records, and persist:
            _repositoryService.AddOnCommit(session);
            _repositoryService.AddOnCommit(sessionLog);
            
            _unitOfWorkService.Commit();

            //Return Id:
            return sessionId;
        }

        /// <summary>
        /// Updates the <see cref="Session" />'s LastAccessedTime -- if it is still valid to do so.
        /// <para>
        /// Returns false if The session was not found.
        /// </para>
        /// <para>
        /// Returns false if the session timed out (ie, that last login was Now, minus SessionTimeout duration)
        /// </para>
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <param name="expired">if set to <c>true</c> [expired].</param>
        /// <param name="sessionTooLong">if set to <c>true</c> [session too long].</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public bool UpdateSessionLastAccessedDateTime(Guid sessionToken, out bool expired, out bool sessionTooLong)
        {
            //TODO : this needs to be optimized with conditional update query

            //the expiry value has to precalculated so that EF can translate it to SQL query
            //for the session to be NOT expired the following has to apply
            
            //lastAccessTime + TimeOUt > NOW
            //wich is the same as:
            //lastAccessTime > NOW - Timeout
            DateTime nowUtc = _dateTimeService.NowUTC;
	        var now = nowUtc.ToLocalTime();

            DateTime lastallowedAccessedDateTimeUtc = nowUtc.Subtract(_settingsService.AppSettings.SessionTimeOut);

            DateTime lastallowedAccessedDateTimeLocalTime = lastallowedAccessedDateTimeUtc.ToLocalTime();
            //DateTimeKind kind = lastallowedAccessedDateTime.Kind;

            DateTime lastallowedLoginDateTimeUtc = nowUtc.Subtract(_settingsService.AppSettings.SessionMaxLength);
            DateTime lastallowedLoginDateTimeLocalTime = lastallowedLoginDateTimeUtc.ToLocalTime();
            

            SessionLog[] sessionLogs = 
                _repositoryService
                .GetByFilter<SessionLog>(
                    s => (s.Id == sessionToken)).ToArray();


			if (!sessionLogs.Any())
			{
				// The provided GUID is not in our DB
				
				expired = true; // Indicate that it's expired, or was never established
				sessionTooLong = false;

				return false;
			}

            if (sessionLogs.Count() > 1)
            {
                throw new Exception(string.Format("Two sessions with the same token {0}", sessionToken));
                //this shouldn't happen
            }

#pragma warning disable 665
            if (sessionTooLong = sessionLogs.Where(s => s.LoggedInDateTime > lastallowedLoginDateTimeLocalTime).None())
#pragma warning restore 665
            {
                expired = false;
                return false; //over 8 hours or so
            }



#pragma warning disable 665
            //FIX: Convert UTC to LocalTime
            if (expired = (sessionLogs.Where(s => s.LastAccessedDateTime > lastallowedAccessedDateTimeLocalTime).None()
				|| sessionLogs.Any(s => s.LoggedOutDateTime <= now)))
#pragma warning restore 665
            {
                return false; //over 20 minutes since last activity
            }

            SessionLog[] sessionLogsNotExpired =
                sessionLogs.Where(s => s.LastAccessedDateTime > lastallowedAccessedDateTimeLocalTime).ToArray();

            
            //Update the first (and only) session that came back:
            SessionLog sessionLog = sessionLogs.First();

            //TimeSpan delta = sessionLog.LastAccessedDateTime.Subtract(lastallowedAccessedDateTimeUtc);

            //DateTime now = nowUtc.ToLocalTime();
            //DateTime lastLoggedIn = sessionLog.LastAccessedDateTime.ToLocalTime();

            //FIX:SessionLog LocalTime:
            DateTime localTime = nowUtc.ToLocalTime();
            sessionLog.LastAccessedDateTime = localTime;
            
            //Update and commit immediately.
            _repositoryService.UpdateOnCommit(sessionLog);
            _unitOfWorkService.Commit();

            //Return true, to indicae the Session was committed.
            return true; //session time updated

            //TODO : delete expired sessions or leave it all on the sql query that will be run nightly?
        }

        //private SessionLog FindSession(Guid sessionToken, DateTime dateTime)
        //{
        //    SessionLog sessionLog = 
        //        _repositoryService.GetByFilter<SessionLog>(
        //        s => 
        //            (s.Id == sessionToken) && 
        //            s.LastAccessedDateTime > dateTime).FirstOrDefault();

        //    return sessionLog;
        //}

		public bool DoesSessionExist(Guid sessionToken)
		{
			return _repositoryService.GetByFilter<SessionLog>(s => (s.Id == sessionToken)).Any();
		}

        public void DeleteSession(Guid sessionToken, bool commitNow = false)
        {
            try
            {
                //Broken for now -- _repositoryService.DeleteOnCommit<Session>(session => session.Id == sessionToken);
                IntAltDelete<Session>(session => session.Id == sessionToken);

				// Set the logged out time:
				DateTime nowUtc = _dateTimeService.NowUTC;
				DateTime localTime = nowUtc.ToLocalTime();
				
	            var sessionLogEntry = _repositoryService.GetByFilter<SessionLog>(sl => sl.Id == sessionToken);
				sessionLogEntry.ForEach(sl => sl.LoggedOutDateTime = localTime);
            }
// ReSharper disable EmptyGeneralCatchClause
            catch
// ReSharper restore EmptyGeneralCatchClause
            {
                //throw;
            }
            if (commitNow)
            {
                _unitOfWorkService.Commit();
            }
        }


        public bool CheckEssaAccessTokenHasBeenLogged(string esaaAccessToken)
        {
            throw new NotImplementedException();
        }

        public void LogEssaAccessToken(string esaaAccessToken)
        {
            throw new NotImplementedException();
        }


        void IntAltDelete<TAggregateRootEntity>(Expression<Func<TAggregateRootEntity, bool>> filter)
    where TAggregateRootEntity : class
        {
            // ReSharper disable RedundantArgumentDefaultValue
            IQueryable<TAggregateRootEntity> aggregateRootEntities = _repositoryService.GetByFilter(filter, null, null);
            // ReSharper restore RedundantArgumentDefaultValue

            

            DbContext dbContext = _repositoryService.Context.GetInnerItem<DbContext>();
            DbSet<TAggregateRootEntity> dbSet = dbContext.Set<TAggregateRootEntity>();
            
            foreach (TAggregateRootEntity entity in aggregateRootEntities)
            {
                DbEntityEntry<TAggregateRootEntity> entityEntry = dbContext.Entry(entity);
                EntityState state = entityEntry.State;
                if (((int)state).BitIsSet((int)EntityState.Detached))
                {
                    dbSet.Attach(entity);
                }
                dbContext.Set<TAggregateRootEntity>().Remove(entity);
            }
            dbContext.SaveChanges();

            //return ForceCommit(forceCommitNow);
        }

    }
}