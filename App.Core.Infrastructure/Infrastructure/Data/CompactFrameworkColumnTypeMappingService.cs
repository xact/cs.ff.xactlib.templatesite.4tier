namespace App.Core.Infrastructure.Data
{
    using System.Collections.Generic;

    public class CompactFrameworkColumnTypeMappingService : IColumnTypeMappingService
    {
        private readonly IDictionary<string, string> _columnMapping;

        public CompactFrameworkColumnTypeMappingService()
        {
            this._columnMapping = new Dictionary<string, string>
                                 {
                                     {"date", /*column type will be changed to */ "datetime"},
                                     //compact edition doesn't have 'date' type
                                     {"nvarchar(max)", /*column type will be changed to */ "ntext"}
                                     //compact edition cannot do more than 4000 characters in nvarchar and serialized identity is longer
                                 };
        }

		public string GetColumnType(string columnType)
        {
            return _columnMapping[columnType];
        }
    }
}