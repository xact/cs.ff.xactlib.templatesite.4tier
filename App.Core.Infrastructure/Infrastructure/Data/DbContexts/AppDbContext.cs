﻿
// ReSharper disable CheckNamespace


using System.Collections;
using System.Linq;

namespace App.Core.Infrastructure.Data.DbContexts
// ReSharper restore CheckNamespace
{
    using System.Data.Common;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using App.Core.Infrastructure.Data.DatabaseInitializers;
    using App.Core.Infrastructure.Migrations;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Data.EF.CodeFirst;
    using XAct.Library.Settings;

    /// <summary>
    /// <para>
    /// See also <see cref="MigrationsAppDbContext"/>
    /// which provides a specialization of this <see cref="AppDbContext"/>
    /// that ensures that the IoC Container is initialized (
    /// as migrations in a CI environment use a DbContext directly,
    /// skipping any application bootstrapping sequence).
    /// </para>
    /// </summary>
    public class AppDbContext : DbContext
    {
        private static bool _configured = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppDbContext" /> class.
        /// <para>
        /// This is the Constructor used by the application in most/all cases
        /// as this is the constructor invoked from 
        /// <see cref="UnitOfWorkServiceConfiguration"/>
        /// </para>
        /// <para>
        /// But even when invoked through the other constructor,
        /// (one with a connection string, which is the one the Continous Integration
        /// invokes when doing a Migration on the build server) 
        /// it will hit this constructor *first* before returning
        /// to complete the steps in that one...
        /// </para>
        /// </summary>
        public AppDbContext()
            : base(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name)
        {
            //As this is on the parameter-less constructor, 
            //gets invoked by everyone.
            ConfigureDbContextInstance();
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="AppDbContext" /> class.
        /// <para>
        /// Important:
        /// Constructor invoked by <see cref="Configuration"/>
        /// when Migrations invoked from the Build Server -- but it 
        /// hits the parameterless constructor first, and therefore
        /// by the time it gets here, the IoC Container is already configured.
        /// </para>
        /// <para>
        /// This connection based constructor is also invoked by unit tests.
        /// </para>
        /// <para>
        /// Other than that, all normal operations are done via the other (parameterless)
        /// constructor.
        /// </para>
        /// </summary>
        /// <param name="nameOrConnectionString">Either the database name or a connection string.</param>
        public AppDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            //Runs other (parameterless) constructor first, so no settings 
            //need to be specifically set here.
            ConfigureDbContextInstance();
        }

		public AppDbContext(DbConnection dbConnection, bool contextOwnsConnection = true)
			: base(dbConnection, contextOwnsConnection)
		{
			ConfigureDbContextInstance();
		}






        //Called by every constructor:
        private void ConfigureDbContextInstance()
		{
		    if (_configured)
		    {
		        return;
            }

            // Ensures:
            // * IoC is available, 
            // * that LazyLoading/Proxies are turned off
            // *  

            //Regrettable have to turn off use of Proxies.
			//Reason was exposed by GetStudent method (which includes Providers, AltNames, Student slaves) blows up
			//with following error:

			//http://stackoverflow.com/questions/13077328/serialization-of-entity-framework-objects-with-one-to-many-relationship
			this.Configuration.ProxyCreationEnabled = false;
			this.Configuration.LazyLoadingEnabled = false;

			//The following causes failure of Tests due to CE not wanting Non-Zero CommandTimeout.
		    Database.CommandTimeout = null;// 1200;

            InitializeDatabase(
                new AppDbContextMigrateDatabaseToLatestVersionInitializer(),
                false,
                false);

		    _configured = true;
		}

        private static bool _InitializeLoaded;
        private static object _InitializeLock = new object();

        /// <summary>
        /// Initializes the database.
        /// </summary>
        /// <internal>
        /// Invoked by:
        /// * SqlCeResetedByCopyingDbFileDatabaseLifeCycleController
        /// * SqlDropInitDbAlwaysLifeCycleController
        /// </internal>
        /// <param name="initializer">The initializer.</param>
        /// <param name="force"></param>
        /// <returns></returns>
        public void InitializeDatabase(IDatabaseInitializer<AppDbContext> initializer, bool force = true,
            bool initializeNow = false)
        {
            if (force)
            {
                _InitializeLoaded = false;
            }

            if (_InitializeLoaded)
            {
                return;
            }

            // watch race condition
            lock (_InitializeLock)
            {
                // are we sure?
                if (_InitializeLoaded)
                {
                    return;
                }

                _InitializeLoaded = true;

                // force Initializer to load only once
                AppDbContextBootstrapper.Initialize();
                System.Data.Entity.Database.SetInitializer<AppDbContext>(initializer);

                if (initializeNow)
                {
                    this.Database.Initialize(force);
                }
            }
        }



        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //Defining Models within AppDbContext's OnModelCreating method 
            //is standard and easy....
            //....but that only initializes models that are known to the current application's 
            //domain -- and doesn't address any model definitions defined in XActLib 
            //or any other 3rd party assembly.

            //this.BuildModelsAutomatically(modelBuilder,true);

            //modelBuilder.Ignore<HelpEntryCategory>();
            //modelBuilder.Ignore<HelpEntry>();
            //modelBuilder.Ignore<HelpEntryTag>();


            ////Get from everything in bin:
            ////ICollection<Type> modelBuilderTypesFound =
            ////    AppDomain.CurrentDomain.GetTypesInmplementingType<IDbModelBuilder>();

            ////Vice versa, get from everything associated to this assembly:
            //var assemblyNames = this.GetType().Assembly.GetReferencedAssemblies();
            //var assemblies = assemblyNames.Select(System.Reflection.Assembly.Load).ToArray();
            //ICollection<Type> modelBuilderTypesFound =
            //    assemblies.GetTypesInmplementingType<IDbModelBuilder>(true);

            //var r = new List<string>();
            //modelBuilderTypesFound.ForEach(x=>r.Add("{0}: {1}".FormatStringInvariantCulture(x.Assembly,x.FullName)));
            //var check = string.Join(Environment.NewLine, r);

            //modelBuilderTypesFound.ForEach(x =>
            //{
            //    IDbModelBuilder dbModelBuilder = ((IDbModelBuilder) XAct.DependencyResolver.Current.GetInstance(x));
            //    dbModelBuilder.OnModelCreating(modelBuilder);
            //});

            this.AutoBuildModels(
                modelBuilder: modelBuilder, 
                invokeModelBuildingWhenFound: true, 
                searchDomain: false);

            //base doesn't do anything, but just not to break anything:
            base.OnModelCreating(modelBuilder);
        }



        /// <summary>
        /// Public method used by Tests to seed a new DbContext.
        /// <para>
        /// Note that it is not checked to see if it has already run,
        /// and can be invoked several times. This could be an issue.
        /// </para>
        /// </summary>
        public void Seed()
        {
            IHostSettingsService hostSettingsService =
                XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();
            XAct.Library.Settings.Db.SeedingType = hostSettingsService.SeedingType;

            if (hostSettingsService.SeedingType != SeedingType.SkipSeeding)
            {
                this.AutoSeed<IDbContextSeeder>(false);
            }

            //SeedInvoker seedInvoker = new SeedInvoker();
            //seedInvoker.Seed(this.GetType().Name, this);

        }

    }
}



