﻿using App.Core.Infrastructure.Initialization.Steps.PostIoC;
using App.Core.Infrastructure.Initialization.Steps.PreIoC;

namespace App.Core.Infrastructure.Data.DbContexts
{
    using App.Core.Infrastructure.Initialization.Steps;
    using App.Core.Infrastructure.Migrations;

    /// <summary>
    /// <para>
    /// The purpose of the <see cref="AppDbContextBootstrapper"/>
    /// is to ensure that th IoC container being used is available
    /// if the AppDbContext is constructed outside the normal run of the application...
    /// as it is when invoked from the Package Manager Console, or from a Build Server.
    /// </para>
    /// </summary>
    public static class AppDbContextBootstrapper
    {

        private static bool _ioCInitialized = false;
        private static readonly object _iocInitializedlock = new object();


        /// <summary>
        /// Ensures the io c context when migrating.
        /// <para>
        /// Invoked from <see cref="AppDbContext.InitializeDatabase"/>
        /// </para>
        /// </summary>
        public static void Initialize()
        {
            if (XAct.DependencyResolver.DependencyInjectionContainer != null)
            {
                return;
            }

            if (_ioCInitialized)
            {
                return;
            }

            lock (_iocInitializedlock)
            {
                if (_ioCInitialized)
                {
                    return;
                }
                //Reuse the Initialization Step:
                new RegisterAllServicesInIoCInitializationStep().Execute();

                //This ensures that NConfig is not running in release mode,
                //even just on the CI (don't want CPU issues there either, when there is a missing app.config).
                new ConfigureNConfigInitializationStep().Execute();


                ////In run time, by the time we have gotten here, 
                ////services are up and running.

                //Set flag:
                _ioCInitialized = true;
            }//~lock

        }


    }
}