﻿namespace App.Core.Infrastructure.Data.DatabaseInitializers
{
    using System.Data.Entity;
    using App.Core.Infrastructure.Data.DbContexts;
    using App.Core.Infrastructure.Services;

    /// <summary>
    /// 
    /// </summary>
    public class AppDbContextMigrateDatabaseToLatestVersionInitializer :
        MigrateDatabaseToLatestVersion<AppDbContext, Migrations.Configuration>
    {

        public AppDbContextMigrateDatabaseToLatestVersionInitializer()
        {
            //Note: do not instantiate a DbMigrator here: stack overflow.
        }
    }
}
