namespace App.Core.Infrastructure.Data.Mapping
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using App.Core.Domain.Models;
    using App.Core.Infrastructure.Data.Maps;

    //Naming pattern is client requested.
    public abstract class ReferenceDataEntityTypeConfigurationBase<TEntity, TEntityEnum> : 
        EntityTypeConfigurationBase<TEntity>
        where TEntity : ReferenceDataBase<TEntityEnum>
        where TEntityEnum : struct, IConvertible
    {

        protected ReferenceDataEntityTypeConfigurationBase(string prefix)
        {
            this.ToTable("Reference_" + prefix);

            // Primary Key
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName(prefix + "Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Properties
            this.Property(t => t.Code).HasColumnName(prefix + "Code").IsRequired().HasMaxLength(10);
            this.Property(t => t.Text).HasColumnName(prefix + "Text").IsRequired().HasMaxLength(100);
            this.Property(t => t.Description).HasColumnName(prefix + "Description").IsRequired().HasMaxLength(200);



            //this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy").IsRequired().HasMaxLength(100);
            //this.Property(t => t.ModifiedDateTime).HasColumnName("ModifiedDateTime");


        }
    }
}