﻿									
Background information.

Whe the application is initializing (see InfrastructureBootstrapper)
one of the steps is to get the AppSettings.

Since AppSettings are a combination of both AppSetttings, and remote
DbSettings, this will probably be the first connection to the Db.

Therefore, RepositoryService -- the only service through which
all interaction through which the app talks to EF, and therefore the
Db -- will ask AppDbContextFactory to create an AppDbContext for it.

AppContext in turn goes and calls OnModelCreating()...which
uses an XACtLib helper to reflect over all asssemblies looking for
Methods that can participate in the building of the Model.

Once the DbContext knows the Model, it has to make a decision as to 
whether the current Db is of the right schema/shape to work with, or
abort.

It does this using a DbInitializer -- which compares the model it 
now kows about against the schema of the Db.

And depending on which Initializer is used, decides to Drop the Db
before recreating the Db...or try to keep working. There are a couple
of Db Intiializers to choose from (eg the most dangerous, which is 
DropCreateDatabaseAlways<TDbContext>).

IN our case we want a custom one, inheriting from 
DropCreateDatabaseIfModelChanges, but that has an override of the 
Seed() method to use another XAct helper method to search for 
all Seed methods.

It's magic...but it allows for the App to seed as it need be
*as well* as the Library to seed its Reference tables as well.

