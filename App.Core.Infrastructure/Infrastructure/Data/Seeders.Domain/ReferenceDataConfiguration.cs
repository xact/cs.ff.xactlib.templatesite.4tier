

namespace App.Core.Infrastructure.Data.Seeders.Domain
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Globalization;
    using System.Linq.Expressions;
    using XAct;
    using XAct.Resources;

    public class ReferenceDataConfiguration<TReferenceDataType, TId>
        where TReferenceDataType : class, IHasCodedReferenceData<TId>, new()
        where TId:struct
    {


        private static Guid GenGuid()
        {
            return XAct.DependencyResolver.Current.GetInstance<IDistributedIdService>().NewGuid();
        }


        public ReferenceDataConfiguration(DbContext dbContext, Expression<Func<TReferenceDataType, object>> identifierExpression, string filter)
        {
            Resources = dbContext.Set<Resource>();
            References = dbContext.Set<TReferenceDataType>();
            DbContext = dbContext;
            IdentifierExpression = identifierExpression;
            Filter = filter;
        }

        public IDbSet<Resource> Resources { get; private set; }
        public IDbSet<TReferenceDataType> References { get; private set; }
        public DbContext DbContext { get; private set; }
        public Expression<Func<TReferenceDataType, object>> IdentifierExpression { get; private set; }
        public string Filter { get; private set; }

        public ReferenceDataConfiguration<TReferenceDataType, TId> AddOrUpdate(
            TId id, //ID used as PK
            string resourceText,
            string ministryCodeTag,
            int order = 0, 
            bool enabled = true,
            string description = "n/a",
			Action<TReferenceDataType> addMoreInfoAction = null
            )
        {
            string resourceKey = id.ToString();

            if (resourceText.IsNullOrEmpty())
            {
                resourceText = resourceKey.SplitPCase().ToSentenceCase();
            }
            if (description.IsNullOrEmpty())
            {
                description = "n/a";
            }
            string resourceFilter = null;
            TReferenceDataType referenceData = 
                new TReferenceDataType().X(
                    id, 
                    resourceFilter, 
                    resourceKey, 
                    ministryCodeTag, 
                    enabled, 
                    order, 
                    description);

	        if (addMoreInfoAction != null)
	        {
		        addMoreInfoAction.Invoke(referenceData);
	        }

	        References.AddOrUpdate(IdentifierExpression, referenceData);

            Resource resource = new Resource(GenGuid(),  Filter, resourceKey, resourceText, CultureInfo.InvariantCulture);

            Resources.AddOrUpdate(m => new { m.Filter, m.Key, m.CultureCode }, resource);

            return this;
        }

        public void Save()
        {
            DbContext.SaveChanges();


        }
    }
}