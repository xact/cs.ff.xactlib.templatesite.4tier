namespace App.Core.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "XActLib.XActLib_Counter",
                c => new
                    {
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ApplicationTennantId = c.Guid(nullable: false),
                        Target = c.String(nullable: false, maxLength: 64),
                        Key = c.String(nullable: false, maxLength: 64),
                        Value = c.Int(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        MarkedForDeletionDateTimeUtc = c.DateTime(),
                        MarkedForDeletionKey = c.Guid(),
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ApplicationTennantId, t.Target, t.Key }, unique: true, name: "IX_ApplicationTennantId_Id_Key");
            
            CreateTable(
                "XActLib.XActLib_DataStoreUpdateLog",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        EnvironmentIdentifier = c.String(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "XActLib.XActLib_ApplicationSettings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        EnvironmentIdentifier = c.String(nullable: false, maxLength: 16),
                        ZoneOrTierIdentifier = c.String(nullable: false, maxLength: 16),
                        HostIdentifier = c.String(nullable: false, maxLength: 16),
                        TennantIdentifier = c.Guid(nullable: false),
                        Key = c.String(nullable: false, maxLength: 128),
                        Enabled = c.Boolean(nullable: false),
                        SerializationMethod = c.Int(nullable: false),
                        SerializedValueType = c.String(nullable: false, maxLength: 1024),
                        SerializedValue = c.String(maxLength: 4000),
                        SerializedDefaultValue = c.String(maxLength: 2048),
                        IsUnlockedInformation = c.String(maxLength: 1024),
                        IsReadableAuthorisationInformation = c.String(maxLength: 1024),
                        IsWritableAuthorisationInformation = c.String(maxLength: 1024),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        Tag = c.String(maxLength: 256),
                        Metadata = c.String(maxLength: 1024),
                        Description = c.String(maxLength: 4000),
                        SerializationMethodRaw = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.EnvironmentIdentifier, t.ZoneOrTierIdentifier, t.HostIdentifier, t.TennantIdentifier, t.Key }, unique: true, name: "IX_EnvAppZoneHostTennantKey");
            
            CreateTable(
                "XActLib.XActLib_ApplicationSettingsAudit",
                c => new
                    {
                        ApplicationTennantId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        SettingFK = c.Guid(nullable: false),
                        Key = c.String(nullable: false, maxLength: 128),
                        SerializationMethod = c.Int(nullable: false),
                        SerializedValueType = c.String(nullable: false, maxLength: 1024),
                        SerializedPreviousValue = c.String(maxLength: 2048),
                        SerializedValue = c.String(maxLength: 4000),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false, maxLength: 64),
                        CreatedByOrganisation = c.String(maxLength: 64),
                        Tag = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ApplicationTennantId, t.Id }, unique: true);
            
            CreateTable(
                "XActLib.XActLib_SettingsRenderingInformation",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 1024),
                        RenderHintsIdentifier = c.String(),
                        RenderGroupingHints = c.String(),
                        RenderOrderHint = c.Int(nullable: false),
                        RenderingImageHints = c.String(),
                        RenderLabelHints = c.String(),
                        RenderViewControlHints = c.String(),
                        RenderEditControlHints = c.String(),
                        RenderEditValidationHints = c.String(),
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "XActLib.XActLib_Resource",
                c => new
                    {
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Filter = c.String(nullable: false, maxLength: 256),
                        Key = c.String(nullable: false, maxLength: 128),
                        CultureCode = c.String(nullable: false, maxLength: 16),
                        Value = c.String(nullable: false),
                        Id = c.Guid(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.Filter, t.Key, t.CultureCode }, unique: true, name: "IX_ResourceFilterKeyCulture");
            
            CreateTable(
                "XActLib.XActLib_HelpEntry",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentFK = c.Guid(),
                        Enabled = c.Boolean(nullable: false),
                        Key = c.String(nullable: false, maxLength: 256),
                        ResourceFilter = c.String(maxLength: 256),
                        Title = c.String(maxLength: 1024),
                        Text = c.String(maxLength: 4000),
                        CategoryFK = c.Guid(nullable: false),
                        Description = c.String(maxLength: 4000),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_HelpEntryCategory", t => t.CategoryFK, cascadeDelete: true)
                .ForeignKey("XActLib.XActLib_HelpEntry", t => t.ParentFK)
                .Index(t => t.ParentFK)
                .Index(t => t.Key, unique: true)
                .Index(t => t.CategoryFK);
            
            CreateTable(
                "XActLib.XActLib_HelpEntryCategory",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Enabled = c.Boolean(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "XActLib.XActLib_HelpEntryTag",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Enabled = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "XActLib.XActLib_Role",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ApplicationTennantId = c.Guid(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        Code = c.String(nullable: false, maxLength: 64),
                        ResourceFilter = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        ParentFK = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_Role", t => t.ParentFK)
                .Index(t => t.ParentFK);
            
            CreateTable(
                "XActLib.XActLib_UserToRole",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ApplicationTennantId = c.Guid(nullable: false),
                        UserIdentifier = c.String(nullable: false, maxLength: 64),
                        RoleFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_Role", t => t.RoleFK)
                .Index(t => t.RoleFK);
            
            CreateTable(
                "XActLib.XActLib_Operation",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 256),
                        Enabled = c.Boolean(nullable: false),
                        Note = c.String(maxLength: 1024),
                        Id = c.Int(nullable: false, identity: true),
                        AllowAnonymous = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "XActLib.XActLib_OperationPermission",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        Enabled = c.Boolean(nullable: false),
                        Description = c.String(maxLength: 4000),
                        OperationFK = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                        Deny = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_Operation", t => t.OperationFK, cascadeDelete: true)
                .Index(t => t.OperationFK);
            
            CreateTable(
                "XActLib.XActLib_MessageAddress",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        MessageId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(maxLength: 254),
                        Value = c.String(nullable: false, maxLength: 254),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .Index(t => t.MessageId);
            
            CreateTable(
                "XActLib.XActLib_MessageAttachment",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        MessageId = c.Guid(nullable: false),
                        Type = c.Int(),
                        ContentType = c.String(nullable: false, maxLength: 256),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        Size = c.Int(),
                        Name = c.String(nullable: false, maxLength: 256),
                        Value = c.Binary(nullable: false),
                        ContentId = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .Index(t => t.MessageId);
            
            CreateTable(
                "XActLib.XActLib_MessageBody",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        MessageId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(maxLength: 64),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .Index(t => t.MessageId);
            
            CreateTable(
                "XActLib.XActLib_MessageHeader",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        MessageId = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 1024),
                        Value = c.String(nullable: false, maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .Index(t => t.MessageId);
            
            CreateTable(
                "XActLib.XActLib_Message",
                c => new
                    {
                        MessageId = c.Guid(nullable: false),
                        ApplicationTennantId = c.Guid(nullable: false),
                        IsDraft = c.Boolean(nullable: false),
                        Status = c.Int(nullable: false),
                        Importance = c.Int(nullable: false),
                        Priority = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(),
                        SentOn = c.DateTime(),
                        DeliveryProtocols = c.String(maxLength: 1024),
                        Subject = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.MessageId);
            
            CreateTable(
                "XActLib.XActLib_MessageStatus",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        MessageId = c.Guid(nullable: false),
                        Source = c.String(maxLength: 1024),
                        Status = c.Int(nullable: false),
                        Note = c.String(maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .Index(t => t.MessageId);
            
            CreateTable(
                "XActLib.XActLib_MessageTag",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Enabled = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "XActLib.XActLib_PersistedFile",
                c => new
                    {
                        ApplicationTennantId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Name = c.String(nullable: false, maxLength: 256),
                        Value = c.Binary(),
                        ContentId = c.String(maxLength: 256),
                        ContentType = c.String(nullable: false, maxLength: 256),
                        CreatedOnUtc = c.DateTime(),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        Tag = c.String(maxLength: 256),
                        Description = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ApplicationTennantId, t.Id }, unique: true, name: "IX_ApplicationTennantId_Id_Key");
            
            CreateTable(
                "XActLib.XActLib_PersistedFileMetadata",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Key = c.String(nullable: false, maxLength: 64),
                        Value = c.String(maxLength: 256),
                        OwnerFK = c.Guid(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false, maxLength: 64),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedBy = c.String(nullable: false, maxLength: 64),
                        Tag = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_PersistedFile", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "XActLib.XActLib_ProgressStatus",
                c => new
                    {
                        ApplicationTennantId = c.Guid(nullable: false),
                        Id = c.Int(nullable: false),
                        Progress = c.Int(nullable: false),
                        Value = c.Int(nullable: false),
                        Title = c.String(maxLength: 256),
                        Text = c.String(maxLength: 2048),
                        CreatedOnUtc = c.DateTime(),
                        LastModifiedOnUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ApplicationTennantId, t.Id }, unique: true, name: "IX_ApplicationTennantId_Id_Key");
            
            CreateTable(
                "XActLib.XActLib_ScheduledTaskExecutionLog",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ScheduledTaskFK = c.Guid(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        DurationTicks = c.Long(nullable: false),
                        ResultStatus = c.Int(nullable: false),
                        Summary = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "XActLib.XActLib_ScheduledTask",
                c => new
                    {
                        ApplicationTennantId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Enabled = c.Boolean(nullable: false),
                        GroupName = c.String(maxLength: 64),
                        Name = c.String(nullable: false, maxLength: 64),
                        StartDelayTicks = c.Long(nullable: false),
                        ChronSchedule = c.String(nullable: false, maxLength: 128),
                        IsExecutable = c.Boolean(nullable: false),
                        ExecutablePath = c.String(maxLength: 512),
                        ExecutableArguments = c.String(maxLength: 4000),
                        AssemblyName = c.String(maxLength: 256),
                        TypeFullName = c.String(maxLength: 128),
                        MethodName = c.String(maxLength: 128),
                        Description = c.String(maxLength: 4000),
                        DeletedOnUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ApplicationTennantId, t.Id }, unique: true);
            
            CreateTable(
                "XActLib.XActLib_ScheduledTaskMetadata",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ScheduledTaskFK = c.Guid(nullable: false),
                        IsAMethodArgument = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        Key = c.String(nullable: false, maxLength: 64),
                        SerializationMethod = c.Int(nullable: false),
                        SerializedValueType = c.String(nullable: false, maxLength: 1024),
                        SerializedValue = c.String(maxLength: 4000),
                        Description = c.String(maxLength: 4000),
                        Enabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_ScheduledTask", t => t.ScheduledTaskFK, cascadeDelete: true)
                .Index(t => t.ScheduledTaskFK);
            
            CreateTable(
                "XActLib.XActLib_UserClaim",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Key = c.String(nullable: false, maxLength: 64),
                        SerializedValueType = c.String(nullable: false, maxLength: 1024),
                        SerializationMethod = c.Int(nullable: false),
                        SerializedValue = c.String(maxLength: 4000),
                        CreatedBy = c.String(nullable: false, maxLength: 64),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        LastModifiedBy = c.String(nullable: false, maxLength: 64),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        UserFK = c.Guid(nullable: false),
                        Authority = c.String(nullable: false, maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_User", t => t.UserFK, cascadeDelete: true)
                .Index(t => t.UserFK);
            
            CreateTable(
                "XActLib.XActLib_UserProperty",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Key = c.String(nullable: false, maxLength: 64),
                        SerializedValueType = c.String(nullable: false, maxLength: 1024),
                        SerializationMethod = c.Int(nullable: false),
                        SerializedValue = c.String(maxLength: 4000),
                        CreatedBy = c.String(nullable: false, maxLength: 64),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        LastModifiedBy = c.String(nullable: false, maxLength: 64),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        UserFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("XActLib.XActLib_User", t => t.UserFK, cascadeDelete: true)
                .Index(t => t.UserFK);
            
            CreateTable(
                "XActLib.XActLib_User",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Enabled = c.Boolean(nullable: false),
                        CreatedBy = c.String(nullable: false, maxLength: 64),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        LastModifiedBy = c.String(nullable: false, maxLength: 64),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        DeletedBy = c.String(maxLength: 64),
                        DeletedOnUtc = c.DateTime(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "XActLib.XActLib_HelpEntry_HelpEntryTag",
                c => new
                    {
                        HelpEntryId = c.Guid(nullable: false),
                        HelpTagId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.HelpEntryId, t.HelpTagId })
                .ForeignKey("XActLib.XActLib_HelpEntry", t => t.HelpEntryId, cascadeDelete: true)
                .ForeignKey("XActLib.XActLib_HelpEntryTag", t => t.HelpTagId, cascadeDelete: true)
                .Index(t => t.HelpEntryId)
                .Index(t => t.HelpTagId);
            
            CreateTable(
                "XActLib.XActLib_Message_MessageTag",
                c => new
                    {
                        MessageId = c.Guid(nullable: false),
                        MessageTagId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.MessageId, t.MessageTagId })
                .ForeignKey("XActLib.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .ForeignKey("XActLib.XActLib_MessageTag", t => t.MessageTagId, cascadeDelete: true)
                .Index(t => t.MessageId)
                .Index(t => t.MessageTagId);
            
            CreateTable(
                "XActLib.XActLib_User_User",
                c => new
                    {
                        MessageId = c.Guid(nullable: false),
                        MessageTagId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.MessageId, t.MessageTagId })
                .ForeignKey("XActLib.XActLib_User", t => t.MessageId)
                .ForeignKey("XActLib.XActLib_User", t => t.MessageTagId)
                .Index(t => t.MessageId)
                .Index(t => t.MessageTagId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("XActLib.XActLib_UserProperty", "UserFK", "XActLib.XActLib_User");
            DropForeignKey("XActLib.XActLib_UserClaim", "UserFK", "XActLib.XActLib_User");
            DropForeignKey("XActLib.XActLib_User_User", "MessageTagId", "XActLib.XActLib_User");
            DropForeignKey("XActLib.XActLib_User_User", "MessageId", "XActLib.XActLib_User");
            DropForeignKey("XActLib.XActLib_ScheduledTaskMetadata", "ScheduledTaskFK", "XActLib.XActLib_ScheduledTask");
            DropForeignKey("XActLib.XActLib_PersistedFileMetadata", "OwnerFK", "XActLib.XActLib_PersistedFile");
            DropForeignKey("XActLib.XActLib_Message_MessageTag", "MessageTagId", "XActLib.XActLib_MessageTag");
            DropForeignKey("XActLib.XActLib_Message_MessageTag", "MessageId", "XActLib.XActLib_Message");
            DropForeignKey("XActLib.XActLib_MessageStatus", "MessageId", "XActLib.XActLib_Message");
            DropForeignKey("XActLib.XActLib_MessageHeader", "MessageId", "XActLib.XActLib_Message");
            DropForeignKey("XActLib.XActLib_MessageBody", "MessageId", "XActLib.XActLib_Message");
            DropForeignKey("XActLib.XActLib_MessageAttachment", "MessageId", "XActLib.XActLib_Message");
            DropForeignKey("XActLib.XActLib_MessageAddress", "MessageId", "XActLib.XActLib_Message");
            DropForeignKey("XActLib.XActLib_OperationPermission", "OperationFK", "XActLib.XActLib_Operation");
            DropForeignKey("XActLib.XActLib_UserToRole", "RoleFK", "XActLib.XActLib_Role");
            DropForeignKey("XActLib.XActLib_Role", "ParentFK", "XActLib.XActLib_Role");
            DropForeignKey("XActLib.XActLib_HelpEntry_HelpEntryTag", "HelpTagId", "XActLib.XActLib_HelpEntryTag");
            DropForeignKey("XActLib.XActLib_HelpEntry_HelpEntryTag", "HelpEntryId", "XActLib.XActLib_HelpEntry");
            DropForeignKey("XActLib.XActLib_HelpEntry", "ParentFK", "XActLib.XActLib_HelpEntry");
            DropForeignKey("XActLib.XActLib_HelpEntry", "CategoryFK", "XActLib.XActLib_HelpEntryCategory");
            DropIndex("XActLib.XActLib_User_User", new[] { "MessageTagId" });
            DropIndex("XActLib.XActLib_User_User", new[] { "MessageId" });
            DropIndex("XActLib.XActLib_Message_MessageTag", new[] { "MessageTagId" });
            DropIndex("XActLib.XActLib_Message_MessageTag", new[] { "MessageId" });
            DropIndex("XActLib.XActLib_HelpEntry_HelpEntryTag", new[] { "HelpTagId" });
            DropIndex("XActLib.XActLib_HelpEntry_HelpEntryTag", new[] { "HelpEntryId" });
            DropIndex("XActLib.XActLib_UserProperty", new[] { "UserFK" });
            DropIndex("XActLib.XActLib_UserClaim", new[] { "UserFK" });
            DropIndex("XActLib.XActLib_ScheduledTaskMetadata", new[] { "ScheduledTaskFK" });
            DropIndex("XActLib.XActLib_ScheduledTask", new[] { "ApplicationTennantId", "Id" });
            DropIndex("XActLib.XActLib_ProgressStatus", "IX_ApplicationTennantId_Id_Key");
            DropIndex("XActLib.XActLib_PersistedFileMetadata", new[] { "OwnerFK" });
            DropIndex("XActLib.XActLib_PersistedFile", "IX_ApplicationTennantId_Id_Key");
            DropIndex("XActLib.XActLib_MessageStatus", new[] { "MessageId" });
            DropIndex("XActLib.XActLib_MessageHeader", new[] { "MessageId" });
            DropIndex("XActLib.XActLib_MessageBody", new[] { "MessageId" });
            DropIndex("XActLib.XActLib_MessageAttachment", new[] { "MessageId" });
            DropIndex("XActLib.XActLib_MessageAddress", new[] { "MessageId" });
            DropIndex("XActLib.XActLib_OperationPermission", new[] { "OperationFK" });
            DropIndex("XActLib.XActLib_UserToRole", new[] { "RoleFK" });
            DropIndex("XActLib.XActLib_Role", new[] { "ParentFK" });
            DropIndex("XActLib.XActLib_HelpEntry", new[] { "CategoryFK" });
            DropIndex("XActLib.XActLib_HelpEntry", new[] { "Key" });
            DropIndex("XActLib.XActLib_HelpEntry", new[] { "ParentFK" });
            DropIndex("XActLib.XActLib_Resource", "IX_ResourceFilterKeyCulture");
            DropIndex("XActLib.XActLib_ApplicationSettingsAudit", new[] { "ApplicationTennantId", "Id" });
            DropIndex("XActLib.XActLib_ApplicationSettings", "IX_EnvAppZoneHostTennantKey");
            DropIndex("XActLib.XActLib_Counter", "IX_ApplicationTennantId_Id_Key");
            DropTable("XActLib.XActLib_User_User");
            DropTable("XActLib.XActLib_Message_MessageTag");
            DropTable("XActLib.XActLib_HelpEntry_HelpEntryTag");
            DropTable("XActLib.XActLib_User");
            DropTable("XActLib.XActLib_UserProperty");
            DropTable("XActLib.XActLib_UserClaim");
            DropTable("XActLib.XActLib_ScheduledTaskMetadata");
            DropTable("XActLib.XActLib_ScheduledTask");
            DropTable("XActLib.XActLib_ScheduledTaskExecutionLog");
            DropTable("XActLib.XActLib_ProgressStatus");
            DropTable("XActLib.XActLib_PersistedFileMetadata");
            DropTable("XActLib.XActLib_PersistedFile");
            DropTable("XActLib.XActLib_MessageTag");
            DropTable("XActLib.XActLib_MessageStatus");
            DropTable("XActLib.XActLib_Message");
            DropTable("XActLib.XActLib_MessageHeader");
            DropTable("XActLib.XActLib_MessageBody");
            DropTable("XActLib.XActLib_MessageAttachment");
            DropTable("XActLib.XActLib_MessageAddress");
            DropTable("XActLib.XActLib_OperationPermission");
            DropTable("XActLib.XActLib_Operation");
            DropTable("XActLib.XActLib_UserToRole");
            DropTable("XActLib.XActLib_Role");
            DropTable("XActLib.XActLib_HelpEntryTag");
            DropTable("XActLib.XActLib_HelpEntryCategory");
            DropTable("XActLib.XActLib_HelpEntry");
            DropTable("XActLib.XActLib_Resource");
            DropTable("XActLib.XActLib_SettingsRenderingInformation");
            DropTable("XActLib.XActLib_ApplicationSettingsAudit");
            DropTable("XActLib.XActLib_ApplicationSettings");
            DropTable("XActLib.XActLib_DataStoreUpdateLog");
            DropTable("XActLib.XActLib_Counter");
        }
    }
}
