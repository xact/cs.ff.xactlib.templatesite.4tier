
using XAct;
using XAct.Data.EF.CodeFirst;

namespace App.Core.Infrastructure.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Linq;
    using App.Core.Infrastructure.Data;
    using App.Core.Infrastructure.Data.DbContexts;



    /// <summary>
    /// Configuration used by Application
    /// </summary>
    public sealed class Configuration : DbMigrationsConfiguration<AppDbContext>    //ConfigurationBase<AppDbContext> // DbMigrationsConfiguration<AppDbContext>
    {
        protected readonly string _typeName;

        //Notice how flag is boolean, but can also be null...
        private static bool? _pendingMigrationsFlag;

        public Configuration()
        {
            _typeName = this.GetType().Name;

            //DO NOT INVOKE XAct.DependencyResolver.Current.GetInstance in constructor
            //As constructor of this class is invoked by MSBuild (on CI server)
            //before DbContext Constructor is invoked
            //(which lazy builds up ServiceLocation bindings when on CI Server).

            base.AutomaticMigrationDataLossAllowed = true;
            //base.AutomaticMigrationsEnabled = false;
            base.MigrationsDirectory = "Infrastructure\\Data\\Migrations";
            base.CommandTimeout = 1200;
        }


        /// <summary>
        /// Seeds the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        protected override void Seed(AppDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            // Correction. That was in EF4.1 
            // In EF5, it's called every time.
            //http://stackoverflow.com/a/10826073
            //http://stackoverflow.com/a/14157184

            context.AutoSeed<IDbContextSeeder>(
                searchDomain: false,
                assemblies: null);

            //new SeedInvoker().Seed(this._typeName, context);

            base.Seed(context);

            //You have to ensure all changes are Committed before continuing...
            context.SaveChanges();            
        }
    }
}