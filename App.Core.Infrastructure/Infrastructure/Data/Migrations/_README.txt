﻿See connection strings in App.Core.AppHost.Web/Config/MachineSpecific/


    //Find and replace the following:
	//<<ConnectionString>>
	//<NewName>>
	//<<Target>>
  
    //Add a new Migration after making changes:
    add-migration <<NewName>> -projectname  "08.App.Core.Infrastructure"  -startupprojectname "01.App.Core.AppHost.Web" -connectionProviderName "System.Data.SqlClient" -connectionstring "<<ConnectionString>>" 

    //Apply updates (all)
    update-database -projectname  "08.App.Core.Infrastructure"  -startupprojectname "01.App.Core.AppHost.Web" -connectionProviderName "System.Data.SqlClient" -connectionstring "<<ConnectionString>>"  -script
    
    //Apply updates or Rollback (to a specific target)
    update-database -target <<TargetName>> -projectname  "08.App.Core.Infrastructure"  -startupprojectname "01.App.Core.AppHost.Web" -connectionProviderName "System.Data.SqlClient" -connectionstring "<<ConnectionString>>" -script

# Misc
Example connection string:

data source=(LocalDb)\v11.0;initial catalog=App_NTier_01;integrated security=True;MultipleActiveResultSets=True;Connection Timeout=300;

