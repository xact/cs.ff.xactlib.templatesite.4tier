namespace App.Core.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.XActLib_ApplicationSettings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        EnvironmentIdentifier = c.String(nullable: false, maxLength: 16),
                        ZoneOrTierIdentifier = c.String(nullable: false, maxLength: 16),
                        HostIdentifier = c.String(nullable: false, maxLength: 16),
                        TennantIdentifier = c.Guid(nullable: false),
                        Key = c.String(nullable: false, maxLength: 128),
                        Enabled = c.Boolean(nullable: false),
                        SerializationMethod = c.Int(nullable: false),
                        SerializedValueType = c.String(nullable: false, maxLength: 1024),
                        SerializedValue = c.String(),
                        SerializedDefaultValue = c.String(),
                        IsUnlockedInformation = c.String(maxLength: 1024),
                        IsReadableAuthorisationInformation = c.String(maxLength: 1024),
                        IsWritableAuthorisationInformation = c.String(maxLength: 1024),
                        CreatedOnUtc = c.DateTime(),
                        LastModifiedOnUtc = c.DateTime(),
                        Tag = c.String(maxLength: 256),
                        Metadata = c.String(maxLength: 1024),
                        Description = c.String(maxLength: 1024),
                        SerializationMethodRaw = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.EnvironmentIdentifier, t.ZoneOrTierIdentifier, t.HostIdentifier, t.TennantIdentifier, t.Key }, unique: true, name: "IX_EnvAppZoneHostTennantKey");
            
            CreateTable(
                "dbo.XActLib_ApplicationSettingsAudit",
                c => new
                    {
                        ApplicationTennantId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        SettingFK = c.Guid(nullable: false),
                        Key = c.String(nullable: false, maxLength: 128),
                        SerializationMethod = c.Int(nullable: false),
                        SerializedValueType = c.String(nullable: false, maxLength: 1024),
                        SerializedPreviousValue = c.String(),
                        SerializedValue = c.String(),
                        CreatedOnUtc = c.DateTime(),
                        CreatedBy = c.String(nullable: false, maxLength: 64),
                        CreatedByOrganisation = c.String(maxLength: 64),
                        Tag = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ApplicationTennantId, t.Id }, unique: true);
            
            CreateTable(
                "dbo.XActLib_SettingsRenderingInformation",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 1024),
                        RenderHintsIdentifier = c.String(),
                        RenderGroupingHints = c.String(),
                        RenderOrderHint = c.Int(nullable: false),
                        RenderingImageHints = c.String(),
                        RenderLabelHints = c.String(),
                        RenderViewControlHints = c.String(),
                        RenderEditControlHints = c.String(),
                        RenderEditValidationHints = c.String(),
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.XActLib_DataStoreUpdateLog",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        EnvironmentIdentifier = c.String(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.XActLib_Counter",
                c => new
                    {
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ApplicationTennantId = c.Guid(nullable: false),
                        Target = c.String(nullable: false, maxLength: 64),
                        Key = c.String(nullable: false, maxLength: 64),
                        Value = c.Int(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        MarkedForDeletionDateTimeUtc = c.DateTime(),
                        MarkedForDeletionKey = c.Guid(),
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ApplicationTennantId, t.Target, t.Key }, unique: true, name: "IX_ApplicationTennantId_Id_Key");
            
            CreateTable(
                "dbo.XActLib_Resource",
                c => new
                    {
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Filter = c.String(nullable: false, maxLength: 256),
                        Key = c.String(nullable: false, maxLength: 128),
                        CultureCode = c.String(nullable: false, maxLength: 16),
                        Value = c.String(nullable: false),
                        Id = c.Guid(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.Filter, t.Key, t.CultureCode }, unique: true, name: "IX_ResourceFilterKeyCulture");
            
            CreateTable(
                "dbo.XActLib_HelpEntryCategory",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Enabled = c.Boolean(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.XActLib_HelpEntry",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentFK = c.Guid(),
                        Enabled = c.Boolean(nullable: false),
                        Key = c.String(nullable: false, maxLength: 256),
                        Title = c.String(maxLength: 1024),
                        Text = c.String(),
                        ResourceFilter = c.String(maxLength: 256),
                        CategoryFK = c.Guid(nullable: false),
                        Description = c.String(maxLength: 1024),
                        Order = c.Int(nullable: false),
                        HelpEntryCategory_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.XActLib_HelpEntryCategory", t => t.CategoryFK, cascadeDelete: true)
                .ForeignKey("dbo.XActLib_HelpEntry", t => t.ParentFK)
                .ForeignKey("dbo.XActLib_HelpEntryCategory", t => t.HelpEntryCategory_Id)
                .Index(t => t.ParentFK)
                .Index(t => t.Key, unique: true)
                .Index(t => t.CategoryFK)
                .Index(t => t.HelpEntryCategory_Id);
            
            CreateTable(
                "dbo.XActLib_HelpEntryTag",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Enabled = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.XActLib_MessageStatus",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        MessageId = c.Guid(nullable: false),
                        Source = c.String(maxLength: 1024),
                        Status = c.Int(nullable: false),
                        Note = c.String(maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .Index(t => t.MessageId);
            
            CreateTable(
                "dbo.XActLib_MessageHeader",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        MessageId = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 1024),
                        Value = c.String(nullable: false, maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .Index(t => t.MessageId);
            
            CreateTable(
                "dbo.XActLib_MessageAddress",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        MessageId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(maxLength: 254),
                        Value = c.String(nullable: false, maxLength: 254),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .Index(t => t.MessageId);
            
            CreateTable(
                "dbo.XActLib_MessageBody",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        MessageId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(maxLength: 64),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .Index(t => t.MessageId);
            
            CreateTable(
                "dbo.XActLib_MessageAttachment",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        MessageId = c.Guid(nullable: false),
                        Type = c.Int(),
                        ContentType = c.String(nullable: false, maxLength: 256),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        Size = c.Int(),
                        Name = c.String(nullable: false, maxLength: 256),
                        Value = c.Binary(nullable: false),
                        ContentId = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.XActLib_Message", t => t.MessageId, cascadeDelete: true)
                .Index(t => t.MessageId);
            
            CreateTable(
                "dbo.XActLib_MessageTag",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Enabled = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.XActLib_Message",
                c => new
                    {
                        ApplicationTennantId = c.Guid(nullable: false),
                        MessageId = c.Guid(nullable: false),
                        IsDraft = c.Boolean(nullable: false),
                        Status = c.Int(nullable: false),
                        Importance = c.Int(nullable: false),
                        Priority = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(),
                        SentOn = c.DateTime(),
                        DeliveryProtocols = c.String(maxLength: 1024),
                        Subject = c.String(maxLength: 1024),
                        MessageTag_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("dbo.XActLib_MessageTag", t => t.MessageTag_Id)
                .Index(t => t.MessageTag_Id);
            
            CreateTable(
                "dbo.XActLib_PersistedFile",
                c => new
                    {
                        ApplicationTennantId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Name = c.String(nullable: false, maxLength: 256),
                        Value = c.Binary(),
                        ContentId = c.String(maxLength: 256),
                        ContentType = c.String(nullable: false, maxLength: 256),
                        CreatedOnUtc = c.DateTime(),
                        LastModifiedOnUtc = c.DateTime(),
                        DeletedOnUtc = c.DateTime(),
                        Tag = c.String(maxLength: 256),
                        Description = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ApplicationTennantId, t.Id }, unique: true, name: "IX_ApplicationTennantId_Id_Key");
            
            CreateTable(
                "dbo.XActLib_PersistedFileMetadata",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Key = c.String(nullable: false, maxLength: 64),
                        Value = c.String(maxLength: 256),
                        OwnerFK = c.Guid(nullable: false),
                        CreatedOnUtc = c.DateTime(),
                        CreatedBy = c.String(maxLength: 64),
                        LastModifiedOnUtc = c.DateTime(),
                        LastModifiedBy = c.String(maxLength: 64),
                        Tag = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.XActLib_PersistedFile", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "dbo.XActLib_Status",
                c => new
                    {
                        ApplicationTennantId = c.Guid(nullable: false),
                        Id = c.Int(nullable: false, identity: true),
                        Progress = c.Int(nullable: false),
                        Value = c.Int(nullable: false),
                        Title = c.String(maxLength: 256),
                        Text = c.String(maxLength: 2048),
                        CreatedOnUtc = c.DateTime(),
                        LastModifiedOnUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ApplicationTennantId, t.Id }, unique: true, name: "IX_ApplicationTennantId_Id_Key");
            
            CreateTable(
                "dbo.XActLib_ScheduledTask",
                c => new
                    {
                        ApplicationTennantId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Enabled = c.Boolean(nullable: false),
                        GroupName = c.String(maxLength: 64),
                        Name = c.String(nullable: false, maxLength: 64),
                        StartDelayTicks = c.Long(nullable: false),
                        ChronSchedule = c.String(nullable: false, maxLength: 128),
                        IsExecutable = c.Boolean(nullable: false),
                        ExecutablePath = c.String(maxLength: 512),
                        ExecutableArguments = c.String(maxLength: 4000),
                        AssemblyName = c.String(maxLength: 256),
                        TypeFullName = c.String(maxLength: 128),
                        MethodName = c.String(maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 2048),
                        DeletedOnUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ApplicationTennantId, t.Id }, unique: true);
            
            CreateTable(
                "dbo.XActLib_ScheduledTaskMetadata",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ScheduledTaskFK = c.Guid(nullable: false),
                        IsAMethodArgument = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        Key = c.String(nullable: false, maxLength: 64),
                        SerializationMethod = c.Int(nullable: false),
                        SerializedValueType = c.String(nullable: false),
                        SerializedValue = c.String(),
                        Description = c.String(),
                        Enabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.XActLib_ScheduledTask", t => t.ScheduledTaskFK, cascadeDelete: true)
                .Index(t => t.ScheduledTaskFK);
            
            CreateTable(
                "dbo.XActLib_Operation",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 256),
                        Enabled = c.Boolean(nullable: false),
                        Note = c.String(maxLength: 1024),
                        Id = c.Int(nullable: false, identity: true),
                        AllowAnonymous = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.XActLib_OperationPermission",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        Enabled = c.Boolean(),
                        Description = c.String(maxLength: 1024),
                        OperationFK = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                        Deny = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.XActLib_Operation", t => t.OperationFK, cascadeDelete: true)
                .Index(t => t.OperationFK);
            
            CreateTable(
                "dbo.XActLib_HelpEntry_HelEntryTag",
                c => new
                    {
                        HelpEntryId = c.Guid(nullable: false),
                        HelpTagId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.HelpEntryId, t.HelpTagId })
                .ForeignKey("dbo.XActLib_HelpEntry", t => t.HelpEntryId, cascadeDelete: true)
                .ForeignKey("dbo.XActLib_HelpEntryTag", t => t.HelpTagId, cascadeDelete: true)
                .Index(t => t.HelpEntryId)
                .Index(t => t.HelpTagId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.XActLib_OperationPermission", "OperationFK", "dbo.XActLib_Operation");
            DropForeignKey("dbo.XActLib_ScheduledTaskMetadata", "ScheduledTaskFK", "dbo.XActLib_ScheduledTask");
            DropForeignKey("dbo.XActLib_PersistedFileMetadata", "OwnerFK", "dbo.XActLib_PersistedFile");
            DropForeignKey("dbo.XActLib_Message", "MessageTag_Id", "dbo.XActLib_MessageTag");
            DropForeignKey("dbo.XActLib_MessageStatus", "MessageId", "dbo.XActLib_Message");
            DropForeignKey("dbo.XActLib_MessageHeader", "MessageId", "dbo.XActLib_Message");
            DropForeignKey("dbo.XActLib_MessageBody", "MessageId", "dbo.XActLib_Message");
            DropForeignKey("dbo.XActLib_MessageAttachment", "MessageId", "dbo.XActLib_Message");
            DropForeignKey("dbo.XActLib_MessageAddress", "MessageId", "dbo.XActLib_Message");
            DropForeignKey("dbo.XActLib_HelpEntry", "HelpEntryCategory_Id", "dbo.XActLib_HelpEntryCategory");
            DropForeignKey("dbo.XActLib_HelpEntry_HelEntryTag", "HelpTagId", "dbo.XActLib_HelpEntryTag");
            DropForeignKey("dbo.XActLib_HelpEntry_HelEntryTag", "HelpEntryId", "dbo.XActLib_HelpEntry");
            DropForeignKey("dbo.XActLib_HelpEntry", "ParentFK", "dbo.XActLib_HelpEntry");
            DropForeignKey("dbo.XActLib_HelpEntry", "CategoryFK", "dbo.XActLib_HelpEntryCategory");
            DropIndex("dbo.XActLib_HelpEntry_HelEntryTag", new[] { "HelpTagId" });
            DropIndex("dbo.XActLib_HelpEntry_HelEntryTag", new[] { "HelpEntryId" });
            DropIndex("dbo.XActLib_OperationPermission", new[] { "OperationFK" });
            DropIndex("dbo.XActLib_ScheduledTaskMetadata", new[] { "ScheduledTaskFK" });
            DropIndex("dbo.XActLib_ScheduledTask", new[] { "ApplicationTennantId", "Id" });
            DropIndex("dbo.XActLib_Status", "IX_ApplicationTennantId_Id_Key");
            DropIndex("dbo.XActLib_PersistedFileMetadata", new[] { "OwnerFK" });
            DropIndex("dbo.XActLib_PersistedFile", "IX_ApplicationTennantId_Id_Key");
            DropIndex("dbo.XActLib_Message", new[] { "MessageTag_Id" });
            DropIndex("dbo.XActLib_MessageAttachment", new[] { "MessageId" });
            DropIndex("dbo.XActLib_MessageBody", new[] { "MessageId" });
            DropIndex("dbo.XActLib_MessageAddress", new[] { "MessageId" });
            DropIndex("dbo.XActLib_MessageHeader", new[] { "MessageId" });
            DropIndex("dbo.XActLib_MessageStatus", new[] { "MessageId" });
            DropIndex("dbo.XActLib_HelpEntry", new[] { "HelpEntryCategory_Id" });
            DropIndex("dbo.XActLib_HelpEntry", new[] { "CategoryFK" });
            DropIndex("dbo.XActLib_HelpEntry", new[] { "Key" });
            DropIndex("dbo.XActLib_HelpEntry", new[] { "ParentFK" });
            DropIndex("dbo.XActLib_Resource", "IX_ResourceFilterKeyCulture");
            DropIndex("dbo.XActLib_Counter", "IX_ApplicationTennantId_Id_Key");
            DropIndex("dbo.XActLib_ApplicationSettingsAudit", new[] { "ApplicationTennantId", "Id" });
            DropIndex("dbo.XActLib_ApplicationSettings", "IX_EnvAppZoneHostTennantKey");
            DropTable("dbo.XActLib_HelpEntry_HelEntryTag");
            DropTable("dbo.XActLib_OperationPermission");
            DropTable("dbo.XActLib_Operation");
            DropTable("dbo.XActLib_ScheduledTaskMetadata");
            DropTable("dbo.XActLib_ScheduledTask");
            DropTable("dbo.XActLib_Status");
            DropTable("dbo.XActLib_PersistedFileMetadata");
            DropTable("dbo.XActLib_PersistedFile");
            DropTable("dbo.XActLib_Message");
            DropTable("dbo.XActLib_MessageTag");
            DropTable("dbo.XActLib_MessageAttachment");
            DropTable("dbo.XActLib_MessageBody");
            DropTable("dbo.XActLib_MessageAddress");
            DropTable("dbo.XActLib_MessageHeader");
            DropTable("dbo.XActLib_MessageStatus");
            DropTable("dbo.XActLib_HelpEntryTag");
            DropTable("dbo.XActLib_HelpEntry");
            DropTable("dbo.XActLib_HelpEntryCategory");
            DropTable("dbo.XActLib_Resource");
            DropTable("dbo.XActLib_Counter");
            DropTable("dbo.XActLib_DataStoreUpdateLog");
            DropTable("dbo.XActLib_SettingsRenderingInformation");
            DropTable("dbo.XActLib_ApplicationSettingsAudit");
            DropTable("dbo.XActLib_ApplicationSettings");
        }
    }
}
