﻿using XAct;

namespace App.Core.Infrastructure.Maps.CustomResolvers
{
    using System;
    using App.Core.Domain.Entities;
    using App.Core.Infrastructure.Maps.CustomResolvers;

    public  class IHasReferenceIdToIntIdAsStringCustomResolver : CustomResolverBase<IHasReferenceData, string>
    {


        protected override string ResolveCore(IHasReferenceData source)
        {
            Type t = source.GetType();

            int enumVar = ((int)source.GetType().GetProperty("Id").GetValue(source, null));

            //convert the enum to an int
            return ((int)enumVar).ToString();

        }
    }
}
