﻿using XAct;

namespace App.Core.Infrastructure.Maps.CustomResolvers
{
    using System;
    using App.Core.Domain.Entities;
    using App.Core.Infrastructure.Maps.CustomResolvers;

    public class IHasReferenceTypeToStringAsStringCustomResolver : CustomResolverBase<IHasReferenceData, string>
    {


        protected override string ResolveCore(IHasReferenceData source)
        {
            Type t = source.GetType();

            object enumVar = source.GetType().GetProperty("Id").GetValue(source, null);

            //convert the enum to a string (not an int!):
            return enumVar.GetType().Name;

        }
    }
}