﻿using XAct;

namespace App.Core.Infrastructure.Maps
{
    using System;
    using App.Core.Domain.Entities;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Maps.CustomResolvers;
    using AutoMapper;
    using XAct.ObjectMapping;

    [ObjectMap]
    public class IHasReference_ReferenceDataViewModel_Map : AutoMapperTypeMapperBase<IHasReferenceData, ReferenceDataViewModel>
    {
        public override void Initialize()
        {
            //Create a default generic map:
            //If that works...great...otherwise, we'll have to map every single Reference Data 
            //specifically, which would suck.
            IMappingExpression<IHasReferenceData, ReferenceDataViewModel> map = base.CreateMap<IHasReferenceData, ReferenceDataViewModel>();

            map.ForMember(d => d.Key, opt => opt.ResolveUsing <IHasReferenceIdToIntIdAsStringCustomResolver>());
            map.ForMember(d => d.Text, opt => opt.ResolveUsing<IHasReferenceIdToStringAsStringCustomResolver>());
        }
    }
}