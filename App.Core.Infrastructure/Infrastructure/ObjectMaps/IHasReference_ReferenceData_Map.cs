﻿using XAct;

namespace App.Core.Infrastructure.Maps
{
    using System;
    using App.Core.Domain.Entities;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Maps.CustomResolvers;
    using AutoMapper;
    using XAct.ObjectMapping;

    [ObjectMap]
    public class IHasReference_ReferenceData_Map : AutoMapperTypeMapperBase<IHasReferenceData, ReferenceDataMessage>
    {
        public override void Initialize()
        {
            //Create a default generic map:
            //If that works...great...otherwise, we'll have to map every single Reference Data 
            //specifically, which would suck.
            IMappingExpression<IHasReferenceData, ReferenceDataMessage> map = base.CreateMap<IHasReferenceData, ReferenceDataMessage>();

            map.ForMember(d => d.Id, opt => opt.ResolveUsing <IHasReferenceIdToIntIdAsStringCustomResolver>());
            map.ForMember(d => d.Order, opt => opt.MapFrom(s=>s.Order));
            //map.ForMember(d => d.Tag, opt => opt.MapFrom(s => s.Code));
            map.ForMember(d => d.ResourceFilter, opt => opt.ResolveUsing<IHasReferenceTypeToStringAsStringCustomResolver>());
            map.ForMember(d => d.Text, opt => opt.ResolveUsing<IHasReferenceIdToStringAsStringCustomResolver>());
            map.ForMember(d => d.Filter, opt => opt.Ignore());
 
            //map.ForMember(d => d.Id, opt => opt.ResolveUsing<IHasReferenceIdToStringAsStringCustomResolver>());

        }
    }



}

