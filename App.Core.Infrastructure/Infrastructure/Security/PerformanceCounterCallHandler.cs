﻿//using System.Reflection;
//using Microsoft.Practices.Unity.InterceptionExtension;
//using XAct;

//namespace App.Core.Infrastructure.Security
//{
//    using XAct.Diagnostics.Performance;

//    public class PerformanceCounterCallHandler : ICallHandler {
//        private Services.IPerformanceCounterService PerformanceCounterService
//        {
//            get { 
//                return XAct.DependencyResolver.Current.GetInstance<Services.IPerformanceCounterService>();
//            }
//        }

//        public int Order { get; set; }

//        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
//        {
//            //We need the method's FQN in orde to authenticate it:
//            MethodInfo methodInfo = (MethodInfo)input.MethodBase;

//            PerformanceCounterAttribute performanceCounterAttribute =
//                methodInfo.GetAttributeRecursively<PerformanceCounterAttribute>(true);



//            IMethodReturn methodReturn;
//            if (performanceCounterAttribute == null)
//            {
//                //This invokes the next item in the Injection pipeline, or eventually calls your method
//                methodReturn = getNext().Invoke(input, getNext);

//                return methodReturn;
//            }
//            if (!performanceCounterAttribute.Enabled)
//            {
//                //This invokes the next item in the Injection pipeline, or eventually calls your method
//                methodReturn = getNext().Invoke(input, getNext);

//                return methodReturn;
//            }

//            string performanceCounterNames = performanceCounterAttribute.Name;


//            foreach (string performanceCounterName in performanceCounterNames.Split(new char[] {',', '|', ';'}))
//            {
//                try
//                {
//                    PerformanceCounterService.Update(performanceCounterName.Trim());
//                }
//                catch
//                {

//                }
//            }
//            //This invokes the next item in the Injection pipeline, or eventually calls your method
//            methodReturn = getNext().Invoke(input, getNext);

//            return methodReturn;
//        }

//    }
//}