﻿namespace App.Core.Infrastructure.Security
{
    using System.Diagnostics;
    using App.Core.Infrastructure.Services;
    using Microsoft.Practices.Unity.InterceptionExtension;

    public class ErrorTracingCallHandler : ICallHandler
    {
        private ITracingService TracingService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<ITracingService>(); }
        }

        public int Order { get; set; }

        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {

            IMethodReturn methodReturn = getNext()(input, getNext);

            if (methodReturn.Exception == null)
            {
                return methodReturn;
            }

            try
            {
                TracingService.TraceException(
                    TraceLevel.Error,
                    methodReturn.Exception,
                    "Intercepted Exception: " + methodReturn.Exception.Message);
            }
            catch
            {
                
            }

            ////the method you intercepted caused an exception
            ////check if it is really a method
            //if (input.MethodBase.MemberType == MemberTypes.Method)
            //{
            //    MethodInfo method = (MethodInfo) input.MethodBase;
            //    if (method.ReturnType == typeof (void))
            //    {
            //        //you should only return null if the method you intercept returns void
            //        return null;
            //    }
            //    //if the method is supposed to return a value type (like int) 
            //    //returning null causes an exception
            //}
            return methodReturn;
        }
    }
}