﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Infrastructure.Factories
{
    public class ReferenceDataFactory<TReferenceData,TId> 
        where TReferenceData : XAct.IHasReferenceData<TId> 
        where TId: struct
    {

        public static TReferenceData Create(TId id)
        {
            var t = XAct.DependencyResolver.Current.GetInstance<TReferenceData>();
            t.Id = id;

            return t;
        }

        public static TReferenceData Create(TId id, string resourceFilter, string resourceKeyOrText, string description)
        {
            var result = Create(id);
            result.Text = resourceKeyOrText;

            return result;
        }

    }
}
