﻿using App.Core.Infrastructure.Models;

namespace App
{
    using App.Core.Domain.Messages;
    using App.Core.Domain.Models;

    public static class SignInInfoExtensions
    {
        public static SessionLog MapTo(this SignInInfo signOnInfo)
        {
            SessionLog result = new SessionLog();

            result.InterfaceTypeFK = signOnInfo.InterfaceTypeFK;
            result.RemoteIp = signOnInfo.RemoteIp;
            result.User = signOnInfo.UserName;

            result.SSOIdentifier = signOnInfo.SSOIdentifier;
            result.SSOType = signOnInfo.SSOProtocol;

            return result;
        }
    }
}
