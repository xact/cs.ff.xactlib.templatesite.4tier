﻿namespace App
{
    public static class HelpEntrySummaryExtensions
    {
        public static App.Core.Domain.Messages.HelpEntrySummary MapTo(this XAct.Assistance.HelpEntrySummary helpEntrySummary)
        {
            App.Core.Domain.Messages.HelpEntrySummary result = new App.Core.Domain.Messages.HelpEntrySummary();

            // Check to see if we have a result key
            if (helpEntrySummary == null || helpEntrySummary.Key == null)
            {
                return null;
            }
            result.Key = helpEntrySummary.Key;
            result.Value = helpEntrySummary.Value;
            if (helpEntrySummary.Children == null)
            {
                return result;
            }
            foreach (XAct.Assistance.HelpEntrySummary summary in helpEntrySummary.Children)
            {
                result.Children.Add(summary.MapTo());
            }
            return result;
        } 
    }
}