﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;

namespace App.Core.Domain.Implementations.Repositories
{
    public static class IncludesExtensions
    {
		public static IQueryable<T> ConditionalInclude<T, TProperty>(this IQueryable<T> source, Func<bool> condition, params Expression<Func<T, TProperty>>[] paths) where T : class
		{
			if (!condition.Invoke()) return source;
			paths.ToList().ForEach(p => source = source.Include(p));
			return source;
		}
    }
}
