using System;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using App.Core.Infrastructure.Services;

namespace App.Core.Infrastructure
{
    /// <summary>
    /// Your handler to actually tell ELMAH about the problem.
    /// 
    /// Taken from http://stackoverflow.com/questions/895901/exception-logging-for-wcf-services-using-elmah
    /// </summary>
    public class HttpErrorHandler : IErrorHandler
    {
        public bool HandleError(Exception error)
        {
            return false;
        }

		/// <summary>
		/// Types of exceptions that are related to the security processing.
		/// (They're not 'exceptional'/unexpected states
		/// </summary>
	    private Type[] _securityExceptionTypes = new[]
	    {
		    typeof (XAct.Security.NotAuthenticatedException),
		    typeof (XAct.Security.NotAuthorizedException)
	    };

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            if (error != null) // Notify ELMAH of the exception.
            {
                IAppEnvironmentService environmentService = XAct.DependencyResolver.Current.GetInstance<IAppEnvironmentService>();
                if (environmentService.HttpContext == null)
                {
                    return;
                }

				Elmah.ErrorSignal.FromCurrentContext().Raise(error);

	            ITracingService tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>();

	            var isSecurityException = _securityExceptionTypes.Contains(error.GetType());

				// For security exeptions - Just log them at Debug level, as they don't need to notify the admins
				// - Everything else - 'Error' level, to alert all trace listeners
	            var traceLevel = isSecurityException
		            ? TraceLevel.Verbose
		            : TraceLevel.Error;

	            var errorHeader = isSecurityException
		            ? "Security exeception"
		            : "Unhandled backend exception";

				tracingService.TraceException(traceLevel, error, errorHeader);
            }
        }
    }
}