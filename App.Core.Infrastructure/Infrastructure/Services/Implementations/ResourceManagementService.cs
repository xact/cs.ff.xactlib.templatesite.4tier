﻿//namespace App.Core.Infrastructure.Services.Implementations
//{
//    using System.Globalization;
//    using XAct.Services;
//    using VENDOR = XAct.Resources;

//    [DefaultBindingImplementation(typeof(IResourceManagementService))]
//    public class ResourceManagementService : ServiceBase, IResourceManagementService
//    {
//        private readonly VENDOR.IResourceService _resourceManagementService;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="ResourceManagementService" /> class.
//        /// </summary>
//        /// <param name="resourceManagementService">The resource management service.</param>
//        public ResourceManagementService(VENDOR.IResourceService resourceManagementService)
//        {
//            _resourceManagementService = resourceManagementService;
//        }


//        /// <summary>
//        /// Persists the specified <c>Resource</c>.
//        /// </summary>
//        /// <param name="cultureInfo">The culture info.</param>
//        /// <param name="filter">The filter.</param>
//        /// <param name="key">The key.</param>
//        /// <param name="value">The value.</param>
//        /// <param name="commitChangesNow">if set to <c>true</c> [commit changes now].</param>
//        public void Persist(CultureInfo cultureInfo, string filter, string key, string value,
//                            bool commitChangesNow = true)
//        {
//            _resourceManagementService.Persist(cultureInfo, filter, key, value, commitChangesNow);
//        }


//        /// <summary>
//        /// Persists the specified <c>Resource</c>.
//        /// </summary>
//        /// <param name="cultureInfoCode">The culture info code.</param>
//        /// <param name="filter">The filter.</param>
//        /// <param name="key">The key.</param>
//        /// <param name="value">The value.</param>
//        /// <param name="commitChangesNow">if set to <c>true</c> [commit changes now].</param>
//        public void Persist(string cultureInfoCode, string filter, string key, string value,
//                            bool commitChangesNow = true)
//        {
//            _resourceManagementService.Persist(cultureInfoCode, filter, key, value, commitChangesNow);
            
//        }

//        /// <summary>
//        /// Deletes the specified <c>Resource</c>
//        /// </summary>
//        /// <param name="cultureInfoCode">The culture info code.</param>
//        /// <param name="filter">The filter.</param>
//        /// <param name="key">The key.</param>
//        public void Delete(CultureInfo cultureInfo, string filter, string key)
//        {
//            _resourceManagementService.Delete(cultureInfo, filter, key);
//        }


//        /// <summary>
//        /// Deletes the specified <c>Resource</c>
//        /// </summary>
//        /// <param name="cultureInfoCode">The culture info code.</param>
//        /// <param name="filter">The filter.</param>
//        /// <param name="key">The key.</param>
//        public void Delete(string cultureInfoCode, string filter, string key)
//        {
//            _resourceManagementService.Delete(cultureInfoCode, filter, key);
//        }

//    }
//}