﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Globalization;
    using XAct.Assistance;
    using XAct.Services;

    public class HelpSevice : App.Core.Infrastructure.Services.IHelpService
    {
        private readonly XAct.Assistance.IHelpService _helpService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpSevice" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="helpService">The help service.</param>
        public HelpSevice(XAct.Assistance.IHelpService helpService)
        {
            _helpService = helpService;
        }

        /// <summary>
        /// Gets the Help Resource Set by key.
        /// </summary>
        /// <param name="key">The key.</param><param name="cultureInfo">The culture info.</param>
        /// <returns/>
        public App.Core.Domain.Messages.HelpEntrySummary GetHelpEntryByKey(string key, CultureInfo cultureInfo = null)
        {
            try
            {
                HelpEntrySummary helpEntrySummary =
                    _helpService.GetByKey(key, true, cultureInfo);

                App.Core.Domain.Messages.HelpEntrySummary result = helpEntrySummary.MapTo();

                return result;
            }
            catch
            {
                throw;
            }
        }


    }
}