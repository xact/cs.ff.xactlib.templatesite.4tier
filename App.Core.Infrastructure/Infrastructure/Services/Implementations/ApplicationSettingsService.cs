namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using App.Core.Infrastructure.Services.Configuration;
    using App.Core.Infrastructure.Services.Configuration;
    using XAct.Services;
    using VENDOR = XAct.Settings;

    /// <summary>
    /// The back tier's implementation of <see cref="IApplicationSettingsService"/>
    /// to return Tier specific application wide settings.
    /// </summary>
    public class ApplicationSettingsService : IApplicationSettingsService
    {
        private readonly VENDOR.IApplicationSettingsService _applicationSettingsService;
        private readonly IApplicationSettingsServiceConfiguration _applicationSettingsServiceConfiguration;


        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSettingsService" /> class.
        /// </summary>
        /// <param name="applicationSettingsService">The host settings service.</param>
        public ApplicationSettingsService(VENDOR.IApplicationSettingsService applicationSettingsService, IApplicationSettingsServiceConfiguration applicationSettingsServiceConfiguration)
        {
            _applicationSettingsService = applicationSettingsService;


            //Embed and initialize:
            _applicationSettingsServiceConfiguration = applicationSettingsServiceConfiguration;

            _applicationSettingsServiceConfiguration.Initialize();
        }

        public AppSettings AppSettings
        {
            get
            {
                AppSettings result = _applicationSettingsService.GetCurrentSettings<AppSettings>();

                return result;
            }
            private set
            {
                throw new NotImplementedException();
            }

        }


        public void Persist()
        {
            _applicationSettingsService.Persist(this.AppSettings);
        }

    }
}