﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Diagnostics;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services.Configuration;
    using XAct;
    using XAct.Services;

    public class MessagingService : App.Core.Infrastructure.Services.IMessagingService
    {
        private readonly XAct.Net.Messaging.IMessagingService _messagingService;

        public ISmtpServiceConfiguration Configuration
        {
            get { return _smtpServiceConfiguration; }
        }
        private readonly ISmtpServiceConfiguration _smtpServiceConfiguration;
	    private readonly ITracingService _tracingService;

	    /// <summary>
        /// Initializes a new instance of the <see cref="MessagingService" /> class.
        /// </summary>
        /// <param name="messagingService">The messaging service.</param>
        /// <param name="smtpServiceConfiguration">The SMTP service configuration.</param>
        public MessagingService(XAct.Net.Messaging.IMessagingService messagingService, 
			ISmtpServiceConfiguration smtpServiceConfiguration,
			ITracingService tracingService)
        {
            _messagingService = messagingService;

            _smtpServiceConfiguration = smtpServiceConfiguration;
	        _tracingService = tracingService;
	        _smtpServiceConfiguration.Initialize();
        }


        public void SendMessage(string from, string to, string cc, string subject, string textbody, string htmlBody)
        {
            //System.Net.Mail.MailMessage mailMessage = new MailMessage();
            //mailMessage.AlternateViews.Add(AlternateView.CreateAlternateViewFromString("blah",contentType,"text/html"));
            //mailMessage.Attachments.Add(new LinkedResource(){});

            XAct.Net.Messaging.Message message=new XAct.Net.Messaging.Message();

            message.From = new XAct.Net.Messaging.MessageAddress(from);
            message.Addresses.Add(new XAct.Net.Messaging.MessageAddress(XAct.Net.Messaging.MessageAddressType.To, to, to));

            message.Addresses.Add(new XAct.Net.Messaging.MessageAddress(XAct.Net.Messaging.MessageAddressType.ReplyTo, to, to));
	        if (!cc.IsNullOrEmpty())
	        {
				message.Addresses.Add(new XAct.Net.Messaging.MessageAddress(XAct.Net.Messaging.MessageAddressType.Cc, cc, cc));    
	        }
            
            
            message.Subject = subject;

            message.Bodies.Add(new XAct.Net.Messaging.MessageBody(XAct.Net.Messaging.MessageBodyType.Html, htmlBody));
            message.Bodies.Add(new XAct.Net.Messaging.MessageBody(XAct.Net.Messaging.MessageBodyType.Text,textbody));

           // VENDOR.MessageAttachment attachment = new VENDOR.MessageAttachment();
           // attachment.LoadFromFile("xyz.gif");

            //message.Attachments.Add(attachment);
			
            SendMessage(message);
        }

        /// <summary>
        /// Sends the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void SendMessage(XAct.Net.Messaging.Message message)
        {
            try
            {
                _messagingService.SendMessage(message);
            }
            catch (System.Exception e)
            {
	            var toAddresses = string.Join(", ", message.To);

				_tracingService.TraceException(TraceLevel.Warning, e, "Unable to send email. (To: {0}, Subject: {1})", toAddresses, message.Subject);

                throw;
            }
        }
    }
}