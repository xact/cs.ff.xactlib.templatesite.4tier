namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Collections.Generic;
    using System.Globalization;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    /// <summary>
    /// An implementation of the 
    /// <see cref="ICultureSpecificResourceService"/> contract
    /// to retrieve Resources as one or more sets
    /// or just single strings.
    /// </summary>
    public class CultureSpecificResourceService : ICultureSpecificResourceService
	{
		private readonly XAct.Resources.IResourceService _resourceService;
		private readonly XAct.IO.Transformations.IMarkdownService _markdownService;

        public CultureSpecificResourceService(XAct.Resources.IResourceService resourceService, XAct.IO.Transformations.IMarkdownService markdownService)
		{
			_resourceService = resourceService;
			_markdownService = markdownService;
		}

        /// <summary>
        /// Returns a single localised Resource string.
        /// <para>
        /// Note that the back tier service is providing a means to define
        /// the culture, as the back tier server is not the same thread
        /// as the front tier -- and therefore may not have it's UICulture
        /// set to that of the end user.
        /// </para>
        /// <para>
        /// The front tier should not call this method (it's expensive)
        /// and find alternate solutions such as using the cached results
        /// of earlier calls to <see cref="GetResourceSet" /> or
        /// <see cref="GetResourceSets" />
        /// </para>
        /// </summary>
        /// <param name="resourceSetName">The name.</param>
        /// <param name="name"></param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <returns></returns>
        public string GetResource(string resourceSetName, string name, CultureInfo cultureInfo, ResourceTransformation resourceTransformation = ResourceTransformation.None)
		{
            string result = _resourceService.GetString(resourceSetName, name, cultureInfo);

                switch (resourceTransformation)
                {
                    case ResourceTransformation.Markdown:
                        return _markdownService.Transform(result);
                    case ResourceTransformation.None:
                    default:
                        return result;
                }
        }
        /// <summary>
        /// Get a Set (usually a View's) set of localised Resource strings.
        /// </summary>
        /// <param name="resourceSetName">Will be the name of the View you want all the scrings for.</param>
        /// <param name="cultureInfo"></param>
        /// <param name="resourceTransformation"></param>
        /// <returns></returns>
        /// <remarks>
        /// Note that the back tier service is providing a means to define
        /// the culture, as the back tier server is not the same thread
        /// as the front tier -- and therefore may not have it's UICulture
        /// set to that of the end user.
        /// </remarks>
        public Dictionary<string, string> GetResourceSet(string resourceSetName, CultureInfo cultureInfo, ResourceTransformation resourceTransformation = ResourceTransformation.None)
        {

            Dictionary<string, string> results = _resourceService.GetAllStrings(resourceSetName, cultureInfo);



            switch (resourceTransformation)
			{
				case ResourceTransformation.Markdown:
                    Dictionary<string,string> results2 = new Dictionary<string,string>();

					foreach (KeyValuePair<string, string> pair in results)
					{
						results2[pair.Key] = _markdownService.Transform(results[pair.Key]);
					}
					return results2;
				case ResourceTransformation.None:
				default:
					return results;
			}
        }



        /// <summary>
        /// Returns all resource strings
        /// </summary>
        /// <param name="cultureInfo">Culture Info</param>
        /// <param name="resourceTransformation">Resource Transformation</param>
        /// <param name="resourceSetNames">List of filters to retrieve resources for</param>
        /// <returns></returns>
        public Dictionary<string, Dictionary<string, string>> GetResourceSets(CultureInfo cultureInfo, ResourceTransformation resourceTransformation = ResourceTransformation.None, params string[] resourceSetNames)
        {
            try
            {
                Dictionary<string, Dictionary<string, string>> results = _resourceService.GetAllStrings(cultureInfo, resourceSetNames);
                return results;
            }
            catch
            {
                throw;
            }
        }
    }
}