﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Diagnostics;
    using System;
    using App.Core.Infrastructure.Services;
    using XAct.Scheduling;
    using XAct.Services;

    public class SchedulingManagementService : Services.ISchedulingManagementService
    {
        private readonly XAct.Scheduling.ISchedulingManagementService _schedulingManagementService;
	    private readonly ITracingService _tracingService;

	    public bool Initialized
        {
            get { return _schedulingManagementService.Initialized; }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="SchedulingManagementService"/> class.
        /// </summary>
        /// <param name="schedulingManagementService">The scheduling management service.</param>
        public SchedulingManagementService(
			XAct.Scheduling.ISchedulingManagementService schedulingManagementService,
			ITracingService tracingService
			)
        {
	        _schedulingManagementService = schedulingManagementService;
	        _tracingService = tracingService;
        }

	    /// <summary>
        /// Gets the singleton scheduler that this service manages.
        /// </summary>
        /// <value>
        /// The scheduler.
        /// </value>
        public ISchedulerController Scheduler
        {
            get { return _schedulingManagementService.Scheduler; }
        }

        /// <summary>
        /// Gets the singleton configuration for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public ISchedulingManagementServiceConfiguration Configuration
        {
            get { return _schedulingManagementService.Configuration; }
        }

        /// <summary>
        /// Initializes the specified application tennant identifier.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        public void Initialize(Guid applicationTennantId)
        {

			try
			{
				_schedulingManagementService.Initialize(applicationTennantId);
			}
			catch (Exception ex)
			{
				_tracingService.TraceException(TraceLevel.Error, ex, "Scheduled tasks - Unable to initialise task");
				throw;
			}
        }

        /// <summary>
        /// Gets the schedules within the group, or all if no group specified.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="group">The group.</param>
        /// <returns></returns>
        public ScheduledTask[] GetSchedules(Guid applicationTennantId, string group = null)
        {
            return _schedulingManagementService.GetSchedules(applicationTennantId,group);
        }

        /// <summary>
        /// Gets the specified schedule.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ScheduledTask GetSchedule(Guid applicationTennantId, Guid id)
        {
            return _schedulingManagementService.GetSchedule(applicationTennantId, id);
        }

        /// <summary>
        /// Gets the specified schedule.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="name">The task's required name.</param>
        /// <param name="group">The task's optional grouping name.</param>
        /// <returns></returns>
        public ScheduledTask GetSchedule(Guid applicationTennantId, string name, string group = null)
        {
            return _schedulingManagementService.GetSchedule(applicationTennantId, name, group);
        }

        /// <summary>
        /// Adds the scheduled task, and starts it.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="scheduledTask">The scheduled task.</param>
        public void AddScheduledTask(Guid applicationTennantId, ScheduledTask scheduledTask)
        {
            _schedulingManagementService.AddScheduledTask(applicationTennantId, scheduledTask);
        }

        /// <summary>
        /// Deletes the scheduled task, and removes it from the scheduler.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="id">The identifier.</param>
        public void DeleteScheduledTask(Guid applicationTennantId, Guid id)
        {
            _schedulingManagementService.DeleteScheduledTask(applicationTennantId, id);
        }

        /// <summary>
        /// Deletes the scheduled task, and removes it from the scheduler.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="name">The task's required name.</param>
        /// <param name="group">The task's optional grouping name.</param>
        public void DeleteScheduledTask(Guid applicationTennantId, string name, string group = null)
        {
            _schedulingManagementService.DeleteScheduledTask(applicationTennantId, name, group);
        }
    }
}