﻿// ReSharper disable CheckNamespace
namespace App.Core.Infrastructure.Services.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="Services.ICounterService"/> contract
    /// to manage counters for an arbitrary entity.
    /// </summary>
    public class CounterService : Services.ICounterService
    {
        private readonly XAct.Metrics.ICounterService _counterService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CounterService"/> class.
        /// </summary>
        /// <param name="counterService">The counter service.</param>
        public CounterService(XAct.Metrics.ICounterService counterService)
        {
            _counterService = counterService;
        }

        /// <summary>
        /// Gets the counter's value, or returns 0 if no counter found.
        /// </summary>
        /// <param name="key">The key for a counter specific to the <paramref name="targetId" />.</param>
        /// <param name="targetId">The target identifier (often a User's Id).</param>
        /// <returns></returns>
        public int GetValue(string key, string targetId = null)
        {
            return _counterService.GetValue(key,targetId);
        }

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="key">The key for a counter specific to the <paramref name="targetId" />.</param>
        /// <param name="value">The value.</param>
        /// <param name="targetId">The target identifier.</param>
        public void SetValue(string key, int value, string targetId = null)
        {
            _counterService.SetValue(key,value,targetId);
        }

        /// <summary>
        /// Removes the specified counter.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="targetId">The target identifier.</param>
        public void Remove(string key, string targetId = null)
        {
            _counterService.Remove(key,targetId);
        }
    }
}