﻿
namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using System.Configuration;
    using XAct.Library.Settings;
    using XAct.Services;

    /// <summary>
	/// Configurable settings for the application.
	/// Sources values from web.config, and allows per-machine value overriding via NConfig (in dev only)
	/// </summary>
    public class HostSettingsService :App.Core.Infrastructure.Services.IHostSettingsService
    {
        private readonly XAct.Settings.IHostSettingsService _hostSettingsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HostSettingsService"/> class.
        /// </summary>
        /// <param name="hostSettingsService">The host settings service.</param>
        public HostSettingsService(XAct.Settings.IHostSettingsService hostSettingsService)
        {
            _hostSettingsService = hostSettingsService;
        }

        public string EnvironmentIdentifier
        {
            get { 
                string result = Get<string>("EnvironmentIdentifier", string.Empty);
                if (result == string.Empty)
                {
                    result = null;
                }

                return result;
            }
        }


        public SeedingType SeedingType
        {
            get
            {
                SeedingType seedingType;
                if (!Enum.TryParse(Get<string>("SeedingType"), true, out seedingType))
                {
                    seedingType = SeedingType.ResetImmutableReferenceData;
                }

                return seedingType;
            }
        }


        public bool DevFlagSeedEveryStartup
        {
            get
            {
                bool result = this.Get<bool>("DevFlagSeedEveryStartup", false);
                return result;
            }
        }


        public bool DisabledSchedulingService
        {
            get
            {
                bool result = this.Get<bool>("DisabledSchedulingService", false);
                return result;
            }
        }



        /// <summary>
        /// Gets the Typed Application setting matching the given key.
        /// 
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam><param name="key">The key.</param><param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns/>
        public TValue Get<TValue>(string key, bool throwExceptionOnConversionException = true)
        {
            return _hostSettingsService.Get<TValue>(key, throwExceptionOnConversionException);
        }

        /// <summary>
        /// Gets the Typed Application setting matching the given key.
        /// 
        /// <para>
        /// If the result is null, <paramref name="defaultValue"/>.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam><param name="key">The key.</param><param name="defaultValue">The default value if no value was found.</param><param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns/>
        public TValue Get<TValue>(string key, TValue defaultValue, bool throwExceptionOnConversionException = true)
        {
            return _hostSettingsService.Get<TValue>(key, defaultValue, throwExceptionOnConversionException);
            
        }

    }
}
