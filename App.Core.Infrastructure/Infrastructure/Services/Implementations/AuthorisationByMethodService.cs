﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using System.Reflection;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Security;
    using XAct.Security;
    using XAct.Security.Authorization;
    using XAct.Services;

    /// <summary>
    /// Implementation of 
    /// <see cref="IAuthorisationByMethodService"/>
    /// <para>
    /// Service invoked by <see cref="AuthenticationCallHandler"/>
    /// </para>
    /// </summary>
    public class AuthorisationByMethodService : IAuthorisationByMethodService
    {
        private readonly IAppEnvironmentService _environmentService;
        private readonly IPrincipalServiceBase _principalService;
        private readonly IOperationBasedAuthorizationService _operationBasedAuthorizationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorisationByMethodService" /> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="operationBasedAuthorizationService">The operation based authorization service.</param>
        public AuthorisationByMethodService(IAppEnvironmentService environmentService,
            IPrincipalServiceBase principalService,
            XAct.Security.Authorization.IOperationBasedAuthorizationService operationBasedAuthorizationService
            )
        {
            _environmentService = environmentService;
            _principalService = principalService;

            _operationBasedAuthorizationService = operationBasedAuthorizationService;
        }

        public bool AuthenticatePrincipalByMethod(MethodInfo methodInfo, out AuthorizationResult authorisationResult)
        {
            //From Method, get Roles Required to access the Role.
            //Then can query whether Principal has any of those roles.

            if (_principalService.Principal == null)
            {
                throw new ArgumentException(
                    "Cannot render judgement as to whether Principal is permitted to perform Operation if thread Principal is not set.");
            }






#pragma warning disable 168
            string currentIdentityName = _principalService.Principal.Identity.Name;
#pragma warning restore 168

            bool result = _operationBasedAuthorizationService.IsAuthorized(methodInfo,out authorisationResult);

            return result;

        }
    }
}