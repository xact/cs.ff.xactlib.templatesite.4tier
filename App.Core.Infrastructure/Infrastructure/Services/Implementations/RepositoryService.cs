namespace App.Core.Infrastructure.Services.Implementations
{
    //using App.Core.Infrastructure.Data;
    using System.Data.Entity.Core.Objects;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using XAct.Diagnostics.Performance.Services;
    using XAct.Services;

    public class RepositoryService : XAct.Domain.Repositories.Services.EntityDbContextGenericRepositoryService, App.Core.Infrastructure.Services.IRepositoryService
    {
        public RepositoryService(XAct.Diagnostics.ITracingService tracingService, XAct.IDistributedIdService distributedIdService, XAct.Diagnostics.Performance.IPerformanceCounterService performanceCounterService, IContextStatePerformanceCounterService contextStatePerformanceCounterService, XAct.Domain.Repositories.IUnitOfWorkService unitOfWorkManagementService)
            : base(tracingService, distributedIdService,performanceCounterService,contextStatePerformanceCounterService,
            unitOfWorkManagementService)
        {            
        }

        public DbContext DbContext
        {
	        get { return base.GetDbContext(); }
        }

	    public ObjectContext ObjectContext
	    {
		    get { return ((IObjectContextAdapter) DbContext).ObjectContext; }
	    }

	    public bool IsAttached<TEntity>(Func<DbEntityEntry<TEntity>, bool> predicate)
            where TEntity : class
        {
            bool result = DbContext.ChangeTracker
                                  .Entries<TEntity>()
                                  .Any(predicate);
            return result;
        }

        public void Detach<TEntity>(TEntity entity) where TEntity : class
        {
            GetEntry(entity).State = EntityState.Detached;
        }

        public DbEntityEntry<TEntity> GetEntry<TEntity>(TEntity entity) where TEntity : class
        {
            DbEntityEntry<TEntity> result = DbContext.Entry(entity);
            return result;
        }

		public IList<DbEntityEntry<TEntity>> GetEntry<TEntity>(Func<DbEntityEntry<TEntity>, bool> predicate)
            where TEntity : class
        {
            List<DbEntityEntry<TEntity>> result = DbContext.ChangeTracker
                                  .Entries<TEntity>()
                                  .Where(predicate)
                                  .ToList();
            return result;
        }

	    public bool IsEntityDirty(object entity)
	    {
		    var entry = GetEntryNonGeneric(entity);
		    return entry.IsDirty();
	    }

	    public DbEntityEntry GetEntryNonGeneric(object entity)
        {
            DbEntityEntry result = DbContext.Entry(entity);
            return result;
        }
    }
}