﻿# Corp.App...Infrastructure.Svcs Implementations #

----

* Do not create a new namespace for this folder (it's the same namespace as the folder below).
   * See: [http://bit.ly/Rqfdso](http://bit.ly/Rqfdso)
* Put here the actual implementations of the interfaces defined in the folder below.
 
 Example:

   namespace Corp.App... {  
     public MyService : MyService{
	    
		 //Inject into the only contructor other services 
		 //this service depends on (ie, implement the DependencyInjection pattern):
		 public MyService(ITracingService tracingService){
	
		   //persist the service as  
		   _tracingService = tracingService;
     }

	 ... implement contract of service as usual...

   }
