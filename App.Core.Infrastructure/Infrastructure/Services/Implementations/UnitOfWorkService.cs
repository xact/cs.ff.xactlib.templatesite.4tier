﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Linq;
    using System.Data.Entity;
    using XAct.Domain.Repositories;
    using XAct.Services;

    /// <summary>
	/// An implementation of the <see cref="Services.IUnitOfWorkService"/>
	/// to commit changes to remote datastores.
	/// </summary>
	public class UnitOfWorkService : Services.IUnitOfWorkService
	{
		private readonly XAct.Domain.Repositories.IUnitOfWorkService _unitOfWorkService;

		/// <summary>
		/// Initializes a new instance of the <see cref="UnitOfWorkService" /> class.
		/// </summary>
		/// <param name="unitOfWorkService">The unit of work management service.</param>
		/// <param name="dbContextCommitExtension">The database context commit extension.</param>
		public UnitOfWorkService(
				XAct.Domain.Repositories.IUnitOfWorkService unitOfWorkService)
		{
			_unitOfWorkService = unitOfWorkService;
		}

		/// <summary>
		/// Commits the current UnitOfWork.
		/// </summary>
        public void Commit(string contextKey = "Default")
		{
		    _unitOfWorkService.GetCurrent(contextKey).Commit(CommitType.Default);
		}

        public void RollBack(string contextKey = "Default")
		{
			var context = GetDbContext(contextKey);

            var changedEntries = context.ChangeTracker.Entries().Where(x => x.State != EntityState.Unchanged).ToList();

			foreach (var entry in changedEntries.Where(x => x.State == EntityState.Modified))
			{
				entry.CurrentValues.SetValues(entry.OriginalValues);
				entry.State = EntityState.Unchanged;
			}

			foreach (var entry in changedEntries.Where(x => x.State == EntityState.Added))
			{
				entry.State = EntityState.Detached;
			}

			foreach (var entry in changedEntries.Where(x => x.State == EntityState.Deleted))
			{
				entry.State = EntityState.Unchanged;
			}
		}

        public void SetCurrent(IUnitOfWork unitOfWork, string contextKey = "Default")
        {
         _unitOfWorkService.SetCurrent(unitOfWork,contextKey);   
        }

        public IUnitOfWork GetCurrent(string contextKey = "Default")
        {
            return _unitOfWorkService.GetCurrent(contextKey);
        }


		public DbContext GetDbContext(string contextKey = "Default")
		{
		    XAct.Domain.Repositories.IUnitOfWork unitOfWork = _unitOfWorkService.GetCurrent(contextKey);
		    DbContext dbContext = unitOfWork.GetInnerItem<DbContext>();
		    return dbContext;

            //_unitOfWorkService.SetCurrent()
            //        ()=>new XAct.Domain.Repositories.EntityDbContext(value));
		}
	}
}