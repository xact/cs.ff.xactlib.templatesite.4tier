﻿using XAct.Data.EF.CodeFirst;

namespace App.Core.Infrastructure.Integration.Steps
{
    using System;
    using App.Core.Infrastructure.Data;
    using App.Core.Infrastructure.Data.DbContexts;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Services;


    [DefaultBindingImplementation(typeof(IAppDbSeederService),BindingLifetimeType.Undefined,Priority.Low)]
    public class AppDbSeederService : IAppDbSeederService
    {
        private readonly IHostSettingsService _hostSettingsService;
        private readonly string _typeName;


        public AppDbSeederService(IHostSettingsService hostSettingsService)
        {
            _hostSettingsService = hostSettingsService;
            _typeName = this.GetType().Name;
        }

        public void Execute()
        {
            bool seedNow = _hostSettingsService.DevFlagSeedEveryStartup;

            if (!seedNow)
            {
                return;
            }

            using (AppDbContext dbContext = new AppDbContext())
            {
                dbContext.AutoSeed<IDbContextSeeder>(searchDomain:false,assemblies:null);
                
                //.AutoSee.AutoSeed<
                //new SeedInvoker().Seed(_typeName, dbContext);

                dbContext.SaveChanges();

            }
        }
    }
}