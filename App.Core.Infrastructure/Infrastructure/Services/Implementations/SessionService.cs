namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using System.Diagnostics;
    using App.Core.Constants;
    using App.Core.Domain.Extensions;
    using App.Core.Domain.Messages;
    using App.Core.Domain.Models;
    using App.Core.Domain.Security;
    using App.Core.Infrastructure.Data.Repositories;
    using XAct;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// Provides functionality for user authentication using OAuth ESAA facade.
    /// </summary>
    public class SessionService : ISessionService
    {
        private readonly ISessionRepositoryService _sessionRepositoryService;
        private readonly ICachingService _cachingService;
        private readonly ITracingService _tracingService;
        private readonly IApplicationSettingsService _settingsService;
        //private readonly IResponseMessageService _responseMessageService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionService" /> class.
        /// </summary>
        /// <param name="sessionRepositoryService">The identity repository service.</param>
        /// <param name="cachingService">The caching service.</param>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="oauthService">The automatic authentication service agent.</param>
        /// <param name="settingsService">The settings service.</param>
        public SessionService(
            ISessionRepositoryService sessionRepositoryService,
            ICachingService cachingService,
            ITracingService tracingService,
            IApplicationSettingsService settingsService
            //IResponseMessageService responseMessageService
            )
        {
            this._sessionRepositoryService = sessionRepositoryService;
            this._cachingService = cachingService;
            this._tracingService = tracingService;
            _settingsService = settingsService;
            //_responseMessageService = responseMessageService;
        }


        /// <summary>
        /// A stub method to ping, to determine whether integration
        /// across infrastructure was successful.
        /// </summary>
        /// <returns></returns>
        public string Ping()
        {
            return DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }

















        /// <summary>
        /// Instantiates the Session entity and identifies with an unique identifier (App Session Token).
        /// The session entity and passed session log entity are then persisted in the database, Session entity is cached.
        /// </summary>
        /// <param name="identityBase64">The identity base64.</param>
        /// <param name="signInInfo">The session log.</param>
        /// <returns>
        /// App Session Token
        /// Errors:
        /// -...
        /// </returns>
        /// <exception cref="System.ArgumentNullException">sessionLog
        /// or
        /// identityBase64</exception>
        /// <exception cref="System.ArgumentException">Invalid identity string</exception>
        public Response<SessionResponse> CreateSession(string identityBase64, SignInInfo signInInfo)
        {
            this._tracingService.DebugTrace(TraceLevel.Verbose, "Create Session called");

            if (signInInfo == null) throw new ArgumentNullException("signInInfo");
            if (identityBase64 == null) throw new ArgumentNullException("identityBase64");

            if (!AppIdentity.ValidateSerializedIdentity(identityBase64))
            {
                //validate the identity string
                throw new ArgumentException("Invalid identity string");
            }

            Session session = new Session { SerializedIdentity = identityBase64 }; //create a session object

            this._sessionRepositoryService.PersistSession(session, signInInfo); //persist session and SessionLog
            this._cachingService.Set(session.CreateCacheIdentifier(), session, _settingsService.AppSettings.SessionTimeOut); //cache the session



            Response<SessionResponse> response =
                new Response<SessionResponse>
                {
                    Data = new SessionResponse { Session = session }
                };

            response.AddMessage(MessageCodes.SUCCESS);

            return response;


        }







        /// <summary>
        /// Given an existing valid (non-default) <see cref="Guid"/>,
        /// updates the associated <see cref="Session"/>'s LastAccessedDateTime
        /// and optionally (depending on <paramref name="returnSession"/>)
        /// returns the updated <see cref="Session"/>.
        /// <para>
        /// Returns a MessageCodes.SECURITY_TIMEOUT Message if timed out.
        /// </para>
        /// </summary>
        /// <param name="sessionToken"></param>
        /// <param name="returnSession"></param>
        /// <returns></returns>
        public Response<SessionResponse> UpdateSessionLastAccessedDateTime(Guid sessionToken, bool returnSession = false)
        {
            if (sessionToken == Guid.Empty)
            {
                throw new ArgumentNullException("sessionToken");
            }

            //Update Session's LastAccessedDateTime, but only if not expired.
            bool sessionExpired;
            bool sessionTooLong;

            bool updated = this._sessionRepositoryService.UpdateSessionLastAccessedDateTime(sessionToken, out sessionExpired, out sessionTooLong);


            Response<SessionResponse> response;

            if (!updated)
            {
                //if false, it's due to 
                //* Session did not exist...should have been found earlier.
                //* Session expired.
                response = new Response<SessionResponse>();

                if (sessionTooLong)
                {
                    response.AddMessage(MessageCodes.SessionTooLong);
                }

                if (sessionExpired)
                {
                    response.AddMessage(MessageCodes.SessionTimout);
                }

                return response;
            }

            //If updated, not expired, so depending on argument received,
            //go back and optionally get the Session.
            Session data = returnSession
                               ? (returnSession ? this._sessionRepositoryService.GetSession(sessionToken) : null)
                               : null;

            //Package it up:
            response
                = new Response<SessionResponse>
                    {
                        Data = new SessionResponse { Session = data },
                    };

            response.AddMessage(MessageCodes.SUCCESS);
            //_responseMessageService.AppendMessage(response, Constants.Messages.MessageCode.RETURN_SUCCESS);

            return response;
        }

        public void DeleteSession(Guid sessionToken)
        {
            this._cachingService.Remove(sessionToken.CreateCacheIdentitfier());
            this._sessionRepositoryService.DeleteSession(sessionToken);
        }

        public bool DoesSessionExist(Guid sessionToken)
        {
            return this._sessionRepositoryService.DoesSessionExist(sessionToken);
        }

        /// <summary>
        /// Tries to get the session from cache. 
        /// If its not in the cache, get's it from the database.
        /// <para>
        /// It does not return null (it will raise Exception if Session not found in Db).
        /// </para>
        /// <para>
        /// It does not update the <c>LastAccessDateTime</c> (that should have been 
        /// done via an earlier invocation of <see cref="UpdateSessionLastAccessedDateTime"/>).
        /// </para>
        /// <para>
        /// Note that the <see cref="Session"/> object's <c>LastAccessedDateTime</c> is not guaranteed to still be valid.
        /// </para>
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <returns></returns>
        public Session GetSession(Guid sessionToken)
        {
            this._tracingService.DebugTrace(TraceLevel.Verbose, "GetSession called");

            try
            {
                Session session;
                //Get Session from cache, and if not found, 
                //load cache from Repository,


                //An exception is raised by GetSession method if Record not found.

                this._cachingService.TryGet(sessionToken.CreateCacheIdentitfier(), out session,
                                            () =>
                                            {
                                                return this._sessionRepositoryService.GetSession(sessionToken, true);
                                            },
                                            _settingsService.AppSettings.SessionTimeOut);
                // theoretically the session can be expired just now
                // the primary purpose of this method is to get the session quickly from the cache for subsequent calls from front tier (during one request to front tier)
                // it does NOT update session's last access time
                // if the session is not in cache it means this particular back tier has not 
                //been used yet with this session due to load balancing with multiple back tiers

                //Return definately a session -- even if maybe stale:

                if (session == null)
                {
                    //We have a problem:
                    //int whatsgoingoon = 3/1;
                }

                return session;
            }
            catch (Exception ex)
            {
                this._tracingService.TraceException(
                    TraceLevel.Error, ex,
                                                    "Unable to get session info for ST:{0}".FormatStringInvariantCulture
                                                        (sessionToken));
                throw;
            }
        }

    }
}