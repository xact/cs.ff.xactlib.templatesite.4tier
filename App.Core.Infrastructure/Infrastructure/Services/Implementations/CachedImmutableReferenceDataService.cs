namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using System.Linq;
    using System.Reflection;
    using App.Core.Domain.Messages;
    using App.Core.Domain.Models;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure._DEBUG.LIB;
    using XAct;
    using XAct.Services;

    /// <summary>
    /// Contract for a service to cache -- in a non-UI specific manner 
    /// (eg: a list of PhoneNumberType objects) --
    /// Reference data used by the various AppHosts.
    /// <para>
    /// Each AppHost in turn caches the data in a UI framework specific
    /// manner (eg: a list of SelectItem, or formatted html, representing
    /// the list of PhoneNumberType).
    /// </para>
    /// <para>
    /// Each service will in turn require access to ICachingService
    /// </para>
    /// </summary>
    /// <internal>
    /// Note how we are Binding Application level services it to *both* 
    /// the Application Service *and* ApplicationFacade Service.
    /// <internal>
    /// Invoked by Implementation of <see cref="ICachedImmutableReferenceDataEntityService"/>
    /// </internal>
    /// </internal>
    public class CachedImmutableReferenceDataService : ICachedImmutableReferenceDataService
    {

        private readonly ICachingService _cachingService;
        private readonly IRepositoryService _repositoryService;


        private TimeSpan ReferenceCacheTimeSpan
        {
            get
            {
                return TimeSpan.FromMinutes(20);
                //return _applicationSettingsService.Settings.ReferenceCacheTimeSpan;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedImmutableReferenceDataService" /> class.
        /// </summary>
        /// <param name="cachingService">The caching service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public CachedImmutableReferenceDataService
            (
            ICachingService cachingService,
            IRepositoryService repositoryService
            )
        {
            _cachingService = cachingService;
            _repositoryService = repositoryService;
        }


        /// <summary>
        /// Gets a collection of Entity
        /// sub-classes of <see cref="App.Core.Domain.Models.ReferenceDataBase{TId}" />
        /// </summary>
        /// <typeparam name="TReferenceData"></typeparam>
        /// <returns></returns>
        public TReferenceData[] GetReferenceData<TReferenceData>()
            where TReferenceData : class, IHasReferenceData
        {
            return this.GetReferenceTypes<TReferenceData>();
        }


        /// <summary>
        /// Gets a collection of Entity
        /// sub-classes of <see cref="App.Core.Domain.Models.ReferenceDataBase{TId}" />
        /// </summary>
        /// <typeparam name="TReferenceData">The type of the reference data.</typeparam>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <returns></returns>
        public TReferenceData[] GetReferenceData<TReferenceData, TEnum>()
            where TReferenceData : ReferenceDataBase<TEnum>
            where TEnum :struct
        {
            return this.GetReferenceTypes<TReferenceData>();
        }







        private ReferenceDataMessage[] GetReferenceTypes(Type type, string key = null)
        {
            MethodInfo method = this.GetType().GetMethod("GetReferenceTypes",BindingFlags.Instance|BindingFlags.NonPublic);

            MethodInfo generic = method.MakeGenericMethod(type);
            
            object tmp = generic.Invoke(this, null);
            
            ReferenceDataMessage[] result = tmp as ReferenceDataMessage[];

            return result;
        }

        private T[] GetReferenceTypes<T>(string key = null)
            where T : class, IHasReferenceData
            //where T : ReferenceDataBase<TEnum>

        {
            T[] data;

            try
            {
            // Example of how to use RepositoryService:
            //              var departmentsQuery =
            //              _exampleRepository
            //              .GetByFilter(
            //              contact => contact.Age >= 18
            //              paging: new PagedQuerySpecification(20,0),
            //              orderBy: q => q.OrderBy(d => d.Name),
            //              new IncludeSpecification("Addresses"));


            if (key.IsNullOrEmpty())
            {
                key = typeof (T).Name;
            }

            this._cachingService.TryGet(
                "RD:" + key,
                //"Reference_InterfaceType"
                out data,
                () => this._repositoryService.GetByFilter<T>(
                    r => r.Enabled,
                    null,
                    null, 
                    q => q.OrderBy(x2 => x2.Order)
                    ).ToArray(),
                   this.ReferenceCacheTimeSpan,
                true);


            }
            catch (System.Exception e)
            {
                if (System.ServiceModel.OperationContext.Current == null)
                {
                    throw;
                }
                throw SoapExceptionFactory.CreateFaultException(true, e);
            }

            return data;
        }

    }
}