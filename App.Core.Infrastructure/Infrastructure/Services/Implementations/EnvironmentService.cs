﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using App.Core.Infrastructure.Services.Implementations;
    using XAct;
    using XAct.Services;

    [DefaultBindingImplementation(typeof(IEnvironmentService),BindingLifetimeType.Undefined,Priority.Normal)]
    public class EnvironmentService : AppEnvironmentService, IEnvironmentService 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvironmentService"/> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        public EnvironmentService(XAct.Environment.IEnvironmentService environmentService)
            : base(environmentService)
        {

        }
    }
}