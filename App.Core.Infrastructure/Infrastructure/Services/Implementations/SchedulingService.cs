﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using XAct.Scheduling;
    using XAct.Services;

    public class SchedulingService : Services.ISchedulingService
    {
        private readonly XAct.Scheduling.ISchedulingService _schedulingService;

        public SchedulingService(XAct.Scheduling.ISchedulingService schedulingService)
        {
            _schedulingService = schedulingService;
        }


        public ISchedulingServiceConfiguration Configuration { get { return _schedulingService.Configuration; } }

        public ScheduledTask[] GetSchedules(string group = null)
        {
            return _schedulingService.GetSchedules(group);
        }

        public ScheduledTask GetSchedule(Guid id)
        {
            return _schedulingService.GetSchedule(id);

        }

        public ScheduledTask GetSchedule(string name, string group = null)
        {
            return _schedulingService.GetSchedule(name,group);
        }

        public void AddScheduledTask(ScheduledTask scheduledTask)
        {
            _schedulingService.AddScheduledTask(scheduledTask);
        }

        public void DeleteScheduledTask(Guid id)
        {
            _schedulingService.DeleteScheduledTask(id);
        }

        public void DeleteScheduledTask(string name, string group = null)
        {
            _schedulingService.DeleteScheduledTask(name, group);
        }

    }
}