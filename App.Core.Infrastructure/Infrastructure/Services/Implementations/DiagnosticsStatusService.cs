﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using App.Core.Infrastructure.Services;
    using XAct.Diagnostics.Services.Implementations;

    public class DiagnosticsStatusService : IDiagnosticsStatusService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly XAct.Diagnostics.Status.IStatusService _statusService;
        private readonly IDiagnosticsStatusServiceConfiguration _statusServiceConfiguration;


        /// <summary>
        /// Initializes a new instance of the <see cref="DiagnosticsStatusService" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="statusService">The status service.</param>
        /// <param name="statusServiceConfiguration">The status service configuration.</param>
        public DiagnosticsStatusService(IDateTimeService dateTimeService, XAct.Diagnostics.Status.IStatusService statusService, IDiagnosticsStatusServiceConfiguration statusServiceConfiguration)
        {
            _dateTimeService = dateTimeService;
            _statusService = statusService;

            _statusServiceConfiguration = statusServiceConfiguration;
        }

        /// <summary>
        /// A stub method to ping, to determine whether integration
        /// across infrastructure was successful.
        /// </summary>
        /// <returns></returns>
        public string Ping()
        {
            return _dateTimeService.NowUTC.ToString();
        }

        /// <summary>
        /// Gets the specified names.
        /// 
        /// </summary>
        /// <param name="names">The names.</param><param name="startDateTimeUtc">The start date time UTC.</param><param name="endDateTimeUtc">The end date time UTC.</param>
        /// <returns/>
        public StatusResponse[] Get(string[] names, DateTime? startDateTimeUtc = null, DateTime? endDateTimeUtc = null)
        {
            return _statusService.Get(names, startDateTimeUtc, endDateTimeUtc);
        }

        /// <summary>
        /// Gets a status summary of conditions on the server.
        /// 
        /// </summary>
        /// <param name="name">The name.</param><param name="arguments">Optional arguments that can be dispatched to the controllers.</param><param name="startDateTimeUtc">The start date time UTC.</param><param name="endDateTimeUtc">The end date time UTC.</param>
        /// <returns/>
        public StatusResponse Get(string name, object arguments = null, DateTime? startDateTimeUtc = null,
                                  DateTime? endDateTimeUtc = null)
        {
            return _statusService.Get(name, arguments, startDateTimeUtc, endDateTimeUtc);
        }

    }
}