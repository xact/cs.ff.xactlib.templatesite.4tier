﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using XAct.Net.Messaging;
    using XAct.Services;

    public class PersistedFileService : App.Core.Infrastructure.Services.IPersistedFileService
    {
        private readonly XAct.Net.Post.IPersistedFileService _persistedFileService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PersistedFileService"/> class.
        /// </summary>
        /// <param name="persistedFileService">The persisted file service.</param>
        public PersistedFileService(XAct.Net.Post.IPersistedFileService persistedFileService)
        {
            _persistedFileService = persistedFileService;
        }

        /// <summary>
        /// Persists (saves or Updates) the <see cref="T:XAct.Net.Messaging.PersistedFile" /> object.
        /// <para>
        /// <see cref="PersistedFile"/> uses a <see cref="Guid"/>
        /// as it's identity, but understand that it is not Generated 
        /// by the Db. It is set by the underlying service (using IDistributedIdentifierService.NewGuid())
        /// upon committing.
        /// </para>
        /// </summary>
        /// <param name="persistedFile">The persisted file.</param>
        public void Persist(PersistedFile persistedFile)
        {
            _persistedFileService.Persist(persistedFile);
        }

        /// <summary>
        /// Retrieves the <see cref="T:XAct.Net.Messaging.PersistedFile" />
        /// </summary>
        /// <param name="id">The owner identifier.</param>
        /// <param name="includeMetadata">if set to <c>true</c>, search for any metadata to include.</param>
        /// <param name="applicationTennantId">The organisation identifier.</param>
        /// <returns></returns>
        public PersistedFile Retrieve(Guid id, bool includeMetadata = false, Guid? applicationTennantId=null)
        {
            return _persistedFileService.Retrieve(id, includeMetadata, applicationTennantId);
        }
    }
}