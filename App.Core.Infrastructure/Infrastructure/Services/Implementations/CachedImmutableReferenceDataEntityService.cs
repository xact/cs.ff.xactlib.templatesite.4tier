﻿// ReSharper disable CheckNamespace

namespace App.Core.Infrastructure.Services.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Linq;
    using App.Core.Domain.Entities;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Services;

    /// <summary>
    /// Implemenation of the <see cref="ICachedImmutableReferenceDataEntityService"/>
    /// contract for a service to get (from cache)
    /// the Reference Entity associated to the given Enum value.
    /// <para>
    /// This provides a means to get to the updatable 
    /// Attributes / Extended properties
    /// (such as Order, Enabled, Internal Code, available on the Entity
    /// only, as Enums can't carry that much information).
    /// </para>
    /// </summary>
    /// <internal>
    /// This service is not dealing with one record a a time:
    /// it is retrieving sets of immutable data from the <see cref="ICachedImmutableReferenceDataService"/>
    /// service, which in turn is using <see cref="ICachingService"/>
    /// to cache the results retrieved from <see cref="IRepositoryService"/>
    /// </internal>
    public class CachedImmutableReferenceDataEntityService : ICachedImmutableReferenceDataEntityService
    {
        private readonly ICachedImmutableReferenceDataService _immutableReferenceService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedImmutableReferenceDataEntityService"/> class.
        /// </summary>
        /// <param name="immutableReferenceService">The immutable reference service.</param>
        public CachedImmutableReferenceDataEntityService( ICachedImmutableReferenceDataService immutableReferenceService )
        {
            _immutableReferenceService = immutableReferenceService;
        }

        public TReferenceData GetReferenceDataRecord<TEnum, TReferenceData>(TEnum enumValue)
            where TReferenceData : class, IHasReferenceData<TEnum>
            where TEnum : struct, IConvertible
        {
            TReferenceData[] records = _immutableReferenceService.GetReferenceData<TReferenceData>();
            return records.Single(m => ((int)(object)m.Id) == (int)(object)enumValue);
        }


        //private TRecord GetRecord<TEnum, TRecord>(Func<TRecord[]> action, TEnum enumValue)
        //    where TRecord: class, IHasReferenceData<TEnum>
        //    where TEnum : struct, IConvertible
        //{
        //     var records = _immutableReferenceService.GetReferenceData<TRecord>();
        //    return records.Single(m => ((int) (object) m.Id) == (int) (object) enumValue);
        //}

    }

}