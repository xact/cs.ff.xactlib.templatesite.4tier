﻿namespace App.Core.Infrastructure.Services
{
    //using App.Core.Infrastructure.Data;
    using System.Data.Entity.Core.Objects;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using App.Core;

    public interface IRepositoryService : XAct.Domain.Repositories.IRepositoryService, IHasAppCoreService
    {
        DbContext DbContext { get; }
	    ObjectContext ObjectContext { get; }
	    bool IsAttached<TEntity>(Func<DbEntityEntry<TEntity>, bool> predicate) where TEntity : class;
        IList<DbEntityEntry<TEntity>> GetEntry<TEntity>(Func<DbEntityEntry<TEntity>, bool> predicate)where TEntity : class;
        DbEntityEntry<TEntity> GetEntry<TEntity>(TEntity entity) where TEntity : class;
        DbEntityEntry GetEntryNonGeneric(object entity);
        //void Detach<TEntity>(TEntity entity) where TEntity : class;
        //void Detach<TModel>(TModel model) where TModel : class;
        bool IsEntityDirty(object entity);
    }
}