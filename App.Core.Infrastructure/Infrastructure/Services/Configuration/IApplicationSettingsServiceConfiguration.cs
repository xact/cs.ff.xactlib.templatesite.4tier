﻿namespace App.Core.Infrastructure.Services.Configuration
{
    using App.Core;
    using XAct;

    public interface IApplicationSettingsServiceConfiguration : IHasAppCoreServiceConfiguration, IHasInitialize
    {
    }
}
