﻿
namespace App.Core.Infrastructure.Services.Configuration
{
    using App.Core;
    using XAct;

    public interface ISmtpServiceConfiguration  : IHasAppCoreServiceConfiguration, IHasInitialize 
    {
        object InnerItem { get; }
    }
}
