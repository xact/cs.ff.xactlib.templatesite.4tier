﻿namespace App.Core.Infrastructure.Services.Configuration.Implementations
{
    using App.Core.Infrastructure.Services;
    using XAct.Diagnostics.Status.Connectors.Implementations;
    using XAct.Services;

    public class DiagnosticsStatusServiceConfiguration : IDiagnosticsStatusServiceConfiguration
    {
        private readonly XAct.Diagnostics.Status.IStatusService _statusService;


        /// <summary>
        /// Initializes a new instance of the <see cref="DiagnosticsStatusServiceConfiguration"/> class.
        /// </summary>
        /// <param name="statusService">The status service.</param>
        public DiagnosticsStatusServiceConfiguration(XAct.Diagnostics.Status.IStatusService statusService)
        {
            _statusService = statusService;


            AppSettingsStatusServiceConnector appSettingsStatusServiceConnector =
                XAct.DependencyResolver.Current.GetInstance<AppSettingsStatusServiceConnector>();

            appSettingsStatusServiceConnector.Configuration.AppSettingDefinitions.Add(new AppSettingsStatusServiceConnectorConfigurationItem("EnvironmentIdentifier","EnvironmentIdentifier"));
            appSettingsStatusServiceConnector.Configuration.AppSettingDefinitions.Add(new AppSettingsStatusServiceConnectorConfigurationItem("SkipMigration","SkipMigration"));
            appSettingsStatusServiceConnector.Configuration.AppSettingDefinitions.Add(new AppSettingsStatusServiceConnectorConfigurationItem("DevFlagSeedEveryStartup","DevFlagSeedEveryStartup"));
            appSettingsStatusServiceConnector.Configuration.AppSettingDefinitions.Add(new AppSettingsStatusServiceConnectorConfigurationItem("SeedingType", "SeedingType"));

            _statusService.Configuration.Register(appSettingsStatusServiceConnector);


            //FileSizeStatusServiceConnector fileSizeStatusServiceConnector = 
            //    XAct.DependencyResolver.Current.GetInstance<FileSizeStatusServiceConnector>();
            //fileSizeStatusServiceConnector.Configuration.Files.Add(new FileNameAndSizeLimits{FileName = });
            //_statusService.Configuration.Register(fileSizeStatusServiceConnector);


            DirectoryAccessibilityStatusServiceConnector directoryAccessibilityStatusServiceConnector= 
                XAct.DependencyResolver.Current.GetInstance<DirectoryAccessibilityStatusServiceConnector>();

            directoryAccessibilityStatusServiceConnector.Configuration.Directories.Add(
                new DirectoryAccessibilityStatusServiceConnectorConfigurationItem("AppData/Logs","AppData/Logs",true));
            
            _statusService.Configuration.Register(directoryAccessibilityStatusServiceConnector);


            //CodeFirstMigrationsStatusServiceConnector codeFirstMigrationsStatusServiceConnector =
            //    XAct.DependencyResolver.Current.GetInstance<CodeFirstMigrationsStatusServiceConnector>();

            //IUnitOfWorkService unitOfWorkService =
            //    XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
            //codeFirstMigrationsStatusServiceConnector.Configuration.Configure<App.Core.Infrastructure.Migrations.Configuration>(unitOfWorkService.DbContext.Database.Connection.ConnectionString,unitOfWorkService.DbContext.Database.Connection. );


        }
    }
}