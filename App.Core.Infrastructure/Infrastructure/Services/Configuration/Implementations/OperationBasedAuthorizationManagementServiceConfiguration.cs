﻿
namespace App.Core.Infrastructure.Services.Configuration.Implementations
{
    using System.Security;
    using App.Core.Infrastructure.Security;
    using XAct;
    using XAct.Security.Authorization;
    using XAct.Security.Authorization.MethodBased;
    using XAct.Services;

    /// <summary>
    /// IMplementation of 3rd party Configuration package
    /// that is injected into 3rd party 
    /// implementation of 
    /// <see cref="IOperationBasedAuthorizationManagementService"/>
    /// which is backing Service invoked by 
    /// <see cref="AuthenticationCallHandler"/>
    /// Configuration
    /// </summary>
    [DefaultBindingImplementation(typeof(IOperationBasedAuthorizationManagementServiceConfiguration),BindingLifetimeType.Undefined,Priority.High)]
    public class OperationBasedAuthorizationManagementServiceConfiguration : IOperationBasedAuthorizationManagementServiceConfiguration
    {
        public bool AllowAccessWhenOperationNotMentionedInDataStore 
        {
			get { return _allowAccessWhenMethodNotMentionedInDataStore; }
	        set
            {
		        throw new SecurityException("OperationBasedAuthorizationManagementServiceConfiguration - Application is trying to change 'allow access'"); 
		        
            }
        }

		private bool _allowAccessWhenMethodNotMentionedInDataStore = false;

    }
}
