﻿using System;
using System.Collections.Generic;
using XAct;

namespace App.Core.Infrastructure.Data.DbContexts
{
    using XAct.Domain.Repositories.Configuration;
    using XAct.Services;
    using XAct.Domain.Repositories;

    /// <summary>
    /// <para>
    /// Essential for any access to the Db using a Repository.
    /// </para>
    /// 
    /// <para>
    /// Only used by (but essential to the app) by RepositoryService,
    /// which uses the default implementation of IUnitOfWorkFactory 
    /// to determine when to create a new UoW. 
    /// If a new UoW is created, it in turn needs to wrap a DbContext...hence
    /// a call to this factory.
    /// </para>
    /// </summary>
    public class UnitOfWorkServiceConfiguration : IUnitOfWorkServiceConfiguration 
    {
        public UnitOfWorkServiceConfiguration()
        {
            SetFactoryDelegate(() => new XAct.Domain.Repositories.EntityDbContext(new AppDbContext()));
        }

        public void SetFactoryDelegate(Func<IUnitOfWork> func, string key = "Default")
        {
            if (key.IsNullOrEmpty())
            {
                key = "Default";
            }
            key = key.ToLower();

            _delegates[key] = func;
        }

        public Func<IUnitOfWork> GetFactoryDelegate(string key = "Default")
        {
            if (key.IsNullOrEmpty())
            {
                key = "Default";
            }
            key = key.ToLower();
            Func<IUnitOfWork> result;

            return _delegates.TryGetValue(key, out result) ? result : null;
        }

        readonly IDictionary<string, Func<IUnitOfWork>> _delegates = new Dictionary<string, Func<IUnitOfWork>>();
    }
}

