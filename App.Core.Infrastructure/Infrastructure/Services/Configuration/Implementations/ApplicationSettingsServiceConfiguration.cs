﻿using XAct.Settings;

namespace App.Core.Infrastructure.Services.Configuration.Implementations
{
    using System.Diagnostics;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services.Configuration;
    using XAct;
    using XAct.Services;

    public class ApplicationSettingsServiceConfiguration : IApplicationSettingsServiceConfiguration
    {
        private readonly IHostSettingsService _hostSettingsService;

        public ApplicationSettingsServiceConfiguration(IHostSettingsService hostSettingsService)
        {
            _hostSettingsService = hostSettingsService;
        }

        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="T:XAct.IHasInitialize" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized { get; private set; }


        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            if (Initialized)
            {
                return;
            }
            lock (this)
            {

            if (Initialized)
            {
                return;
            }
                //Most times, XActLib works by Convention and doesn't need much setup.
                //But there are a couple of tweaks that need manual intervention.


                // One tweak is defining the Type of the object used to carry around
                // ApplicationSettings. 
                // The default one (XAct.Settings.Settings) is functional -- but it's properties aren't typed.
                // Using an on-the-fly code builder as was used by ASP.NET 2.0's Profile class is cute...but
                // overkill. Defining a custom Type will do fine:
                XAct.Settings.IApplicationSettingsServiceConfiguration vendorConfig = 
                    XAct.DependencyResolver.Current.GetInstance<XAct.Settings.IApplicationSettingsServiceConfiguration>();

                vendorConfig.Initialize<AppSettings>(null,
                    "BackTier",
                    null,
                    null,
                    (s)=> s != "N"
					);

                //BUGFIX: Lib wasn't setting this. Next version of XActLib will incorporate this:
                //vendorConfig.ZoneOrTierName = "BackTier";

                Debug.Assert(vendorConfig.ZoneOrTierIdentifier == "BackTier");


                string hostIdentifier =  _hostSettingsService.EnvironmentIdentifier;
         
                vendorConfig.EnvironmentIdentifier = hostIdentifier;
                Initialized = true;
            }
        }


    }
}