﻿namespace App.Core.Infrastructure.Services.Configuration.Implementations
{
    using App.Core.Infrastructure.Services.Configuration;
    using XAct;
    using XAct.Net.Messaging;
    using XAct.Services;

    public class SmtpServiceConfiguration : ISmtpServiceConfiguration
    {
        private XAct.Net.Messaging.Services.Configuration.ISmtpMessageDeliveryServiceConfiguration _smtpConfiguration;

        public bool Initialized { get; private set; }


        public void Initialize()
        {

            if (Initialized)
            {
                return;
            }
            lock (this)
            {
                if (Initialized)
                {
                    return;
                }

            _smtpConfiguration =
                XAct.DependencyResolver.Current.GetInstance<XAct.Net.Messaging.Services.Configuration.ISmtpMessageDeliveryServiceConfiguration>();


            //Or leave it Disabled, so that settings come from Config File.

            IApplicationSettingsService settingsService =
                XAct.DependencyResolver.Current.GetInstance<Core.Infrastructure.Services.IApplicationSettingsService>();


            AppSettings appSettings = settingsService.AppSettings;



            //TODO: Settings not coming up from Db Settings

            ////Indicate whether Configuration will be taken into account, 
            ////or settings will come from local web.config
            bool enabled = appSettings.Services_Messaging_SMTP_Enabled;

                if (enabled)
                {
                    _smtpConfiguration.Enabled = enabled;


                    if (!appSettings.Services_Messaging_SMTP_FromAddress.IsNullOrEmpty())
                    {
                        _smtpConfiguration.FromAddress = new MessageAddress(appSettings.Services_Messaging_SMTP_FromAddress);
                    }


                    if (!appSettings.Services_Messaging_SMTP_Host.IsNullOrEmpty())
                    {
                        _smtpConfiguration.ServerName = appSettings.Services_Messaging_SMTP_Host;
                    }

                    //int intTmp;
                    if (appSettings.Services_Messaging_SMTP_Port != 0)
                    {
                        _smtpConfiguration.Port = appSettings.Services_Messaging_SMTP_Port;
                    }

                    //bool boolTmp;
                    if (appSettings.Services_Messaging_SMTP_SSL)
                    {
                        _smtpConfiguration.Ssl = appSettings.Services_Messaging_SMTP_SSL;
                    }
                    //Obviously, a Db is a terrible place to put UserNames and Passwords, 
                    //but it's show below to demonstrate how to do it if need be (as in ... never):
                    if (!appSettings.Services_Messaging_SMTP_UserName.IsNullOrEmpty())
                    {
                        _smtpConfiguration.UserName = appSettings.Services_Messaging_SMTP_UserName;
                    }
                    if (!appSettings.Services_Messaging_SMTP_Password.IsNullOrEmpty())
                    {
                        _smtpConfiguration.Password = appSettings.Services_Messaging_SMTP_Password;
                    }

                    Initialized = true;
                }


            }
        }

        public object InnerItem
        {
            get { return _smtpConfiguration; }
        }

      
    }
}