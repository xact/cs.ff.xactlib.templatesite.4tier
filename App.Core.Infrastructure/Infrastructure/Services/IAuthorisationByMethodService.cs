﻿namespace App.Core.Infrastructure.Services
{
    using System.Reflection;
    using App.Core;
    using XAct.Security;

    public interface IAuthorisationByMethodService : IHasAppCoreService
    {
        bool AuthenticatePrincipalByMethod(MethodInfo methodInfo, out AuthorizationResult authorisationResult);
    }
}
