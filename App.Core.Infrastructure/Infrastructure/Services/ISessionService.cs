﻿namespace App.Core.Infrastructure.Services
{
    using System;
    using App.Core.Application.Services.Facades;
    using App.Core.Domain.Messages;
    using App.Core.Domain.Models;
    using XAct.Messages;

    public interface ISessionService : IHasAppService, IHasPing
    {

        /// <summary>
        /// Instantiates the <see cref="Session"/> entity and identifies with an unique identifier (App Session Token).
        /// The session entity and passed SignInInfo entity are then persisted in the database, Session entity is cached.
        /// </summary>
        /// <param name="identityBase64">The identity base64.</param>
        /// <param name="signInInfo">The session log.</param>
        /// <returns>
        /// App Session Token
        /// Errors:
        /// -...
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// sessionLog
        /// or
        /// identityBase64
        /// </exception>
        /// <exception cref="System.ArgumentException">Invalid identity string</exception>
        Response<SessionResponse> CreateSession(string identityBase64, SignInInfo signInInfo);


        /// <summary>
        /// Given an existing valid (non-default) <see cref="Guid"/>,
        /// updates the associated <see cref="Session"/>'s LastAccessedDateTime
        /// and optionally (depending on <paramref name="returnSession"/>)
        /// returns the updated <see cref="Session"/>.
        /// <para>
        /// Returns a MessageCodes.SECURITY_TIMEOUT Message if timed out.
        /// </para>
        /// </summary>
        /// <param name="sessionToken"></param>
        /// <param name="returnSession"></param>
        /// <returns>The <see cref="Session"/></returns>
        Response<SessionResponse> UpdateSessionLastAccessedDateTime(Guid sessionToken, bool returnSession = false);

        void DeleteSession(Guid sessionToken);

        bool DoesSessionExist(Guid sessionToken);


        /// <summary>
        /// Tries to get the session from cache. If its not in the cache, get's it from the database.
        /// <para>
        /// It does not return null (it will raise Exception if Session not found in Db).
        /// </para>
        /// <para>
        /// It does not update the <c>LastAccessDateTime</c> (that should have been 
        /// done via an earlier invocation of <see cref="UpdateSessionLastAccessedDateTime"/>).
        /// </para>
        /// <para>
        /// Note that the <see cref="Session"/> object's <c>LastAccessedDateTime</c> is not guaranteed to still be valid.
        /// </para>
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <returns></returns>
        Session GetSession(Guid sessionToken);

    }
}