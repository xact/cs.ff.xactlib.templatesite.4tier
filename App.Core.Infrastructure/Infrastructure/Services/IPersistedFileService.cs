﻿
namespace App.Core.Infrastructure.Services
{
    using System;
    using App.Core;
    using XAct.Net.Messaging;

    /// <summary>
    /// Contract for a service to
    ///             persist File Streams
    ///             to a persistent store mechanism.
    /// 
    /// </summary>
    public interface IPersistedFileService : IHasAppCoreService
    {
        /// <summary>
        /// Persists (saves or Updates) the <see cref="T:XAct.Net.Messaging.PersistedFile"/> object.
        /// <para>
        /// Important:
        /// </para>
        /// <para>
        /// <see cref="PersistedFile"/> uses a <see cref="Guid"/>
        /// as it's identity, but understand that it is not Generated 
        /// by the Db. It is set by the underlying service (using IDistributedIdentifierService.NewGuid())
        /// upon committing.
        /// </para>
        /// </summary>
        /// <param name="persistedFile">The persisted file.</param>
        void Persist(PersistedFile persistedFile);



        /// <summary>
        /// Retrieves the <see cref="T:XAct.Net.Messaging.PersistedFile" />
        /// </summary>
        /// <param name="applicationTennantId">The organisation identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="includeMetadata">if set to <c>true</c>, search for any metadata to include.</param>
        /// <returns></returns>
        PersistedFile Retrieve(Guid id, bool includeMetadata = false, Guid? applicationTennantId=null);
    }
}
