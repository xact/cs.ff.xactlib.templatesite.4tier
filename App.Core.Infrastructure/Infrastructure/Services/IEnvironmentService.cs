﻿namespace App.Core.Infrastructure.Services
{
    using App.Core;
    using App.Core.Infrastructure.Services;

    public interface IEnvironmentService :IAppEnvironmentService, IHasAppCoreService
    {
    }
}
