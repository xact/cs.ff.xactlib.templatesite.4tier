﻿
namespace App.Core.Infrastructure.Services
{
    using System;
    using App.Core;
    using XAct.Scheduling;

    public interface ISchedulingService : IHasAppCoreService
    {
        ISchedulingServiceConfiguration Configuration { get; }

        ScheduledTask[] GetSchedules(string group = null);

        ScheduledTask GetSchedule(Guid id);

        ScheduledTask GetSchedule(string name, string group = null);

        void AddScheduledTask(ScheduledTask scheduledTask);

        void DeleteScheduledTask(Guid id);

        void DeleteScheduledTask(string name, string group = null);

    }
}
