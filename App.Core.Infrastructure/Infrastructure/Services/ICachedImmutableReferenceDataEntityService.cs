﻿using XAct;

namespace App.Core.Infrastructure.Services
{
    using System;
    using App.Core;
    using App.Core.Domain.Entities;

    /// <summary>
    /// Contract for a service to get (from cache)
    /// the Reference Entity associated to the given Enum value.
    /// <para>
    /// This provides a means to get to the updatable 
    /// Attributes / Extended properties
    /// (such as Order, Enabled, Internal Code, available on the Entity
    /// only, as Enums can't carry that much information).
    /// </para>
    /// </summary>
    public interface ICachedImmutableReferenceDataEntityService : IHasAppCoreService
    {
        TReferenceData GetReferenceDataRecord<TEnum, TReferenceData>(TEnum enumValue)
            where TReferenceData : class, IHasReferenceData<TEnum>
            where TEnum : struct, IConvertible;
    }
}