﻿namespace App.Core.Infrastructure.Services
{
    using App.Core;
    using App.Core.Infrastructure.Services.Configuration;

    /// <summary>
    /// Contract for a service to send messages via SMTP (and optionally other protocols).
    /// </summary>
    public interface IMessagingService : IHasAppCoreService
    {

        ISmtpServiceConfiguration Configuration { get; }

        /// <summary>
        /// Sends the message.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="cc">The cc.</param>
        /// <param name="bcc">The BCC.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="textbody">The textbody.</param>
        /// <param name="htmlBody">The HTML body.</param>
        void SendMessage(string from, string to, string cc, string subject, string textbody, string htmlBody);
    }
}
