﻿using System;
using XAct.Domain.Repositories;
using XAct.Domain.Repositories.Configuration;

namespace App.Core.Infrastructure.Services
{
    using System.Data.Entity;
    using App.Core;

    /// <summary>
    /// Contract for a service to commit the current unit of work,
    /// committing changes to the remote datastores.
    /// </summary>
    public interface IUnitOfWorkService : IHasAppCoreService
    {

        void SetCurrent(IUnitOfWork unitOfWork, string contextKey = "Default");

        IUnitOfWork GetCurrent(string contextKey = "Default");

        
        
        //DbContext DbContext { get; set; }

        /// <summary>
        /// Commits the current UnitOfWork.
        /// </summary>
        void Commit(string contextKey = "Default");

        void RollBack(string contextKey = "Default");

        //DbContext GetDbContext(string contextKey = "Default");

    }
}
