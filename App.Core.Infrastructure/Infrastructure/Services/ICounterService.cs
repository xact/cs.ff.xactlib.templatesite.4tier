﻿
namespace App.Core.Infrastructure.Services
{
    using System;
    using App.Core;

    /// <summary>
    /// Contract for a service 
    /// to manage counters for an arbitrary entity.
    /// </summary>
    public interface ICounterService : IHasAppCoreService
    {
        /// <summary>
        /// Gets the counter's value, or returns 0 if no counter found.
        /// 
        /// </summary>
        /// <param name="targetId">The target identifier (often a User's Id).</param><param name="key">The key for a counter specific to the <paramref name="targetId"/>.</param>
        /// <returns/>
        int GetValue(string key, string targetId = null);

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="key">The key for a counter specific to the <paramref name="targetId"/>.</param><param name="value">The value.</param><param name="targetId">The target identifier.</param>
        void SetValue(string key, int value, string targetId=null);

        /// <summary>
        /// Removes the specified counter.
        /// 
        /// </summary>
        /// <param name="targetId">The target identifier.</param><param name="key">The key.</param>
        void Remove(string key, string targetId = null);
    }
}
