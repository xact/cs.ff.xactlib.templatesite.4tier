﻿namespace App.Core.Infrastructure.Data.Maps
{
    using System;
    using XAct;

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class EntityTypeMapAttribute : Attribute
    {
        public string Tag { get; set; }

        public EntityTypeMapAttribute(string tag)
        {
            tag.ValidateIsNotNullOrEmpty("tag");
            Tag = tag;
        }
    }
}