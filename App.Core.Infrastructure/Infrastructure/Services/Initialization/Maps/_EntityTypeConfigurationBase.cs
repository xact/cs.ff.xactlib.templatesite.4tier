﻿namespace App.Core.Infrastructure.Data.Maps
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct;
    using XAct.Data;

    [EntityTypeMap("Core")]
    public abstract class EntityTypeConfigurationBase<TEntity> : EntityTypeConfiguration<TEntity>
        where TEntity : class 
    {

    }


    public abstract class ReferenceDataPersistenceMapBase<T,TId> : XAct.Data.ReferenceDataPersistenceMapBase<T,TId>
        where TId : struct
        where T : class, IHasReferenceData<TId>, IHasTimestamp, new()
    {
        protected ReferenceDataPersistenceMapBase(string tableNameSuffix, DatabaseGeneratedOption idDatabaseGeneratedOption) : base(tableNameSuffix, idDatabaseGeneratedOption)
        {
        }
    }
}
