
namespace App.Module.Example.Repositories.Implementations
{
    using System;
    using App.FW.Infrastructure.Data.Repositories;
    using App.FW.Infrastructure.Services;
    using App.Module.Example.Domain.Repositories;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// <para>
    /// Repositories are implemented in an infrastructure layer
    /// as they relate to one or more veondor product 
    /// (in this case IE's EF + SqlServer) but they are unlike other
    /// Inrastructure contracts.
    /// </para>
    /// <para>
    /// The are a bridge pattern between Domain and Infrastructure.
    /// Hence why the contract is in the Domain assembly, with the 
    /// implementation is in an infrastructure assembly.
    /// </para>
    /// </internal>
    [DefaultBindingImplementation(typeof(IExampleRepositoryService))]
    public class ExampleRepositoryService : RepositoryServiceBase, IExampleRepositoryService
    {
        private readonly XAct.Domain.Repositories.IUnitOfWorkService _unitOfWorkService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FakeExampleRepositoryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="environmentService"> </param>
        /// <param name="unitOfWorkService"></param>
        public ExampleRepositoryService(
			Core.Infrastructure.Services.ITracingService tracingService,
			Core.Infrastructure.Services.IAppEnvironmentService environmentService,
			IRepositoryService repositoryService,
            XAct.Domain.Repositories.IUnitOfWorkService unitOfWorkService
			)
			: base(tracingService, environmentService, repositoryService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public string Ping()
		{
			return DateTime.Now.ToShortTimeString();
		}

        public ExampleSummary[] Search(string term, IPagedQuerySpecification pagedQuerySpecification)
        {
            //ExampleSummary[] results = _fakeRepo.Where(x => x.Name.Contains(term)).Skip(pagedQuerySpecification.PageIndex * pagedQuerySpecification.PageSize).Take(pagedQuerySpecification.PageSize).Select(x => new ExampleSummary { Id = x.Id, Name = x.Name }).ToArray();
            //return results;

            throw new NotImplementedException("fake repo has to be changed to real one");
        }

        public ExampleSummary[] Search(string term)
        {
            //return _fakeRepo.Where(x => x.Name.Contains(term)).Select(x=> new ExampleSummary {Id=x.Id,Name=x.Name }).ToArray();
            throw new NotImplementedException("fake repo has to be changed to real one");
        }

	    public Example GetSingle(Guid id)
	    {
	        return _repositoryService.GetSingle<Example>(example => example.Id == id);		    
        }

        public Example GetSingle(string code)
        {
            return _repositoryService.GetSingle<Example>(example => example.Code == code);
        }
        public void Add(Example example)
        {
            _repositoryService.AddOnCommit(example);
        }

        public void Update(Example example)
        {            
            _repositoryService.UpdateOnCommit(example);
        }

        public void Delete(Guid id)
        {
            _repositoryService.DeleteOnCommit<Example>(example => example.Id == id);
        }

        public void Delete(Example example)
        {
            _repositoryService.DeleteOnCommit<Example>(example);
        }

        public void Delete(string code)
        {
            _repositoryService.DeleteOnCommit<Example>(example => example.Code == code);
        }

    }
}