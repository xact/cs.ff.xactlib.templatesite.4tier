

namespace App.Module.Example.Repositories.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// <para>
    /// Repositories are implemented in an infrastructure layer
    /// as they relate to one or more veondor product 
    /// (in this case IE's EF + SqlServer) but they are unlike other
    /// Inrastructure contracts.
    /// </para>
    /// <para>
    /// The are a bridge pattern between Domain and Infrastructure.
    /// Hence why the contract is in the Domain assembly, with the 
    /// implementation is in an infrastructure assembly.
    /// </para>
    /// </internal>
    //[DefaultBindingImplementation(typeof(IExampleRepositoryService))]
    public class FakeExampleRepositoryService : RepositoryServiceBase, IExampleRepositoryService
    {
// ReSharper disable InconsistentNaming
	    static readonly List<Example> _fakeRepo = new List<Example>();
// ReSharper restore InconsistentNaming


		static FakeExampleRepositoryService()
		{
            //Make some fake records:
			for (int i = 0; i < 100; i++)
			{
				_fakeRepo.Add(new Example {Id = (i+1).ToGuid(), Name = "Some Name " + (i + 1)});
			}
		}

	    /// <summary>
	    /// Initializes a new instance of the <see cref="FakeExampleRepositoryService" /> class.
	    /// </summary>
	    /// <param name="tracingService">The tracing service.</param>
	    /// <param name="repositoryService">The repository service.</param>
	    /// <param name="environmentService"> </param>
	    public FakeExampleRepositoryService(
			Core.Infrastructure.Services.ITracingService tracingService,
			Core.Infrastructure.Services.IAppEnvironmentService environmentService,
			IRepositoryService repositoryService
			)
			: base(tracingService, environmentService, repositoryService)
		{
		}


	    public string Ping()
		{
			return DateTime.Now.ToShortTimeString();

		}

        public ExampleSummary[] Search(string term, IPagedQuerySpecification pagedQuerySpecification)
        {
            ExampleSummary[] results = 
                _fakeRepo
                .Where(x => x.Name.Contains(term))
                .Skip(pagedQuerySpecification.PageIndex * pagedQuerySpecification.PageSize)
                .Take(pagedQuerySpecification.PageSize).Select(x => new ExampleSummary { Id = x.Id, Name = x.Name }).ToArray();
            return results;
        }

        public ExampleSummary[] Search(string term)
        {
            return _fakeRepo.Where(x => x.Name.Contains(term)).Select(x=> new ExampleSummary {Id=x.Id,Name=x.Name }).ToArray();
        }

	    public Example GetSingle(Guid id)
	    {
		    return _fakeRepo.FirstOrDefault(x => x.Id == id);
        }

        public Example GetSingle(string code)
        {
            return _fakeRepo.FirstOrDefault(x => x.Code == code);
        }

        public void Add(Example example)
        {
            example.Id = Guid.NewGuid();
            
            _fakeRepo.Add(example);
        }
        public void Update(Example example)
        {
            Example toUpdate = GetSingle(example.Id);

            toUpdate.Name = example.Name;

            _fakeRepo.Add(example);
        }

        public void Delete(Guid id)
        {
            Example toRemove = GetSingle(id);
            _fakeRepo.Remove(toRemove);
        }

        public void Delete(string code)
        {
            Example toRemove = GetSingle(code);

            _fakeRepo.Remove(toRemove);
        }

        public void Delete(Example example)
        {
            Example toRemove = GetSingle(example.Id);

            _fakeRepo.Remove(toRemove);
        }
    }
}