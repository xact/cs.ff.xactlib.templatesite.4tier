﻿
namespace App.Modules.Example.Domain.Models
{
    using System;
    using System.Runtime.Serialization;
    using App.Modules.Example.Domain.Entities;

    /// <summary>
    /// A nullable version of <see cref="Example"/> that can be posted
    /// to an Application Layer Service (on the same tier as the Repository Service)
    /// for it to map over an existing <see cref="Example"/>, prior to validating,
    /// and saving.
    /// <para>
    /// IMPORTANT: Properties can be left null, indicating no change
    /// </para>
    /// </summary>
    [DataContract]
    public class NullableExample : Example
    {

        /// <summary>
        /// A nullable date time.
        /// </summary>
        [DataMember]
        public new System.DateTime? Date { get; set; }

        /// <summary>
        /// A flag to indicate the value of <see cref="Date"/> is to be cleared.
        /// </summary>
        [DataMember]
        public bool? DateIsCleared { get; set; }


        /// <summary>
        /// Gets or sets the category fk.
        /// </summary>
        [DataMember]
        public new Guid? CategoryFK { get; set; }


    }
}
