namespace App.Core.Domain.Models
{
    using System.Runtime.Serialization;
    using App.Core.Domain.Entities;
    using XAct;

    /// <summary>
    /// <para>
    /// IMPORTANT: Notice how this reference data type is decorated
    /// with <see cref="WellKnownNameAttribute"/> which is needed
    /// by <c>CachedImmutableReferenceDataServiceConfiguration</c>.
    /// </para>
    /// </summary>
    [DataContract]
    [XAct.WellKnownName(Constants.ReferenceData.ImmutableReferenceData.Gender)]
    public class Reference_Gender : ReferenceDataBase<Gender>
    {


    }
}
