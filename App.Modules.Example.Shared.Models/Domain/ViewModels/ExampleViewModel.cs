﻿namespace App.Modules.Example.Domain.ViewModels
{
    using System.Runtime.Serialization;
    using XAct;

    /// <summary>
    /// A DTO for serializing <see cref="Example"/>
    /// </summary>
    /// <internal>
    /// Note that DTOs are decorated with WCF attributes
    /// so that they can be serialized across tiers if necessary
    /// as well as JSON-ed for the UI.
    /// </internal>
    public class ExampleViewModel : IHasId<int>, IHasName
    {
        [DataMember(Name = "Id")]
        public int Id { get; set; }


        [DataMember(Name = "Name")]
        public string Name { get; set; }


        /// <summary>
        /// Type is a Imuttable (ie, it's not going to change
        /// during the lifetime of the app) that is kept in
        /// sync with an Enum value.
        /// <para>
        /// The Enum is used for logical routing. 
        /// </para>
        /// <para>
        /// The reasons for it are to 
        /// a) show how one can keep the Db Relational
        /// b) associate richer meaning to Enum values (ie, dynamically disable, etc.)
        /// </para>
        /// </summary>
        [DataMember(Name = "TypeFK")]
        public int TypeFK { get; set; }


        [DataMember(Name = "CategoryFK")]
        public int CategoryFK { get; set; }

    }
}