﻿namespace App.Modules.Example.Domain.ViewModels
{
    using System.Runtime.Serialization;
    using XAct;

    /// <summary>
    /// The ViewModel for a <see cref="ExampleType"/>
    /// </summary>
    [DataContract]
    public class ExampleTypeViewModel : IHasId<int>, IHasEnabled, IHasName, IHasOrder /*TODO:IHasResourceFilterAndKey*/
    {

        /// <summary>
        /// Gets or sets the id.
        /// <para>
        /// Note that this <see cref="ExampleType"/>
        /// is a Fixed List, and the Id's align with the 
        /// <see cref="ExampleTypeEnum"/> that goes with it.
        /// </para>
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public int Id { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ExampleType" /> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get; set; }


        public int Order { get; set; }

        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="T:XAct.IHasName" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public string Name { get; set; }


        /// <summary>
        /// Gets or sets the Filter for the Resource in the <c>XAct.Resources.IResourceFilter, XAct.Resources</c>.
        /// </summary>
        /// <value>
        /// The resource filter.
        /// </value>
        [DataMember]
        public string ResourceFilter { get; set; }

        /// <summary>
        /// The Key to the Resource in the <c>XAct.Resources.IResourceService, XAct.Resources</c>.
        /// </summary>
        [DataMember]
        public string ResourceKey { get; set; }

    }
}
