﻿
namespace App.Modules.Example.Domain.ViewModels
{
    using System.Runtime.Serialization;
    using XAct;

    /// <summary>
    /// View IOModel for the <see cref="ExampleCategory"/>
    /// Reference Type.
    /// </summary>
    [DataContract(Name = "ExampleCategory")]
    public class ExampleCategoryViewModel : IHasId<int>
    {

        /// <summary>
        /// Gets or sets the datastore Identifier.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="T:XAct.IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "Enabled")]
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="T:XAct.IHasOrder" />.
        /// </para>
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [DataMember(Name = "Order")]
        public int Order { get; set; }

        /// <summary>
        /// Gets the Exampe Category Name (note, not Localised).
        /// <para>Member defined in<see cref="T:XAct.IHasName" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        [DataMember(Name = "Key")]
        public string Key { get; set; }

        [DataMember(Name = "Value")]
        public string Value { get; set; }

        [DataMember(Name = "Tag")]
        public string Tag { get; set; }

        [DataMember(Name = "Filter")]
        public string Filter { get; set; }
    }
}
