﻿// ReSharper disable CheckNamespace
namespace App.Modules.Example.Domain.Messages
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    [DataContract]
    public class ExampleSearchTerms
    {

        /*
        IMPORTANT:
        WCF is an *opt-in* serialization mechanism. 
        All properties you want serialized across the wire
        have to be marked DataMember, and the class with DataContract
        before they make it across the tier correctly.
        Domain Enum Type have to be marked with EnumMember.
        If you forget to add that, you'll be chasing annoying bugs.
        */

        //Just one field, but could be expanded 
        //to include other attributes such as
        //CategoryName, Gender, etc.
        [DataMember]
        public string Term { get; set; }
    }




}
