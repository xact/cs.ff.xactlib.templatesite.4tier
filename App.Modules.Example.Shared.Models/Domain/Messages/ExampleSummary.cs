﻿// ReSharper disable CheckNamespace
namespace App.Modules.Example.Domain.Messages
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;
    using App.Modules.Example.Domain.Entities;
    using XAct;

    /// <summary>
    /// An Example Summary is a short flattened version of 
    /// <see cref="Example"/>, <see cref="ExampleCategory"/> 
    /// <see cref="ExampleTag"/> 
    /// of use when Searching and Listing
    /// <see cref="Example"/>
    /// </summary>
    [DataContract]
    public class ExampleSummary: IHasId<Guid>, IHasName
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="T:XAct.IHasName" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public string Name { get; set; }


    }
}