﻿// ReSharper disable CheckNamespace
namespace App.Modules.Example.Domain.Entities
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    using App.Core.Domain.Entities;
    using App.Modules.Example.Domain.Entities.Enums;
    using App.Modules.Example.Domain.Models;
    using XAct;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Example : IHasDistributedGuidIdAndTimestamp, XAct.IHasName, IHasExampleType
        /*,TODO:In Next Lib version: XAct.IHasCode*/
    {

        /*
        IMPORTANT: WCF CONSIDERATIONS:
         * WCF is an *opt-in* serialization mechanism. 
         * All properties you want serialized across the wire
         * have to be marked DataMember, and the class with DataContract
         * before they make it across the tier correctly.
         * If you forget to add that, you'll be chasing annoying bugs...
        */


         /* 
         * IMPORTANT: EF CONSIDERATIONS: VIRUTAL:
         * Properties are marked as virtual. 
         * This allows two things:
         * If all scalar properties are marked virtual, it allows Proxies to be automatically created
         * in order to manage ChangeTracking more effectively (Proxies are allow for faster saves than 
         * Snapshot comparison.
         * If Collection properties are marked virtual, this allows Late Binding.
        */

        /* 
         * IMPORTANT: EF CONSIDERATIONS: IDs:
         * When an ID is created using a Guid, there are many syncronisation
         * reasons to *NOT* let the db generate the Guid, and instead take
         * over the process by defining the Guid in the Constructor.
         * This allows syncronisation (which an int cannot) allowing for syncing
         * between local, cloud, and mobile records.
         * That said, a Timestamp property is then also required (in order for EF
         * to distinguish between initial save (when timestamp is null) and subsequent
         * updates.
         */

        /*
         * IMPORTANT: EF CONSIDERATIONS: *IMMUTABLE* REFERENCE DATA: 
         * Note than in this example object there are two Reference datas. 
         * One of them is considered Immutable (will be the same throughout the app,
         * unless recompiled). The other is considered  Mutable 
         * (end users could add/remove to the list without compiling).
         * The mutable list's Id is encoded as an Enum -- we use this Enum
         * as the FK type for the immutable list.
         * For the mutable list, we can't encode with an Enum, so we just use 
         * a standard scalar Type (int/Guid). In this case, to allow for Categories
         * to be added from a Mobile, Azure, or on local databases -- and be synced later --
         * we chose a Guid.
         */

        /*
         * IMPORTANT: EF CONSIDERTIONS: NOT NULL
         * Nullable columns make for slower indexes and can lead to subtle Linq bugs
         * when writting Where(x) statements. It's best to avoid them where possible,
         * sacrificing storage space.
         * This is one of the contributing factors behind the preference of round-tripping
         * up to the UI a nullable ViewModel version of the Entity, rather than the non-nullable
         * Entity itself. 
         */

        /*
         * IMPORTANT: VIEWMODEL CONSIDERATIONS:
         * Due to the way HTML works, as well as partial rendering on demand, it is possible
         * that the post back data is not present.
         * It's best to use a NullableInputModel to post data back, ignoring null data.
         * Fields are cleared by posting back string.Empty (for strings), and DateTime.Empty (for Dates)
         * or use other fields (EnabledCleared=true).
         */


        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string Code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <internal>
        /// Note that this property is not nullable
        /// (whereas the equivalent property on 
        /// <see cref="NullableExample"/> is.
        /// </internal>
        [DataMember]
        public virtual DateTime Date { get; set; }



        /// <summary>
        /// Type is a Imuttable (ie, it's not going to change
        /// during the lifetime of the app) that is kept in
        /// sync with an Enum value.
        /// <para>
        /// The Enum is used for logical routing. 
        /// </para>
        /// <para>
        /// The reasons for it are to 
        /// a) show how one can keep the Db Relational
        /// b) associate richer meaning to Enum values (ie, dynamically disable, etc.)
        /// </para>
        /// </summary>
        [DataMember]
        public virtual ExampleTypeEnum TypeFK { get; set; }

        /// <summary>
        /// The Type of the Example entity.
        /// </summary>
        /// <internal>
        /// The property is marked as IgnoreDataMember
        /// to ensure it is not serialized between Front/Back tiers
        /// as it's not needed (we just needed the FK)
        /// </internal>
        [IgnoreDataMember]//REF
        public virtual ExampleType Type { get; set; }



        /// <summary>
        /// The Id to the Guid based mutable reference data.
        /// </summary>
        [DataMember]
        public virtual Guid CategoryFK { get; set; }

        /// <summary>
        /// The Category of the Example.
        /// <para>
        /// This Reference Data is Mutable (items can be added/removed
        /// without breaking logic).
        /// </para>
        /// <para>
        /// No Enum is associated to this data.
        /// </para>
        /// </summary>
        /// <internal>
        /// The property is marked as IgnoreDataMember
        /// to ensure it is not serialized between Front/Back tiers
        /// as it's not needed (we just needed the FK)
        /// </internal>
        [IgnoreDataMember]//REF (BUT MUTABLE, SO NEED WATCHING/TRIGGERING)
        public virtual ExampleCategory Category { get; set; }




        public ICollection<ExampleDetail> Details
        {
            get { return _details ?? (_details = new Collection<ExampleDetail>()); }
        }
        [DataMember(Name = "Details")]
        private ICollection<ExampleDetail> _details;
 
        /// <summary>
        /// Initializes a new instance of the <see cref="Example"/> class.
        /// </summary>
        public Example()
        {
            this.GenerateDistributedId();
        }

    }
}
