﻿// ReSharper disable CheckNamespace
namespace App.Modules.Example.Domain.Entities
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;
    using XAct;

    /// <summary>
    /// This entity demonstrates how to associate 
    /// ValueTypes to Entities.
    /// <para>
    /// In an accounting app, 
    /// this could be a LineItem, where <see cref="Example"/>
    /// could be the Invoice, 
    /// in a CRM, this could be an Address, where <see cref="Example"/>
    /// could be a contact.
    /// etc.
    /// </para>
    /// </summary>
    [DataContract]
    public class ExampleDetail : IHasId<int>, IHasKeyValue<string>
    {
        /*
         * TIP: EF Speed tip:
         * Mark all Scalar properties virtual so that
         * change tracking can be done by making model Proxies 
         * (it's faster than Snapshot change tracking).
         */

        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Key { get; set; }
        
        [DataMember]
        public virtual string Value { get; set; }
    }
}
