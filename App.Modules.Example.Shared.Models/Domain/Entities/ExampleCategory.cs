﻿// ReSharper disable CheckNamespace
namespace App.Modules.Example.Domain.Entities
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;
    using App.Core.Domain.Messages;
    using XAct;

    /// <summary>
    /// An Example Category to describe 
    /// <see cref="Example"/>
    /// <para>
    /// IMPORTANT: Notice how this reference data type is decorated
    /// with <see cref="WellKnownNameAttribute"/> which is needed
    /// by <c>CachedImmutableReferenceDataServiceConfiguration</c>.
    /// </para>
    /// </summary>
    [DataContract]
    [XAct.WellKnownName(Core.Constants.ReferenceData.ImmutableReferenceData.ExampleCategory)]
    public class ExampleCategory : ReferenceDataMessage
    {


    }
}
