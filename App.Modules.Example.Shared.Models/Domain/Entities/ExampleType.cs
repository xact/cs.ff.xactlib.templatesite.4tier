﻿// ReSharper disable CheckNamespace
namespace App.Modules.Example.Domain.Entities
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;
    using App.Core.Domain.Models;
    using App.Modules.Example.Domain.Entities.Enums;
    using XAct;

    /// <summary>
    /// An <see cref="ExampleType"/> to describe
    /// <see cref="Example"/>.
    /// <para>
    /// Note that this <see cref="ExampleType"/>
    /// is a Fixed List, and the Id's align with the 
    /// <see cref="ExampleTypeEnum"/> that goes with it.
    /// </para>
    /// <para>
    /// IMPORTANT: Notice how this reference data type is decorated
    /// with <see cref="WellKnownNameAttribute"/> which is needed
    /// by <c>CachedImmutableReferenceDataServiceConfiguration</c>.
    /// </para>
    /// </summary>
    [DataContract]
    //For the Core to be able to find reference 
    //data in Modules, the Reference data has to
    //* be decorated with WellKnownNameAttribute
    //* derive from IHasReferenceData
    [XAct.WellKnownName(Core.Constants.ReferenceData.ImmutableReferenceData.ExampleType)]
    public class ExampleType : 
        IHasReferenceData<Guid>
    {

        /// <summary>
        /// Gets or sets the id.
        /// <para>
        /// Note that this <see cref="ExampleType"/>
        /// is a Fixed List, and the Id's align with the 
        /// <see cref="ExampleTypeEnum"/> that goes with it.
        /// </para>
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        [DataMember]
        public byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ExampleType" /> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Enabled { get; set; }


        [DataMember]
        public virtual int Order { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        [DataMember]
        public virtual string Code { get; set; }


        /// <summary>
        /// Gets or sets the Filter for the Resource in the <c>XAct.Resources.IResourceFilter, XAct.Resources</c>.
        /// </summary>
        /// <value>
        /// The resource filter.
        /// </value>
        [DataMember]
        public virtual string ResourceFilter { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [DataMember]
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        [DataMember]
        public string Tag { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        /// <value>
        /// The filter.
        /// </value>
        [DataMember]
        public string Filter { get; set; }


        /// <summary>
        /// Gets or sets the modified by.
        /// </summary>
        /// <value>
        /// The modified by.
        /// </value>
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified date time.
        /// </summary>
        /// <value>
        /// The modified date time.
        /// </value>
        public DateTime ModifiedDateTime { get; set; }
    }
}
