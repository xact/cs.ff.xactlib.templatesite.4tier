﻿namespace App.Core.Domain.Entities
{
    using App.Core.Domain.Models;
    using App.Modules.Example.Domain.Entities;

    public interface IHasExampleType : IHasExampleTypeFK
    {

        ExampleType Type { get; set; }
    }
}
