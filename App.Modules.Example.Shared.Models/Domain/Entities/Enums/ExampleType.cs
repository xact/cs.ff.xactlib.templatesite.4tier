﻿// For enums, use the same namespace as the entities they describe 

// ReSharper disable CheckNamespace
namespace App.Modules.Example.Domain.Entities.Enums
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    [DataContract]
    public enum ExampleTypeEnum
    {
        /// <summary>
        ///     No value specified.
        ///     <para>
        ///         NOTE: This is an error condition (a speficified type is expected).
        ///     </para>
        ///     <para>
        ///         Value = 0
        ///     </para>
        /// </summary>
        /// <internal>
        ///     <para>
        ///         VERY IMPORTANT:
        ///         Always give numerical values to enums.
        ///         Reasoning is that persistance to db (via EF, other)
        ///         as well as WCF to Client Proxies becomes unmutable
        ///         saving you from nasty suprises that are unfixable.
        ///     </para>
        ///     <para>
        ///         IMPORTANT:
        ///         Start enums with an Undefined value. Helps
        ///         trap 'unset' db issues without having to make the
        ///         datatypes nullable.
        ///     </para>
        ///     <para>
        ///         IMPORTANT:
        ///         Namespace of enums is 'Domain', not 'Domain.Enums'
        ///         as the Problem domain is the same (you wouln't make
        ///         a 'Domain.Ints'/'Domain.Strings' folder would you).
        ///         They are just in their own folder for Solution
        ///         Explorer reasons, not logically partitioned (folders
        ///         and namespaces are not 1-1 in all cases).
        ///     </para>
        /// </internal>
        [EnumMember] Unspecified = 0,

        [EnumMember] TypeOne = 1,

        [EnumMember] TypeTwo = 2
    }
}