// For enums, use the same namespace as the entities they describe 

// ReSharper disable CheckNamespace
namespace App.Core.Domain.Entities
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    [DataContract]
    public enum Gender : int
    {
        /// <summary>
        ///     No value specified.
        ///     <para>
        ///         NOTE: This is an error condition (a speficified type is expected).
        ///     </para>
        ///     <para>
        ///         Value = 0
        ///     </para>
        /// </summary>
        [EnumMember]
        Undefined = 0,

        ///// <summary>
        ///// 
        ///// <para>
        ///// An error state.
        ///// </para>
        ///// </summary>
        //[EnumMember]
        //Unrecognized = 1,
        /// <summary>
        ///     A valid db entry.
        /// </summary>
        [EnumMember] Unknown = 3,

        /// <summary>
        ///     The student is known to be a male.
        /// </summary>
        [EnumMember] Male = 1,

        /// <summary>
        ///     The student is known to be a female.
        /// </summary>
        [EnumMember] Female = 2
    }
}