﻿namespace App.Modules.Example.Domain.Entities
{
    using System.Runtime.Serialization;
    using XAct;

    /// <summary>
    /// Example Entity that is associated to <c>Example</c>
    /// via a Many-To-Many Relationship.
    /// </summary>
    [DataContract(Name="ExampleTag")]
    public class ExampleTag: IHasId<int>,IHasEnabled, IHasName
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual int Id { get; set; }


        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="T:XAct.IHasName" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Name { get; set; }


        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="T:XAct.IHasName" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual bool Enabled { get; set; }

        
    }
}
