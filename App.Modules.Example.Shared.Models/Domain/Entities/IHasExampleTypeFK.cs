﻿namespace App.Core.Domain.Entities
{
    using App.Modules.Example.Domain.Entities.Enums;

    public interface IHasExampleTypeFK
    {
        ExampleTypeEnum TypeFK { get; set; }

    }
}