﻿

namespace App.Modules.Example.Constants.Infrastruccture.Diagnostics
{


    public static partial class PerformanceCounters
    {
        public const string Category =
            App.Core.Constants.Infrastructure.Diagnostics.PerformanceCounters.CategoryNamePrefix +
            " Performance Counters";

        //Put here the list of Operations specific to this Module.

        public const string Add = "Add";
        public const string Update = "Update";
        public const string Delete = "Delete";

        public const string SearchById = "SearchById";
        public const string SearchByTerms = "Search";

        public const string SubOperations =
            App.Core.Constants.Infrastructure.Diagnostics.PerformanceCounters.SubOperations;


    }

}
