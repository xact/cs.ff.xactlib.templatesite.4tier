﻿using XAct;

namespace App.Core.Infrastructure.Front.Services.Configuration
{

    /// <summary>
    /// The ConfigureNConfigInitializationStep interface.
    /// <para>
    /// The implementation of this contract will be 
    /// dependency injected into the 
    /// implementation of <see cref="IHostSettingsService"/>.
    /// </para>
    /// </summary>
    public interface IHostSettingsServiceConfiguration  : IHasAppServiceConfiguration, IHasInitialize
    {

    }
}