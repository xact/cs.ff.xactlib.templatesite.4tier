﻿namespace App.Core.Infrastructure.Front.Implementation
{
    using System;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    public class FormsAuthenticationCookieServiceConfiguration : IFormsAuthenticationCookieServiceConfiguration
    {
        private readonly IApplicationSettingsService _applicationSettingsService;
        public TimeSpan SessionTimeOut { get; private set; }

        public FormsAuthenticationCookieServiceConfiguration(IApplicationSettingsService applicationSettingsService)
        {
            _applicationSettingsService = applicationSettingsService;

            SessionTimeOut = _applicationSettingsService.AppSettings.SessionTimeOut;

        }
    }
}