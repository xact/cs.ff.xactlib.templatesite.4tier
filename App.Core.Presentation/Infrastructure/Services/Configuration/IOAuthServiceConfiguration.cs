﻿namespace App.Core.Infrastructure.Front.Services.Configuration
{
    public interface IOAuthServiceConfiguration : IHasAppServiceConfiguration
    {
        string Host { get; }
        string Scope { get;  }
    }
}