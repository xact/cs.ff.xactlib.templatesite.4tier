﻿namespace App.Core.Infrastructure.Front.Services.Configuration
{
    using XAct.Services;

    public class OAuthServiceConfiguration :IOAuthServiceConfiguration
    {
        
        private readonly IHostSettingsService _hostSettingsService;

        public OAuthServiceConfiguration(IHostSettingsService hostSettingsService)
        {
            _hostSettingsService = hostSettingsService;

            int ssoVersion = _hostSettingsService.Get<int>("Services/OAuth/ESAA/Version", 1);

            if (ssoVersion == 1)
            {
                Host = _hostSettingsService.Get<string>("Services/OAuth/ESAA/V1/Endpoint",
                                                        "https://lsi.security.ORG");
                Scope = _hostSettingsService.Get<string>("Services/OAuth/ESAA/V1/Scope", "APP_NSI2");
            }
            else
            {
                Host = _hostSettingsService.Get<string>("Services/OAuth/ESAA/V2/Endpoint",
                                                        "https://lsi.security.ORG");
                Scope = _hostSettingsService.Get<string>("Services/OAuth/ESAA/V2/Scope", "APP_NSI2");
            }
        }
        public string Host { get; private set; }
        public string Scope { get; private set; }
    }
}