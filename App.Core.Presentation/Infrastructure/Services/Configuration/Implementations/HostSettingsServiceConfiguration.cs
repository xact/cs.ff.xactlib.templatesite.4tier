﻿namespace App.Core.Infrastructure.Front.Services.Configuration.Implementations
{
    using XAct.Services;
    using XAct.Settings;

    /// <summary>
    /// Implementation of the 
    /// <see cref="Configuration.IHostSettingsServiceConfiguration"/> contract.
    /// <para>
    /// The implementation is dependency injected into the 
    /// <see cref="Services.IHostSettingsService"/>
    /// teaching it to where to look for the NConfig settings.
    /// </para>
    /// </summary>
    public class HostSettingsServiceConfiguration : Configuration.IHostSettingsServiceConfiguration
    {

        //Needs to be Static so that it can be checked from DbContext.
        //public bool Initialized { get {return {HostSettingsServiceConfiguration.Initialized;} } }


        public bool Initialized { get; private set; }


        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
              if (Initialized)
            {
                return;
            }

            lock (this)
            {
                if (Initialized)
                {
                    return;
                }
                //Do nothing specific.

                //Set flag:
                Initialized = true;


                //	* It undocumented


                IAppSettingsHostSettingsServiceConfiguration x = XAct.DependencyResolver.Current.GetInstance<XAct.Settings.IAppSettingsHostSettingsServiceConfiguration>();

                x.Initialize();
            }

        }

    }
}