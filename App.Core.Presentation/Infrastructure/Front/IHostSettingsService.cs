﻿

namespace App.Core.Infrastructure.Front.Services
{


	/// <summary>
	/// Contract for an app specific service
	/// to return settings relevant to the 
	/// *HOST* (ie the server) that this app
	/// is sitting on.
	/// <para>
	/// Often implemented as a configuration file (eg: app.config)
	/// backed service implemented, in the application's
	/// AppHost (the configuration mechanism varies per environment, 
	/// hence the implementation is in the AppHost), using
	///             <c>XAct.Settings.ApplicationSettings.AppSettings</c>
	/// </para>
	/// <para>
	/// NOTE: Not the same as <see cref="IApplicationSettingsService"/>.
	/// </para>
	/// <para>
	/// Important: although the notion of ApplicationSettings
	///             are umbiqutious, there are several disadvantages
	///             to using ApplicationSettings versus
	///             <c>ProfileSettings</c> (that *can* includ readonly
	///             settings, which is the same as ApplicationSettings).
	///             One of these is that Application Settings
	///             are not serializeable (a separeate mechanism
	///             has to be created to push them to a RIA client)
	///             and are more an old school web server, or WinForm
	///             concept.
	/// </para>
	/// </summary>
	public interface IHostSettingsService
	{

		/// <summary>
		/// Gets the name of the Config File's ConnectionSettingName.
		/// <para>
		/// Default is 'App.Core'
		/// </para>
		/// </summary>
		/// <value>
		/// The name of the db connection string.
		/// </value>
		string DbConnectionSettingName { get; }




		#region Raw -- Use the Compiler, Luke! -- ie: take the time to create Typed Property Wrappers above.

		/// <summary>
		/// Gets the Typed Application setting matching the given key.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam><param name="key">The key.</param><param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
		/// <returns/>
		TValue Get<TValue>(string key, bool throwExceptionOnConversionException = true);

		/// <summary>
		/// Gets the Typed Application setting matching the given key.
		/// <para>
		/// If the result is null, <paramref name="defaultValue"/>.
		/// </para>
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam><param name="key">The key.</param><param name="defaultValue">The default value if no value was found.</param><param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
		/// <returns/>
		TValue Get<TValue>(string key, TValue defaultValue, bool throwExceptionOnConversionException = true);

		#endregion
	}
}
