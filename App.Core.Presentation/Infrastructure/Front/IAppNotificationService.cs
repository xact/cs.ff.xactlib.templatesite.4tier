﻿namespace App.Core.Infrastructure.Front.Services
{
    using XAct;

    public interface IAppNotificationService : IHasAppService, IHasInitialize
    {
        
    }
}
