﻿namespace App.Core.Infrastructure.Front.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    public class CachedCultureSpecificReferenceDataService : ICachedCultureSpecificReferenceDataViewModelService
    {
        private readonly ICachedImmutableReferenceDataMessageService _cachedReferenceDataService;
        private readonly ICultureSpecificResourceService _cultureSpecificResourceService;

        public CachedCultureSpecificReferenceDataService(
            ICachedImmutableReferenceDataMessageService cachedReferenceDataService, 
            ICultureSpecificResourceService cultureSpecificResourceService)
        {
            _cachedReferenceDataService = cachedReferenceDataService;
            _cultureSpecificResourceService = cultureSpecificResourceService;
        }

        //public void GetAllReferenceData(CultureInfo cultureInfo)
        //{
        //    //Get from cache -- or call back tier -- the 
        //    //generic ReferenceData sets.
        //    Dictionary<string,ReferenceData[]> results = 
        //        _cachedReferenceDataService.GetAllReferenceData();

        //    //But the entries won't be translated...

        //}

		public ReferenceDataViewModel[] GetReferenceDataViewModel(string typeName, CultureInfo cultureInfo)
        {

            //Get from cache -- or call back tier -- the 
            //generic ReferenceData set.
			ReferenceDataMessage[] results = _cachedReferenceDataService.GetReferenceData(typeName);

            //But the entries won't be translated...
            //So have to get strings that match:

			Dictionary<string, string> cultureSpecificStrings;

			try
			{
				cultureSpecificStrings = _cultureSpecificResourceService.GetResourceSet(typeName, cultureInfo);
			}
#pragma warning disable 168
			catch (Exception ex)
#pragma warning restore 168
			{
				throw new ApplicationException(string.Format("Unable to load reference data for resource: {0}", typeName));
			}

			List<ReferenceDataViewModel> output = new List<ReferenceDataViewModel>(); 


            //FUTURE:
            //Neat thig is that one can loop through the resources, in order, 
            //and then (in future) add in custom user MRU order.


            //Have to merge the two parts (shared attributes + culture specific resources):
            //Note that we doing this by the Server Order and only returning Enabled:
            foreach (ReferenceDataMessage referenceData in results.Where(d => d.Enabled).OrderBy(d => d.Order))
            {
                ReferenceDataViewModel referenceDataViewModel = new ReferenceDataViewModel();

                referenceDataViewModel.Key = referenceData.Id.ToString();
	            string refDataValue;
                referenceDataViewModel.Text = cultureSpecificStrings.TryGetValue(referenceData.Text, out refDataValue)
					? refDataValue
					: "(Unknown)";

                output.Add(referenceDataViewModel);
            }

            return output.ToArray();
        }
    }
}

