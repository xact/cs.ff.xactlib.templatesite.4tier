namespace App.Core.Infrastructure.Front.Services.Implementations
{
    using App.Core.Infrastructure.Front.Services.Configuration;
    using App.Core.Infrastructure.Services;
    using XAct.Services;
    using VENDOR = XAct.Settings;

    public class HostSettingsService : IHostSettingsService
    {
        private readonly IHostSettingsServiceConfiguration _hostSettingsServiceConfiguration;
        private readonly VENDOR.IHostSettingsService _hostSettingsService;


        /// <summary>
        /// Initializes a new instance of the <see cref="HostSettingsService"/> class.
        /// </summary>
        /// <param name="hostSettingsServiceConfiguration">The host settings service configuration.</param>
        public HostSettingsService(IHostSettingsServiceConfiguration hostSettingsServiceConfiguration)
        {
            //The constructor called it's Initialize method.
            _hostSettingsServiceConfiguration = hostSettingsServiceConfiguration;
        }

        /// <summary>
        /// Gets the name of the Config File's ConnectionSettingName.
        /// <para>
        /// Value is 'App.Core'
        /// </para>
        /// </summary>
        /// <value>
        /// The name of the db connection string.
        /// </value>
        public string DbConnectionSettingName
        {
            get { return "App.Core"; }
        }




        /// <summary>
        /// Initializes a new instance of the app specific <see cref="HostSettingsService" /> class.
        /// <para>
        /// Internally, wraps a vendor provided service.
        /// </para>
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="hostSettingsService">The host settings service.</param>
        public HostSettingsService(ITracingService tracingService, VENDOR.IHostSettingsService hostSettingsService)
        {
            _hostSettingsService = hostSettingsService;
        }

        public TValue Get<TValue>(string key, bool throwExceptionOnConversionException = true)
        {
            return _hostSettingsService.Get<TValue>(key, throwExceptionOnConversionException);
        }

        public TValue Get<TValue>(string key, TValue defaultValue, bool throwExceptionOnConversionException = true)
        {
            return _hostSettingsService.Get(key, defaultValue, throwExceptionOnConversionException);
        }

    }
}