﻿namespace App.Core.Infrastructure.Front.Services.Implementations
{
    using System;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    /// <summary>
    /// Implementation of the 
    /// <see cref="ICachedWcfServiceAgentService"/>
    /// to return results cached results of 
    /// previous WCF requests. 
    /// </summary>
    public class CachedWcfServiceAgentService : ICachedWcfServiceAgentService
    {
        private readonly ICachingService _cachingService;
        private readonly IWcfServiceAgentService _wcfServiceAgentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedWcfServiceAgentService"/> class.
        /// </summary>
        /// <param name="applicationSettingsService">The application settings service.</param>
        /// <param name="cachingService">The caching service.</param>
        /// <param name="wcfServiceAgentService">The WCF service agent service.</param>
        public CachedWcfServiceAgentService(
            ICachingService cachingService, 
            IWcfServiceAgentService wcfServiceAgentService)
        {
            _cachingService = cachingService;
            _wcfServiceAgentService = wcfServiceAgentService;
        }

        /// <summary>
        /// Checks for a cached value, and if not found/stale
        /// Connects to the specified service, executes the remote Operation,
        /// caching the result before returning it.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="func">The function to be executed on the service</param>
        /// <param name="cacheKey">The cache key.</param>
        /// <param name="cacheTimeSpan">The cache time span.</param>
        /// <returns></returns>
        public TResult Query<TService, TResult>(Func<TService, TResult> func, string cacheKey, TimeSpan? cacheTimeSpan = null, bool bypassCaching=false, bool forceUpdateOfCache=false)
        {
            if (!cacheTimeSpan.HasValue)
            {
                cacheTimeSpan = XAct.Library.Settings.Caching.DefaultReferenceCachingTimeSpan;
                    //_applicationSettingsService.AppSettings.ReferenceCacheTimeSpan;
            }

            TResult results;

            //Try getting it out of the cache, and if not possible,
            //define how to get the results from across the wire:
            if (bypassCaching)
            {
                return _wcfServiceAgentService.Query(func);
            }
            
            if (forceUpdateOfCache)
            {
                _cachingService.Remove(cacheKey);
            }

            _cachingService.TryGet(
                cacheKey, 
                out results,
                () => _wcfServiceAgentService.Query(func), 
                cacheTimeSpan.Value, 
                true);

            return results;

        }

    }
}