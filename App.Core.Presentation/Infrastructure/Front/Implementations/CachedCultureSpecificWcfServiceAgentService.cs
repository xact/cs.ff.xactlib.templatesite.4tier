﻿namespace App.Core.Infrastructure.Front.Services.Implementations
{
    using System;
    using System.Globalization;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    /// <summary>
    /// An implementation of the
    /// <see cref="ICachedCultureSpecificWcfServiceAgentService"/>
    /// Service to return results cached 
    /// or 
    /// retrieve fresh values from a remote 
    /// WCF Service.
    /// </summary>
    public class CachedCultureSpecificWcfServiceAgentService : ICachedCultureSpecificWcfServiceAgentService
    {
        private readonly IApplicationSettingsService _applicationSettingsService;
        private readonly ICultureSpecificCachingService _cultureSpecificCachingService;
        private readonly IClientEnvironmentService _clientEnvironmentService;
        private readonly IWcfServiceAgentService _wcfServiceAgentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedCultureSpecificWcfServiceAgentService"/> class.
        /// </summary>
        /// <param name="applicationSettingsService">The application settings service.</param>
        /// <param name="cultureSpecificCachingService">The culture specific caching service.</param>
        /// <param name="clientEnvironmentService">The client environment service.</param>
        /// <param name="wcfServiceAgentService">The WCF service agent service.</param>
        public CachedCultureSpecificWcfServiceAgentService(
            IApplicationSettingsService applicationSettingsService, 
            
            ICultureSpecificCachingService cultureSpecificCachingService, 
            IClientEnvironmentService clientEnvironmentService, 
            IWcfServiceAgentService wcfServiceAgentService)
        {
            _applicationSettingsService = applicationSettingsService;
            _cultureSpecificCachingService = cultureSpecificCachingService;
            _clientEnvironmentService = clientEnvironmentService;
            _wcfServiceAgentService = wcfServiceAgentService;
        }

        public TResult Query<TService, TResult>(Func<TService, TResult> func, string cacheKey, CultureInfo cultureInfo = null, TimeSpan? cacheTimeSpan=null)
        {
            if (cultureInfo == null)
            {
                cultureInfo = _clientEnvironmentService.ClientUICulture;
            }

            if (!cacheTimeSpan.HasValue)
            {
                cacheTimeSpan = _applicationSettingsService.AppSettings.ReferenceCacheTimeSpan;
            }

            TResult results;

            //Try getting it out of the cache, and if not possible,
            //define how to get the results from across the wire:
            bool result = 
                _cultureSpecificCachingService.TryGet(
                cultureInfo, 
                cacheKey, 
                out results,
// ReSharper disable RedundantArgumentDefaultValue
                () => _wcfServiceAgentService.Query(func), cacheTimeSpan.Value, true);
// ReSharper restore RedundantArgumentDefaultValue

            return results;

        }

    }
}