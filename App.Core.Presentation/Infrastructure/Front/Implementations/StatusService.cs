﻿//using System;

//namespace App.Core.Infrastructure.Front.Services
//{
//    using XAct;
//    using XAct.Diagnostics.Services.Implementations;
//    using XAct.Services;

//    [DefaultBindingImplementation(typeof(IStatusService),BindingLifetimeType.SingletonScope,Priority.Low)]
//    public class StatusService : IStatusService
//    {
//        private readonly IWcfServiceAgentService _wcfServiceAgentService;

//        public StatusService(IWcfServiceAgentService wcfServiceAgentService)
//        {
//            _wcfServiceAgentService = wcfServiceAgentService;
//        }

//        public virtual StatusResponse[] Get(string[] names, DateTime? startDateTimeUtc = null, DateTime? endDateTimeUtc = null)
//        {
//            //Invoke the facade:
//            return _statusService.Get(names, startDateTimeUtc, endDateTimeUtc);
//        }

//        public virtual StatusResponse Get(string name, object arguments = null, DateTime? startDateTimeUtc = null,
//                                  DateTime? endDateTimeUtc = null)
//        {
//            //Invoke the facade:
//            return _statusService.Get(name, arguments, startDateTimeUtc, endDateTimeUtc);
//        }

//    }

//}
