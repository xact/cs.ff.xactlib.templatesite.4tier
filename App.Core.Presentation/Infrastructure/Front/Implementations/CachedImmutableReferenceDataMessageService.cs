﻿namespace App.Core.Infrastructure.Front.Services
{
    using System.Collections.Generic;
    using App.Core.Application.Services.Facades;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    public class CachedImmutableReferenceDataMessageService : ICachedImmutableReferenceDataMessageService
    {
        private readonly ICachedWcfServiceAgentService _cachedWcfServiceAgentService;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="CachedImmutableReferenceDataMessageService"/> class.
        /// </summary>
        /// <param name="cachedWcfServiceAgentService">The cached WCF service agent service.</param>
        public CachedImmutableReferenceDataMessageService(ICachedWcfServiceAgentService cachedWcfServiceAgentService)
        {
            _cachedWcfServiceAgentService = cachedWcfServiceAgentService;
        }

        public Dictionary<string,ReferenceDataMessage[]> GetAllReferenceData()
        {
            throw new System.NotImplementedException();
        }

        public ReferenceDataMessage[] GetReferenceData(string typeName, bool byPassCache=false, bool resetCache=false)
        {
            //Use same cache key as in back:
            string cacheKey = Constants.Caching.CacheKeys.ReferenceDataPrefix + typeName;

            ReferenceDataMessage[] results =

            _cachedWcfServiceAgentService
                    .Query<IReferenceDataServiceFacade, ReferenceDataMessage[]>(
                        s => s.GetReferenceData(typeName),
                        cacheKey,
                        // ReSharper disable RedundantArgumentDefaultValue
                        null, //Leave it as null, so that underlying service sets it to app specific default time.,
                        byPassCache,
                        resetCache
                    );
                // ReSharper restore RedundantArgumentDefaultValue
            

            return results;
        }
    }
}
