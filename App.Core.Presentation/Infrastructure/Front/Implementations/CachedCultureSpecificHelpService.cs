﻿namespace App.Core.Infrastructure.Front.Services.Implementations
{
    using System.Globalization;
    using App.Core.Application.Services.Facades;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    /// <summary>
    /// Implementation of the 
    /// <see cref="IHelpService"/>
    /// to retrieve hierarchical help entries.
    /// <para>
    /// The help entries are translated per 
    /// the specified CultureInfo,
    /// or fall back to the value retrieved
    /// from 
    /// <see cref="IClientEnvironmentService"/>
    /// </para>
    /// </summary>
    public class CachedCultureSpecificHelpService : IHelpService
    {
        private readonly ICachedCultureSpecificWcfServiceAgentService _cultureSpecificCachedWcfServiceAgentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedHelpService"/> class.
        /// </summary>
        /// <param name="cultureSpecificCachedWcfServiceAgentService">The culture specific cached WCF service agent service.</param>
        public CachedCultureSpecificHelpService(ICachedCultureSpecificWcfServiceAgentService cultureSpecificCachedWcfServiceAgentService)
        {
            _cultureSpecificCachedWcfServiceAgentService = cultureSpecificCachedWcfServiceAgentService;
        }

        /// <summary>
        /// Gets the specified help entry
        /// </summary>
        /// <param name="helpEntryKey"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public HelpEntrySummary GetHelpEntryByKey(string helpEntryKey, CultureInfo cultureInfo)
        {

            HelpEntrySummary helpEntrySummary =
                _cultureSpecificCachedWcfServiceAgentService
                    .Query<IHelpServiceFacade, HelpEntrySummary>(
                        service => service.GetByKey(helpEntryKey, cultureInfo.Name),
                        helpEntryKey,
                        cultureInfo);

            return helpEntrySummary;
        }



    }
}