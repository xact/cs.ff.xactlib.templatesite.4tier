﻿
/*
In an N-Tier Application, the Front Tier contains a Presentation service
that uses Service Clients of the Back Tier.
In a 3-Tier Application, the Presentation layer is in the same Tier as Infrastructure and Domain Services
and uses them directly.
*/

namespace App.Core.Infrastructure.Front.Services.Implementations
{
    using App.Core.Application.Services.Facades;
    using App.Core.Infrastructure;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    /// <summary>
    /// Implementation of the 
    /// <see cref="IApplicationSettingsService"/>
    /// Contract for a service to return
    /// Tier specific application wide settings.
    /// </summary>
    public class CachedApplicationSettingsService : IApplicationSettingsService
    {
        private readonly ICachingService _cachingService;
        private readonly IWcfServiceAgentService _wcfServiceAgentService;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="CachedApplicationSettingsService" /> class.
        /// </summary>
        public CachedApplicationSettingsService(ICachingService cachingService, IWcfServiceAgentService wcfServiceAgentService):
            base()
        {
            _cachingService = cachingService;
            _wcfServiceAgentService = wcfServiceAgentService;
        }

        /// <summary>
        /// Get an object containing all the
        /// Tier specific Application Settings.
        /// </summary>
        public AppSettings AppSettings
        {

            get
            {

                AppSettings appSettings;
                
                //IMPORTANT:
                //In most cass we can use ICachedWcfServiceAgentSevice, 
                //but in this specific case we have to do it explicitly, 
                //rather than use ICachedWcfServiceAgentService
                //as ICachedWcfServiceAgentService has reference 
                //on  IApplicationSettingsService which will cause 
                //a stack overflow trying to resolve.



                _cachingService.TryGet(
                    Constants.Caching.CacheKeys.AppSettingsKey,
                    out appSettings,
                    () => _wcfServiceAgentService.Query<ISettingServiceFacade, AppSettings>(s => s.Retrieve()),
                    XAct.Library.Settings.Caching.DefaultShortCachingTimeSpan
                    );

                return appSettings;
            }
        }




        public void Persist()
        {
            throw new System.NotImplementedException();
        }
    }
}

