﻿# if NTIER

/*
In an N-Tier Application, the Front Tier contains a Presentation service
that uses Service Clients of the Back Tier.
In a 3-Tier Application, the Presentation layer is in the same Tier as Infrastructure and Domain Services
and uses them directly.
*/

namespace App.Core.Infrastructure.Front.Services.Implementations
{
    using App.Core.Infrastructure.Services;
    using Microsoft.AspNet.SignalR.Client;
    using XAct.Services;

    public class AppNotificationService : IAppNotificationService
    {
        private readonly ICachingService _cachingService;
        private IHubProxy _appMessageHubProxy;


        public AppNotificationService(ICachingService cachingService)
        {
            _cachingService = cachingService;
        }
      
        public bool Initialized { get; private set; }

        public void Initialize()
        {

            if (Initialized)
            {
                return;
            }
            lock (this)
            {
                if (Initialized)
                {
                    return;
                }
                CreateHubProxy();
                Initialized = true;
            }
            CreateHubProxy();
        }

        private /*async*/ void CreateHubProxy()
        {
            HubConnection hubConnection = new HubConnection("http://localhost:53228/");


            //Create a proxy hub of the remote Hub. Specify the hubname given in the 
            //HubNameAttribute (eg: [HubName("myChatHub")]) over the Hub
            _appMessageHubProxy = hubConnection.CreateHubProxy("appMessageHub");

            //Wire up to the message using the js specific signature
            //which in this case is broadcastMessage(sender, message)
            _appMessageHubProxy.On<string, string>(
                "broadcastMessage",
                ProcessMessage)
                ;

            /*await*/
            hubConnection.Start();
        }

        private void ProcessMessage(string sender, string message)
        {
            if (message == "CLEAR CACHE")
            {
                _cachingService.Clear();
            }
        }


    }
}
#endif
