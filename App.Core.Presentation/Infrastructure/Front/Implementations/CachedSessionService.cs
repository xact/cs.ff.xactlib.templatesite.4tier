﻿//namespace App.Core.Infrastructure.Front.Services.Implementations
//{
//    using System;
//    using System.Diagnostics;
//    using System.Linq;
//    using System.Security.Claims;
//    using System.Security.Principal;
//    using App.Core.Application.Services.Facades;
//    using App.Core.Constants.Domain;
//    using App.Core.Domain.Extensions;
//    using App.Core.Domain.Messages;
//    using App.Core.Domain.Models;
//    using App.Core.Domain.Security;
//    using App.Core.Infrastructure.Services;
//    using XAct;
//    using XAct.Messages;
//    using XAct.Services;

//    //using dk.nita.saml20.identity;

//    /// <summary>
//    /// An implementation of the 
//    /// <see cref="CachedSessionService"/> contract
//    /// to create and cache new and existing Sessions 
//    /// (and the AppIdentity they contain), 
//    /// and update their associated SessionLog records.
//    /// </summary>
//    [DefaultBindingImplementation(typeof (ICachedSessionService))]
//    public class CachedSessionService : ICachedSessionService
//    {
//        private readonly ICachingService _cachingService;
//        private readonly ITracingService _tracingService;
//        private readonly IPrincipalService _principalService;
//        private readonly IWcfServiceAgentService _serviceAgent;
//        private readonly IApplicationSettingsService _applicationSettingsService;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="CachedSessionService" /> class.
//        /// </summary>
//        /// <param name="cachingService">The caching service.</param>
//        /// <param name="tracingService">The tracing service.</param>
//        /// <param name="principalService">The principal service.</param>
//        /// <param name="serviceAgentService">The service agent.</param>
//        /// <param name="applicationSettingsService">The settings service.</param>
//        public CachedSessionService
//            (

//            ICachingService cachingService,
//            ITracingService tracingService,
//            IPrincipalService principalService,
//            IWcfServiceAgentService serviceAgentService,
//            IApplicationSettingsService applicationSettingsService
//            )
//        {
//            _cachingService = cachingService;
//            _tracingService = tracingService;
//            _principalService = principalService;
//            _serviceAgent = serviceAgentService;
//            _applicationSettingsService = applicationSettingsService;
//        }

//        /// <summary>
//        /// 
//        /// <para>
//        /// NOTE: Invoked from M2m2CachedSessionService.
//        /// </para>
//        /// </summary>
//        /// <param name="signInInfo"></param>
//        /// <param name="name"></param>
//        /// <param name="userDetails"></param>
//        /// <param name="privileges"></param>
//        /// <returns></returns>
//        public Response<Guid?> CreateNewSessionAndCacheIdentity(SignInInfo signInInfo,
//                                                                string name,
//                                                                UserDetails userDetails, string[] privileges)
//        {


//            AppIdentity appIdentity = CreateAppIdentityFromNameUserDetailsAndPermissions(name, userDetails, privileges);

//            //Time to get on with the real stuff:
//            //appIdentity = CreateAppIdentity(name, userDetails, privileges);

//            return CreateNewSessionAndCacheIdentity(signInInfo, appIdentity,true);

//        }

//        /// <summary>
//        /// Create a new <see cref="AppIdentity" /> after SignOn.
//        /// <para>
//        /// Method invoked by Saml Handler when done processing response from SSO Authority.
//        /// </para>
//        /// </summary>
//        /// <param name="signInInfo">The sign in info.</param>
//        /// <param name="srcIdentity">The original identity (probably a FormsIdentity).</param>
//        /// <param name="userDetails">Property bag that describes the user (filled in from info returned from SSO).</param>
//        /// <param name="privileges">The roles.</param>
//        /// <returns>
//        /// Session Guid
//        /// </returns>
//        public
//            Response<Guid?> CreateNewSessionAndCacheIdentity(SignInInfo signInInfo, IIdentity srcIdentity,
//                                                             UserDetails userDetails, string[] privileges)
//        {
//            //Just received the Identity from our custom SAML action,
//            //which is processing the response from the SSO authority,
//            //but has not yet created a Cookie.

//            //It's time for us to create a new AppIdentity from the Received Identity, 
//            //create a session for it,
//            //and return the Session information, which is what will be embedded in the cookie.


//            //Time to get on with the real stuff:
//            AppIdentity appIdentity = CreateAppIdentityFromNameUserDetailsAndPermissions(userDetails.UserId /*srcIdentity.Name*/, userDetails, privileges);

//            //Optional step:
//            EmbedSourceIdentityIntoAppIdentity(srcIdentity, appIdentity);

            
//            return CreateNewSessionAndCacheIdentity(signInInfo, appIdentity,true);
//        }





//        private Response<Guid?> CreateNewSessionAndCacheIdentity(SignInInfo signInInfo, AppIdentity appIdentity, bool addToThread)
//        {
//            try
//            {

//                //The SignInInfo will be used to populate the SessionLog Entity. 
//                //It will need the name the Identity. 
//                //Although not strictly germain to the process of signing on, and therefore maybe should
//                //not be a property of this Message, it's easier to do it this way
//                //rather than force the Repository to deserialize the Identity, just to get the name.
//                signInInfo.UserName = appIdentity.Name;

//                //Name create a new Session object, save it to the Db, Cache it in the Back Tier, then then Front tier,
//                //then return just the SessionId, which is all we want in order to 
//                Response<Guid?> sessionIdResponse =
//                    this.CreateNewSession(appIdentity, signInInfo, addToThread);

//                if (sessionIdResponse.Success)
//                {
//                    //If we got here, the session id is already set as a property 
//                    //of the appIdentity.
//                    Debug.Assert(appIdentity.SessionToken != null);
//                }

//                //Let's return the whole thing, with response messages, etc.
//                return sessionIdResponse;

//            }
//            catch (Exception e)
//            {
//                throw new ApplicationException(
//                    string.Format("AccountService - Unable to create new identity for {0}", appIdentity.UserId), e);
//            }
//        }




//        private static void EmbedSourceIdentityIntoAppIdentity(IIdentity srcIdentity, AppIdentity appIdentity)
//        {
//            //Embed SAML based Identity in current identity
//            //This is just a precaution at this point, so we can always reaccess the same identity (and its claims)
//            //that were given to us when we first signed in).

//            //But...we can't embed the type directly or it will fail deserialization
//            //in a lower tier that does not have a ref to this identity.
//            //So encode it in base64 -- we can always later decode it if we need to.
//            Type t = srcIdentity.GetType();
//            SerializationMethod serializationMethod = SerializationMethod.Base64Binary;
//            appIdentity.SetInnerObject(t.Serialize(srcIdentity, ref serializationMethod));
//        }


//        public Response<bool> UpdateSessionAndSetOrClearThreadPrincipal(Guid? sessionId)
//        {


//            //At this point, we should have a valid Forms Authentication Cookie
//            //We should have a SessionId, and therefore
//            //can access a valid Session and therefore AppIdentity

//            //BUT... as the cookie is not a good indication of whether 
//            //timed out (and it's a security concern to rely on anything
//            //been sent to a useragent) we check whether the Db Session record 
//            //is expired:

//            Response<bool> r = new Response<bool>();

//            //bool sessionUpdated = false;
//            AppIdentity appIdentity = null;

//            if (sessionId.HasValue)
//            {
//                //Will return null AppIdentity if it has expired.
//                Response<AppIdentity> appIdentityResponse =
//                    this.RetrieveSessionIdentity(sessionId.Value);


//                appIdentityResponse.TransferProperties(r);

//                r.Data = appIdentityResponse.Success;

//                if (r.Data)
//                {
//                    appIdentity = appIdentityResponse.Data;
//                }
//            }


//            //Now set, or clear, thread principal:
//            IPrincipal principalToAttach;
//            if (!r.Data)
//            {
//                //There's a chance life is not good, and the Db session has exired...
//                //IIdentity identity = appIdentity ?? new GenericIdentity("ExpiredOnDb");
//                principalToAttach = null;
//            }
//            else
//            {

//                //Embed the SessionId in the appIdentity.
//                //We do this so that the SessionId can travel within
//                //an easy "container", that the SecurityClientMessageInspector
//                //can retrieve easily (via EnvironmentService) and get the SessionId
//                //from it.  Just convenience really. Otherwise we would have to 
//                //hold the sessionId in Request state. Just less things to deal with.
//                appIdentity.SessionToken = sessionId.Value;

//                principalToAttach = appIdentity.CreatePrincipal();
//            }

//            //Set the Current User on the Context (not the thread directly, which is a Non-WebApp Concept)
//            //so that Context switching happens correctly, without leaving 
//            //behind ghost Users, etc. on the wrong thread:

//            //Not also that we set the principal whether it is null or not.
//            //In other words, if we have a FormsIdentity, tried to retrieve a Session
//            //but it was timed out, we've cleared it out so thread has no identity.
//            //Otherwise, we've replaced it with an app specific identity.
//            _principalService.Principal = principalToAttach;

//            return r;
//        }








//        /// <summary>
//        /// Creates an application <see cref="RetrieveSessionResponse" />.
//        /// Embeds the <see cref="AppIdentity" /> in the <see cref="RetrieveSessionResponse" /> object.
//        /// The <see cref="RetrieveSessionResponse" /> is persisted in the database, and cached in both tiers.
//        /// </summary>
//        /// <param name="appIdentity">The appIdentity.</param>
//        /// <param name="signInInfo">The session log.</param>
//        /// <returns>Session Id</returns>
//        /// <exception cref="System.ArgumentNullException">appIdentity</exception>
//        private Response<Guid?> CreateNewSession(AppIdentity appIdentity, SignInInfo signInInfo, bool addToThread)
//        {
//            if (appIdentity == null) throw new ArgumentNullException("appIdentity");
//            if (signInInfo == null) throw new ArgumentNullException("signInInfo");

//            Response<Guid?> guidResponse = new Response<Guid?>();
//            try
//            {

//                string identityBase64 = appIdentity.SerializeToBase64();

//                //Retrieve from the back tier the newly created Session object:                
//                Response<SessionResponse> sessionResponseResponse =
//                    _serviceAgent.Query<ISessionServiceFacade, Response<SessionResponse>>(
//                        service => service.CreateSession(identityBase64, signInInfo));

//                //TODO : enable when migration of provider live dates is sorted
//                //if (response.HasMessage) throw new Exception(string.Format("Security Error {0}", response.Message)); //temporary solution

//                if (!sessionResponseResponse.Success)
//                {
//                    //Failed:
//                    ProcessApplicationMessages(sessionResponseResponse);

//                    //transfer messages from one response to another:
//                    sessionResponseResponse.TransferProperties(guidResponse);

//                    //Return null, indicating there is no Guid
//                    //(avoid ambiguity of Guid.Empty)
//                    guidResponse.Data = null;

//                    //Get out early, without the session id:
//                    return guidResponse;
//                }


//                SessionResponse sessionResponse = sessionResponseResponse.Data;


//                Session session = sessionResponse.Session;

//                //When we created the AppIdentity to send down
//                //it didn't have the SessionId.
//                //Set the identity directly into the appIdentity.
//                //This is so that the Inspector can easily find the Guid to attach to WCF calls
//                //to the Back Tier.
//                appIdentity.SessionToken = session.Id;

//                //And cache the result so that in future we don't have 
//                //to cross the tiers when we want to convert SessionId (from Cookie)
//                //back to an AppIdentity:
//                string cacheKey = session.CreateCacheIdentifier();

//                appIdentity.OrganisationSummary = sessionResponse.OrganisationSummary;
                
//                this._cachingService.Set(cacheKey, session, _applicationSettingsService.AppSettings.SessionTimeOut);

//                if (addToThread)
//                {
//                    var principalToAttach = appIdentity.CreatePrincipal();
//                    _principalService.Principal = principalToAttach;
//                }


//                //There are probably no use cases where anyone needs the whole Session
//                //-- just the AppIdentity -- so return nothing more than the Id of the 
//                //newly created Session:
//                guidResponse.Data = session.Id;


//                return guidResponse;
//            }
//            catch (Exception ex)
//            {

//                //Unexpected behaviour:

//                guidResponse.Data = null;

//                this._tracingService.TraceException(TraceLevel.Error, ex, "Unable to create session for user");
//                throw;
//            }
//        }

//        // An early concept on passing messages from Back to Front.
//        // Surpassed by using SignalR for this part.
//        private void ProcessApplicationMessages(Response<SessionResponse> response)
//        {
//            if (response.Messages.Count > 0)
//            {
//                foreach (Message message in response.Messages)
//                {
//                    if (message.MessageCode.Id == Constants.Notifications.MessageIds.InstructionId)
//                    {
//                        //TODO:
//                        //if (message.Description.StartsWith("CACHE:CLEAR"))
//                        //{
//                        //    _cachingService.Remove(message.Description);
//                        //}
//                    }
//                }
//            }
//        }

//        /// <summary>
//        /// Retrieves the cached <see cref="RetrieveSessionResponse" /> <see cref="AppIdentity" />.
//        /// <para>
//        /// An Exception is raised if the SessionToken is empty.
//        /// </para>
//        /// <para>
//        /// Returns null if Session expired.
//        /// </para>
//        /// </summary>
//        /// <param name="sessionToken">The session token.</param>
//        /// <returns>
//        ///   <see cref="AppIdentity" />
//        /// </returns>
//        public Response<AppIdentity> RetrieveSessionIdentity(Guid sessionToken)
//        {
//            if (sessionToken == Guid.Empty)
//            {
//                throw new ArgumentException("sessionToken");
//            }

//            //Call private method to Retrieve Session, or Null if Expired
//            //with reasons as to why expired (either due to passing 20 minutes, or 8 hours).
//            Response<SessionResponse> sessionResponse = this.UpdateSessionTimeoutAndRetrieveSession(sessionToken);

//            Response<AppIdentity> appIdentityResponse = new Response<AppIdentity>();

//            if (!sessionResponse.Success)
//            {
//                //Expired.
//                //Can't get an AppIdentity from something that is expired.
//                //So transfer 528  (session over 20 mins)  or 530 (session over 8 hours)
//                sessionResponse.TransferProperties(appIdentityResponse);

//                return appIdentityResponse;
//            }


//            appIdentityResponse.Data = AppIdentity.DeserializeFromBase64(sessionResponse.Data.Session.SerializedIdentity);

//            return appIdentityResponse;
//        }

//        /// <summary>
//        /// Tries to get the session from cache. 
//        /// If its not in the cache, get's it from the database. 
//        /// In both cases updates the session's last access time
//        /// on the database.
//        /// <para>
//        /// Returns Null if Session is Expired.
//        /// </para>
//        /// </summary>
//        /// <param name="sessionToken">The session token.</param>
//        /// <returns></returns>
//        private Response<SessionResponse> UpdateSessionTimeoutAndRetrieveSession(Guid sessionToken)
//        {
//            if (sessionToken == Guid.Empty)
//            {
//                throw new ArgumentException("sessionToken");
//            }

//            Session session;

//            //First check in the Cache (we're always going to go across the wire
//            //but we can check whether we want to retrieve the session, or not bother,
//            //saving a little time.
//            this._cachingService.TryGet(sessionToken.CreateCacheIdentitfier(), out session);

//            //If session in front tier cache, no need to 
//            //request it from the back tier -- just have to 
//            //update it's LastAccessedDate there, and leave it there.
//            bool sessionInCache = session != null;

//            //Whether it is null or not,
//            //creae a response object:
//            Response<SessionResponse> sessionResponse = null;



//            //Invoke the service.
//            //Should always return a Response, 
//            //with or without a Session as payload.
//            //The Session object, the very first time (as it is not cached in this 
//            //front tier) will have an AppIdentity payload, that the Session method
//            //will have already deserialized.
//            this._serviceAgent.Query<ISessionServiceFacade>
//                (
//                    service =>
//                        {
//                            //Invoke Session method on that
//                            sessionResponse = RetrieveSessionResponse(
//                                sessionToken,
//                                service,
//                                sessionInCache, ref session);
//                        }
//                );

//            return sessionResponse;
//        }

//        private Response<SessionResponse> RetrieveSessionResponse(Guid sessionToken, ISessionServiceFacade service,
//                                                          bool sessionInCache, ref Session session)
//        {
//            //This is the first call to *this* front server (as the session Token is non-null, 
//            //the user must have already signed in and created a Session record, using a different Front server)

//            //Update the Session Time, requesting the Session to be returned:
//            Response<SessionResponse> sessionResponse = service.UpdateSessionTime(sessionToken, !sessionInCache);

//            if (sessionResponse == null)
//            {
//                throw new ArgumentException();
//            }

//            if (!sessionResponse.Success)
//            {

//                if ((sessionResponse.Messages.Any(m =>
//                                                  new[]
//                                                      {
//                                                          MessageCodes.Security_SessionTimedOutNotEstablished_528,
//                                                          MessageCodes.Error_SessionTooLong_530
//                                                      }.Contains(m.MessageCode))))
//                {
//                    //It timed out...that's it...
//                    //Clear cache:
//                    if (!sessionInCache)
//                    {
//                        this._cachingService.Remove(sessionToken.CreateCacheIdentitfier());
//                    }
//                }
//                return sessionResponse;
//            }

//            //At this point, we know the request for an existing Session
//            //was successful.

//            if (!sessionInCache)
//            {
//                //As it was not in this front tier (ie, first call)
//                //we cache the session in this front tier:
//                session = sessionResponse.Data.Session;

//                this._cachingService.Set(
//                    sessionToken.CreateCacheIdentitfier(),
//                    session,
//                    _applicationSettingsService.AppSettings.SessionTimeOut);
//            }
//            else
//            {
//                sessionResponse.Data.Session = session;
//            }

//            //return true:
//            return sessionResponse;
//        }

//        /// <summary>
//        /// Deletes the session with the specified session token from the database and caches in both tiers.
//        /// <para>
//        /// An Exception is raised if the SessionToken is empty.
//        /// </para>
//        /// </summary>
//        /// <param name="sessionToken">The session token.</param>
//        public bool DeleteSessionAndClearThreadIdentity(Guid? sessionToken)
//        {
//            bool sessionExists = false;
//            if ((sessionToken.HasValue) && (sessionToken.Value != Guid.Empty))
//            {
//                this._serviceAgent.Query<ISessionServiceFacade>(service => sessionExists = service.DoesSessionExist(sessionToken.Value));

				
//                //Delete from back tier and Db first:
//                this._serviceAgent.Query<ISessionServiceContract>(service => service.DeleteSession(sessionToken.Value));

//                //Finish up by deleting in this tier:
//                this._cachingService.Remove(sessionToken.Value.CreateCacheIdentitfier());
//                //Whether session record cleared or not, we can clean up the thread.
//                _principalService.Principal = null;
		        
//            }
//            return sessionExists;
//        }

//        public static AppIdentity CreateAppIdentityFromNameUserDetailsAndPermissions(string name, UserDetails userDetails,
//                                                                                 string[] privileges)
//        {
//            //Create new AppIdentity:
//            AppIdentity appIdentity = new AppIdentity(name);

//            if (privileges == null || privileges.Length == 0)
//            {
//                // There are no user roles without permissions specified, so this is a possible worry.
//                // Covers: If ESAA/LSI bums out and gives us nothing (is misconfigured)
//                try
//                {
//                    XAct.DependencyResolver.Current.GetInstance<ITracingService>()
//                        .Trace(TraceLevel.Info, string.Format("No permissions received for user '{0}'. Possible error/misconfiguration with ESAA", name));
//                }
//                catch
//                {
                    
//                }
//            }
//            else
//            {
//                foreach (string permission in privileges)
//                {
//                    appIdentity.AddClaim(new Claim(App.Core.Constants.Security.ClaimTypes.Privilege, permission));
//                }
//                appIdentity.AddClaim(new Claim(Constants.Security.ClaimTypes.Privilege, privileges.JoinSafely(",")));
//            }

//            appIdentity.AddClaim(new Claim(Constants.Security.ClaimTypes.FullName, userDetails.FullDisplayName));
//            appIdentity.AddClaim(new Claim(Constants.Security.ClaimTypes.UserId, userDetails.UserId));

//            appIdentity.AddClaim(new Claim(Constants.Security.ClaimTypes.OrgId, userDetails.OrgId));
//            try
//            {
//                appIdentity.AddClaim(new Claim(Constants.Security.ClaimTypes.OrgName, userDetails.OrgName ?? string.Empty));
//            }
//            catch
//            {
//                //doesn't like it if setting to null.
//            }



//            //claim.Add(new Claim(Constants.Security.ClaimTypes.FullName, Constants.Security.M2M.M2MFullName));

//            return appIdentity;
//        }

//        public void IsStillAlive()
//        {
//            this._serviceAgent.Query<ISessionServiceFacade>(service => service.IsStillAlive());
//        }



//    }
//}