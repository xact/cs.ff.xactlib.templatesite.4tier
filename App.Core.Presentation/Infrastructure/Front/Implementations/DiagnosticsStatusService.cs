﻿using System;

namespace App.Core.Infrastructure.Front.Services
{
    using App.Core.Application.Services.Facades;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Services;


    /// <summary>
    /// Implementation of the FrontTier's status service.
    /// <para>
    /// This is a presentation specific override of the default infrastructure implementation.
    /// It uses the <see cref="IWcfServiceAgentService"/> to communicate with the back tier.
    /// </para>
    /// </summary>
    /// <internal>
    /// Note that binding is hig
    /// </internal>
    [DefaultBindingImplementation(typeof(IDiagnosticsStatusService),BindingLifetimeType.SingletonScope,Priority.Normal)]
    public class DiagnosticsStatusService : IDiagnosticsStatusService
    {
        private readonly IWcfServiceAgentService _wcfServiceAgentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DiagnosticsStatusService"/> class.
        /// </summary>
        /// <param name="wcfServiceAgentService">The WCF service agent service.</param>
        public DiagnosticsStatusService(IWcfServiceAgentService wcfServiceAgentService)
        {
            _wcfServiceAgentService = wcfServiceAgentService;
        }


        public virtual string Ping()
        {
            return _wcfServiceAgentService.Query<IDiagnosticsServiceFacade, string>(service => service.Ping());
        }

        public virtual StatusResponse[] Get(string[] names, DateTime? startDateTimeUtc = null, DateTime? endDateTimeUtc = null)
        {
            var results = _wcfServiceAgentService.Query<IDiagnosticsServiceFacade,StatusResponse[]>(service => service.GetSet(names,startDateTimeUtc,endDateTimeUtc));

            return results;
        }

        public virtual StatusResponse Get(string name, object arguments = null, DateTime? startDateTimeUtc = null,
                                  DateTime? endDateTimeUtc = null)
        {
            var results = _wcfServiceAgentService.Query<IDiagnosticsServiceFacade, StatusResponse>(service => service.GetSingle(name, startDateTimeUtc, endDateTimeUtc));

            return results;
        }

    }

}
