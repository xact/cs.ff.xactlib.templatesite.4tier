﻿namespace App.Core.Infrastructure.Front.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using App.Core.Application.Services.Facades;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Services;

    /// <summary>
    /// Implementation of the
    /// <see cref="ICultureSpecificResourceService"/> contract
    /// to return resources (strings) 
    /// specific to a 
    /// the specified <see cref="CultureInfo"/>.
    /// <para>
    /// Implementation uses 
    /// <see cref="ICachedCultureSpecificWcfServiceAgentService"/>
    /// so results are cached in this tier.
    /// </para>
    /// </summary>
    public class CachedCultureSpecificResourceService : ICultureSpecificResourceService
    {
        private readonly ICachedCultureSpecificWcfServiceAgentService _cachedCultureSpecificWcfServiceAgentService;
        private readonly IClientEnvironmentService _clientEnvironmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedCultureSpecificResourceService" /> class.
        /// </summary>
        /// <param name="cultureSpecificCachedWcfServiceAgentService">The culture specific cached WCF service agent service.</param>
        /// <param name="clientEnvironmentService">The client environment service.</param>
        public CachedCultureSpecificResourceService(
            ICachedCultureSpecificWcfServiceAgentService cultureSpecificCachedWcfServiceAgentService,
            IClientEnvironmentService clientEnvironmentService)
        {
            _cachedCultureSpecificWcfServiceAgentService = cultureSpecificCachedWcfServiceAgentService;
            _clientEnvironmentService = clientEnvironmentService;
        }


        /// <summary>
        /// Gets a single resource in a filter set (ie in a view.
        /// <para>
        /// Internally, this will be invoking <see cref="GetResourceSet" />
        /// first in order to cache resources for the whole view.
        /// </para>
        /// </summary>
		/// <param name="resourceSet"></param>
        /// <param name="resourceKey"></param>
        /// <param name="cultureInfo"></param>
        /// <param name="resourceTransformation"></param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException"></exception>
		public string GetResource(string resourceSet, string resourceKey, CultureInfo cultureInfo = null,
            ResourceTransformation resourceTransformation = ResourceTransformation.None)
        {
	        try
	        {
				// Retrieve resource group from cache, or server
				Dictionary<string, string> results = GetResourceSet(resourceSet, cultureInfo, resourceTransformation);

				// Pluck out string by resourceKey
	            string result;
	            if (!results.TryGetValue(resourceKey, out result))
	            {
					result = "#ERR:{0}:{1}".FormatStringInvariantCulture(resourceSet, resourceKey);
	            }

	            return result;

	        }
	        catch (Exception e)
	        {
			throw new ApplicationException(string.Format("ResourceService - Unable to get resources. Filter: '{0}', resourceKey'{1}'", resourceSet, resourceKey), e);
	        }
        }


        /// <summary>
        /// Gets and caches locally the requested resources, per page/filter.
        /// </summary>
		/// <param name="resourceSet">Name of the filter/set/page/view.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <returns></returns>
		public Dictionary<string, string> GetResourceSet(string resourceSet, CultureInfo cultureInfo = null, ResourceTransformation resourceTransformation = ResourceTransformation.None)
        {

            Dictionary<string, Dictionary<string, string>> results = GetResourceSets(cultureInfo,
																						  resourceTransformation, resourceSet);

			Dictionary<string, string> result = results[resourceSet];

            return result;
        }

        /// <summary>
        /// Gets the resource sets.
        /// </summary>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
		/// <param name="resourceSets">The filters.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException"></exception>
        public Dictionary<string, Dictionary<string, string>> GetResourceSets(CultureInfo cultureInfo = null,
																			  ResourceTransformation resourceTransformation = 
                                                                                  ResourceTransformation.None,
																			  params string[] resourceSets)
        {
			if (cultureInfo == null)
            {
                cultureInfo = _clientEnvironmentService.ClientUICulture;
            }

            string cultureInfoCode = cultureInfo.ToString();

            Dictionary<string, Dictionary<string, string>> results =
                new Dictionary<string, Dictionary<string, string>>();



            //We are not forcing, so we choose alternate root of checking locally
            //and seeing if we have it already. If we don't, we check remotely.
			foreach (string resourceSet in resourceSets)
            {
                try
                {

					string resourceSetName = resourceSet;

                    //Cache name gets a prefix (although we send off just the resourceSetName):
                    string cacheKey = Constants.Caching.CacheKeys.ResourceKeyPrefix + resourceSetName;

                    Dictionary<string, string> individualfilterResults =
                        _cachedCultureSpecificWcfServiceAgentService
                            .Query<IResourceServiceFacade, Dictionary<string, string>>(
                                service =>
                                    service.GetResourceSet(resourceSetName, cultureInfoCode, resourceTransformation),
                                cacheKey,
                                cultureInfo);


					results[resourceSet] = individualfilterResults;
                }
                catch (Exception e)
                {
                    throw new ApplicationException(
						string.Format("ResourceService - Unable to get resources for view '{0}'", resourceSet), e);
                }
            }
            return results;
        }
    }
}