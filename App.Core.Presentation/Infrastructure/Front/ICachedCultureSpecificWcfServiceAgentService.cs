﻿namespace App.Core.Infrastructure.Front.Services
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Service to return results cached 
    /// or 
    /// retrieve fresh values from a remove 
    /// WCF Service.
    /// </summary>
    public interface ICachedCultureSpecificWcfServiceAgentService : IHasAppService
    {

            /// <summary>
            /// Connects to the service of the specified type, executes the function, returns the service call result
            /// and closes the connection.
            /// </summary>
            /// <typeparam name="TService">The type of the service.</typeparam>
            /// <typeparam name="TResult">The type of the result.</typeparam>
            /// <param name="func">The function to be executed on the service</param>
            /// <returns></returns>
        TResult Query<TService, TResult>(Func<TService, TResult> func, string cacheKey, CultureInfo cultureInfo = null,TimeSpan? cacheTimeSpan =null);

    }
}
