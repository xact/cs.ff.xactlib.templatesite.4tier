﻿namespace App.Core.Infrastructure.Front.Initialization.Steps
{
    using System.Collections.Generic;
    using XAct.Services;

    public static class ServiceInitializationArguments
    {
        public static object IoCContainer { get; set; }

        public static List<IBindingDescriptorBase> BindingDescriptors
        {
            get { return _bindingDescriptors ?? (_bindingDescriptors = new List<IBindingDescriptorBase>()); }
        }

        private static List<IBindingDescriptorBase> _bindingDescriptors;
    }
}