﻿namespace App.Core.Infrastructure.Front.Initialization.Steps.Implementations
{
    using App.Core.Infrastructure.Services.Configuration;
    using XAct;

    public class ConfigureObjectMapsInitializationStep : IHasInitializationStep
    {
        public void Execute()
        {
            IObjectMappingServiceConfiguration objectMappingServiceConfiguration =
                XAct.DependencyResolver.Current.GetInstance<IObjectMappingServiceConfiguration>();

            objectMappingServiceConfiguration.Initialize();
        }
    }
}