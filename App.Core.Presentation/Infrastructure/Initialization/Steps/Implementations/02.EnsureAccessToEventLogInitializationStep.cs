﻿namespace App.Core.Infrastructure.Front.Initialization.Steps.Implementations
{
    using System;
    using System.Diagnostics;
    using XAct;

    public class  EnsureAccessToEventLogInitializationStep : IHasInitializationStep
    {


        public void Execute()
        {


            string tmp = System.Configuration.ConfigurationManager.AppSettings["EventLogSourceNames"];

            if (tmp.IsNullOrEmpty())
            {
                return;
            }

            string[] sourceNames = tmp.Split(new char[] {',', '|', ';'},
                                             StringSplitOptions.RemoveEmptyEntries);
            foreach (string sourceName in sourceNames)
            {

                try
                {
                    if (!EventLog.SourceExists(tmp))
                    {
                        EventSourceCreationData data = new EventSourceCreationData(sourceName, "Application");

                        string name = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                        //Create the Application EventLog
                        EventLog.CreateEventSource(data);
                    }

                    //Write to it:
                    EventLog.WriteEntry(sourceName, "Check:can write to EventLog.");

                }
                catch
                {
                    new DefaultTraceListener().TraceEvent(new TraceEventCache(), "APP", TraceEventType.Error, 0,
                                                          "Could not Create '{0}' Application EventLog or write to it."
                                                              .FormatStringInvariantCulture(sourceName));
                }
            }
        }
    }
}
