﻿namespace App.Core.Infrastructure.Front.Initialization.Steps.Implementations
{
    using System;
    using AutoMapper.Internal;
    using AutoMapper.Mappers;
    using XAct;

    /// <summary>
    /// 
    /// </summary>
    internal class HardReferencesToEnsureCorrectDeploy : IHasInitializationStep
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HardReferencesToEnsureDbMigrationOccursOnCiServer"/> class.
        /// </summary>
        public HardReferencesToEnsureCorrectDeploy()
        {
        }


        public void Execute()
        {
            //This method will never be called, but creates a hard link to classes
            //within problematic assemblies that are only referred to indirectly, via
            //reflection.
            //Due to the way that MSBuild optimises, it leaves behind assemblies it sees no 
            //hard link to...the omission of causing failed WebDeploy packages.
            //Symptoms are:
            //Builds locally, but requests a Code Migration. When you deploy to Build Server
            //Publish.proj complains that model is not in sync. What is happening is that
            //Build server is seeing all code migrations. But not seeing Assembly that would
            //cause the migration (ie, from its wrong point of view, it has "one code migration too far").

            //This is a work around.
			Type[] hardReferences =
				new Type[]
					{
						//means AutoMapper.Net4.dll will be referenced and copied to output test folder.
						typeof(NullableConverterFactory),
						typeof(DataReaderMapper),
                        //typeof(WindowsSystemPerformanceCounterService)
					};

			// AutoMapper - Ensure that their 'Net4' DLL is *used* in the code.
			// -- This is required to have the correct DLLs packaged with a *release* build
			// -- The class references above aren't enough (they only work for debug mode releases)
			NullableConverterFactory trickDirectReferenceToAutoMapperNet4 = new NullableConverterFactory();
			bool trickUtiliseAutoMapper4 = trickDirectReferenceToAutoMapperNet4.IsEnum();
        }
    }

}
