﻿namespace App.Core.Infrastructure.Front.Initialization.Steps.Implementations
{
    using System;
    using System.Diagnostics;
    using App.Core.Infrastructure.Front.Services;
    using App.Core.Infrastructure.Services;
    using XAct;

    public class RunSmokeTestsInitializationStep : IHasInitializationStep
    {

        //Use late binding so that Tracing is not called so early in init sequence.
        //So that unit tests can safely call them, and replace as need by for mocking, etc.
        private ITracingService TracingService { get { return _tracingService ?? (_tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>()); } }
        private ITracingService _tracingService;
        private readonly string _typeName;
        
        public RunSmokeTestsInitializationStep()
		{
            _typeName = this.GetType().Name;
        }

        public void Execute()
        {


            XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterService>().Update("UNSPECIFIED - Requests - Front Operations Performed");
            XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterService>().Update("UNSPECIFIED - Requests - Front Operations Per Second");


            //At this point, Services should be enabled, so one should always be able to 
            //to find the umbiquous TracingService:
			_tracingService.Trace(TraceLevel.Verbose, "Starting Front Infrastructure Smoke Test...");
			
            Type[] smokeTestsTypes = new Type[]
                {
                    typeof(IHaveBasicServicesSmokeTest),
                    typeof(ICrossTierLoggingSmokeTest)
                };


            //Execute each Initialization Step:
            smokeTestsTypes.ForEach(ExecuteSmokeTest);


            //Note that the default tracing solution is passing the data down to 
            //a NLog based TraceListener that can then be routed to a rolling logfile, 
            //an event log, a db table, and/or an SMTP solution. Examples of various NLog
            //configurations are shown in the Config folder.


        }

        void ExecuteSmokeTest(Type type)
        {
            IFrontSmokeTest smokeTest =  (IFrontSmokeTest) XAct.DependencyResolver.Current.GetInstance(type);

	        try
	        {
		        smokeTest.Execute();
	        }
	        catch (Exception ex)
	        {
		        _tracingService.TraceException(TraceLevel.Warning, ex, "Smoke test failed: '{0}'", type.AssemblyQualifiedName);

				// Allow app to continue after failed smoke test 
	        }
        }
    }
}
