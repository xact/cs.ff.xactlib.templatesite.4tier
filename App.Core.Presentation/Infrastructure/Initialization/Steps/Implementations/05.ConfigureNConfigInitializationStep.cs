﻿namespace App.Core.Infrastructure.Front.Initialization.Steps.Implementations
{
    using System.Diagnostics;
    using XAct;
    using XAct.Services;
    using XAct.Settings;

    public class ConfigureNConfigInitializationStep : IHasInitializationStep
    {


        /// <summary>
        /// Initializations the step registration of services definitions into the io C.
        /// </summary>
        /// <internal>
        /// This method can't be turned into an IHasInitializationStep because
        /// the IoC it is going to initialize has not been initialized...
        /// NOTE: remain public so that it can be invoked by DbContext when necessary
        /// from DbMigrations.
        ///   </internal>
        public void  Execute ()
        {
			// This step disables NConfig in Release mode.
			// We've had issues were sometimes NConfig can crash the app, so it must be
			// disabled outside of dev. (See Jira #1333)

            
            NConfigInitializer.EnableNConfig = true;

        }
    }
}