﻿namespace App.Core.Infrastructure.Front.Initialization.Steps.Implementations
{
    using System.Diagnostics;
    using XAct;
    using XAct.Diagnostics.Performance.Services.Configuration;
    using XAct.Services;

    public class RegisterServicesInIoCInitializationStep : IHasInitializationStep
    {


        /// <summary>
        /// Initializations the step registration of services definitions into the io C.
        /// </summary>
        /// <internal>
        /// This method can't be turned into an IHasInitializationStep because
        /// the IoC it is going to initialize has not been initialized...
        /// NOTE: remain public so that it can be invoked by DbContext when necessary
        /// from DbMigrations.
        ///   </internal>
        public void  Execute ()
        {



            //For the applicatio to function, all the available 
            //services -- those that are app specific, and those
            //defined in the underlying vendor libraries  -- have
            //to be registed in the IoC engine (Unity).

            //By hand,it's a rather tedious job. 
            //The library automates this by searching the assemblies
            //by reflection, finding all class definitions that 
            //are decorated witht the DefaultBindingImplementation
            //interface, and registers them.

            //Rather elegant, actually, as the whole service registration
            //can all be handled with one line:
            XAct.Services.IoC.UnityBootstrapper.Initialize(
                ServiceInitializationArguments.IoCContainer, 
                true, 
                WatchPreLoad, 
                WatchPostLoad,
                ServiceInitializationArguments.BindingDescriptors.ToArray());

            //Temp Fix:
            XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterServiceConfiguration>()
                .RaiseExceptionsIfPerformanceCounterNotFound = false;

            //An example of a custom app specific binding registered early
            //superceding any service that would have been found by reflection
            //(ie: service class decorated with DefaultBindingImplementationAppribute)
            //and automaticall registered (by this Initialize method):.
            //new BindingDescriptor<ITracingService, MyTracingService>()

            //Or just add custom ones, like so (I don't use Xml binding, makes no sense)...

            ////Correction for missing binding in XActLib -- will add soon, so we can remove from here.
            ////and remove ref to XAct.Settings.Host.AppSettings
            //new BindingDescriptor
            //	<XAct.Settings.IAppSettingsHostSettingsService, XAct.Settings.AppSettingsHostSettingsService>()

        }

        /// <summary>
        /// A delegate that allows watching the magic that goes 
        /// on during XActlib's automatic binding.
        /// <para>
        /// Can see bindings that were found, and are about to be Registered in the underlying DependencyInjectionContainer.
        /// </para>
        /// </summary>
        /// <param name="bindingDescriptor"></param>
        private static void WatchPreLoad(IBindingDescriptor bindingDescriptor)
        {
            //Note that we are using Trace at this early stage because we can't be sure 
            //TracingService has yet been registered and is available.

            Debug.WriteLine("Binding {0}=>{1}".FormatStringInvariantCulture(bindingDescriptor.InterfaceType,
                                                                            bindingDescriptor.ImplementationType));
        }

        private static void WatchPostLoad(IBindingDescriptorResult bindingDescriptorResult)
        {
            //Note that we are using Trace at this early stage because we can't be sure 
            //TracingService has yet been registered and is available.

            if (bindingDescriptorResult.Exception != null)
            {

                Trace.TraceError("Error Registering Binding:");
                Trace.TraceError("  (Interface:{0} => Implementation:{1})".FormatStringInvariantCulture(
                    bindingDescriptorResult.BindingDescriptor.InterfaceType,
                    bindingDescriptorResult.BindingDescriptor.ImplementationType
                                     ));
            }

            if (bindingDescriptorResult.Skipped)
            {
                return;
            }

            Debug.WriteLine("Binding Success: {0}"
                                       .FormatStringInvariantCulture(bindingDescriptorResult));
			Debug.WriteLine("...Interface:{0})"
                                       .FormatStringInvariantCulture(
                                           bindingDescriptorResult.BindingDescriptor.InterfaceType));
			Debug.WriteLine("...Implementation:{0})"
                                       .FormatStringInvariantCulture(
                                           bindingDescriptorResult.BindingDescriptor.ImplementationType));

            if (bindingDescriptorResult.Exception != null)
            {
                Trace.TraceError(bindingDescriptorResult.Exception.Message);
            }
        }
    }
}