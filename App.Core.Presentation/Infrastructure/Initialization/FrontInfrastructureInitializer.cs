﻿namespace App.Core.Infrastructure.Front.Initialization
{
    using App.Core.Infrastructure.Front.Initialization.Steps.Implementations;

    /// <summary>
    /// Part of the initialization sequence chain.
    /// <para>
    /// The initialization sequence is as follows:
    /// <code>
    /// * Global.asax 
    ///     * ... invokes AppHostInitializer,
    ///         * ... which first invokes PresentationInitializer
    ///             * ... which first invokes FrontInfrastructureInitializer  (this class)
    ///               where IoC (ie Unity) is initialized with Infrastructure Services...
    ///         * ....now, coming back up...Presentation services are registered...
    ///     * ... now that both Infras and presentation are initialized, AppHostInitializer
    ///       finishes up by registering AppHost level Services.
    /// * Initialization done...
    /// * Integration cycle then starts...
    ///   It too goes down through the layers, and back up, in much the same way... 
    ///   but this time actually using remote services (eg, to load caches, etc).      
    /// </code>
    /// </para>
    /// </summary>
    public static class FrontInfrastructureInitializer
    {


        public static void Execute()
        {
            //----------------------------------------------------------
            //Steps prior to IoC being available:
            new EnsureDefaultTracerWhenInDebugModeInitializationStep().Execute();
            new EnsureAccessToEventLogInitializationStep().Execute();

            new RegisterServicesInIoCInitializationStep().Execute();
            
			//This ensures that NConfig is not running in release mode,
            //even just on the CI (don't want CPU issues there either, when there is a missing app.config).
            new ConfigureNConfigInitializationStep().Execute();

            //----------------------------------------------------------
            //Steps post IoC being up and running:
            new HardReferencesToEnsureCorrectDeploy().Execute();


            //Register all maps.
            //TODO: Move this down to a FW, shared Init step:
            new ConfigureObjectMapsInitializationStep().Execute();

            //new INitializeValidationServiceInitializationStep().Execute();


        }







    }
}
