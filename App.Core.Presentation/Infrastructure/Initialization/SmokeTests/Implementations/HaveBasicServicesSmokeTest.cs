﻿namespace App.Core.Infrastructure.Front.Initialization
{
    using App.Core.Infrastructure.Front.Services;
    using XAct;
    using XAct.Services;

    [DefaultBindingImplementation (typeof(IHaveBasicServicesSmokeTest),BindingLifetimeType.Undefined, Priority.Low)]
    public class HaveBasicServicesSmokeTest : IHaveBasicServicesSmokeTest
    {
        public void Execute()
        {
            IWcfServiceAgentServiceConfiguration serviceConfigService = XAct.DependencyResolver.Current.GetInstance<IWcfServiceAgentServiceConfiguration>();
        }
    }
}