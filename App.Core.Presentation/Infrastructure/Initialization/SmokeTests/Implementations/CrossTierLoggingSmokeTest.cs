namespace App.Core.Infrastructure.Front.Initialization
{
    using System.Diagnostics;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Services;

    [DefaultBindingImplementation(typeof(ICrossTierLoggingSmokeTest), BindingLifetimeType.Undefined, Priority.Low)]
    public class CrossTierLoggingSmokeTest : ICrossTierLoggingSmokeTest
    {
        public void Execute()
        {
            //Front tier's logging should have one TraceListener that
            //is communicating with the back end,
            //so these messages should be ending up in the back tier's log file.
            //Check...
            XAct.DependencyResolver.Current.GetInstance<ITracingService>().Trace(TraceLevel.Verbose,  "...");
        }
    }
}