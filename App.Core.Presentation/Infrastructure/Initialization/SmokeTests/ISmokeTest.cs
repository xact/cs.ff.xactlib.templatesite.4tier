﻿namespace App.Core.Infrastructure.Front.Initialization
{
    using XAct;

    /// <summary>
    /// Contract for Initialization SmokeTests in both
    /// Front Infrastructure and Presentation.
    /// </summary>
    public interface IFrontSmokeTest : IHasExecutableAction
    {

    }
}