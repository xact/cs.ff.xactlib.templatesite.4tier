﻿namespace XAct.Services.Comm.ServiceModel
{
    using System.ServiceModel;

    /// <summary>
    /// A Service Client Builder that will 
    /// in both a 3 tier or N-Tier environment.
    /// </summary>
    public class ServiceClientEndpointBuilder
    {
        /// <summary>
        /// Creates a Service instance either
        /// as a WCF Client Endpoint (as would be required
        /// in an N-Tier configuration) or as a direct
        /// instance, as normal.
        /// <para>
        /// When creating a Service Client Endpoint, uses
        /// a named configuration in the application's config
        /// </para>
        /// <para>
        /// Example:
        /// <code>
        /// <![CDATA[
        /// var try3TierFirst = appSettings.SomeModeFlag;
        /// 
        /// ISomeRemoteService service =
        /// ServiceClientEndpointBuilder.BuildServiceClient("BasicHttpBinding_IService1", try3TierFirst);
        /// ]]>
        /// </code>
        /// </para>
        /// using
        /// file.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <param name="endpointConfigurationName">Name of the endpoint configuration.</param>
        /// <param name="tryFirstDirectInstance">if set to <c>true</c> [try first direct instance].</param>
        /// <returns></returns>
        public static TService BuildServiceClient<TService>(string endpointConfigurationName, bool tryFirstDirectInstance)
        {

            try
            {
                TService service;

                if (tryFirstDirectInstance)
                {


                    //Depending if running in 3 tier mode or 4 tier mode
                    //use the factory to create a Service Channel:
                    service = XAct.DependencyResolver.Current.GetInstance<TService>();
                }
                else
                {
                    service = default(TService);
                }

                if (object.Equals(service, default(TService)))
                {
                    //Binding binding = new BasicHttpBinding("BasicHttpBinding_IService1");

                    //Retrieve a named binding from Config file and
                    //Create a channelFactory for the remote service:
                    //ChannelFactory<TService> stripperFactoryDirect = new ChannelFactory<TService>(new BasicHttpBinding(), new EndpointAddress("http://localhost:17862/BackNSI/Services/Session.svc"));

                    ChannelFactory<TService> stripperFactory = new ChannelFactory<TService>(endpointConfigurationName);


                    //We are currently in 4 tier mode.
                    service = stripperFactory.CreateChannel();
                }
                return service;
            }

            catch 
            {
                throw;
            }
        }
    }
}
