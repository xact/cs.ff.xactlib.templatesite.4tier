﻿namespace App.Core.Infrastructure.Front.HttpHandlers
{
    using System.Web;

    public abstract class ServiceLocatorHttpHandlerFactoryBase<THttpHandler> :
        XAct.Web.HttpHandlers.ServiceLocatorHttpHandlerFactoryBase<THttpHandler>,
        IHttpHandlerFactory
        where THttpHandler : IHttpHandler
    {

        protected ServiceLocatorHttpHandlerFactoryBase(bool isReusable):base(isReusable){}

        protected ServiceLocatorHttpHandlerFactoryBase() : base(true){}    }
}
