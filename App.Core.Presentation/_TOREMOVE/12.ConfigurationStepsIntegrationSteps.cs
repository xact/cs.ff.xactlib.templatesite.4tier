﻿//namespace App.Core.Infrastructure.Front.Initialization.Steps.Implementations
//{
//    using System;
//    using System.Diagnostics;
//    using App.Core.Infrastructure.Services;
//    using XAct;
//    using XAct.Initialization;

//    public class ConfigurationStepsIntegrationStep : IHasInitializationStep
//    {

//        //Use late binding so that Tracing is not called so early in init sequence.
//        //So that unit tests can safely call them, and replace as need by for mocking, etc.
//        private ITracingService TracingService { get { return _tracingService ?? (_tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>()); } }
//        private ITracingService _tracingService;
//        private readonly string _typeName;


//        public ConfigurationStepsIntegrationStep(ITracingService tracingService)
//        {
//            _typeName = this.GetType().Name;
//        }


//        /// <summary>
//        /// Now that all the services are registered, one can actually 
//        /// perform initialization (ie, loading up caching, etc.)
//        /// </summary>
//        public void Execute()
//        {

//        }


//    }
//}