﻿
namespace App.Core.Presentation.Initialization
{
    using App.Core.Infrastructure.Front.Initialization;


    /// <summary>
    /// Part of the initialization sequence chain.
    /// <para>
    /// The initialization sequence is as follows:
    /// <code>
    /// * Global.asax 
    ///     * ... invokes AppHostInitializer,
    ///         * ... which first invokes PresentationInitializer (this class)
    ///             * ... which first invokes FrontInfrastructureInitializer
    ///               where IoC (ie Unity) is initialized with Infrastructure Services...
    ///         * ....now, coming back up...Presentation services are registered...
    ///     * ... now that both Infras and presentation are initialized, AppHostInitializer
    ///       finishes up by registering AppHost level Services.
    /// * Initialization done...
    /// * Integration cycle then starts...
    ///   It too goes down through the layers, and back up, in much the same way... 
    ///   but this time actually using remote services (eg, to load caches, etc).      
    /// </code>
    /// </para>
    /// </summary>
    public static class PresentationInitializer
    {

        public static void Execute()
        {
#if THREETIER
            //If all one happy app, then 
            //skip ... but havn't figured out how to mix in one solution
            //3 and 4 tier.
#else 
            FrontInfrastructureInitializer.Execute();
#endif
        }

    }
}
