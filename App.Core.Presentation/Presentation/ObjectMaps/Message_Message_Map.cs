﻿namespace App.Core.Presentation.Maps
{
    using App.Core.Infrastructure.Maps;
    using App.Core.Infrastructure.Services;
    using AutoMapper;
    using XAct.Messages;

    /// <summary>
    /// <see cref="IObjectMappingService"/> map
    /// invoked when mapping <see cref="IResponse"/>
    /// objects.
    /// <para>
    /// For a use case example, see:
    /// <see cref="MergeRequestPOXOperationProcessor"/>.
    /// </para>
    /// </summary>
    // ReSharper disable InconsistentNaming
    public class Message_Message_Map : MapBase<Message, Message>
        // ReSharper restore InconsistentNaming
    {


        public override void Initialize()
        {

            IMappingExpression<Message, Message> map = base.CreateMap<Message, Message>();

            map.ForMember(d => d.Arguments, opt => opt.MapFrom(s => s.Arguments));
            map.ForMember(d => d.InnerMessages, opt => opt.MapFrom(s => s.InnerMessages));
            map.ForMember(d => d.MessageCode, opt => opt.MapFrom(s => s.MessageCode));
            map.ForMember(d => d.PresentationAttributes, opt => opt.MapFrom(s => s.PresentationAttributes));

        }
    }

    public class MessageCode_MessageCode_Map :MapBase<MessageCode, MessageCode>
    {
        public override void Initialize()
        {
            IMappingExpression<MessageCode, MessageCode> map = base.CreateMap<MessageCode, MessageCode>();

            map.ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id));
            map.ForMember(d => d.Severity, opt => opt.MapFrom(s => s.Severity));

            //map.AfterMap(AfterMap);
        }
        //void AfterMap(MessageCode s, MessageCode t)
        //{
        //    XAct.DependencyResolver.Current.GetInstance<IObjectMappingService>()
        //        .Map<MessageCodeMetadata, MessageCodeMetadata>(s.Metadata, t.Metadata);
        //}
    }

    //public class MessageCodeMetadata_MessageCodeMetadata_Map : MapBase<MessageCodeMetadata, MessageCodeMetadata>
    //{
    //    public override void Initialize()
    //    {
    //        IMappingExpression<MessageCodeMetadata, MessageCodeMetadata> map = base.CreateMap<MessageCodeMetadata, MessageCodeMetadata>();

    //        map.ForMember(d => d.ArgumentCount, opt => opt.MapFrom(s => s.ArgumentCount));
    //        map.ForMember(d => d.ResourceFilter, opt => opt.MapFrom(s => s.ResourceFilter));
    //        map.ForMember(d => d.ResourceKey, opt => opt.MapFrom(s => s.ResourceKey));

    //    }
    //}
}