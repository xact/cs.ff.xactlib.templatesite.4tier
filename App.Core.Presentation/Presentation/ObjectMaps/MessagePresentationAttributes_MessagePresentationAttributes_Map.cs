﻿namespace App.Core.Presentation.Maps
{
    using App.Core.Infrastructure.Maps;
    using AutoMapper;
    using XAct.Messages;

    /// <summary>
    /// <see cref="IObjectMappingService"/> map
    /// invoked when mapping <see cref="IResponse"/>
    /// objects.
    /// <para>
    /// For a use case example, see:
    /// <see cref="MergeRequestPOXOperationProcessor"/>.
    /// </para>
    /// </summary>
    // ReSharper disable InconsistentNaming
    public class MessagePresentationAttributes_MessagePresentationAttributes_Map : MapBase<MessagePresentationAttributes, MessagePresentationAttributes>
// ReSharper restore InconsistentNaming
    {


        public override void Initialize()
        {

            IMappingExpression<MessagePresentationAttributes, MessagePresentationAttributes> map = base.CreateMap<MessagePresentationAttributes, MessagePresentationAttributes>();

            map.ForMember(d => d.Text, opt => opt.MapFrom(s => s.Text));
            map.ForMember(d => d.AdditionalInformation, opt => opt.MapFrom(s => s.AdditionalInformation));
            map.ForMember(d => d.AdditionalInformationUrl, opt => opt.MapFrom(s => s.AdditionalInformationUrl));
        }
    }
}