﻿namespace App.Core.Presentation.Maps
{
    using App.Core.Infrastructure.Maps;
    using AutoMapper;
    using XAct.Messages;

    /// <summary>
    /// <see cref="IObjectMappingService"/> map
    /// invoked when mapping <see cref="IResponse"/>
    /// objects.
    /// <para>
    /// For a use case example, see:
    /// <see cref="MergeRequestPOXOperationProcessor"/>.
    /// </para>
    /// </summary>
// ReSharper disable InconsistentNaming
    public class MessageMetadata_MessageMetadata_Map : MapBase<XAct.Messages.Metadata, XAct.Messages.Metadata>
// ReSharper restore InconsistentNaming
    {
        public override void Initialize()
        {

            IMappingExpression<Metadata, Metadata> map = base.CreateMap<XAct.Messages.Metadata, XAct.Messages.Metadata>();

            map.ForMember(d => d.Key, opt => opt.MapFrom(s => s.Key));
            map.ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value));
        }
        
    }
}