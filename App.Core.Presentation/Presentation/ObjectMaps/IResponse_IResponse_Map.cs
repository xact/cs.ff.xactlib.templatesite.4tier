﻿namespace App.Core.Presentation.Maps
{
    using App.Core.Infrastructure.Maps;
    using AutoMapper;
    using XAct.Messages;

    /// <summary>
    /// <see cref="IObjectMappingService"/> map
    /// invoked when mapping <see cref="IResponse"/>
    /// objects.
    /// <para>
    /// For a use case example, see:
    /// <see cref="MergeRequestPOXOperationProcessor"/>.
    /// </para>
    /// </summary>
// ReSharper disable InconsistentNaming
    public class IResponse_IResponse_Map : MapBase<IResponse, IResponse>
// ReSharper restore InconsistentNaming
    {


        public override void Initialize()
        {

            IMappingExpression<IResponse, IResponse> map = base.CreateMap<IResponse, IResponse>();

            map.ForMember(d => d.TimeElapsed, opt => opt.MapFrom(s => s.TimeElapsed));
            map.ForMember(d => d.Metadata, opt => opt.MapFrom(s => s.Metadata));
            map.ForMember(d => d.Messages, opt => opt.MapFrom(s => s.Messages));
        }
    }
}