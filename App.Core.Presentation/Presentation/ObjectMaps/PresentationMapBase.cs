﻿namespace App.Core.Presentation.Maps
{
    using App.Core.Infrastructure;
    using App.Core.Infrastructure.Services;
    using XAct.ObjectMapping;

    [ObjectMap]
    public abstract class PresentationMapBase<TSource, TTarget> : Infrastructure.Maps.MapBase<TSource,TTarget>
    {

        protected App.Core.Infrastructure.Services.IClientEnvironmentService ClientEnvironmentService
        {
            get
            {
                return XAct.DependencyResolver.Current.GetInstance<App.Core.Infrastructure.Services.IClientEnvironmentService>();
            }
        }

        protected AppSettings AppSettings
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>().AppSettings; }
        }

    }
}
