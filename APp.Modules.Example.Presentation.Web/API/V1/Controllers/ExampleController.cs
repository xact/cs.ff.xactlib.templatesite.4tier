﻿using App.Core.Infrastructure.Services;

namespace App.Modules.Example.Presentation.Web.API.PublicAPI.v1.Controllers
{
    using System.Linq;
    using System.Web.Http;
    using App.Core.AppHost.Areas.API.V2.Controllers;
    using App.Modules.Example.Domain.Entities;

    [RoutePrefix("api/v1/example")]
    public class ExampleController : ApiController
    {
        //private readonly IDateTimeService _dateTimeService;
        ////App.Core.Application.AppBootstrapper 
        //public ExampleController(IDateTimeService dateTimeService)
        //{
        //    _dateTimeService = dateTimeService;
        //}

        //[Queryable]
        [Route("")]
        public IQueryable<Example> Get()
        {
            return null;
        }
    }
}
