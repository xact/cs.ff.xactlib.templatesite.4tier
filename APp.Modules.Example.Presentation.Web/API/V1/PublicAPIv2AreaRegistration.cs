﻿//using System.Web.Http;
//using System.Web.Mvc;
//using System.Web.Routing;
//using App.Core.AppHost.Areas.UiAPI.Controllers;

//namespace App.Core.AppHost.Areas.PublicAPI.v2 {
//    public class PublicApiv2AreaRegistration : AreaRegistration {

//        public override string AreaName
//        {
//            get
//            {
//                // Must match up with the namespace
//                // (Used for area-selection)
//                return "PublicAPI.V2";
//            }
//        }

//        public override void RegisterArea(AreaRegistrationContext context)
//        {
//            //NOT the way to do it:
//            //* for one, it has Actions (that's an MVC, not an WebAPI concept)
//            //* it also has no tie-in to namespacing:
//            //* for three, it's not MapRoute, it's MapHttpRoute:
//            //  For MVC we use route = context.MapRoute
//            //  Whereas in WebAPI we use route = context.MapHttpRoute
//            //context.MapRoute(
//            //    "UiAPI_default",
//            //    "UiAPI/{controller}/{action}/{id}",
//            //    new { action = "Index", id = UrlParameter.Optional }
//            //);

//            //AND (Important as it's so easy to miss) no Action is in path

//            var route = context.Routes.MapHttpRoute(
//                "API_V2_default",
//                "API/V2/{controller}/{id}",
//                new { area = AreaName, id = UrlParameter.Optional }
//                );

//            if (route.DataTokens == null)
//            {
//                route.DataTokens = new RouteValueDictionary();
//            }

//            //We assoicate this route to this namespace:
//            string nsToSearchIn = typeof(App.Core.AppHost.Areas.API.V2.Controllers.ExampleController).Namespace;

//            route.DataTokens["Namespaces"] = new string[] { nsToSearchIn };


//        }

//    }
//}
