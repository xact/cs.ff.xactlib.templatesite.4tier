﻿using System;
using System.Web.Mvc;

namespace App.Core.AppHost.Controllers
{
    using App.Modules.Example.Domain.Entities;
    using App.Modules.Example.Domain.ViewModels;
    using App.Modules.Example.Presentation.Examples.Infrastructure.Services.Clients;
    using XAct.Messages;

    public class ExampleController : Controller
    {
        private readonly IExampleService _exampleService;

        public ExampleController(IExampleService exampleService)
        {
            _exampleService = exampleService;
        }

        //
        // GET: /Example/
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult List()
        {

            return View();
        }

        public ActionResult View(Guid id)
        {
            //Note that the Example entity is a Domain object
            //which we do *not* want to round-trip (that's only ok
            //for rookie scenarios). 
            //Here we are round-tripping a a ViewModel specifically 
            //tailored for the task (ie, it might have more or less properties than are just in Example,
            //for example be a combination of flattening of Example + ExampleDetails,
            //and not including some properties on Example).
            Response<ExampleViewModel> exampleViewModelResponse = _exampleService.GetSingleForViewing(id);

            return View(exampleViewModelResponse);
        }


        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            //Again, we don't want to round-trip the Domain entity.
            //There are many reasons for this. For example, if we were looking at 
            //a customers purchase record, we want to ensure that the Amount is in the ViewBag,
            //not the Model (we don't want the user to be able to manipulate the post back
            //of the Amount in order to purchase a car for a price of 0.01...)
            Response<ExampleViewModel> exampleViewModelResponse = _exampleService.GetSingleForViewing(id);

            return View(exampleViewModelResponse);
        }

        [HttpPost]
        public ActionResult Edit(ExampleViewModel example)
        {
            return View();
        }



        public ActionResult Exception()
        {
            throw new Exception("This is a test exception");
        }

    }
}
