﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web.Helpers;
//using System.Web.Http;
//using App.Core.AppHost.Attributes;
//using App.Core.Domain.Models;
//using App.Core.Front.IOModels;
//using App.Core.Presentation.Services;
//using XAct;
//using XAct.Messages;

//namespace App.Core.AppHost.Areas.UiAPI.Controllers
//{
//    using App.Core.Domain.Messages;
//    using System.Net.Http;

//    /// <summary>
//    /// A Rest/WebAPI Controller that handles a *single* Student.
//    /// </summary>
//    //[BreezeController]
//    public class StudentController : ApiControllerBase
//    {
//        private readonly IStudentService _studentService;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="StudentController" /> class.
//        /// </summary>
//        /// <param name="studentService">The student service.</param>
//        public StudentController(IStudentService studentService)
//        {
//            _studentService = studentService;
//        }

//        [HttpGet]
//        public StudentViewModel Get(long id)
//        {

//            var nsn = id; // For clarity

//            //return _studentService.GetStudent(id);
//            return _studentService.GetStudentViewModel(nsn, false);
//        }

//        /// <summary>
//        /// Add student
//        /// </summary>
//        /// <param name="student"></param>
//        [HttpPost]
//        public Response<long?> Post(Student student)
//        {
//            if (student == null) { throw new ArgumentNullException("student"); }
//            var response = _studentService.AddStudentNoSearch(student);
//            return response;
//        }

//        /// <summary>
//        /// Update student
//        /// </summary>
//        /// <param name="student"></param>
//        [HttpPut]
//        public Response Put([FromBody] Student student)
//        {
//            //TODO: 
//            //It would be preferrable that 
//            //the post-back object were a NullableStudent.
//            //Reasoning is: if certain fields of the output model (Student)
//            //were rendered as ReadOnly, they won't be posted back.
//            //In the case of non-nullable Model fields,
//            //this causes an ambiguity between default(TWhatever) and null
            
//            //If the Student (and not NullableStudent is used) then AutoMapper conditions
//            //used must be:
//            //As long as MVC and WebAPI postback can be configured to leave disambiguate strings from nulls when filling the model:
//            //map.ForMember(t=>t.AString,opt=>opt.ConditionalOn(s=>s.AString!=null));
//            //For Enums, can be either a null check, or an Enum check against a WellKnown 0 value:
//            //map.ForMember(t=>t.AnEnum,opt=>opt.ConditionalOn(s=>!s.AnEnum.HasValue);
//            //map.ForMember(t=>t.AnEnum,opt=>opt.ConditionalOn(s=>s.AnEnum!=MyEnum.NotSet);
//            //IMPORTANT: PostBack Input Model Date Properties have to be nullable -- which decreases the trustability of the Db Schema.
//            //map.ForMember(t=>t.ADateTime, opt=>opt.ConditionalOn(s=>!s.ADateTime.HasValue);
//            //IMPORTANT: Ints have to be Nullable as well -- which decreases the trustability of the Db Schema.
//            //map.ForMember(t=>t.AnInt, opt=>opt.ConditionalOn(s=>!s.AnInt.HasValue);
            
//            //The process would be:
//            //Render using Student
//            //Postback using NullableStudent
//            //Pull up from Db existing Student
//            //Map NullableStudent over Student, using above rules
//            //Postback to db the updated student. 



//            //In all cases:
//            //we do *not* do anything such as
//            //retrieving the student in the front Presentation tier, 
//            //and doing the mapping in Presentation layer as that means a double crossing of tiers
//            //and we don't *certainly* don't need to attach the posted back student to the back tier context.
//            //That would be just plain incorrect understanding of how EF works (well).
            
//            //To see the before after changes on an entity, one can use the retrieved students tracking values -- they always
//            //contain the values as they came out of the db, and compare them to the current values of the entity (after being
//            //mapped over with the NullableStudent). There's even a flag (Changed or something) that shows what fields are updated.



//            if (student == null) { throw new ArgumentNullException("student"); }

//            UIUpdateStudentRequest updateStudentRequest = new UIUpdateStudentRequest();

//            // TEMP - Ensure alt names have the studentId set correctly
//            foreach (var altName in student.AltNames)
//            {
//                altName.StudentFK = student.Id;
//            }

//            updateStudentRequest.Student = student;

//            Response<Student> updateResult = 
//                _studentService.UpdateStudent(updateStudentRequest);

//            Response response = new Response();

//            updateResult.TransferProperties(response);

//            return response;
//        }
//    }
//}