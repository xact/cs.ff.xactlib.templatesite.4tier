﻿//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;
//using System.Net.Http;
//using System.Web;
//using System.Web.Http;
//using App.Core.Infrastructure.Front.Services;
//using Microsoft.Practices.ServiceLocation;

//namespace App.Core.AppHost.Areas.UiAPI.Controllers
//{
//    using App.Core.AppHost.Areas.API.V2.Controllers;
//    using App.Core.Infrastructure.Services;

//    public class TestExceptionController : ApiControllerBase
//    {
//        [HttpGet]
//        public string GetExample(int id)
//        {
//            var logger = ServiceLocator.Current.GetInstance<ITracingService>();
//            logger.Trace(TraceLevel.Warning, "Test trace - constructor test");

//            // Temp: Code to test controller resolution
//            try
//            {
//                var sx = ServiceLocator.Current.GetInstance<StudentController>();
//            } 
//            catch (Exception ex)
//            {
//                logger.TraceException(TraceLevel.Warning, ex, "Temp: Constructor issue tracking");
//            }

//            try
//            {
//                var sx = ServiceLocator.Current.GetInstance<StudentCheckController>();
//            }
//            catch (Exception ex)
//            {
//                logger.TraceException(TraceLevel.Warning, ex, "Temp: Constructor issue tracking");
//            }

//            throw new ApplicationException("This is a test WebAPI exception");
//        }
//    }
//}