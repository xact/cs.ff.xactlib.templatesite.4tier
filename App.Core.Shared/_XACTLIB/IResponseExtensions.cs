﻿//had to change the namespace from Xact as it was clashing with other stuff
namespace XAct
{
    using System.Diagnostics;
    using XAct.Messages;

    public static class IResponseExtensions
    {
        /// <summary>
        /// Extraneous. 
        /// <para>
        /// But some developers find 
        /// <c>response.IsError()</c> to be clearer than <c>!response.Success</c>
        /// </para>
        /// <para>
        /// It's your choice (I recommend having to use a method when avoidable).
        /// </para>
        /// </summary>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        public static bool IsError(this IResponse response)
        {
            return !response.Success;
        }

        //public static bool IsSuccess(this IResponse response)
        //{
        //    return response.Success;
        //}

        /// <summary>
        /// Nests the response's existing messages under a new parent <see cref="MessageCode"/>.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="parentMessageCode">The parent message code.</param>
        /// <returns></returns>
        public static bool Nest(this IResponse response, MessageCode parentMessageCode)
        {
            if (response.Success)
            {
                return true;
            }

            //Failed.
            //So Make new Message, and 

            Message parentMessage = new Message(parentMessageCode);

            parentMessage.InnerMessages.AddRange(response.Messages);

            //Reset Response message array:
            response.Messages.Clear();

            //And add new parentMessage back in:
            response.Messages.Add(parentMessage);

            //Which will be false:
            Debug.Assert(!response.Success);    

            //Done:
            return response.Success;
        }


	    /// <summary>
	    /// Clones the basic Response object, and sets the given data property as the specified type
	    /// </summary>
	    /// <typeparam name="T"></typeparam>
	    /// <param name="response"></param>
	    /// <param name="data"></param>
	    /// <returns></returns>
	    public static Response<T> CloneWithNewDataType<T>(this IResponse response, T data)
		{
			var result = new Response<T>();
			result.Messages.AddRange(response.Messages);
			result.Metadata.AddRange(result.Metadata);
			result.Data = data;
			return result;
		}

	    public static Response CloneWithNoData<T>(this Response<T> response)
	    {
		    var result = new Response();
		    result.Messages.AddRange(response.Messages);
		    result.Metadata.AddRange(response.Metadata);
		    return result;
	    }
    }
}
