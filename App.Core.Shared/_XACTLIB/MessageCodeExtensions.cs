﻿using System.Collections.Generic;
using System.Linq;
using XAct.Messages;

namespace XAct
{
    public static class MessageCodeExtensions
    {

        public static bool ContainsMessageCode(this IEnumerable<MessageCode> messageCodes,
                                               MessageCode checkForMessageCode)
        {
            return (messageCodes.Any(m => m.Id == checkForMessageCode.Id));
        }
    }

}