﻿
namespace XAct
{
    using System.Linq;
    using System.Xml.Linq;

    public static class XElementExtensions
    {
        /// <summary>
        /// This will ensure that elements that are empty
        /// have a string.Empty value -- which effectively
        /// ensures they have both an open and close tag.
        /// <para>
        /// Use only for extremely esoteric legacy-matching scenarios,
        /// as the RFC for XML specifies a preference for self-closing tags,
        /// rather than open/close tags containing no value.
        /// </para>
        /// <para>There is also an equivalent XDocument extension
        /// </para>
        /// </summary>
        /// <internal>
        /// Src: http://stackoverflow.com/a/2459207/1052767
        /// </internal>
        /// <param name="document"></param>
        public static void ForceOpenCloseTags(this XElement element)
        {
            foreach (XElement childElement in
                from xElement in element.DescendantNodes().OfType<XElement>()
                where xElement.IsEmpty
                select xElement)
            {
                childElement.Value = string.Empty;
            }
        }
    }
}
