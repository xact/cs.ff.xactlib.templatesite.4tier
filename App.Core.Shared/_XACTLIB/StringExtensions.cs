﻿namespace XAct
{
    public static class StringExtensions
    {
        public static bool ToBool2(this string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }

            string testChar = text.Substring(0, 1).ToUpper(/*_invariantCulture*/);
            string secondChar = (text.Length > 1) ? (text.Substring(1, 1).ToUpper()):string.Empty;

            switch (testChar)
            {
                case "S": //Can't remember why I added S and V...hum..
                case "V":
                case "O":
                    return secondChar != "F";//OFF
                    //versus:
                    //ON
                    //OUI
                    //OK
                case "Y":
                case "1":
                case "T":
                    return true;
            }
            return false;
        }
    }
}