﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace XAct
{
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    using XAct.IO;

    public static class XDocumentExtensions
    {
        ///// <summary>
        ///// This will ensure that elements that are empty
        ///// have a string.Empty value -- which effectively
        ///// ensures they have both an open and close tag.
        ///// <para>
        ///// Use only for extremely esoteric legacy-matching scenarios,
        ///// as the RFC for XML specifies a preference for self-closing tags,
        ///// rather than open/close tags containing no value.
        ///// </para>
        ///// </para>
        ///// <para>There is also an equivalent XElement extension
        ///// </summary>
        ///// <internal>
        ///// Src: http://stackoverflow.com/a/2459207/1052767
        ///// </internal>
        ///// <param name="document"></param>
        //public static void ForceOpenCloseTags(this XDocument document)
        //{
        //    foreach (XElement childElement in
        //        from xElement in document.DescendantNodes().OfType<XElement>()
        //        where xElement.IsEmpty
        //        select xElement)
        //    {
        //        childElement.Value = string.Empty;
        //    }
        //}

        public static string ToISO8859WithLegacyDeclaration(this XElement root)
        {
            var xDocument = new XDocument();
            xDocument.Add(root);
            string result = xDocument.ToStringWithDeclaration(Encoding.GetEncoding("ISO-8859-1"));
            result = result.Replace("iso-8859", "ISO-8859");
            return result;
        }

        public static string ToStringWithDeclaration(this XDocument xDocument, Encoding encoding = null)
        {
            //Not in this App!
            //Contract.Requires<ArgumentNullException>(xDocument!=null);

            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }


            var stringBuilder = new StringBuilder();
            using (TextWriter textWriter = new StringWriterWithEncoding(stringBuilder, encoding))
            {
                xDocument.Save(textWriter, SaveOptions.DisableFormatting);
            }
            return stringBuilder.ToString();
        }


        public static void ToStreamWithDeclaration(this XDocument xDocument, Stream stream, Encoding encoding = null)
        {
            using (var writer = new XmlTextWriter(stream, encoding))
            {
                xDocument.WriteTo(writer);
                writer.Close();
            }
        }


    }
}
