﻿namespace XAct.Validation
{
    using XAct.Messages;

    public interface IValidator<in T> : IValidator
    {
        bool Validate(T entity, IResponse response, IPropertyValidatorInvoker context = null);
    }

    public interface IValidator
    {
        bool Validate(object entity, IResponse response, IPropertyValidatorInvoker context = null);
    }
}