﻿namespace XAct.Validation
{
    using XAct.Messages;

    public abstract class ClassValidatorBase<T> : IValidator<T>
    {
        public bool Validate(T entity, IResponse response, IPropertyValidatorInvoker context = null)
        {
            response = new Response();
            return Validate((object)entity, response, context);
        }


        public abstract bool Validate(object entity, IResponse response, IPropertyValidatorInvoker context = null);
    }
}