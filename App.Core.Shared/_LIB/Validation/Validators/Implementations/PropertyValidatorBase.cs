﻿namespace XAct.Validation
{
    using System.Reflection;
    using XAct.Messages;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public abstract class PropertyValidatorBase : IPropertyValidator//<TModel>
    {
        
        //public bool Validate(TModel entity, PropertyInfo propertyInfo, IPropertyValidatorInvoker context, object value, IResponse response)
        //{
        //    return Validate((object)entity, propertyInfo, context, value, response);
        //}

        /// <summary>
        /// Validate2s the specified entity.
        /// <para>
        /// This is the method invoked by 
        /// <see cref="PropertyValidatorAttribute"/>.</para>
        /// </summary>
        /// <param name="entity">The entity to which the property belongs.</param>
        /// <param name="propertyInfo">The <see cref="PropertyInfo"/> describing the Property.</param>
        /// <param name="context">The <see cref="PropertyValidatorAttribute"/> that was attached to the Property.</param>
        /// <param name="value">The Property's value.</param>
        /// <param name="response">The <see cref="IResponse"/> to fill.</param>
        /// <returns>THe <see cref="IResponse.Success"/> value.</returns>
        public abstract bool Validate(object entity, PropertyInfo propertyInfo, IPropertyValidatorInvoker context, object value, IResponse response);
    }
}