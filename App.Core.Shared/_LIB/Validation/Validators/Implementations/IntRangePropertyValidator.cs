﻿//namespace XAct.Validation
//{
//    using System.Reflection;
//    using XAct.Messages;

//    public class IntRangePropertyValidator : PropertyValidatorBase
//    {
//        public override bool Validate(object entity, PropertyInfo propertyInfo, IPropertyValidatorInvoker context,
//                                      object value, IResponse response)
//        {
//            NumberRangePropertyValidatorAttribute typedContext = context as NumberRangePropertyValidatorAttribute;

//            int min = typedContext != null ? typedContext.Min:int.MinValue;
//            int max = typedContext != null ? typedContext.Max:int.MaxValue;

//            int typedValue = (int) value;

//            if (typedValue < min)
//            {
//                response.AddMessage(this.MessageCodes.Bar);
//            }

//            if (typedValue > max)
//            {
//                response.AddMessage(MessageCodes.Bar);
//            }

//            return response.Success;
//        }
//    }
//}