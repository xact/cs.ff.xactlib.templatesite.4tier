﻿namespace XAct.Validation
{
    using System.Reflection;
    using XAct.Messages;

    /// <summary>
    /// Contract implemented by subclasses of <see cref="PropertyValidatorBase"/>
    /// Invoked when an implementation of <see cref="IValidatorService"/> 
    /// invokes the Property's 
    /// <see cref="PropertyValidatorAttribute.Validate"/>s method.
    /// </summary>
    public interface IPropertyValidator<in TModel> : IPropertyValidator
    {
        /// <summary>
        /// Validates the specified entity's Property Value, 
        /// using optional flags from passed 
        /// <see cref="IPropertyValidatorInvoker"/>
        /// </summary>
        /// <param name="model">The model to validate.</param>
        /// <param name="propertyInfo">The property information.</param>
        /// <param name="context">The context.</param>
        /// <param name="value">The value.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        bool Validate(TModel model, PropertyInfo propertyInfo, IPropertyValidatorInvoker context, object value, IResponse response);
    }

    /// <summary>
    /// Contract implemented by subclasses that are the PropertyValidators
    /// (that <see cref="PropertyValidatorAttribute"/>s invoke).
    /// </summary>
    public interface IPropertyValidator
    {
        bool Validate(object model, PropertyInfo propertyInfo, IPropertyValidatorInvoker context, object value, IResponse response);
    }
}