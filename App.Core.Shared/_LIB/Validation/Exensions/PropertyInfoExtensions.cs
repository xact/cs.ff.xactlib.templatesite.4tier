﻿namespace XAct.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class PropertyInfoExtensions
    {

        public static TAttribute[] GetAttributesRecursively<TAttribute>(this PropertyInfo propertyInfo, bool searchingSuperClassesAndInterfaces = true, bool allowDescendents = true)
            //where TAttribute : Attribute
        {
            Attribute[] r = propertyInfo.GetAttributesRecursively(typeof(TAttribute), searchingSuperClassesAndInterfaces);

            if (r == null)
            {
                return new TAttribute[0];
            }

            IEnumerable<TAttribute> r2 = r.Cast<TAttribute>();
            
            return r2.ToArray();
        }

        /// <summary>
        /// Gets Attributes on the given <see cref="PropertyInfo" />.
        /// <para>
        /// The Default System has a recurse <see cref="bool" /> flag,
        /// but it doesn't do anything: no recursion takes place on
        /// <see cref="MethodInfo" /> or <see cref="PropertyInfo" /> objects.
        /// </para>
        /// <para>
        /// This is a known issue. See: http://hyperthink.net/blog/getcustomattributes-gotcha/
        /// </para>
        /// <para>
        /// This method does does.
        /// </para>
        /// </summary>
        /// <param name="propertyInfo">The property information.</param>
        /// <param name="attributeType">Type of the attribute.</param>
        /// <param name="searchingSuperClassesAndInterfaces">if set to <c>true</c> [searching super classes and interfaces].</param>
        /// <param name="allowDescendents">if set to <c>true</c> [allow descendents].</param>
        /// <returns></returns>
        public static Attribute[] GetAttributesRecursively(this PropertyInfo propertyInfo, Type attributeType,
                                                           bool searchingSuperClassesAndInterfaces = true, bool allowDescendents = true)
        {
            //Get the attributes on this PropertyInfo:
            List<Attribute> resultAttributes = allowDescendents 
// ReSharper disable UseMethodIsInstanceOfType
                //NOTE: Resharper recommends to use IsInstanceOf...but think it breaks it.
                                                   ? propertyInfo.GetCustomAttributes(true).Where(x => attributeType.IsAssignableFrom(x.GetType())).Cast<Attribute>().ToList()
// ReSharper restore UseMethodIsInstanceOfType
                                                   : propertyInfo.GetCustomAttributes(attributeType, searchingSuperClassesAndInterfaces).Cast<Attribute>().ToList();

            //If not required, let's get out now as recursion is expensive:
            if (!searchingSuperClassesAndInterfaces)
            {
                return resultAttributes.ToArray();
            }

            //But if we must, get all the superclasses and interfaces for the Type of this propertyInfo
            foreach (Type parentClassType in propertyInfo.DeclaringType.GetParentTypes())
            {

                //The Source property we are trying to match upon parent Methods, 
                //has parameters, ...which we use as filters for properties on the parent element.
                PropertyInfo parentPropertyInfo =
                    parentClassType.GetProperty(propertyInfo.Name, propertyInfo.PropertyType);


                if (parentPropertyInfo == null)
                {
                    //It's possible that this parent/anscestor 
                    //type doesn't sport this property:
                    continue;
                }


                //But if it does, get its attributes:
                // ReSharper disable UseMethodIsInstanceOfType
                //IsAssignableFrom seems to produce better results:
                IEnumerable<Attribute> found =
                    parentPropertyInfo.GetCustomAttributes()
                                      .Where(x => attributeType.IsAssignableFrom(x.GetType()))
                                      .Cast<Attribute>();
                // ReSharper restore UseMethodIsInstanceOfType

                resultAttributes.AddRange(found);

            }

            //The final result is a list of attributes, with the sub-class attributes
            //listed before attributes applied to super-classes (useful for when
            //trying to determine whether to be discard others or not):
            Attribute[] results = resultAttributes.ToArray();

            return results;
        }

    }
}