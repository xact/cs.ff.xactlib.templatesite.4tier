﻿namespace XAct.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class ListExtensions
    {
        public static IEnumerable<T> TopologicalSort<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> dependencies, bool throwOnCycle = false)
        {
            List<T> sorted = new List<T>();
            HashSet<T> visited = new HashSet<T>();

            foreach (T item in source)
            {
                Visit(item, visited, sorted, dependencies, throwOnCycle);
            }
            return sorted;
        }

        private static void Visit<T>(T item, HashSet<T> visited, List<T> sorted, Func<T, IEnumerable<T>> dependencies, bool throwOnCycle)
        {
            if (visited.Contains(item))
            {
                if (throwOnCycle)
                {
                    throw new Exception("Cyclic dependency found");
                }
            }
            else
            {
                visited.Add(item);

                T[] tmp = dependencies(item).ToArray();

                foreach (T dep in tmp)
                {
                    Visit(dep, visited, sorted, dependencies, throwOnCycle);
                }
                sorted.Add(item);
            }
        }
    }
}