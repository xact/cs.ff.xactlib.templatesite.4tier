﻿namespace XAct.Validation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using App.Core;
    using XAct.Messages;
    using XAct.Services;

    public class ValidatorServiceCacheModelItem
    {
        /// <summary>
        /// Gets or sets the type of the object to validate.
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        /// Array of PropertyValidatorAttributes applied to Properties.
        /// <para>
        /// Note that each Validator implements <see cref="IHasPropertyInfo"/>
        /// with a reference to the <see cref="PropertyInfo"/> of the object's 
        /// Property it decorates.
        /// </para>
        /// <para>
        /// The Validators are arranged in an order of dependence on others.
        /// </para>
        /// </summary>
        public IPropertyValidatorInvoker[] ValidatorAttributes { get; set; }

        /// <summary>
        /// Sparse
        /// </summary>
        public IPropertyValidatorInvoker[] ValidatorAttributes2 { get; set; }
    }

/// <summary>
/// 
/// </summary>
    public interface IValidatorServiceConfiguration : IHasAppServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a flag indicating whether the ValidatorService
        /// should check subclasses (and Interfaces) for Validator
        /// attributes, or just the given type.
        /// </summary>
        bool SearchSuperClassesAndInterfacesForValidatorAttributes { get; set; }

        /// <summary>
        /// If <see cref="SearchSuperClassesAndInterfacesForValidatorAttributes"/>
        /// is true, there's a possibility that the Validator
        /// will find two instances of the same attribute type.
        /// <para>
        /// This flag determines whether the <see cref="IValidatorService"/>
        /// uses both, or just the highest one.
        /// </para>
        /// </summary>
        bool AllowDuplicateValidatorTypes { get; set; }
    }

    public class ValidatorServiceConfiguration : IValidatorServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a flag indicating whether the ValidatorService
        /// should check subclasses (and Interfaces) for Validator
        /// attributes, or just the given type.
        /// </summary>
        public bool SearchSuperClassesAndInterfacesForValidatorAttributes { get; set; }
        /// <summary>
        /// If <see cref="SearchSuperClassesAndInterfacesForValidatorAttributes" />
        /// is true, there's a possibility that the Validator
        /// will find two instances of the same attribute type.
        /// <para>
        /// This flag determines whether the <see cref="IValidatorService" />
        /// uses both, or just the highest one.
        /// </para>
        /// </summary>
        public bool AllowDuplicateValidatorTypes { get; set; }
    }

    public class ValidatorService : IValidatorService
    {
        private readonly IMessageCodeResolvingService _messageCodeResolvingService;
        protected readonly IValidatorServiceConfiguration _validatorServiceConfiguration;

        public ValidatorService(IMessageCodeResolvingService messageCodeResolvingService, IValidatorServiceConfiguration validatorServiceConfiguration)
        {
            _messageCodeResolvingService = messageCodeResolvingService;
            _validatorServiceConfiguration = validatorServiceConfiguration;
        }

        //Reflection is expensive, so cache results:
        readonly Dictionary<Type, Tuple<PropertyInfo, PropertyValidatorAttribute[]>[]> _cachedReflection = 
            new Dictionary<Type, Tuple<PropertyInfo, PropertyValidatorAttribute[]>[]>();

        readonly Dictionary<Type,ValidatorServiceCacheModelItem> _cacheModelItems2 = 
            new Dictionary<Type, ValidatorServiceCacheModelItem>();

        /// <summary>
        /// Validates the specified object
        /// using <see cref="PropertyValidatorAttribute" />s
        /// applied to the object's Properties
        /// and/or Interface property contracts that the
        /// Properties implement.
        /// </summary>
        /// <typeparam name="T">The type of the object being validated.</typeparam>
        /// <param name="objectToValidate">The object to validate.</param>
        /// <param name="response">The response containing zero or more
        /// <see cref="XAct.Messages.Message" /> elememts of various <see cref="Severity" />.</param>
        /// <param name="exitOnFirstValidationFailure">if set to <c>true</c> [exit on first validation failure].</param>
        /// <param name="excludeTags">The contexts.</param>
        /// <returns>
        /// The <see cref="IResponse.Success" /> value.
        /// </returns>
        public bool Validate<T>(T objectToValidate, IResponse response, bool exitOnFirstValidationFailure = true, params string[] excludeTags)
        {
            return Validate(typeof (T), objectToValidate, response, exitOnFirstValidationFailure,
                _validatorServiceConfiguration.SearchSuperClassesAndInterfacesForValidatorAttributes,
                _validatorServiceConfiguration.AllowDuplicateValidatorTypes,
                excludeTags);
        }


        public bool Validate<T>(T objectToValidate, IResponse response, bool exitOnFirstValidationFailure = true, bool searchSuperClassesAndInterfacesForValidatorAttributes = true, bool allowDuplicateAttributeTypes = false, params string[] excludeTags)
        {
            return Validate(typeof(T), objectToValidate, response, exitOnFirstValidationFailure, searchSuperClassesAndInterfacesForValidatorAttributes,allowDuplicateAttributeTypes, excludeTags);
        }


        /// <summary>
        /// Validates the specified object
        /// using <see cref="PropertyValidatorAttribute" />s
        /// applied to the object's Properties
        /// and/or Interface property contracts that the
        /// Properties implement.
        /// </summary>
        /// <param name="typeOfObectToValidate">The type of obect to validate.</param>
        /// <param name="objectToValidate">The object to validate.</param>
        /// <param name="response">The response containing zero or more
        /// <see cref="XAct.Messages.Message" /> elememts of various <see cref="Severity" />.</param>
        /// <param name="exitOnFirstValidationFailure">if set to <c>true</c> [exit on first validation failure].</param>
        /// <param name="excludeTags">The contexts.</param>
        /// <returns>
        /// The <see cref="IResponse.Success" /> value.
        /// </returns>
        public bool Validate(Type typeOfObectToValidate, object objectToValidate, IResponse response,
                             bool exitOnFirstValidationFailure = true, params string[] excludeTags)
        {
            //IMPORTANT:
            //The naive way to validate an object is to iterate through each Property.
            //This unfortunately doesn't account for the issue of Precendence as is required to handle cases 
            //when validators are Conditional on the results of another (named) validator(s).

            //A case in point is that Required Validators have to be processed before other Validators.


            return TheMoreElegantWay(
                        typeOfObectToValidate, 
                        objectToValidate, 
                        response, 
                        exitOnFirstValidationFailure,
                        _validatorServiceConfiguration.SearchSuperClassesAndInterfacesForValidatorAttributes,
                        _validatorServiceConfiguration.AllowDuplicateValidatorTypes,
                        excludeTags);
        }

        public bool Validate(Type typeOfObectToValidate, object objectToValidate, IResponse response, bool exitOnFirstValidationFailure = true, bool searchSuperClassesAndInterfacesForValidatorAttributes = true, bool allowDuplicateAttributeTypes = false, params string[] excludeTags)
        {
            return TheMoreElegantWay(
                        typeOfObectToValidate,
                        objectToValidate,
                        response,
                        exitOnFirstValidationFailure,
                        searchSuperClassesAndInterfacesForValidatorAttributes,
                        allowDuplicateAttributeTypes,
                        excludeTags);
        }

        public ValidatorServiceCacheModelItem GetSchema(Type typeOfObectToValidate)
        {
            return GetSchema(typeOfObectToValidate,
                                      _validatorServiceConfiguration.SearchSuperClassesAndInterfacesForValidatorAttributes,
                                      _validatorServiceConfiguration.AllowDuplicateValidatorTypes);
        }


        public ValidatorServiceCacheModelItem GetSchema(Type typeOfObectToValidate, bool searchSuperClassesAndInterfaces,
                                                        bool allowDuplicates)
        {
            return RetrieveTypeSchema(typeOfObectToValidate, searchSuperClassesAndInterfaces, allowDuplicates);
        }






















        /// <summary>
        /// Thes the more elegant way.
        /// <para>
        /// If we are including superclasses, there's a probability that on one property there
        /// will be more than one rule (eg, from subclass, there's a maxLength of 200, on a superclass or interface,
        /// a maxLength of 50...and 50 will fail the Validation -- when we wanted to have only the maxLength=200 checked.
        /// In such cases, we want to <see cref="excludeContexts"/> the Tag applied to the maxLength=50 rule.
        /// </para>
        /// </summary>
        /// <param name="typeOfObectToValidate">The type of obect to validate.</param>
        /// <param name="objectToValidate">The object to validate.</param>
        /// <param name="response">The response.</param>
        /// <param name="exitOnFirstValidationFailure">if set to <c>true</c> [exit on first validation failure].</param>
        /// <param name="searchSuperClassesAndInterfacesForValidatorAttributes">if set to <c>true</c> [search super classes and interfaces for validator attributes].</param>
        /// <param name="allowDuplicateAttributeTypes">if set to <c>true</c> [allow duplicate attribute types].</param>
        /// <param name="includeContexts">The include contexts.</param>
        /// <param name="excludeContexts">The exclude contexts.</param>
        /// <returns></returns>
        private bool TheMoreElegantWay(Type typeOfObectToValidate, object objectToValidate, IResponse response,
                                       bool exitOnFirstValidationFailure,
            bool searchSuperClassesAndInterfacesForValidatorAttributes, bool allowDuplicateAttributeTypes, 
            string[] excludeContexts)
        {

            //The cach contains two sets of reflection -- one deep, one shallow (limited to the item and no superclasses or interfaces).
            ValidatorServiceCacheModelItem typeSchema = RetrieveTypeSchema(typeOfObectToValidate, 
                searchSuperClassesAndInterfacesForValidatorAttributes, 
                allowDuplicateAttributeTypes);

            //Depending on the given flag, we choose one of the set:
            IPropertyValidatorInvoker[] propertyValidatorAttributes =
                (searchSuperClassesAndInterfacesForValidatorAttributes)
                    ? typeSchema.ValidatorAttributes //Full (with SuperClasses)
                    : typeSchema.ValidatorAttributes2 //Thin
                    ;

            ////Exclude some conditions.
            //propertyValidatorAttributes = ResizeCollection(propertyValidatorAttributes, includeContexts);
            

            //propertyValidatorAttributes = ResizeCollectionIfTagMatchesGivenCondition(propertyValidatorAttributes,includeContexts);



            //If we are including superclasses, there's a probability that on one property there
            //will be more than one rule (eg, from subclass, there's a maxLength of 200, on a superclass or interface,
            //a maxLength of 50...and 50 will fail the Validation -- when we wanted to have only the 200 checked.
            //In such cases, we want to Exclude the Tag applied to the 50 rule.
            propertyValidatorAttributes = 
                StripOutPropertiesMarkedWithExcludeContextTag(propertyValidatorAttributes,excludeContexts);


            Dictionary<string, bool> conditionalResults = new Dictionary<string, bool>();

            foreach (IPropertyValidatorInvoker propertyValidatorAttribute in propertyValidatorAttributes)
            {

                //If the attribute has a ConditionalOn, it's value will be in the conditionalResults dictionary
                //check it. If false, don't bother going further if it is false:
                if (!EnsureAllPrecendentConditionsAreMet(propertyValidatorAttribute, conditionalResults))
                {
                    //On to next property to check...
                    continue;
                }

                //If the check is only a condition, we will want result, but
                //not the messages, so we create a dummy response object to give to the validate
                //method, so we can get its Success flag, while not adding messages to original response
                //object:
                IResponse response2 = (propertyValidatorAttribute.IsOnlyAConditionFlag) ? new Response() : response;

                //Validate, and get messages:
                bool validationResult = propertyValidatorAttribute.Validate(
                    objectToValidate, 
                    propertyValidatorAttribute.PropertyInfo, 
                    response2);


                //If it has a name, save results, so that further
                //elements can refer back to its result (within EnsureAllPrecendentConditionsAreMet)
                //(if they have ConditionalOn pointing back to this element's Tag.
                if (!propertyValidatorAttribute.Tag.IsNullOrEmpty())
                {
                    conditionalResults[propertyValidatorAttribute.Tag] = validationResult;
                }

                //As it is a flag, we don't want to stop validating either way
                if (propertyValidatorAttribute.IsOnlyAConditionFlag)
                {
                    //So now that result was saved in ConditionalResults, 
                    //we can safely reset it to true, because we don't care whether false or true
                    //we don't want to stop validating.
                    validationResult = true;
                }

                //Iterate deeper:
                IterateThroughNestedObjects(objectToValidate, response, exitOnFirstValidationFailure,
                                            propertyValidatorAttribute, validationResult,
                                            searchSuperClassesAndInterfacesForValidatorAttributes,
                                            allowDuplicateAttributeTypes,
                                            excludeContexts);

                //In most cases, we want to ge out early, after the first failure
                //but there are times we want a list of everything that failed validation.
                if ((!validationResult) && (exitOnFirstValidationFailure))
                {
                    return false;
                }

            }
            return response.Success;
        }

        private void IterateThroughNestedObjects(object objectToValidate, IResponse response, bool exitOnFirstValidationFailure,
                                                 IPropertyValidatorInvoker propertyValidatorAttribute, bool validationResult,
                                                 bool searchSuperClassesAndInterfacesForValidatorAttributes,
            bool allowDuplicateAttributeTypes,
            string[] excludeContexts
            )
        {
            INestedValidator nestedValidator = propertyValidatorAttribute as INestedValidator;

            if (nestedValidator == null)
            {
                return;
            }

            //Still needs work (as not dealing with IEnumerable condition)
            PropertyInfo nestedPropertyInfo = propertyValidatorAttribute.PropertyInfo;


            object nestedObject = nestedPropertyInfo.GetValue(objectToValidate, BindingFlags.Instance|BindingFlags.Public, null,null,null);
            if (nestedObject != null)
            {
                Type enumerableType = GetEnumarableType(nestedPropertyInfo.PropertyType);

                if (enumerableType == null)
                {
                    TheMoreElegantWay(
                        nestedPropertyInfo.PropertyType,
                        nestedObject,
                        response,
                        exitOnFirstValidationFailure,
                        searchSuperClassesAndInterfacesForValidatorAttributes,
                        allowDuplicateAttributeTypes,
                        excludeContexts);
                }
                else
                {
                    //It's enumerable:

                    IEnumerator enumerator = ((IEnumerable)nestedObject).GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        object subObject = enumerator.Current;

                        TheMoreElegantWay(
                            enumerableType,
                            subObject,
                            response,
                            exitOnFirstValidationFailure,
                            searchSuperClassesAndInterfacesForValidatorAttributes,
                            allowDuplicateAttributeTypes,
                            excludeContexts);
                    }
                }
            }

            bool success = response.Success;
        }


        Type GetEnumarableType(IEnumerable v)
        {
            if (v == null)
            {
                return null;
            }
            return GetEnumarableType(v.GetType());
        }

        Type GetEnumarableType(Type t)
        {
            Type[] interfaces = t.GetInterfaces();
            foreach (Type i in interfaces)
            {
                if (i.IsGenericType && i.GetGenericTypeDefinition().Equals(typeof(IEnumerable<>)))
                {
                    return i.GetGenericArguments()[0];
                }
            }
            return null;
        }

        private static bool EnsureAllPrecendentConditionsAreMet(IPropertyValidatorInvoker propertyValidatorAttribute, Dictionary<string, bool> conditionalResults)
        {

            
            if (propertyValidatorAttribute.IsConditionalOn.IsNullOrEmpty())
            {
                return true;
            }

            //It's conditional. But was the element it was conditional on, successful?
            foreach (string conditionalOn in propertyValidatorAttribute.IsConditionalOn.Split(
                new char[]{',', ';', '|'}, StringSplitOptions.RemoveEmptyEntries)
                )
            {
                bool precendentWasSuccessful;
                conditionalResults.TryGetValue(conditionalOn, out precendentWasSuccessful);

                if (!precendentWasSuccessful)
                {
                    return false;
                }
            }

            return true;
        }


        private IPropertyValidatorInvoker[] ResizeCollection(IPropertyValidatorInvoker[] propertyValidatorAttributes, string[] includeContexts)
        {
            if ((includeContexts == null) || (includeContexts.Length == 0))
            {
                return propertyValidatorAttributes;
            }

            //We're looking for cases where the properties have a tag that matches 
            //the given scenario:
            IPropertyValidatorInvoker[] matched =
                propertyValidatorAttributes.Where(x => includeContexts.Contains(y => y == x.Tag)).ToArray();


            if (matched.Length == 0)
            {
                //Tag not found -- no changes:
                return propertyValidatorAttributes;
            }

            //Before doing any changes, we need to copy the array in order 
            //for RemoveAll to not damage the 
            //original list, which we will be using under different Contexts:
            List<IPropertyValidatorInvoker> propertyValidatorAttributes2 = new List<IPropertyValidatorInvoker>();
            propertyValidatorAttributes2.AddRange(propertyValidatorAttributes);


            //We found one property that has the 'include' tag mentioned.

            //In such cases, we only need to use of the validators applied to that propertyinfo.
            //All the other ones, we get rid of -- except those on which they have ConditionOn...

            //oh hell...then those too have to be brought back in... etc.
            //it's
            PropertyInfo[] pis = matched.Select(x => x.PropertyInfo).ToArray();

            propertyValidatorAttributes2.RemoveAll(x => pis.Contains(x.PropertyInfo) && !matched.Contains(x));

            return propertyValidatorAttributes2.ToArray();
        }

        
        //private static IPropertyValidatorInvoker[] ResizeCollectionIfTagMatchesGivenCondition(IPropertyValidatorInvoker[]
        //                                                                                           propertyValidatorAttributes,
        //    string[] includeContexts
        //                                                                                       )
        //{
        //    if (includeContexts == null || includeContexts.Length == 0)
        //    {
        //        return propertyValidatorAttributes;
        //    }


        //    foreach (
        //        IPropertyValidatorInvoker propertyValidatorInvoker in
        //            includeContexts.Select(id => propertyValidatorAttributes.FirstOrDefault(x => x.Tag == id))
        //                    .Where(tmp => tmp != null))
        //    {
        //        //There's a tag with the name on it. This is the only one we will be checking.
        //        propertyValidatorAttributes = new[] {propertyValidatorInvoker};
        //        break;
        //    }

        //    return propertyValidatorAttributes;
        //}


        //If we are including superclasses, there's a probability that on one property there
        //will be more than one rule (eg, from subclass, there's a maxLength of 200, on a superclass or interface,
        //a maxLength of 50...and 50 will fail the Validation -- when we wanted to have only the 200 checked.
        //In such cases, we want to Exclude the Tag applied to the 50 rule.
        private IPropertyValidatorInvoker[] StripOutPropertiesMarkedWithExcludeContextTag(
            IPropertyValidatorInvoker[] propertyValidatorAttributes,
            string[] excludeContexts)
        {
            if ((excludeContexts == null) || (excludeContexts.Length == 0))
            {
                //No change
                return propertyValidatorAttributes;
            }      //We're looking for cases where the properties have a tag that matches 
            //the given scenario:
            IPropertyValidatorInvoker[] matched =
                propertyValidatorAttributes.Where(x => excludeContexts.Contains(y => y == x.Tag)).ToArray();


            if (matched.Length == 0)
            {
                //Tag not found -- no changes:
                return propertyValidatorAttributes;
            }

            //Before doing any changes, we need to copy the array in order 
            //for RemoveAll to not damage the 
            //original list, which we will be using under different Contexts:
            List<IPropertyValidatorInvoker> propertyValidatorAttributesClone = new List<IPropertyValidatorInvoker>();
            propertyValidatorAttributesClone.AddRange(propertyValidatorAttributes);

            //Remove all that were found to have the given tag:
            propertyValidatorAttributesClone.RemoveAll(matched.Contains);

            return propertyValidatorAttributesClone.ToArray();
        }


        protected ValidatorServiceCacheModelItem RetrieveTypeSchema(Type typeOfObjectToValidate, bool searchingSuperClassesAndInterfaces, bool allowDuplicateAttributeTypes)
        {
            ValidatorServiceCacheModelItem typeSchema;

            if (_cacheModelItems2.TryGetValue(typeOfObjectToValidate, out typeSchema))
            {
                return typeSchema;
            }


            typeSchema = new ValidatorServiceCacheModelItem();

            //GET FULL SET:
            IEnumerable<IPropertyValidatorInvoker> propertyValidatorAttribute =
                CreateUnorderedListOfPropertyValidatorAttributes(
                    typeOfObjectToValidate,
                    true);


            IPropertyValidatorInvoker[] results =
                OrderListOfPropertyValidatorAttributes(propertyValidatorAttribute).ToArray();


            typeSchema.ValidatorAttributes = results;



            //GET SPARSE SET
            propertyValidatorAttribute =
                CreateUnorderedListOfPropertyValidatorAttributes(
                    typeOfObjectToValidate,
                    false);
            results =
                OrderListOfPropertyValidatorAttributes(propertyValidatorAttribute).ToArray();
            typeSchema.ValidatorAttributes2 = results;
            _cacheModelItems2[typeOfObjectToValidate] = typeSchema;



            //We now have both sets of attributes -- attributes on object only,
            //and full tree of superclasses and )

            return typeSchema;
        }

        private IEnumerable<IPropertyValidatorInvoker> CreateUnorderedListOfPropertyValidatorAttributes(Type typeOfObjectToValidate, bool searchingSuperClassesAndInterfaces)
        {
            List<IPropertyValidatorInvoker> results = new List<IPropertyValidatorInvoker>();


            PropertyInfo[] propertyInfos = typeOfObjectToValidate.GetProperties();
            
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                //Get Attributes on this property, as well as all 
                //virtual base classes *and* interfaces (ie, more than what the .NET framework 
                //does by default).
                IPropertyValidatorInvoker[] propertyValidatorAttributes =
                    propertyInfo.GetAttributesRecursively<IPropertyValidatorInvoker>( 
                        searchingSuperClassesAndInterfaces: searchingSuperClassesAndInterfaces,
                        allowDescendents: true
                        );


                // Ensures the required validators are processed first
                // by ensuring Required validators have a Tag (a Guid is given if not)
                // and all others are made ConditionalOn, pointing back at it.
                EnsureRequiredValidatorsAreProcessedFirst(propertyValidatorAttributes);


                foreach (PropertyValidatorAttribute propertyValidatorAttribute in propertyValidatorAttributes)
                {
                    //Inject the PropertyInfo into the target object
                    //for later reference by the Validate method:
                    propertyValidatorAttribute.PropertyInfo = propertyInfo;

                    IHasMessageCodeIdentifier messageCodeIdentifier =
                        propertyValidatorAttribute as IHasMessageCodeIdentifier;

                    //For those that are marked as being MessageCode based, 
                    //resolve the string into the MessageCode:
                    ResolveMessageCode(messageCodeIdentifier);
                }

                results.AddRange(propertyValidatorAttributes);
            }
            return results;
        }


        /// <summary>
        /// Ensures the required validators are processed first
        /// by ensuring Required validators have a Tag (a Guid is given if not)
        /// and all others are made ConditionalOn, pointing back at it.
        /// </summary>
        /// <param name="propertyValidatorAttributes">The property validator attributes.</param>
        /// <returns></returns>
        protected IPropertyValidatorInvoker[] EnsureRequiredValidatorsAreProcessedFirst(
            IPropertyValidatorInvoker[] propertyValidatorAttributes)
        {
            Type requiredAttributeType = typeof (RequiredPropertyValidatorAttribute);

            IEnumerable<IPropertyValidatorInvoker> requiredAttributes =
                propertyValidatorAttributes.Where(x => requiredAttributeType.IsAssignableFrom(x.GetType()));

            IPropertyValidatorInvoker requiredAttribute = requiredAttributes.FirstOrDefault();


            if (requiredAttribute == null)
            {
                return propertyValidatorAttributes;
            }

            string tag = requiredAttribute.Tag;
            if (tag.IsNullOrEmpty())
            {
                requiredAttribute.Tag = tag = Guid.NewGuid().ToString();
            }

            //propertyValidatorAttributes.Where(x => !requiredAttributes.Contains(x)).ForEach(x => x.ConditionalOn = SafePrepend(x.ConditionalOn,tag));
            //Other requires will have no name or conditional on, and therefore go last.
            //Include even other Requires, in case others in turn have conditionals on them...
            propertyValidatorAttributes.Where(x => x!=requiredAttribute).ForEach(x => x.IsConditionalOn = SafePrepend(x.IsConditionalOn,tag));

            return propertyValidatorAttributes;
        }

        /// <summary>
        /// For Validators marked as being MessageCode based,
        /// using the MessageCodeResolvingService to 
        /// find the MessageCode.
        /// </summary>
        /// <param name="messageCodeIdentifier">The message code identifier.</param>
        private void ResolveMessageCode(IHasMessageCodeIdentifier messageCodeIdentifier)
        {
            //MessageCode is a struct. 
            //Therefore not allowable as a compile time Attribute parameter.
            //Therefore we need an external service to translate a given string
            //to a MessageCode.
            if (messageCodeIdentifier == null)
            {
                return;
            }

            MessageCode messageCode =
                _messageCodeResolvingService.Resolve(messageCodeIdentifier.MessageCodeIdentifier);

            messageCodeIdentifier.Initialize(messageCode);
        }

        private static string SafePrepend(string text1, string prefix, string separator=";")
        {
            if (text1.IsNullOrEmpty())
            {
                return prefix;
            }
            return prefix + separator + text1;
        }


        protected IEnumerable<IPropertyValidatorInvoker> OrderListOfPropertyValidatorAttributes(IEnumerable<IPropertyValidatorInvoker> propertyValidatorAttributes)
        {

            IPropertyValidatorInvoker[] results = 
                propertyValidatorAttributes
                    .TopologicalSort(x => 
                        propertyValidatorAttributes
                            .Where(y =>
                                       {
                                           if (x.IsConditionalOn.IsNullOrEmpty())
                                           {
                                               return false;
                                           }
                                           return
                                               x.IsConditionalOn.Split(new[] {';', '|', ','},
                                                                     StringSplitOptions.RemoveEmptyEntries)
                                                .Contains(y.Tag);
                                       })).ToArray();

            return results;
        }


    }
}