﻿namespace XAct.Validation
{
    using System;
    using XAct.Messages;

    /// <summary>
    /// Contract for a service to validate entites
    /// whose members are decorated with
    /// <see cref="PropertyValidatorAttribute" />
    /// </summary>
    public interface IValidatorService
    {


        /// <summary>
        /// Validates the specified object.
        /// <para>
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToValidate">The object to validate.</param>
        /// <param name="response">The response.</param>
        /// <param name="exitOnFirstValidationFailure">if set to <c>true</c> [exit on first validation failure].</param>
        /// <param name="excludeTags">Sometimes a validator needs to be ignored under certain conditions. If so, name the Validators to exclude using CSV.</param>
        /// <returns></returns>
        bool Validate<T>(T objectToValidate, IResponse response, bool exitOnFirstValidationFailure = true, params string[] excludeTags);


        /// <summary>
        /// Validates the specified object
        /// using <see cref="PropertyValidatorAttribute"/>s
        /// applied to the object's Properties 
        /// and/or Interface property contracts that the
        /// Properties implement.
        /// </summary>
        /// <typeparam name="T">The type of the object being validated.</typeparam>
        /// <param name="objectToValidate">The object to validate.</param>
        /// <param name="response">The response containing zero or more
        /// <see cref="XAct.Messages.Message"/> elememts of various <see cref="Severity"/>.</param>
        /// <param name="exitOnFirstValidationFailure">if set to <c>true</c> [exit on first validation failure].</param>
        /// <param name="excludeTags">The contexts.</param>
        /// <returns>The <see cref="IResponse.Success"/> value.</returns>
        bool Validate<T>(T objectToValidate, IResponse response, bool exitOnFirstValidationFailure = true, bool allowInheritence = true, bool allowDuplicateAttributeTypes = false, params string[] excludeTags);



        /// <summary>
        /// Validates the specified object
        /// using <see cref="PropertyValidatorAttribute" />s
        /// applied to the object's Properties
        /// and/or Interface property contracts that the
        /// Properties implement.
        /// </summary>
        /// <param name="typeOfObjectToValidate">The type of the object being validated.</param>
        /// <param name="objectToValidate">The object to validate.</param>
        /// <param name="response">The response containing zero or more
        /// <see cref="XAct.Messages.Message" /> elememts of various <see cref="Severity" />.</param>
        /// <param name="exitOnFirstValidationFailure">if set to <c>true</c> [exit on first validation failure].</param>
        /// <param name="excludeTags">The contexts.</param>
        /// <returns>
        /// The <see cref="IResponse.Success" /> value.
        /// </returns>
        bool Validate(Type typeOfObjectToValidate, object objectToValidate, IResponse response, bool exitOnFirstValidationFailure = true, params string[] excludeTags);



        /// <summary>
        /// Validates the specified object
        /// using <see cref="PropertyValidatorAttribute" />s
        /// applied to the object's Properties
        /// and/or Interface property contracts that the
        /// Properties implement.
        /// </summary>
        /// <param name="typeOfObjectToValidate">The type of the object being validated.</param>
        /// <param name="objectToValidate">The object to validate.</param>
        /// <param name="response">The response containing zero or more
        /// <see cref="XAct.Messages.Message" /> elememts of various <see cref="Severity" />.</param>
        /// <param name="exitOnFirstValidationFailure">if set to <c>true</c> [exit on first validation failure].</param>
        /// <param name="allowInheritence">if set to <c>true</c> [allow inheritence].</param>
        /// <param name="allowDuplicateAttributeTypes">if set to <c>true</c> [allow duplicate attribute types].</param>
        /// <param name="excludeTags">The contexts.</param>
        /// <returns>
        /// The <see cref="IResponse.Success" /> value.
        /// </returns>
        bool Validate(Type typeOfObjectToValidate, object objectToValidate, IResponse response, bool exitOnFirstValidationFailure = true, bool allowInheritence = true, bool allowDuplicateAttributeTypes = false, params string[] excludeTags);
         

        /// <summary>
        /// Gets the ordered list of validators applied to given object.
        /// </summary>
        /// <param name="typeOfObectToValidate">The type of obect to validate.</param>
        /// <returns></returns>
        ValidatorServiceCacheModelItem GetSchema(Type typeOfObectToValidate);

        ValidatorServiceCacheModelItem GetSchema(Type typeOfObectToValidate, bool searchSuperClassesAndInterfaces, bool allowDuplicateValidatorTypes);
    }
}