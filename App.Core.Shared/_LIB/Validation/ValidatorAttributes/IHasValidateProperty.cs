﻿namespace XAct.Validation
{
    using System.Reflection;
    using XAct.Messages;

    /// <summary>
    /// Invoked by an implementation of 
    /// <see cref="IValidatorService"/>
    /// <para>
    /// Implemented by <see cref="PropertyValidatorAttribute"/>
    /// </para>
    /// </summary>
    public interface IHasValidateProperty
    {
        /// <summary>
        /// Validate the element's property.
        /// <para>
        /// Invoked by <see cref="IValidatorService"/>.
        /// </para>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="propertyInfo"></param>
        /// <param name="response"></param>
        /// <returns>The <see cref="IResponse.Success"/> value.</returns>
        bool Validate(object model, PropertyInfo propertyInfo, IResponse response);
    }
}