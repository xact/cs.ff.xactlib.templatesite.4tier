namespace XAct.Validation
{

    public class RequiredPropertyValidatorAttribute : MessageCodePropertyValidatorAttribute
    {
        public RequiredPropertyValidatorAttribute(string messageCodeIdentifier)
            : base(messageCodeIdentifier)
        {

        }


        protected override bool Validate(object entity, System.Reflection.PropertyInfo propertyInfo, object value,
                                         Messages.IResponse response)
        {

            return Assert(response, ()=>value.IsDefaultOrNotInitialized()==false);
        }
    }
}