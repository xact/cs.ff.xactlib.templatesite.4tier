﻿namespace XAct.Validation
{
    using System;
    using System.Globalization;

    public class StringIsParsableAsDateTimeValidatorAttribute : MessageCodePropertyValidatorAttribute
    {
        public string Pattern { get; set; }

        public StringIsParsableAsDateTimeValidatorAttribute(string messageCodeIdentifier)
            : base(messageCodeIdentifier)
        {
            Pattern = "yyyyMMdd";
        }

        protected override bool Validate(object entity, System.Reflection.PropertyInfo propertyInfo, object value,
                                         Messages.IResponse response)
        {

            if (value != null)
            {
                string typedValue = value.ToString();

                if (!typedValue.IsNullOrEmpty())
                {
                    DateTime tmp;
                    bool result = base.Assert(response,
                                              () =>
                                              (DateTime.TryParseExact(
                                                  typedValue, Pattern, null, DateTimeStyles.None, out tmp)));
                    return result;
                }


            }
            return response.Success;
        }
    }
}