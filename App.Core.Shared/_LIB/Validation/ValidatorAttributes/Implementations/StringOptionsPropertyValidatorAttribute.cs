﻿namespace XAct.Validation
{

    public class StringOptionsPropertyValidatorAttribute : MessageCodePropertyValidatorAttribute
    {

        public string CSVOptions 
        {
            get { return _csvOptions; }
            set
            {
                _csvOptions = value;
                _options = _csvOptions.Split(new char[] {',', ';', '|'});
            }
        }

        public bool CaseSensitive { get; set; }

        public string[] Options { get { return _options; } }
        public string[] _options;
        private string _csvOptions;


        public StringOptionsPropertyValidatorAttribute(string messageCodeIdentifier)
            : base(messageCodeIdentifier)
        {
        }

        protected override bool Validate(object entity, System.Reflection.PropertyInfo propertyInfo, object value,
                                         Messages.IResponse response)
        {

            if (value == null)
            {
                return response.Success;
            }

            string typedValue = value.ToString();

            bool result = typedValue.IsNullOrEmpty();

            if (result)
            {
                return response.Success;
            }

            //if (!result)
            //{
            //    return base.Assert(response, () => result);
            //}



            result = _options.Contains(x => string.Compare(x, typedValue, !CaseSensitive)==0);

            return base.Assert(response, () => result);
        }
    }


}