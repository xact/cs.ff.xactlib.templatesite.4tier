﻿namespace XAct.Validation
{
    public class StringNumberRangePropertyValidatorAttribute : MessageCodePropertyValidatorAttribute
    {
        public long Min { get; set; }
        public long Max { get; set; }

        public StringNumberRangePropertyValidatorAttribute(string messageCodeIdentifier):base(messageCodeIdentifier)
        {
            Min = long.MinValue;
            Max = long.MaxValue;
        }

        protected override bool Validate(object entity, System.Reflection.PropertyInfo propertyInfo, object value,
                                         Messages.IResponse response)
        {
            //Don't use the built in logic:
            //return base.Validate(entity, propertyInfo, value, response);

            //provide custom:
            if (value != null)
            {
                string typedValue = value.ToString();
                
                long tmp = 0;
                if (!typedValue.IsNullOrEmpty())
                {
                    bool result = base.Assert(response,
                                              () =>
                                              (long.TryParse(typedValue, out tmp)));
                    
                }

                return base.Assert(response, () => tmp >= Min && tmp <= Max);
            }
            return response.Success;    
 
        }

    }

}