﻿namespace XAct.Validation
{
    using System;
    using System.Reflection;
    using XAct.Messages;

    /// <summary>
    /// Implements <see cref="IPropertyValidatorInvoker" />, which subclasses
    /// <see cref="IHasValidateProperty" /> and <see cref="IHasValidatorType" />
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class PropertyValidatorAttribute : Attribute, IPropertyValidatorInvoker
    {
        /// <summary>
        /// Gets or sets a value indicating whether the validation
        /// is used only to set a flag, that a later validator
        /// can use to skip it's validation.
        /// <para>
        /// In other words, if A is marked with `IsAcondition`,
        /// and B is marked with ConditionalOn="A", if A is false,
        /// validation still says it's true, and skips checking B.
        /// </para>
        /// <para>
        /// On the other hand, if B is marked as conditional on A,
        /// and is not marked with IsACondition, and A fails, then
        /// A's validation failure is treated as a validation failure,
        /// and even if B is skipped, the whole validation is
        /// still going to marked as false...
        /// </para>
        /// </summary>
        public bool IsOnlyAConditionFlag { get; set; }

        /// <summary>
        /// Validate this property after the specified
        /// property, and only if it validated as true.
        /// </summary>
        public string IsConditionalOn { get; set; }

        /// <summary>
        /// Gets or sets the optional unique
        /// tag used to identify this single 
        /// Attribute, among other PropertyValdiatorAttributes applied 
        /// to the Property.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public string Tag { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="PropertyInfo" />
        /// describing the Property that the
        /// <see cref="PropertyValidatorAttribute" />
        /// is decorating.
        /// <para>
        /// Note that this is injected by <see cref="IValidatorService" />
        /// when invoking the <see cref="PropertyValidatorAttribute" />s
        /// <see cref="IHasValidateProperty.Validate" /> method.
        /// </para>
        /// </summary>
        public PropertyInfo PropertyInfo { get; set; }


        /// <summary>
        /// Optional override of the <see cref="PropertyInfo"/>.Name.
        /// </summary>
        public string PropertyName
        {
            get
            {
                if (_propertyName != null)
                {
                    return _propertyName;
                }
                else
                {
                    return (PropertyInfo!=null)? PropertyInfo.Name:null;
                }
            }
            set { _propertyName = value; }
        }

        /// <summary>
        /// Validates the specified entity
        /// by invoking <see cref="PropertyValidator"/>
        /// to instantiate a Validator, and invoke 
        /// its <see cref="IPropertyValidator.Validate"/>
        /// method.
        /// <para>
        /// IMPORTANT: In most simple cases, 
        /// the method is overridden in a subclass attribute
        /// to provide the validation logic directly in the 
        /// Attribute.
        /// </para>
        /// <para>
        /// But there are cases -- notably when the service 
        /// requires DI of external services, that it is 
        /// appropriate to keep the Attribute separate from the
        /// actual logic in the form of a <see cref="IPropertyValidator"/>.
        /// </para>
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="propertyInfo">The property information.</param>
        /// <param name="value">The value.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        protected virtual bool Validate(object entity, PropertyInfo propertyInfo, object value, IResponse response)
        {
            return PropertyValidator.Validate(entity, propertyInfo, this, value, response);
        }


        /// <summary>
        /// Gets or sets the Type of the
        /// <see cref="IPropertyValidator" />
        /// to invoke.
        /// <para>
        /// Invoked by an implementation of
        /// <see cref="IValidatorService" />
        /// </para>
        /// </summary>
        /// <value>
        /// The type of the validator.
        /// </value>
        public virtual Type ValidatorType { get; set; }

        protected IPropertyValidator PropertyValidator
        {
            get
            {
                return (ValidatorType != null)
                           ? (_PropertyValidator ??
                              (_PropertyValidator =
                               (IPropertyValidator) DependencyResolver.Current.GetInstance(ValidatorType)))
                           : null;
            }
        }

        private IPropertyValidator _PropertyValidator;
        private string _propertyName;


        /// <summary>
        /// Validates the specified entity.
        /// <para>
        /// Invoked by <see cref="IValidatorService"/>
        /// </para>
        /// <para>
        /// Extracts the property value from the entity,
        /// and invokes the <see cref="Validate"/> method
        /// which by default instantiates the 
        /// <see cref="ValidatorType"/> and invokes its
        /// <see cref="IPropertyValidator.Validate"/> method.
        /// </para>
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="propertyInfo">The property information.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        public bool Validate(object entity, PropertyInfo propertyInfo, IResponse response)
        {
            //Using the PropertyInfo, get the entity's Value:
            object value = propertyInfo.GetValue(entity, null);

            //Console.WriteLine("{0}:{1}:{2}:{3}".FormatStringCurrentUICulture(entity.GetType().Name, propertyInfo.Name,
            //                                                                 this.GetType().Name, value));

            //Validate the Value, using 
            //the PropertyValidator,
            //this Attribute (so that the Validator can query it for its properties, such as Min, Max, Pattern, etc.)
            return this.Validate(entity, propertyInfo, value, response);
        }

        public override string ToString()
        {
            return "Property:{0}, Type:{1}, Tag: {2}, ConditionalOn:{3}".FormatStringInvariantCulture(this.PropertyInfo.Name, this.GetType().Name, this.Tag, this.IsConditionalOn);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            IPropertyValidatorInvoker typedObj = obj as IPropertyValidatorInvoker;
            return typedObj != null && Equals(typedObj);

        }

        public bool Equals(IPropertyValidatorInvoker other)
        {
            if (other == null)
                return false;

            string tmp1 = this.PropertyInfo.Name + ":" + this.GetType().Name + ":" + this.Tag;
            string tmp2 = other.PropertyInfo.Name + ":" + other.GetType().Name + ":" + other.Tag;
            if (tmp1 == tmp2)
                return true;
            else
                return false;
        }

    }


}