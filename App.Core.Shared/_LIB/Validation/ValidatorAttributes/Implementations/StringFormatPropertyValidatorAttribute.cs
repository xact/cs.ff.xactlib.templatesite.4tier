﻿namespace XAct.Validation
{
    using System.Text.RegularExpressions;

    public class StringFormatPropertyValidatorAttribute : MessageCodePropertyValidatorAttribute
    {
        public string Pattern { get; set; }

        public StringFormatPropertyValidatorAttribute(string messageCodeIdentifier)
            : base(messageCodeIdentifier)
        {
        }

        protected override bool Validate(object entity, System.Reflection.PropertyInfo propertyInfo, object value,
                                         Messages.IResponse response)
        {
            if (value == null)
            {
                return response.Success;
            }

            string typedValue = value.ToString();
            if (typedValue.IsNullOrEmpty())
            {
                return response.Success;
            }
            Match match = System.Text.RegularExpressions.Regex.Match(typedValue, Pattern);

            return base.Assert(response, () => match.Success);

        }
    }

}