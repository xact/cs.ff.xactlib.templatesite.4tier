﻿namespace XAct.Validation
{
    public class MinStringLengthPropertyValidatorAttribute : MessageCodePropertyValidatorAttribute
    {
        public int MinLength { get; set; }

        public MinStringLengthPropertyValidatorAttribute(string messageCodeIdentifier)
            : base(messageCodeIdentifier)
        {
        }

        protected override bool Validate(object entity, System.Reflection.PropertyInfo propertyInfo, object value,
                                         Messages.IResponse response)
        {

            if (value == null)
            {
                return base.Assert(response, () => false);

            }
            string typedValue = value.ToString();

            return base.Assert(response, () => (typedValue.Length >= MinLength));
        }
    }
}