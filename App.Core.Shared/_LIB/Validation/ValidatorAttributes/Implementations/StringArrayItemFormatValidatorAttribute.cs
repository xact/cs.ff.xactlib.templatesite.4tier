﻿namespace XAct.Validation
{
    using System.Text.RegularExpressions;

    public class StringArrayItemFormatValidatorAttribute : MessageCodePropertyValidatorAttribute
    {
        public StringArrayItemFormatValidatorAttribute(string messageCodeIdentifier)
            : base(messageCodeIdentifier)
        {
        }

        public string Pattern { get; set; }

        protected override bool Validate(object entity, System.Reflection.PropertyInfo propertyInfo, object value,

                                         Messages.IResponse response)
        {

            if (value == null)
            {
                return response.Success;
            }

            string[] typedValues = (string[]) value;

            foreach (string item in typedValues)
            {
                Match match = System.Text.RegularExpressions.Regex.Match(item, Pattern);

                if (!base.Assert(response, () => match.Success))
                {
                    return false;
                }
            }

            return response.Success;
        }
    }
}