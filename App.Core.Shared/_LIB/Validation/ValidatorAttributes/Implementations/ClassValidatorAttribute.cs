﻿namespace XAct.Validation
{
    using System;

    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Interface, AllowMultiple = true)]
    public abstract class ClassValidatorAttribute : Attribute, IHasTag
    {
        public string Tag { get; set; }
    }
}