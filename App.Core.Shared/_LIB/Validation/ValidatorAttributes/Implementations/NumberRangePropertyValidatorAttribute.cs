﻿namespace XAct.Validation
{
    public class NumberRangePropertyValidatorAttribute : MessageCodePropertyValidatorAttribute
    {
        public int Min { get; set; }
        public int Max { get; set; }

        public NumberRangePropertyValidatorAttribute(string messageCodeIdentifier):base(messageCodeIdentifier)
        {
            Min = int.MinValue;
            Max = int.MaxValue;
        }

        protected override bool Validate(object entity, System.Reflection.PropertyInfo propertyInfo, object value,
                                         Messages.IResponse response)
        {
            //Don't use the built in logic:
            //return base.Validate(entity, propertyInfo, value, response);

            //provide custom:

            int typedValue = (int)value;

            return base.Assert(response, () => typedValue >= Min && typedValue <= Max);
 
        }

    }

}