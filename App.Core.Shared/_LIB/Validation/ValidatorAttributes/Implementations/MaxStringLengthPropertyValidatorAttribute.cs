﻿namespace XAct.Validation
{
    public class MaxStringLengthPropertyValidatorAttribute : MessageCodePropertyValidatorAttribute
    {
        public int MaxLength { get; set; }

        public MaxStringLengthPropertyValidatorAttribute(string messageCodeIdentifier)
            : base(messageCodeIdentifier)
        {
        }

        protected override bool Validate(object entity, System.Reflection.PropertyInfo propertyInfo, object value,
                                         Messages.IResponse response)
        {

            if (value != null)
            {
                string typedValue = value.ToString();

                if (!typedValue.IsNullOrEmpty())
                {
                    bool result = base.Assert(response, () => (typedValue.Length <= MaxLength));
                    return result;
                }

            }
            return response.Success;
        }
    }
}