﻿namespace XAct.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using XAct.Messages;

    /// <summary>
    /// Abstract base class specialization of <see cref="PropertyValidatorAttribute"/>
    /// that allows the BaseClass to be identified by string.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public abstract class MessageCodePropertyValidatorAttribute : PropertyValidatorAttribute , IHasMessageCodeIdentifier
    {
        /// <summary>
        /// Gets the unique string identifier of the message code.
        /// </summary>
        /// <value>
        /// The message code identifier.
        /// </value>
        string IHasMessageCodeIdentifier.MessageCodeIdentifier
        {
            get { return _messageCodeIdentifier; }
        }
        protected string _messageCodeIdentifier;

        public MessageCode MessageCode
        {
            get { return _messageCode; }
        }
        private MessageCode _messageCode;

        public string CSVAdditionalArguments { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageCodePropertyValidatorAttribute" /> class.
        /// <para>
        /// Important:
        /// <see cref="MessageCode" /> is a struct, and therefore not accepted by the compiler
        /// as an compile time inline Attribute argument.
        /// </para>
        /// <para>
        /// Use a string reference of your preference (eg: "123")
        /// and supply an implementation of <see cref="IMessageCodeResolvingService" />
        /// to <see cref="IValidatorService" />
        /// to resolve the value during runtime.
        /// </para>
        /// </summary>
        /// <param name="messageCodeIdentifier">The message code identifier.</param>
        protected MessageCodePropertyValidatorAttribute(string messageCodeIdentifier)
        {
            _messageCodeIdentifier = messageCodeIdentifier;
        }

        void IHasMessageCodeIdentifier.Initialize(MessageCode messageCode)
        {
            _messageCode = messageCode;
        }

        protected bool Assert(IResponse response, Func<bool> func)
        {
            if (func.Invoke() == false)
            {
                string[] tmp = (CSVAdditionalArguments != null)
                                    ? CSVAdditionalArguments.Split(new char[] { ',', '|', ';' }) //,StringSplitOptions.RemoveEmptyEntries
                                    : new string[0];

                List<string> args = tmp.ToList();

                args.Insert(0,this.PropertyName);

                response.AddMessage(this.MessageCode, args.ToArray());
            }
            return response.Success;
        }
    }
}