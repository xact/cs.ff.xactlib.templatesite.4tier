﻿namespace XAct.Validation
{
    public class NestedValidatorAttribute : PropertyValidatorAttribute, INestedValidator
    {
        protected override bool Validate(object entity, System.Reflection.PropertyInfo propertyInfo, object value,
                                        Messages.IResponse response)
        {

            return response.Success;
        }
       
    }


}