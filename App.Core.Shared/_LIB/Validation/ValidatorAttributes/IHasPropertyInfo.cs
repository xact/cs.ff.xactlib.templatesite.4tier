﻿namespace XAct.Validation
{
    using System.Reflection;

    public interface IHasPropertyInfo
    {
        /// <summary>
        /// Gets or sets the <see cref="PropertyInfo"/>
        /// describing the Property that the 
        /// <see cref="PropertyValidatorAttribute"/>
        /// is decorating.
        /// <para>
        /// Note that this is injected by <see cref="IValidatorService"/>
        /// when invoking the <see cref="PropertyValidatorAttribute"/>s
        /// <see cref="IHasValidateProperty.Validate"/> method.
        /// </para>
        /// </summary>
        PropertyInfo PropertyInfo { get; set; }   
    }
}