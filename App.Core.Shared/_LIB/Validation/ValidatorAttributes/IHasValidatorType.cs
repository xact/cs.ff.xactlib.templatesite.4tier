﻿namespace XAct.Validation
{
    using System;

    /// <summary>
    /// Contract implemented by 
    /// Attributes that implement 
    /// <see cref="IPropertyValidatorInvoker"/>
    /// (eg: subclasses of <see cref="PropertyValidatorAttributeBase"/>)
    /// </summary>
    public interface IHasValidatorType
    {
        /// <summary>
        /// Gets or sets the <see cref="Type"/> of the 
        /// <see cref="IPropertyValidator"/>
        /// to invoke
        /// <para>
        /// Invoked by an implementation of 
        /// <see cref="IValidatorService"/>
        /// </para>
        /// <para>
        /// Note that in most trivial cases the <see cref="PropertyValidatorAttributeBase"/>
        /// subclass simply uses an override of it's <see cref="PropertyValidatorAttributeBase.Validate"/>
        /// method. It's only when it doesn't provide custom logic that the Attribute
        /// refers to this property, in order to create a Validator to validate the Value.
        /// </para>
        /// </summary>
        /// <value>
        /// The <see cref="Type"/> of the <see cref="IValidator"/>.
        /// </value>
        Type ValidatorType { get; set; }
    }
}