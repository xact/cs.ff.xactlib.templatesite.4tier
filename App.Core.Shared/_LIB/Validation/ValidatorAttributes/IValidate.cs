﻿
namespace XAct.Validation
{
    using System;
    using System.Reflection;

    public interface IPropertyValidatorInvoker : IHasPropertyInfo, IHasValidateProperty, IHasValidatorType, IHasTag, IHasConditionalOn, IEquatable<IPropertyValidatorInvoker>
    {
        /// <summary>
        /// Optional override of the <see cref="PropertyInfo"/>.Name.
        /// </summary>
        string PropertyName { get; set; }
        
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IHasConditionalOn
    {

        /// <summary>
        /// Gets or sets a value indicating whether the validation
        /// logic is used only to set a flag that a later validator
        /// can use to skip to determine whether it should
        /// validation check.
        /// <para>
        /// An example use case is the validation of a loan submittal form,
        /// when HeadOfHouseHold is ticked or not. It's neither incorrect or 
        /// correct on its own. But it can be used to determine whther 
        /// child properties (income, number of children, etc. about the head
        /// of household) are to be verified or not.
        /// </para>
        /// <para>
        /// In other words, if A is marked with `IsOnlyAConditionFlag`,
        /// and B is marked with ConditionalOn="A", if A is false,
        /// validation still says it's true, and skips checking B.
        /// If both A and B is false, the total answer can only be false if another property
        /// C, not marked as `IsOnlyAConditionalFlag`, fails.
        /// </para>
        /// <para>
        /// On the other hand, if B is marked as conditional on A,
        /// but A is not marked with `IsOnlyAConditionFlag`, and A fails, then
        /// A's validation failure is treated as a valid validation failure,
        /// and even if B is skipped (as it is dependent on A), 
        /// the whole validation is
        /// still going to result in false (due to A).
        /// </para>
        /// </summary>
        bool IsOnlyAConditionFlag { get; set; }

        /// <summary>
        /// Validate this property after the specified
        /// property, and only if it validated as true.
        /// </summary>
        string IsConditionalOn { get; set; }
    }
}
