﻿namespace XAct.Validation
{
    using XAct.Messages;

    public interface IHasMessageCodeIdentifier 
    {
        /// <summary>
        /// Gets the unique string used to identity
        /// a single instance of an implementation  of 
        /// <see cref="MessageCode"/>.
        /// <para>
        /// An example of its usage would be define somewhere
        /// in the application a class of read only instances of <see cref="MessageCode"/>
        /// <code>
        /// <![CDATA[
        /// public namespace App.Constants {
        ///   public static class MessageCodes {
        ///     public static MessageCode Foo = new MessageCode(123, Severity.Error);
        ///     public static MessageCode Bar = new MessageCode(123, Severity.BlockingWarning);
        ///     public static MessageCode Woo = new MessageCode(123, Severity.Information);
        ///   }
        /// }
        /// ]]>
        /// </code>
        /// and register this class with the 
        /// <see cref="IMessageCodeResolvingServiceConfiguration"/>:
        /// <code>
        /// <![CDATA[
        /// var configuration = XAct.DependencyResolver.Current.GetInstance<IMessageCodeResolvingServiceConfiguration>();
        /// configuration.DefinedMessageCodeSources(typeof(MessageCodes));
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <value>
        /// The message code identifier.
        /// </value>
        string MessageCodeIdentifier { get; }
        MessageCode MessageCode { get; }
        void Initialize(MessageCode messageCode);

        /// <summary>
        /// Any additional arguments you need to pass to the Message formatter, after the ProeprtyName.
        /// </summary>
        string CSVAdditionalArguments { get; set; }
    }
}