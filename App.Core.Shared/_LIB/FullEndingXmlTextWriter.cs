﻿using System.IO;
using System.Xml;

namespace App.Core.Utils
{
	public class FullEndingXmlTextWriter : XmlTextWriter
	{
		public FullEndingXmlTextWriter(TextWriter w)
			: base(w)
		{
		}

		public override void WriteEndElement()
		{
			WriteFullEndElement();
		}
	}
}