﻿namespace XAct.Settings.Implementations
{
    using System.Collections.Generic;
    using NConfig;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// Implementation of
    /// <see cref="IAppSettingsHostSettingsServiceConfiguration" />
    /// </summary>
    [DefaultBindingImplementation(typeof(IHostSettingsServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.High)]
    [DefaultBindingImplementation(typeof(IAppSettingsHostSettingsServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.High)]
    public class OVERRIDE_AppSettingsHostSettingsServiceConfiguration : IAppSettingsHostSettingsServiceConfiguration
    {

        /// <summary>
        /// Gets or sets the use NConfig to merge 
        /// Machine specific settings in from 
        /// <see cref="NConfigFilePath"/>.
        /// </summary>
        public bool UseNConfig
        {
            get { return _useNConfig; }
            set { _useNConfig = value; }
        }
        private bool _useNConfig=true;


        /// <summary>
        /// The Relative path to look for NConfig files to merge into AppSettings.
        /// <para>
        /// The default value is <c>"Config\\MachineSpecificSettings\\app.config"</c>
        /// </para>
        /// </summary>
        public string NConfigFilePath
        {
            get { return _nconfigFilePath; }

            set { _nconfigFilePath = value; }
        }

        private static string _nconfigFilePath = "Config\\MachineSpecificSettings\\app.config";


        /// <summary>
        /// Gets the full paths to templates used to find NConfig override files.
        /// </summary>
        /// <value>
        /// The n configuration paths searched.
        /// </value>
        public string[] NConfigPathsSearched
        {
            get { return _NConfigPathsSearched; }
        }

        private static string[] _NConfigPathsSearched;
        /// <summary>
        /// Gets or sets the v dir application setting key.
        /// The default setting is <c>VDir</c>
        /// </summary>
        public string VDirAppSettingKey
        {
            get { return _vdirAppSettingKey; }
            set { _vdirAppSettingKey = value; }
        }
        private static string _vdirAppSettingKey = "VDir";


        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="IHasInitialize" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized { get { return StaticInitialized; } private set { StaticInitialized = value; } }

        private static bool StaticInitialized { get; set; }



        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            if (Initialized)
            {
                return;
            }
            lock (this)
            {
                if (!UseNConfig)
                {
                    return;
                }

                if (Initialized)
                {
                    return;
                }
                //Local Path:
                //NConfigurator.UsingFiles("..\\..\\Config\\Custom.config", "..\\..\\Config\\Connections.config").SetAsSystemDefault();
                //Also works in Solution sharing across projects:

                string tmp = string.Empty;
                if (!this.VDirAppSettingKey.IsNullOrEmpty())
                {
                    tmp = System.Configuration.ConfigurationManager.AppSettings[this.VDirAppSettingKey] ?? string.Empty;
                }

                string[] possibleVirtualDirectories = tmp.Split(new[] { '|', ',', ';' }
                    /*,StringSplitOptions.RemoveEmptyEntries*/
                                                                );

                List<string> tmp2 = new List<string>();
                List<string> tmp3 = new List<string>();
#pragma warning disable 168
                //Worth knowing:...
                string hostAlias = NConfigurator.Settings.HostAlias;
#pragma warning restore 168

                foreach (string s in possibleVirtualDirectories)
                {
                    string t = (s.IsNullOrEmpty() ? string.Empty : s + "\\") + this.NConfigFilePath;
                    tmp2.Add(t);

                    //Note that in a unit test, the output will reflect somethng similar to:
                    //C:\Users\SkyS\AppData\Local\Temp\TestResults\skys_SKYSW8LT 2014-03-02 19_15_21\Out\Config\MachineSpecificSettings\app.config

                    string t2 = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().MapPath(t);

                    if (t2.Contains("\\bin\\") && (System.Web.HttpContext.Current!=null))
                    {
                        t2 = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().MapPath("..\\" + t);
                        
                    }
                    tmp3.Add(t2);

                }
                try
                {
                    _NConfigPathsSearched = tmp3.ToArray();
                    string[] directories = tmp2.ToArray();

                    NConfigurator.UsingFiles(directories).SetAsSystemDefault();
                }
                catch (System.Exception e)
                {
                    ITracingService tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>();
                    tracingService.TraceException(TraceLevel.Error, e, "Could not handle NConfig references.");
                }

#pragma warning disable 168
                string check = System.Configuration.ConfigurationManager.AppSettings["NConfig"];
#pragma warning restore 168

                Initialized = true;
            }
        }
    }
}