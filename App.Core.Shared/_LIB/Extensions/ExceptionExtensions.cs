﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.LIB.Extensions
{
	public static class ExceptionExtensions
	{

		//NOTE: This was moved out of XAct, as referencing it seemed to start requiring a direct reference to PerformanceCounterInstallers...

		/// <summary>
		/// <para>
		/// An XActLib Extension.
		/// </para>
		/// <para>Creates a log-string from the Exception.</para>
		/// <para>The result includes the stacktrace, innerexception et cetera, separated by <seealso cref="System.Environment.NewLine"/>.</para>
		/// </summary>
		/// <param name="ex">The exception to create the string from.</param>
		/// <param name="additionalMessage">Additional message to place at the top of the string, maybe be empty or null.</param>
		/// <returns></returns>
		/// <internal>
		/// Src: http://bit.ly/eoOpqq 
		/// </internal>
		public static string ToLogString(this Exception ex, string additionalMessage)
		{
			StringBuilder msg = new StringBuilder();

			// Append user message at the top
			if (!string.IsNullOrEmpty(additionalMessage))
			{
				msg.Append(additionalMessage);
				msg.Append(System.Environment.NewLine);
			}

			if (ex != null)
			{
				try
				{
					Exception orgEx = ex;

					msg.Append(System.Environment.NewLine);
					msg.AppendFormat("* Exception: {0}", ex.GetType().Name);
					msg.Append(System.Environment.NewLine);
					
					// List out inner exceptions
					var level = 0;
					while (orgEx != null)
					{
						if (level > 0)
						{
							msg.AppendFormat("(Level: {0})", level);
						}
						msg.Append(orgEx.Message);
						msg.Append(System.Environment.NewLine);
						orgEx = orgEx.InnerException;

						level++;
					}

					// List out optional properties:

					foreach (object i in ex.Data)
					{
						msg.Append(System.Environment.NewLine);
						msg.Append("* Data :");
						msg.Append("\t" + i.ToString());
						msg.Append(System.Environment.NewLine);
					}


					if (ex.Source != null)
					{
						msg.Append(System.Environment.NewLine);
						msg.Append("* Source:");
						msg.Append(System.Environment.NewLine);
						msg.Append("\t" + ex.Source);
						msg.Append(System.Environment.NewLine);
					}

					if (ex.TargetSite != null)
					{
						msg.Append(System.Environment.NewLine);
						msg.Append("* TargetSite:");
						msg.Append(System.Environment.NewLine);
						msg.Append("\t" + ex.TargetSite.ToString());
						msg.Append(System.Environment.NewLine);
					}

					// Leave the stack trace 'til the end, as it's big & ugly.
					if (ex.StackTrace != null)
					{
						msg.Append(System.Environment.NewLine);
						msg.Append("* StackTrace:");
						msg.Append(System.Environment.NewLine);
						msg.Append(ex.StackTrace);
						msg.Append(System.Environment.NewLine);
					}

					Exception baseException = ex.GetBaseException();

					msg.Append(System.Environment.NewLine);
					msg.Append("* BaseException:");
					msg.Append(System.Environment.NewLine);
					msg.Append(baseException.GetBaseException());
				}
				finally
				{
				}
			}
			return msg.ToString();
		}
	}
}
