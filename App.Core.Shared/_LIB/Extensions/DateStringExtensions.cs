﻿using System;
using XAct;

namespace App.Core.Domain.Extensions
{
	public static class DateStringExtensions
	{

	    /// <summary>
	    /// Returns an 8 character date string when a 6 character date of format yymmdd is passed in
	    /// </summary>
	    /// <param name="dateString"></param>
	    /// <returns></returns>
	    public static string ToEightDigitDateString(this string dateString)
	    {
	        //int years = int.Parse(value.Substring(0, 2));
	        //value = years > (DateTime.Now.Year - 2000) ? "19" + value : "20" + value;
	        dateString = dateString.SafeTrim();
	        if ((dateString == null) || (dateString.Length != 6))
	        {
	            return dateString;
	        }

	        int tmp;
	        // Assume that the date is decimal and 6 characters
	        if (int.TryParse(dateString, out tmp))
	        {
	            string lastTwoDigitsOfYear = DateTime.Now.ToString("yy");
	            int currentYear = int.Parse(lastTwoDigitsOfYear);
	            int dateYear = int.Parse(dateString.Substring(0, 2));
	            int century = DateTime.Now.Year/100;


	            // [FS461] If date year is greater than current year then assume 19th century otherwise its the 20th century
	            if (dateYear > currentYear)
	            {
	                century -= 1;
	            }

	            dateString = century + dateString;
	        }


	        return dateString;
	    }
	}
}
