﻿using App.Core.Infrastructure.Data.Repositories;

namespace App.Core.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Returns the same list short of the given item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="itemToLeaveOut">The item to leave out.</param>
        /// <returns></returns>
        public static IEnumerable<T> Except<T>(this IEnumerable<T> list, T itemToLeaveOut)
        {
            return list
                .Except(new List<T> {itemToLeaveOut})
                .ToList();
        }

        /// <summary>
        /// Converts the input list to semicolon delimited string. Each item in the list is converted by object.ToString() function.
        /// Example: 1; 2; 3;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        public static string ToSemicolonDelimited<T>(this IEnumerable<T> list)
        {	        
            return list.ToDelimited("; ", item => item.ToString());
        }

        /// <summary>
        /// Converts the input list to semicolon delimited string. Each item in the list is converted by object.ToString() function.
        /// Example: 1, 2, 3,
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        public static string ToCommaDelimited<T>(this IEnumerable<T> list)
        {			
            return list.ToDelimited(", ", item => item.ToString());
        }

		/// <summary>
		/// Converts the input list to pipe delimited string. Each item in the list is converted by object.ToString() function.
		/// Example: 1|2|3
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">The list.</param>
		/// <returns></returns>
	    public static string ToPipeDelimited<T>(this IEnumerable<T> list)
	    {
		    return list.ToDelimited("|", item => item.ToString());
	    }

	    /// <summary>
        /// Converts input list to string delimited by separator. Each item in the list is converted by given function.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="separator">The separator.</param>
        /// <param name="toSringFunction">To sring function.</param>
        /// <returns></returns>
        public static string ToDelimited<T>(this IEnumerable<T> list, string separator, Func<T, string> toSringFunction)
        {
	        if (list == null) return string.Empty;
            return String.Join(separator, list.Select(toSringFunction));
        }

	   
    }
}