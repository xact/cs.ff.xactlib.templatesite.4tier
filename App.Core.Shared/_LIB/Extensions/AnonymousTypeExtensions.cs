namespace App.Core.Extensions
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    public static class AnonymousTypeExtensions
	{
		private static readonly Type[] WriteTypes = new[] {
			typeof(string), typeof(DateTime), typeof(Enum), 
			typeof(decimal), typeof(Guid),
		};
		public static bool IsSimpleType(this Type type)
		{
			return type.IsPrimitive || WriteTypes.Contains(type);
		}

		public static bool IsEnumerable(this Type type)
		{
			return (type.GetInterface("IEnumerable") != null);
		}

		public static XElement ToXml(this object input)
		{
			return input.ToXml(null);
		}
		public static XElement ToXml(this object input, string element)
		{
			if (input == null)
				return null;

			if (string.IsNullOrEmpty(element))
				element = "object";
			element = XmlConvert.EncodeName(element);
			var ret = new XElement(element);

			if (input != null)
			{
				var type = input.GetType();
				var props = type.GetProperties();

				if (!type.IsSimpleType() && type.IsEnumerable())
				{
					// singularize the element name
					if (element.EndsWith("ies"))
						element = element.Substring(0, element.Length - 3) + "y";
					else if (element.EndsWith("s") && element.Length > 1)
						element = element.Substring(0, element.Length - 1);
					foreach (object item in input as IEnumerable)
					{
						ret.Add(item.GetType().IsSimpleType()
							        ? new XElement(element, item)
							        : item.ToXml(element));
					}
				}
				else
				{
					var elements = from prop in props
					               let name = XmlConvert.EncodeName(prop.Name)
					               let val = prop.GetValue(input, null)
					               let value = prop.PropertyType.IsSimpleType()
						                           ? new XElement(name, val)
						                           : val.ToXml(name)
					               where value != null
					               select value;
					ret.Add(elements);
				}

			}

			return ret;
		}
	}
}