﻿using System;
using System.Text;
using XAct;

namespace App.Core.Domain.Extensions
{
	public static class StringEncodingExtensions
	{

	    /// <summary>
	    /// Removes the BOM from the beginning of string
	    /// </summary>
        /// <param name="encodedString"></param>
	    /// <returns></returns>
	    public static string RemoveByteOrderMark(this string encodedString)
	    {
            string byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            if (encodedString.StartsWith(byteOrderMarkUtf8))
            {
                encodedString = encodedString.Remove(0, byteOrderMarkUtf8.Length);
            }

            return encodedString;
	    }
	}
}
