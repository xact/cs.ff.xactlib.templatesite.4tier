﻿using System;
using System.IO;
using System.Reflection;

namespace App.Core.Shared.Tests.Common.Extensions
{
    public static class FileInfoExtensions
    {
        /// <summary>
        /// <para>
        /// Helper tool for testing, trying to find 
        /// files via relative path, while testing within NUnit unit tests.f
        /// </para>
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public static FileInfo MakeRelativeToExecutingAssembly(this FileInfo fileInfo)
        {
            if (Path.IsPathRooted(fileInfo.FullName))
            {
                return fileInfo;
            }

            string rootDir =
                Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);

            const string prefix ="file:\\";

            if (rootDir.StartsWith(prefix))
            {
                rootDir = rootDir.Substring(0,"file:\\".Length);
            }

            return new FileInfo(Path.Combine(rootDir, fileInfo.FullName));
        }



        public static DirectoryInfo MakeRelativeToExecutingAssembly(this DirectoryInfo directoryInfo)
        {
            if (Path.IsPathRooted(directoryInfo.FullName))
            {
                return directoryInfo;
            }

            string rootDir =
                Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);

            const string prefix = "file:\\";

            if (rootDir.StartsWith(prefix))
            {
                rootDir = rootDir.Substring(0, "file:\\".Length);
            }

            return new DirectoryInfo(Path.Combine(rootDir, directoryInfo.FullName));
        }

    }
}
