﻿//Put Extensions in the lowest (ie, the App) Namespace
//So that they are available without having to add
//a using namespace statement. More intutitive.

namespace App.Core
{
    using System.Collections.Generic;

    public static  class CollectionExtensions
	{
		public static void AddRange<T>(this ICollection<T> destination,
								   IEnumerable<T> source)
		{
			if (source == null) { return; }

			foreach (T item in source)
			{
				destination.Add(item);
			}
		}       
	}
}
