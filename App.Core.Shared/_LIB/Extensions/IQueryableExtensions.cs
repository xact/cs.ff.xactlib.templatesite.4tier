﻿using App.Core.Infrastructure.Data.Repositories;

namespace App.Core.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    public static class IQueryableExtensions
	{
		public static IOrderedEnumerable<TSource> OrderByWithDirection<TSource, TKey>
			(this IEnumerable<TSource> source,
			 Func<TSource, TKey> keySelector,
			 bool descending)
		{
			return @descending ? source.OrderByDescending(keySelector)
							  : source.OrderBy(keySelector);
		}

		public static IOrderedQueryable<TSource> OrderByWithDirection<TSource, TKey>
			(this IQueryable<TSource> source,
			 Expression<Func<TSource, TKey>> keySelector,
			 bool descending)
		{
			return @descending ? source.OrderByDescending(keySelector)
							  : source.OrderBy(keySelector);
		}

		public static Func<IQueryable<TSource>, IOrderedQueryable<TSource>> MakeOrderByWithDirection<TSource, TKey>
			( 
			 Expression<Func<TSource, TKey>> keySelector,
			 bool ascending)
		{

			Func<IQueryable<TSource>, IOrderedQueryable<TSource>> orderBy;

			if (@ascending)
			{
				orderBy = q => q.OrderBy(keySelector);
			}
			else
			{
				orderBy = q => q.OrderByDescending(keySelector);
			}

			return orderBy;
		}

		public static IQueryable<T> ConditionalFilter<T>(this IQueryable<T> source, Func<bool> condition, Expression<Func<T, bool>> filter)
	    {
		    return condition.Invoke() ? source.Where(filter) : source;
	    }

	    /// <summary>
	    /// Enumerates the query spliting it into separate database queries, each of fetchingPageSize.
	    /// </summary>
	    /// <typeparam name="T"></typeparam>
	    /// <param name="source">The source.</param>
	    /// <param name="fetchingPageSize">Size of the page the query will be split to.</param>
	    /// <returns></returns>
	    public static List<T> ToList<T>(this IQueryable<T> source, int fetchingPageSize)
	    {
		    var pagedQueryEnumerator = new PagedQueryEnumerator<T>(source, fetchingPageSize);
		    var result = new List<T>();
		    
			IList<T> page;
			while ((page = pagedQueryEnumerator.ReadNext()).Count != 0)
			    result.AddRange(page);

		    return result;
	    }
	}
}