﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XAct;

namespace App.Core.LIB.Extensions
{
    /// <summary>
    /// Implementation of a <see cref="Queue" /> that doesn't enqueue items that are already in the queue />
    /// </summary>
    /// <remarks>Uses an underlying dictionary and <see cref="IHasIntId" /> interface to check for uniqueness
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    public class UniqueQueue<T> where T : IHasIntId
    {
        private readonly bool _isReplacementQueue;
        private readonly Queue<T> _queue;
        private readonly Dictionary<int, T> _dictionary;

        public UniqueQueue()
        {
            _queue = new Queue<T>();
            _dictionary = new Dictionary<int, T>();
        }

        public void Enqueue(T item)
        {
            if (_dictionary.Keys.Contains(item.Id))
            {
                return;
            }
            _dictionary.Add(item.Id, item);
            _queue.Enqueue(item);
        }

        public T Dequeue()
        {
            var item = _queue.Dequeue();
            _dictionary.Remove(item.Id);
            return item;
        }

        public int Count()
        {
            return _queue.Count();
        }
    }
}