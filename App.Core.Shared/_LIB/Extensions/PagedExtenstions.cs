using System.Collections.Generic;
using System.Linq;
using XAct.Messages;

namespace App.Core.LIB.Extensions
{
	public static class PagedExtenstions
	{
		/// <summary>
		/// Creates a smaller paged response, given the full data list
		/// + Copies over the paging params and sets the 'total' record count
		/// </summary>
		/// <typeparam name="TRequest"></typeparam>
		/// <typeparam name="TResponse"></typeparam>
		/// <param name="pagedRequest"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		public static PagedResponse<TResponse> CreatePagedResponse<TRequest, TResponse>(this PagedRequest<TRequest> pagedRequest, ICollection<TResponse> data)	
		{
			// Ensure page index is valid
			var pageIndex = pagedRequest.PageIndex >= 0 ? pagedRequest.PageIndex : 0;

			// Allow '0' page size to retrieve all records
			var pagedData = pagedRequest.PageSize > 0
								? data.Skip(pagedRequest.PageSize * pageIndex)
										.Take(pagedRequest.PageSize).ToList()
				                : data;

			var pagedResponse = new PagedResponse<TResponse>(
				pagedData,
				pageIndex,
				pagedRequest.PageSize,
				data.Count());

			return pagedResponse;
		}

		/// <summary>
		/// Clones the PagedRequest to a new type, copying only the paging properties.
		/// </summary>
		/// <typeparam name="TOrig"></typeparam>
		/// <typeparam name="TNew"></typeparam>
		/// <param name="origRequest"></param>
		/// <returns></returns>
		public static PagedRequest<TNew> CloneWithNewType<TOrig, TNew>(this PagedRequest<TOrig> origRequest, TNew newData)
		{
			PagedRequest<TNew> newRequest = new PagedRequest<TNew>();

			newRequest.PageIndex = origRequest.PageIndex;
			newRequest.PageSize = origRequest.PageSize;

			newRequest.Data = newData;

			return newRequest;
		}

		/// <summary>
		/// Clones the basic Response object, and sets the given data property as the specified type
		/// </summary>		
		/// <returns></returns>
		public static PagedResponse<TNew> ClonePagedWithNewType<TOrig, TNew>(this PagedResponse<TOrig> response, ICollection<TNew> data)
		{
			var result = new PagedResponse<TNew>();

			result.Messages.AddRange(response.Messages);
			result.Metadata.AddRange(result.Metadata);

			result.Data = data;

			result.PageIndex = response.PageIndex;
			result.PageSize = response.PageSize;
			result.TotalCount = response.TotalCount;

			return result;
		}
	}
}
