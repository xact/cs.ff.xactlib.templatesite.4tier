﻿using System.Collections.Generic;
using System.Linq;

namespace App.Core.Infrastructure.Data.Repositories
{
	/// <summary>
	/// Enumerates through pages of results using the given query.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class PagedQueryEnumerator<T>
	{
		private readonly IQueryable<T> _query;
		private int _currentPage;
		private readonly int _pageSize;

		/// <summary>
		/// Initializes a new instance of the <see cref="PagedQueryEnumerator{T}"/> class.
		/// </summary>
		/// <param name="query">The query (must contain query by clause).</param>
		/// <param name="pageSize">Size of the page.</param>
		public PagedQueryEnumerator(IQueryable<T> query, int pageSize)
		{
			_query = query;
			_pageSize = pageSize;
		}

		/// <summary>
		/// Reads the next page using the query. 
		/// Each call hits the database.
		/// </summary>
		/// <returns></returns>
		public List<T> ReadNext()
		{
			var query = _query				
				.Skip(_currentPage*_pageSize)
				.Take(_pageSize);

			var result = query.ToList();
			_currentPage++;

			return result;
		}
	}
}