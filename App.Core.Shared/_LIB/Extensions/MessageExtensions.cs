﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XAct;
using XAct.Messages;

namespace App.Core.LIB.Extensions
{
	public static class MessageExtensions
	{
		private static readonly string[] messageContainsSeparatorChars = new string[] { ":", "."};

		/// <summary>
		/// Returns the message, and any nested messages as a single string
		/// <para>
		/// Note the Message's PresentationAttributes must have been filled first.
		/// </para>
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		public static string GetMessageTextIncludingNestedText(this Message message)
		{
            if (message.PresentationAttributes == null)
            {
                throw new ArgumentException("The message's PresentationAttributes must have been filled in first.");
            }
			var messageText = message.PresentationAttributes.Text;

		    if (message.InnerMessages == null || !message.InnerMessages.Any())
		    {
		        return messageText;
		    }

		    foreach (var innerMessage in message.InnerMessages)
		    {
                //Does the message end with ':' or '.'
		        var lastCharOfMessage = messageText.Trim().Substring(messageText.Trim().Length - 1, 1);

                //If so, we append it to existing message, with a comma in between. 
                //Otherwise, a space.
		        var separator = messageContainsSeparatorChars.Contains(lastCharOfMessage)
		                            ? " "
		                            : ", ";

		        if (innerMessage.PresentationAttributes == null)
		        {
		            throw new ArgumentException("The message's PresentationAttributes must have been filled in first.");
		        }
		        messageText += separator + innerMessage.PresentationAttributes.Text;
		    }
            

		    return messageText;
		}
	}
}
