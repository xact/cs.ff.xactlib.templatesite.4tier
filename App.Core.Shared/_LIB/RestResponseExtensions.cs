﻿namespace App.Core.Extensions
{
    using System;
    using System.Net;
    using Domain;
    using RestSharp;
    using XAct;
    using XAct.Messages;

    /// <summary>
	/// Provides a set of static methods for extending RestResponse class.
	/// </summary>
	public static class RestResponseExtensions
	{

        /// <summary>
        /// Converts RestResponse object to Reponse object.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="restResponse">The rest response.</param>
        /// <returns></returns>
		public static Response<TData> ToResponse<TData>(this IRestResponse<TData> restResponse)
        {
            return ToResponse(restResponse, data => restResponse.Data); //when data conversion mapping is 1:1
        }

        /// <summary>
        /// Converts RestResponse object to Reponse object and applies data conversion function.
        /// </summary>
        /// <typeparam name="TDataInput">The type of the data input.</typeparam>
        /// <typeparam name="TDataOutput">The type of the data output.</typeparam>
        /// <param name="restResponse">The rest response.</param>
        /// <param name="convertor">The convertor.</param>
        /// <returns></returns>
        public static Response<TDataOutput> ToResponse<TDataInput, TDataOutput>(this IRestResponse<TDataInput> restResponse, Func<TDataInput, TDataOutput> convertor)
        {
            Response<TDataOutput> message = new Response<TDataOutput>
                              {
                                  Data = convertor.Invoke(restResponse.Data)
                              };



            message.AddMessage(new MessageCode(-1,Severity.Error));
            ////YIKES! How do we know if this is success or not?!?
            //message.Messages.Add new Message(Severity.Undefined, (ulong)restResponse.StatusCode, restResponse.StatusDescription));

            return message;
        }
	}
}