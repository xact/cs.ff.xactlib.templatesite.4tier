﻿namespace App.Core.Shared.Tests.Common.Comparers
{
    public interface IAssertor<in T>
    {
        void AssertIsEqual(T x, T y);
    }
}