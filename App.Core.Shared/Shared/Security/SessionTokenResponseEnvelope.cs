﻿namespace App.Core.Shared.Services.Services.Security
{
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using App.Core.Domain.Messages;
    using App.Core.Domain.Models;
    using XAct.Messages;

    [DataContract]
    public class SessionTokenResponseEnvelope
    {

        [DataMember]
        public Response<SessionResponse> SessionResponse { get; set; }

        public MessageHeader ToMessageHeader()
        {
            return new MessageHeader<SessionTokenResponseEnvelope>(this)
                .GetUntypedHeader(
                    Constants.Security.Session.SessionTokenResponseHeaderName, 
                    Constants.Security.Session.SessionTokenResponseHeaderNamespace);
        }
    }
}