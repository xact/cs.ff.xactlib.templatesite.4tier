﻿
namespace App.Core.Infrastructure.Services
{
    using System;
    using System.Collections;

    public interface ISessionStateService : IHasAppCoreService
    {

        object this[string key] { get; set; }

        int Count { get; }

        ICollection Keys { get; }

        void Add(string key, object value);

        void Clear();

        void Remove(string key);

        void RemoveAll();

        void CopyTo(Array array, int index);

    }
}
