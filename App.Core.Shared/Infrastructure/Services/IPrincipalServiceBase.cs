﻿namespace App.Core.Infrastructure.Services
{
    using System.Security.Principal;

    public interface IPrincipalServiceBase : IHasAppCoreService
    {

        IPrincipal Principal { get; set; }

        string CurrentIdentityIdentifier { get; }

	    bool HasPrivilege(string privilegeName);
    }
}
