﻿namespace App.Core.Infrastructure.Services
{
    using System;
    using XAct.Exceptions;

    /// <summary>
    /// The contract for a service to register ExceptionHandling descriptors.
    /// 
    /// </summary>
    /// <summary>
    /// See XAct.EnterpriseLibrary
    ///             and see if you can think of a way of generalizing it...
    /// 
    /// </summary>
    public interface IExceptionHandlingService : IHasAppCoreService
    {
        /// <summary>
        /// Gets or sets the default exception handling configuration.
        /// 
        /// <para>
        /// Default is Null.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The default exception handling configuration.
        /// 
        /// </value>
        IExceptionHandlingConfiguration DefaultExceptionHandlingConfiguration { get; set; }

        /// <summary>
        /// Gets the default type of Exception to create
        ///             when the <see cref="T:XAct.Exceptions.ExceptionHandlingBehaviour"/>
        ///             is set to <see cref="F:XAct.Exceptions.ExceptionHandlingBehaviour.ReplaceException"/>
        /// </summary>
        /// 
        /// <value>
        /// The default exception type to raise.
        /// 
        /// </value>
        Type DefaultExceptionType { get; set; }

        /// <summary>
        /// Handle the exception, and based on any rules
        ///             defined in the ExceptionService,
        ///             log the given Exception,
        ///             and raise a new one (cleansed) exception
        ///             suitable to passed up to higher layers.
        /// 
        /// <para>
        /// 
        /// <code>
        /// <![CDATA[
        ///             try {
        ///               var a=1;var b=0;
        ///               var r = a/b;
        ///             }
        ///             catch (ref System.Exception e){
        ///               //Only have to throw exception if result is true:
        ///               if (exceptionService.Handle(ref e)){throw;}
        ///             }
        ///             ]]>
        /// </code>
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="category"/><param name="exception"/>
        bool HandleException(ref Exception exception, string category = null);


        /// <summary>
        /// Handles the specified function.
        /// <para>
        /// <code>
        /// <![CDATA[
        /// function PublicAPIMethod(Request request){
        ///   _exceptionHandlingService.Handle(()=>{return _applicationService.GetStudent(request);});
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="function">The function.</param>
        /// <param name="category">The category.</param>
        /// <returns>The result or an exception that is </returns>
        TResult Handle<TResult>(Func<TResult> function, string category=null);



        void Handle(Action action, string category = null);

    }
}