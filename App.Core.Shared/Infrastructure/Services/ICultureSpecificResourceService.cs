﻿namespace App.Core.Infrastructure.Services
{
    using System.Collections.Generic;
    using System.Globalization;
    using App.Core.Domain.Messages;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// If you're wondering why I didn't call it just
    /// ResourceSource, it's to disambiguate it from the 
    /// System's ResourceService (eases Resharper's guessing...).
    /// </internal>
    public interface ICultureSpecificResourceService : IHasAppCoreService
	{
        /// <summary>
        /// Returns a single localised Resource string.
        /// <para>
        /// Note that the back tier service is providing a means to define
        /// the culture, as the back tier server is not the same thread
        /// as the front tier -- and therefore may not have it's UICulture
        /// set to that of the end user.
        /// </para>
        /// <para>
        /// The front tier should not call this method (it's expensive)
        /// and find alternate solutions such as using the cached results
        /// of earlier calls to <see cref="GetResourceSet"/> or
        /// <see cref="GetResourceSets"/>
        /// </para>
        /// </summary>
        /// <param name="resourceSetName">The name.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <returns></returns>
		string GetResource(string resourceSetName, string name, CultureInfo cultureInfo, ResourceTransformation resourceTransformation = ResourceTransformation.None);


        /// <summary>
        /// Get a Set (usually a View's) set of localised Resource strings.
        /// </summary>
        /// <remarks>
        /// Note that the back tier service is providing a means to define 
        /// the culture, as the back tier server is not the same thread 
        /// as the front tier -- and therefore may not have it's UICulture
        /// set to that of the end user.
        /// </remarks>
        /// <param name="resourceSetName">Will be the name of the View you want all the scrings for.</param>
        /// <param name="cultureInfo"></param>
        /// <param name="resourceTransformation"></param>
        /// <returns></returns>
        Dictionary<string, string> GetResourceSet(string resourceSetName, CultureInfo cultureInfo, ResourceTransformation resourceTransformation = ResourceTransformation.None);

        /// <summary>
        /// Returns all resource strings 
        /// </summary>
        /// <param name="cultureInfo">Culture Info</param>
        /// <param name="resourceTransformation">Resource Transformation</param>
        /// <param name="resourceSetNames">List of filters to retrieve resources for</param>
        /// <returns></returns>
        Dictionary<string, Dictionary<string, string>> GetResourceSets(CultureInfo cultureInfo, ResourceTransformation resourceTransformation = ResourceTransformation.None, params string[] resourceSetNames);

	}
}

