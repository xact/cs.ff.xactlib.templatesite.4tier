﻿namespace App.Core.Infrastructure.Services
{
    using System.Globalization;
    using App.Core;
    using App.Core.Domain.Messages;

    /// <summary>
    /// Contract for a service
    /// to provide to the Front Tier any Help Resources it may need.
    /// </summary>
    /// <internal>
    /// This Service does not inherit from <c>IHelpServiceFacade</c>
    /// because <see cref="CultureInfo"/> is not serializable, and therefore
    /// there <c>IHelpServiceFacade</c> has to first convert it from
    /// a WCF allowed <c>string</c>
    /// </internal>
    public interface IHelpService : IHasAppCoreService
    {
        HelpEntrySummary GetHelpEntryByKey(string helpEntryKey, CultureInfo cultureInfo = null);
    }


}
