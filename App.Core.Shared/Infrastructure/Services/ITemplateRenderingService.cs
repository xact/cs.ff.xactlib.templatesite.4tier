﻿namespace App.Core.Infrastructure.Services
{
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    public interface ITemplateRenderingService : IHasAppCoreService
  {
    //ITemplateRenderingServiceSettings Settings { get; }

    string CompileTemplateString(string templateText, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true);

    long CompileTemplateString(Stream outputStream, string templateText, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true);

    string CompileTemplateFile(string templateFileName, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true);

    long CompileTemplateFile(Stream outputStream, string templateFileName, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true);

    string CompileTemplateStream(TextReader textReader, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true);

    long CompileTemplateStream(Stream outputStream, TextReader textReader, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true);
  }
}
