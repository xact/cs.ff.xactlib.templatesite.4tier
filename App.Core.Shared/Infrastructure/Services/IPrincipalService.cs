﻿namespace App.Core.Infrastructure.Services
{
    using App.Core.Domain.Security;

    public interface IPrincipalService : IPrincipalServiceBase, IHasAppService 
    {

        /// <summary>
        /// Gets or sets the current identity.
        /// </summary>
        /// <value>
        /// The current identity.
        /// </value>
		IAppIdentity CurrentAppIdentity { get; }

	    IAppIdentity GetCurrentAppIdentity(bool throwAuthExceptionIfNotFound = false);
    }
}