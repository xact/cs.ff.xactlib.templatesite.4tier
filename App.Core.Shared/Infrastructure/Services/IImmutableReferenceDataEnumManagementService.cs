﻿namespace App.Core.Infrastructure.Services
{
    using System;
    using System.Globalization;
    using App.Core.Domain.Messages;

    /// <summary>
	/// Contract for a
	/// Service to convert an Reference Data Enum
	/// to a Resource string.
	/// <para>
	/// Note that this deals only with the caching
	/// of a Reference Data item's text (not any additional
	/// information regarding codes, filters, etc.)
	/// so it may need refactoring?
	/// </para>
	/// </summary>
    public interface IImmutableReferenceDataEnumManagementService : IHasAppCoreService
    {

		/// <summary>
		/// Retrieves from cache
		/// the string resource associated to the given Enum.
		/// <para>
		/// If the resource is not in cache,
		/// invokes <see cref="ICultureSpecificResourceService" />
		/// to retrieve it from the back tier.
		/// </para>
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="enumValue">The enum value.</param>
		/// <param name="cultureInfo">The culture information.</param>
		/// <returns></returns>
        string ToResource<T>(T enumValue, CultureInfo cultureInfo = null)
            where T : struct, IConvertible;

        string TextToResource(CultureInfo cultureInfo, string text, params object[] textArguments);

        ReferenceDataViewModel ReferenceDataCodeToReferenceDataViewModel<T>(string tag, CultureInfo cultureInfo)
            where T : struct, IConvertible;

        ReferenceDataViewModel EnumToReferenceDataViewModel<T>(T enumValue, CultureInfo cultureInfo)
            where T : struct, IConvertible;

        string EnumToReferenceDataCode<TReferenceEnum>(TReferenceEnum enumValue)
            where TReferenceEnum : struct, IConvertible;

        string ReferenceDataCodeToEnumValue<TReferenceEnum>(string code)
            where TReferenceEnum : struct, IConvertible;

        bool ValidateReferenceDataCodeEnumValue<TReferenceEnum>(string code)
            where TReferenceEnum : struct, IConvertible;

        TReferenceEnum ReferenceDataCodeToEnum<TReferenceEnum>(string code)
            where TReferenceEnum : struct, IConvertible;

        bool ReferenceDataCodeToEnum<TReferenceEnum>(string code, out TReferenceEnum result)
        where TReferenceEnum : struct, IConvertible;

    }
}
