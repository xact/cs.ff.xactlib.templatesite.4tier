﻿namespace App.Core.Infrastructure.Services
{
    using XAct.ObjectMapping;

    /// <summary>
    /// Contract for a Service 
    /// to configure the 
    /// <see cref="T:XAct.ObjectMapping.ITypeMapper"/>s
    /// that an implementation of IObjectMappingService
    /// will use.
    /// </summary>
    public interface IObjectMappingRegistrationService : IHasAppCoreService
    {

        bool Test();

        /// <summary>
        /// Means of checking as to which maps were registered.
        /// </summary>
        ITypeMapper[] Maps { get; }

        /// <summary>
        /// Registers the <see cref="T:XAct.ObjectMapping.ITypeMapper`2"/> with the service
        ///             so that it can later be used by <see cref="T:XAct.ObjectMapping.IObjectMappingService"/>
        /// </summary>
        /// <param name="typeMapper">The type mapper.</param><param name="contextName">Name of the context.</param>
        void RegisterMapper(ITypeMapper typeMapper, string contextName = null);
    }
}
