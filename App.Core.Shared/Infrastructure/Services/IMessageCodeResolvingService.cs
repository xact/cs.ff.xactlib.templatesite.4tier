﻿namespace XAct.Validation
{
    using App.Core;
    using XAct.Messages;

    /// <summary>
    /// <para>
    /// Service used by 
    /// <see cref="IValidatorService"/>
    /// to convert the strings given in the Validator Attributes
    /// to MessageCodes that can be raised.
    /// </para>
    /// </summary>
    public interface IMessageCodeResolvingService : IHasAppCoreService
    {



        /// <summary>
        /// Resolves the specified message code identifier
        /// (commonly just the string name of the static <see cref="MessageCode"/> property)
        /// into it's <see cref="MessageCode"/>.
        /// <para>
        /// For example, given an app having common MessageCodes defined as follows:
        /// <code>
        /// <![CDATA[
        /// public namespace App.Constants {
        ///   public static class MessageCodes {
        ///     public static MessageCode Foo = new MessageCode(123, Severity.Error);
        ///     public static MessageCode Bar = new MessageCode(123, Severity.BlockingWarning);
        ///     public static MessageCode Woo = new MessageCode(123, Severity.Information);
        ///     public static MessageCode Todo = new MessageCode(-1, Severity.Error);
        ///   }
        /// }
        /// ]]>
        /// </code>
        /// and the app's bootstrapping sequence initializing the <see cref="IMessageCodeService"/> as follows
        /// <code>
        /// <![CDATA[
        /// void Main(){
        ///   var serviceConfiguration = 
        ///      XAct.DependencyLocator.Currrent.GetInstance<IMessageCodeResolvingServiceConfiguration>();
        ///   serviceConfiguration.Types.Add(typeof(MessageCodes));
        ///   serviceConfiguration.DefaultType = MessageCodes.Todo;
        /// }
        /// ]]>
        /// </code>
        /// the following will resolve as follows:
        /// <code>
        /// <![CDATA[
        /// MessageCode messageCode = 
        ///   XAct.DependencyLocator.Current.GetInstance<IMessageCodeResolvingService>().Resolve("woo");
        /// Debug.Assert(messageCode.Id == "Woo");
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="messageCodeIdentifier">The message code identifier.</param>
        /// <returns></returns>
        MessageCode Resolve(string messageCodeIdentifier);
    }
}