﻿namespace App.Core.Infrastructure.Services
{
    using System;

    /// <summary>
    /// Contract for a service to generate sequential Guids suitable
    ///             for distributed scenarios.
    /// 
    /// </summary>
    public interface IDistributedIdService : IHasAppCoreService
    {
        /// <summary>
        /// Generates a Guid that is unique across machines, 
        /// while keeping them sequential (an important
        /// improvement over what Sql Server's SEQUENTIALID() 
        /// method does).
        /// </summary>
        /// <returns></returns>
        Guid NewGuid();
    }
}
