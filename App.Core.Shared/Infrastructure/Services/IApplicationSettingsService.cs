﻿
namespace App.Core.Infrastructure.Services
{
    using App.Core.Infrastructure;

    /// <summary>
    /// Contract for a service to return
    /// Tier specific application wide settings.
    /// </summary>
    public interface IApplicationSettingsService : IHasAppCoreService
    {
        /// <summary>
        /// Get an object containing all the 
        /// Tier specific Application Settings.
        /// </summary>
        AppSettings AppSettings { get; }


        void Persist();

    }
}
