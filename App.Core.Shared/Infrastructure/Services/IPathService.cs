﻿namespace App.Core.Infrastructure.Services
{
    public interface IPathService : IHasAppCoreService
    {
        int GetRootLength(string path);

        char[] GetInvalidPathChars();

        char[] GetInvalidFileNameChars();

        string Combine(string directory, string fileName);

        string GetFileName(string path);

        string GetExtension(string fileName);

        string GetFileNameWithoutExtension(string path);

        string GetDirectoryName(string path);

        string ChangeExtension(string path, string extension);

        bool HasExtension(string path);

        bool IsPathRooted(string path);

    }
}
