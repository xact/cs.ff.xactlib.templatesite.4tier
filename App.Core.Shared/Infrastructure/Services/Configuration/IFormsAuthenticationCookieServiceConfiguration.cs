﻿namespace App.Core.Infrastructure.Front.Implementation
{
    using System;

    public interface IFormsAuthenticationCookieServiceConfiguration : IHasAppCoreServiceConfiguration
    {
        TimeSpan SessionTimeOut { get; }
    }
}