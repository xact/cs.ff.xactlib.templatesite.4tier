﻿namespace App.Core.Infrastructure.Services.Configuration
{
    using XAct;

    /// <summary>
    /// Contract for a 
    /// Service Configuration object
    /// to contain the parameters required to 
    /// configure an implementation of the
    /// <see cref="IObjectMappingService"/>.
    /// <para>
    /// Invoked during initialization.
    /// </para>
    /// </summary>
    public interface IObjectMappingServiceConfiguration  : IHasAppServiceConfiguration, IHasInitialize
    {
    }


}
