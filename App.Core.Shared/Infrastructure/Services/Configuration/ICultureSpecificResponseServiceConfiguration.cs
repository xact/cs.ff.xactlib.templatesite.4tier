﻿namespace App.Core.Infrastructure.Services.Configuration
{
    using App.Core.Infrastructure.Services;
    using XAct;

    /// <summary>
    /// Contract for 
    /// package of configuration settings
    /// required by an implementation of the
    /// <see cref="ICultureSpecificResponseService"/>
    /// <para>
    /// Its default binding scope is <c>Singleton</c>.
    /// </para>
    /// </summary>
    public interface ICultureSpecificResponseServiceConfiguration : IHasAppServiceConfiguration, IHasInitialize
    {
        /// <summary>
        /// Gets or sets name of the resource set (filter)
        /// in which to search for the given Resource key.
        /// </summary>
        /// <value>
        /// The resource set name.
        /// </value>
        string ResourceSet { get; set; }

        /// <summary>
        /// Gets or sets the message 
        /// resource key format.
        /// <para>
        /// Default value is "{0}".
        /// </para>
        /// </summary>
        /// <value>
        /// The resource key format.
        /// </value>
        string ResourceKeyFormat { get; set; }


        /// <summary>
        /// Gets or sets the additional information 
        /// resource key format.
        /// <para>
        /// Default value is "{0}".
        /// </para>
        /// </summary>
        /// <value>
        /// The resource key format.
        /// </value>
        string AdditionalInformationResourceKeyFormat { get; set; }

        /// <summary>
        /// Gets or sets the additional information url
        /// resource key format.
        /// <para>
        /// Default value is null.
        /// </para>
        /// </summary>
        /// <value>
        /// The resource key format.
        /// </value>
        string AdditionalInformationUrlFormat { get; set; }
    }
}
