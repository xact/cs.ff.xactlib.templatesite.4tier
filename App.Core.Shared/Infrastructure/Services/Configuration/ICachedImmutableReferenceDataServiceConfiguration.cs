﻿namespace App.Core.Infrastructure.Services.Configuration
{
    using System;
    using App.Core;
    
    public interface ICachedImmutableReferenceDataServiceConfiguration : IHasAppCoreServiceConfiguration
    {
        bool TryGet(string shortTypeName, out Type referenceType);
        string[] GetKeys();

    }
}