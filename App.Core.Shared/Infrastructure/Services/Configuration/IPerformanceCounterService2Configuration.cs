﻿using System.Collections.Generic;
using System.Diagnostics;

namespace App.Core.Infrastructure.Services
{
    public interface IPerformanceCounterServiceConfiguration : IHasAppCoreServiceConfiguration
	{
		Dictionary<string, PerformanceCounter> PerformanceCounters { get; }

	}
}