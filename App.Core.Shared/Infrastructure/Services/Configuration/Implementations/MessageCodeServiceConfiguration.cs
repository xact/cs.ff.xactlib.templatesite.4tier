﻿
using App.Core.Constants;

namespace App.Core.Infrastructure.Services.Configuration
{

    public class MessageCodeServiceConfiguration : XAct.Messages.Services.Configuration.Implementations.MessageCodeMetadataServiceConfiguration
    {

        public MessageCodeServiceConfiguration() : base()
        {
            base.Scan(typeof(MessageCodes));
        }
    }
}
