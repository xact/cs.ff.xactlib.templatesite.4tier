﻿namespace App.Core.Infrastructure.Services.Configuration.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct;
    using XAct.ObjectMapping;
    using XAct.Services;

    /// <summary>
    /// Implementation of the 
    /// <see cref="IObjectMappingServiceConfiguration"/>
    /// contract for a 
    /// Service Configuration object
    /// to contain the parameters required to 
    /// configure an implementation of the
    /// <see cref="IObjectMappingService"/>.
    /// <para>
    /// Invoked during initialization.
    /// </para>
    /// </summary>
    public class ObjectMappingServiceConfiguration : IObjectMappingServiceConfiguration 
    {
        private readonly IObjectMappingRegistrationService _objectMappingManagementService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectMappingServiceConfiguration"/> class.
        /// </summary>
        /// <param name="objectMappingManagementService">The object mapping management service.</param>
        public ObjectMappingServiceConfiguration(IObjectMappingRegistrationService objectMappingManagementService)
        {
            _objectMappingManagementService = objectMappingManagementService;

            Initialize();
        }

        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="T:XAct.IHasInitialize" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized
        {
            get { return _initialized; }
            set
            {
                throw new ArgumentException("Cannot set Initialized.");
            }
        }
        private bool _initialized;



        public void Initialize()
        {

            if (_initialized)
            {
                return;
            }

            var maps = System.AppDomain.CurrentDomain.GetTypesDecoratedWithAttribute<ObjectMapAttribute>(null, true);

            foreach (KeyValuePair<Type, ObjectMapAttribute> kvp in maps)
            {
                string contextName = null;
                if (kvp.Key.IsAbstract)
                {
                    continue;
                }

                //Causes recursion-- probably due to some poorly constructed map or custom resolver..which I don't have the 
                //time to find now:
                //var typeMapperInstance = (ITypeMapper) XAct.DependencyResolver.Current.GetInstance(kvp.Key);
                ITypeMapper typeMapperInstance = (ITypeMapper) System.Activator.CreateInstance(kvp.Key);

                _objectMappingManagementService.RegisterMapper(typeMapperInstance, contextName);
            }

            _initialized = true;
        }

    }
}