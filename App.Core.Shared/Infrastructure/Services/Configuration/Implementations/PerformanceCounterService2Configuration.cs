﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using XAct.Services;

namespace App.Core.Infrastructure.Services.Configuration.Implementations
{
    using XAct;

    [DefaultBindingImplementation(typeof(IPerformanceCounterServiceConfiguration),BindingLifetimeType.SingletonScope,Priority.Low)]
	public class PerformanceCounterService2Configuration : IPerformanceCounterServiceConfiguration
	{

		public Dictionary<string, PerformanceCounter> PerformanceCounters
		{
			get { return _performanceCounters; }
		}
		Dictionary<string, PerformanceCounter> _performanceCounters = new Dictionary<string, PerformanceCounter>();


		public PerformanceCounterService2Configuration()
		{
			//This try / catch block is adding the Performance Counters to APP.
			//If the counters are not created - the handled exception will occur, but the application ca carry on
			//An application PerformanceCountersCreate  is used to create the counters.
			//To do that follow the steps:
			//1.) Get the latest version of your APP project
			//2.) Get the latest version of the PerformanceCountersCreate project from the TFS location $/APP/Src/Tools/PerformanceCountersCreate.
			//3.) Compile PerformanceCountersCreate project and run it's console application with Administrator privilege
			//This will create all necessary counters on your machine and now your APP solution will stop complaining about the missing Performance Counters.


			try
			{

			}
			catch (Exception)
			{

			}
			
			
		}
	}
}