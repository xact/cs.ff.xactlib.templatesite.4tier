﻿namespace App.Core.Infrastructure.Configuration.Implementations
{
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services.Configuration;
    using XAct;
    using XAct.Services;

    /// <summary>
    /// An implementation of the 
    /// <see cref="ICultureSpecificResponseServiceConfiguration"/>
    /// to provide a package of configuration settings
    /// required by an implementation of the
    /// <see cref="ICultureSpecificResponseService"/>
    /// <para>
    /// Its default binding scope is <c>Singleton</c>.
    /// </para>
    /// </summary>
// ReSharper disable RedundantArgumentDefaultValue
    [DefaultBindingImplementation(typeof(ICultureSpecificResponseServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.Normal)]
// ReSharper restore RedundantArgumentDefaultValue
    public class CultureSpecificResponseServiceConfiguration : ICultureSpecificResponseServiceConfiguration
    {


        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="T:XAct.IHasInitialize" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized { get; private set; }


        /// <summary>
        /// Gets or sets name of the resource set (filter)
        /// in which to search for the given Resource key.
        /// </summary>
        /// <value>
        /// The resource set name.
        /// </value>
		public string ResourceSet { get { return _resourceSet; } set { _resourceSet = value; } }
	    private string _resourceSet = "MessageCode";

        /// <summary>
        /// Gets or sets the resource key format.
        /// <para>
        /// Default value is "{0}".
        /// </para>
        /// </summary>
        /// <value>
        /// The resource key format.
        /// </value>
        public string ResourceKeyFormat
        {
            get { return _resourceKeyFormat; }
            set { _resourceKeyFormat = (value.IsNullOrEmpty() == false) ? value : "{0}"; }
        }
        private string _resourceKeyFormat = "{0}";



        /// <summary>
        /// Gets or sets the additional information 
        /// resource key format.
        /// <para>
        /// Default value is "{0}".
        /// </para>
        /// </summary>
        /// <value>
        /// The resource key format.
        /// </value>
        public string AdditionalInformationResourceKeyFormat
        {
            get { return _additionalInformationResourceKeyFormat; }
            set { _additionalInformationResourceKeyFormat = value; }
        }
        private string _additionalInformationResourceKeyFormat = string.Empty;

        /// <summary>
        /// Gets or sets the additional information url
        /// resource key format.
        /// <para>
        /// Default value is null.
        /// </para>
        /// </summary>
        /// <value>
        /// The resource key format.
        /// </value>
        public string AdditionalInformationUrlFormat
        {
            get { return _additionalInformationUrlFormat; }
            set { _additionalInformationUrlFormat = value; }
        }
        private string _additionalInformationUrlFormat = string.Empty;


        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
              if (Initialized)
            {
                return;
            }

            lock (this)
            {
                if (Initialized)
                {
                    return;
                }
                //Do nothing specific.

                //Set flag:
                Initialized = true;
            }

        }

    }
}