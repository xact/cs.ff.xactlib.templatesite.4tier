﻿
namespace App.Core.Infrastructure.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using App.Core;
    using XAct;
    using XAct.Messages;

    public interface ICultureSpecificResponseMappingService : IHasAppCoreService
    {
        Response Map(Response source);

        Response Map<TDataSource>(IResponse<TDataSource> source)
            where TDataSource : class, new();

            TResponse Map<TDataSource,TDataTarget,TResponse>(IResponse<TDataSource> source, TResponse target=null)
            where TDataSource : class, new()
            where TDataTarget : class, new()
            where TResponse : class, IResponse<TDataTarget>, new()
            ;
            
    }
}
