﻿namespace App.Core.Infrastructure.Services
{
    /// <summary>
    /// Contract for returning information
    /// specific about the Host machine/Device.
    /// </summary>
    public interface IDeviceInformationService : IHasAppCoreService
    {

        /// <summary>
        ///   Gets the unique device ID.
        ///   <para>
        ///     This is a Unique ID of the phone, 
        ///     hashed with the supplied key.
        ///   </para>
        /// <para>
        /// On a server, it's a hash of the domain/machine name.
        /// </para>
        /// </summary>
        /// <param name = "hashSalt">The hash key.</param>
        /// <remarks>
        ///   <para>
        ///   </para>
        /// </remarks>
        /// <returns>A 40 char (20bytes x 2chars) string.</returns>
        string GetUniqueDeviceID(string salt = null);
    }
}
