﻿using XAct;

namespace App.Core.Infrastructure.Services
{
    using App.Core;
    using App.Core.Domain.Models;

    /// <summary>
    /// Contract for a service to cache -- in a non-UI specific manner 
    /// (eg: a list of PhoneNumberType objects) --
    /// Reference data used by the various AppHosts.
    /// <para>
    /// Each AppHost in turn caches the data in a UI framework specific
    /// manner (eg: a list of SelectItem, or formatted html, representing
    /// the list of PhoneNumberType).
    /// </para>
    /// <para>
    /// Each service will in turn require access to ICachingService
    /// </para>
    /// <internal>
    /// Invoked by Implementation of <see cref="ICachedImmutableReferenceDataEntityService"/>
    /// </internal>
    /// </summary>
    public interface ICachedImmutableReferenceDataService : IHasAppCoreService
    {

        /// <summary>
        /// Gets a collection of Entity 
        /// sub-classes of <see cref="App.Core.Domain.Models.ReferenceDataBase{TId}"/>
        /// </summary>
        /// <typeparam name="TReferenceData"></typeparam>
        TReferenceData[] GetReferenceData<TReferenceData>()
            where TReferenceData : class, IHasReferenceData;

        /// <summary>
        /// Gets a collection of Entity 
        /// sub-classes of <see cref="App.Core.Domain.Models.ReferenceDataBase{TId}"/>
        /// </summary>
        /// <typeparam name="TReferenceData">The type of the reference data.</typeparam>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <returns></returns>
        TReferenceData[] GetReferenceData<TReferenceData, TEnum>()
            where TReferenceData : ReferenceDataBase<TEnum>
            where TEnum :struct 
            ;

    }
}
