﻿namespace App.Core.Infrastructure.Services
{
    using System;
    using XAct;
    using XAct.Messages;
    using XAct.Validation;

    /// <summary>
    /// Contract for a service to validate entites
    /// whose members are decorated with 
    /// <see cref="PropertyValidatorAttribute"/>
    /// </summary>
    public interface IValidatorService : IHasAppCoreService
    {
        /// <summary>
        /// Validates the specified object
        /// using <see cref="PropertyValidatorAttribute"/>s
        /// applied to the object's Properties 
        /// and/or Interface property contracts that the
        /// Properties implement.
        /// </summary>
        /// <typeparam name="T">The type of the object being validated.</typeparam>
        /// <param name="objectToValidate">The object to validate.</param>
        /// <param name="response">The response containing zero or more
        /// <see cref="XAct.Messages.Message"/> elememts of various <see cref="Severity"/>.</param>
        /// <param name="exitOnFirstValidationFailure">if set to <c>true</c> [exit on first validation failure].</param>
        /// <param name="contexts">The contexts.</param>
        /// <returns>The <see cref="IResponse.Success"/> value.</returns>
        bool Validate<T>(T objectToValidate, IResponse response, bool exitOnFirstValidationFailure = true, params string[] contexts);


        /// <summary>
        /// Validates the specified object
        /// using <see cref="PropertyValidatorAttribute" />s
        /// applied to the object's Properties
        /// and/or Interface property contracts that the
        /// Properties implement.
        /// </summary>
        /// <param name="typeOfObjectToValidate">The type of the object being validated.</param>
        /// <param name="objectToValidate">The object to validate.</param>
        /// <param name="response">The response containing zero or more
        /// <see cref="XAct.Messages.Message" /> elememts of various <see cref="Severity" />.</param>
        /// <param name="exitOnFirstValidationFailure">if set to <c>true</c> [exit on first validation failure].</param>
        /// <param name="contexts">The contexts.</param>
        /// <returns>
        /// The <see cref="IResponse.Success" /> value.
        /// </returns>
        bool Validate(Type typeOfObjectToValidate, object objectToValidate, IResponse response, bool exitOnFirstValidationFailure = true, params string[] contexts);


        /// <summary>
        /// Gets the ordered list of validators applied to given object.
        /// </summary>
        /// <param name="typeOfObectToValidate">The type of obect to validate.</param>
        /// <returns></returns>
        ValidatorServiceCacheModelItem GetSchema(Type typeOfObectToValidate);

    }
}
