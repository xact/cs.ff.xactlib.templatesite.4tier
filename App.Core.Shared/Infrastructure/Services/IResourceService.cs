﻿namespace App.Core.Application.Services
{
    using System.Collections.Generic;
    using System.Globalization;
    using App.Core.Domain.Messages;

    /// <summary>
    /// 
    /// </summary>
    public interface IResourceService : IHasAppCoreService
    {
        /// <summary>
        /// Gets a single localised resource string.
        /// </summary>
        /// <param name="resourceSetName">The filter.</param>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <returns></returns>
        string GetResource(string resourceSetName, string name, CultureInfo cultureInfo, ResourceTransformation resourceTransformation = ResourceTransformation.None);

        /// <summary>
        /// Gets a dictionary of a single Resource Filter set.
        /// </summary>
        /// <param name="resourceSetName">Name of the view.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <returns></returns>
        /// <internal>
        /// Notice how this method is only intended to be called from it's own 
        /// tier, so - unlike IResourceServiceFacade - it accepts 
        /// a CultureInfo (which is not serializable across WCF).
        ///   </internal>
        Dictionary<string, string> GetResourceSet(string resourceSetName, CultureInfo cultureInfo, ResourceTransformation resourceTransformation = ResourceTransformation.None);

        /// <summary>
        /// Returns a dictionary of sets of resources
        /// where the dictionary key is the resource Filter
        /// (often a View name).
        /// </summary>
        /// <param name="cultureInfo">Culture Info</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <param name="resourceSetNames">List of filters to retrieve resources for</param>
        /// <returns>Returns a Dictionary containing all resource strings.</returns>
        /// <internal>
        /// Notice how this method is only intended to be called from it's own 
        /// tier, so - unlike IResourceServiceFacade - it accepts 
        /// a CultureInfo (which is not serializable across WCF).
        ///   </internal>
        Dictionary<string, Dictionary<string, string>> GetResourceSets(CultureInfo cultureInfo, ResourceTransformation resourceTransformation = ResourceTransformation.None, params string[] resourceSetNames);



    }
}
