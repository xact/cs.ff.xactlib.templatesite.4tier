﻿namespace App.Core.Infrastructure.Services
{
    public interface IMarkdownTransformationService : IHasAppCoreService
    {

        /// <summary>
        /// Transforms the specified markdown text to html output.
        /// 
        /// </summary>
        /// <param name="markdownText">The markdown text.</param>
        /// <returns/>
        string Transform(string markdownText);

    }
}
