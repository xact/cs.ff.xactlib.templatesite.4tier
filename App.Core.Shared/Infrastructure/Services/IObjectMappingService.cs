﻿

namespace App.Core.Infrastructure.Services
{
    using System;

    /// <summary>
	/// Contract for a service 
	/// to perform mapping from one entity to another.
	/// </summary>
	/// <remark>
	/// <para>
	/// Note that the management of the mappers
	///             (the registering of Mappers) is handled
	///             by a different interface (IMappingManagementService)
	///             usually accessed only by the application's
	///             bootstrapper/initialization.
	///             (SOC).
	/// </para>
	/// </remark>    
    public interface IObjectMappingService : IHasAppCoreService
	{

        /// <summary>
        /// Means of checking as to which maps were registered.
        /// </summary>
        object Maps { get; }



        /// <summary>
        /// Test whether 
        /// </summary>
        bool Test();


	    /// <summary>
	    /// Test whether Mapping Exists between source and target.
	    /// </summary>
	    /// <typeparam name="TSource"></typeparam>
	    /// <typeparam name="TTarget"></typeparam>
	    /// <returns></returns>
	    bool TestMapExists<TSource, TTarget>();


	    bool TestMapIsValid<TSource, TTarget>(out Exception exception);

		/// <summary>
		/// Maps the given Source object to the instantiated target object.
		/// 
		/// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns/>
		TTarget Map<TSource, TTarget>(TSource source)
			where TSource : class
			where TTarget : class, new();

        /// <summary>
        /// Maps the given Source object to the instantiated target object.
        /// 
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns/>
        TTarget Map<TSource, TTarget>(TSource source, TTarget target)
            where TSource : class
            where TTarget : class;



    }
}

