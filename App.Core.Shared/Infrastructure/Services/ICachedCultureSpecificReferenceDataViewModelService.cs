﻿namespace App.Core.Infrastructure.Services
{
    using System.Globalization;
    using App.Core;
    using App.Core.Domain.Messages;

    /// <summary>
    /// Contract to cache projections of Reference Data.
    /// <para>
    /// The difference between <see cref="ReferenceDataViewModel"/>
    /// and <see cref="ReferenceDataMessage"/> is that <see cref="ReferenceDataViewModel"/>
    /// is already localized, ready to be used at the perimeter of the application.
    /// </para>
    /// <para>
    /// Only use this service at the outer boundary of the application.
    /// In the front tier, this is the UI, or API, and in the back tier, 
    /// this is Emails, and serialization to external services (IQOffice, etc.)
    /// </para>
    /// </summary>
    public interface ICachedCultureSpecificReferenceDataViewModelService : IHasAppCoreService
    {
        ///// <summary>
        ///// 
        ///// <para>
        ///// Method intended only for SmokeTesting as it gets all reference data.
        ///// Note that it fetches the data sequentially, in a chatty way.
        ///// </para>
        ///// </summary>
        //void GetAllReferenceData();


        /// <summary>
        /// Looks in local cache for the reference data set, and if not found invokes
        /// a remove service to retrieve the whole Reference Data set, caches it,
        /// and returns the whole set.
        /// </summary>
        /// <param name="typeName">Name of the type.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <returns></returns>
        ReferenceDataViewModel[] GetReferenceDataViewModel(string typeName, CultureInfo cultureInfo);

    }
}