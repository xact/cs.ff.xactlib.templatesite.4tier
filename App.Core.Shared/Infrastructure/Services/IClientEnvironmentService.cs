﻿
namespace App.Core.Infrastructure.Services
{
    using System.Globalization;

    /// <summary>
    /// Contract for a service 
    /// to describe the
    /// UserAgent/Client Environment.
    /// </summary>
    public interface IClientEnvironmentService : IHasAppCoreService
    {
        /// <summary>
        /// The CultureInfo of the User Agent.
        /// <para>
        /// This determines which language of the 
        /// Resources
        /// are returned to the User.
        /// </para>
        /// <para>
        /// Set by every call to an API.
        /// </para>
        /// </summary>
        CultureInfo ClientUICulture { get; }

        string RemoteIp { get; }

    }
}
