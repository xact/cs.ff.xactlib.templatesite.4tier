﻿using System;
using XAct;
using XAct.Services;
using VENDOR = XAct.Environment;

namespace App.Core.Infrastructure.Services.Implementations
{
    using App.Core.Infrastructure.Services;

    [DefaultBindingImplementation(typeof(IClientPersistenceService),BindingLifetimeType.Undefined,Priority.High)]
    public class ClientPersistenceService : IClientPersistenceService
    {
        private readonly VENDOR.IClientPersistenceService _clientPersistenceService;

        public ClientPersistenceService(VENDOR.IClientPersistenceService clientPersistenceService)
        {
            _clientPersistenceService = clientPersistenceService;
        }

        public string GetValue(string containerName, string key = null)
        {
            return _clientPersistenceService.GetValue(containerName, key);
        }

        public void SetValue(string containerName, string value, TimeSpan duration = default(TimeSpan), string key = null)
        {
            _clientPersistenceService.SetValue(containerName, value,duration, key);
        }

        public void ClearContainer(string containerName)
        {
            _clientPersistenceService.ClearContainer(containerName);
        }
    }
}