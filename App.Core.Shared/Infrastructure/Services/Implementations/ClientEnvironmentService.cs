﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Globalization;
    using XAct.Environment;
    using XAct.Services;
    using XAct.Web.Environment;

    /// <summary>
    /// An implementation of the 
    /// <see cref="Services.IClientEnvironmentService"/>
    /// to describe the
    /// UserAgent/Client Environment.
    /// </summary>
    public class ClientEnvironmentService : Services.IClientEnvironmentService
    {
        private readonly IClientEnvironmentService _clientEnvironmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientEnvironmentService" /> class.
        /// </summary>
        public ClientEnvironmentService(IWebClientEnvironmentService clientEnvironmentService)
        {
            _clientEnvironmentService = clientEnvironmentService;

            //Leave
        }

        /// <summary>
        /// The CultureInfo of the User Agent.
        /// <para>
        /// This determines which language of the
        /// Resources
        /// are returned to the User.
        /// </para>
        /// <para>
        /// Set by every call to an API.
        /// </para>
        /// </summary>
        public CultureInfo ClientUICulture
        {
            get
            {
                return _clientEnvironmentService.ClientUICulture;
            }
        }

        public string RemoteIp
        {
            get
            {
                string result = _clientEnvironmentService.ClientIP;

                return result;
            }
        }

    }
}