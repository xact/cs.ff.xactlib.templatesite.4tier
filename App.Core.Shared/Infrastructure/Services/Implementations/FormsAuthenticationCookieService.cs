﻿namespace App.Core.Infrastructure.Front.Implementation
{
    using System;
    using System.Web;
    using System.Web.Security;
    using XAct;
    using XAct.Services;


    public class FormsAuthenticationCookieService : IFormsAuthenticationCookieService
    {
        private readonly IFormsAuthenticationCookieServiceConfiguration _formsAuthenticationCookieServiceConfiguration;
        private readonly Infrastructure.Services.IDateTimeService _dateTimeService;
        private readonly XAct.Environment.IPrincipalService _principalService;

        public FormsAuthenticationCookieService(
            IFormsAuthenticationCookieServiceConfiguration formsAuthenticationCookieServiceConfiguration,
            Infrastructure.Services.IDateTimeService dateTimeService, XAct.Environment.IPrincipalService principalService)
        {
            _formsAuthenticationCookieServiceConfiguration = formsAuthenticationCookieServiceConfiguration;
            _dateTimeService = dateTimeService;
            _principalService = principalService;
        }

        public void SetSessionCookie(TimeSpan? duration = null,
                                     string cookiePath = null)
        {
            SetSessionCookie(_principalService.Principal.Identity.Name);
        }

        public void SetSessionCookie(string userName, string userData = null, TimeSpan? duration = null,
                                     string cookiePath = null)
        {
            if (cookiePath.IsNullOrEmpty())
            {
                cookiePath = FormsAuthentication.FormsCookiePath;
            }

            //if (duration == null)
            //{
            //    duration = _formsAuthenticationCookieServiceConfiguration.SessionTimeOut;
            //}

            //To find a session that has timed out, one needs a cookie.
            //The cookie is kept for the session.
            //But the data within it is timed. If it's only valid for 20 minutes, 
            //it will expire. And need a new session...not right.
            //The cookie needs to last longer.
            //The max a session can be is 8 hours.
            //So to find a session that has timed out, make it 8...or more.
            //Making it sliding @ 20 mins won't solve the problem either.
            duration = TimeSpan.FromHours(24);

            //if (duration == TimeSpan.MinValue)
            //{
            //    duration = TimeSpan.FromMinutes(20);
            //}

            FormsAuthentication.Initialize();

            //Create ticket:
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                2, //Default version is 2
                userName,
                _dateTimeService.NowUTC,
                _dateTimeService.NowUTC.AddMinutes(duration.Value.TotalMinutes),
                false,
                userData,
                cookiePath);

            // Encrypt the ticket.
            string encryptedTicket = FormsAuthentication.Encrypt(ticket);

            string cookieName = FormsAuthentication.FormsCookieName;
            HttpCookie httpCookie = new HttpCookie(cookieName, encryptedTicket);

            //Leave blank, so that the cookie is only a session cookie: 
            //httpCookie.Expires

            HttpContext.Current.Response.Cookies.Add(httpCookie);

        }

        public void RemoveSessionCookie()
        {
            try
            {
                FormsAuthentication.SignOut();
            }
            catch
            {
                HttpContext.Current.Request.Cookies.Remove(FormsAuthentication.FormsCookieName);
                HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
                
            }
        }


    }
}