﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using XAct.Services;

    public class PathService : IPathService
    {
        private readonly XAct.IO.IPathService _pathService;

        public PathService(XAct.IO.IPathService pathService)
        {
            _pathService = pathService;
        }


        public int GetRootLength(string path)
        {
            return _pathService.GetRootLength(path);
        }

        public char[] GetInvalidPathChars()
        {
            return _pathService.GetInvalidPathChars();
        }

        public char[] GetInvalidFileNameChars()
        {
            return _pathService.GetInvalidFileNameChars();
        }

        public string Combine(string directory, string fileName)
        {
            return _pathService.Combine(directory, fileName);
        }

        public string GetFileName(string path)
        {
            return _pathService.GetFileName(path);
        }

        public string GetExtension(string fileName)
        {
            return _pathService.GetExtension(fileName);
        }

        public string GetFileNameWithoutExtension(string path)
        {
            return _pathService.GetFileNameWithoutExtension(path);
        }

        public string GetDirectoryName(string path)
        {
            return _pathService.GetDirectoryName(path);
        }

        public string ChangeExtension(string path, string extension)
        {
            return _pathService.ChangeExtension(path, extension);
        }

        public bool HasExtension(string path)
        {
            return _pathService.HasExtension(path);
        }

        public bool IsPathRooted(string path)
        {
            return _pathService.IsPathRooted(path);
        }

    }
}