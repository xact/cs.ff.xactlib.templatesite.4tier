namespace App.Core.Infrastructure.Services
{
    using System;
    using App.Core.Infrastructure.Front.Services;
    using App.Core.Infrastructure._DEBUG.LIB;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// Implementation of a service to invoke remote Back Tier service facades,
    /// and map the response object's payload to a front end presentation ready model (often a ViewModel).
    /// </summary>
    public class CultureSpecificValidatedQueryResponseService: ICultureSpecificValidatedQueryResponseService
    {
        private readonly IWcfServiceAgentService _serviceAgentService;
        private readonly ICultureSpecificResponseMappingService _cultureSpecificResponseMappingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CultureSpecificValidatedQueryResponseService"/> class.
        /// </summary>
        /// <param name="serviceAgentService">The service agent service.</param>
        /// <param name="cultureSpecificResponseMappingService">The culture specific response mapping service.</param>
        public CultureSpecificValidatedQueryResponseService(
            IWcfServiceAgentService serviceAgentService,
            ICultureSpecificResponseMappingService cultureSpecificResponseMappingService)
        {
            _serviceAgentService = serviceAgentService;
            _cultureSpecificResponseMappingService = cultureSpecificResponseMappingService;
        }

        /// <summary>
        /// Performs a query on a remote back tier Application Facade Service operation,
        /// and maps the returned Response's Data payload to a model type appropriate for the front
        /// tier presentation layer service (often a ViewModel).
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TServiceResponseModel">The type of the service response model.</typeparam>
        /// <typeparam name="TResponseViewModel">The type of the response view model.</typeparam>
        /// <param name="func">The function.</param>
        /// <param name="postProcessTheResponse">An optional post processing of the response, prior to mapping to a viewmodel.</param>
        /// <param name="postProcessViewModel">An optional post processing of the final response.</param>
        /// <returns></returns>
        public Response<TResponseViewModel> Query<TService, TServiceResponseModel, TResponseViewModel>(
            Func<TService, Response<TServiceResponseModel>> func,
            Action<IResponse<TServiceResponseModel>> postProcessTheResponse = null,
            Action<IResponse<TResponseViewModel>> postProcessViewModel = null)
                        where TServiceResponseModel : class, new()
            where TResponseViewModel : class, new()
        {

            try
            {
                //Validate:
                //TODO


                //Invoke the service and get back a Response or Responnse<TDataSource>
                Response<TServiceResponseModel> response1 = _serviceAgentService.Query(func);


                if (postProcessTheResponse != null)
                {
                    //Note:
                    //I originally was checking to see if success before invoking this, 
                    //but it's possible the post processing is specifally to accept, and clear out
                    //a blockingwarning, under some circumstances.
                    postProcessTheResponse.Invoke(response1);
                }

                if (!response1.Success)
                {
                    //TResponse2 response2 = new TResponse2();
                    //return;
                }

                //Transfer settings:
                //Map the Response<Entity> to Response<ViewModel>:
                //ensuring that Messages are transferred, 
                //then Culture specific translation of MessageCodes are done
                //and attached to output object:
                Response<TResponseViewModel> viewModelResponse =
                    _cultureSpecificResponseMappingService
                        .Map<TServiceResponseModel, TResponseViewModel, Response<TResponseViewModel>>(response1);


                if (postProcessViewModel != null)
                {
                    postProcessViewModel.Invoke(viewModelResponse);
                }

                //Done:
                return viewModelResponse;
            }
            catch (System.Exception e)
            {
                if (System.ServiceModel.OperationContext.Current == null)
                {
                    throw;
                }
                throw SoapExceptionFactory.CreateFaultException(true, e);
            }

        }


        public Response Query<TService>(Func<TService, Response> func, Action<IResponse> postProcessResponse = null)
        {
            //Validate:
            //TODO

            try
            {
                //Invoke the service and get back a Response or Responnse<TDataSource>
                Response response = _serviceAgentService.Query(func);

                if (postProcessResponse != null)
                {
                    //Note:
                    //I originally was checking to see if success before invoking this, 
                    //but it's possible the post processing is specifally to accept, and clear out
                    //a blockingwarning, under some circumstances.
                    postProcessResponse.Invoke(response);
                }

                return _cultureSpecificResponseMappingService.Map(response);
            }
            catch (System.Exception e)
            {
                if (System.ServiceModel.OperationContext.Current == null)
                {
                    throw;
                }
                throw SoapExceptionFactory.CreateFaultException(true, e);
            }
        }

    }
}