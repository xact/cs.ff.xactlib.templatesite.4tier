﻿namespace App.Core.Application.Services.Implementations
{
    using System;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.Linq;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Services;
    using App.Core.Infrastructure.Services;

    [DefaultBindingImplementation(typeof(IImmutableReferenceDataEnumManagementService),BindingLifetimeType.Undefined,Priority.Low)]
    public class IMmutableReferenceDataEnumManagementService : IImmutableReferenceDataEnumManagementService
    {

        private readonly IClientEnvironmentService _clientEnvironmentService;
        private readonly IResourceService _cultureSpecifiCachedResourceService;
        private readonly ICachedImmutableReferenceDataMessageService _cachedCachedReferenceDataMessageService;
        private readonly ICachedCultureSpecificReferenceDataViewModelService _cultureSpecificCachedReferenceDataViewModelService;



        public IMmutableReferenceDataEnumManagementService(IClientEnvironmentService clientEnvironmentService,
                                                  IResourceService cultureSpecifiCachedResourceService,
                                                  ICachedImmutableReferenceDataMessageService cachedCachedReferenceDataMessageService,
                                                  ICachedCultureSpecificReferenceDataViewModelService cultureSpecificCachedReferenceDataViewModelService)
        {
            _clientEnvironmentService = clientEnvironmentService;
            _cultureSpecifiCachedResourceService = cultureSpecifiCachedResourceService;
            _cachedCachedReferenceDataMessageService = cachedCachedReferenceDataMessageService;
            _cultureSpecificCachedReferenceDataViewModelService = cultureSpecificCachedReferenceDataViewModelService;
        }


        /// <summary>
        /// Retrieves from cache
        /// the string resource associated to the given Enum.
        /// <para>
        /// If the resource is not in cache,
        /// invokes <see cref="ICultureSpecificResourceService" />
        /// to retrieve it from the back tier.
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumValue"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">T must be an enumerated type</exception>
        public string ToResource<T>(T enumValue, CultureInfo cultureInfo = null)
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }
            if (cultureInfo == null)
            {
                cultureInfo = _clientEnvironmentService.ClientUICulture;
            }

            //Get the name of the Enum type
            //which we will use as basis of key
            string resourceSet = typeof(T).Name;

            string resourceKey = enumValue.ToString();

            string result = _cultureSpecifiCachedResourceService.GetResource(
                resourceSet,
                resourceKey,
                cultureInfo,
                ResourceTransformation.None);

            return result;
        }

        public string TextToResource(CultureInfo cultureInfo, string text, params object[] textArguments)
        {
            if (cultureInfo == null)
            {
                cultureInfo = _clientEnvironmentService.ClientUICulture;
            }

            return string.Format(cultureInfo, text, textArguments);
        }

        public ReferenceDataViewModel ReferenceDataCodeToReferenceDataViewModel<T>(string tag, CultureInfo cultureInfo)
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            //Get the name of the Enum type
            //which we will use as basis of key
            string enumTypeName = typeof(T).Name;

            ReferenceDataViewModel[] referenceDataViewModels =
                GetReferenceDataViewModels(enumTypeName, cultureInfo);

            return referenceDataViewModels.Single(s => s.Tag == tag);
        }



        public ReferenceDataViewModel EnumToReferenceDataViewModel<T>(T enumValue, CultureInfo cultureInfo)
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            //Get the name of the Enum type
            //which we will use as basis of key
            string enumTypeName = typeof(T).Name;

            ReferenceDataViewModel[] referenceDataViewModels =
                GetReferenceDataViewModels(enumTypeName, cultureInfo);

            string enumValueName = enumValue.ToString(CultureInfo.InvariantCulture);

            ReferenceDataViewModel result = referenceDataViewModels.Single(s => s.Key == enumValueName);

            return result;
        }


        public ReferenceDataMessage EnumToReferenceData<T>(T enumValue)
            where T : struct, IConvertible
        {

            string enumValueName = enumValue.ToString(CultureInfo.InvariantCulture);

            ReferenceDataMessage result = EnumToReferenceDataSet<T>().Single(s => s.Text == enumValueName);

            return result;
        }


		
        private ReferenceDataMessage[] EnumToReferenceDataSet<T>()
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            //Get the name of the Enum type
            //which we will use as basis of key
            string enumTypeName = typeof(T).Name;

            ReferenceDataMessage[] referenceDataSet = GetReferenceDataSet(enumTypeName);

            return referenceDataSet;
        }


        public string EnumToReferenceDataCode<TReferenceEnum>(TReferenceEnum enumValue)
            where TReferenceEnum : struct, IConvertible
        {
            ReferenceDataMessage referenceData = EnumToReferenceData(enumValue);

            string ReferenceDataCode = referenceData.Tag;

            return ReferenceDataCode;
        }

        public string ReferenceDataCodeToEnumValue<TReferenceEnum>(string ReferenceDataCode) 
			where TReferenceEnum : struct, IConvertible
        {
            throw new NotImplementedException();
        }

        public bool ValidateReferenceDataCodeEnumValue<TReferenceEnum>(string ReferenceDataCode) where TReferenceEnum : struct, IConvertible
        {
            ReferenceDataMessage[] referenceDataSet = EnumToReferenceDataSet<TReferenceEnum>();
            ReferenceDataMessage referenceData = referenceDataSet.SingleOrDefault(s => (string.Compare(s.Tag, ReferenceDataCode,StringComparison.InvariantCultureIgnoreCase) == 0));
	        if (referenceData == null) return false;
			TReferenceEnum result;
            return Enum.TryParse(referenceData.Text, out result);
        }

        public ReferenceDataMessage ReferenceDataCodeToReferenceData<TReferenceEnum>(string ReferenceDataCode)
            where TReferenceEnum : struct, IConvertible
        {
            ReferenceDataMessage[] referenceDataSet = EnumToReferenceDataSet<TReferenceEnum>();

            ReferenceDataMessage referenceData = referenceDataSet.Single(s => (string.Compare(s.Tag, ReferenceDataCode, StringComparison.InvariantCultureIgnoreCase) == 0));

            return referenceData;
        }


        public TReferenceEnum ReferenceDataCodeToEnum<TReferenceEnum>(string ReferenceDataCode)
            where TReferenceEnum : struct, IConvertible
        {
            //Have Code -- but need to get back to enum
            //using ReferenceData as go between.

            //Get single ReferenceData (non culture specific):
            ReferenceDataMessage[] referenceDataSet = EnumToReferenceDataSet<TReferenceEnum>();
            ReferenceDataMessage referenceData = referenceDataSet.Single(s => (string.Compare(s.Tag, ReferenceDataCode, StringComparison.InvariantCultureIgnoreCase) == 0));

            //From that, parse back to the enum element:
            TReferenceEnum result;
            Enum.TryParse<TReferenceEnum>(referenceData.Text, out result);

            //Done:
            return result;
        }


        public bool ReferenceDataCodeToEnum<TReferenceEnum>(string ReferenceDataCode, out TReferenceEnum result)
            where TReferenceEnum : struct, IConvertible
        {
            //Have Code -- but need to get back to enum
            //using ReferenceData as go between.

            //Get single ReferenceData (non culture specific):
            ReferenceDataMessage[] referenceDataSet = EnumToReferenceDataSet<TReferenceEnum>();
            ReferenceDataMessage referenceData = referenceDataSet.Single(s => (string.Compare(s.Tag, ReferenceDataCode, StringComparison.InvariantCultureIgnoreCase) == 0));

            //From that, parse back to the enum element:
            bool r = Enum.TryParse<TReferenceEnum>(referenceData.Text, out result);

            //Done:
            return r;
        }

        private ReferenceDataMessage[] GetReferenceDataSet(string enumTypeName)
        {
            ReferenceDataMessage[] referenceDatas =
                _cachedCachedReferenceDataMessageService.GetReferenceData(enumTypeName);

            return referenceDatas;
        }

        private ReferenceDataViewModel[] GetReferenceDataViewModels(string enumTypeName, CultureInfo cultureInfo)
        {
            if (cultureInfo == null)
            {
                cultureInfo = _clientEnvironmentService.ClientUICulture;
            }

            Contract.Assert(cultureInfo != null);

            ReferenceDataViewModel[] referenceDataViewModels =
                _cultureSpecificCachedReferenceDataViewModelService
                    .GetReferenceDataViewModel(
                        enumTypeName,
                        cultureInfo);

            return referenceDataViewModels;
        }
    }
}