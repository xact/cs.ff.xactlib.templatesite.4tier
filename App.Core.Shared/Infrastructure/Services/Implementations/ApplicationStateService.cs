﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using System.Collections;
    using XAct.Services;
    using VENDOR = XAct.State;

    public class ApplicationStateService : IApplicationStateService
    {
        private readonly VENDOR.IApplicationStateService _applicationStateService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationStateService"/> class.
        /// </summary>
        /// <param name="applicationStateService">The application state service.</param>
        public ApplicationStateService(VENDOR.IApplicationStateService applicationStateService)
        {
            this._applicationStateService = applicationStateService;
        }

        public object this[string key]
        {
            get
            {
                return this._applicationStateService[key];
            }
            set
            {
                this._applicationStateService[key]=value;
            }
        }

        public int Count
        {
            get
            {
                return this._applicationStateService.Count;
            }
        }

        public ICollection Keys
        {
            get
            {
                return this._applicationStateService.Keys;
            }
        }

        public void Add(string key, object value)
        {
            this._applicationStateService.Add(key, value);
        }

        public void Clear()
        {
            this._applicationStateService.Clear();
        }

        public void Remove(string key)
        {
            this._applicationStateService.Remove(key);
        }

        public void RemoveAll()
        {
            this._applicationStateService.RemoveAll();
        }

        public void CopyTo(Array array, int index)
        {
            this._applicationStateService.CopyTo(array,index);
        }
    }
}