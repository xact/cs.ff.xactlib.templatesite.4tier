﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using XAct.Services;
    using VENDOR = XAct;

    public class DistributedIdService : IDistributedIdService
    {
        private readonly VENDOR.IDistributedIdService _distributedIdService;

        public DistributedIdService(VENDOR.IDistributedIdService distributedIdService)
        {
            this._distributedIdService = distributedIdService;
        }

        public Guid NewGuid()
        {
            //Generate sequential Guids, making for faster inserts into tables.
            return this._distributedIdService.NewGuid();
        }
    }
}