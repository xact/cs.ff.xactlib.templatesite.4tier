namespace App.Core.Infrastructure.Services
{
    using System;
    using XAct.Messages;
    using XAct.Services;

    public class MessageCodeMetadataService : IMessageCodeMetadataService
    {
        private readonly XAct.Messages.IMessageCodeMetadataService _messageCodeMetadataService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageCodeMetadataService"/> class.
        /// </summary>
        /// <param name="messageCodeMetadataService">The message code metadata service.</param>
        public MessageCodeMetadataService(XAct.Messages.IMessageCodeMetadataService messageCodeMetadataService)
        {
            _messageCodeMetadataService = messageCodeMetadataService;
        }

        public void Scan(params Type[] classesContainingStaticPropertiesOfTypeMessageCode)
        {
            _messageCodeMetadataService.Scan(classesContainingStaticPropertiesOfTypeMessageCode);
        }

        public bool TryGet(MessageCode messageCode, out MessageCodeMetadataAttribute messageCodeMetadataAttribute)
        {
            return _messageCodeMetadataService.TryGet(messageCode, out messageCodeMetadataAttribute);
        }

        public bool TryGet(string messageCodeIdentifier, out MessageCode messageCode,
                           out MessageCodeMetadataAttribute messageCodeMetadataAttribute)
        {
            return _messageCodeMetadataService.TryGet(messageCodeIdentifier, out messageCode, out messageCodeMetadataAttribute);
        }
    }
}