﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using XAct.IO;
    using XAct.Services;

    /// <summary>
    /// Service used to translated Markdown text to HTML.
    /// <para>
    /// Rich Templates (such as Email and help docs)  
    /// degrade poorly over time, leading to poor user experience
    /// over time, or expensive updates.
    /// This services takes care of this by allowing developers
    /// to create templates in Markdown, to be rendered to Html current
    /// for the day.
    /// </para>
    /// </summary>
    public class MarkdownTransformationService : IMarkdownTransformationService
    {
        private readonly XAct.IO.Transformations.IMarkdownService _markdownService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MarkdownTransformationService" /> class.
        /// </summary>
        /// <param name="markdownService">The markdown service.</param>
        public MarkdownTransformationService(XAct.IO.Transformations.IMarkdownService markdownService)
        {
            this._markdownService = markdownService;
        }

        /// <summary>
        /// Transforms the specified markdown text to html output.
        /// </summary>
        /// <param name="markdownText">The markdown text.</param>
        /// <returns></returns>
        public string Transform(string markdownText)
        {
            return this._markdownService.Transform(markdownText, Format.Html);
        }
    }
}