﻿namespace App.Core.Application.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services.Configuration;
    using XAct;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// Implementation of the 
    /// <see cref="ICultureSpecificResponseService"/> contract to
    /// process the <c>Id</c>
    /// (and <c>Arguments</c> if any)
    /// of an instance of <see cref="Message"/>,
    /// and set the <c>Message</c>
    /// property of the given <see cref="Message"/> 
    /// value to a culture specific resource string.
    /// <para>
    /// IMPORTANT:
    /// </para>
    /// <para>
    /// Think *CAREFULLY* before using this service in the back tier
    /// as rendering of this kind should be done only at the boundary
    /// of the application. Usually this means the UX. But it does 
    /// also mean external systems -- such as Email. 
    /// </para>
    /// </summary>
    public class CultureSpecificResponseService : ICultureSpecificResponseService
    {
        private readonly IResourceService _resourceService;
        private readonly ICultureSpecificResponseServiceConfiguration _cultureSpecificMessageServiceConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="CultureSpecificResponseService" /> class.
        /// </summary>
        /// <param name="cultureSpecificMessageServiceConfiguration">The culture specific message service configuration.</param>
        /// <param name="resourceService">The resource service.</param>
        public CultureSpecificResponseService(
            ICultureSpecificResponseServiceConfiguration cultureSpecificMessageServiceConfiguration,
            IResourceService resourceService)
        {
            _cultureSpecificMessageServiceConfiguration = cultureSpecificMessageServiceConfiguration;
            _cultureSpecificMessageServiceConfiguration.Initialize();

            _resourceService = resourceService;
        }

        /// <summary>
        /// Sets the presentation attributes on each <see cref="Message" />
        /// in the <see cref="IResponse" /> implementation.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="cultureInfo">The culture information.</param>
        public void SetPresentationAttributes(IResponse response, CultureInfo cultureInfo = null)
        {
            SetPresentationAttributes(response.Messages);
        }

	    public void SetPresentationAttributes(IList<Message> messages, CultureInfo cultureInfo = null)
	    {
            //If there are no messages to translate, don't bother doing anything:
			if (!messages.Any())
            {
                return;
            }

            //Iterate through each message
			foreach (Message message in messages)
            {
                //appending to each one, a MessagePresentationAttributes object
                //that contains the culture specific Resource Text.
                SetPresentationAttributes(message, cultureInfo);

				// Set nested messages (recursively)
				if (message.InnerMessages != null && message.InnerMessages.Any())
				{
					SetPresentationAttributes(message.InnerMessages);
				}
            }
        }



        /// <summary>
        /// Uses the <c>Id</c> value given
        /// (and <c>Arguments</c> if any)
        /// to find the related culture specific resource,
        /// and set the <c>IText</c>
        /// of the given <see cref="Message" />
        /// implementation.
        /// property.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="cultureInfo">The culture information.</param>
        public void SetPresentationAttributes(Message message, CultureInfo cultureInfo = null)
        {
            //For each message received
            //create a MessagePresentationAttributes
            //to contain the culture specific translation of the
            //message id,
            //and attach it to the Message.

            //We attach/expand the object, rather than replace
            //so that we keep the Message Id -- it could be of
            //use to the receive (eg M2M scenario, which doesn't care 
            //much about the actual text).
			// Check conversion back to Enum


			MessageCode messageCode = message.MessageCode;


            //Important: get the ResourceSet from the Config object.
            //(it will most probably be 'MessageCode'):
            string metaDataResourceFilter;
            string metaDataText;
            byte metatDataArgumentCount;

            messageCode.GetMetadata(
                out metaDataResourceFilter, 
                out metaDataText, 
                out metatDataArgumentCount);




            string resourceText;

            //but just in case we pass it through the config object's regex format to add prefix/suffix
            //ie, the enum might be "MessageCode.Something" and that would imply "Something" is the key, 
            //but maybe we are editing it to say "__Something__" is the resource key:
            if (!metaDataResourceFilter.IsNullOrEmpty())
            {
                //Text is only a Key if the Filter is set:
                string metaDataResourceKey =
                    _cultureSpecificMessageServiceConfiguration.ResourceKeyFormat.FormatStringInvariantCulture(
                        metaDataText);
                
                
                //Now that we have the ResourceSet and ResourceKey
                //We can look up text, additionalInfo, and url:

                //Get the Text:
                //Now get the raw resource for the text message:
                // (without parsing it):
                resourceText =
                    _resourceService.GetResource(metaDataResourceFilter, metaDataResourceKey, cultureInfo);
            }
            else
            {
                resourceText = metaDataText;

            }



            //Get the Additional Info:
            string additionalInformationText;

            if (!_cultureSpecificMessageServiceConfiguration.AdditionalInformationResourceKeyFormat.IsNullOrEmpty())
            {
                string additionalInformationResourceKey =
                    _cultureSpecificMessageServiceConfiguration.AdditionalInformationResourceKeyFormat
                                                               .FormatStringInvariantCulture(message.MessageCode.Id);

                try
                {
                    additionalInformationText = _resourceService.GetResource(metaDataResourceFilter,
                                                                           additionalInformationResourceKey, cultureInfo);
                }
                catch
                {
                    additionalInformationText = string.Empty;
                }
            }
            else
            {
                additionalInformationText = string.Empty;
            }



            //Get the Url:
            string additionalInformationUrlText;
            if (!_cultureSpecificMessageServiceConfiguration.AdditionalInformationUrlFormat.IsNullOrEmpty())
            {
                additionalInformationUrlText =
                    _cultureSpecificMessageServiceConfiguration.AdditionalInformationUrlFormat
                                                               .FormatStringInvariantCulture(message.MessageCode.Id);
            }
            else
            {
                additionalInformationUrlText = string.Empty;
            }


            //Now that we have all 3 parts: we can regex all 3:

            //Get the number of arguments the Regex pattern will expect:
            int requiredRegexArgumentCount = metatDataArgumentCount;


            int haveArgumentCount = (message.Arguments == null)?0: message.Arguments.Length;

            if (haveArgumentCount < requiredRegexArgumentCount)
            {
                throw new ArgumentOutOfRangeException(
                    string.Format(
                    "Argument Count does not match: {0} has {1} accounts, yet receiving: {2}",
                    messageCode.ToString(),
                    requiredRegexArgumentCount,
                    haveArgumentCount
                    ));
            }

            if (message.Arguments != null && message.Arguments.Length > 0)
            {
                //Regex complains of covariant conversions between string[] and object[] but tested in Linqpad and works:
                //although it will complain if it is a null array.
                resourceText = string.Format(cultureInfo, resourceText, message.Arguments);
                additionalInformationText = string.Format(cultureInfo, additionalInformationText, message.Arguments);
                additionalInformationUrlText = string.Format(cultureInfo, additionalInformationUrlText,
                                                             message.Arguments);
            }

            MessagePresentationAttributes messagePresentationAttributes =
                message.PresentationAttributes ?? (message.PresentationAttributes = new MessagePresentationAttributes());

            messagePresentationAttributes.Text =  resourceText;
            messagePresentationAttributes.AdditionalInformation = additionalInformationText;
            messagePresentationAttributes.AdditionalInformationUrl = additionalInformationUrlText;
        }


        public string GetMessage(MessageCode messageCode, CultureInfo cultureInfo = null)
        {


            //Important: get the ResourceSet from the Config object.
            //(it will most probably be 'MessageCode'):
            //Important: get the ResourceSet from the Config object.
            //(it will most probably be 'MessageCode'):
            string metaDataResourceFilter;
            string metaDataText;
            byte metatDataArgumentCount;

            messageCode.GetMetadata(
                out metaDataResourceFilter,
                out metaDataText,
                out metatDataArgumentCount);




            string resourceText;

            //but just in case we pass it through the config object's regex format to add prefix/suffix
            //ie, the enum might be "MessageCode.Something" and that would imply "Something" is the key, 
            //but maybe we are editing it to say "__Something__" is the resource key:
            if (!metaDataResourceFilter.IsNullOrEmpty())
            {
                //Text is only a Key if the Filter is set:
                string metaDataResourceKey =
                    _cultureSpecificMessageServiceConfiguration.ResourceKeyFormat.FormatStringInvariantCulture(
                        metaDataText);


                //Now that we have the ResourceSet and ResourceKey
                //We can look up text, additionalInfo, and url:

                //Get the Text:
                //Now get the raw resource for the text message:
                // (without parsing it):
                resourceText =
                    _resourceService.GetResource(metaDataResourceFilter, metaDataResourceKey, cultureInfo);
            }
            else
            {
                resourceText = metaDataText;

            }

            return resourceText;

        }


    }


}
