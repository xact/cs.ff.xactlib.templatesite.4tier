namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using XAct.Services;
    using VENDOR = XAct.Exceptions;

    public class ExceptionHandlingService : IExceptionHandlingService
    {

        private readonly VENDOR.IExceptionHandlingService _exceptionHandlingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlingService" /> class.
        /// </summary>
        /// <param name="exceptionHandlingService">The exception handling service.</param>
        public ExceptionHandlingService(VENDOR.IExceptionHandlingService exceptionHandlingService)
        {
            this._exceptionHandlingService = exceptionHandlingService;
        }

        public VENDOR.IExceptionHandlingConfiguration DefaultExceptionHandlingConfiguration
        {
            get
            {
                return this._exceptionHandlingService.Configuration;
            }
            set
            {
                this._exceptionHandlingService.Configuration = value;
            }
        }

        public Type DefaultExceptionType
        {
            get
            {
                return this._exceptionHandlingService.DefaultExceptionType;
            }
            set
            {
                this._exceptionHandlingService.DefaultExceptionType = value;
            }
        }

        public bool HandleException(ref Exception exception, string category = null)
        {
            return this._exceptionHandlingService.HandleException(ref exception, category);
        }

        public TResult Handle<TResult>(Func<TResult> function, string category = null)
        {
            try
            {
                return function.Invoke();
            }
            catch (System.Exception e)
            {
                if (this._exceptionHandlingService.HandleException(ref e))
                {
                    throw;
                }
                throw e;
            }
        }
        public void Handle(Action action, string category = null)
        {
            try
            {
                action.Invoke();
            }
            catch (System.Exception e)
            {
                if (this._exceptionHandlingService.HandleException(ref e))
                {
                    throw;
                }
                throw e;
            }
        }
    }
}