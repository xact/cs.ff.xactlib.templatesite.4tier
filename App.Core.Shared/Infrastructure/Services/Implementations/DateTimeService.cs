﻿namespace App.Core.Infrastructure.Services
{
    using System;
    using XAct.Environment;
    using XAct.Services;

    public class DateTimeService :IDateTimeService
    {
        private readonly XAct.Environment.IDateTimeService _dateTimeService;

        public DateTimeService(XAct.Environment.IDateTimeService dateTimeService)
        {
            _dateTimeService = dateTimeService;
        }

        public IDateTimeServiceConfiguration Configuration
        {
            get
            {
                return _dateTimeService.Configuration;
            }
        }

        public DateTime Now
        {
            get
            {
                return _dateTimeService.Now;
            }
        }

        public DateTime NowUTC
        {
            get
            {
                return _dateTimeService.NowUTC; }
        }

        public void SetUTCOffset(DateTime nowUTC)
        {
            _dateTimeService.SetUTCOffset(nowUTC);
        }

        public void SetUTCOffset(TimeSpan timeSpanOffset)
        {
            _dateTimeService.SetUTCOffset(timeSpanOffset);
        }

        public void ResetUTCOffset()
        {
            _dateTimeService.ResetUTCOffset();
        }
    }
}