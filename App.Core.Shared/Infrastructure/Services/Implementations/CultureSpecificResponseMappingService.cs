﻿namespace App.Core.Infrastructure.Services
{
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Messages;
    using XAct.Services;

    public class CultureSpecificResponseMappingService : ICultureSpecificResponseMappingService
    {
        private readonly ICultureSpecificResourceService _cultureSpecificResourceService;
        private readonly IObjectMappingService _objectMappingService;
        private readonly ICultureSpecificResponseService _cultureSpecificResponseService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CultureSpecificResponseMappingService" /> class.
        /// </summary>
        /// <param name="cultureSpecificResourceService">The culture specific resource service.</param>
        /// <param name="cultureSpecificResponseService"></param>
        public CultureSpecificResponseMappingService(ICultureSpecificResourceService cultureSpecificResourceService,
                                                     IObjectMappingService objectMappingService,
                                                     ICultureSpecificResponseService cultureSpecificResponseService)
        {
            _cultureSpecificResourceService = cultureSpecificResourceService;
            _objectMappingService = objectMappingService;
            _cultureSpecificResponseService = cultureSpecificResponseService;
        }

        public Response Map(Response source)
        {
            //Convert them to UI specific messages:
            _cultureSpecificResponseService.SetPresentationAttributes(source);

            return source;
        }


        public Response Map<TDataSource>(IResponse<TDataSource> source)
            where TDataSource : class, new()
        {

            if (source.GetType() == typeof(Response))
            {
                return source as Response;
            }
            var target = new Response();

            //Convert them to UI specific messages:
            _cultureSpecificResponseService.SetPresentationAttributes(source);


            source.TransferProperties(target);

            return target;
        }


        public TResponse Map<TDataSource, TDataTarget, TResponse>(IResponse<TDataSource> source,
                                                                  TResponse target =null)
            where TDataSource : class, new()
            where TDataTarget : class, new()
            where TResponse : class, IResponse<TDataTarget>, new()
        {

            if (typeof(TDataSource) == typeof(TDataTarget))
            {
                //Convert them to UI specific messages:
                _cultureSpecificResponseService.SetPresentationAttributes(source);
                return (TResponse)source;
            }

            if (target == null)
            {
                target = new TResponse();
            }

            //Transfer messages:
            source.TransferProperties(target);

            //Convert them to UI specific messages:
            _cultureSpecificResponseService.SetPresentationAttributes(target);

            //Done:
            return target;
        }
          
    }
}