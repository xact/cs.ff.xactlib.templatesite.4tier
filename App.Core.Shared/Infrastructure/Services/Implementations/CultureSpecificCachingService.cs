namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using System.Globalization;
    using App.Core.Infrastructure.Services;
    using XAct.Services;
    using VENDOR = XAct.Caching;

    /// <summary>
    /// Implementation of the 
    /// <see cref="ICultureSpecificCachingService"/> contract
    /// to persist 
    /// data according to specified <see cref="CultureInfo"/>.Name
    /// </summary>
    public class CultureSpecificCachingService : ICultureSpecificCachingService
    {
        private readonly VENDOR.ICultureInfoBasedCachingService _cultureInfoBasedCachingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CultureSpecificCachingService" /> class.
        /// </summary>
        /// <param name="cultureInfoBasedCachingService">The culture information based caching service.</param>
        /// <internal>
        /// NOTE:CachingService cannot in
        ///   </internal>
        public CultureSpecificCachingService(VENDOR.ICultureInfoBasedCachingService cultureInfoBasedCachingService)
// ReSharper disable RedundantBaseConstructorCall
            : base()
// ReSharper restore RedundantBaseConstructorCall
        {
            _cultureInfoBasedCachingService = cultureInfoBasedCachingService;

            //FAQ: Why the time consuming wrapping?
            //To protect the rest of the app from a vendor.
            //Even if the vendor is yourself, or you know 
            //the vendor personally, it's *not the app*.


            //FAQ: But isn't that slower than just using the vendor's service
            //all over the app?
            //It's certainly faster than having to write the whole service
            //yourself -- without giving up on integrity of protecting
            //your app from vendor rot, making it less upgradeable over time.
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="isValid">The is valid.</param>
        public void Add<TData>(CultureInfo cultureInfo, string key, TData objectToCache, Func<bool> isValid)
        { 
            _cultureInfoBasedCachingService.Set(cultureInfo,key,objectToCache,isValid);
        }

        /// <summary>
        /// Caches the given value for the specified time.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="expiryTimespan">The timespan to cache data for.</param>
        public void Add<TData>(CultureInfo cultureInfo, string key, TData objectToCache, TimeSpan expiryTimespan)
        {
            _cultureInfoBasedCachingService.Set(cultureInfo, key, objectToCache, expiryTimespan);
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        /// <param name="dataFactory">The data factory.Can be as simple as ()=&gt;(dataToCache)</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <param name="updateNow"></param>
        public void Add<TData>(CultureInfo cultureInfo, string key, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true, bool updateNow = true)
        {
            _cultureInfoBasedCachingService.Set(cultureInfo, key,dataFactory, expiryTimespan, selfUpdating);
        }


        /// <summary>
        /// Gets the <see cref="T:System.Object"/> with the specified key.
        /// 
        /// </summary>
        /// 
        /// <value/>
        /// <typeparam name="TData">The type of the data.</typeparam>
        public bool TryGet<TData>(CultureInfo cultureInfo, string key, out TData result)
        {
            return _cultureInfoBasedCachingService.TryGet(cultureInfo, key,out result);
        }

        /// <summary>
        /// Gets the <see cref="T:System.Object" /> with the specified key.
        /// If not found, registers the datafactory, and returns its results.
        /// <para>
        /// Note: basically combines TryGet and Add and into one operation.
        /// </para>
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <param name="dataFactory">The data factory.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <returns></returns>
        public bool TryGet<TData>(CultureInfo cultureInfo, string key, out TData result, Func<TData> dataFactory, TimeSpan expiryTimespan,
                                  bool selfUpdating = true)
        {
            return _cultureInfoBasedCachingService.TryGet(cultureInfo, key, out result,dataFactory,expiryTimespan,selfUpdating);
        }

        /// <summary>
        /// Removes the cached item that has the given key name.
        /// </summary>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        public void Remove(CultureInfo cultureInfo, string key)
        {
            _cultureInfoBasedCachingService.Remove(cultureInfo,key);
        }

        /// <summary>
        /// Clears out all cached items.
        /// 
        /// </summary>
        public void Clear(CultureInfo cultureInfo)
        {
            _cultureInfoBasedCachingService.Clear(cultureInfo);
        }
    }
}