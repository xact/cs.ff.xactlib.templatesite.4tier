﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using XAct.Services;
    using VENDOR = XAct.Environment;

    /// <summary>
    /// An implementation of the 
    /// <see cref="IApplicationTennantService"/> contract
    /// to manage the current tennant's unique identifier in a multi-tennant 
    /// application.
    /// <para>
    /// This application does not use a tennant based architecture, 
    /// but the service is provided anyway in order to query the value
    /// for debugging purposes.
    /// </para>
    /// </summary>
    public class ApplicationTennantService : IApplicationTennantService
    {
        private readonly VENDOR.IApplicationTennantService _applicationTennantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationTennantService"/> class.
        /// </summary>
        /// <param name="_applicationTennantService">The _application tennant service.</param>
        public ApplicationTennantService(VENDOR.IApplicationTennantService _applicationTennantService)
        {
            this._applicationTennantService = _applicationTennantService;
        }

        /// <summary>
        /// Gets the current application tennant's unique Id.
        /// <para>
        /// Some library services use a tennant identifier to focus
        /// repository queries to return data specific to the current tennant.
        /// </para>
        /// <para>
        /// This application does not use a tennant based architecture, 
        /// but the service is provided anyway in order to query the value
        /// for debugging purposes.
        /// </para>
        /// </summary>
        /// <returns></returns>
        public Guid Get()
        {
            //In this app, will always be Guid.Empty.
            return _applicationTennantService.Get();
        }
    }
}