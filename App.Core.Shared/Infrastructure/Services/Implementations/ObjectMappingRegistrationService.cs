﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Collections.Generic;
    using XAct.Services;
    using VENDOR = XAct.ObjectMapping;

    /// <summary>
    /// Implementation of the 
    /// <see cref="IObjectMappingRegistrationService"/> contract
    /// to configure the 
    /// <see cref="T:XAct.ObjectMapping.ITypeMapper"/>s
    /// that an implementation of IObjectMappingService
    /// will use.
    /// </summary>
    public class ObjectMappingRegistrationService : IObjectMappingRegistrationService
    {
        private readonly XAct.ObjectMapping.IObjectMappingRegistrationService _mappingRegistrationService;

        public VENDOR.ITypeMapper[] Maps { get { return _mapped.ToArray(); } }
        private readonly List<VENDOR.ITypeMapper> _mapped = new List<VENDOR.ITypeMapper>();



        /// <summary>
        /// Test whether 
        /// </summary>
        public bool Test()
        {
#if DEBUG
            AutoMapper.Mapper.AssertConfigurationIsValid();
#endif
            return true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectMappingRegistrationService"/> class.
        /// </summary>
        /// <param name="mappingRegistrationService">The mapping registration service.</param>
        public ObjectMappingRegistrationService(VENDOR.IObjectMappingRegistrationService mappingRegistrationService)
        {
            _mappingRegistrationService = mappingRegistrationService;
        }

        /// <summary>
        /// Registers the <see cref="T:XAct.ObjectMapping.ITypeMapper`2" /> with the service
        /// so that it can later be used by <see cref="T:XAct.ObjectMapping.IObjectMappingService" />
        /// </summary>
        /// <param name="typeMapper">The type mapper.</param>
        /// <param name="contextName">Name of the context.</param>
        public void RegisterMapper(VENDOR.ITypeMapper typeMapper, string contextName = null)
        {
            _mapped.Add(typeMapper);
            _mappingRegistrationService.RegisterMapper(typeMapper, contextName);
        }


    }
}