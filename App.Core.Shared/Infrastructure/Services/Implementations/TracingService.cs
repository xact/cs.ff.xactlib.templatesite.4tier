
using App.Core.LIB.Extensions;

namespace App.Core.Infrastructure.Services.Implementations
{
	using System;
	using System.Diagnostics;
	using XAct.Services;
	using VENDOR = XAct.Diagnostics;


	/// <summary>
	/// Implementation of the 
	/// application specific <see cref="ITracingService"/>
	/// which wraps a vendor service.
	/// </summary>
	public class TracingService : App.Core.Infrastructure.Services.ITracingService
	{
		private readonly VENDOR.ITracingService _tracingService;

		/// <summary>
		/// Initializes a new instance of the <see cref="TracingService" /> class.
		/// </summary>
		/// <param name="tracingService">The tracing service.</param>
		public TracingService(VENDOR.ITracingService tracingService)
		{
			//FAQ: Why the time consuming wrapping?
			//To protect the rest of the app from a vendor.
			//Even if the vendor is yourself, or you know 
			//the vendor personally, it's *not the app*.

			//FAQ: But isn't that slower than just using the vendor's service
			//all over the app?
			//It's certainly faster than having to write the whole service
			//yourself -- without giving up on integrity of protecting
			//your app from vendor rot, making it less upgradeable over time.

			_tracingService = tracingService;
		}

		public void QuickTrace(string message = null, params object[] arguments)
		{
			_tracingService.QuickTrace(message, arguments);
		}

		public void Trace(TraceLevel traceLevel, string message, params object[] messageArguments)
		{
			_tracingService.Trace(traceLevel.Map(), message, messageArguments);
		}

		public void Trace(int stackTraceOffset, TraceLevel traceLevel, string message, params object[] arguments)
		{
			_tracingService.Trace(stackTraceOffset, traceLevel.Map(), message, arguments);
		}

		public void TraceException(TraceLevel traceLevel, Exception exception, string message, params object[] arguments)
		{
			// Format the excpetion ourself, and pass it to the normal logging. (We want all the contents as a single entry
			Trace(traceLevel, exception.ToLogString(string.Format(message, arguments)));

			//_tracingService.TraceException(traceLevel.Map(), exception, message, arguments);
		}

		public void TraceException(int stackTraceFrameOffset, TraceLevel traceLevel, Exception exception, string message, params object[] arguments)
		{
			// Format the excpetion ourself, and pass it to the normal logging. (We want all the contents as a single entry
			Trace(stackTraceFrameOffset, traceLevel, exception.ToLogString(string.Format(message, arguments)));

			//_tracingService.TraceException(stackTraceFrameOffset, traceLevel.Map(), exception, message, arguments);
		}

		public void DebugTrace(TraceLevel traceLevel, string message, params object[] arguments)
		{
			_tracingService.DebugTrace(traceLevel.Map(), message, arguments);
		}

		public void DebugTrace(int stackTraceFrameOffset, TraceLevel traceLevel, string message, params object[] arguments)
		{
			_tracingService.DebugTrace(stackTraceFrameOffset, traceLevel.Map(), message, arguments);
		}

		public void DebugTraceException(TraceLevel traceLevel, Exception exception, string message, params object[] arguments)
		{
			_tracingService.DebugTraceException(traceLevel.Map(), exception, message, arguments);
		}

		public void DebugTraceException(int stackTraceFrameOffset, TraceLevel traceLevel, Exception exception, string message, params object[] arguments)
		{
			_tracingService.DebugTraceException(stackTraceFrameOffset, traceLevel.Map(), exception, message, arguments);
		}


		
	}

	public static class TraceLevelExtensions
	{
		public static XAct.Diagnostics.TraceLevel Map(this System.Diagnostics.TraceLevel traceLevel)
		{
			return ((XAct.Diagnostics.TraceLevel)traceLevel);
		}
	}
}