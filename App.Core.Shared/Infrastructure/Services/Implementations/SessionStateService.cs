﻿namespace App.Core.Infrastructure.Services
{
    using System;
    using System.Collections;
    using XAct.Services;

    public class SessionStateService : ISessionStateService
    {
        private readonly XAct.State.ISessionStateService _sessionStateService;

        public SessionStateService(XAct.State.ISessionStateService sessionStateService)
        {
            _sessionStateService = sessionStateService;
        }

        public object this[string key]
        {
            get
            {
                return _sessionStateService[key];
            }
            set
            {
                _sessionStateService[key] = value;
            }
        }

        public int Count
        {
            get
            {
                return _sessionStateService.Count;
            }
        }

        public ICollection Keys
        {
            get
            {
                return _sessionStateService.Keys;
                
            }
        }

        public void Add(string key, object value)
        {
            _sessionStateService.Add(key, value);
        }

        public void Clear()
        {
            _sessionStateService.Clear();
        }

        public void Remove(string key)
        {
            _sessionStateService.Remove(key);
        }

        public void RemoveAll()
        {
            _sessionStateService.RemoveAll();
        }

        public void CopyTo(Array array, int index)
        {
            _sessionStateService.CopyTo(array,index);
        }
    }
}