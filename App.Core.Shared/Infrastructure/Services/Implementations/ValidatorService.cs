﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using XAct.Messages;
    using XAct.Services;
    using VENDOR = XAct.Validation;

    public class ValidatorService : App.Core.Infrastructure.Services.IValidatorService
    {
        private readonly VENDOR.IValidatorService _validatorService;

        public ValidatorService(VENDOR.IValidatorService validatorService)
        {
            _validatorService = validatorService;
        }

        public bool Validate<T>(T objectToValidate, IResponse response, bool exitOnFirstValidationFailure = true,
                                params string[] contexts)
        {
            return _validatorService.Validate<T>(objectToValidate, response, exitOnFirstValidationFailure, contexts);
        }

        public bool Validate(Type typeOfObjectToValidate, object objectToValidate, IResponse response,
                             bool exitOnFirstValidationFailure = true, params string[] contexts)
        {
            return _validatorService.Validate(typeOfObjectToValidate, objectToValidate, response, exitOnFirstValidationFailure, contexts);
        }

        public VENDOR.ValidatorServiceCacheModelItem GetSchema(Type typeOfObectToValidate)
        {
            return _validatorService.GetSchema(typeOfObectToValidate);
        }
    }
}