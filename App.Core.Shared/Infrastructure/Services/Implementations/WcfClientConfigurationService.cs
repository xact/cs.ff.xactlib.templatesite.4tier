namespace App.Core.Infrastructure.Front.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.ServiceModel.Configuration;

    /// <summary>
    /// Implementation of the 
    /// <see cref="IWcfServiceAgentServiceConfiguration"/>
    /// to manage the configuration settings
    /// required to create WCF Session Agents.
    /// <para>
    /// Used by the <see cref="IWcfServiceAgentService"/>.
    /// </para>
    /// </summary>
    public class WcfServiceAgentServiceConfiguration : IWcfServiceAgentServiceConfiguration
    {
        //private IList<ChannelEndpointElement> _endPointConfigs;


        //private Dictionary<string, ChannelEndpointElement> _endpointConfigCache = new Dictionary<string, ChannelEndpointElement>();

        private Dictionary<string, string> _endpointConfigContractTypeToConfigNameCache = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
 
        /// <summary>
        /// Defines whether to use Direct call to AppFacade service operation
        /// or a remote invocation method.
        /// </summary>
        public bool IsNTier { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WcfServiceAgentServiceConfiguration"/> class.
        /// </summary>
        public WcfServiceAgentServiceConfiguration(/*No dependencies*/)
        {
            IsNTier = true;
            LoadEndpointsFromConfig();
        }

        /// <summary>
        /// Loads the endpoints from the web.config.
        /// </summary>
        /// <exception cref="System.Exception">Client secion missing in the configuration file.</exception>
        private void LoadEndpointsFromConfig()
        {

            _endpointConfigContractTypeToConfigNameCache.Clear();
            
            ClientSection clientSection = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            if (clientSection == null) throw new Exception("Client secion missing in the configuration file.");


                foreach (ChannelEndpointElement e in clientSection.Endpoints)
                {
                    //_endpointConfigCache[e.Contract] = e;
                    if (string.IsNullOrEmpty(e.Name))
                    {
                        throw new Exception(
                            string.Format("Client configuration for the service {0} is missing the name attribute",
                                          e.Contract));
                    }
                    _endpointConfigContractTypeToConfigNameCache[e.Contract] = e.Name;
                }

            //here we can put additional configuration validations - are all service configured? do they contain all required config attributes etc.
            
            //IDEA : I think we should inject the configuration to this service and not 'let' it read from the app config file.
            //the services and their behaviours are tightly coupled with this configuration and don't make sense without it.            
        }

        /// <summary>
        /// Gets the configuration name for a client for a particular service contract type from the web.config system.serviceModel configuration section.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// </exception>
        public string GetConfigNameForClient<TService>()
        {
            string typeFullName = typeof (TService).FullName;
            string configName;

            if (!_endpointConfigContractTypeToConfigNameCache.TryGetValue(typeFullName, out configName))
            {
                throw new Exception(
                    string.Format("Unable to find client configuration for the requested service type {0} or " +
                                  "contract attribute is missing.", typeFullName));
            }
            return configName;

            //ChannelEndpointElement clientConfig = _endPointConfigs.FirstOrDefault(config => config.Contract == typeFullName);
            //if (clientConfig == null)
            //    throw new Exception(string.Format("Unable to find client configuration for the requested service type {0} or " +
            //                                      "contract attribute is missing.", typeFullName));
            //if (string.IsNullOrEmpty(clientConfig.Name)) 
            //    throw new Exception(string.Format("Client configuration for the service {0} is missing the name attribute", typeFullName));

            //return clientConfig.Name;
        }
    }
}