﻿// ReSharper disable CheckNamespace

namespace App.Core.Application.Services.Implementations
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.Globalization;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="IResourceService" />
    /// to provide to the Front Tier any Resources it may need.
    /// </summary>
    public class ResourceService : IResourceService
    {
        private readonly ICultureSpecificResourceService _resourceService;


        /// <summary>
        ///     Initializes a new instance of the <see cref="ResourceService" /> class.
        /// </summary>
        /// <param name="resourceService">The resource service.</param>
        public ResourceService(ICultureSpecificResourceService resourceService)
        {
            _resourceService = resourceService;
        }


        /// <summary>
        /// Gets a single localised resource string.
        /// </summary>
        /// <param name="resourceSetName">The filter.</param>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <returns></returns>
        /// <internal>
        /// Notice how this method is only intended to be called from it's own
        /// tier, so - unlike IResourceServiceFacade - it accepts
        /// a CultureInfo (which is not serializable across WCF).
        ///   </internal>
        public string GetResource(string resourceSetName, string name, CultureInfo cultureInfo,
                                ResourceTransformation resourceTransformation = ResourceTransformation.None)
        {
            if (cultureInfo == null)
            {
                cultureInfo = CultureInfo.InvariantCulture;
            }

            string result = _resourceService.GetResource(resourceSetName, name, cultureInfo, resourceTransformation);

            return result;
        }

        /// <summary>
        /// Gets a dictionary of a single Resource Filter set.
        /// </summary>
        /// <param name="resourceSetName"></param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <returns></returns>
        /// <internal>
        /// Notice how this method is only intended to be called from it's own
        /// tier, so - unlike IResourceServiceFacade - it accepts
        /// a CultureInfo (which is not serializable across WCF).
        ///   </internal>
        public Dictionary<string, string> GetResourceSet(string resourceSetName, CultureInfo cultureInfo,
                                                           ResourceTransformation resourceTransformation =
                                                               ResourceTransformation.None)
        {
            if (cultureInfo == null)
            {
                cultureInfo = CultureInfo.InvariantCulture;
            }

            Dictionary<string, string> result = _resourceService.GetResourceSet(resourceSetName, cultureInfo,
                                                                            resourceTransformation);
            return result;
        }




        /// <summary>
        /// Returns a dictionary of sets of resources
        /// where the dictionary key is the resource Filter
        /// (often a View name).
        /// </summary>
        /// <param name="cultureInfo">Culture Info</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <param name="resourceSetNames">List of filters to retrieve resources for</param>
        /// <returns>
        /// Returns a Dictionary containing all resource strings.
        /// </returns>
        /// <internal>
        /// Notice how this method is only intended to be called from it's own
        /// tier, so - unlike IResourceServiceFacade - it accepts
        /// a CultureInfo (which is not serializable across WCF).
        ///   </internal>
        public Dictionary<string, Dictionary<string, string>> GetResourceSets(CultureInfo cultureInfo,
                                                                              ResourceTransformation
                                                                                  resourceTransformation =
                                                                                  ResourceTransformation.None,
                                                                              params string[] resourceSetNames)
        {
            if (cultureInfo == null)
            {
                cultureInfo = CultureInfo.InvariantCulture;
            }

            Dictionary<string, Dictionary<string, string>> result = _resourceService.GetResourceSets(cultureInfo,
                                                                                                     resourceTransformation,
                                                                                                     resourceSetNames);

            return result;
        }
    }
}