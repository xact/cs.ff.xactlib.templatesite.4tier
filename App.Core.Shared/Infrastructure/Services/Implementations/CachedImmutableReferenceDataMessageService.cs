﻿using XAct;

namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services.Configuration;
    using XAct.Services;

    /// <summary>
    /// IMPORTANT: Overridden in front tier, in order to use WCF.
    /// </summary>
    public class CachedImmutableReferenceDataMessageService : ICachedImmutableReferenceDataMessageService
    {
        private readonly ICachingService _cachingService;
        private readonly ICachedImmutableReferenceDataServiceConfiguration _cachedReferenceDataServiceConfiguration;
        private readonly ICachedImmutableReferenceDataService _cachedImmutableReferenceService;
        private readonly IObjectMappingService _objectMappingService;



        TimeSpan ReferenceCacheTimeSpan
        {
            get
            {
                //return _applicationSettingsService.Settings.ReferenceCacheTimeSpan;
                return TimeSpan.FromMinutes(20);
            }
        }

        public CachedImmutableReferenceDataMessageService(
            ICachingService cachingService,
            ICachedImmutableReferenceDataServiceConfiguration cachedReferenceDataServiceConfiguration,
            ICachedImmutableReferenceDataService cachedImmutableReferenceService,
            IObjectMappingService objectMappingService)
        {
            _cachingService = cachingService;
            _cachedReferenceDataServiceConfiguration = cachedReferenceDataServiceConfiguration;
            _cachedImmutableReferenceService = cachedImmutableReferenceService;
            _objectMappingService = objectMappingService;
        }


        public Dictionary<string, ReferenceDataMessage[]> GetAllReferenceData()
        {


            Dictionary<string, ReferenceDataMessage[]> results = new Dictionary<string, ReferenceDataMessage[]>();
            foreach (string key in _cachedReferenceDataServiceConfiguration.GetKeys())
            {
                results[key] = GetReferenceData(key);
            }
            return results;

#if OLD_WAY_USING_DIRECT_INVOCATION
            string[] enumsThatHaveNoBackingDbRecordsSoSkip = new[]
                {
                    "FieldUpdated",
                    "FieldUpdateType"
                };

            //loop through all constants (ie all names of Ref Data such as Gender, FileType, etc.)
            foreach (FieldInfo fieldInfo in typeof (Constants.ReferenceData.ImmutableReferenceData).GetFields())
            {
                //And if 
                if (fieldInfo.FieldType != typeof (String))
                {
                    continue;
                }

                //Get name of Ref Data Type 
                string fieldName = fieldInfo.GetValue(null) as string;

                //There are a few of these Constants/Enums that are not backed by Database referential
                //records, so skip these:
                if (enumsThatHaveNoBackingDbRecordsSoSkip.Contains(fieldName, s => s == fieldName))
                {
                    continue;
                }

                //Use other method to get cached copy:
                results[fieldInfo.Name] = GetReferenceData(fieldInfo.Name);

            }
            return results;
#endif
        }


        public ReferenceDataMessage[] GetReferenceData(string referenceType, bool byPassCache = false, bool resetCache = false)
        {
            //Get the type of the Domain reference Data:
            Type referenceDataType;
            _cachedReferenceDataServiceConfiguration.TryGet(referenceType, out referenceDataType);

            //Invoke the Generic endpoint:
            ReferenceDataMessage[] results = Get2(referenceType, referenceDataType);

            return results;

        }

        private ReferenceDataMessage[] Get2(string typeShortName, Type appReferenceDataType)
        {
            //We are expecting an array of our 'all-purpose' non-culture specific Reference data object:
            ReferenceDataMessage[] results;


            //Create a cache key for the Reference data:
            //Note that we are using the 'WellKnown' name, not the 
            //Enum Type name, nor the Database Record Type name:
            string cacheKey = App.Core.Constants.Caching.CacheKeys.ReferenceDataPrefix + typeShortName;
            // typeof (TRecord).Name

            MethodInfo method = this.GetType()
                                    .GetMethod("ReferenceDatas2", BindingFlags.Instance | BindingFlags.NonPublic);
            MethodInfo generic = method.MakeGenericMethod(appReferenceDataType);

            object tmp = generic.Invoke(this, null);


            _cachingService.TryGet(
                //The key used is important, 
                //as if the data is updatable (ie, a user
                //can flip an item's Enabled state) 
                //the cache has to be cleared.
                //To clear the cache for that type (not the whole Cache
                //which would be very wasteful).
                cacheKey,
                out results,
                () => (ReferenceDataMessage[])generic.Invoke(this, null),
                //Cache the results over time.
                this.ReferenceCacheTimeSpan
                );
            return results;
        }

        private ReferenceDataMessage[] ReferenceDatas2<TRecord>()
            where TRecord : class, IHasReferenceData, new()
        {
            //Invoke the method to get the records from the CultureInsensitive cache
            TRecord[] rawDbRecords = _cachedImmutableReferenceService.GetReferenceData<TRecord>();

            //Iterate through items, making a culture specific array of objects,
            //and cache that:
            List<ReferenceDataMessage> genericReferenceDataItems = new List<ReferenceDataMessage>();

            //Iterate through the Db records received (eg: ReferenceData_Gender)
            //and map them to newly created ReferenceData objects.
            foreach (TRecord rawRecord in rawDbRecords)
            {
                //TO map them, have to craeate destination
                //first:
                ReferenceDataMessage cultureSpecificRecord =
                    (ReferenceDataMessage)
                    System.Activator.CreateInstance(typeof(ReferenceDataMessage));

                //Keep the generics on to Tell AutoMapper what we *Specifically* want
                //or it will try to match from proxies...and fail.
                //Note that it is using a map defined in Back.Infrastructure.Map
                _objectMappingService.Map<IHasReferenceData, ReferenceDataMessage>(rawRecord,
                                                                                                   cultureSpecificRecord);


                //Not used at present, but could...
                //cultureSpecificRecord.Filter = rawRecord.Filter;

                genericReferenceDataItems.Add(cultureSpecificRecord);
            }

            //Done. Get out as an array:
            return genericReferenceDataItems.ToArray();
        }




    }
}