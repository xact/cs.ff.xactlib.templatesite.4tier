namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using XAct.Services;
    using VENDOR = XAct.Caching;


    public class CachingService : ICachingService
    {
        private readonly VENDOR.ICachingService _cachingService;

        /// <summary>
        /// 
        /// </summary>
        /// <internal>
        /// NOTE:CachingService cannot in
        /// </internal>
        /// <param name="cachingService"></param>
        public CachingService(VENDOR.ICachingService cachingService)
			: base()
		{
            _cachingService = cachingService;

            //FAQ: Why the time consuming wrapping?
            //To protect the rest of the app from a vendor.
            //Even if the vendor is yourself, or you know 
            //the vendor personally, it's *not the app*.


            //FAQ: But isn't that slower than just using the vendor's service
            //all over the app?
            //It's certainly faster than having to write the whole service
            //yourself -- without giving up on integrity of protecting
            //your app from vendor rot, making it less upgradeable over time.


        }

        /// <summary>
        /// Adds the specified key.
        /// 
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam><param name="key">The key.</param><param name="objectToCache">The object to cache.</param><param name="isValid">The is valid.</param>
        public void Set<TData>(string key, TData objectToCache, Func<bool> isValid)
        {
            _cachingService.Set(key, objectToCache, isValid);
        }

        /// <summary>
        /// Caches the given value for the specified time.
        /// 
        /// </summary>
        /// <param name="key">The key.</param><param name="expiryTimespan">The timespan to cache data for.</param><param name="objectToCache">The object to cache.</param><typeparam name="TData">The type of the data.</typeparam>
        public void Set<TData>(string key, TData objectToCache, TimeSpan expiryTimespan)
        {
            _cachingService.Set(key, objectToCache, expiryTimespan);
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="dataFactory">The data factory.Can be as simple as ()=&gt;(dataToCache)</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <param name="updateNow"></param>
        public void Set<TData>(string key, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true,bool updateNow=true)
        {
            _cachingService.Set(key, dataFactory, expiryTimespan, selfUpdating);
        }

        /// <summary>
        /// Gets the <see cref="T:System.Object"/> with the specified key.
        /// 
        /// </summary>
        /// 
        /// <value/>
        /// <typeparam name="TData">The type of the data.</typeparam>
        public bool TryGet<TData>(string key, out TData result)
        {
            return _cachingService.TryGet(key, out result);
        }

        /// <summary>
        /// Gets the <see cref="T:System.Object"/> with the specified key.
        ///             If not found, registers the datafactory, and returns its results.
        /// 
        /// <para>
        /// Note: basically combines TryGet and Add and into one operation.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam><param name="key">The key.</param><param name="result">The result.</param><param name="dataFactory">The data factory.</param><param name="expiryTimespan">The expiry timespan.</param><param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <returns/>
        public bool TryGet<TData>(string key, out TData result, Func<TData> dataFactory, TimeSpan expiryTimespan,
                                  bool selfUpdating = true)
        {
            bool found = 
                _cachingService.TryGet<TData>(key, out result, dataFactory, expiryTimespan, selfUpdating);
            return found;
        }

        /// <summary>
        /// Removes the cached item that has the given key name.
        /// 
        /// </summary>
        /// <param name="key">The key.</param>
        public void Remove(string key)
        {
            _cachingService.Remove(key);
        }

        /// <summary>
        /// Clears out all cached items.
        /// 
        /// </summary>
        public void Clear()
        {
            _cachingService.Clear();
        }

    }


}