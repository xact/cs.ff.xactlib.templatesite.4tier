namespace App.Core.Infrastructure.Services.Implementations
{
    using XAct;
    using XAct.Services;
    using VENDOR = XAct.Environment;

    [DefaultBindingImplementation(typeof(IAppEnvironmentService),BindingLifetimeType.Undefined,Priority.VeryLow)]
    public class AppEnvironmentService : IAppEnvironmentService
    {


        private readonly VENDOR.IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppEnvironmentService"/> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        public AppEnvironmentService(VENDOR.IEnvironmentService environmentService)
        {
            _environmentService = environmentService;
        }

        
        /// <summary>
        /// Gets a value indicating whether the currrent process is interactive.
        /// 
        /// <para>
        /// A Windows Service (unless it has 'interact with desktop' set to true)
        ///             would return false. So would IIS. In such cases, don't show Modal dialogs
        ///             ...as nobody will be able to reacto them.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// 
        /// <value>
        /// <c>true</c> if this instance is interactive; otherwise, <c>false</c>.
        /// 
        /// </value>
        public bool IsUserInteractive { get { return _environmentService.IsUserInteractive; } }


        //public IAppIdentity GetIdn


        /// <summary>
        /// Gets the name of the current user's domain (eg: JQDEV).
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The name of the domain.
        /// </value>
        public string DomainName { get { return _environmentService.DomainName; } }

        /// <summary>
        /// Gets the name of the application.
        /// 
        /// <para>
        /// Gets or Sets the name of the application
        ///               using the custom membership provider.
        ///               Default value is '/'
        /// 
        /// </para>
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The name of the application.
        /// </value>
        public string ApplicationName { get { return _environmentService.ApplicationName; } }

        /// <summary>
        /// Gets the hash of the application name.
        /// 
        /// </summary>
        public string ApplicationNameHash { get { return _environmentService.ApplicationNameHash; } }

        /// <summary>
        /// Gets the application's base path.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The application's base path.
        /// </value>
        public string ApplicationBasePath { get { return _environmentService.ApplicationBasePath; } }


        public string MachineName { get { return _environmentService.MachineName; } }

        /// <summary>
        /// The HttpContext.
        /// 
        /// <para>
        /// Will be null in a non-IIS based environment (ie desktop)
        /// 
        /// </para>
        /// 
        /// <para>
        /// Note that it is returned as an Object (and not HttpContext)
        ///             to remain compilable in Windows Client applications when
        ///             System.Web is not available.
        /// 
        /// </para>
        /// 
        /// <para>
        /// Note that internally, Reflection is being used.
        /// 
        /// </para>
        /// 
        /// </summary>
        public object HttpContext { get { return _environmentService.HttpContext; } }

        /// <summary>
        /// Returns a context relative new line
        ///             ("<br/>" when <see cref="P:XAct.Environment.IEnvironmentService.HttpContext"/> is not null,
        ///             <see cref="P:System.Environment.NewLine"/> when not.
        /// 
        /// </summary>
        public string NewLine { get { return _environmentService.NewLine; } }

        
        /// <summary>
        /// Maps the VirtualPath ('~/SubDir/SomeFile.htm')
        ///             to a Dir relative to <see cref="P:XAct.Environment.IEnvironmentService.ApplicationBasePath"/>
        ///             eg: ('c:\somedir\SubDir\SomeFile.htm');
        /// 
        /// </summary>
        /// <param name="virtualPath"/>
        /// <returns/>
        public string MapPath(string virtualPath)
        {
            return _environmentService.MapPath(virtualPath);
        }

        
        /// <summary>
        /// Gets the total amount of memory *thought* to be allocated.
        /// 
        /// <para>
        /// With Garbage Collection and other mitigating factors, it's
        ///             not exactly a hard fact at any one time.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="waitForGarbageCollectionBeforeReturning">Wait for garbage collection before returning, or return immediately.</param>
        /// <returns>
        /// The amount of memory allocated
        /// </returns>
        public long GetTotalMemoryAllocated(bool waitForGarbageCollectionBeforeReturning = false)
        {
            return _environmentService.GetTotalMemoryAllocated(waitForGarbageCollectionBeforeReturning);
        }


        
    }
}