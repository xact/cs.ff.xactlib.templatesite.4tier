namespace XAct.Validation
{
    using XAct.Messages;
    using XAct.Services;


    public class MessageCodeResolvingService : IMessageCodeResolvingService
    {
        private readonly IMessageCodeMetadataService _messageCodeMetadataService;

        private readonly MessageCode _defaultMessageCode = default(MessageCode);


        /// <summary>
        /// Initializes a new instance of the <see cref="MessageCodeResolvingService" /> class.
        /// </summary>
        /// <param name="messageCodeMetadataService">The message code metadata service.</param>
        public MessageCodeResolvingService(IMessageCodeMetadataService messageCodeMetadataService)
        {
            _messageCodeMetadataService = messageCodeMetadataService;
        }


        public MessageCode Resolve(string messageCodeIdentifier)
        {
            MessageCode messageCode;
            MessageCodeMetadataAttribute messageCodeMetadataAttribute;
            if (!_messageCodeMetadataService.TryGet(messageCodeIdentifier, out messageCode, out messageCodeMetadataAttribute))
            {
                messageCode = _defaultMessageCode;
            }

            return messageCode;

        }
    }
}