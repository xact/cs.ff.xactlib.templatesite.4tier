﻿namespace App.Core.Services.Implementations
{
    using System;
    using System.Collections;
    using XAct.Services;
    using XAct;
    using XAct.State;

    public class ContextStateService : App.Core.Services.IContextStateService
    {
        private readonly XAct.State.IContextStateService _contextStateService;

        public IDictionary Items { get { return _contextStateService.Items; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextStateService"/> class.
        /// </summary>
        /// <param name="contextStateService">The context state service.</param>
        public ContextStateService(XAct.State.IContextStateService contextStateService)
        {
            _contextStateService = contextStateService;
        }



        public void IncrementContextOperationCounter(int offset = 1, string tag = "Default")
        {
            if (tag.IsNullOrEmpty())
            {
                tag = "Default";
            }
            string key = string.Format("XAct.Lib.UnitOfWork.{0}.OperationCount", tag);

            int value = (int)(this.Items[key] ?? 0) + offset;

            this.Items[key] = value;
        }

        public int GetContextOperationCounter(string tag = "Default")
        {
            if (tag.IsNullOrEmpty())
            {
                tag = "Default";
            }
            string key = string.Format("XAct.Lib.UnitOfWork.{0}.OperationCount", tag);

            key = string.Format(key, tag);

            int value = (int)(this.Items[key] ?? 0);
            return value;
        }


        public void IncrementContextOperationDuration(TimeSpan timeSpan = default(TimeSpan), string tag = "Default")
        {
            if (tag.IsNullOrEmpty())
            {
                tag = "Default";
            }
            string key = string.Format("XAct.Lib.UnitOfWork.{0}.OperationDuration", tag);
            TimeSpan newValue = ((TimeSpan)(this.Items[key] ?? TimeSpan.Zero)).Add(timeSpan);
            this.Items[key] = newValue;

        }

        public TimeSpan GetContextOperationDuration(string tag = "Default")
        {
            if (tag.IsNullOrEmpty())
            {
                tag = "Default";
            }
            string key = string.Format("XAct.Lib.UnitOfWork.{0}.OperationDuration", tag);
            TimeSpan value = (TimeSpan)(this.Items[key] ?? TimeSpan.Zero);
            return value;

        }

    }
}
