﻿namespace App.Core.Infrastructure.Services
{
    using XAct.Services;

    /// <summary>
    /// Implementation of the 
    /// <see cref="DeviceInformationService"/>
    /// </summary>
    public class DeviceInformationService : IDeviceInformationService
    {
        private readonly XAct.Environment.IDeviceInformationService _deviceInformationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceInformationService"/> class.
        /// </summary>
        /// <param name="deviceInformationService">The device information service.</param>
        public DeviceInformationService(XAct.Environment.IDeviceInformationService deviceInformationService)
        {
            _deviceInformationService = deviceInformationService;
        }

        /// <summary>
        /// Gets the unique device ID.
        /// <para>
        /// This is a Unique ID of the phone,
        /// hashed with the supplied key.
        /// </para>
        /// <para>
        /// On a server, it's a hash of the domain/machine name.
        /// </para>
        /// </summary>
        /// <param name="salt"></param>
        /// <returns>
        /// A 40 char (20bytes x 2chars) string.
        /// </returns>
        public string GetUniqueDeviceID(string salt = null)
        {
            return _deviceInformationService.GetUniqueDeviceID();
        }
    }
}