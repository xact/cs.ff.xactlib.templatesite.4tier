﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Security.Principal;
    using App.Core.Domain.Security;
    using XAct;
    using XAct.Environment;
    using XAct.Services;

    public class PrincipalService : PrincipalServiceBase, Services.IPrincipalService
    {
        private IAppIdentity _currentAppIdentity;

        public PrincipalService(IPrincipalService principalService) : base(principalService)
        {
        }

        public IAppIdentity CurrentAppIdentity
        {
            get
            {
                return _currentAppIdentity;
            }
            private set
            {
                _currentAppIdentity = value;
            }
        }

        public IAppIdentity GetCurrentAppIdentity(bool throwAuthExceptionIfNotFound = false)
        {
            throw new System.NotImplementedException();
        }
    }

    public class PrincipalServiceBase: IPrincipalServiceBase
    {
        private readonly XAct.Environment.IPrincipalService _principalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrincipalService"/> class.
        /// </summary>
        /// <param name="principalService">The principal service.</param>
        public PrincipalServiceBase(XAct.Environment.IPrincipalService principalService)
        {
            _principalService = principalService;			
        }

	    public IPrincipal Principal
	    {
		    get { return _principalService.Principal; }
		    set { _principalService.Principal = value; }
	    }

	    public string CurrentIdentityIdentifier
        {
            get { return _principalService.CurrentIdentityIdentifier; }
        }

		public bool HasPrivilege(string privilegeName)
	    {
		    return _principalService.Principal.IsInRole(privilegeName);
	    }


    }
}