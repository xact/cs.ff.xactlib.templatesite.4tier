﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using XAct.Services;
    using VENDOR = XAct.Exceptions;

    public class ExceptionHandlingManagementService : IExceptionHandlingManagementService
    {
        private readonly VENDOR.IExceptionHandlingManagementService _exceptionHandlingManagementService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlingManagementService" /> class.
        /// </summary>
        /// <param name="commonServices">The common services.</param>
        /// <param name="exceptionHandlingManagementService">The exception handling management service.</param>
        public ExceptionHandlingManagementService(VENDOR.IExceptionHandlingManagementService exceptionHandlingManagementService)
        {
            _exceptionHandlingManagementService = exceptionHandlingManagementService;
        }

        /// <summary>
        /// Registers the specified exception handling description.
        /// 
        /// </summary>
        /// <param name="exceptionHandlingDescription">The exception handling description.</param><param name="category">The category.</param>
        /// <returns/>
        public void Register(VENDOR.IExceptionHandlingConfiguration exceptionHandlingDescription, string category = "default")
        {
            _exceptionHandlingManagementService.Register(exceptionHandlingDescription,category);
        }

        /// <summary>
        /// Retrieves the specified exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public VENDOR.IExceptionHandlingConfiguration Retrieve(Exception exception, string category = "default")
        {
            return _exceptionHandlingManagementService.Retrieve(exception, category);
        }

    }
}
