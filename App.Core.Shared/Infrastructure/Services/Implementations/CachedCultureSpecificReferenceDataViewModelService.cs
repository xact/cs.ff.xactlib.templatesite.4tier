﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    /// <summary>
    /// Contract to cache projections of Reference Data.
    /// <para>
    /// The difference between <see cref="ReferenceDataViewModel"/>
    /// and <see cref="ReferenceDataMessage"/> is that <see cref="ReferenceDataViewModel"/>
    /// is already localized, ready to be used at the perimeter of the application.
    /// </para>
    /// <para>
    /// Only use this service at the outer boundary of the application.
    /// In the front tier, this is the UI, or API, and in the back tier, 
    /// this is Emails, and serialization to external services (IQOffice, etc.)
    /// </para>
    /// </summary>
    public class CachedCultureSpecificReferenceDataViewModelService : ICachedCultureSpecificReferenceDataViewModelService
    {
        private readonly ICachedImmutableReferenceDataMessageService _cachedReferenceDataService;
        private readonly ICultureSpecificResourceService _cultureSpecificResourceService;

        public CachedCultureSpecificReferenceDataViewModelService(ICachedImmutableReferenceDataMessageService cachedReferenceDataService, 
            ICultureSpecificResourceService cultureSpecificResourceService
            )
        {
            _cachedReferenceDataService = cachedReferenceDataService;
            _cultureSpecificResourceService = cultureSpecificResourceService;
        }


        public ReferenceDataViewModel[] GetReferenceDataViewModel(string typeName, CultureInfo cultureInfo)
        {

            //Get from cache -- or call back tier -- the 
            //generic ReferenceData set.
            ReferenceDataMessage[] results =
                _cachedReferenceDataService.GetReferenceData(typeName);

            //But the entries won't be translated...
            //So have to get strings that match:

            Dictionary<string, string> cultureSpecificStrings;

            try
            {
                cultureSpecificStrings = _cultureSpecificResourceService.GetResourceSet(typeName,cultureInfo);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Unable to load reference data for resource: {0}", typeName));
            }

            List<ReferenceDataViewModel> output = new List<ReferenceDataViewModel>();


            //FUTURE:
            //Neat thig is that one can loop through the resources, in order, 
            //and then (in future) add in custom user MRU order.


            //Have to merge the two parts (shared attributes + culture specific resources):
            //Note that we doing this by the Server Order and only returning Enabled:
            foreach (ReferenceDataMessage referenceData in results.Where(d => d.Enabled).OrderBy(d => d.Order))
            {
                ReferenceDataViewModel referenceDataViewModel = new ReferenceDataViewModel();

                referenceDataViewModel.Key = referenceData.Id.ToString();
                string refDataValue;
                referenceDataViewModel.Text = cultureSpecificStrings.TryGetValue(referenceData.Text, out refDataValue)
                    ? refDataValue
                    : "(Unknown)";

                output.Add(referenceDataViewModel);
            }

            return output.ToArray();
        }

    }
}