﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System;
    using App.Core.Infrastructure.Services.Configuration;
    using AutoMapper;
    using XAct.Services;
    using VENDOR = XAct.ObjectMapping;

    /// <summary>
	/// The contract for a Service to map
	///             Entities to DTOs and back again.
	/// </summary>
	/// <remark>
	/// <para>
	/// Note that the management of the mappers
	///             (the registering of Mappers) is handled
	///             by a different interface (IMappingManagementService)
	///             usually accessed only by the application's
	///             bootstrapper/initialization.
	///             (SOC).
	/// 
	/// </para>
	/// </remark>
    public class ObjectMappingService : IObjectMappingService
	{
        private readonly XAct.ObjectMapping.IObjectMappingService _mappingService;
        private readonly IObjectMappingServiceConfiguration _objectMapperConfiguration;
        //private readonly IObjectMappingServiceConfiguration _objectMapperConfiguration;
        private readonly IObjectMappingRegistrationService _objectMappingRegistrationService;

        /// <summary>
        /// Test whether 
        /// </summary>
        public bool Test()
        {
            _objectMappingRegistrationService.Test();
            return true;
        }

        /// <summary>
        /// Test whether Mapping Exists between source and target.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TTarget"></typeparam>
        /// <returns></returns>
        public bool TestMapExists<TSource, TTarget>()
        {
            return (AutoMapper.Mapper.FindTypeMapFor<TSource, TTarget>() != null);
        }

        public bool TestMapIsValid<TSource, TTarget>(out Exception exception)
        {
            exception = null;
            TypeMap mapper = AutoMapper.Mapper.FindTypeMapFor<TSource, TTarget>();
            if (mapper != null)
            {
                try
                {
                    AutoMapper.Mapper.AssertConfigurationIsValid(mapper);
                    return true;
                }
                catch (Exception e)
                {
                    exception = e;
                }
            }
            return false;
        }

        /// <summary>
        /// Means of checking as to which maps were registered.
        /// </summary>
        public object Maps { get { return _objectMappingRegistrationService.Maps; }}

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectMappingService" /> class.
        /// </summary>
        /// <param name="mappingService">The mapping service.</param>
        /// <param name="objectMapperConfiguration">The object mapper configuration.</param>
        /// <param name="objectMappingRegistrationService">The object mapping registration service.</param>
		public ObjectMappingService( VENDOR.IObjectMappingService mappingService, 
            IObjectMappingServiceConfiguration objectMapperConfiguration, 
            IObjectMappingRegistrationService objectMappingRegistrationService)
		{
            _mappingService = mappingService;

            _objectMapperConfiguration = objectMapperConfiguration;
            _objectMapperConfiguration.Initialize();


            //_objectMapperConfiguration = objectMapperConfiguration;
            _objectMappingRegistrationService = objectMappingRegistrationService;
		}



        /// <summary>
        /// Maps the given Source object to the instantiated target object.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns/>
        public TTarget Map<TSource, TTarget>(TSource source)
            where TSource : class
            where TTarget : class, new()
        {
            //Ensure maps created before using them:
            //_objectMapperConfiguration.Initialize();
            try
            {
                return _mappingService.Map<TSource, TTarget>(source);
            }
            catch
            {
                VENDOR.ITypeMapper[] maps = _objectMappingRegistrationService.Maps;
                throw;
            }
        }


	    /// <summary>
	    /// Maps the given Source object to the instantiated target object.
	    /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns/>
	    public TTarget Map<TSource, TTarget>(TSource source, TTarget target = null)
	        where TSource : class
	        where TTarget : class
	    {
            //Ensure maps created before using them:
	        //_objectMapperConfiguration.Initialize();

// ReSharper disable RedundantTypeArgumentsOfMethod
	        try
	        {
	            return _mappingService.Map<TSource, TTarget>(source, target);
	        }
	        catch
	        {

                VENDOR.ITypeMapper[] maps = _objectMappingRegistrationService.Maps;
                throw;
	        }
// ReSharper restore RedundantTypeArgumentsOfMethod
	    }
	}
}