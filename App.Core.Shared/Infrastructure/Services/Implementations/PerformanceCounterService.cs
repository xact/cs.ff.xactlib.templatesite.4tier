﻿using System.Collections.Generic;
using System.Diagnostics;

namespace App.Core.Infrastructure.Services.Implementations
{
    using XAct.Services;
    using VENDOR = XAct.Diagnostics.Performance;


    /// <summary>
    /// An implementation of the 
    /// <see cref="IPerformanceCounterService"/>
    /// to provide a service to work with performance counter sets (pairs).
    /// </summary>
    public class PerformanceCounterService : IPerformanceCounterService
    {
	    public IPerformanceCounterServiceConfiguration PerformanceCounterService2Configuration { get; private set; }
	    //private readonly VENDOR.IPerformanceCounterService _performanceCounterSetService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceCounterService"/> class.
        /// </summary>
        /// <param name="performanceCounterSetService">The performance counter set service.</param>
		public PerformanceCounterService(IPerformanceCounterServiceConfiguration performanceCounterService2Configuration) //VENDOR.IPerformanceCounterService performanceCounterSetService
        {
	        PerformanceCounterService2Configuration = performanceCounterService2Configuration;
	        //_performanceCounterSetService = performanceCounterSetService;
        }


	    /// <summary>
        /// Updates the value of the counter.
        /// 
        /// <para>
        /// Used to update simple <see cref="T:XAct.Diagnostics.Performance.IPerformanceCounterSet"/>s that have only one counter
        ///             within the set.
        /// 
        /// </para>
        /// 
        /// <para>
        /// If no value is provided, this is the same as simply calling <c>Increment()</c>
        ///             on the counter.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="performanceCounterSetName">Name of the performance counter set.</param><param name="amount">The amount to increment, or decrement the counter by.</param>
        public void Update(string performanceCounterSetName, int? amount = null)
	    {

		    PerformanceCounter performanceCounter;
			if (
				!PerformanceCounterService2Configuration.PerformanceCounters.TryGetValue(performanceCounterSetName,
				                                                                         out performanceCounter))
			{


				return;
			}
			

		    if (!amount.HasValue)
		    {
			    performanceCounter.Increment();
		    }
		    else
		    {
			    performanceCounter.IncrementBy(amount.Value);
		    }
		    //_performanceCounterSetService.Update(performanceCounterSetName,amount);
        }

		///// <summary>
		///// Updates the value of the bottom (denominator) counter (ie, the 'b' in an 'a/b' configuration).
		/////             of <see cref="T:XAct.Diagnostics.Performance.IPerformanceCounterSet"/>.
		///// 
		///// <para/>
		///// 
		///// <para>
		///// If no value is provided, this is the same as simply calling <c>Increment()</c>
		/////             on the counter.
		///// 
		///// </para>
		///// 
		///// </summary>
		///// <param name="performanceCounterSetName">Name of the performance counter set.</param><param name="amount">The amount to increment, or decrement the counter by.</param>
		//public void UpdateDenominator(string performanceCounterSetName, int? amount = null)
		//{
		//	_performanceCounterSetService.UpdateDenominator(performanceCounterSetName, amount);
		//}

    }
}