﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using XAct.Services;
    using VENDOR = XAct.IO;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// <para>Contract is DefaultBindingImplementation for automatic wiring up in IoC</para>
    /// </internal>
    public class TemplateRenderingService : ITemplateRenderingService
    {
        private readonly VENDOR.ITemplateRenderingService _templateRenderingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateRenderingService" /> class.
        /// </summary>
        /// <param name="templateRenderingService">The template rendering service.</param>
        public TemplateRenderingService (VENDOR.ITemplateRenderingService templateRenderingService)
		{
            _templateRenderingService = templateRenderingService;
        }


        //ITemplateRenderingServiceSettings Settings { get; }

        public string CompileTemplateString(string templateText, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            return _templateRenderingService.CompileTemplateString(templateText, optionalReferencedAssemblies, optionalContext, avoidContextPrefixIfPossible);
        }

        public long CompileTemplateString(Stream outputStream, string templateText, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            return _templateRenderingService.CompileTemplateString(outputStream, templateText, optionalReferencedAssemblies, optionalContext, avoidContextPrefixIfPossible);

        }

        public string CompileTemplateFile(string templateFileName, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            return _templateRenderingService.CompileTemplateFile(templateFileName, optionalReferencedAssemblies, optionalContext, avoidContextPrefixIfPossible);
        }

        public long CompileTemplateFile(Stream outputStream, string templateFileName, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            return _templateRenderingService.CompileTemplateFile(outputStream, templateFileName, optionalReferencedAssemblies, optionalContext, avoidContextPrefixIfPossible);
        }

        public string CompileTemplateStream(TextReader textReader, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            return _templateRenderingService.CompileTemplateStream(textReader, optionalReferencedAssemblies, optionalContext, avoidContextPrefixIfPossible);
        }

        public long CompileTemplateStream(Stream outputStream, TextReader textReader, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            return _templateRenderingService.CompileTemplateStream(outputStream, textReader, optionalReferencedAssemblies, optionalContext, avoidContextPrefixIfPossible);
        }

    }
}
