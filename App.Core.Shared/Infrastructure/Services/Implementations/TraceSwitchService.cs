﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Diagnostics;
    using System.Reflection;
    using XAct.Services;
    using VENDOR=XAct.Diagnostics;

    public class TraceSwitchService : ITraceSwitchService
    {
        private readonly VENDOR.ITraceSwitchService _traceSwitchService;

        public TraceSwitchService(VENDOR.ITraceSwitchService traceSwitchService)
        {
            _traceSwitchService = traceSwitchService;
        }

        public VENDOR.ITraceSwitchServiceConfiguration Configuration
        {
            get
            {
                return _traceSwitchService.Configuration;
            }
        }

        public bool Enabled
        {
            get
            {
                return _traceSwitchService.Enabled;
            }
            set
            {
                _traceSwitchService.Enabled = value;
            }
        }

        public bool ReflectionEnabled
        {
            get
            {
                return _traceSwitchService.ReflectionEnabled;
            }
            set
            {
                _traceSwitchService.ReflectionEnabled = value;
            }
        }

        public TraceSwitch this[string traceSwitchName]
        {
            get
            {
                return _traceSwitchService[traceSwitchName];
            }
        }

        public TraceSwitch GetNamedTraceSwitch(string traceSwitchName, XAct.Diagnostics.TraceLevel defaultTraceLevel)
        {

            return _traceSwitchService.GetNamedTraceSwitch(traceSwitchName, defaultTraceLevel);
        }

        public TraceSwitch GetApplicationTraceSwitch()
        {
            return _traceSwitchService.GetApplicationTraceSwitch();
        }

        public bool ShouldTrace(XAct.Diagnostics.TraceLevel traceLevel, System.Type classInstanceType)
        {
            return _traceSwitchService.ShouldTrace(traceLevel, classInstanceType);
        }

        public TraceSwitch GetAssemblyTraceSwitch(Assembly assembly)
        {
            return _traceSwitchService.GetAssemblyTraceSwitch(assembly);
        }

        public TraceSwitch GetTraceSwitch(int stackTraceFrameOffset)
        {
            return _traceSwitchService.GetTraceSwitch(stackTraceFrameOffset);
        }

        public TraceSwitch GetTraceSwitchByType(System.Type instanceType)
        {
            return _traceSwitchService.GetTraceSwitchByType(instanceType);
        }
    }
}