﻿namespace App.Core.Infrastructure.Services
{
    using System;

    /// <summary>
    /// The interface for an application specific service that caches data.
    /// </summary>
    public interface ICachingService : IHasAppCoreService
    {
        /// <summary>
        /// Adds the specified key.
        /// 
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam><param name="key">The key.</param><param name="objectToCache">The object to cache.</param><param name="isValid">The is valid.</param>
        void Set<TData>(string key, TData objectToCache, Func<bool> isValid);

        /// <summary>
        /// Caches the given value for the specified time.
        /// 
        /// </summary>
        /// <param name="key">The key.</param><param name="expiryTimespan">The timespan to cache data for.</param><param name="objectToCache">The object to cache.</param><typeparam name="TData">The type of the data.</typeparam>
        void Set<TData>(string key, TData objectToCache, TimeSpan expiryTimespan);

        /// <summary>
        /// Adds the specified key.
        /// 
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam><param name="key">The key.</param><param name="dataFactory">The data factory.Can be as simple as ()=&gt;(dataToCache)</param><param name="expiryTimespan">The expiry timespan.</param><param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        void Set<TData>(string key, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true, bool updateNow=false);

        /// <summary>
        /// Gets the <see cref="T:System.Object"/> with the specified key.
        /// 
        /// </summary>
        /// 
        /// <value/>
        /// <typeparam name="TData">The type of the data.</typeparam>
        bool TryGet<TData>(string key, out TData result);

        /// <summary>
        /// Gets the <see cref="T:System.Object"/> with the specified key.
        ///             If not found, registers the datafactory, and returns its results.
        /// 
        /// <para>
        /// Note: basically combines TryGet and Add and into one operation.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam><param name="key">The key.</param><param name="result">The result.</param><param name="dataFactory">The data factory.</param><param name="expiryTimespan">The expiry timespan.</param><param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <returns/>
        bool TryGet<TData>(string key, out TData result, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true);

        /// <summary>
        /// Removes the cached item that has the given key name.
        /// 
        /// </summary>
        /// <param name="key">The key.</param>
        void Remove(string key);

        /// <summary>
        /// Clears out all cached items.
        /// 
        /// </summary>
        void Clear();

    }
}
