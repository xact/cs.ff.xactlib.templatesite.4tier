﻿namespace App.Core.Infrastructure.Services
{
	/// <summary>
	/// Contract to return environment settings.
	/// </summary>
	/// <internal>
	/// <para>
	/// By using a service for indirection, 
	/// security and timing issues are more easily
	/// mockable.
	/// </para>
	/// </internal>
    public interface IAppEnvironmentService : IHasAppCoreService
	{
		/// <summary>
		/// Gets a value indicating whether the currrent process is interactive.
		/// 
		/// <para>
		/// A Windows Service (unless it has 'interact with desktop' set to true)
		///             would return false. So would IIS. In such cases, don't show Modal dialogs
		///             ...as nobody will be able to reacto them.
		/// 
		/// </para>
		/// 
		/// </summary>
		/// 
		/// <value>
		/// <c>true</c> if this instance is interactive; otherwise, <c>false</c>.
		/// 
		/// </value>
		bool IsUserInteractive { get; }


		
		/// <summary>
		/// Gets the name of the current user's domain (eg: JQDEV).
		/// 
		/// </summary>
		/// 
		/// <value>
		/// The name of the domain.
		/// </value>
		string DomainName { get; }

		/// <summary>
		/// Gets the name of the application.
		/// 
		/// <para>
		/// Gets or Sets the name of the application
		///               using the custom membership provider.
		///               Default value is '/'
		/// 
		/// </para>
		/// 
		/// </summary>
		/// 
		/// <value>
		/// The name of the application.
		/// </value>
		string ApplicationName { get; }

		/// <summary>
		/// Gets the hash of the application name.
		/// 
		/// </summary>
		string ApplicationNameHash { get; }

		/// <summary>
		/// Gets the application's base path.
		/// 
		/// </summary>
		/// 
		/// <value>
		/// The application's base path.
		/// </value>
		string ApplicationBasePath { get; }

		
		/// <summary>
		/// The HttpContext.
		/// 
		/// <para>
		/// Will be null in a non-IIS based environment (ie desktop)
		/// 
		/// </para>
		/// 
		/// <para>
		/// Note that it is returned as an Object (and not HttpContext)
		///             to remain compilable in Windows Client applications when
		///             System.Web is not available.
		/// 
		/// </para>
		/// 
		/// <para>
		/// Note that internally, Reflection is being used.
		/// 
		/// </para>
		/// 
		/// </summary>
		object HttpContext { get; }

		/// <summary>
		/// Returns a context relative new line
		///             ("<br/>" when <see cref="P:XAct.Environment.IEnvironmentService.HttpContext"/> is not null,
		///             <see cref="P:System.Environment.NewLine"/> when not.
		/// 
		/// </summary>
		string NewLine { get; }

		
		/// <summary>
		/// Maps the VirtualPath ('~/SubDir/SomeFile.htm')
		///             to a Dir relative to <see cref="P:XAct.Environment.IEnvironmentService.ApplicationBasePath"/>
		///             eg: ('c:\somedir\SubDir\SomeFile.htm');
		/// 
		/// </summary>
		/// <param name="virtualPath"/>
		/// <returns/>
		string MapPath(string virtualPath);

		
		/// <summary>
		/// Gets the total amount of memory *thought* to be allocated.
		/// 
		/// <para>
		/// With Garbage Collection and other mitigating factors, it's
		///             not exactly a hard fact at any one time.
		/// 
		/// </para>
		/// 
		/// </summary>
		/// <param name="waitForGarbageCollectionBeforeReturning">Wait for garbage collection before returning, or return immediately.</param>
		/// <returns>
		/// The amount of memory allocated
		/// </returns>
		long GetTotalMemoryAllocated(bool waitForGarbageCollectionBeforeReturning = false);


	    string MachineName { get; }
	}
}
