namespace App.Core.Infrastructure.Services
{
    using System;
    using XAct.Messages;

    public interface IMessageCodeMetadataService : IHasAppCoreService
    {
        void Scan(params Type[] classesContainingStaticPropertiesOfTypeMessageCode);

        bool TryGet(MessageCode messageCode, out MessageCodeMetadataAttribute messageCodeMetadataAttribute);

        bool TryGet(string messageCodeIdentifier, out MessageCode messageCode, out MessageCodeMetadataAttribute messageCodeMetadataAttribute);

    }
}