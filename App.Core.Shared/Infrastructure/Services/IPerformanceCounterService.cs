﻿namespace App.Core.Infrastructure.Services
{
	/// <summary>
    /// Contract for a service that provides
    /// a means to update Performant Counter Sets (pairs).
    /// </summary>
    public interface IPerformanceCounterService : IHasAppCoreService
    {

        /// <summary>
        /// Updates the value of the counter.
        /// 
        /// <para>
        /// Used to update simple <see cref="T:XAct.Diagnostics.Performance.IPerformanceCounterSet"/>s that have only one counter
        ///             within the set.
        /// 
        /// </para>
        /// 
        /// <para>
        /// If no value is provided, this is the same as simply calling <c>Increment()</c>
        ///             on the counter.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="performanceCounterSetName">Name of the performance counter set.</param><param name="amount">The amount to increment, or decrement the counter by.</param>
        void Update(string performanceCounterSetName, int? amount = null);

		///// <summary>
		///// Updates the value of the bottom (denominator) counter (ie, the 'b' in an 'a/b' configuration).
		/////             of <see cref="T:XAct.Diagnostics.Performance.IPerformanceCounterSet"/>.
		///// 
		///// <para/>
		///// 
		///// <para>
		///// If no value is provided, this is the same as simply calling <c>Increment()</c>
		/////             on the counter.
		///// 
		///// </para>
		///// 
		///// </summary>
		///// <param name="performanceCounterSetName">Name of the performance counter set.</param><param name="amount">The amount to increment, or decrement the counter by.</param>
		//void UpdateDenominator(string performanceCounterSetName, int? amount = null);

    }


    
}
