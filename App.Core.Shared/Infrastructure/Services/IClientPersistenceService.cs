﻿namespace App.Core.Infrastructure.Services
{
    using System;

    public interface IClientPersistenceService : IHasAppCoreService
    {
        string GetValue(string containerName, string key = null);
        void SetValue(string containerName, string value, TimeSpan duration = default(TimeSpan), string key = null);
        void ClearContainer(string containerName);
    }
}
