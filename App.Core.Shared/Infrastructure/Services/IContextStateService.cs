﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Services
{
    using System.Collections;

    public interface IContextStateService : IHasAppCoreService
    {
        IDictionary Items { get; }



        void IncrementContextOperationCounter(int offset = 1, string tag = "Default");

        int GetContextOperationCounter(string tag = "Default");

        void IncrementContextOperationDuration(TimeSpan timeSpan = default(TimeSpan), string tag = "Default");

        TimeSpan GetContextOperationDuration(string tag = "Default");

    }


}
