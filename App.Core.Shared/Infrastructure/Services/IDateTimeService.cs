﻿namespace App.Core.Infrastructure.Services
{
    using System;
    using XAct.Environment;

    public interface IDateTimeService : IHasAppCoreService
    {
    IDateTimeServiceConfiguration Configuration { get; }

    //[Obsolete("Use UTC, and convert to Local at UI level or other required ouput only.", false)]
    //DateTime Now { get; }

    DateTime NowUTC { get; }

    void SetUTCOffset(DateTime nowUTC);

    void SetUTCOffset(TimeSpan timeSpanOffset);

    void ResetUTCOffset();
    }
}
