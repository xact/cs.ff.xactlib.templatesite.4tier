namespace App.Core.Infrastructure.Services
{
    using System;
    using App.Core;
    using XAct.Messages;

    /// <summary>
    /// Contract for a service to invoke remote Back Tier service facades,
    /// and map the response object's payload to a front end presentation ready model (often a ViewModel).
    /// </summary>
    public interface ICultureSpecificValidatedQueryResponseService : IHasAppCoreService
    {

        /// <summary>
        /// Performs a query on a remote back tier Application Facade Service operation,
        /// and maps the returned Response's Data payload to a model type appropriate for the front
        /// tier presentation layer service (often a ViewModel).
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TServiceResponseModel">The type of the service response model.</typeparam>
        /// <typeparam name="TResponseViewModel">The type of the response view model.</typeparam>
        /// <param name="func">The function.</param>
        /// <param name="postProcessTheResponse">An optional post processing of the response, prior to mapping to a viewmodel.</param>
        /// <param name="postProcessViewModel">An optional post processing of the final response.</param>
        /// <returns></returns>
        Response<TResponseViewModel> Query<TService, TServiceResponseModel, TResponseViewModel>(
            Func<TService, Response<TServiceResponseModel>> func, 
            Action<IResponse<TServiceResponseModel>> postProcessTheResponse = null,
            Action<IResponse<TResponseViewModel>> postProcessViewModel = null)
            where TServiceResponseModel : class, new()
            where TResponseViewModel : class, new();


        
        
        Response Query<TService>(
            Func<TService, Response> func, Action<IResponse> postProcessResponse = null);
    }
}