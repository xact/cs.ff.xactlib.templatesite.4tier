﻿namespace App.Core.Infrastructure.Services
{
    using System.Collections.Generic;
    using App.Core;
    using App.Core.Domain.Messages;

    public interface ICachedImmutableReferenceDataMessageService : IHasAppCoreService
    {
        ReferenceDataMessage[] GetReferenceData(string referenceType, bool byPassCache = false, bool resetCache = false);

        Dictionary<string, ReferenceDataMessage[]> GetAllReferenceData();


    }
}
