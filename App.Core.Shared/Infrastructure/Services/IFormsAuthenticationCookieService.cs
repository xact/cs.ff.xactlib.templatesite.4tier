﻿namespace App.Core.Infrastructure.Front
{
    using System;

    public interface IFormsAuthenticationCookieService : IHasAppCoreService
    {
        void SetSessionCookie(TimeSpan? duration = null,
                              string cookiePath = null);

        void SetSessionCookie(string userName, string userData = null, TimeSpan? duration = null,
                              string cookiePath = null);


        void RemoveSessionCookie();
    }
}
