﻿namespace App.Core.Infrastructure.Services
{
    using System.Globalization;
    using XAct.Messages;

    /// <summary>
    /// Contract for a service to 
    /// process the <c>Id</c>
    /// (and <c>Arguments</c> if any)
    /// of an instance of <see cref="Message"/>,
    /// and set it <c>Text</c> 
    /// value to a culture specific resource string.
    /// </summary>
    public interface ICultureSpecificResponseService : IHasAppCoreService
    {

        /// <summary>
        /// Sets the presentation attributes on each <see cref="Message"/>
        /// in the <see cref="IResponse"/> implementation.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="cultureInfo">The culture information.</param>
        void SetPresentationAttributes(IResponse response, CultureInfo cultureInfo=null);

        /// <summary>
        /// Uses the <c>Id</c> value given
        /// (and <c>Arguments</c> if any)
        /// to find the related culture specific resource,
        /// and set the <c>IText</c>
        /// of the given <see cref="Message"/>
        /// implementation.
        /// property.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="cultureInfo">The culture information.</param>
        void SetPresentationAttributes(Message message, CultureInfo cultureInfo=null);


        string GetMessage(MessageCode messageCode, CultureInfo cultureInfo = null);

    }
}
