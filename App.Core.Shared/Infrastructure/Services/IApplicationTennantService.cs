﻿
// ReSharper disable CheckNamespace
namespace App.Core.Infrastructure.Services
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for a service to manage the current
    /// tennant's unique identifier in a multitennant applcation.
    /// <para>
    /// This application does not use a tennant based architecture, 
    /// but the service is provided anyway in order to query the value
    /// for debugging purposes.
    /// </para>
    /// </summary>
    public interface IApplicationTennantService : IHasAppCoreService 
    {
            Guid Get();
        
    }
}
