// ReSharper disable CheckNamespace
namespace App.Core.Constants.Security
// ReSharper restore CheckNamespace
{
    public static partial class Session
    {
        //public const string SessionTokenCookieName = "SessionToken";

        public const string SessionTokenHeaderNamespace = "SessionTokenNamespace";
        public const string SessionTokenHeaderName = "SessionTokenHeader";

        public const string SessionTokenResponseHeaderNamespace = "SessionTokenResponseNamespace";
        public const string SessionTokenResponseHeaderName = "SessionTokenResponseHeader";
    
    
    }
}