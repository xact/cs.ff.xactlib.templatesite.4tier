namespace App.Core.Infrastructure.Front.Services
{
    using System;

    public interface IWcfClient<out TServiceType> : IDisposable
    {
        /// <summary>
        /// Gets the proxy object for the service.
        /// Each call is passed through the socket (precious resource). Avoid foreach loops etc.
        /// </summary>
        /// <value>
        /// The proxy.
        /// </value>
        TServiceType Proxy { get; }
    }
}