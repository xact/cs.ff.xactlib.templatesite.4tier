﻿using System.ServiceModel;

namespace App.Core.Infrastructure.Front.Services.Agents
{
    using System;
    using System.ServiceModel.Description;

    public class WcfClient<TServiceType, TEndpointBehavior> : IWcfClient<TServiceType>
            where TEndpointBehavior : IEndpointBehavior
    {

        public TimeSpan ServiceChannelFactoryClosing = TimeSpan.FromSeconds(3);
        public TimeSpan WpfServiceClosing = TimeSpan.FromSeconds(3);

        /// <summary>
        /// A factory that creates channels of different types that are used by clients to send messages to variously configured service endpoints.
        /// </summary>
        /// <typeparam name="TServiceType">The type of channel produced by the channel factory. This type must be either <see cref="T:System.ServiceModel.Channels.IOutputChannel"/> or <see cref="T:System.ServiceModel.Channels.IRequestChannel"/>.</typeparam>
        private readonly ChannelFactory<TServiceType> _factory;

        /// <summary>
        /// Gets the proxy object for the service.
        /// Each call is passed through the socket (precious resource). Avoid foreach loops etc.
        /// </summary>
        /// <value>
        /// The proxy.
        /// </value>
        public TServiceType Proxy { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WcfClient{TServiceType}"/> class.
        /// </summary>
        /// <param name="clientConfigurationName">Name of the client configuration.</param>
        public WcfClient(string clientConfigurationName, TimeSpan? serviceChannelFactoryClosing=null, TimeSpan? wpfServiceClosing=null)
        {
            if (serviceChannelFactoryClosing.HasValue)
            {
                this.ServiceChannelFactoryClosing = serviceChannelFactoryClosing.Value;
            }

            if (wpfServiceClosing.HasValue)
            {
                this.ServiceChannelFactoryClosing = wpfServiceClosing.Value;
            }


            _factory = new ChannelFactory<TServiceType>(clientConfigurationName);

            //the security behaviour is added to all services except of the session service
            //if (typeof (TServiceType) != typeof (Application.Services.Facades.ISessionServiceFacade))
            //{
            //  _factory.Endpoint.Behaviors.Add(new SecurityClientEndpointBehavior());
            //}
            _factory.Endpoint.Behaviors.Add(System.Activator.CreateInstance<TEndpointBehavior>());
			
            Proxy = _factory.CreateChannel();			
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _factory.Close(this.ServiceChannelFactoryClosing);
            ((ICommunicationObject)Proxy).Close(this.WpfServiceClosing);
        }
    }
}
