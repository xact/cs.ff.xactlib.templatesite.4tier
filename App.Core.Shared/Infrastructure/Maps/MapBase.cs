﻿
namespace App.Core.Infrastructure.Maps
{
    using XAct.ObjectMapping;

    [ObjectMap]
    public abstract class MapBase<TSource, TTarget> : AutoMapperTypeMapperBase<TSource, TTarget>
    {
    }
}
