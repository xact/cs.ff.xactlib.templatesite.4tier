﻿namespace App.Core.Infrastructure.Maps.CustomResolvers
{
    using App.Core.Infrastructure.Services;
    using AutoMapper;

    public abstract class ClientCustomResolverBase<TSource, TTarget> : ValueResolver<TSource, TTarget>
    {
        protected IClientEnvironmentService _ClientEnvironmentService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IClientEnvironmentService>(); }
        }
    }
}