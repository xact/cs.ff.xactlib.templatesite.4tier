﻿using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace App.Core.Shared.Services.Services.Security
{
    public class SecurityClientEndpointBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(
            ServiceEndpoint endpoint, BindingParameterCollection bindingParameters
            ) { return; }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new SecurityClientMessageInspector());
            //foreach (ClientOperation op in clientRuntime.Operations) op.ParameterInspectors.Add(new Inspector());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            //endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new Inspector());
            //foreach (DispatchOperation op in endpointDispatcher.DispatchRuntime.Operations)
            //    op.ParameterInspectors.Add(new Inspector());
        }

        public void Validate(ServiceEndpoint endpoint) { return; }
    }
}