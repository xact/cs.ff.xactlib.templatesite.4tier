﻿namespace App.Core.Shared.Services.Services.Security
{
    using System;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using App.Core.Domain.Security;

    /// <summary>
    /// SOAP Message Header, 
    /// attached by 
    /// <see cref="SecurityClientMessageInspector"/>
    /// in front tier,
    /// in to pass to back tier, where it is is intercepted
    /// by <see cref="App.Core.Infrastructure.Services.SecurityDispatchMessageInspector"/>, 
    /// in order to extract SessionToken 
    /// which is used to retrieve from Cache/Db a Session
    /// and from that an <see cref="AppIdentity"/>,
    /// which is attached to the Back Tier's thread.
    /// </summary>
    [DataContract]
    public class SessionTokenEnvelope
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SessionTokenEnvelope" /> class.
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        public SessionTokenEnvelope(Guid? sessionToken)
        {
            SessionToken = sessionToken;
        }

        /// <summary>
        /// Gets or sets the session token.
        /// </summary>
        /// <value>
        /// The session token.
        /// </value>
        [DataMember]
        public Guid? SessionToken { get; set; }


        /// <summary>
        /// Each Request from the front Tier to the back tier
        /// increments this counter.
        /// </summary>
        [DataMember]
        public int RequestCounter { get; set; }


        /// <summary>
        /// Get a flag indicating that the Front Tier
        /// does not have a Session object in its cache
        /// (previous requests to the back tier
        /// must have come in via other
        /// Front Tier servers).
        /// and needs the Back Tier to return one that it can
        /// cache.
        /// </summary>
        [DataMember]
        public bool ReturnSession { get; set; }

        /// <summary>
        /// Converts this instance to untyped MessageHeader.
        /// </summary>
        /// <returns></returns>
        public MessageHeader ToMessageHeader()
        {
            return new MessageHeader<SessionTokenEnvelope>(this)
                .GetUntypedHeader(
                Constants.Security.Session.SessionTokenHeaderName, 
                Constants.Security.Session.SessionTokenHeaderNamespace);
        }
    }
}