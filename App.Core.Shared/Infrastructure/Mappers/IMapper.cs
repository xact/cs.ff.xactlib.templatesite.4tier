﻿namespace App.Core.Infrastructure.Mappers
{
    using System;

    public interface IMapper<in TSource, TTarget>
        where TSource : class
        where TTarget : class, new()
    {
        bool IsMapRegistered();
        bool IsMapValid(out Exception exception);

        TTarget Map(TSource source);
        TTarget Map(TSource source, TTarget target);
    }
}