﻿using XAct.Services;

namespace App.Core.Infrastructure.Mappers.Implementations
{
    using System;
    using App.Core.Infrastructure.Services;
    using XAct;

    [DefaultBindingImplementation(typeof(IMapper<,>),BindingLifetimeType.Undefined, Priority.Low)]
    public class MapperBase<TSource,TTarget> : IMapper<TSource, TTarget>
        where TSource : class
        where TTarget : class, new()
    {
        protected readonly IObjectMappingService _objectMappingService;

        public MapperBase(IObjectMappingService objectMappingService)
        {
            _objectMappingService = objectMappingService;
        }

        public bool IsMapRegistered()
        {
            return _objectMappingService.TestMapExists<TSource, TTarget>();
        }

        public bool IsMapValid(out Exception exception)
        {
            return _objectMappingService.TestMapIsValid<TSource, TTarget>(out exception);
        }

        public virtual TTarget Map(TSource source)
        {
            return _objectMappingService.Map<TSource,TTarget>(source);
        }

        public virtual TTarget Map(TSource source, TTarget target)
        {
            return _objectMappingService.Map<TSource, TTarget>(source, target);
        }
    }
}