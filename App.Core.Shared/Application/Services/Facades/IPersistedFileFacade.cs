﻿
namespace App.Core.Application.Services.Facades
{
    using System;
    using System.ServiceModel;
    using App.Core.Application.Services.Facades;
    using App.Core.Shared.Services.Attributes;
    using XAct.Net.Messaging;
    using XAct.Security;
    using XAct.Services.Comm.ServiceModel;

    public interface IPersistedFileFacade : IHasServiceFacade
    {

        /// <summary>
        /// Persists (saves or Updates) the <see cref="T:XAct.Net.Messaging.PersistedFile"/> object.
        /// </summary>
        /// <param name="persistedFile">The persisted file.</param>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [Authorization]
        void Persist(PersistedFile persistedFile);

        /// <summary>
        /// Retrieves the <see cref="T:XAct.Net.Messaging.PersistedFile"/>
        /// </summary>
        /// <param name="id">The identifier.</param><param name="includeMetadata">if set to <c>true</c>, search for any metadata to include.</param>
        /// <returns/>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [Authorization]
        PersistedFile Retrieve(Guid id, bool includeMetadata = false);

        /// <summary>
        /// Retrieves the <see cref="T:XAct.Net.Messaging.PersistedFile"/>
        /// </summary>
        /// <param name="applicationTennantId">The organisation identifier.</param><param name="ownerId">The owner identifier.</param><param name="includeMetadata">if set to <c>true</c>, search for any metadata to include.</param>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [Authorization]
        PersistedFile Retrieve(Guid applicationTennantId, Guid ownerId, bool includeMetadata = false);

    }
}
