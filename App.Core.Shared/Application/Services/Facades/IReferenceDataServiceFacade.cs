﻿namespace App.Core.Application.Services.Facades
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using App.Core.Application.Services.Facades;
    using App.Core.Domain.Messages;
    using App.Core.Shared.Services.Attributes;
    using XAct.Security;
    using XAct.Services.Comm.ServiceModel;

    [ServiceContract(Name = "ReferenceDataService")]
    [Authorization]
    public interface IReferenceDataServiceFacade : IHasServiceFacade
    {

        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof (AppSoapException))]
        [Authorization]
        Dictionary<string, ReferenceDataMessage[]> GetAllReferenceData();


        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [Authorization]
        //[AllowAnonymous] //Has to be anonymous as Home page is requesting it at present.
        [Authorization]
        [AllowAnonymous] //Has to be anonymous as Home page is requesting it at present.
        ReferenceDataMessage[] GetReferenceData(string referenceType);


    }
}

