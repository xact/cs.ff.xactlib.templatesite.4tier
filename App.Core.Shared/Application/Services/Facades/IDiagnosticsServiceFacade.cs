﻿namespace App.Core.Application.Services.Facades
{
    using System;
    using System.ServiceModel;
    using App.Core.Application.Services.Facades;
    using App.Core.Shared.Services.Attributes;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Security;
    using XAct.Services.Comm.ServiceModel;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Note that implementation should not use caching as the
    /// service it invokes
    /// </internal>
    [ServiceContract(Name = "DiagnosticsService")]
    [Authorization]
    public interface IDiagnosticsServiceFacade : IHasServiceFacade
    {

        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [XAct.Security.AllowAnonymous]
        string Ping();

        /// <summary>
        /// TODO
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof (AppSoapException))]
        [XAct.Security.AllowAnonymous]
        StatusResponse[] GetSet(string[] names, DateTime? startDateTimeUtc = null, DateTime? endDateTimeUtc = null);



        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [XAct.Security.AllowAnonymous]
        StatusResponse GetSingle(string name, object arguments = null, DateTime? startDateTimeUtc = null, DateTime? endDateTimeUtc = null);

    }
}