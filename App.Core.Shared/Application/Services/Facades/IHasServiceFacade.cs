﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Application.Services.Facades
{
    using XAct;

    public interface IHasServiceFacade : IHasAppService
    {
    }
}
