﻿using System.Collections.Generic;
using System.Globalization;
using System.ServiceModel;
using App.Core.Domain.Messages;
using App.Core.Shared.Services.Attributes;
using XAct.Security;
using XAct.Services.Comm.ServiceModel;

namespace App.Core.Application.Services.Facades
{
    using App.Core.Application.Services.Facades;

    [ServiceContract(Name="ResourceService")]
    [Authorization]
    public interface IResourceServiceFacade : IHasServiceFacade
    {

        /// <summary>
        /// Gets a single localised resource string.
        /// <para>
        /// Warning: calling this method repetively is expensive.
        /// The preferred design is for the Presentation tier
        /// to work instead with a cached result set from having
        /// invoked <see cref="GetResourceSet"/>  or 
        /// <see cref="GetResourceSets"/>
        /// </para>
        /// </summary>
        /// <param name="resourceSetName">Name of the resource set.</param>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <returns></returns>
        /// <internal>
        /// The reason for using a string -- rather than <see cref="CultureInfo" />
        /// to pass context information regarding the context -- is that t
        /// CultureInfo causes SOAP serialisation to not occur.
        ///   </internal>
        [OperationContract(Name="GetString")]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [Authorization]
        [XAct.Security.AllowAnonymous]
        string GetResource(string resourceSetName, string name, string cultureInfoCode, ResourceTransformation resourceTransformation = ResourceTransformation.None);




        /// <summary>
        /// Gets a dictionary of a single Resource Filter set.
        /// </summary>
        /// <param name="resourceSetName">Name of the view.</param>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <returns></returns>
        /// <internal>
        /// The reason for using a string -- rather than <see cref="CultureInfo" />
        /// to pass context information regarding the context -- is that t
        /// CultureInfo causes SOAP serialisation to not occur.
        ///   </internal>
        [OperationContract(Name="GetResourceSet")]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [Authorization]
        [XAct.Security.AllowAnonymous]
        Dictionary<string, string> GetResourceSet(string resourceSetName, string cultureInfoCode, ResourceTransformation resourceTransformation = ResourceTransformation.None);

        /// <summary>
        /// Returns a dictionary of sets of resources
        /// where the dictionary key is the resource Filter
        /// (often a View name).
        /// </summary>
        /// <param name="cultureInfoCode">Culture Info</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <param name="resourceSetNames">List of filters to retrieve resources for</param>
        /// <returns>Returns a Dictionary containing all resource strings.</returns>
        /// <internal>
        /// The reason for using a string -- rather than <see cref="CultureInfo" />
        /// to pass context information regarding the context -- is that t
        /// CultureInfo causes SOAP serialisation to not occur.
        ///   </internal>
        [OperationContract(Name = "GetResourceSets")]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [Authorization]
        [XAct.Security.AllowAnonymous]
        Dictionary<string, Dictionary<string, string>> GetResourceSets(string cultureInfoCode, ResourceTransformation resourceTransformation = ResourceTransformation.None, params string[] resourceSetNames);


    }


}
