﻿using System.ServiceModel;
using App.Core.Shared.Services.Attributes;
using XAct.Security;
using XAct.Services.Comm.ServiceModel;

namespace App.Core.Application.Services.Facades
{
    [ServiceContract]
    [Authorization]
    public interface IHasPing : XAct.Services.Comm.ServiceModel.IHasPing
    {
        /// <summary>
        /// A stub method to ping, to determine whether integration 
        /// across infrastructure was successful.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [Authorization]
        [AllowAnonymous]
        new string Ping();
    }
}