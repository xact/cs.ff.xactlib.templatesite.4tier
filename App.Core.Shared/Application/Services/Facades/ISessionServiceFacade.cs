﻿////BACK

//using App.Core.Constants.Security;
//using App.Core.Shared.Services.Attributes;
//using XAct.Messages;
//using XAct.Security;
//using XAct.Services.Comm.ServiceModel;

//namespace App.Core.Application.Services.Facades
//{
//    using System;
//    using System.ServiceModel;
//    using App.Core.Constants.Application.Security;
//    using App.Core.Domain.Messages;
//    using App.Core.Domain.Models;
//    using XAct.Diagnostics.Performance;

//    /// <summary>
//    /// Provides functionality for user authentication using OAuth ESAA facade.
//    /// </summary>
//    [ServiceContract]
//    [Authorization]
//    public interface ISessionServiceFacade  : IHasServiceFacade, IHasPing
//    {
//        /// <summary>
//        /// Instantiates the Session entity and identifies with an unique identifier (App Session Token).
//        /// The session entity and passed SignInInfo entity are then persisted in the database, Session entity is cached,
//        /// identity deserialized and attached to a thread - for authorization purposes.
//        /// </summary>
//        /// <param name="identityBase64">The identity base64.</param>
//        /// <param name="signInInfo">The session log.</param>
//        /// <returns>
//        /// App Session Token
//        /// Errors:
//        /// -...
//        /// </returns>
//        /// <exception cref="System.ArgumentNullException">
//        /// sessionLog
//        /// or
//        /// identityBase64
//        /// </exception>
//        /// <exception cref="System.ArgumentException">Invalid identity string</exception>
//        [OperationContract]
//        [ApplyDataContractResolver]
//        [FaultContract(typeof(AppSoapException))]
//        [AllowAnonymous]
//        [PerformanceCounter(Constants.Diagnostics.PerformanceCounters.Category, Constants.Diagnostics.PerformanceCounters.Login, true, Constants.Diagnostics.PerformanceCounters.SubOperations, PerformanceCounterUpdateType.Automatic)]
//        Response<SessionResponse> CreateSession(string identityBase64, SignInInfo signInInfo);

//        /// <summary>
//        /// Passes the ESSA Access Token and organization ID to the OAUTH interface, retrieves ESAA Token Info which is used
//        /// to create the user identity with claims. Identity is serialized to base64 format and stored within the session entity.
//        /// Session entity and SessionInfo is identified by GUID (APP Session Info), stored in the database and locally cached.
//        /// The Session entity is then passed back to the caller.
//        /// </summary>
//        /// <param name="esaaAccessToken">The esaa access token.</param>
//        /// <param name="OrgId">The org unique identifier.</param>
//        /// <param name="signInInfo">The sign information information.</param>
//        /// <returns>
//        /// Created session
//        /// Errors:
//        /// - When ESAA Access Token has been used more than once
//        /// - ...
//        /// </returns>
//        /// <exception cref="System.ArgumentNullException">esaaAccessToken
//        /// or
//        /// sessionLog
//        /// or
//        /// orgId</exception>
//        /// <exception cref="System.ArgumentException"></exception>
//        [OperationContract]
//        [ApplyDataContractResolver]
//        [FaultContract(typeof(AppSoapException))]
//        [AllowAnonymous]
//        [PerformanceCounter(Constants.Diagnostics.PerformanceCounters.Category, Constants.Diagnostics.PerformanceCounters.Login, true, Constants.Diagnostics.PerformanceCounters.SubOperations, PerformanceCounterUpdateType.Automatic)]
//        Response<SessionResponse> CreateSessionOAuth(string esaaAccessToken, string OrgId, SignInInfo signInInfo);

//        [OperationContract]
//        [ApplyDataContractResolver]
//        [FaultContract(typeof(AppSoapException))]
//        [AllowAnonymous]
//        Response<SessionResponse> UpdateSessionTime(Guid sessionToken, bool returnSession = false);

//        [OperationContract]
//        [ApplyDataContractResolver]
//        [FaultContract(typeof(AppSoapException))]
//        [AllowAnonymous] // TODO: Awaiting ability to have AuthoriseByAOP run without needing a Role specified
//        [PerformanceCounter(Constants.Diagnostics.PerformanceCounters.Category, Constants.Diagnostics.PerformanceCounters.Logout, true, Constants.Diagnostics.PerformanceCounters.SubOperations, PerformanceCounterUpdateType.Automatic)]
//        void DeleteSession(Guid sessionToken);

//        [OperationContract]
//        [ApplyDataContractResolver]
//        [FaultContract(typeof(AppSoapException))]
//        [AllowAnonymous]
//        bool DoesSessionExist(Guid sessionToken);




//        [OperationContract]
//        [ApplyDataContractResolver]
//        [FaultContract(typeof (AppSoapException))]
//        //NO: WE WANT IT TO RAISE EXCEPTION [AllowAnonymous]
//        [Authorization(Roles = Privileges.NSI2_SEARCH_VIEW)]
//        void IsStillAlive();
//    }
//}