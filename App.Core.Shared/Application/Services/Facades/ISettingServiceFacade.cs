﻿
using System.ServiceModel;
using App.Core.Infrastructure;
using App.Core.Shared.Services.Attributes;
using XAct.Security;
using XAct.Services.Comm.ServiceModel;



namespace App.Core.Application.Services.Facades
{
    using App.Core.Application.Services.Facades;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Note that implementation should not use caching as the
    /// service it invokes
    /// </internal>
    [ServiceContract(Name = "SettingService")]
    [Authorization]
    public interface ISettingServiceFacade : IHasServiceFacade, IHasPing
    {
        /// <summary>
        /// Retrieves this Application's combined App and Host settings.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [Authorization]
        AppSettings Retrieve();

    }
}
