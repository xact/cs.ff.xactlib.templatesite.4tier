﻿using System.ServiceModel;
using App.Core.Domain.Messages;
using App.Core.Shared.Services.Attributes;
using XAct.Security;
using XAct.Services.Comm.ServiceModel;

namespace App.Core.Application.Services.Facades
{
    using App.Core.Application.Services.Facades;

    [ServiceContract(Name="HelpService")]
    [Authorization]
    public interface IHelpServiceFacade : IHasServiceFacade
    {

        // Arguement for Culture is a string, rather than a CultureInfo
        //as CultureInfo can't be WCF serialized.


        /// <summary>
        /// Gets the Help Entry Resouce string by filter and key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="cultureInfoCode">The culture information code.</param>
        /// <returns></returns>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        [Authorization]
        HelpEntrySummary GetByKey(string key, string cultureInfoCode = null);

        

    }


}
