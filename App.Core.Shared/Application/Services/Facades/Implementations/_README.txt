﻿In the context of an NTier app, these are the 
Interface Contracts of Application Services 
that are shareable with the Presentation Layer in an 
another Tier. 

Their actual Implementations are defined elsewhere, in App.Core.Application

This is so that this Assembly can be shared with both the Application
and Presentation tier, while leaving the Service Instances be only
in the back tier.