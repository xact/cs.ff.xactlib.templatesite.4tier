﻿using App.Core.Infrastructure.Services;

namespace App.Core.Infrastructure.Validators
{
    using XAct;
    using XAct.Messages;
    using XAct.Services;

    [DefaultBindingImplementation(typeof(IValidator<>),BindingLifetimeType.Undefined, Priority.Low)]
    public class ValidatorBase<TEntity> : IValidator<TEntity>
    {
		private readonly IValidatorService _validatorService;

		public ValidatorBase(IValidatorService validatorService)
		{
			_validatorService = validatorService;			
		}

	    protected ValidatorBase() 
			: this(XAct.DependencyResolver.Current.GetInstance<IValidatorService>())
	    {
	    }

	    public virtual bool Validate(TEntity entity, IResponse response)
	    {
		    return _validatorService.Validate(entity, response);
	    }
    }
}