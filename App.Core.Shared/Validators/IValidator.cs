﻿namespace App.Core.Infrastructure.Validators
{
    using XAct.Messages;

    public interface IValidator<in TEntity>
    {        
        bool Validate(TEntity entity,  IResponse response);
    }
}