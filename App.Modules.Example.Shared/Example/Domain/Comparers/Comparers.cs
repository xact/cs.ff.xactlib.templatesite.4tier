﻿using System;
using XAct;

namespace App.Core.Domain.Extensions
{
	public delegate bool NameComparer(string name1, string name2);

	public static class NameComparers
	{
		/// <summary>
		/// String comparer. Strings are trimmed and compared case insensitive.
		/// </summary>
		public static NameComparer CaseInsensitive = (name1, name2) => Trim(name1).ToLower() == Trim(name2).ToLower();

		/// <summary>
		/// String comparer. Strings are trimmed and compared case sensitive.
		/// </summary>
		public static NameComparer CaseSensitive = (name1, name2) => Trim(name1) == Trim(name2);

		public static NameComparer GetComparer(CaseSensitivity caseSensitivity)
		{
			switch (caseSensitivity)
			{
				case CaseSensitivity.Insensitive:
					return CaseInsensitive;
				case CaseSensitivity.Sensitive:
					return CaseSensitive;
			}
			throw new Exception("Case sensitivy not registered.");
		}

		private static string Trim(string s)
		{
			return String.IsNullOrEmpty(s) ? String.Empty : s.Trim();
		}
	}
}