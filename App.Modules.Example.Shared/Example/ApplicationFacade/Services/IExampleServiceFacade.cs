﻿namespace App.Modules.Example.Example.ApplicationFacade.Services
{
    using System.ServiceModel;
    using App.Core.Shared.Services.Attributes;
    using App.Modules.Example.Domain.Entities;
    using App.Modules.Example.Domain.Messages;
    using App.Modules.Example.Domain.Models;
    using App.Modules.Example.Domain.ViewModels;
    using XAct.Security;
    using System;
    using XAct.Messages;
    using XAct.Services.Comm.ServiceModel;

    /// <summary>
	/// Contract for a Facade of an Application Layer service.
	/// </summary>
	/// <internal>
	/// <para>
    /// Contract returns Domain Entities (eg: <see cref="Example"/>) and Domain Projections
    /// rather IO Messages (eg: <see cref="NullableExample"/> or <see cref="ExampleViewModel"/>).
    /// </para>
    /// <para>
    /// The Facade acheives the following design goals:
    /// </para>
    /// <para>
    /// * Exposes a simplified monitored interface that the Presentation
    ///   layer can invoke, without having to deal with the 
    /// * In general, a presentation layer operation is suppossed to prepare
    ///   it's arguments into a single request argument package to a single 
    ///   Application layer operation (Chunky communication, rather than Chatty). 
    ///   This achieves several aims:
    ///   * a better point at which 3 tiers apps can be scaled to n-tier apps later as needed.
    ///   * a single point where a UnitofWork can commit (rather than committing several
    ///     times during an operation, which can make it much harder to perform undo half operations
    ///     upon failure.
    ///   * allows the Application layer services to have re-entrant methods.
    ///     In other words, if an operation is recursive, at the end of which a Commit is required
    ///     and must be public for other uses, you want
    ///     to put the recursive (ie re-entrant) method on the Application layer, and put on the Facade
    ///     layer just a non-re-entrant wrapper method that invokes the Application layer method, 
    ///     and that concludes with a Commit. 
    /// </para>
    /// </internal>
    [ServiceContract(Name = "ExampleService")]
    [Authorization]
    public interface IExampleServiceFacade
	{		
        /// <summary>
        /// Gets a <see cref="Response"/> containing a single <see cref="Example" />.
        /// <para>
        /// If validation fails, or the record is not found, etc., the <see cref="Response"/>
        /// will instead contain error messages.
        /// </para>
        /// </summary>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        Response<Example> GetSingle(Guid id);


        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        Response<Example> GetSingle(string code);


        //[OperationContract]
        //[ApplyDataContractResolver]
        //[FaultContract(typeof(AppSoapException))]
        //PagedResponse<ExampleSummary> PagedSearch(PagedRequest<ExampleSearchTerms> request);

        //[OperationContract]
        //[ApplyDataContractResolver]
        //[FaultContract(typeof(AppSoapException))]
        //ExampleSummary[] Search(ExampleSearchTerms searchTerms);

        /// <summary>
        /// Adds the specified <see cref="Example"/> entity
        /// <para>
        /// If validation fails, or the record is not found, etc., the <see cref="Response"/>
        /// will instead contain error messages.
        /// </para>
        /// </summary>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        Response Add(NullableExample example);


        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        Response Update(NullableExample example);


        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        Response<ExampleSummary[]> Search(ExampleSearchTerms request);

        /// <summary>
        /// Deletes the specified entity,
        /// and returns a <see cref="Response"/>
        /// </summary>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        Response Delete(NullableExample example);


        /// <summary>
        /// Deletes the specified entity,
        /// and returns a <see cref="Response"/>
        /// </summary>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        Response Delete(Guid id);



        /// <summary>
        /// Deletes the specified entity, using it's <see cref="XAct.IHasCode"/>
        /// and returns a <see cref="Response"/>
        /// </summary>
        [OperationContract]
        [ApplyDataContractResolver]
        [FaultContract(typeof(AppSoapException))]
        Response Delete(string code);

    }
}
