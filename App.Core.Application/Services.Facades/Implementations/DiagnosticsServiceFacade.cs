﻿namespace App.Core.Application.Services.Facades.Implementations
{
    using System;
    using System.ServiceModel;
    using App.Core.Application.Services.Facades;
    using App.Core.Infrastructure;
    using App.Core.Infrastructure.Services;
    using App.Core.Shared.Services.Attributes;
    using App.Core.Infrastructure.Services;
    using XAct;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Services;
    using XAct.Services.Comm.ServiceModel;

    //[ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class DiagnosticsServiceFacade : ServiceFacadeBase, IDiagnosticsServiceFacade
    {
        readonly IDiagnosticsStatusService _statusService;


        /// <summary>
        /// Initializes a new instance of the <see cref="DiagnosticsServiceFacade" /> interface.
        /// </summary>
        /// <param name="unitOfWorkService">The unit of work service.</param>
        /// <param name="statusService">The status service.</param>
        public DiagnosticsServiceFacade(IUnitOfWorkService unitOfWorkService, IDiagnosticsStatusService  statusService):base(unitOfWorkService)
        {
            _statusService = statusService;
        }

        public virtual string Ping()
        {
            //Note: security Attributes applied directly, as method cannot be defined
            //in  IDiagnosticsServiceFacade (it requires a 'new', which confuses WCF)
            return _statusService.Ping();
        }

        public virtual StatusResponse[] GetSet(string[] names, DateTime? startDateTimeUtc = null, DateTime? endDateTimeUtc = null)
        {
            //Note that the WCF Operation is named differently than the service
            //as WCF cannot handle overloads.
            return _statusService.Get(names, startDateTimeUtc, endDateTimeUtc);
        }

        /// <summary>
        /// Gets a status summary of conditions on the server.
        /// 
        /// </summary>
        /// <param name="name">The name.</param><param name="arguments">Optional arguments that can be dispatched to the controllers.</param><param name="startDateTimeUtc">The start date time UTC.</param><param name="endDateTimeUtc">The end date time UTC.</param>
        /// <returns/>
        public virtual StatusResponse GetSingle(string name, object arguments = null, DateTime? startDateTimeUtc = null,
                                  DateTime? endDateTimeUtc = null)
        {
            //Note that the WCF Operation is named differently than the service
            //as WCF cannot handle overloads.
            return _statusService.Get(name, arguments, startDateTimeUtc, endDateTimeUtc);
        }


    }
}