﻿There are pros/cons of using an AppFacade separate from the Application Service:


* Advantage:
    * Specific Typing. 
	  An example would be the passing of CultureInfo. It's not WCF serializable, so
	  a WCF based AppFacade contract would expose it as a string CultureInfoCode. 
	  It's more elegant to let the AppFacade accept the string, and convert it to a
	  typed CultureInfo, before invoking the Application service contract which can.
    * WCF error trapping.
	  In a net applciation throwing exceptions is the norm. But WCF isn't .NET.
	  To transfer exceptions through the wire, they have to be converted to Fault Contracts.
	  This kind of communication specific handling should not be in the Application
	  Service, reserving it for the wrapping App Service Facade.
    * Security:
	  A front tier can call a back tier once -- but the back tier might call itself
	  multiple times. An example of such a scenario would be when a back tier service
	  such as ExampleService exposes both an AddExample and a BatchAddExample operation.
	  The BatchAdd could be entry point, but in turn reuse and invoke AddExample operation
	  multiple times. 
	  AuthN and AuthZ verification can get expensive, and should be minimized to 
	  back tier entry points. 
	* Commits
	  The end step of an operation is the logical place to commit a UnitOfWork.
	  Therefore the AddExample and BatchAddExample methods last operation would be uow.Commit();
	  In the case of there being no Facade, the BatchAddExample will invoke the AddExample
	  multiple times, and Commit() expensively, multiple times.
	  A better solution is to commit only on AppFacade operations. This way, the AppFacade's BatchAddExample 
	  operation invokes the Application Service's AddExample method multiple times without a commit being
	  invoked, until all the AddExample methods are completed.
	      * The notion of using a stack commit counter to keep track of when a commit should be done
		    adds uneccessary brittleness.
* Disadvantages:
    * Multiple files, very similar to each other.
	    * A strategy to mitigate the confusion that arises from using many files with very similar names
		  is to use different names:
		    * IExampleService 
			* IExampleServiceFacade or IExampleServiceContract (reflecting the WCF nature of the contract)
			* ExampleService
			* ExampleServiceFacade
	