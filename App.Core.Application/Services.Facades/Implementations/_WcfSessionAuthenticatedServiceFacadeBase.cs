﻿namespace App.Core.Application.Services.Facades.Implementations
{
    using App.Core.Application.Services.Facades.Implementations;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services;

    /// <summary>
    /// Base class for services requiring established session for the communication. Session expiry will be checked
    /// and updated before each call to any of the service methods.
    /// </summary>
    [SecurityServiceBehaviour]
    public abstract class WcfSessionAuthenticatedServiceFacadeBase : ServiceFacadeBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="WcfSessionAuthenticatedServiceFacadeBase"/> class.
        /// </summary>
        /// <param name="unitOfWorkService">The unit of work service.</param>
        protected WcfSessionAuthenticatedServiceFacadeBase(IUnitOfWorkService unitOfWorkService):base(unitOfWorkService){}
    }
}