// ReSharper disable CheckNamespace

namespace App.Core.Application.Services.Facades.Implementations
// ReSharper restore CheckNamespace
{
    using App.Core.Application.Services;
    using App.Core.Application.Services.Facades;
    using XAct;
    using XAct.Services;
    using App.Core.Infrastructure;
    using App.Core.Infrastructure.Services;
    using XAct.Security;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Note how we are Binding Application level services it to *both* 
    /// the Application Service *and* ApplicationFacade Service.
    /// </internal>
    [ServiceErrorBehaviour(typeof (HttpErrorHandler))]
    public class SettingServiceFacade : ServiceFacadeBase, ISettingServiceFacade
    {
        private readonly ISettingService _settingService;

        public SettingServiceFacade(ISettingService settingService, IUnitOfWorkService unitOfWorkService)
            : base(unitOfWorkService)
        {
            _settingService = settingService;
        }

        public virtual string Ping()
        {
            string results = InvokeServiceFunction(() => _settingService.Ping());

            return results;
        }

        [AllowAnonymous]
        public virtual AppSettings Retrieve()
        {
            AppSettings results = InvokeServiceFunction(() => _settingService.Retrieve());

            return results;
        }
    }
}