namespace App.Core.Application.Services.Facades.Implementations
{
    using System.Collections.Generic;
    using App.Core.Application.Services.Facades;
    using XAct;
    using XAct.Services;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;

    public class ReferenceDataServiceFacade : ServiceFacadeBase, IReferenceDataServiceFacade
    {
        private readonly ICachedImmutableReferenceDataMessageService _cachedReferenceDataService;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="ReferenceDataServiceFacade"/> class.
        /// </summary>
        public ReferenceDataServiceFacade(
            IUnitOfWorkService unitOfWorkService, 
            ICachedImmutableReferenceDataMessageService cachedReferenceDataService)
            : base(unitOfWorkService)
        {
            _cachedReferenceDataService = cachedReferenceDataService;
        }

        public virtual Dictionary<string, ReferenceDataMessage[]> GetAllReferenceData()
        {
            return
                InvokeServiceFunction(
                    () => _cachedReferenceDataService.GetAllReferenceData());
        }

        public virtual ReferenceDataMessage[] GetReferenceData(string referenceType)
        {
            ReferenceDataMessage[] results =
                InvokeServiceFunction(
                    () => _cachedReferenceDataService.GetReferenceData(referenceType));

            return results;
        }


    }
}