﻿// ReSharper disable CheckNamespace

namespace App.Core.Application.Services.Facades.Implementations
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.Globalization;
    using App.Core.Application.Services.Facades;
    using XAct;
    using XAct.Security;
    using XAct.Services;
    using App.Core.Infrastructure.Services;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure;
    using App.Core.Infrastructure.Services;

    /// <summary>
    ///     Implementation of the <see cref="ICultureSpecificResourceService" />
    ///     to provide to the Front Tier any Resources it may need.
    /// </summary>
    [ServiceErrorBehaviour(typeof (HttpErrorHandler))]
    public class ResourceServiceFacade : ServiceFacadeBase,  IResourceServiceFacade
    {
        private readonly Application.Services.IResourceService _resourceService;


        //// FAKE CONSTRUCTOR UNTIL WE CREATE A SERVICEHOST FACTORY.
        //// (WCF CANT DO DEPENDENCY INJECTION BY DEFAULT)
        //public ResourceService()
        //    : this(Shortcuts.ServiceLocate<Infrastructure.Services.IResourceService>())
        //{
        //}

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceServiceFacade" /> class.
        /// </summary>
        /// <param name="resourceService">The resource service.</param>
        /// <param name="unitOfWorkService">The unit of work service.</param>
        public ResourceServiceFacade(Application.Services.IResourceService resourceService, IUnitOfWorkService unitOfWorkService)
            : base(unitOfWorkService)
        {
            _resourceService = resourceService;
        }


        /// <summary>
        ///     Gets a single localised resource string.
        ///     Gets the view resources.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="resourceTransformation">The resource transformation.</param>
        /// <returns></returns>
        /// <internal>
        ///     The reason for using a string -- rather than <see cref="CultureInfo" />
        ///     to pass context information regarding the context -- is that t
        ///     CultureInfo causes SOAP serialisation to not occur.
        /// </internal>
        [AllowAnonymous]
        public virtual Dictionary<string, string> GetResourceSet(string viewName, string cultureInfoCode,
                                                           ResourceTransformation resourceTransformation =
                                                               ResourceTransformation.None)
        {

            CultureInfo cultureInfo = (cultureInfoCode.IsNullOrEmpty())
                                          ? CultureInfo.InvariantCulture
                                          : new CultureInfo(cultureInfoCode);

            Dictionary<string, string> results =
                InvokeServiceFunction(() => _resourceService.GetResourceSet(viewName, cultureInfo, resourceTransformation));

            return results;

        }


        public virtual string GetResource(string filter, string name, string cultureInfoCode,
                                ResourceTransformation resourceTransformation = ResourceTransformation.None)
        {
            CultureInfo cultureInfo = (cultureInfoCode.IsNullOrEmpty())
                                          ? CultureInfo.InvariantCulture
                                          : new CultureInfo(cultureInfoCode);

            string results =
                InvokeServiceFunction(() => _resourceService.GetResource(filter,name, cultureInfo, resourceTransformation));

            return results;
            
        }


        public virtual Dictionary<string, Dictionary<string, string>> GetResourceSets(string cultureInfoCode,
                                                          ResourceTransformation resourceTransformation =
                                                              ResourceTransformation.None, params string[] filters)
        {
            CultureInfo cultureInfo = (cultureInfoCode.IsNullOrEmpty())
                                          ? CultureInfo.InvariantCulture
                                          : new CultureInfo(cultureInfoCode);

            Dictionary<string, Dictionary<string,string>> results =
                InvokeServiceFunction(() => _resourceService.GetResourceSets(cultureInfo, resourceTransformation,filters));

            return results;
        }
    }
}