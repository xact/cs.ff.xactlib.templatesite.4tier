﻿// ReSharper disable CheckNamespace
namespace App.Core.Application.Services.Facades.Implementations
// ReSharper restore CheckNamespace
{
    using System.Globalization;
    using System.ServiceModel;
    using App.Core.Application.Services.Facades;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure;
    using XAct;
    using XAct.Services;
    using XAct.Security;
    using App.Core.Application.Services.Implementations;
    using App.Core.Infrastructure.Services;

    /// <summary>
    /// An implementation of the <see cref="App.Core.Application.Services.IHelpService"/>
    /// (and therefore <see cref="IHelpServiceFacade"/>) 
    /// to provide to the Front Tier any Help Resources it may need.
    /// </summary>
	[ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public class HelpServiceFacade : ServiceFacadeBase, IHelpServiceFacade
    {
        private readonly App.Core.Application.Services.IHelpService _helpService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpService" /> class.
        /// </summary>
        /// <param name="helpService">The resource service.</param>
        /// <param name="unitOfWorkService">The unit of work service.</param>
        public HelpServiceFacade(App.Core.Application.Services.IHelpService helpService, IUnitOfWorkService unitOfWorkService)
            : base(unitOfWorkService)
        {
            _helpService = helpService;
        }

        /// <summary>
        /// Gets the Help Entry Resouce string by filter and key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="cultureInfoCode">The culture information code.</param>
        /// <returns></returns>
        /// <exception cref="FaultReason"></exception>
        [AllowAnonymous]
        public virtual HelpEntrySummary GetByKey(string key, string cultureInfoCode = null)
        {

            CultureInfo cultureInfo = (cultureInfoCode.IsNullOrEmpty())
                                          ? CultureInfo.InvariantCulture
// ReSharper disable AssignNullToNotNullAttribute
                                          : new CultureInfo(cultureInfoCode);
// ReSharper restore AssignNullToNotNullAttribute
            
            HelpEntrySummary result = InvokeServiceFunction(() => _helpService.GetHelpEntryByKey(key, cultureInfo));

            return result;


        }



    }


}