﻿// ReSharper disable CheckNamespace
namespace App.Core.Application.Services.Facades.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.ServiceModel;
    using App.Core.Infrastructure;
    using XAct.Diagnostics;
    using XAct.Security;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure._DEBUG.LIB;

    [Authorization]
    [ServiceErrorBehaviour(typeof(HttpErrorHandler))]
    public abstract class ServiceFacadeBase
    {
        protected IUnitOfWorkService UnitOfWorkService
        {
            get { return _unitOfWorkService; }
        }
        private readonly IUnitOfWorkService _unitOfWorkService;        

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceFacadeBase"/> class.
        /// </summary>
        /// <param name="unitOfWorkService">The unit of work service.</param>
        protected ServiceFacadeBase(IUnitOfWorkService unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        //idea: if we have problems with the AOP - since we have the facade anyways we can find out 
        //what method name has been passed here through expression tree (change the argument to expression)
        //and then do a one-line of code authorization here by calling the authorizationbymethod service 
        //then we can drop the AOP arguments and proxy generation

        /// <summary>
        /// Invokes an Application Service function,
        /// optionally committing the UoW afterwards.
        /// <para>
        /// Any exception raised is transforming them into
        /// WCF FaultContract messages.
        /// </para>
        /// </summary>
        /// <typeparam name="TR">The type of the r.</typeparam>
        /// <param name="func">The action.</param>
        /// <param name="commit">if set to <c>true</c> Commits the unit of work after completing the action.</param>
        /// <returns></returns>
        protected virtual TR InvokeServiceFunction<TR>(Func<TR> func, bool commit = true)
        {
            var result = default(TR);
            InvokeServiceAction(() => result = func.Invoke(),commit);
			return result;
        }

        /// <summary>
        /// Invokes an Application Service,
        /// optionally committing the UoW afterwards.
        /// <para>
        /// Any exception raised is transforming them into
        /// WCF FaultContract messages.
        /// </para>
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="commit">if set to <c>true</c> Commits the unit of work after completing the action.</param>
        protected virtual void InvokeServiceAction(Action action, bool commit=true)
        {
            try
            {
                action.Invoke();
                if (commit)
                {
                    _unitOfWorkService.Commit();
                }
            }
            catch (Exception e)
            {
                if (OperationContext.Current == null)
                {
                    throw;
                }
                
				Exception exceptionWithTicket = SoapExceptionFactory.CreateFaultException(true, e);
                XAct.Shortcuts.TraceException(TraceLevel.Error, e, "Back Tier Service Facade Exception");
				
				throw exceptionWithTicket;
            }
        }
    }
}
