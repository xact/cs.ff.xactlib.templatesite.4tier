﻿
namespace App.Core.Application.Initialization
{
    using App.Core.Infrastructure.Initialization;

    /// <summary>
    /// The boostrapping sequence is called from the Application's entry point
    /// (eg: Global.asax ApplicationStart, or a Console Application's Main 
    /// method).
    /// </summary>
    public static class ApplicationInitializer
    {
        /// <summary>
        /// Part of the initialization sequence chain.
        /// <para>
        /// The initialization sequence is as follows:
        /// <code>
        /// * Global.asax 
        ///     * ... invokes AppHostInitializer,
        ///         * ... which first invokes ApplicationInitializer (this class)
        ///             * ... which first invokes InfrastructureInitializer
        ///               where IoC (ie Unity) is initialized with Infrastructure Services...
        ///         * ....now, coming back up...Application layer services are registered...
        ///     * ... now that both Infras and presentation are initialized, AppHostInitializer
        ///       finishes up by registering AppHost level Services.
        /// * Initialization done...
        /// * Integration cycle then starts...
        ///   It too goes down through the layers, and back up, in much the same way... 
        ///   but this time actually using remote services (eg, to load caches, etc).      
        /// </code>
        /// </para>
        /// </summary>
        public static void Execute()
        {
            //Run Lower Layer Steps first:
            InfrastructureInitializer.Execute();
            //Before any initialization specific to this layer.

            //Hint: prefer avoidin initializing Database DbCOntext's in the bootstrap
            //sequence, and wait for the first request for a page that demands it.
            //That will allow for easier installation and health checking by providing
            //pages to that regard that can run even if the database is still not configured
            //correctly.
        }

    }
}