﻿namespace App.Core.Application.Integration
{
    using App.Core.Infrastructure.Integration;

    /// <summary>
    /// Integration happens in a second wave after the
    /// Initialization sequence (see <see cref="ApplicationInitialization"/>
    /// </summary>
    public static class ApplicationIntegrator
    {

        /// <summary>
        /// Executes this instance.
        /// </summary>
        public static void Execute()
        {
            //Run Lower Layer Steps first:
            InfrastructureIntegrator.Execute();
        }


    }
}
