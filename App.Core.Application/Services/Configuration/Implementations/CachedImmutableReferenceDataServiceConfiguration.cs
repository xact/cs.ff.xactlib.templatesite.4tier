﻿namespace App.Core.Application.Services.Configuration
{
    using System.Diagnostics;
    using System.Linq;
    using System;
    using System.Collections.Generic;
    using App.Core.Domain.Models;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure.Services.Configuration;
    using XAct;
    using XAct.Services;

    public class CachedImmutableReferenceDataServiceConfiguration : ICachedImmutableReferenceDataServiceConfiguration
    {
        private readonly ITracingService _tracingService;
        Dictionary<string, Type>  _referenceData = new Dictionary<string, Type>();
          
        public CachedImmutableReferenceDataServiceConfiguration(ITracingService tracingService)
        {
            _tracingService = tracingService;
            Initialize();
        }

        private void Initialize()
        {


            _referenceData.Add(Constants.ReferenceData.ImmutableReferenceData.InterfaceType, typeof(Reference_InterfaceType));
            

            //That's all well and good for Reference Data within Core -- 
            //but what about the other assemblies.

            //All reference data within the app implements: ReferenceDataBase<TId> 
            //So we can use that to look for the classes to register:

            Type[] referenceDataTypes = 
                AppDomain.CurrentDomain.GetTypesImplementingType<IHasReferenceDataBase>(true);

            //Each Type should be decorated with an interface that describes the element tag name:
            foreach (Type referenceDataType in referenceDataTypes)
            {
                WellKnownNameAttribute wellKnownNameAttribute =
                    referenceDataType.GetAttributeRecursively<WellKnownNameAttribute>(true);

                string tag = wellKnownNameAttribute.Name;

                _referenceData.Add(tag, referenceDataType);
            }


            //You can see how the above solves the problem that the following recursive referencing brings up (Core can't reference Module)
            //_referenceData.Add(Constants.ReferenceData.ImmutableReferenceData.ExampleCategory, ExampleCategory);
            //_referenceData.Add(Constants.ReferenceData.ImmutableReferenceData.ExampleType, ExampleType);
            //_referenceData.Add(Constants.ReferenceData.ImmutableReferenceData.Gender, typeof(Reference_Gender));
        
            
        }

        public bool TryGet(string shortTypeName, out Type referenceType)
        {
            bool result = _referenceData.TryGetValue(shortTypeName, out referenceType);
#if DEBUG
            if (!result)
            {
                _tracingService.Trace(TraceLevel.Error, "CachedReferenceDataServiceConfiguration entry not found.");
            }
#endif
            return result;
        }

        public string[] GetKeys()
        {
            return _referenceData.Keys.ToArray();
        }

    }
}