﻿namespace App.Core.Application.Services
{
    using App.Core.Application.Services.Facades;
    using App.Core.Infrastructure;

    public interface ISettingService : IHasAppService, IHasPing
    {

            AppSettings Retrieve();
    }
}