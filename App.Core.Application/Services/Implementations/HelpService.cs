﻿namespace App.Core.Application.Services.Implementations
{
    using System.Globalization;
    using App.Core.Domain.Messages;
    using App.Core.Infrastructure.Services;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="Services.IHelpService"/>
    /// (and therefore <c>IHelpServiceFacade</c>) 
    /// to provide to the Front Tier any Help Resources it may need.
    /// <para>
    /// Wrapped by the Service Facade implementation of 
    /// <c>IHelpServiceFacade</c>
    /// </para>
    /// </summary>
    public class HelpService : Services.IHelpService 
    {
        private readonly IHelpService _helpService;


        /// <summary>
        /// Initializes a new instance of the <see cref="HelpService" /> class.
        /// </summary>
        /// <param name="helpService">The resource service.</param>
        public HelpService(IHelpService helpService)
        {
            _helpService = helpService;
        }


        public HelpEntrySummary GetHelpEntryByKey(string key, CultureInfo cultureInfo = null)
        {
            
            HelpEntrySummary result = _helpService.GetHelpEntryByKey(key, cultureInfo);

            return result;
        }

    }


}