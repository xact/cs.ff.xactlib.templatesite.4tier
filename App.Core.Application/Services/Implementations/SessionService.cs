﻿namespace App.Core.Application.Services.Implementations
{
    using App.Core.Application.Services.Facades.Implementations;
    using App.Core.Constants;
    using System;
    using System.Diagnostics;
    using App.Core.Domain.Messages;
    using App.Core.Domain.Models;
    using App.Core.Domain.Security;
    using App.Core.Infrastructure.Services;
    using XAct.Messages;
    using XAct.Services;
    using XAct;

    public class SessionService : Services.ISessionService
    {
	    private readonly IDateTimeService _dateTimeService;
	    private readonly Infrastructure.Services.ISessionService _sessionService;


        /// <summary>
        /// Initializes a new instance of the <see cref="SessionService" /> class.
        /// </summary>
        /// <param name="sessionService">The session service.</param>
        public SessionService
            (
                IDateTimeService dateTimeService,
                Infrastructure.Services.ISessionService sessionService
            )
        {
            _dateTimeService = dateTimeService;
            _sessionService = sessionService;
        }

        public string Ping()
        {
            return _sessionService.Ping();
        }

        /// <summary>
        /// Instantiates the Session entity and identifies with an unique identifier (app session token).
        /// The session entity and passed SignInInfo entity are then persisted in the database, Session entity is cached.
        /// <para>
        /// If Failed (due to Provider code being incorrect),
        /// returns Message containing Error code.
        /// </para>
        /// <para>
        /// <para>
        /// Called only once, from the Front Tier,
        /// on return from an SSO.
        /// One example being from within SetAppSamlIPrincipal handler,
        /// which calls the front IAccountService, which calls the front ISessionService
        /// which calls the back <see cref="SessionServiceFacade.CreateSession"/>, which calls this method implementation.
        /// </para>
        /// </summary>
        /// <param name="identityBase64">The identity base64.</param>
        /// <param name="signInInfo">The session log.</param>
        /// <returns>
        /// App Session Token
        /// Errors:
        /// -...
        /// </returns>
        public Response<SessionResponse> CreateSession(string identityBase64, SignInInfo signInInfo)
        {
            Response<SessionResponse> response;
            
            //FIX: 
            response = new Response<SessionResponse>();
            
            AppIdentity identity = AppIdentity.DeserializeFromBase64(identityBase64); //deserialize identity coming from front tier (ESSA)

            //if (provider == null) //if provider is not in the database return error
            //{
            //    //Create alternate/replacement Response:
            //    //response = new Response<Session>();

            //    //With reason:
            //    response.AddMessage(MessageCodes.Security_InvalidOrganisationForUser_515);

            //    return response;
            //}


			////Check live date once you have provider:
			//if (provider.GoliveDateTime == null || provider.GoliveDateTime.Value.Date > _dateTimeService.Now.Date)
			//{
			//	//Create alternate/replacement Response:
			//	response = new Response<Session>();

			//	//With reason:
			//	response.AddMessage(MessageCodes.General_NSICannotBeAccessed_1283);

			//	return response;
			//}

	        //Response r2 = _providerService.IsEnabledForInterfaceX(provider,signInInfo.InterfaceTypeFK);

            //if (r2.Messages.Count > 0)
            //{
            //    r2.TransferProperties(response);
            //    return response;
            //}

            //attach provider information on the deserialized identity for later use (auditing etc).
            //identity.ProviderFK = provider.Id;
            //identity.ProviderName = provider.Name;

            //identity.OrganisationSummary.SetValue("InterfaceType", (int) signInInfo.InterfaceTypeFK);



            //signInInfo.OrganisationId = provider.Code;

            // Instantiates the Session entity and identifies it with an unique identifier (App session token).
            // The session entity and passed SignInInfo entity are then persisted in the database
            // Session entity is cached.
            // Will always ge a response (never null):
            // And Response.Success will be marked as Success.
            response = _sessionService.CreateSession(identity.SerializeToBase64() //reserialize - now with the provider name and fk
                                                         , signInInfo);


            if (response.Success)
            {
                response.Data.OrganisationSummary = identity.OrganisationSummary;
            }
            //Should be already right...
            Debug.Assert(response.Success);

            return response;
        }



        /// <summary>
        /// Given an existing valid (non-default) <see cref="Guid" />,
        /// updates the associated <see cref="Session" />'s LastAccessedDateTime
        /// and optionally (depending on <paramref name="returnSession" />)
        /// returns the updated <see cref="Session" />.
        /// <para>
        /// Returns a MessageCodes.SECURITY_TIMEOUT Message if timed out.
        /// </para>
        /// </summary>
        /// <param name="sessionToken"></param>
        /// <param name="returnSession"></param>
        /// <returns>
        /// The <see cref="Session" />
        /// </returns>
        public Response<SessionResponse> UpdateSessionLastAccessedDateTime(Guid sessionToken, bool returnSession = false)
        {
            return _sessionService.UpdateSessionLastAccessedDateTime(sessionToken, returnSession);
        }

        /// <summary>
        /// Deletes the session.
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        public void DeleteSession(Guid sessionToken)
        {
            _sessionService.DeleteSession(sessionToken);
        }
		
        /// <summary>
        /// Tries to get the session from cache. If its not in the cache, get's it from the database.
        /// <para>
        /// Note that the <see cref="Session"/> object's <c>LastAccessedDateTime</c> is not guaranteed to still be valid.
        /// </para>
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <returns></returns>
        public Session GetSession(Guid sessionToken)
        {
            Session result = _sessionService.GetSession(sessionToken);
            return result;
        }

		public bool DoesSessionExist(Guid sessionToken)
		{
			return _sessionService.DoesSessionExist(sessionToken);
		}

    }
}