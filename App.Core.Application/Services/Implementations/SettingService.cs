
namespace App.Core.Application.Services.Implementations
{
    using System;
    using System.Globalization;
    using App.Core.Infrastructure;
    using App.Core.Infrastructure.Services;
    using App.Core.Infrastructure._DEBUG.LIB;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Note how we are Binding Application level services it to *both* 
    /// the Application Service *and* ApplicationFacade Service.
    /// </internal>
    public class SettingService : ISettingService
    {
        private readonly IApplicationSettingsService _applicationSettingsService;


        public SettingService(IApplicationSettingsService applicationSettingsService)
        {
            _applicationSettingsService = applicationSettingsService;
        }

        public string Ping()
        {
            return DateTime.UtcNow.ToString(CultureInfo.InvariantCulture);
        }

        public AppSettings Retrieve()
        {
	        try
	        {
                return _applicationSettingsService.AppSettings;

	        }
	        catch (System.Exception e)
	        {
		        if (System.ServiceModel.OperationContext.Current == null)
		        {
			        throw;
		        }
		        throw SoapExceptionFactory.CreateFaultException(true, e);
	        }


        }
    }
}