﻿//namespace App.Core.Presentation.Maps.Resolvers
//{
//    using System.Diagnostics;
//    using App.Core.Domain.Entities;
//    using App.Core.Infrastructure.Maps.CustomResolvers;
//    using XAct;

//    /// <summary>
//    /// Converts a Gender Enum value to a Resource packed string.
//    /// </summary>
//    public class IHasGenderFK_ResourceString_CustomResolver : ClientCustomResolverBase<IHasGenderFK, string>
//    {


//        /// <summary>
//        /// Converts the enum value (an int) to a string, 
//        /// that was sourced from the Resource cache.
//        /// </summary>
//        /// <param name="source">The source.</param>
//        /// <returns></returns>
//        protected override string ResolveCore(IHasGenderFK source)
//        {
//            Gender gender = (Gender) source.GenderFK;

//            string result = gender.ToResource(_ClientEnvironmentService.ClientUICulture);

//            Debug.Assert(!result.IsNullOrEmpty());

//            return result;
//        }
//    }


//    //public class IHasAltNameVerificationMethodFKCustomResolver : CustomResolverBase<IAltNameVerificationMethodFK, string>
//    //{


//    //    /// <summary>
//    //    /// Converts the enum value (an int) to a string, 
//    //    /// that was sourced from the Resource cache.
//    //    /// </summary>
//    //    /// <param name="source">The source.</param>
//    //    /// <returns></returns>
//    //    protected override string ResolveCore(IAltNameVerificationMethodFK source)
//    //    {
//    //        AltNameVerificationMethod altNameVerificationType = (AltNameVerificationMethod)source.AltNameVerificationMethod;

//    //        string result = altNameVerificationType.ToResource(ClientEnvironmentService.ClientUICulture);

//    //        Debug.Assert(!result.IsNullOrEmpty());

//    //        return result;
//    //    }
//    //}
//}