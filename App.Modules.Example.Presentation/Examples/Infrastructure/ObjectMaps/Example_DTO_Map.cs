﻿namespace App.Modules.Example.Presentation.Examples.Infrastructure.ObjectMaps
{
    using App.Core.Infrastructure.Maps;
    using App.Modules.Example.Domain.Entities;
    using App.Modules.Example.Domain.ViewModels;
    using AutoMapper;

    class Example_DTO_Map : MapBase<Example, ExampleViewModel>
    {

        public override void Initialize()
        {
            IMappingExpression<Example, ExampleViewModel> map = base.CreateMap<Example, ExampleViewModel>();
        }
    }
}
