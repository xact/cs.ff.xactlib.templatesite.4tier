﻿namespace App.Modules.Example.Presentation.Examples.Infrastructure.Services.Clients.Implementations
{
    using System;
    using App.Core.Infrastructure.Services;
    using App.Modules.Example.Domain.Entities;
    using App.Modules.Example.Domain.Models;
    using App.Modules.Example.Domain.ViewModels;
    using App.Modules.Example.Example.ApplicationFacade.Services;
    using XAct.Messages;
    using XAct.Services;

    public class ExampleService : IExampleService
    {
        private readonly ICultureSpecificValidatedQueryResponseService _cultureSpecificValidatedQueryResponseService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExampleService" /> class.
        /// </summary>
        public ExampleService(
            ICultureSpecificValidatedQueryResponseService cultureSpecificValidatedQueryResponseService)
        {
            _cultureSpecificValidatedQueryResponseService = cultureSpecificValidatedQueryResponseService;
        }


        public Response<ExampleViewModel> GetSingleForViewing(Guid id)
        {

            return
                _cultureSpecificValidatedQueryResponseService
                    .Query<IExampleServiceFacade, Example, ExampleViewModel>(service => service.GetSingle(id));
        }

        public Response<NullableExample> GetSingleForEditing(Guid id)
        {

            return
                _cultureSpecificValidatedQueryResponseService
                    .Query<IExampleServiceFacade, Example, NullableExample>(service => service.GetSingle(id));
        }

        public Response Add(NullableExample example)
        {

            return
                _cultureSpecificValidatedQueryResponseService
                    .Query<IExampleServiceFacade>(service => service.Add(example));
        }


        public Response Update(NullableExample example)
        {
            // Updates are the most interesting operations,
            // and several design patterns come into play.
            // * First of all, the request argument should be a nullable version
            //   of the Entity (reason: not only is it that HTML's protocol does not send back checkboxes that are not set,
            //   but it is a common design pattern to have remote api client's to submit only what's changed, as it saves
            //   bandwidth.
            // * Secondly, the Application layer is the orchestrator that has to retrieve the existing record, and merge
            //   the values over the existing record.
            // * Thirdly, it has to validate the merged data before it resubmits it for saving.
            // * Finally, it has to invoke the Domain RepositoryService to persist it.
            // From the above one can conclude that the Nullable version of the Entity has to be known by the ApplicationService
            // and therefore the AppFacade as well -- hence why we don't do the mapping here in the Presentation layer.

                return
                    _cultureSpecificValidatedQueryResponseService
                        .Query<IExampleServiceFacade>(service => service.Update(example));

        }


        public Response Delete(Guid id)
        {
            return
                _cultureSpecificValidatedQueryResponseService
                    .Query<IExampleServiceFacade>(service => service.Delete(id));

        }
    }
}