﻿namespace App.Modules.Example.Presentation.Examples.Infrastructure.Services.Clients
{
    using System;
    using App.Core;
    using App.Modules.Example.Domain.Models;
    using App.Modules.Example.Domain.ViewModels;
    using XAct.Messages;

    public interface IExampleService : IHasAppService
    {

        //PagedResponse<ExampleSummary> PagedSearch(PagedRequest<ExampleSearchTerms> pagedRequest);
        
        /// <summary>
        /// Gets a single <see cref="ExampleViewModel"/>
        /// suitable for Viewing as a single record.
        /// </summary>
        Response<ExampleViewModel> GetSingleForViewing(Guid id);

        Response<NullableExample> GetSingleForEditing(Guid id);

        Response Add(NullableExample example);
        Response Delete(Guid id);
        Response Update(NullableExample example);
    }

}
