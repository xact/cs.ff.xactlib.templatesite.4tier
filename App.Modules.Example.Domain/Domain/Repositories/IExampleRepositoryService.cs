﻿namespace App.Modules.Example.Domain.Repositories
{
    using System;
    using App.Modules.Example.Domain.Entities;
    using App.Modules.Example.Domain.Messages;

    /// <summary>
	/// 
	/// <para>
	/// Note that it is a stateless Service: the 
	/// implementation in turn users 
	/// KW_APP.KW_MODULE.Infrastructure.IRepositoryService
	/// u
	/// </para>
	/// </summary>
    public interface IExampleRepositoryService //: IRepository<Example>
	{

        string Ping();

        Example GetSingle(Guid id);

        Example GetSingle(string code);        

        ExampleSummary[] Search(string term, XAct.Messages.IPagedQuerySpecification pagedQuerySpecification);

        ExampleSummary[] Search(string term);

        void Add(Example example);

        void Update(Example example);

        void Delete(Example example);

        void Delete(Guid id);

        void Delete(string code);

    }
}
