
* MOE Wifi/Network:
    * In case you're still at MOE:
	    * We're using Varun's machine to host databases.
		* But the network doesn't resolve Named machines. 
		* So, use notepad.exe (started as Admin) to edit c:\Windows\System32\drivers\hosts file,
		* and map 192.168.1.182 to VARUNIMW7PC
* Download from TFS (Obviously, you've done that if you are reading this....)
* Doesn't hurt to do a Get Latest Version (GLV) again to ensure you have latest.
* Define the Start Projects:
    * Select Solution, Properties, Startup Project, Select (in the following order):
	    *  App.WA.Back.AppHost.Web
		*  App.WA.Front.AppHost.Web
* Build. If you have Nuget configured correctly, it will download Nuget packages.
    * If it hickups, do it a second time.
* Customize Application Config files for your Development Station.
    * Important: as common, we use the *.config files in the AppHost projects.
	  But we also use NConfig to 'merge/crush' over those settings any custom
	  settings per your dev station.
    * In Back.AppHost:
		* find the /Config/MachineSpecificSettings/ folder,
		* ensure DisplayAllFiles is on to see any files on the harddrive that
		 are not part of the solution
		* select someone elses custom config file (eg: NSI04--the build server)
		* copy it, 
		* rename it to your machine's name.
		* ensure it is marked as copy-to target
		* ensure it is checked in
		* then remove it from the project/solution (we don't want it packaged
		   to the client's target)
		* Edit the file to customize the connection string, etc to your needs.
			* More on that somewhere....
	* Repeat operations for Front.AppHost:
	    * Should be relatively minor as front doesn't need much custom work
		  per developer.
* Customzie Application Test Config files.
    * We use the NConfig for the Tests as well, so you'll need to provide
	  a named YOUR_MACHINE_NAME.app.config file in the following locations:
        * AP.WA.Core.Back.Tests.Common
		    * \Config
		    * \App
        * App.WA.Core.Front.Tests.Common
		    * \Config
			* \App
        * App.WA.Core.Shared.Tests.Common
		    * \Config
			* \App
* Customize Framework Test Config files.
    * We use the NConfig for the Tests as well, so you'll need to provide
	  a named YOUR_MACHINE_NAME.app.config file in the following locations:
        * App.Core.Back.Tests.Common
		    * \Config
        * App.Core.Front.Tests.Common
		    * \Config
        * App.Core.Shared.Tests.Common
		    * \Config
	* Each file has to be marked with "Copy to Destination If Newer"
	* This way, YOUR_MACHINE.app.config gets copied to the Tests.Common bin 
	  directory, and from there copied to the Bin\Debug directory 
	  of the App.Core.Front.Tests\bin\debug and App.FW.Back.Tests\bin\debug
	  so that when the unit tests run, NConfig runs, and finds the relevant
	  parts...

Done all that? Good...

* Get the unit tests runing.
* When the unit tests run, run the app, by hitting (F5).
    * If you did the Startup projects in the order mentioned, you'll get two
	  browser tabs opening (Back, *then* Front)
	* You might run into Assembly conflicts (that's the nature of Nuget
	  downloading the Latest package, which might be later than the ones
	  in the web.config). 
	  You'll need to do Assembly Redirects. Ask if you have never done that.
    