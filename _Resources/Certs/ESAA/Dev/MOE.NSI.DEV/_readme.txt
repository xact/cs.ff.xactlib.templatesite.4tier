Certificates were built using:


echo Step 1: Create Self-Signed Cert suitable for signage, but not SSL.
makecert -n "CN=MOE.NSI.DEV" -b 01/01/2013 -e 01/01/2020 -sky exchange -r -pe -sv MOE.NSI.DEV.pvk MOE.NSI.DEV.cer
echo Step 2: Combining Server *.cer and *.pvk into a *.pfx that can be installed on Server...
pvk2pfx.exe -pvk MOE.NSI.DEV.pvk -spc MOE.NSI.DEV.cer -pfx MOE.NSI.DEV.pfx



To install:

* Start -> Run ... "MMC.exe"
* Files -> Add/Remove Snap In...
* Select Certificates Snapin
* Select "Computer Account"
* Select "Localmachine"

* Install the Root Certificate:
    * Console Root -> Trusted Root Certification Authority -> Certificates
    * Right-CLick on Certificates
    * Select "All Tasks/Import..."
    * Wizard... Next...Browser for File... 
        * Filter by * so you can see it... then select MOE.NSI.DEV.pfx file
        * Continue...when asked for a password, it's either blank, or the Fancy Mocatad password (ask if you don't know what that means)
    * When the Root CA is installed, time to do similar in Personal
* Install the Personal Certificate:
    * Console Root -> Personal -> Certificates
    * Right-CLick on Certificates
    * Select "All Tasks/Import..."
    * Wizard... Next...Browser for File... 
        * Filter by * so you can see it... then select MOE.NSI.DEV.pfx file
        * Continue...when asked for a password, it's either blank, or the Fancy Mocatad password (ask if you don't know what that means)
* Give access to Personal Certificate:
    * Select the installed cert
    * Right-click
    * Select "All Tasks/ Manage Private Keys"
    * Give full rights to the account that is running the website.

 