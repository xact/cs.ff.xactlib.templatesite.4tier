﻿
namespace App.Modules.Example.Data.PersistenceMaps
{
    using System.ComponentModel.DataAnnotations.Schema;
    using App.Core.Domain.Entities;
    using App.Core.Domain.Models;
    using App.Core.Infrastructure.Data.Maps;


    public class Gender_PersistenceMap : ReferenceDataPersistenceMapBase<Reference_Gender, Gender>
    {
        public Gender_PersistenceMap() : 
            base("Gender", DatabaseGeneratedOption.None) 
        {
        }
    }
}
