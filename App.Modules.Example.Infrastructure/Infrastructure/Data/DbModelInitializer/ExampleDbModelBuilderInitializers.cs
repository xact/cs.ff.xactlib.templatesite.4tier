﻿namespace App.Core.Infrastructure.Data.Mapping.DbInitializers
{
    using System.Data.Entity;
    using App.Core.Domain.Entities;
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Implementation of <see cref="IDbModelBuilder"/>
    /// that will create Maps and Relationships for <see cref="Example"/>
    /// <see cref="ExampleTag"/>, <see cref="ExampleCategory"/>, etc.
    /// </summary>
    public class ExampleDbModelBuilderInitializers : XAct.Data.EF.CodeFirst.IDbModelBuilder
    {
        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Already added, as the following implement EntityTypeConfigurationBase<Example>
            ////--------------------------------------------------
            //modelBuilder.Configurations.Add(new ExampleMap());
            ////--------------------------------------------------
            //modelBuilder.Configurations.Add(new ExampleTypeMap());
            ////--------------------------------------------------
            //modelBuilder.Configurations.Add(new ExampleCategoryMap());
            ////--------------------------------------------------
            //modelBuilder.Configurations.Add(new ExampleTagMap());
            ////--------------------------------------------------
        }
    }
}
