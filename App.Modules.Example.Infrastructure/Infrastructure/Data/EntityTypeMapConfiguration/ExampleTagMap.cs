namespace App.Modules.Example.Infrastructure.Data.EntityTypeMapConfiguration
{
    using App.Core.Infrastructure.Data.Maps;
    using App.Modules.Example.Domain.Entities;

    public class ExampleTagMap : EntityTypeConfigurationBase<ExampleTag>
    {
        public ExampleTagMap()
        {
            int colOrder = 0;
            int indexMember = 1;//1 based.

            this.ToTable("ExampleTag");

            this
                .HasKey(m => m.Id);

            this
                        .Property(m => m.Name)
                        .HasMaxLength(128)
                        .IsRequired()
            .HasColumnOrder(colOrder++)
            ;
        }
    }
}