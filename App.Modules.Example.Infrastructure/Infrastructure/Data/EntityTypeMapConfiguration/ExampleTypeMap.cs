
namespace App.Modules.Example.Infrastructure.Data.EntityTypeMapConfiguration
{
    using App.Core.Infrastructure.Data.Maps;
    using App.Modules.Example.Domain.Entities;

    public class ExampleTypeMap : EntityTypeConfigurationBase<ExampleType>
    {
        public ExampleTypeMap()
        {
            int colOrder = 0;
            int indexMember = 1;//1 based.

            this.ToTable("ExampleType");

            this
                .HasKey(m => m.Id);
            this
                        .Property(m => m.Code)
                        .HasMaxLength(64)
                        .IsRequired()
            .HasColumnOrder(colOrder++)
            ;
            this
                        .Property(m => m.Enabled)
                        .IsRequired()
            .HasColumnOrder(colOrder++)
            ;
            this
                        .Property(m => m.Order)
                        .IsRequired()
            .HasColumnOrder(colOrder++)
            ;
            this
                        .Property(m => m.ResourceFilter)
                        .HasMaxLength(128)
                        .IsOptional()
            .HasColumnOrder(colOrder++)
            ;
            this
                        .Property(m => m.Text)
                        .HasMaxLength(128)
                        .IsRequired()
            .HasColumnOrder(colOrder++)
            ;
        }
    }
}