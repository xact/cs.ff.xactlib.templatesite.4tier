namespace App.Modules.Example.Infrastructure.Data.EntityTypeMapConfiguration
{
    using App.Core.Infrastructure.Data.Maps;
    using App.Modules.Example.Domain.Entities;

    public class ExampleCategoryMap : EntityTypeConfigurationBase<ExampleCategory>
    {
        public ExampleCategoryMap()
        {
            int colOrder = 0;
            int indexMember = 1;//1 based.


            this.ToTable("ExampleCategory");

            this
                .HasKey(m => m.Id);
            this
                .Property(m => m.Enabled)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.ResourceFilter)
                .HasMaxLength(128)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Text)
                .HasMaxLength(64)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Description)
                .HasMaxLength(64)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

        }
    }
}