namespace App.Modules.Example.Infrastructure.Data.EntityTypeMapConfiguration //App.Core.Domain.Models.Mapping
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using App.Core.Infrastructure.Data.Maps;
    using App.Modules.Example.Domain.Entities;

    public class ExampleMap : EntityTypeConfigurationBase<Example>
    {
        public ExampleMap()
        {

            int colOrder = 0;
            int indexMember = 1; //1 based.


            this.ToTable("Example");

            // Primary Key
            this.HasKey(t => t.Id);

            //this.Property(x => x.Name)
            //    .HasMaxLength(64)
            //    .HasColumnOrder(2)
            //    .HasColumnName("Foo");

            this
                .Property(m => m.Id)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(x => x.Code)
                .IsRequired()
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(new IndexAttribute("IX_Code", indexMember++) {IsUnique = true}))
                .HasColumnOrder(colOrder++)
                ;



            // Relationships 
            this.HasRequired(t => t.Category)
                .WithMany()
                .HasForeignKey(t => t.CategoryFK)
                .WillCascadeOnDelete(false);

            this.HasRequired(t => t.Type)
                .WithMany()
                .HasForeignKey(t => t.TypeFK)
                .WillCascadeOnDelete(false)
                ;




            //this.HasMany(m=>m.Tags)
            //    .WithMany(t=>t.Examples)
            //    .Map(
            //        mc =>
            //            {
            //                mc.ToTable("ExampleJoinExampleTag");
            //                mc.MapLeftKey("ExampleId");
            //                mc.MapRightKey("ExampleTagId");
            //            });




        }
    }
}
