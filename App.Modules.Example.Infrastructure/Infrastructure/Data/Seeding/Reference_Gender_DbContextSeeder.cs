﻿using XAct.Data.EF.CodeFirst.Seeders.Implementations;

namespace App.Core.Infrastructure.Data.Seeders.Domain
{
    using System.Data.Entity;
    using App.Core.Domain.Entities;
    using App.Core.Domain.Models;
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;
    using XAct.Library.Settings;


    /// <summary>
    /// Domain specific Reference Data.
    /// <para>
    /// Note how the Initializer's Order is set to higher than default value, so that
    /// it runs before <see cref="SampleStudentDbContextSeeder"/>.
    /// </para>
    /// </summary>
    public class ReferenceGenderDbContextSeeder : DbContextSeederBase<Reference_Gender>
    {

        public override void SeedInternal(DbContext dbContext)
        {

        }

        public override void CreateEntities()
        {
            
            //    new ReferenceDataConfiguration<Reference_Gender, Gender>(dbContext, x => x.Id, Constants.ReferenceData.ImmutableReferenceData.Gender)

            //this.InternalEntities.Add( Gender.Male, null, "M", 2)
            //    .AddOrUpdate(Gender.Female, null, "F", 1)
            //    .AddOrUpdate(Gender.Unknown, null, "U", 3)
        }
    }
}