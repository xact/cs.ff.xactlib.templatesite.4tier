﻿using System.Data.Entity;
using XAct.Data.EF.CodeFirst.Seeders.Implementations;

namespace App.Core.Infrastructure.Data.Seeders.Domain
{
    using App.Modules.Example.Domain.Entities;
    using XAct.Data.EF.CodeFirst;

    public class ExampleDbContextSeeder : DbContextSeederBase<Example>
    {

        public ExampleDbContextSeeder(ReferenceGenderDbContextSeeder referenceGenderDbContextSeeder) :
            base(new IDbContextSeeder[] {referenceGenderDbContextSeeder})
        {
            
        }

        public override void SeedInternal(DbContext dbContext)
        {
            this.SeedInternalHelper(dbContext,false);

        }

        public override void CreateEntities()
        {
            //throw new System.NotImplementedException();
        }
    }
}