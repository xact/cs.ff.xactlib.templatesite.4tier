﻿using System.Data.Entity;
using XAct.Data.EF.CodeFirst.Seeders.Implementations;

namespace App.Core.Infrastructure.Data.Seeders.Domain
{
    using App.Modules.Example.Domain.Entities;
    using XAct.Data.EF.CodeFirst;

    public class ExampleTagDbContextSeeder : DbContextSeederBase<ExampleTag>
    {

        public ExampleTagDbContextSeeder() :
            base(new IDbContextSeeder[] { })
        {

        }

        public override void SeedInternal(DbContext dbContext)
        {


            //throw new System.NotImplementedException();
        }

        public override void CreateEntities()
        {
            //throw new System.NotImplementedException();
        }
    }
}