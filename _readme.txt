### Publish-Dev.cmd ###
This is the Command line for running Publish.proj from the command line, 
for testing purposes, without needing Visual Studio to compile the 
*.sln

It basically is doing the following (which is useful to understand, in 
order to better understand how Publish.proj works, and what vars it's 
looking for.

%windir%\Microsoft.NET\Framework\v4.0.30319\msbuild.exe 
  Publish.proj 
    /fl 
	/p:DeployOnBuild=true;PublishProfile=Dev;DbMigrationsDeployDb=true;DbMigrationsRollBackDb=true


You'll need to install the MSBuild Extension Pack on your local machine.
https://msbuildextensionpack.codeplex.com/



