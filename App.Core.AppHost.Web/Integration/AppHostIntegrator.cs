﻿using System.Reflection;
using App.Core.AppHost.Web.Integration.Steps.PostIoC;
using XAct.Commands;

namespace App.Core.AppHost.Web.Integration
{
    public class AppHostIntegrator
    {
        public static void Execute()
        {

            new IntegrateLowerLayersIntegrationStep().Execute();
        }
    }

}