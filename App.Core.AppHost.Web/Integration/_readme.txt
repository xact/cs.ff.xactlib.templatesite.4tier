﻿The reason we go to the trouble of using interfaces
is that once the IoC Container has scanned/been initialized 
in a lower level (Integration), one can use DI in the constructor
of each step, whereas before, we have to use the Brittle mechanism 
of using ServiceLocator.
