﻿using App.Core.Application.Integration;
using XAct;

namespace App.Core.AppHost.Web.Integration.Steps.PostIoC
{
    public class IntegrateLowerLayersIntegrationStep : IHasIntegrationStep
    {
        public void Execute()
        {
            ApplicationIntegrator.Execute();
        }
    }
}