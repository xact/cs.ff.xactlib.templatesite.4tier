﻿

using App.Core.AppHost.Web.Initialization.Steps.PostIoC;

namespace App.Core.Back.AppHost.Initialization
{
    using App.Core.AppHost.Web.Initialization.Steps.PreIoC;


    public static class AppHostInitializer
    {

        /// <summary>
        /// Part of the initialization sequence chain.
        /// <para>
        /// The initialization sequence is as follows:
        /// <code>
        /// * Global.asax 
        ///     * ... invokes AppHostInitializer (this class),
        ///         * ... which first invokes ApplicationInitializer
        ///             * ... which first invokes InfrastructureInitializer
        ///               where IoC (ie Unity) is initialized with Infrastructure Services...
        ///         * ....now, coming back up...Application layer services are registered...
        ///     * ... now that both Infras and presentation are initialized, AppHostInitializer
        ///       finishes up by registering AppHost level Services.
        /// * Initialization done...
        /// * Integration cycle then starts...
        ///   It too goes down through the layers, and back up, in much the same way... 
        ///   but this time actually using remote services (eg, to load caches, etc).      
        /// </code>
        /// </para>
        /// </summary>
        public static void Execute()
        {



            //----------------------------------------------------------
            //Steps prior to IoC being available:
            new HardReferencesToEnsureDbMigrationOccursOnCiServer().Execute();
            new PreInitializationStep().Execute();
            new InitializeLowerLayersInitializationStep().Execute();
            //----------------------------------------------------------
            //Now that lower layers (ie Front Infrastructure) is initialized
            //IoC works, etc.
            //Continue....
            //new RegisterAllAreasInitializationStep().Execute(); // Must occur before MVC routes
            new RegisterWebAPIRoutesInitializationStep().Execute(); // Must occur before MVC routes
            new RegisterMvcRoutesInitializationStep().Execute();
            new RegisterReplacementMVCFactoriesInitializationStep().Execute();
            new MvcMiscInitializationStep().Execute();
            new RegisterReplacementWebAPIFactoriesInitializationStep().Execute();
            //new RegisterWebAPIAreaRouterInitializationStep().Execute();
            //new ConnectToBackServiceInitializationStep().Execute();
            //TODO:new RegisterWebAPIRoutesInitializationStep().Execute(); // Must occur before MVC routes
            //----------------------------------------------------------

            XAct.Settings.IApplicationSettingsService applicationSettingsService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Settings.IApplicationSettingsService>();

            var check = applicationSettingsService.Current;
        }

    }
}
