﻿Initialization of the app is the very first action in the whole app.

As there are several Layers within the Back Tier, and not wanting
to end up with a Monolithic design (where the Top layer knows about 
things happing two layers down), 
the AppHostInitializer first invokes the ApplicationInitializer
The ApplicationInitializer first invokes the InfrastructureInitializer


The Plant.UML Sequence diagram is defined as the following:

 
B.AppHostInitializer -> B.ApplicationInitializer: Execute()
ApplicationInitializer -> B.InfrastructureInitialer: Execute()
B.InfrastructureInitializer --> B.ApplicationInitialier: (void)
B.ApplicationInitialier --> B.AppHostInitializer: (void)



Once the Initialization sequence is complete, the Integration sequence
begins:

B.AppHostIntegrator -> B.ApplicationIntegrator: Execute()
ApplicationIntegrator -> B.InfrastructureInitialer: Execute()
B.InfrastructureIntegrator --> B.ApplicationInitialier: (void)
B.ApplicationInitialier --> B.AppHostIntegrator: (void)
