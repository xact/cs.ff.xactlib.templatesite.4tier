﻿using App.Core.Application.Initialization;
using XAct;

namespace App.Core.AppHost.Web.Initialization.Steps.PreIoC
{
    public class InitializeLowerLayersInitializationStep : IHasInitializationStep
    {

        /// <summary>
        /// Part of the initialization sequence chain.
        /// <para>
        /// The initialization sequence is as follows:
        /// <code>
        /// * Global.asax 
        ///     * ... invokes AppHostInitializer,
        ///         * ... which first invokes ApplicationInitializer
        ///             * ... which first invokes InfrastructureInitializer
        ///               where IoC (ie Unity) is initialized with Infrastructure Services...
        ///         * ....now, coming back up...Application layer services are registered...
        ///     * ... now that both Infras and presentation are initialized, AppHostInitializer
        ///       finishes up by registering AppHost level Services.
        /// * Initialization done...
        /// * Integration cycle then starts...
        ///   It too goes down through the layers, and back up, in much the same way... 
        ///   but this time actually using remote services (eg, to load caches, etc).      
        /// </code>
        /// </para>
        /// </summary>
        public void Execute()
        {
            ApplicationInitializer.Execute();
        }
    }
}