﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using XAct;

namespace App.Core.AppHost.Web.Initialization.Steps.PreIoC
{
    public class PreInitializationStep : IHasInitializationStep
    {

        public void Execute()
        {

            //As per 
            //* http://www.asp.net/signalr/overview/signalr-20/hubs-api/hubs-api-guide-server
            //* 
            //Register SignalR routes before any other routes.
            //But this is obsolete: 
            //RouteTable.Routes.MapHubs();
            //But still having troubles (interfering with WCF endpoints)

            AreaRegistration.RegisterAllAreas();

            //4.0
            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            //5.0
            //GlobalConfiguration.Configure(WebApiConfig.Register);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            //RouteConfig.RegisterRoutes(RouteTable.Routes);

        }
    }
}