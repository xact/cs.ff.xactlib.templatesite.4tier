﻿namespace App.Core.AppHost.Web.Initialization.Steps.PreIoC
{
    using App.Core.Infrastructure.Services.Implementations;
    using XAct;
    using System;
    using AutoMapper.Internal;
    
    /// <summary>
    /// 
    /// </summary>
    internal class HardReferencesToEnsureDbMigrationOccursOnCiServer : IHasInitializationStep
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HardReferencesToEnsureDbMigrationOccursOnCiServer"/> class.
        /// </summary>
        public HardReferencesToEnsureDbMigrationOccursOnCiServer()
        {
        }


        public void Execute()
        {
            //This method will never be called, but creates a hard link to classes
            //within problematic assemblies that are only referred to indirectly, via
            //reflection.
            //Due to the way that MSBuild optimises, it leaves behind assemblies it sees no 
            //hard link to...the omission of causing failed WebDeploy packages.
            //Symptoms are:
            //Builds locally, but requests a Code Migration. When you deploy to Build Server
            //Publish.proj complains that model is not in sync. What is happening is that
            //Build server is seeing all code migrations. But not seeing Assembly that would
            //cause the migration (ie, from its wrong point of view, it has "one code migration too far").

            //This is a work around.
            Type[] hardReferences =
                new Type[]
                    {
                        typeof(App.Core.Infrastructure.Services.Implementations.TracingService) //Hard link to something in App.Core.Infrastructure
                        //typeof(HelpDbModelBuilder),
                        //typeof(ResourceDbModelBuilder),
                        //typeof(SecurityDbModelBuilder),
                        //typeof(MessagingDbModelBuilder),
                        //typeof(EFSettingsDbModelBuilder),
                        //typeof(NVelocityTemplateService),
                        //typeof(PersistedFileDbModelBuilder),
                        //typeof(CounterDbModelBuilder),
                        //typeof(MarkdownDeepMarkdownService),
                        //typeof(NullableConverterFactory),
                        //                        typeof(UserDbModelBuilder),
                        ////Not sure this is going to be a strong enough bond to get 
                        ////typeof(SchedulingDbModelBuilder), across:
                        //typeof(FWHardReferenceToEnsureDbMigrationOccursOnCiServer),

                    };

            var x = hardReferences.ToString();

			// AutoMapper - Ensure that their 'Net4' DLL is *used* in the code.
			// -- This is required to have the correct DLLs packaged with a *release* build
			// -- The class references above aren't enough (they only work for debug mode releases)
			var trickDirectReferenceToAutoMapperNet4 = new NullableConverterFactory();
			var trickUtiliseAutoMapper4 = trickDirectReferenceToAutoMapperNet4.IsEnum();
        }
    }
}
