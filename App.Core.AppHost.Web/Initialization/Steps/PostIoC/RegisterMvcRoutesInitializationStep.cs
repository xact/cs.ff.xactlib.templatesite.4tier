﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using App.Core.AppHost.Web.App_Start;
using XAct;


namespace App.Core.AppHost.Web.Initialization.Steps.PostIoC
{
    //[DefaultBindingImplementation(typeof(IRegisterMvcRoutesInitializationStep))]
    public class RegisterMvcRoutesInitializationStep : IHasInitializationStep//: IRegisterMvcRoutesInitializationStep
    {
        public void Execute()
        {

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);

            AuthConfig.RegisterAuth();
        }
    }
}