﻿//using System;
//using System.Net.Http;
//using System.Web.Http;
//using System.Web.Http.Controllers;
//using System.Web.Http.Dispatcher;
//using XAct;

//namespace App.Core.AppHost.Web.Initialization.Steps.PostIoC
//{
//    using System.Web.Http.ExceptionHandling;
//    using System.Web.Mvc;
//    using System.Web.Mvc.Async;
//    using XAct.Services;
//    using XAct.Services.IoC;

//    public class ReplaceMvcFactoriesInitializationStep : IHasInitializationStep
//    {


//        public void Execute()
//        {

//            //For WebAPI:
//            //var x = GlobalConfiguration.Configuration.DependencyResolver;
//            //GlobalConfiguration.Configuration.DependencyResolver = new WebAPIServiceLocatorDependencyResolver();

//            //After we've done the headless initialization, 
//            //we do this Host (the 'Head') specific initialization:
//            XAct.Services.IoC.MvcBootstrapper.Initialize();



//            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(new BindingDescriptor(BindingType.Custom, typeof(ITempDataProvider), typeof(SessionStateTempDataProvider)));
//            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(new BindingDescriptor(BindingType.Custom, typeof(System.Web.Mvc.ITempDataProviderFactory), typeof(ServiceLocatorTempDataProviderFactory)));


//            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(new BindingDescriptor(BindingType.Custom, typeof(IAsyncActionInvoker), typeof(AsyncControllerActionInvoker)));
//            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(new BindingDescriptor(BindingType.Custom, typeof(IAsyncActionInvokerFactory), typeof(ServiceLocatorAsyncActionInvokerFactory)));

//            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(new BindingDescriptor(BindingType.Custom, typeof(IExceptionHandler), typeof(System.Web.Http.ExceptionHandling.ExceptionHandler)));


//            //IHttpControllerActivator
//            //ServiceActivator
//        }
//    }
//}


//namespace XAct.Services.IoC
//{
//    using System.Web.Mvc;
//    using System.Web.Mvc.Async;


//    public class ServiceActivator : IHttpControllerActivator
//    {
//        public ServiceActivator(HttpConfiguration configuration) { }

//        public IHttpController Create(HttpRequestMessage request
//            , HttpControllerDescriptor controllerDescriptor, Type controllerType)
//        {
//            var controller = XAct.DependencyResolver.Current.GetInstance(controllerType) as IHttpController;
//            return controller;
//        }
//    }

//    public class ServiceLocatorAsyncActionInvokerFactory : IAsyncActionInvokerFactory
//    {
//        public IAsyncActionInvoker CreateInstance()
//        {
//            IAsyncActionInvoker result = XAct.DependencyResolver.Current.GetInstance<IAsyncActionInvoker>(false);
//            return result ?? new AsyncControllerActionInvoker();
//        }
//    }

//    public class ServiceLocatorTempDataProviderFactory : ITempDataProviderFactory
//    {
//        public ITempDataProvider CreateInstance()
//        {
//            ITempDataProvider result = XAct.DependencyResolver.Current.GetInstance<ITempDataProvider>(false);
//            return result ?? new SessionStateTempDataProvider();
//        }
//    }
//}