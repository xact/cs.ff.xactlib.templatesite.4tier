﻿using System.Web.Http;
using XAct.Services;

namespace App.Core.AppHost.Web.Initialization.Steps.PostIoC
{
    using XAct;

    //[DefaultBindingImplementation(typeof(IRegisterWebAPIRoutesInitializationStep))]
    public class RegisterWebAPIRoutesInitializationStep : IHasInitializationStep//: IRegisterWebAPIRoutesInitializationStep
    {
        public void Execute()
        {
            //4.0
			//WebApiConfig.Register(GlobalConfiguration.Configuration);
            //5.0
            GlobalConfiguration.Configure(WebApiConfig.Register);

            //routes.MapHttpRoute(
            //  name: "DefaultApi",
            //  routeTemplate: "api/{area}/{controller}/{id}",
            //  defaults: new { id = RouteParameter.Optional }
            //);
        }
    }


    //[DefaultBindingImplementation(typeof(IRegisterWebApiAreaRouterInitializationStep))]
}