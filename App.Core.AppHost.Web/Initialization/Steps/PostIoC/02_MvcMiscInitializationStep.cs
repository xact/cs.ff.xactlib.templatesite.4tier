﻿using System.Diagnostics;
using System.Web.Mvc;
using XAct;
using XAct.Services;

namespace App.Core.AppHost.Web.Initialization.Steps.PostIoC
{

    public class MvcMiscInitializationStep : IHasInitializationStep
    {

        public void Execute()
        {

            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                new BindingDescriptor(
                    BindingType.Undefined, 
                    typeof(System.Web.Mvc.ITempDataProvider), 
                    typeof(SessionStateTempDataProvider),
                    BindingLifetimeType.Undefined
                    ));


            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                new BindingDescriptor(
                    BindingType.Undefined,
                    typeof(System.Web.Mvc.Async.IAsyncActionInvoker),
                    typeof(System.Web.Mvc.Async.AsyncControllerActionInvoker),
                    BindingLifetimeType.Undefined
                    ));

#if DEBUG
            Debug.Assert(XAct.DependencyResolver.Current.GetInstance<System.Web.Mvc.ITempDataProvider>(false) != null);
            Debug.Assert(XAct.DependencyResolver.Current.GetInstance<System.Web.Mvc.Async.AsyncControllerActionInvoker>(false) != null);
#endif 

        }
    }
}