﻿using System.Web.Mvc;
using App.Core.Infrastructure.Services;

namespace App.Core.AppHost.Web.Controllers
{
    public class SetupController : Controller
    {
 
        IDateTimeService _dateTimeService;

        public SetupController(IDateTimeService dateTimeService)
        {
            _dateTimeService = dateTimeService;
        }

        //
        // GET: /Setup/

        public ActionResult Index()
        {
            return View();
        }

 
    }
}