﻿using System;
using System.Web.Mvc;
using XAct;
using App.Core.Infrastructure.Services;

namespace App.Core.AppHost.Web.Controllers
{
    public class MvcHomeController : Controller
    {
        IDateTimeService _dateTimeService;

        public MvcHomeController(IDateTimeService dateTimeService)
        {
            _dateTimeService = dateTimeService;
        }

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

		// Test method, to test exception logging
		public ActionResult Exception()
		{
			throw new Exception("This is a test exception");
		}

    }
}
