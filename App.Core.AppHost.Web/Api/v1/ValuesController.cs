﻿using System.Collections.Generic;
using System.Web.Http;

namespace App.Core.AppHost.Web.Controllers
{
    
    [RoutePrefix("api/v1/values")]
    public class ValuesController : ApiController
    {
        //The downside of using RoutePrefix, is
        //that without adding a RouteAttribute, it defaults back
        //to Routing by Convention (ie, what's in RouteConfig.cs)
        [Route("")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("{id:int}")]
        public string Get(int id)
        {
            return "value!";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}