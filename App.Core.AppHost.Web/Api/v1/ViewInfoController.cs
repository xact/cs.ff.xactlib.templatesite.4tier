﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Http;
using App.Core.Infrastructure.Services;
using XAct;
using XAct.Environment;
using IEnvironmentService = App.Core.Infrastructure.Services.IEnvironmentService;

namespace App.Core.AppHost.Web.Api.v1
{
    [RoutePrefix("api/v1/viewinfo")]
    public class ViewInfoController : ApiController
    {
        private readonly ITracingService _tracingService;
        private readonly IEnvironmentService _environmentService;
        private readonly IProductInformationService _productInformationService;

        public ViewInfoController(ITracingService tracingService, IEnvironmentService environmentService, IProductInformationService productInformationService)
        {
            _tracingService = tracingService;
            _environmentService = environmentService;
            _productInformationService = productInformationService;
            //IProductInformationService
            
        }

        [Route()]
        public ViewInfoBase[] Get()
        {
            var x = _productInformationService.GetProductInformation();

            return new ViewInfoBase[] { new ViewInfoApp
            {

                Title = x.ProductName, 
                SubTitle = x.ProductDisplaySubTitleResourceKey, 
                Version =  new Version(1, 1)
            } };
        }

        //[Route("{id:string}")]
        public ViewInfoBase[] Get(string id)
        {
            if (id.IsNullOrEmpty())
            {
                id = "Default";
            }
            var results = new List<ViewInfoBase>();
            if (id == "Default")
            {
                results.Add(
                    new ViewInfoApp
                    {
                        Title="MyApp",
                        SubTitle="MySubTitle",
                        Version = new Version(1,1)
                    });
            }
            return results.ToArray();
        }

    }
    [DataContract]
    //[ServiceKnownType("GetKnownTypes")]
    [KnownType("GetKnownTypes")]
    public abstract class ViewInfoBase
    {
        public static Type[] GetKnownTypes()
        {
            // collect and pass back the list of known types
            return new[] { typeof(ViewInfoApp) };
        }

        /// <summary>
        /// Gets or sets the unique Module Key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public string ModuleKey { get; set; }

        /// <summary>
        /// Gets or sets the unique Key of the View
        /// within the Module for which this information is for.
        /// </summary>
        public string ViewKey { get; set; }
    }

    public class ViewInfoApp : ViewInfoBase
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public Version Version { get; set; }
    }

    static class KnownTypesProvider
    {
        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            // collect and pass back the list of known types
            return new[] {typeof (ViewInfoApp)};
        }
    }


}
