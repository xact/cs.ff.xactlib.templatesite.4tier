﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace App.Core.AppHost.Web.Messages
{
    [Serializable]
    public class UnhandledFaultException :FaultException
    {
        public UnhandledFaultException()
        {
            
        }
        public UnhandledFaultException(Exception exception)
        {
            
        }
    }
}