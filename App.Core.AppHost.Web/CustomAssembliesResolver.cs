﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http.Dispatcher;

namespace App.Core.AppHost.Web
{

public class CustomAssembliesResolver : DefaultAssembliesResolver
{
    public CustomAssembliesResolver() : base()
    {
        string path = this.GetType().Assembly.Location;

    }

   public override ICollection<Assembly> GetAssemblies()
   {
       ICollection<Assembly> baseAssemblies = base.GetAssemblies();
      List<Assembly> assemblies = new List<Assembly>(baseAssemblies);
      
      // var controllersAssembly = Assembly.LoadFrom(@"C:\libs\controllers\ControllersLibrary.dll");
      
      // baseAssemblies.Add(controllersAssembly);
      return assemblies;
   }
}
}