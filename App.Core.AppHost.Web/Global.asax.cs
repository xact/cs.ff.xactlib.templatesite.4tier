﻿using System.Diagnostics;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using App.Core.Back.AppHost.Initialization;
using App.Core.Infrastructure.Services;
using TraceLevel = System.Diagnostics.TraceLevel;

namespace App.Core.AppHost.Web
{
    using App.Core.AppHost.Web.Integration;
    using App.Core.Infrastructure.Services;

    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {


        IHostSettingsService hostSettingsService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>(); }
        } 
        


	    /// <summary>
        /// Part of the initialization sequence chain.
        /// <para>
        /// The initialization sequence is as follows:
        /// <code>
        /// * Global.asax 
        ///     * ... invokes AppHostInitializer (this class),
        ///         * ... which first invokes ApplicationInitializer
        ///             * ... which first invokes InfrastructureInitializer
        ///               where IoC (ie Unity) is initialized with Infrastructure Services...
        ///         * ....now, coming back up...Application layer services are registered...
        ///     * ... now that both Infras and presentation are initialized, AppHostInitializer
        ///       finishes up by registering AppHost level Services.
        /// * Initialization done...
        /// * Integration cycle then starts...
        ///   It too goes down through the layers, and back up, in much the same way... 
        ///   but this time actually using remote services (eg, to load caches, etc).      
        /// </code>
        /// </para>
        /// </summary>
        protected void Application_Start()
        {
            XAct.Library.Settings.State.WhenHttpStateIsNotAvailableMakeContextStateThreadBasedRatherThanSingleton = false;


            
            //GlobalConfiguration.Configuration.Services.Replace(
            //    typeof(IAssembliesResolver), 
            //    new CustomAssembliesResolver());

            AppHostInitializer.Execute();

            //If we can Integrate now, let's do it:
            if (System.Web.HttpContext.Current != null)
            {
                AppHostIntegrator.Execute();
            }


			Trace.TraceInformation("Application tier starting.");
        }

		protected void Application_Disposed()
		{
			Trace.TraceInformation("Application tier stopping.");
		}
         
		protected void Application_Error()
		{
			// Log unhandled exceptions
			var lastException = Server.GetLastError();
			var logger = XAct.DependencyResolver.Current.GetInstance<ITracingService>();
			logger.TraceException(TraceLevel.Error, lastException, "Unhandled exception on App BackWeb");
		}

		protected void Application_BeginRequest()
		{
		}
    }
}