﻿using System.Web.Http.Dependencies;
using XAct.Diagnostics;
using IDependencyResolver = System.Web.Http.Dependencies.IDependencyResolver;

namespace XActDebug.Services.IoC
{
    /// <summary>
    /// 
    /// </summary>
    public class WebAPIServiceLocatorDependencyResolver : WebAPIServiceLocatorDependencyScope, IDependencyResolver
    {
        private readonly ITracingService _tracingService;
        //System.Web.Http.Dependencies.IDependencyResolver _previousDependencyResolver
        //    = System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebAPIServiceLocatorDependencyResolver"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public WebAPIServiceLocatorDependencyResolver(ITracingService tracingService)
            : base(tracingService)
        {
            _tracingService = tracingService;
        }

        /// <summary>
        /// Starts a resolution scope.
        /// </summary>
        /// <returns>
        /// The dependency scope.
        /// </returns>
        public IDependencyScope BeginScope()
        {
            return new WebAPIServiceLocatorDependencyScope(_tracingService);
        }

    }

}