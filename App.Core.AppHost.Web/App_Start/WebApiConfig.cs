﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;

namespace App.Core.AppHost.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            ControllerBuilder.Current.DefaultNamespaces.Add("App.Modules.Example.Presentation.Web.API.PublicAPI.v1.Controllers");
            
            FilterConfig.RegisterHttpFilters(GlobalConfiguration.Configuration.Filters);

            //Turn on v2 mapping 'by attributes' (as oppossed to 'by convention'):
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );



        }



    }
}
