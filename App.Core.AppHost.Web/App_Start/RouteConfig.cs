﻿using System.Web.Mvc;
using System.Web.Routing;

namespace App.Core.AppHost.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("Contents/{*pathInfo}");

            //Obsolete: RouteTable.Routes.MapHubs();

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",

                
                //I much prefer creating a SPA for both front and back,
                //but for now, we'll just go with a more mundane MVC
                //site for the back tier.
                defaults: new { controller = "MvcHome", action = "Index", id = UrlParameter.Optional }
            );



        }
    }
}