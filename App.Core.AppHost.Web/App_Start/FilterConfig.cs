﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using App.Core.Infrastructure.Services;
using IExceptionFilter = System.Web.Mvc.IExceptionFilter;

namespace App.Core.AppHost.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
			filters.Add(new LogExceptionFilter());
            filters.Add(new HandleErrorAttribute());
        }

		public static void RegisterHttpFilters(HttpFilterCollection filters)
		{
			filters.Add(new LogWebApiErrorAttribute());
		}

		// TODO: Move this to a (new) Shared.Web kinda project


		/// <summary>
		/// Types of exceptions that are related to the security processing.
		/// (They're not 'exceptional'/unexpected states
		/// </summary>
		private static List<Type> _securityExceptionTypes = new List<Type>()
		{
		    typeof (XAct.Security.NotAuthenticatedException),
		    typeof (XAct.Security.NotAuthorizedException)
	    };

		/// <summary>
		/// Log unhandled MVC exceptions
		/// (This seems to be more reliable that using Application_Error event)
		/// </summary>
		public class LogExceptionFilter : IExceptionFilter
		{
            private ITracingService _tracingService;
            private ITracingService TracingService
            {
                // Lazy load tracing service, as it can't reliably be done in the constructor (it's called in the MVC boot sequence
                get { return _tracingService ?? (_tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>()); }
            }

			public void OnException(ExceptionContext context)
			{
				Exception ex = context.Exception;
				if (!(ex is HttpException)) // Ignore "file not found"
				{
                    TracingService.TraceException(TraceLevel.Error, ex, "Unhandled web exception");
				}
			}
		}

		/// <summary>
		/// Log WebAPI Exceptions
		/// (Note - This is a different filter to the main MVC error handling)
		/// </summary>
		public class LogWebApiErrorAttribute : System.Web.Http.Filters.ExceptionFilterAttribute
		{
		    private ITracingService _tracingService;
		    private ITracingService TracingService
		    {
                // Lazy load tracing service, as it can't reliably be done in the constructor (it's called in the MVC boot sequence
		        get { return _tracingService ?? (_tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>()); }
		    }

			public override void OnException(
				System.Web.Http.Filters.HttpActionExecutedContext actionExecutedContext)
			{
				if (actionExecutedContext.Exception != null)
				{
					var isSecurityException = _securityExceptionTypes.Contains(actionExecutedContext.Exception.GetType());

					// For security exeptions - Just log them at Debug level, as they don't need to notify the admins
					// - Everything else - 'Error' level, to alert all trace listeners
					var traceLevel = isSecurityException
						? TraceLevel.Verbose
						: TraceLevel.Error;

					var errorHeader = isSecurityException
						? "Security exeception"
						: "Unhandled backend exception";

					TracingService.TraceException(traceLevel, actionExecutedContext.Exception, errorHeader);

                    TracingService.TraceException(TraceLevel.Error, actionExecutedContext.Exception, "Unhandled WebAPI exception");
				}

				base.OnException(actionExecutedContext);
			}
		}
    }
}